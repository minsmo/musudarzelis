<?php
date_default_timezone_set('Europe/Vilnius');//PHP 5.2 warning

register_shutdown_function('shutdownHandler');
function shutdownHandler() {
	if(connection_aborted()) {
		$bt = debug_backtrace();
		$bt0 = (isset($bt[0]) && isset($bt[0]['file']) ? basename(dirname($bt[0]['file'])).'/'.basename($bt[0]['file']).':'.$bt[0]['line'] : '');
		$bt1 = (isset($bt[1]) ? basename(dirname($bt[1]['file'])).'/'.basename($bt[1]['file']).':'.$bt[1]['line'] : '');
		$bt2 = (isset($bt[2]) ? basename(dirname($bt[2]['file'])).'/'.basename($bt[2]['file']).':'.$bt[2]['line'] : '');
		$errorType = array(
			E_ERROR          => 'ERROR',
			E_WARNING        => 'WARNING',
			E_PARSE          => 'PARSING ERROR',
			E_NOTICE         => 'NOTICE',
			E_CORE_ERROR     => 'CORE ERROR',
			E_CORE_WARNING   => 'CORE WARNING',
			E_COMPILE_ERROR  => 'COMPILE ERROR',
			E_COMPILE_WARNING => 'COMPILE WARNING',
			E_USER_ERROR     => 'USER ERROR',
			E_USER_WARNING   => 'USER WARNING',
			E_USER_NOTICE    => 'USER NOTICE',
			E_STRICT         => 'STRICT NOTICE',
			E_RECOVERABLE_ERROR  => 'RECOVERABLE ERROR'
		);
		
		//TODO: SQLite + http://php.net/manual/en/features.connection-handling.php
		$text = date("y-m-d H:i:s").json_encode($_GET).json_encode($_POST).(defined('DB_PREFIX') ? DB_PREFIX : '').'  '.$_SERVER['REMOTE_ADDR'].' '.$_SERVER['REQUEST_METHOD'].' '.$_SERVER['REQUEST_URI'].' '.$_SERVER['HTTP_USER_AGENT'].(isset($_SERVER['HTTP_REFERER']) ? ' REF:'.$_SERVER['HTTP_REFERER'].' ' : '').(isset($_SESSION['USER_DATA']['email']) ? ' '.$_SESSION['USER_DATA']['email'] : '')." USER_ID:".USER_ID.' '.$bt0.' '.$bt1.' '.$bt2."\n";
		file_put_contents($PATH.'/private/logs/critical.log', 'connection_aborted:'.$text, FILE_APPEND | LOCK_EX);
	}

	if(!is_null($lasterror = error_get_last())) {
		$bt = debug_backtrace();
		$bt0 = (isset($bt[0]) && isset($bt[0]['file']) ? basename(dirname($bt[0]['file'])).'/'.basename($bt[0]['file']).':'.$bt[0]['line'] : '');
		$bt1 = (isset($bt[1]) ? basename(dirname($bt[1]['file'])).'/'.basename($bt[1]['file']).':'.$bt[1]['line'] : '');
		$bt2 = (isset($bt[2]) ? basename(dirname($bt[2]['file'])).'/'.basename($bt[2]['file']).':'.$bt[2]['line'] : '');
		$errorType = array(
			E_ERROR          => 'ERROR',
			E_WARNING        => 'WARNING',
			E_PARSE          => 'PARSING ERROR',
			E_NOTICE         => 'NOTICE',
			E_CORE_ERROR     => 'CORE ERROR',
			E_CORE_WARNING   => 'CORE WARNING',
			E_COMPILE_ERROR  => 'COMPILE ERROR',
			E_COMPILE_WARNING => 'COMPILE WARNING',
			E_USER_ERROR     => 'USER ERROR',
			E_USER_WARNING   => 'USER WARNING',
			E_USER_NOTICE    => 'USER NOTICE',
			E_STRICT         => 'STRICT NOTICE',
			E_RECOVERABLE_ERROR  => 'RECOVERABLE ERROR'
		);
		$text = date("y-m-d H:i:s").(defined('DB_PREFIX') ? DB_PREFIX : '').' '.$errorType[$lasterror['type']]." (".$lasterror['message'].") ".str_replace(PATH.'/', '', $lasterror['file']).":".$lasterror['line']." ".$_SERVER['REMOTE_ADDR'].' '.$_SERVER['REQUEST_METHOD'].' '.$_SERVER['REQUEST_URI'].' '.$_SERVER['HTTP_ACCEPT_LANGUAGE'].' '.$_SERVER['HTTP_USER_AGENT'].(isset($_SERVER['HTTP_REFERER']) ? ' REF:'.$_SERVER['HTTP_REFERER'].' ' : '').(isset($_SESSION['USER_DATA']['email']) ? ' '.$_SESSION['USER_DATA']['email'] : '')." USER_ID:".USER_ID.' '.$bt0.' '.$bt1.' '.$bt2."\n";
		$PATH = defined('PATH') ? PATH : str_replace('/var/pseudo/', '/home/kestutis/', $_SERVER['DOCUMENT_ROOT']);
		switch ($lasterror['type']) {
			case E_ERROR:
			case E_CORE_ERROR:
			case E_COMPILE_ERROR:
			case E_USER_ERROR:
			case E_RECOVERABLE_ERROR:
			case E_CORE_WARNING:
			case E_COMPILE_WARNING:
			case E_PARSE:
				file_put_contents($PATH.'/private/logs/critical.log', $text, FILE_APPEND | LOCK_EX);
				mail('forkik@gmail.com', 'Error from auto_prepend '.str_replace('/var/pseudo/', '', $_SERVER['DOCUMENT_ROOT']), $text); 
			break;
			default:
				file_put_contents($PATH.'/private/logs/critical.log', 'default:'.$text, FILE_APPEND | LOCK_EX);
		}
	}
}
//2) Then add this "php_value auto_prepend_file /www/auto_prepend.php" to your .htaccess file in the web root.
//http://php.net/manual/en/function.set-error-handler.php


//Šiaip http://stackoverflow.com/questions/18050071/php-parse-syntax-errors-and-how-to-solve-them
