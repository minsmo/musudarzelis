<?php
//header('P3P: CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
//http://anvilstudios.co.za/blog/2010/08/23/session-cookies-faulty-in-ie8/
//http://genotrance.wordpress.com/2006/11/23/session-cookies-rejected-by-internet-explorer/
//http://aspnetresources.com/blog/frames_webforms_and_rejected_cookies
//http://www.phinesolutions.com/ie-session-cookie-problem.html
//http://stackoverflow.com/questions/21718788/php-session-cookies-and-internet-explorer
//ini_set("session.cookie_lifetime", "0");
session_set_cookie_params(0);
session_name('MUSUDARZELIS');
//setcookie('MUSUDARZELIS', '', time()-3600); 
session_start();

$person_type = array(
	0 => 'Tėvų',
	1 => 'Auklėtojo-mokytojo',
	2 => 'Buhalterio',
	3 => 'Vadovo',//Vadovybės
	4 => 'Muzikos pedagogo',
	5 => 'Medicinos seselės',
	6 => 'Logopedo',
	7 => 'Meninio ugdymo pedagogo',
	8 => 'Kūno kultūros pedagogo',
	9 => 'Dietisto',
	10 => 'Kineziterapeuto',
	11 => 'Socialinio pedagogo'
);
$person_type_extended = array(
	0 => 'Tėvų',
	1 => 'Auklėtojo-mokytojo',
	2 => 'Buhalterio',
	3 => 'Vadovo',//Vadovybės
	4 => 'Muzikos pedagogo',
	5 => 'Medicinos seselės',
	6 => 'Logopedo (spec. pedagogo, surdopedagogo, tiflopedagogo ir psichologo)',
	7 => 'Meninio ugdymo pedagogo',
	8 => 'Kūno kultūros pedagogo',
	9 => 'Dietisto (virėjos?)',
	10 => 'Kineziterapeuto',
	11 => 'Socialinio pedagogo'
);
asort($person_type_extended);
$person_type_who = array(
	0 => 'Tėvas',
	1 => 'Auklėtojas-mokytojas',
	2 => 'Buhalteris',
	3 => 'Vadovas',//Vadovybė
	4 => 'Muzikos pedagogas',
	5 => 'Medicinos darbuotojas',
	6 => 'Logopedas',
	7 => 'Meninio ugdymo pedagogas',
	8 => 'Kūno kultūros pedagogas',
	9 => 'Dietistas',
	10 => 'Kineziterapeutas',
	11 => 'Socialinis pedagogas'
);
asort($person_type_who);
$person_type_who_dgs/*pl. plural*/ = array(
	0 => 'Tėvai',
	1 => 'Auklėtojai-mokytojai',
	2 => 'Buhalteriai',
	3 => 'Vadovai',//Vadovybė
	4 => 'Muzikos pedagogai',
	5 => 'Medicinos darbuotojai',
	6 => 'Logopedai',
	7 => 'Meninio ugdymo pedagogai',
	8 => 'Kūno kultūros pedagogai',
	9 => 'Dietistai',
	10 => 'Kineziterapeutai',
	11 => 'Socialiniai pedagogai'
);
$person_type_sau = array(
	0 => 'Tėveli',
	1 => 'Auklėtojau-mokytojau',//Nuo grupių
	2 => 'Buhalteri',
	3 => 'Vadove',//Vadovybe
	4 => 'Muzikos pedagoge',//Nuo grupių
	5 => 'Medicinos sesele',
	6 => 'Logopede',
	7 => 'Meninio ugdymo pedagoge',//Nuo grupių
	8 => 'Kūno kultūros pedagoge',//Nuo grupių
	9 => 'Dietiste',
	10 => 'Kineziterapeute',
	11 => 'Socialini pedagoge'
);

$all_groups_for_person_type = array(
	3 => 'vadovui',
	2 => 'buhalteriui',
	5 => 'medicinos seselei',
	6 => 'logopedui',
	9 => 'dietistui',
	10 => 'kineziterapeutui',
	11 => 'Socialiniam pedagogui'
);

$login_depends_on_assigned_groups = array();
foreach($person_type_sau as $id => $dumb)
	if($id != 0 && !isset($all_groups_for_person_type[$id]))
		$login_depends_on_assigned_groups[$id] = '';
$all_groups_for_person_type_who = array();
foreach($all_groups_for_person_type as $id => $dump)
		$all_groups_for_person_type_who[$id] = mb_lcfirst($person_type_who[$id]);


$person_subtype = array(
	0 => '&lt;Nenustatyta&gt;',
	1 => 'Mama',
	2 => 'Tėtis'
);
$user_type = array(
	1 => 'Mama',
	2 => 'Tėvas',
	//Darbuotojas?
);
define('PARENTS', 0);

$workerGroupsAccessBehaviour = [
	0 => 'default',
	1 => 'force_all_groups',
	2 => 'force_only_selected_groups',
];
define('workerGroupsAccessBehaviour\default_', 0);
define('workerGroupsAccessBehaviour\force_all_groups', 1);
define('workerGroupsAccessBehaviour\force_only_selected_groups', 2);

if(isset($_GET['atsijungti'])) {
	$_SESSION = array();
	session_destroy();
	/*
	$_SESSION['USER'] = false;
	$_SESSION['DARBUOT'] = false;
	$_SESSION['ADMIN'] = false;
	unset($_SESSION['GROUP_ID']);
	unset($_SESSION['CHILD_ID']);
	unset($_SESSION['USER_ID']);
	unset($_SESSION['DARB_ID']);
	unset($_SESSION['DB_PREFIX']);
    //$_SESSION['BUHALT'] = false;//buhalteris*/
	header("Location: ?");
    //header("Location: http://zvangutis.darzelis.albinas.lt/");
}
$pass = 0;
//$pass_multiKids = 0;
$last_prefix = '';
$last_type = 0;
$last_id = 0;
$last_person_type = 0;
if(KESV && isset($_GET['Lpastas'])) {
	$_POST['pastas'] = $_GET['Lpastas'];
	$_POST['slaptazodis'] = '';
	$_POST['prisijungimas'] = '';
}
						
if(isset($_POST['pastas']) &&  isset($_POST['slaptazodis']) && isset($_POST['prisijungimas'])) {//DRY:
	if( !empty($_POST['pastas']) ) {//&& !empty($_POST['slaptazodis']) ) {
		$found = [];
		$found_cnt = 0;
		unset($_SESSION['second']);
		foreach($db_prefixes as $ID => $prefix) {
			//if(!($_SERVER['SERVER_NAME'] == 'dev.musudarzelis.lt' && $prefix == 'r_')) {
			//if(!(($_SERVER['SERVER_NAME'] == 'musudarzelis.lt' || $_SERVER['SERVER_NAME'] == 'www.musudarzelis.lt' || $_SERVER['SERVER_NAME'] == 'devr.musudarzelis.lt') && $prefix == 't_')) {
				
				if(KESV) {
					$r_check = db_query("SELECT *
					FROM `".$prefix."users` LEFT JOIN `".$prefix."users_allowed` ON `".$prefix."users_allowed`.`user_id`=`".$prefix."users`.`user_id`
					WHERE `email`='".mysqli_real_escape_string($db_link, $_POST['pastas'])."'", 'aaNeteisinga užklausa: ');
					$r_check_cnt = mysqli_num_rows($r_check);
					$r_cnt = 0;
					if($r_check_cnt > 0) {
						$r = db_query("SELECT * 
						FROM `".$prefix."users` JOIN `".$prefix."users_allowed` ON `".$prefix."users_allowed`.`user_id`=`".$prefix."users`.`user_id`
						WHERE `email`='".mysqli_real_escape_string($db_link, $_POST['pastas'])."'", 'bbNeteisinga užklausa: ');
						$r_cnt = mysqli_num_rows($r);
					}
				} else {
					$r_check = db_query("SELECT *
					FROM `".$prefix."users` LEFT JOIN `".$prefix."users_allowed` ON `".$prefix."users_allowed`.`user_id`=`".$prefix."users`.`user_id`
					WHERE `email`='".mysqli_real_escape_string($db_link, $_POST['pastas'])."' AND `password`='".passwd_hash($_POST['slaptazodis'])."'", 'aaNeteisinga užklausa: ');
					$r_check_cnt = mysqli_num_rows($r_check);
					$r_cnt = 0;
					if($r_check_cnt > 0) {
						$r = db_query("SELECT * 
						FROM `".$prefix."users` JOIN `".$prefix."users_allowed` ON `".$prefix."users_allowed`.`user_id`=`".$prefix."users`.`user_id`
						WHERE `email`='".mysqli_real_escape_string($db_link, $_POST['pastas'])."' AND `password`='".passwd_hash($_POST['slaptazodis'])."'", 'bbNeteisinga užklausa: ');
						$r_cnt = mysqli_num_rows($r);
					}
				}
				if($r_check_cnt != $r_cnt) {//gal > vietoj !=
					++$found_cnt;
					$bad_login_msg = "<script type=\"text/javascript\">alert('$r_check_cnt $r_cnt Nors Jums prisijungimas suteiktas, tačiau neduotas leidimas dirbti su sistema. Jei norite prisijungti prie sistemos ir su ja dirbti, prašome kreiptis į darželio vadovą.'); window.history.back()</script>";//Nors Jums prisijungimas duotas, bet nesuteiktas leidimas matyti ar daryti kažką šioje sistemoje. Jei norite kažką matyti ar daryti šioje sistemoje prašome kreiptis į darželio vadovybę, kad suteiktų teisių kažką matyti ar daryti šioje sistemoje.
					goto normal;
				} elseif($r_cnt >= 1) {
					++$found_cnt;
					$user = mysqli_fetch_assoc($r);
					//$_SESSION['second'][$prefix]['USER_DATA'] = $user;
					
					$result_d = db_query("SELECT * 
						FROM `".$prefix."users_allowed` JOIN `".$prefix."darbuotojai` ON (`".$prefix."users_allowed`.`person_id`=`".$prefix."darbuotojai`.`ID` AND `".$prefix."darbuotojai`.`isDeleted`=0)
						WHERE `".$prefix."users_allowed`.`person_type`>0 AND `".$prefix."users_allowed`.`user_id`=".(int)$user['user_id'], 'aNeteisinga užklausa: ');
					if(mysqli_num_rows($result_d) >= 1) {
						$last_prefix = $prefix;
						$last_type = 'employees';
						while($row = mysqli_fetch_assoc($result_d)) {
							$last_id = $row['ID'];
							$last_person_type = $row['person_type'];
							$_SESSION['second'][$prefix]['USER_DATA'][$row['person_type']] = $user;
							$_SESSION['second'][$prefix]['USER_DATA'][$row['person_type']]['person_type'] = $row['person_type'];
							$_SESSION['second'][$prefix]['USER_DATA'][$row['person_type']]['person_subtype'] = $row['person_subtype'];//TODO: maybe merge with unset()
							$_SESSION['second'][$prefix]['employees'][$row['ID'].'.'.$row['person_type']] = $row;
							++$pass;
						}
						//++$pass_multiKids;
					}
					
					//if($parents_access_allowance[$ID]) {
					$result = db_query("SELECT * 
					FROM `".$prefix."users_allowed` JOIN `".$prefix."vaikai` ON `".$prefix."users_allowed`.`person_id`=".$prefix."vaikai.ID AND `".$prefix."users_allowed`.`person_type`=0 AND `".$prefix."vaikai`.`isDeleted`=0
					WHERE `".$prefix."users_allowed`.`user_id`=".(int)$user['user_id'], 'bNeteisinga užklausa: ');//TODO: FIX BUG when primary kid ID record is marked as deleted
					if(mysqli_num_rows($result) >= 1) {
						$last_prefix = $prefix;
						$last_type = 'kids';
						//++$pass_multiKids;
						while($child = mysqli_fetch_assoc($result)) {
							$last_id = $child['ID'];
							$last_person_type = $child['person_type'];
							$_SESSION['second'][$prefix]['USER_DATA'][$child['person_type']] = $user;
							$_SESSION['second'][$prefix]['USER_DATA'][$child['person_type']]['person_type'] = $child['person_type'];
							$_SESSION['second'][$prefix]['USER_DATA'][$child['person_type']]['person_subtype'] = $child['person_subtype'];//TODO:  maybe merge with unset()
							$_SESSION['second'][$prefix]['kids'][$child['ID']] = $child;
							++$pass;
						}
					}
					//}
				} /*else {
		    			echo($prefix.'Kiekis: '.mysqli_num_rows($r).'<br>');
				}*/
			//}
			//}
		}
		if($found_cnt == 0) {
			$bad_login_msg = "<script type=\"text/javascript\">alert('Klaidingi prisijungimo duomenys (".filterText($_POST['pastas'])." ir slaptažodis ".mb_strlen($_POST['slaptazodis'])." simbolių). Prašome bandyti dar kartą.\\n\\nSiūlome pasitikrinti: 1. Ar teisingai įvedėte el. pašto adresą ir slaptažodį; 2. Ar klaviatūroje neįjungtas rašymas tik didžiosiomis raidėmis (klavišas„Caps Lock“). 3. Jei slaptažodyje yra skaičių, tai ar įjungtas skaičių įvedimas klaviatūroje (klavišas „Num Lock“).\\n\\nNaują slaptažodį galite gauti į savo el. pašto dėžutę nuėję į „Pamiršau slaptažodį“.');</script>";// window.history.go(-1);
			//http://stackoverflow.com/questions/14420624/how-to-keep-changed-form-content-when-leaving-and-going-back-to-https-page-wor
			$_SESSION['USER'] = false;
			$_SESSION['DARBUOT'] = false;
			$_SESSION['ADMIN'] = false;
	        //$_SESSION['BUHALT'] = false;
			define("USER", false);
			define("DARBUOT", false);
			define("ADMIN", false);
	       // define("BUHALT", false);
  			db_query("INSERT INTO `0bad_logins` SET `ipv4`=INET_ATON('".$_SERVER['REMOTE_ADDR']."'), `email`='".mysqli_real_escape_string($db_link, $_POST['pastas'])."'");
	       goto normal;//Show form
		} else {
			/*if(!defined('USER')) {
				if(!isset($no_groups))
					$bad_login_msg = "<script type=\"text/javascript\">alert('Nors Jums prisijungimas duotas, bet nesuteiktas leidimas matyti ar daryti kažką šioje sistemoje. Jei norite kažką matyti ar daryti šioje sistemoje prašome kreiptis į darželio vadovybę, kad suteiktų teisių kažką matyti ar daryti šioje sistemoje.'); window.history.back()</script>";
				else
					$bad_login_msg = "<script type=\"text/javascript\">alert('Nors Jums prisijungimas duotas, bet nepriskirta jokia grupė. Jei norite kažką matyti ar daryti šioje sistemoje prašome kreiptis į darželio vadovybę, kad priskirtų Jus prie grupės, kad galėtumėte kažką matyti ar daryti šioje sistemoje.'); window.history.back()</script>";
				goto normal;
			}*/
		}
	} else {
		$bad_login_msg = "<script type=\"text/javascript\">alert('El. pašto adresas negali būti tuščias');</script>";// window.history.back()
		goto normal;//Show form
	}
}

if($pass > 1) {//[Additional intermediate step]
	?><!DOCTYPE html>
<html dir="ltr" lang="lt-lt">
<head>
<meta charset="utf-8">
<title>Prisijungimas</title>
<style>
* {font-family: 'Times New Roman', serif; font-size: 16px;}
</style>
</head>
<body>
	<?php
	//echo '<pre>';
	//print_r($_SESSION['second']);
	?>
		<div style="width: 600px; margin: 0 auto;">
		<!-- Pasirinkite leidimo rūšį -->Prisijungti kaip:
		<ul>
		<!-- <select name="secondPass" autofocus> -->
		<?php
	function removeqsvar($url, $varname) {
		list($urlpart, $qspart) = array_pad(explode('?', $url), 2, '');
		parse_str($qspart, $qsvars);
		unset($qsvars[$varname]);
		$newqs = http_build_query($qsvars);
		return $urlpart . '?' . $newqs;
	}
		if(isset($_SESSION['second'])) {
			foreach($_SESSION['second'] as $db_prefix => $people) {
				$result_db_prefix = db_query("SELECT yt.* FROM `".$db_prefix."config` yt JOIN (SELECT `title`, MAX(`valid_from`) `valid_from` FROM `".$db_prefix."config` WHERE `valid_from`<='".CURRENT_DATE."' AND `title`='darzelio_pavadinimas' GROUP BY `title`) ss ON yt.`title`=ss.`title` AND yt.`valid_from`=ss.`valid_from`");
				$title = mysqli_fetch_assoc($result_db_prefix);
		
				$url = removeqsvar('//'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'], 'Lpastas');
				if(isset($people['employees'])) {
					foreach($people['employees'] as $employees_i) {
						echo "<li><form method=\"post\" action=\"$url\"><input type=\"hidden\" name=\"secondPass\" value=\"$db_prefix.employees.".$employees_i['ID'].".".$employees_i['person_type']."\"><input type=\"submit\" value=\"Darbuotojas (".$person_type[$employees_i['person_type']].") – ".$employees_i['vardas']." ".$employees_i['pavarde'].' '.$title['value']."\"></form></li>";
					}
				}
				if(isset($people['kids'])) {
					foreach($people['kids'] as $kids_i) {
						echo "<li><form method=\"post\" action=\"$url\"><input type=\"hidden\" name=\"secondPass\" value=\"$db_prefix.kids.".$kids_i['ID'].".".$kids_i['person_type']."\"><input type=\"submit\" value=\"Vaikas – ".$kids_i['vardas']." ".$kids_i['pavarde'].' '.$title['value']."\"></form></li>";
					}
				}
			}
		}
		?>
		</ul>
		<!-- </select><br>
		<input type="submit" value="Prisijungti šiuo leidimu"> --></p>
	</div>
</body>
</html>
	<?php
	die();//TODO: REVIEW THE LINE
} elseif($pass == 1 || isset($_POST['secondPass'])) {//Final pass
	if($pass == 1) {//Direct
		$id = $last_id;
		$user_person_type = $last_person_type;
	} elseif(isset($_POST['secondPass'])) {//Select
		list($last_prefix, $last_type, $id, $user_person_type) = explode('.', $_POST['secondPass']);
		$id = (int)$id;
		if(!in_array($last_prefix, $db_prefixes))
			die(loga('prefix:'.$last_prefix, 'login'));
		if($last_type == 'employees' && !isset($_SESSION['second'][$last_prefix][$last_type][$id.'.'.$user_person_type]) || $last_type == 'kids' && !isset($_SESSION['second'][$last_prefix][$last_type][$id]))
			die(loga('prefix:'.$last_prefix.'; type:'.$last_type.'; id:'.$id, 'login'));
			
	}
	$_SESSION['USER_DATA'] = $_SESSION['second'][$last_prefix]['USER_DATA'][$user_person_type];

	//$last_type
	if($last_type == 'employees') {
		$employee = $_SESSION['second'][$last_prefix]['employees'][$id.'.'.$user_person_type];
		
		$_SESSION['AUKLETOJA'] = $user_person_type == 1;
		define("AUKLETOJA", $user_person_type == 1);
		$_SESSION['ADMIN'] = $user_person_type == 3;
		define("ADMIN", $user_person_type == 3);
		$_SESSION['BUHALT'] = $user_person_type == 2;
		define("BUHALT", $user_person_type == 2);
		$_SESSION['MUZIKOS_PEDAGOGAS'] = $user_person_type == 4;
		define("MUZIKOS_PEDAGOGAS", $user_person_type == 4);
		$_SESSION['SESELE'] = $user_person_type == 5;
		define("SESELE", $user_person_type == 5);
		$_SESSION['LOGOPEDAS'] = $user_person_type == 6;
		define("LOGOPEDAS", $user_person_type == 6);
		$_SESSION['MENO_PEDAGOGAS'] = $user_person_type == 7;
		define("MENO_PEDAGOGAS", $user_person_type == 7);
		$_SESSION['KUNO_KULTUROS_PEDAGOGAS'] = $user_person_type == 8;
		define("KUNO_KULTUROS_PEDAGOGAS", $user_person_type == 8);
		$_SESSION['DIETISTAS'] = $user_person_type == 9;
		define("DIETISTAS", $user_person_type == 9);
		$_SESSION['KINEZITERAPEUTAS'] = $user_person_type == 10;
		define("KINEZITERAPEUTAS", $user_person_type == 10);
		$_SESSION['SOCIALINIS_PEDAGOGAS'] = $user_person_type == 11;
		define("SOCIALINIS_PEDAGOGAS", $user_person_type == 11);
		//TODO: ???????????
		if(isset($all_groups_for_person_type[$user_person_type]) && $employee['exception_workerGroupsAccess'] == workerGroupsAccessBehaviour\default_ || $employee['exception_workerGroupsAccess'] == workerGroupsAccessBehaviour\force_all_groups) {
			//Is this needed?
			$_SESSION['ALL_GROUPS'] = 1;
			$result = db_query("SELECT * FROM `".$last_prefix."grupes`");
			$groups = mysqli_fetch_assoc($result);
			$_SESSION['GROUP_ID'] = $groups['ID'];
			define("GROUP_ID", (int)$_SESSION['GROUP_ID']);
			//Could be removed:
			$_SESSION['GROUPS'] = array($groups['ID'] => $groups['pavadinimas']);
			while($groups = mysqli_fetch_assoc($result))
				$_SESSION['GROUPS'][$groups['ID']] = $groups['pavadinimas'];
			session_regenerate_id(TRUE);
		} else {
			$result = db_query("SELECT `".$last_prefix."darb_grupes`.`grup_id`, `".$last_prefix."grupes`.`pavadinimas`
			FROM `".$last_prefix."darb_grupes` JOIN `".$last_prefix."grupes` ON `".$last_prefix."grupes`.`ID`=`".$last_prefix."darb_grupes`.`grup_id`
			WHERE `".$last_prefix."darb_grupes`.`darb_id`=".$employee['ID']);
			if(mysqli_num_rows($result)) {
				$groups = mysqli_fetch_assoc($result);
				$_SESSION['GROUP_ID'] = $groups['grup_id'];
				define("GROUP_ID", (int)$_SESSION['GROUP_ID']);
				//Could be removed:
				$_SESSION['GROUPS'] = array($groups['grup_id'] => $groups['pavadinimas']);
				while($groups = mysqli_fetch_assoc($result))
					$_SESSION['GROUPS'][$groups['grup_id']] = $groups['pavadinimas'];
				//setcookie('MUSUDARZELIS', '', time()-3600);
				session_regenerate_id(TRUE);
			} else {
				$no_groups = true;
				$bad_login_msg = "<script type=\"text/javascript\">alert('Nors Jums prisijungimas duotas, bet nepriskirta jokia grupė, todėl Jums neleista prisijungti. Dėl Jūsų priskyrimo prie vaikų grupės kreipkitės į savo įstaigos vadovus ar kitą atsakingą asmenį.');</script>";//Jei norite kažką matyti ar daryti šioje sistemoje prašome kreiptis į darželio vadovus, kad priskirtų Jus prie vaikų grupės, kad galėtumėte kažką matyti ar daryti šioje sistemoje.   window.history.back()  TODO: log, report.
				goto normal;
			}
		}
		if(!isset($no_groups)) {
			$_SESSION['USER'] = true;
			define("USER", true);
			
			$_SESSION['DARBUOT'] = true;
			define("DARBUOT", true);

			$_SESSION['DARB_ID'] = (int)$employee['ID'];
			define("DARB_ID", (int)$employee['ID']);
			
			$_SESSION['USER_ID'] = (int)$employee['user_id'];
			define("USER_ID", (int)$employee['user_id']);
			if(!KESV)
				db_query("UPDATE `".$last_prefix."users` SET `last_login_ipv4`=INET_ATON('".$_SERVER['REMOTE_ADDR']."'), `last_login_time`=CURRENT_TIMESTAMP(), `login_counter`=`login_counter`+1 WHERE `user_id`=".USER_ID);

			$_SESSION['DB_PREFIX'] = $last_prefix;
			define("DB_PREFIX", $_SESSION['DB_PREFIX']);
		}
		/*if($employee['buhalt']) {
			$_SESSION['BUHALT'] = true;
			define("BUHALT", true);
		} else {
			$_SESSION['BUHALT'] = false;
			define("BUHALT", false);
		}*/
	} elseif($last_type == 'kids') {
		$child = $_SESSION['second'][$last_prefix]['kids'][$id];
		$_SESSION['USER'] = true;
		$_SESSION['DARBUOT'] = false;
		$_SESSION['ADMIN'] = false;
		$_SESSION['BUHALT'] = false;
		$_SESSION['AUKLETOJA'] = false;
		$_SESSION['MUZIKOS_PEDAGOGAS'] = false;
		$_SESSION['SESELE'] = false;
		$_SESSION['LOGOPEDAS'] = false;
		$_SESSION['MENO_PEDAGOGAS'] = false;
		$_SESSION['KUNO_KULTUROS_PEDAGOGAS'] = false;
		$_SESSION['DIETISTAS'] = false;
		$_SESSION['KINEZITERAPEUTAS'] = false;
		$_SESSION['SOCIALINIS_PEDAGOGAS'] = false;
		$_SESSION['CHILD_ID'] = (int)$child['parent_kid_id'];
		$_SESSION['USER_ID'] = (int)$child['user_id'];
		$_SESSION['GROUP_ID'] = (int)$child['grupes_id'];
		define("USER", true);
		define("DARBUOT", false);
		define("ADMIN", false);
		define("BUHALT", false);
		define("AUKLETOJA", false);
		define("MUZIKOS_PEDAGOGAS", false);
		define("SESELE", false);
		define("LOGOPEDAS", false);
		define("MENO_PEDAGOGAS", false);
		define("KUNO_KULTUROS_PEDAGOGAS", false);
		define("DIETISTAS", false);
		define("KINEZITERAPEUTAS", false);
		define("SOCIALINIS_PEDAGOGAS", false);
		define("CHILD_ID", (int)$child['parent_kid_id']);
		define("USER_ID", (int)$child['user_id']);
		define("GROUP_ID", (int)$child['grupes_id']);
		
		if(!KESV)
			db_query("UPDATE `".$last_prefix."users` SET `last_login_ipv4`=INET_ATON('".$_SERVER['REMOTE_ADDR']."'), `last_login_time`=CURRENT_TIMESTAMP(), `login_counter`=`login_counter`+1 WHERE `user_id`=".USER_ID);
		
		$_SESSION['DB_PREFIX'] = $last_prefix;
		define("DB_PREFIX", $_SESSION['DB_PREFIX']);
		
		$children = array();
		foreach($_SESSION['second'][$last_prefix]['kids'] as $kid)
			$children[$kid['ID']] = $kid;
		$_SESSION['children'] = $children;
		session_regenerate_id(TRUE);
	}
}
	//if(isset($found_cnt) && $found_cnt > 0 && isset($_POST['pastas']) &&  isset($_POST['slaptazodis']) && isset($_POST['prisijungimas']))
		
normal:
	if(!defined('USER')) {
		define("USER", isset($_SESSION['USER']) && $_SESSION['USER']);
	}
	if(!defined('DARBUOT')) {
		define("DARBUOT", isset($_SESSION['DARBUOT']) && $_SESSION['DARBUOT']);
	}
	if(!defined('ADMIN')) {
		define("ADMIN", isset($_SESSION['ADMIN']) && $_SESSION['ADMIN']);
	}
	if(!defined('BUHALT')) {
    	define("BUHALT", isset($_SESSION['BUHALT']) && $_SESSION['BUHALT']);
    }
    if(!defined('AUKLETOJA')) {
    	define("AUKLETOJA", isset($_SESSION['AUKLETOJA']) && $_SESSION['AUKLETOJA']);
    }
    if(!defined('MUZIKOS_PEDAGOGAS')) {
    	define("MUZIKOS_PEDAGOGAS", isset($_SESSION['MUZIKOS_PEDAGOGAS']) && $_SESSION['MUZIKOS_PEDAGOGAS']);
    }
    if(!defined('SESELE')) {
    	define("SESELE", isset($_SESSION['SESELE']) && $_SESSION['SESELE']);
    }
    if(!defined('LOGOPEDAS')) {
    	define("LOGOPEDAS", isset($_SESSION['LOGOPEDAS']) && $_SESSION['LOGOPEDAS']);
    }
    if(!defined('MENO_PEDAGOGAS')) {
    	define("MENO_PEDAGOGAS", isset($_SESSION['MENO_PEDAGOGAS']) && $_SESSION['MENO_PEDAGOGAS']);
    }
	if(!defined('KUNO_KULTUROS_PEDAGOGAS')) {
		define("KUNO_KULTUROS_PEDAGOGAS", isset($_SESSION['KUNO_KULTUROS_PEDAGOGAS']) && $_SESSION['KUNO_KULTUROS_PEDAGOGAS']);
	}
	if(!defined('DIETISTAS')) {
		define("DIETISTAS", isset($_SESSION['DIETISTAS']) && $_SESSION['DIETISTAS']);
	}
	if(!defined('SOCIALINIS_PEDAGOGAS')) {
		define("SOCIALINIS_PEDAGOGAS", isset($_SESSION['SOCIALINIS_PEDAGOGAS']) && $_SESSION['SOCIALINIS_PEDAGOGAS']);
	}
	if(!defined('KINEZITERAPEUTAS')) {
		define("KINEZITERAPEUTAS", isset($_SESSION['KINEZITERAPEUTAS']) && $_SESSION['KINEZITERAPEUTAS']);
	}
	
	if(isset($_POST['change_child_id']) && isset($_SESSION['children'][$_POST['change_child_id']]))
		$_SESSION['CHILD_ID'] = $_POST['change_child_id'];
	
	if(!defined('CHILD_ID')) {
		if(isset($_SESSION['CHILD_ID']) && $_SESSION['CHILD_ID']) define("CHILD_ID", (int)$_SESSION['CHILD_ID']);
		else define("CHILD_ID", 0);
	}
	if(isset($_GET['change_group_id']) && isset($_SESSION['GROUPS'][$_GET['change_group_id']]))
		$_SESSION['GROUP_ID'] = (int)$_GET['change_group_id'];
	if(!defined('GROUP_ID')) {
		if(isset($_SESSION['GROUP_ID']) && $_SESSION['GROUP_ID']) define("GROUP_ID", (int)$_SESSION['GROUP_ID']);
		else define("GROUP_ID", 0);
	}
	
	if(!defined('USER_ID')) {
		if(isset($_SESSION['USER_ID']) && $_SESSION['USER_ID']) define("USER_ID", (int)$_SESSION['USER_ID']);
		else define("USER_ID", 0);
	}
	if(!defined('DARB_ID')) {
		if(isset($_SESSION['DARB_ID']) && $_SESSION['DARB_ID']) define("DARB_ID", (int)$_SESSION['DARB_ID']);
		else define("DARB_ID", 0);
	}
	if(!defined('DB_PREFIX')) {
		if(isset($_SESSION['DB_PREFIX']) && $_SESSION['DB_PREFIX']) define("DB_PREFIX", $_SESSION['DB_PREFIX']);// else define("DB_PREFIX", '');//TODO: Review comment
	}

if($_GET['q'] == 'pakeisti_slaptazodi') {
	$change_password_state = 0;
	if(isset($_POST['token'])) {
		if($_POST['token'] == $_SESSION['token']) {
			if($_POST['new_pass'] == $_POST['new_pass2']) {
				$r = db_query("SELECT * 
					FROM `".DB_PREFIX."users` JOIN `".DB_PREFIX."users_allowed` ON `".DB_PREFIX."users_allowed`.`user_id`=`".DB_PREFIX."users`.`user_id`
					WHERE `email`='".mysqli_real_escape_string($db_link, $_SESSION['USER_DATA']['email'])."' AND `password`='".passwd_hash($_POST['old_pass'])."'");
				if(mysqli_num_rows($r)) {
					//$groups = mysqli_fetch_assoc($result);
					$r = db_query("UPDATE `".DB_PREFIX."users` SET `password`='".passwd_hash($_POST['new_pass'])."'
					WHERE `email`='".mysqli_real_escape_string($db_link, $_SESSION['USER_DATA']['email'])."' AND `password`='".passwd_hash($_POST['old_pass'])."'");
					$change_password_state = 1;
				} else {
					//logdie('Slaptažodžio pakeitimas negalimas.');
					$change_password_state = -3;
				}
			} else
				$change_password_state = -1;
		} else
			$change_password_state = -2;
	}
	$_SESSION['token'] = md5(uniqid(rand(), true));
}

//DARBUOT ????
