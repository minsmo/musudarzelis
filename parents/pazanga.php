<?php if(!defined('USER')) exit(); ?>
<h1>Vaiko pažanga ir pasiekimai</h1>
<div id="content">
	<form method="get" action="">
		Nuo:
		<div class="sel"><select name="nuo">
		<?php
		$result = db_query("SELECT * FROM `".DB_achievements."` WHERE `vaiko_id`=".CHILD_ID." GROUP BY `data` ORDER BY `data` ASC");
		while ($row = mysqli_fetch_assoc($result))
		    echo '<option value="'.$row['data'].'"'.(isset($_GET['nuo']) && $row['data'] == $_GET['nuo'] ? ' selected="selected"' : '').'>'.$row['data'].'</option>';
		?>
		</select></div>
		Iki: 
		<div class="sel"><select name="iki">
		<?php
		$result = db_query("SELECT * FROM `".DB_achievements."` WHERE `vaiko_id`=".CHILD_ID." GROUP BY `data` ORDER BY `data` DESC"); 
		while ($row = mysqli_fetch_assoc($result))
		    echo '<option value="'.$row['data'].'"'.(isset($_GET['iki']) && $row['data'] == $_GET['iki'] ? ' selected="selected"' : '').'>'.$row['data'].'</option>';
		?>
		</select></div>
		<input type="submit" value="Filtruoti" class="filter">
	</form>

	<?php
	$search = '';
	$limit = ' LIMIT 30';
	if(isset($_GET['nuo'])) {
		$search .= ' AND `data`>=\''.db_fix($_GET['nuo']).'\'';
		$limit = '';
	}
	if(isset($_GET['iki'])) {
		$search .= ' AND `data`<=\''.db_fix($_GET['iki']).'\'';
		$limit = '';
	}
	
	if(!empty($limit))
		echo 'Rodoma 30 naujausių:';
	?>
	<table>
	<tr>
		<th class="date-cell">Diena</th>
		<th>Pastabos</th>
		<th>Pasiekimai</th>
		<th>Sukūrė</th>
		<th>Prisegtukas</th>
	</tr>
	<?php
	$result = db_query("SELECT * FROM `".DB_achievements."` JOIN `".DB_achievements_access."` ON `".DB_achievements."`.`ID`=`".DB_achievements_access."`.`achievement_id` AND `allowed_for_person_type`=0 -- Tėvams
	WHERE `".DB_achievements."`.`vaiko_id`=".CHILD_ID." $search
	ORDER BY `data` DESC".$limit);
	$sum = 0;
	if(mysqli_num_rows($result) > 0) {
		while ($row = mysqli_fetch_assoc($result)) {
			/*$result3 = db_query("SELECT * FROM `".DB_achievements."` WHERE `vaiko_id`=".CHILD_ID." AND `type`=1 AND `data`='".$row['data']."'");
		    $row3['value'] = '';
		    if(mysqli_num_rows($result3) > 0)
		        $row3 = mysqli_fetch_assoc($result3);
		            */
			echo "\t<tr>
			<td>".$row['data']."</td>
			<td>".filterText($row['value'])."</td>
			<td>".filterText($row['achievement'])."</td>
			<td>".getAllEmployees($row['createdByEmployeeId'])."</td>
			<td>".(!empty($row['attachment']) ? '<a href="'.UPLOAD_DIR.'progress_and_achievements/'.$row['vaiko_id']."/".$row['attachment'].'">'.$row['attachment'].'</a>' : '')."</td>";
			/*echo '<td>';
			//$result2 = mysqli_query($db_link, "SELECT * FROM `".DB_achievements."` WHERE `vaiko_id`=".(int)$row['vaiko_id']." AND `veikimo_d_id`=".(int)$row['veikimo_d_id']);
			//while ($row2 = mysqli_fetch_assoc($result2))
			//	echo $child_achievements[$row2['tipas']]. ": ".filterText($row2['pavadinimas']).";<br>";
			$result2 = db_query("SELECT * FROM `".DB_achievements."` WHERE `vaiko_id`=".CHILD_ID." AND `type`=0 AND `data`='".$row['data']."'");
		    while ($row2 = mysqli_fetch_assoc($result2))
		    	echo filterText($row2['value']).";<br>";//$child_achievements[$row2['achievementType']]. ": ".

			echo "</td>
			";*/
			echo "\n\t</tr>";
		}
	} else echo "<tr><td colspan=\"5\"><strong>Tuščia</span></td></tr>";
?>
</table>
</div>
