<?php if(!defined('DARBUOT') && !DARBUOT) exit(); ?>
<h1>Virtuali biblioteka</h1>
<div id="content">
<?php
$debt = [];
$result = db_query("SELECT * FROM `3libraryObject_debt` WHERE `kindergarten_id`=".DB_ID." AND `returnDate` IS NULL");
while($row = mysqli_fetch_assoc($result))
	$debt[$row['libraryObjectId']] = $row;

	$result = db_query("SELECT * FROM `3libraryObject` WHERE `kindergarten_id`=".DB_ID." AND `onlyForWorkers`=0 ORDER BY `libraryObjectType`, `title`");
	if(mysqli_num_rows($result) > 0) {
		?>
		<h2>Turtas</h2>
		<table>
		<tr>
			<th>Pavadinimas</th>
			<th class="date-cell">Leidimo<br>metai</th>
			<th>ISBN</th>
			<th>Trumpas aprašas</th>
			<th>Kur saugoma</th>
			<th>Rūšis</th>
			<th class="no-print">Paskolinimas</th>
		</tr>
		<?php
		while($row = mysqli_fetch_assoc($result)) {
			echo "<tr><td>".filterText($row['title'])."</td><td>".filterText($row['publication_year'])."</td><td>".filterText($row['ISBN'])."</td><td>".nl2br(filterText($row['description']))."</td><td>".filterText($row['place'])."</td><td>".$library_objects_types[$row['libraryObjectType']]."</td><td class=\"no-print\">";
			if(isset($debt[$row['ID']])) {
				echo ' Paskolinta iki '.$debt[$row['ID']]['debtTo'];
			} else
				echo "Galima pasiimti";
			echo "</td></tr>";
		}
		echo '</table>';
	}
?>
</div>
