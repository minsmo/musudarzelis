<?php if(!defined('USER')) exit; ?>
<h1>Susirašinėjimas</h1>
<div id="content">
<?php
if(isset($_POST['message']) && !empty($_POST['message'])) {
	$attachment_names = [];
	$photo_new_names = [];
	$ok = true;
	if(isset($_FILES['attachment'])) {
		$ok = false;
		for($i = 0; $i < count($_FILES['attachment']['tmp_name']); ++$i) {
			if( isset($_FILES['attachment']['tmp_name'][$i]) && is_uploaded_file($_FILES['attachment']['tmp_name'][$i]) ) {
				$add_attachment = true;
				$attachment_name = date('Y-m-d_H.i').'_'.$_FILES['attachment']['name'][$i];
				//if(verify_image($_FILES['attachment']['tmp_name'])) {
					$photo_ext = strrchr($_FILES['attachment']['name'][$i], ".");

					if($_FILES['attachment']['size'][$i] >= 5*1024*1024) {
						echo "<div class=\"red center\">Klaida: failas gali užimti iki 5 MB</div>";
					} elseif(!in_array(strtolower($photo_ext), $file_type)) {
						echo "<div class=\"red center\">Klaida: Failo tipas privalo būti iš šių failų tipų: ".implode(', ', $file_type)."</div>";
					} else {
						if(!is_dir(UPLOAD_DIR.'messages'))
						    if(!mkdir(UPLOAD_DIR.'messages', 0777))
						        echo "<div class=\"red center\">Nepavyko sukurti katalogo</div>";

						$photo_new_name = UPLOAD_DIR.'messages/'.$attachment_name;
						$photo_new_names[] = $photo_new_name;
						if(move_uploaded_file($_FILES['attachment']['tmp_name'][$i], $photo_new_name)) {
							echo "<div class=\"green center\">Failas išsaugotas.</div>";
							$ok = true;
							$attachment_names[] = $attachment_name;
						} else
							echo "<div class=\"red center\">Failo išsaugoti nepavyko.</div>";
						//chmod($photo_new_name, 0644);
					}
				//} else echo "<div class=\"red center\">Failas nėra paveiksliukas.</div>";
			} else {
				$ok = true;
			}
		}
	}
	
	list($type, $ID) = explode('.', $_GET['to']);
	if($type == 'd')
		$to_result = mysqli_query($db_link, "SELECT `".DB_users_allowed."`.* 
			FROM `".DB_users_allowed."` JOIN `".DB_employees."` ON `".DB_users_allowed."`.`person_id`=`".DB_employees."`.`ID`
			WHERE `".DB_users_allowed."`.`person_type`>0 AND `".DB_employees."`.`isDeleted`=0 AND `".DB_employees."`.`ID`=".(int)$ID) or logdie('aNeteisinga užklausa: '.mysqli_error($db_link));
	else
		//"SELECT `".DB_users_allowed."`.* 
		//	FROM `".DB_users_allowed."` JOIN `".DB_children."` ON `".DB_users_allowed."`.`person_id`=`".DB_children."`.`ID`
		//	WHERE `".DB_users_allowed."`.`person_type`=0 AND `".DB_children."`.`ID`=".(int)$ID
		$to_result = db_query("SELECT `".DB_users_allowed."`.* 
			FROM `".DB_users_allowed."` JOIN `".DB_children."` ON `".DB_users_allowed."`.`person_id`=`".DB_children."`.`parent_kid_id`
			WHERE `".DB_users_allowed."`.`person_type`=0 AND `".DB_children."`.`parent_kid_id`=".(int)$ID." AND `".DB_children."`.`isDeleted`=0 AND `".DB_children."`.`valid_from`<=CURDATE()
			ORDER BY `valid_from` DESC LIMIT 1", 'bNeteisinga užklausa: ');
	
	$from_result = db_query("SELECT * FROM `".DB_users_allowed."` WHERE `person_type`=0 AND `user_id`=".(int)USER_ID, 'cNeteisinga užklausa: ');
	if(mysqli_num_rows($to_result) > 0) {
		$to = mysqli_fetch_assoc($to_result);
		$from = mysqli_fetch_assoc($from_result);
		if(!mysqli_query($db_link, "INSERT INTO `".DB_messages."` SET 
		fromUserId=".USER_ID.", fromPersonType=".$from['person_type'].", fromPersonId=".$from['person_id'].", 
		toUserId=".$to['user_id'].", toPersonType=".$to['person_type'].", toPersonId=".$to['person_id'].", 
		message='".db_fix($_POST['message'])."', `attachments`='".(empty($attachment_names) ? '' : db_fix(serialize($attachment_names)))."'")) {
			print_r($from);
			//logdie('dNeteisinga užklausa: ' . mysqli_error($db_link).print_r($from, true));
		} else {
			msgBox('OK', 'Žinutė sėkmingai išsiųsta.');
		}
	}
}
?>
<div id="latest-new-msg">
	<?php
	/*$d_result = mysqli_query($db_link, "SELECT *, `".DB_messages."`.`created`, MIN(`".DB_messages."`.`read`) AS `read`
			FROM `".DB_messages."` LEFT JOIN `".DB_users."` ON `fromUserId`=`user_id`
			WHERE `fromPersonType`>0 AND `toPersonId`=".CHILD_ID." AND `toPersonType`=0 
			GROUP BY fromPersonId
			ORDER BY `".DB_messages."`.`created` DESC
			LIMIT 4") or logdie('Neteisinga užklausa: '.mysqli_error($db_link));
	$v_result = mysqli_query($db_link, "SELECT *, `".DB_messages."`.`created`, MIN(`".DB_messages."`.`read`) AS `read`
			FROM `".DB_messages."` LEFT JOIN `".DB_users."` ON `fromUserId`=`user_id`
			WHERE `fromPersonType`=0 AND `toPersonId`=".CHILD_ID." AND `toPersonType`=0 
			GROUP BY fromPersonId
			ORDER BY `".DB_messages."`.`created` DESC
			LIMIT 4") or logdie('Neteisinga užklausa: '.mysqli_error($db_link));*/
	$d_result = mysqli_query($db_link, "SELECT *, MAX(`".DB_messages."`.`created`) AS `created`, MIN(`".DB_messages."`.`read`) AS `read`
			FROM `".DB_messages."` LEFT JOIN `".DB_users."` ON `fromUserId`=`user_id`
			WHERE `fromPersonType`>0 AND `toPersonId`=".CHILD_ID." AND `toPersonType`=0 
			GROUP BY `fromPersonId`
			ORDER BY MAX(`".DB_messages."`.`created`) DESC
			LIMIT 5") or logdie('Neteisinga užklausa: '.mysqli_error($db_link));
	$v_result = mysqli_query($db_link, "SELECT *, MAX(`".DB_messages."`.`created`) AS `created`, MIN(`".DB_messages."`.`read`) AS `read`
			FROM `".DB_messages."` LEFT JOIN `".DB_users."` ON `fromUserId`=`user_id`
			WHERE `fromPersonType`=0 AND `toPersonId`=".CHILD_ID." AND `toPersonType`=0 
			GROUP BY fromPersonId
			ORDER BY MAX(`".DB_messages."`.`created`) DESC
			LIMIT 5") or logdie('Neteisinga užklausa: '.mysqli_error($db_link));
	if(mysqli_num_rows($d_result)) {
		echo '<div>5 naujausi susirašinėjimai su darbuotojais:</div>';
		while($row = mysqli_fetch_assoc($d_result))
			echo "<div".(!$row['read'] ? ' class="msg-unread"' : '' )."><a href=\"/zinutes?to=d.${row['fromPersonId']}\">${row['created']} ".filterText($row['name'])." ".filterText($row['surname'])."</a></div>";
	}
	if(mysqli_num_rows($v_result)) {
		echo '<div>5 naujausi susirašinėjimai su vaikų tėvais:</div>';
		while($row = mysqli_fetch_assoc($v_result))
			echo "<div".(!$row['read'] ? ' class="msg-unread"' : '' )."><a href=\"/zinutes?to=v.${row['fromPersonId']}\">${row['created']} ".filterText($row['name'])." ".filterText($row['surname'])."</a></div>";
	}
	?>
	<hr>
	Susirašinėti su:
	<form method="get" action="/zinutes" style="display: inline;">
	<div class="sel"><select name="to" onchange="this.form.submit()">
	<option value="">Pasirinkite žmogų</option>
	<optgroup label="Darbuotojai">
	<?php
	
	$result = db_query("SELECT `".DB_employees."`.* 
		FROM `".DB_users_allowed."` 
		JOIN `".DB_employees."` ON `".DB_users_allowed."`.`person_id`=`".DB_employees."`.`ID`
		LEFT JOIN `".DB_employees_groups."` ON `".DB_employees."`.`ID`=`".DB_employees_groups."`.`darb_id`
		WHERE `".DB_users_allowed."`.`person_type`>0 AND 
		(`".DB_users_allowed."`.`person_type` IN (".implode(',', array_keys($all_groups_for_person_type)).") OR `".DB_employees_groups."`.`grup_id`=".(int)$vaik['grupes_id'].")
		GROUP BY `".DB_employees."`.`ID`
		ORDER BY vardas, pavarde");//show only 3 employees and adm
		//`".DB_employees_groups."`.`grup_id` IS NOT NULL AND 
		//GROUP BY `".DB_employees."`.`ID`
	while ($row = mysqli_fetch_assoc($result)) {
		echo "<option value=\"d.".$row['ID']."\"".($row['isDeleted'] ? ' style="color: grey;"' : '').(isset($_GET['to']) && 'd.'.$row['ID'] == $_GET['to'] ? ' selected="selected"' : '').">".filterText($row['vardas']." ".$row['pavarde']).($row['isDeleted'] ? ' (ištrintas)' : '')."</option>";
		if(isset($_GET['to']) && 'd.'.$row['ID'] == $_GET['to']) {
			$kam = filterText($row['vardas']." ".$row['pavarde']);
			$isDeleted = $row['isDeleted'];
		}
	}
	?>
	</optgroup>
	<optgroup label="Vaikų tėvai">
	<?php
	//$_SESSION['children'][CHILD_ID]['grupes_id'] arba $vaik['grupes_id']
	$result = db_query("SELECT cr.* 
		FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<=CURDATE() GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
		JOIN `".DB_users_allowed."` ON cr.`parent_kid_id`=`".DB_users_allowed."`.`person_id`
		WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 AND `".DB_users_allowed."`.`person_type`=0 AND cr.`grupes_id`=".(int)$vaik['grupes_id']."
		AND cr.`parent_kid_id`<>".CHILD_ID."
		GROUP BY cr.`parent_kid_id`
		ORDER BY cr.vardas, cr.pavarde");
	/*OLD: $result = db_query("SELECT `".DB_children."`.* 
		FROM `".DB_users_allowed."` JOIN `".DB_children."` ON `".DB_users_allowed."`.`person_id`=`".DB_children."`.`ID`
		WHERE `".DB_users_allowed."`.`person_type`=0 AND `".DB_children."`.`grupes_id`=".(int)$vaik['grupes_id']." AND `".DB_children."`.`ID`<>".CHILD_ID."
		GROUP BY `".DB_children."`.`ID`
		ORDER BY vardas, pavarde");*/
	while($row = mysqli_fetch_assoc($result)) {
		echo "<option value=\"v.".$row['parent_kid_id']."\"".(isset($_GET['to']) && 'v.'.$row['parent_kid_id'] == $_GET['to'] ? ' selected="selected"' : '').">".filterText($row['vardas']." ".$row['pavarde'])."</option>";
		if(isset($_GET['to']) && 'v.'.$row['parent_kid_id'] == $_GET['to']) {
			$kam = filterText($row['vardas']." ".$row['pavarde']);
			$isDeleted = false;
		}
	}
	?>
	</optgroup>
	</select></div>
	</form>
</div>

<?php /* if(isset($_GET['to']) && !empty($_GET['to'])) { ?>
<div style="float: left;">
	<!--<fieldset style="width: 610px;">
	<legend>Naujas žinutė</legend>-->
	<form action="/zinutes?to=<?=$_GET['to']?>" method="post">
		<div>Kam: <?=(isset($kam) ? $kam : '')?></div>
		<div><textarea name="message" style="width: 600px; height: 80px;"></textarea></div>
		<div><input type="submit" value="Išsiųsti"></div>
	</form>
	<!-- </fieldset>-->
	<?php
	list($type, $ID) = explode('.', $_GET['to']);
	$ID = (int)$ID;
	if($type == 'd')
		$result = mysqli_query($db_link, "SELECT *, `".DB_messages."`.`created` FROM `".DB_messages."` LEFT JOIN `".DB_users."` ON `fromUserId`=`user_id`
			WHERE (`fromPersonId`=".CHILD_ID." AND `fromPersonType`=0 AND `toPersonId`=$ID AND `toPersonType`>0) 
			   OR (`fromPersonId`=$ID AND `fromPersonType`>0 AND `toPersonId`=".CHILD_ID." AND `toPersonType`=0) ORDER BY `".DB_messages."`.`created` DESC") or logdie('Neteisinga užklausa: '.mysqli_error($db_link));
	else
		$result = mysqli_query($db_link, "SELECT *, `".DB_messages."`.`created` FROM `".DB_messages."` LEFT JOIN `".DB_users."` ON `fromUserId`=`user_id`
			WHERE (`fromPersonId`=".CHILD_ID." AND `fromPersonType`=0 AND `toPersonId`=$ID AND `toPersonType`=0) 
			   OR (`fromPersonId`=$ID AND `fromPersonType`=0 AND `toPersonId`=".CHILD_ID." AND `toPersonType`=0) ORDER BY `".DB_messages."`.`created` DESC") or logdie('Neteisinga užklausa: '.mysqli_error($db_link));
	while($row = mysqli_fetch_assoc($result)) {
		if($row['fromPersonId'] == CHILD_ID && $row['fromPersonType']>0)
			echo "<div class=\"msg msg-myself\"><span class=\"msg-created\">${row['created']}</span>: ".nl2br(filterText($row['message']))."</div>";
		else
			echo "<div class=\"msg".(!$row['read'] ? ' msg-unread' : '' )."\"><span class=\"msg-created\">${row['created']}</span> <span class=\"msg-from\">${row['name']} ${row['surname']}</span>: ".nl2br(filterText($row['message']))."</div>";
	}
	if($type == 'd')
		$result = mysqli_query($db_link, "UPDATE `".DB_messages."` SET `read`=`read`+1
			WHERE `fromPersonId`=$ID AND `fromPersonType`>0 AND `toPersonId`=".CHILD_ID." AND `toPersonType`=0") or logdie('Neteisinga užklausa: '.mysqli_error($db_link));// ORDER BY `created` DESC LIMIT 
	else
		$result = mysqli_query($db_link, "UPDATE `".DB_messages."` SET `read`=`read`+1
			WHERE `fromPersonId`=$ID AND `fromPersonType`=0 AND `toPersonId`=".CHILD_ID." AND `toPersonType`=0") or logdie('Neteisinga užklausa: '.mysqli_error($db_link));// ORDER BY `created` DESC LIMIT 
	?>
</div>
<?php }*/

if(isset($_GET['to']) && !empty($_GET['to'])) { ?>
<div style="float: left; width: 440px;">
	<!--<fieldset style="width: 610px;">
	<legend>Naujas žinutė</legend>-->
	<?php if(isset($isDeleted) && !$isDeleted) { ?>
	<form enctype="multipart/form-data" action="/zinutes?to=<?=$_GET['to']?>" method="post">
		<div>Kam: <?=(isset($kam) ? $kam : '')?></div>
		<div><textarea name="message" style="width: 440px; height: 80px;"></textarea></div>
		<p><label>Prisegtukas: <input type="file" name="attachment[]" id="file" style="width: 330px;" multiple></label></p>
		<div><input type="submit" value="Išsiųsti" class="submit"></div>
	</form>
	<?php } ?>
	<!-- </fieldset>-->
	<?php
	list($type, $ID) = explode('.', $_GET['to']);
	$ID = (int)$ID;
	if($type == 'd')
		$result = mysqli_query($db_link, "SELECT *, `".DB_messages."`.`created` FROM `".DB_messages."` LEFT JOIN `".DB_users."` ON `fromUserId`=`user_id`
			WHERE (`fromPersonId`=".CHILD_ID." AND `fromPersonType`=0 AND `toPersonId`=$ID AND `toPersonType`>0) 
				OR (`fromPersonId`=$ID AND `fromPersonType`>0 AND `toPersonId`=".CHILD_ID." AND `toPersonType`=0) ORDER BY `".DB_messages."`.`created` DESC") or logdie('Neteisinga užklausa: '.mysqli_error($db_link));
	else
		$result = mysqli_query($db_link, "SELECT *, `".DB_messages."`.`created` FROM `".DB_messages."` LEFT JOIN `".DB_users."` ON `fromUserId`=`user_id`
			WHERE (`fromPersonId`=".CHILD_ID." AND `fromPersonType`=0 AND `toPersonId`=$ID AND `toPersonType`=0) 
				OR (`fromPersonId`=$ID AND `fromPersonType`=0 AND `toPersonId`=".CHILD_ID." AND `toPersonType`=0) ORDER BY `".DB_messages."`.`created` DESC") or logdie('Neteisinga užklausa: '.mysqli_error($db_link));
	while($row = mysqli_fetch_assoc($result)) {
		//$_SESSION['USER_DATA']
		if($row['fromPersonId'] == CHILD_ID && $row['fromPersonType'] == 0) {
			echo "<div class=\"msg msg-myself\"><span class=\"msg-created\">${row['created']}</span>".
				($row['firstRead'] ? ' (<span title="atidarė žinutę">atidarė</span> '.$row['firstRead'].')' : '').": ".nl2br(filterText($row['message']));
			if( !empty($row['attachments']) ) {
				$row['attachments'] = unserialize($row['attachments']);
				foreach($row['attachments'] as $attachment)
					echo '<div style="padding-left: 20px;">Prisegtukas: <a href="'.UPLOAD_DIR.'messages/'.filterText(rawurlencode($attachment)).'">'.filterText($attachment).'</a></div>';
			}
			echo "</div>";
		} else {
			echo "<div class=\"msg".(!$row['read'] ? ' msg-unread' : '' )."\"><span class=\"msg-created\" title=\"Išsiuntė\">${row['created']}</span> <span class=\"msg-from\">".filterText($row['name'])." ".filterText($row['surname'])."</span>: ".nl2br(filterText($row['message']));
			if( !empty($row['attachments']) ) {
				$row['attachments'] = unserialize($row['attachments']);
				foreach($row['attachments'] as $attachment)
					echo '<div style="padding-left: 20px;">Prisegtukas: <a href="'.UPLOAD_DIR.'messages/'.filterText(rawurlencode($attachment)).'">'.filterText($attachment).'</a></div>';
			}
			echo "</div>";
		}
	}
	if($type == 'd') {
		$result = db_query("UPDATE `".DB_messages."` SET `firstRead`=CURRENT_TIMESTAMP()
			WHERE `fromPersonId`=$ID AND `fromPersonType`>0 AND `toPersonId`=".CHILD_ID." AND `toPersonType`=0 AND `firstRead` IS NULL");
		$result = db_query("UPDATE `".DB_messages."` SET `read`=`read`+1
			WHERE `fromPersonId`=$ID AND `fromPersonType`>0 AND `toPersonId`=".CHILD_ID." AND `toPersonType`=0");// ORDER BY `created` DESC LIMIT 
	} else {
		$result = db_query("UPDATE `".DB_messages."` SET `firstRead`=CURRENT_TIMESTAMP()
			WHERE `fromPersonId`=$ID AND `fromPersonType`=0 AND `toPersonId`=".CHILD_ID." AND `toPersonType`=0 AND `firstRead` IS NULL");
		$result = db_query("UPDATE `".DB_messages."` SET `read`=`read`+1
			WHERE `fromPersonId`=$ID AND `fromPersonType`=0 AND `toPersonId`=".CHILD_ID." AND `toPersonType`=0");// ORDER BY `created` DESC LIMIT
	}
	?>
	<script>
    function checkUpload() {
    	var totalSize = 0;
    	var files = document.getElementById('file').files;
    	for(var i = 0; i < files.length; ++i) {
			var file = files[i];
			totalSize += file.size;
			if(!file || file.size < 2*1024*1024) {
				//Submit form        
			} else {
				alert('Failas pavadinimu „'+file.name+'“ didesnis negu 2 MB (užima net '+(file.size/1024.0/1024.0).toFixed(2)+' MB). Jo įkelti neleidžiama dėl Jūsų pačių saugumo, nes tėvams gali užimti per daug laiko jį atidaryti, tėvai gali negauti dėl tėvų el. pašto dėžučių apribojimų.\n\nJei tai paveiksliukas išsaugokite jį jpg formatu arba sumažinkite jo raišką, kad jo užimamas dydis sumažėtų. Tada bandykite dar kartą jau su mažesniu failu.');
				//Prevent default and display error
				//evt.preventDefault();
				return false;
			}
		}
		if(totalSize > 15*1024*1024) {
			alert('Prisegami failai bendrai užima daugiau negu 15 MB (užima net '+(totalSize/1024.0/1024.0).toFixed(2)+' MB). Neleidžiama jų išsiųsti dėl Jūsų pačių saugumo, nes tėvai gali negauti jų dėl tėvų el. pašto dėžučių apribojimų, netgi su plačiai naudojamomis el. pašto dėžutėmis tokiomis kaip Gmail, Yahoo, Outlook, nes jos leidžia iki 10–25 MB (be to tėvams gali užimti per daug laiko juos atidaryti).\n\nJei tai paveiksliukai išsaugokite juos jpg formatu arba sumažinkite jų raišką, kad užimamas dydis sumažėtų. Tada bandykite dar kartą jau su mažesniais failais.');
				//Prevent default and display error
				//evt.preventDefault();
				return false;
		}
    }
    </script>
</div>
<?php
}
?>
</div>
