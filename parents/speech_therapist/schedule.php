<?php if(!defined('USER')) exit; ?>
<h1><span class="no-print">Logopedo, specialiojo pedagogo, tiflopedagogo, surdopedagogo </span>PRATYBŲ, SPECIALIŲJŲ PAMOKŲ TVARKARAŠTIS</h1>
<div id="content">

<table class="vertical-hover">
<tr>
	<th>Laikas</th>
	<th>Diena</th>
	<th>Grupė</th>
	<th class="no-print">Grupės vaikai (tik Jūsų žiūrimo vaiko)</th>
</tr>
<?php
/*$kids_of_schedule = [];
$result = db_query("SELECT `".DB_bureliai_schedule_kids."`.*, cr.*
	FROM `".DB_bureliai_schedule_kids."`
	JOIN `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<=CURDATE() GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from` AND cr.`parent_kid_id`=`".DB_bureliai_schedule_kids."`.`kid_id`
	WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 ORDER BY ".orderName('cr'));

while($row = mysqli_fetch_assoc($result)) {
	$kids_of_schedule[$row['bureliai_schedule_id']][] = array(
		'kid_id' => $row['kid_id'],
		'name' => filterText(getName($row['vardas'], $row['pavarde'])),
	);
}*/
$result = db_query("SELECT `".DB_speech_groups."`.`title`, `".DB_speech_schedule."`.*
	FROM `".DB_speech_schedule."` 
	JOIN `".DB_speech_groups."` ON `".DB_speech_schedule."`.`speech_group_id`=`".DB_speech_groups."`.`ID`
	JOIN `".DB_speech_groups_kids."` ON `".DB_speech_groups_kids."`.`group_id`=`".DB_speech_groups."`.`ID` AND `".DB_speech_groups_kids."`.`kid_id`=".CHILD_ID."
	ORDER BY `day`, `time`, `title`");
	//`".DB_speech_groups."`.`pavadinimas`
while($row = mysqli_fetch_assoc($result)) { 
?>
<tr>
	<td><?=filterText($row['time'])?></td>
	<td><?=$dienos_kada[$row['day']]?></td>
	<td><?=filterText($row['title'])?></td>
	<td class="no-print">
		<?php
		//TODO: finish (get valid kid data)
		$result_d = db_query("SELECT * FROM `".DB_children."` WHERE `".DB_children."`.`parent_kid_id`=".CHILD_ID." GROUP BY `parent_kid_id`");//TODO: in the future maybe someone need to add the same kid twice then will be a need for GROUP BY `kid_id`
		if(mysqli_num_rows($result_d) > 0) {
			$d = mysqli_fetch_assoc($result_d);
			echo filterText($d['vardas']).' '.filterText($d['pavarde']);
		} else {
			//echo 'Dėmesio! Norint, kad auklėtojos ir muzikos pedagogės galėtų dirbti<!-- prisijungti ir prisijungusios dirbti --> su šia grupe privaloma priskirti norimus darbuotojus prie šios grupės. Tai padaryti pirmiausia spauskite ant nuorodos, esančios dešiniau, pavadinimu „Keisti darbuotojų priskyrimus“.';
		}
		?></td>
</tr>
<?php
}

?>
</table>
</div>
