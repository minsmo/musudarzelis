<?php if(!defined('USER')) exit; ?>
<h1>Valgiaraštis</h1>
<div id="content">
<?php
//Moved upper to get date to auto check <select>
$result = db_query("SELECT * FROM `".DB_food_menu."` WHERE `kindergarten_id`=".DB_ID
	.(isset($_GET['date']) ? " AND `date`>='".db_fix($_GET['date'])."' ORDER BY `date` ASC" : ' ORDER BY `date` DESC')
	." LIMIT 0,5");
$breakfast = false;
$brunch = false;
$lunch = false;
$afternoon_tea = false;
$dinner = false;
$rows = array();
while($row = mysqli_fetch_assoc($result)) {
	if(!empty($row['breakfast'])) $breakfast = true;
	if(!empty($row['brunch'])) $brunch = true;
	if(!empty($row['lunch'])) $lunch = true;
	if(!empty($row['afternoon_tea'])) $afternoon_tea = true;
	if(!empty($row['dinner'])) $dinner = true;
	$rows[] = $row;
}
if(!isset($_GET['date']))
	$rows = array_reverse($rows);

?>
<div class="no-print">Rodoma 5-ioms dienoms.</div>
<form method="get">
	<div style="float: left; margin-right: 5px; padding: 4px 0px;">Rodyti nuo </div><div class="sel" style="float: left; margin-right: 7px;"><select name="date">
	<?php
	$result = db_query("SELECT `date` FROM `".DB_food_menu."` WHERE `kindergarten_id`=".DB_ID." ORDER BY `date` DESC");
	while($row = mysqli_fetch_assoc($result)) {
		echo "<option value=\"".$row['date']."\"".(isset($_GET['date']) && $_GET['date'] == $row['date'] || $rows[0]['date'] == $row['date'] ? ' selected="selected" style="font-weight: bold"' : '').">".$row['date']."</option>";
	}
	?>
	</select></div>
	<input type="submit" class="filter" value="Filtruoti">
</form>
<?php
echo '<table>';
echo '<tr>';
foreach($rows as $row) 
	echo '<th class="center" style="text-transform: uppercase;">'.$row['date'].'</th>';
echo '</tr>';
if($breakfast) {
	echo '<tr>';
	foreach($rows as $row) 
		echo '<th class="center" style="text-transform: uppercase;">Pusryčiai</th>';
	echo '</tr><tr>';
	foreach($rows as $row) 
		echo '<td>'.nl2br(filterText($row['breakfast'])).'</td>';
	echo '</tr>';
}
if($brunch) {
	echo '<tr>';
	foreach($rows as $row) 
		echo '<th class="center" style="text-transform: uppercase;">Priešpiečiai</th>';
	echo '</tr><tr>';
	foreach($rows as $row) 
		echo '<td>'.nl2br(filterText($row['brunch'])).'</td>';
	echo '</tr>';
}
if($lunch) {
	echo '<tr>';
	foreach($rows as $row) 
		echo '<th class="center" style="text-transform: uppercase;">Pietūs</th>';
	echo '</tr><tr>';
	foreach($rows as $row) 
		echo '<td>'.nl2br(filterText($row['lunch'])).'</td>';
	echo '</tr>';
}
if($afternoon_tea) {
	echo '<tr>';
	foreach($rows as $row) 
		echo '<th class="center" style="text-transform: uppercase;">Pavakariai</th>';
	echo '</tr><tr>';
	foreach($rows as $row) 
		echo '<td>'.nl2br(filterText($row['afternoon_tea'])).'</td>';
	echo '</tr>';
}
if($dinner) {
	echo '<tr>';
	foreach($rows as $row) 
		echo '<th class="center" style="text-transform: uppercase;">Vakarienė</th>';
	echo '</tr><tr>';
	foreach($rows as $row) 
		echo '<td>'.nl2br(filterText($row['dinner'])).'</td>';
	echo '</tr>';
}
echo '</table>';
?>
</div>
