<?php if(!defined('USER')) exit; ?>
<h1>Tekstas</h1>
<div id="content">
<?php
$form_title = [0 => 'Be formos'];
$result = db_query("SELECT `form_id`, `title` FROM `".DB_docs_templates."` WHERE `kindergarten_id`=".DB_ID);
while ($row = mysqli_fetch_assoc($result))
	$form_title[$row['form_id']] = $row['title'];

$result = db_query("SELECT *, UNIX_TIMESTAMP(`openedDate`) AS `openedDateTimestamp` FROM `".DB_docs."`
	WHERE `allow_parents`=1 AND (`group_id`=".$vaik['grupes_id']." AND `kid_id`=0 OR `kid_id`=".$vaik['parent_kid_id'].") AND `isDeleted`=0 AND `kindergarten_id`=".DB_ID."
	ORDER BY `".DB_docs."`.`date` DESC");
if(mysqli_num_rows($result) > 0) {
	?>
<table>
<tr>
	<th class="date-cell">Data</th>
	<th>Pavadinimas</th>
	<th>Formos pavadinimas</th>
	<th>Sukūrė</th>
	<th>Atnaujino</th>
	<th>Kita</th>
</tr>
<?php
	while ($row = mysqli_fetch_assoc($result)) {
		if(isset($_GET['view']) && $_GET['view'] == $row['answer_id'])
			$answer = $row;
		echo "		<tr".(isset($_GET['view']) && $_GET['view'] == $row['answer_id'] ? ' class="opened-row"' : '').">
		<td>".$row['date']."</td>
		<td>".filterText($row['title'])."</td>
		<td>".$form_title[$row['form_id']]."</td>
		<td>".substr($row['created'], 0, -3)." ".getAllEmployees($row['createdByEmployeeId'])."</td>
		<td>".substr(date_empty($row['updated']), 0, -3)." ".getAllEmployees($row['updatedByEmployeeId'])."</td>
		<td class=\"no-print\"><a href=\"?view=".$row['answer_id']."#planning-view\">Peržiūrėti</a></td>
	</tr>";	
	}
	echo '</table>';
}

if(isset($_GET['view']) && isset($answer)) {
	?><div id="planning-view">
	<?php if($answer['form_id'] != 0) { ?>
	<div class="no-print">Pildyta pagal formą: <?=$form_title[$answer['form_id']]?></div>
	<?php } ?>
	<?php /*if($answer['group_id'] != 0) { ?>
	<p><span class="title">Grupė:</span> <?=$grupes[$answer['group_id']]?></p>
	<?php }*/ ?>
	<p><span class="title">Pavadinimas:</span> <?=filterText($answer['title'])?></p>
	<p><span class="title">Data:</span> <?=date_empty($answer['date'])?></p>
	<p><span class="title"></span> <?=/*nl2br*/(strip_tags($answer['content'], '<br><p><div><strong><em><table><tr><th><td><span><s><ol><ul><li><h1><h2><h3><del><pre><cite><img><hr><blockquote><tt><code><kbd><samp><var><ins><q><tbody><thead><tfoot>'))?></p>
	<div class="no-print">
			<div style="float: left; width: 70px;">Sukūrė:</div><div style="float: left;"><?=$answer['created']?> <?=getAllEmployees($answer['createdByEmployeeId'])?></div>
			<div style="clear: left; float: left; width: 70px;">Atnaujino:</div><div style="float: left;"><?=date_empty($answer['updated'])?> <?=getAllEmployees($answer['updatedByEmployeeId'])?></div>
			<div style="clear: left;"></div>
			<?php if(KESV) { ?>
			Keitė kartų: <?=$answer['updatedCounter']?>
			<?php } ?>
		</div>
	</div>
	<?php
}
?>
</div>
