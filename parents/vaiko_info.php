<?php if(!defined('USER')) exit; ?>
<h1>Duomenys darželiui</h1>
<div id="content">
<ul>
  <li>Vaiko vardas: <strong><?php echo filterText($vaik['vardas']); ?></strong>;</li>
  <li>Vaiko pavardė: <strong><?php echo filterText($vaik['pavarde']); ?></strong>;</li>
  <li>Mamos vardas: <strong><?php echo filterText($vaik['mamos_vardas']); ?></strong>;</li>
  <li>Mamos pavardė: <strong><?php echo filterText($vaik['mamos_pavarde']); ?></strong>;</li>
  <li>Tėčio vardas: <strong><?php echo filterText($vaik['tevo_vardas']); ?></strong>;</li>
  <li>Tėčio pavardė: <strong><?php echo filterText($vaik['tevo_pavarde']); ?></strong>;</li>
  <li>Lanko grupę: <strong><?php echo filterText($vaik['pavadinimas']); ?></strong>;</li>
  <li>Gimė: <strong><?php echo filterText($vaik['gim_data']); ?></strong>;</li>
  <li>Tėvų adresas: <strong><?php echo filterText($vaik['tevu_adresas']); ?></strong>;</li>
  <li>Mamos tel.: <strong><?php echo nr($vaik['mam_tel']); ?></strong>;</li>
  <li>Tėčio tel.: <strong><?php echo nr($vaik['tecio_tel']); ?></strong>;</li>
  <li>Globėjo tel.: <strong><?php echo nr($vaik['glob_tel']); ?></strong>;</li>
  <li>Namų tel.: <strong><?php echo nr($vaik['namu_tel']); ?></strong>;</li>
  <li>Papildomi telefonai: <strong><?php echo nr($vaik['papildomi_tel']); ?></strong>;</li>
  <li>Susitariama darželio lankymo pradžia: <strong><?php echo date_empty($vaik['lankymo_pradzia']); ?></strong>;</li>
  <li>Susitariama darželio lankymo pabaiga: <strong><?php echo date_empty($vaik['lankymo_pabaiga']); ?></strong>;</li>
  <?php if(!KaunoVaikystesLobiai) { ?>
  <li>Lanko: <strong><?php echo $vaik_darzelio_tipas[$vaik['arDarzelyje']]; ?></strong>;</li>
  <li>Ar pusryčiaus? <strong><?php echo ($vaik['arPusryciaus'] ? 'Taip' : 'Ne'); ?></strong>;</li>
  <li>Ar pietaus? <strong><?php echo ($vaik['arPietaus'] ? 'Taip' : 'Ne'); ?></strong>;</li>
  <li>Ar turės pavakarius? <strong><?php echo ($vaik['arPavakariaus'] ? 'Taip' : 'Ne'); ?></strong>;</li>
  <li>Ar vakarieniaus? <strong><?php echo ($vaik['arVakarieniaus'] ? 'Taip' : 'Ne'); ?></strong>;</li>
  <?php
	$socialinis_remtinumas = array(
		0 => 'Socialiai <strong>neremtina</strong>',
		1 => 'Socialiai <strong>remtina: 50 % nuolaida</strong>',
		3 => 'Socialiai <strong>remtina: 70 % nuolaida</strong>',
		4 => 'Socialiai <strong>remtina: 80 % nuolaida</strong>',
		5 => 'Socialiai <strong>remtina: 90 % nuolaida</strong>',
		2 => 'Socialiai <strong>remtina: 100 % nuolaida</strong>',
	);
  ?>
  <li><?=$socialinis_remtinumas[$vaik['arNuolaida']]?>;</li>
  <?php } ?>
<!--  <li>Ar rodyti kitiems tėvams el. paštą: <strong><?php echo ($vaik['arRodytiElPasta'] ? 'Rodyti' : 'Nerodyti'); ?></strong>;</li> -->
  <li>Ką turėtume žinoti apie vaiką: <strong><?php echo filterText($vaik['turetume_zinoti']); ?></strong>;</li>
  <?php /* <li>Kita informacija: <strong><?php echo filterText($vaik['kita_info']); ?></strong>.</li> */ ?>
</ul>
</div>
