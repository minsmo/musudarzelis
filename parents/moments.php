<?php if(!defined('USER')) exit; ?>
<h1>Vaiko veikla</h1>
<div id="content">
<?php
$width = 150;
$height = 150;
if(!isset($_GET['show'])) { ?>
	<table>
		<tr>
			<th>Data ir pavadinimas</th>
			<th>Sukūrė</th>
			<th>Veiksmai</th>
		</tr>
		<?php
		//
		//JOIN 
		//(`".DB_moment_activity."`.`visibility`=1 AND `".DB_moment_activity."`.`ID`=`".DB_moment_activity_links."`.`moment_activity_id` AND  OR
		// `".DB_moment_activity."`.`visibility`=2 AND `".DB_moment_activity."`.`ID`=`".DB_moment_activity_links."`.`moment_activity_id`)
		$result = db_query("SELECT `".DB_moment_activity."`.*
			FROM `".DB_moment_activity."` 
			LEFT JOIN `".DB_moment_activity_links."` `is_group` ON `".DB_moment_activity."`.`ID`=`is_group`.`moment_activity_id` AND `link_type`='group_id' AND `value`='".$vaik['grupes_id']."'
			LEFT JOIN `".DB_moment_activity_links."` `is_kid_id` ON `".DB_moment_activity."`.`ID`=`is_kid_id`.`moment_activity_id` AND `is_kid_id`.`link_type`='kid_id' AND `is_kid_id`.`value`='".CHILD_ID."'
			WHERE `kindergarten_id`=".DB_ID." AND (`".DB_moment_activity."`.`visibility`=0 OR `".DB_moment_activity."`.`visibility`=1 AND `is_group`.`value` IS NOT NULL OR `".DB_moment_activity."`.`visibility`=2 AND `is_kid_id`.`value` IS NOT NULL) AND `".DB_moment_activity."`.`isDeleted`=0
			ORDER BY `date` DESC");
		while ($row = mysqli_fetch_assoc($result)) {
			echo '<tr'.(isset($_GET['edit']) && $_GET['edit'] == $row['ID'] ? ' class="opened-row"' : '').'>
			<td>'.$row['date']." <strong><a href=\"?show=".$row['ID']."\">".filterText($row['title'])."</a></strong></td>
			<td>".getAllEmployees($row['createdByEmployeeId'])."</td>
			<td><a href=\"?show=".$row['ID']."\">Peržiūrėti</a></tr>\n";
		}
		?>
	</table>
<?php } ?>





<?php if(isset($_GET['show'])) {
	$result = db_query("SELECT `".DB_moment_activity."`.*, `".DB_moment_activity_links."`.`link_type`, `".DB_moment_activity_links."`.`value`
		FROM `".DB_moment_activity."` LEFT JOIN `".DB_moment_activity_links."` ON `".DB_moment_activity."`.`ID`=`".DB_moment_activity_links."`.`moment_activity_id` AND `link_type`='group_id' AND `value`='".$vaik['grupes_id']."'
		-- WHERE `kindergarten_id`=".DB_ID." AND (`visibility`=0 OR `visibility`<>0 AND `value`=".$vaik['grupes_id'].") 
		 LEFT JOIN `".DB_moment_activity_links."` AS `additional` ON `".DB_moment_activity."`.`ID`=`additional`.`moment_activity_id` AND `additional`.`link_type`='kid_id' AND `additional`.`value`='".CHILD_ID."'
		WHERE `kindergarten_id`=".DB_ID." AND (`".DB_moment_activity."`.`visibility`=0 OR `".DB_moment_activity."`.`visibility`=1 AND `".DB_moment_activity_links."`.`value`=".$vaik['grupes_id']." OR `".DB_moment_activity."`.`visibility`=2 AND `additional`.`value`=".CHILD_ID.") AND `".DB_moment_activity."`.`isDeleted`=0 AND `".DB_moment_activity."`.`ID`=".(int)$_GET['show']."
		 ORDER BY `date` DESC");
	$show = mysqli_fetch_assoc($result);
	if(!empty($show['title'])) {
		$kids = [];
		$all_kids_newest_valid_info = "SELECT cr.* FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `isDeleted`=0 AND `archyvas`=0 GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from` WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 ORDER BY ".orderName('cr');
		$result = db_query($all_kids_newest_valid_info);
		while($row = mysqli_fetch_assoc($result))
			$kids[$row['parent_kid_id']] = filterText(getName($row['vardas'], $row['pavarde']));
	
		$result = db_query("SELECT * FROM `".DB_moment_activity_links."`
			WHERE `moment_activity_id`='".(int)$_GET['show']."'");
		$activity_links = [];
		while ($row = mysqli_fetch_assoc($result))
			$activity_links[$row['link_type']][] = $row['value'];
		
		echo "<h2>".$show['date']." <strong><a href=\"?show=".$show['ID']."\">".filterText($show['title'])."</a></strong></h2>";
		echo nl2br(filterText($show['description']));
		
		echo '<div class="notice">Veiklą matys: '.$moments_visibility[$show['visibility']];
		echo '<br>Grupė: ';
		foreach($activity_links as $key => $val) {
			if($key == 'group_id') {
				foreach($val as $value)
					echo getAllGroups()[$value].' ';
			} /*elseif($key == 'kid_id') {
				foreach($val as $value)
					echo [$value];
			}*/
		}
		echo '</div>';
		if(isset($activity_links['kid_id']) && count($activity_links['kid_id'])) {
			$kids_marks = [];
			if(isset($activity_links['kid_id'])) {
				foreach($activity_links['kid_id'] as $val)
					$kids_marks[] = $kids[$val];
			}
			echo '<span class="notice">Priskirti '.count($activity_links['kid_id']).' vaikai: '.implode(', ', $kids_marks).'.</span>';
		}
		?>
		<link rel="stylesheet" href="/libs/fancyBox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen">
		<script type="text/javascript" src="/libs/fancyBox/jquery.fancybox.pack.js?v=2.1.5"></script>
		<script type="text/javascript">
		$(document).ready(function() {
			$(".fancybox").fancybox({
				openEffect	: 'none',
				closeEffect	: 'none',
				tpl: {
					error	 : '<p class="fancybox-error">Užklaustas turinys negali būti užkrautas.<br>Prašome bandyti dar kartą vėliau.</p>',
					closeBtn : '<a title="Uždaryti" class="fancybox-item fancybox-close" href="javascript:;"></a>',
					next	 : '<a title="Kitas" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
					prev	 : '<a title="Ankstesnis" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
				}

			});
		});
		</script>
		<a href="?newFoto=<?=$show['ID']?>" class="no-print fast-action fast-action-add">Pridėti foto</a>
		<ol class="foto">
			<?php
			$result = db_query("SELECT `".DB_moment_photos."`.*, `".DB_moment_photos_links."`.`link_type`, `".DB_moment_photos_links."`.`value`
				FROM `".DB_moment_photos."` JOIN `".DB_moment_activity."` ON 
				`".DB_moment_activity."`.`ID`=`".DB_moment_photos."`.`moment_activity_id` AND `kindergarten_id`=".DB_ID." AND `".DB_moment_activity."`.`ID`=".(int)$_GET['show']." AND `".DB_moment_activity."`.`isDeleted`=0 AND `".DB_moment_photos."`.`isDeleted`=0
				LEFT JOIN `".DB_moment_photos_links."` ON `".DB_moment_photos."`.`ID`=`".DB_moment_photos_links."`.`moment_photo_id` AND `link_type`='group_id' AND `value`='".$vaik['grupes_id']."'
				LEFT JOIN `".DB_moment_photos_links."` AS `additional` ON `".DB_moment_photos."`.`ID`=`additional`.`moment_photo_id` AND `additional`.`link_type`='kid_id' AND `additional`.`value`='".CHILD_ID."'
				WHERE `".DB_moment_photos."`.`visibility`=0 OR `".DB_moment_photos."`.`visibility`=1 AND `".DB_moment_photos_links."`.`value`=".$vaik['grupes_id']." OR `".DB_moment_photos."`.`visibility`=2 AND `additional`.`value`=".CHILD_ID."
				ORDER BY `date` DESC");//AND `".DB_moment_activity."`.`ID`=".(int)$_GET['show']."
			$rows = [];
			$ids = [];
			while ($row = mysqli_fetch_assoc($result)) {
				$rows[] = $row;
				$ids[] = $row['ID'];
			}
			//print_r(implode(',', $ids))
			$photos_links = [];
			if(!empty($ids)) {
				$res = db_query("SELECT * FROM `".DB_moment_photos_links."` WHERE `moment_photo_id` IN (".implode(',', $ids).")");
				while ($row = mysqli_fetch_assoc($res))
					$photos_links[$row['moment_photo_id']][$row['link_type']][] = $row['value'];
			}
			foreach($rows as $row) {
				echo '<li><a class="fancybox" rel="gallery1" href="'.UPLOAD_DIR.'foto/'.$row['url'].'" title="'.$row['title'].'"><span class="img"><img alt="" src="'.UPLOAD_DIR.'foto/'.str_replace('.jpg', '_'.$width.'x'.$height.'.jpg', $row['url']).'"></span>'.$row['title'];
				echo '</a><span class="notice">';
				$marks = [];
				if(isset($photos_links[$row['ID']]['event']) || isset($photos_links[$row['ID']]['achievement']))
					echo '<br>Žymės (kategorijos): ';
				if(isset($photos_links[$row['ID']]['event']))
					$marks[] = 'Renginys';
				if(isset($photos_links[$row['ID']]['achievement']))
					$marks[] = 'Vaiko pasiekimas';
					//
				if(isset($photos_links[$row['ID']]['achievement_cat_id'])) {
					foreach($photos_links[$row['ID']]['achievement_cat_id'] as $val)
						$marks[] = $achievements_areas[$val];
				}
				if(isset($photos_links[$row['ID']]['kid_id'])) {
					foreach($photos_links[$row['ID']]['kid_id'] as $val)
						$marks[] = $kids[$val];
				}
				echo implode(', ', $marks);
				echo '</span></li>';
			}
			?>
		</ol>
		<?php 
	}
}
?>
</div>
