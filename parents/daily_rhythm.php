<?php if(!defined('USER')) exit; ?>
<h1>Vaiko dienos ritmas</h1>
<div id="content">
<table>
<tr><th>Laikas</th><th>Veikla</th></tr>
<?php
$result = db_query("SELECT * FROM `".DB_daily_rhythm."` WHERE `kindergarten_id`=".DB_ID." AND `group_id`=".(int)$vaik['grupes_id']/*GROUP_ID*/." ORDER BY `period`");
while($row = mysqli_fetch_assoc($result)) {
	echo "<tr><td>".filterText($row['period'])."</td><td>".nl2br(filterText($row['activity']))."</td></tr>";
}
?>
</table>
</div>
