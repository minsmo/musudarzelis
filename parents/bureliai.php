<?php if(!defined('USER')) exit; ?>

<h1>Vaiko lankomi <!-- papildomi --> neformaliojo ugdymo veiklos užsiėmimai</h1>
<div id="content">
	<table class="vertical-hover">
<tr>
	<th>Veiklos pavadinimas</th>
	<th>Diena</th>
	<th>Nuo</th>
	<th>Iki</th>
</tr>

	<?php
	$r = db_query("SELECT * FROM ".DB_bureliai." 
	JOIN ".DB_bureliai_schedule." ON ".DB_bureliai.".ID=".DB_bureliai_schedule.".burelio_id
	LEFT JOIN ".DB_bureliai_schedule_kids." ON ".DB_bureliai_schedule_kids.".bureliai_schedule_id=".DB_bureliai_schedule.".ID AND ".DB_bureliai_schedule_kids.".kid_id=".CHILD_ID."
	WHERE grup_id=".(int)$vaik['grupes_id']." OR ".DB_bureliai_schedule_kids.".kid_id IS NOT NULL
	ORDER BY `diena`, `nuoVal`, `nuoMin`, `ikiVal`, `ikiMin`, `pavadinimas`");
	while($row = mysqli_fetch_assoc($r))
		echo '<tr><td>'.filterText($row['pavadinimas']).'</td><td>'.$dienos_kada[$row['diena']].'</td><td>'.nr_val_min($row['nuoVal'], $row['nuoMin']).'</td><td>'.nr_val_min($row['ikiVal'], $row['ikiMin']).'</td></tr>';
	?>
	</table>
</div>
