<?php if(!defined('ADMIN')) exit();
//Idea: spalvos blokelio nurodo, kad esi viduje kažko. kaip matrioška.
//Problema: 
?>
<link rel="stylesheet" href="/css/jquery-ui-1.11.2.min.css">
<script type="text/javascript" src="/libs/jquery-ui-1.11.2.min.js"></script>
<script><?php echo file_get_contents('workers/new_achievements.js'); ?></script>
<script src="libs/Chart.min.js"></script>

<h1>Vaiko pasiekimai pagal naują ikimokyklinį aprašą</h1>
<div id="content">
<?=ui_print()?>

<?php
include 'workers/new_achievements_descriptions.php';

//Show table
$result = db_query("SELECT *, COUNT(*) `cnt` FROM `".DB_kid_level."` WHERE `kindergarten_id`=".DB_ID." AND `kid_id`=".CHILD_ID." GROUP BY `date` ORDER BY `date` DESC");
if(mysqli_num_rows($result) > 0) {
	?>
	<form method="get"<?=(isset($_GET['show_table']) ? '' : ' class="no-print"')?> style="margin-top: 10px;">
		<div style="float: left; line-height: 33px;">Įvertinimo data &nbsp;</div> <div class="sel" style="float: left"><select name="show_table" onchange="if(this.value != '') this.form.submit()" required="required">
		<?php
		while ($row = mysqli_fetch_assoc($result))
			echo "<option value=\"".$row['date']."\"".(isset($_GET['show_table']) && $row['date'] == $_GET['show_table'] ? ' selected="selected" class="selected">'.$selectedMark : '>').$row['date']."</option>";
		?>
		</select></div>
		<input type="submit" class="filter no-print" value="Rodyti lentelėje">
	</form><div class="cl"></div>
	<?php
}
if(isset($_GET['show_table'])) {
	
	$date = $_GET['show_table'];
	$result = db_query("SELECT cr.*, `".DB_kid_level."`.*
		FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<='".db_fix($date)."' GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
		LEFT JOIN `".DB_kid_level."` ON cr.`parent_kid_id`=`".DB_kid_level."`.kid_id AND `".DB_kid_level."`.`kindergarten_id`=".DB_ID." AND `".DB_kid_level."`.`date`='".db_fix($date)."'
		WHERE cr.`isDeleted`=0 AND cr.`parent_kid_id`=".CHILD_ID." AND cr.`archyvas`=0
		-- GROUP BY cr.parent_kid_id
		ORDER BY ".orderName('cr', $date).", `".DB_kid_level."`.`area`");//cr.`vardas` ASC, cr.`pavarde` ASC");
	$kids_level = [];
	$kids = [];
	$min_level = 7;
	$max_level = 0;
	while($row = mysqli_fetch_assoc($result)) {
		$kids_level[$row['parent_kid_id']][$row['area']] = $row['level'];
		if($row['level'] != 0)
			$min_level = min($min_level, $row['level']);
		$max_level = max($max_level, $row['level']);
		if(!empty($row['gim_data'])) {
			$start_date = new DateTime($row['gim_data']);
			$since_start = $start_date->diff(new DateTime());
			$kids[$row['parent_kid_id']] = '<td>'.$row['gim_data'].' (<span style="font-family:\'Lucida Console\', monospace">'.str_pad($since_start->y, 2, " ", STR_PAD_LEFT).'</span> m.&nbsp;<span style="font-family:\'Lucida Console\', monospace">'.str_pad($since_start->m, 2, " ", STR_PAD_LEFT).'</span> mėn.)</td>';
		} else 
			$kids[$row['parent_kid_id']] = '<td></td>';
		$kids[$row['parent_kid_id']] .= '<td><strong>'.filterText(getName($row['vardas'], $row['pavarde'])).'</strong></td>';
	}
	$diff_colors = ($max_level - $min_level);
	//echo $min_level.$max_level;
	$diff_colors_step = ($diff_colors > 0 ? round(255 / $diff_colors) : 255);
	$diff_colors_bg = [$min_level => 0, $max_level => 255];
	for($i = $min_level+1; $i < $max_level; ++$i)
		$diff_colors_bg[$i] = $diff_colors_bg[$i-1]+$diff_colors_step;
	$diff_colors_dechex = [];
	for($i = $min_level; $i <= $max_level; ++$i)
		$diff_colors_dechex[$i] = dechex($diff_colors_bg[$i]).dechex($diff_colors_bg[$i]).dechex($diff_colors_bg[$i]);
	//print_r($diff_colors_bg);
	echo '<table class="print-bg"><tr><th>Amžius<br>iki<br>dabar/'.date('Y-m-d').'</th><th></th>';
	foreach($achievements_areas as $area => $title)
		echo "<th style='height: 240px; width: 15px;'><div style='transform: rotate(-90deg); width: 250px; position: absolute; margin-left: -120px; margin-top: -17px;'><a href=\"#\" data-id=\"dialog-".$area."\" class=\"dialog-opener\">$title</a></div></th>";
	echo '</tr>';
	foreach($kids as $ID => $kid) {
		echo '<tr>'.$kids[$ID];
			foreach($achievements_areas as $area => $title) {
				if(isset($kids_level[$ID][$area]) && $kids_level[$ID][$area] != 0)
					echo '<td style="text-align: center; background-color: #'.$diff_colors_dechex[$kids_level[$ID][$area]].'; box-shadow: inset 0 0 0 1000px #'.$diff_colors_dechex[$kids_level[$ID][$area]].'; color: #'.($diff_colors_bg[$kids_level[$ID][$area]] < 128 ? 'fff' : '000').'; -ms-filter: "progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=\'#'.$diff_colors_dechex[$kids_level[$ID][$area]].'\', endColorstr=\'#'.$diff_colors_dechex[$kids_level[$ID][$area]].'\')"; /* IE8 */"><span style="text-align: center; background-color: inherit; color: #'.($diff_colors_bg[$kids_level[$ID][$area]] < 128 ? 'fff' : '000').';">'.$kids_level[$ID][$area].'</span></td>';
					//http://stackoverflow.com/questions/3893986/css-media-print-issues-with-background-color
				else
					echo '<td></td>';
			}
		echo '</tr>';
	}
	echo '</table>';
}




$additional = '';
if(!empty($vaik['gim_data'])) {
	$start_date = new DateTime($vaik['gim_data']);
	$since_start = $start_date->diff(new DateTime());
	$additional = ' '.$vaik['gim_data'].' (dabar t. y. '.date('Y-m-d').' vaikui '.$since_start->y.' m. '.$since_start->m.' mėn.)';
}
echo '<h3 style="margin-left: 25px;">'.filterText(/*$l->getName(*/filterText($vaik['vardas'].' '.$vaik['pavarde'])/*, 'nau')*/).$additional.':</h3>';
$result = db_query("SELECT * FROM `".DB_kid_level."` WHERE `kindergarten_id`=".DB_ID." AND `kid_id`=".(int)CHILD_ID." GROUP BY `date` ORDER BY `date` DESC");
//echo '<div style="margin-left: 50px;">';
if(mysqli_num_rows($result) > 0) {
	$list = [];
	while ($row = mysqli_fetch_assoc($result))
		$list[] = $row;
	?>
	<form method="get" style="margin-top: 7px;">
		<div style="float: left; line-height: 32px; margin-left: 25px; margin-right: 5px;">Nustatymai vaiko diagramai:</div>
		<div class="sel" style="float: left; margin-right: 5px;"><select name="date">
		<?php
		foreach($list as $row)
			echo "<option value=\"".$row['date']."\"".(isset($_GET['date']) && $_GET['date'] == $row['date'] ? ' selected="selected" class="selected">'.$selectedMark : '>').$row['date']."</option>";
		?>
		</select></div>
		<div class="primary-data-label" title="Diagramos spalva">(juoda)</div>
		<div class="sel" style="float: left; margin-right: 5px;"><select name="dateWith">
		<?php
		echo "<option value=\"\"".(isset($_GET['dateWith']) && $_GET['dateWith'] == '' ? ' selected="selected" class="selected">'.$selectedMark : '>')."Nelyginti su kita data</option>";
		foreach($list as $row)
			echo "<option value=\"".$row['date']."\"".(isset($_GET['dateWith']) && $_GET['dateWith'] == $row['date'] ? ' selected="selected" class="selected">'.$selectedMark : '>').$row['date']."</option>";
		?>
		</select></div>
		<div class="secondary-data-label">(žalia)</div>
		<div class="sel no-print" style="float: left; margin-right: 5px;"><select name="chart_type">
		<?php
		$chart_types = ['Linijų', 'Stulpelių', 'Voratinklio'];
		foreach($chart_types as $id => $val)
			echo "<option value=\"".$id."\"".(isset($_GET['chart_type']) && $_GET['chart_type'] == $id ? ' selected="selected" class="selected">'.$selectedMark : '>').$val."</option>";
		?>
		</select></div>
		<input class="no-print filter" type="submit" id="view-btn" value="Nupiešti">
	</form>
	
	<?php
	if(isset($_GET['date'])) {
		$result = db_query("SELECT * FROM `".DB_kid_level."` WHERE `kindergarten_id`=".DB_ID." AND `kid_id`=".(int)CHILD_ID." AND `date`='".db_fix($_GET['date'])."'");
		$level = array();
		while ($row = mysqli_fetch_assoc($result))
			$level[$row['area']] = $row['level'];
		$level2 = array();
		if(!empty($_GET['dateWith'])) {
			$result = db_query("SELECT * FROM `".DB_kid_level."` WHERE `kindergarten_id`=".DB_ID." AND `kid_id`=".(int)CHILD_ID." AND `date`='".db_fix($_GET['dateWith'])."'");
			while ($row = mysqli_fetch_assoc($result))
				$level2[$row['area']] = $row['level'];
		}
		?>
		<br>
		<canvas id="achievementsChart" width="767" height="400"></canvas>
		<script>
		// Get the context of the canvas element we want to select
		var ctx = document.getElementById("achievementsChart").getContext("2d");
		Chart.defaults.global.scaleFontSize = 16;
		Chart.defaults.global.scaleFontColor = "#000";
		Chart.defaults.global.responsive = true;
		Chart.defaults.global.scaleOverride = true;
		Chart.defaults.global.scaleSteps = 7,
		Chart.defaults.global.scaleStepWidth = 1;
		Chart.defaults.global.scaleStartValue = 0;
		var data = {
			labels: ["<?php echo implode('","', $achievements_areas); ?>"],
			datasets: [
				{
					fillColor: "<?php echo ($_GET['chart_type'] == 1 ? '#666' : 'transparent'); ?>",//"#666",//"rgba(220,220,220,0.2)",
					strokeColor: "<?php echo ($_GET['chart_type'] == 1 ? '#666' : '#000'); ?>",//"rgba(220,220,220,1)",
					pointColor: "#000",//"rgba(220,220,220,1)",
					pointStrokeColor: "#000",
					pointHighlightFill: "#000",
					data: [<?php foreach($achievements_areas as $area => $title) {
						if(isset($level[$area]))
							echo $level[$area].',';
						else
							echo '0,';
					} ?>]
				}
				<?php if(!empty($_GET['dateWith'])) { ?>
				, {
					fillColor: "<?php echo ($_GET['chart_type'] == 1 ? '#357d32' : 'transparent'); ?>",//'rgba(188,216,188,0.5)',//'#bcd8bc',//"rgba(151,187,205,0.2)",
					strokeColor: '#357d32',//"rgba(151,187,205,1)",
					pointColor: 'rgba(53,125,50,0.8)',//'#357d32',//"rgba(151,187,205,1)",
					pointStrokeColor: 'rgba(53,125,50,1)',//'#357d32',//"#fff",
					pointHighlightFill: 'rgba(53,125,50,1)',//'#357d32',//"#fff",
					pointHighlightStroke: 'rgba(53,125,50,1)',//'#357d32',//"rgba(151,187,205,1)",
				
					data: [<?php foreach($achievements_areas as $area => $title) {
						if(isset($level2[$area]))
							echo $level2[$area].',';
						else
							echo '0,';
					} ?>]
				}
				<?php } ?>
			]
		};
		//var myNewChart = new Chart(ctx).PolarArea(data);
		<?php if($_GET['chart_type'] == 0) { ?>
		var myLineChart = new Chart(ctx).Line(data/*, options*/);
		<?php } ?>
		<?php if($_GET['chart_type'] == 1) { ?>
		var myLineChart = new Chart(ctx).Bar(data/*, options*/);
		<?php } ?>
		<?php if($_GET['chart_type'] == 2) { ?>
		var myLineChart = new Chart(ctx).Radar(data/*, options*/);
		<?php } ?>
		</script>
		<?php
	}
}
?>
</div>
