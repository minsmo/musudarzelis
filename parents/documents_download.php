<?php
include '../main.php';
if(!defined('USER')) exit; 
abstract class DocumentsCommonPermissions {//Access rights
    const Workers = 0;
    const All = 1;
}


//
if(isset($_GET['kid_document_id'])) {
	$result = db_query("SELECT * FROM `".DB_documents_common."`	WHERE `kindergarten_id`='".DB_ID."' AND `ID`=".(int)$_GET['kid_document_id']." AND `vaiko_id`=".CHILD_ID);
	$row = mysqli_fetch_assoc($result);
	if(isset($row['failo_vardas'])) {
		$file = '.'.UPLOAD_DIR.(int)$row['vaiko_id']."/".basename($row['failo_vardas']);
	}
}

if(isset($_GET['id'])) {
	$result = db_query("SELECT * FROM `".DB_documents_common."`	WHERE `kindergarten_id`='".DB_ID."' AND `ID`=".(int)$_GET['id']."  AND `access`=".DocumentsCommonPermissions::All);
	$row = mysqli_fetch_assoc($result);
	if(isset($row['file_name'])) {
		$file = '.'.UPLOAD_DIR."common/".basename($row['file_name']);
	}
}

if (file_exists($file)) {
	header('Content-Description: File Transfer');
	header('Content-Type: application/octet-stream');
	header('Content-Disposition: attachment; filename='.basename($file));
	header('Expires: 0');
	header('Cache-Control: must-revalidate');
	header('Pragma: public');
	header('Content-Length: ' . filesize($file));
	readfile($file);
	exit;
}
