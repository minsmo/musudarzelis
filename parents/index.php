<?php if(!defined('DARBUOT') || !defined('USER') || !USER || DARBUOT) exit(); ?>
<!DOCTYPE html>
<html dir="ltr" lang="lt-LT">
<head>
    <meta charset="UTF-8">
    <script type="text/javascript">function prefixedPerfSupport(){if(!!window.msPerformance){return"ms"}else if(!!window.webkitPerformance){return"webKit"}else{return false}}var start=(new Date).getTime();var featureDetect={};featureDetect.supported=window.msPerformance||window.webkitPerformance||window.performance?true:false;featureDetect.prefixed=prefixedPerfSupport();featureDetect.unprefixed=self.performance?true:false;if(featureDetect.supported){if(featureDetect.unprefixed){var t=window.performance.timing.navigationStart}else if(featureDetect.prefixed=="ms"){var t=window.msPerformance.timing.navigationStart}else if(featureDetect.prefixed=="webkit"){var t=window.webkitPerformance.timing.navigationStart}else{var t=start}}else{var t=start}var DOMContentLoadedDiff</script><script type="text/javascript" src="/libs/jsnlog.min.js"></script>
    <title><?php echo filterText($config['darzelio_pavadinimas']); ?></title>
    <link rel="stylesheet" href="/css/is-new.css">
    <script>var ie = false;</script>
	<!--[if lt IE 12]>
	<style type="text/css">
	select {width: 100%;}
	.sel {background-image:none;}
	</style>
	<![endif]-->
	<!--[if lt IE 8]>
	<style type="text/css">
	select { width: auto; }
	</style>
	<![endif]-->
	<!--[if lt IE 7]>
	<style type="text/css">
	#nav {position: absolute; top: 0;}
	#content-sidebar {left: 0;}
	#content-wrapper {margin-left: 229px;}
	</style>
	<script>ie = true;if (!window.console) console = {log: function() {}};</script>
	<![endif]-->
	<script type="text/javascript" src="/libs/jquery-1.11.3.min.js"></script>
	<meta name="msapplication-config" content="none"/>
	<script>
	$(document).on("DOMContentLoaded",function(){DOMContentLoadedDiff=(new Date).getTime()-t});
	$(function() {
		var open = true;
		$('#menu-toggle').click(function() {
			$('#nav').toggle();
			if(open) {
				if(ie)
					$('#content-wrapper').css('marginLeft', 0);
				else
					$('#content-sidebar').css('left', '0');
				$('#menu-toggle').css('transform', 'rotate(180deg)');
				$('#menu-toggle').addClass('menu-toggle-closed');
				$('#menu-toggle').removeClass('menu-toggle-opened');
				
			} else {
				if(ie)
					$('#content-wrapper').css('marginLeft', '229px');
				else
					$('#content-sidebar').css('left', '229px');
				$('#menu-toggle').css('transform', 'rotate(0deg)');
				$('#menu-toggle').addClass('menu-toggle-opened').removeClass('menu-toggle-closed');
			}
			open = !open;
		});
	});
	</script>
</head>
<body onload="onLoad()">
<?php
if(!ALLOW_PARENTS && !KESV)
	die('<h1>Jūsų įstaiga nesuteikusi teisių prieiti prie sistemos. Galite kreiptis į savo įstaigos kuruojantį asmenį.</h1> <a href="/?atsijungti">Atsijungti</a>');
	
$vaik = mysqli_fetch_assoc(db_query("SELECT `".DB_groups."`.*, cr.* 
FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<=CURDATE() GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from` AND cr.`parent_kid_id`=".CHILD_ID." AND cr.`isDeleted`=0
LEFT JOIN `".DB_groups."` ON `".DB_groups."`.`ID`=cr.`grupes_id`"));
?>
<div id="header" class="no-print">
	<div id="title">
		<?=$config['darzelio_pavadinimas']?>
	</div>
	<?php
	include './libs/linksniai.php';
	$l = new Linksniai;
	?>
	Sveiki, <?=($_SESSION['USER_DATA']['person_subtype'] == 1 ? 'mamyte' : 'tėveli')?>
	<?=$l->getName(filterText($_SESSION['USER_DATA']['name'].' '.$_SESSION['USER_DATA']['surname']))?>
</div>
<div id="content-sidebar">
	<div id="nav" class="no-print">
	<?php
	//	echo "<span class=\"no-print\">Vaikas: <strong>".$vaik['vardas']." ".$vaik['pavarde'].'</strong> - <a href="/vaiko_info"'.(isset($_GET['q']) && $_GET['q'] == 'vaiko_info' ? ' class="active"' : '').'>Duomenys darželiui</a>';
    if(count($_SESSION['children']) > 1) {
       echo '<form method="post" style="display: inline;"><div class="sel"><select name="change_child_id" onchange="this.form.submit()">';
       foreach($_SESSION['children'] as $key => $row)
          echo "<option value=\"".$row['ID']."\"".(CHILD_ID == $row['ID']  ? ' selected="selected"' : '').">".$row['vardas']." ".$row['pavarde']."</option>";
       echo '</select></div></form>';
    }
    ?>
    <ul>
    	<li<?php echo ($_GET['q'] == '' ? ' class="active"' : '')?> style="background-position: 5px center; background-image: url(img/menu-home.png);"><a href="/">Pagrindinis</a></li>
    	<?php
    	$result = db_query("SELECT * FROM `1parents_menu_access` WHERE `kindergarten_id`=".DB_ID);
		$used = [];
		$allowed_URL = [];
		while($row = mysqli_fetch_assoc($result)) {
			$used[$row['menu_item_id']] = '';
			$allowed_URL[$parents_menu_items[$row['menu_item_id']]['url']] = '';
		}
		foreach($parents_menu_items as $key => $pair)
			if(isset($used[$key]))
				echo '<li'.($_GET['q'] == $pair['url'] ? ' class="active"' : '')."><a href=\"/".$pair['url']."\">".$pair['title']."</a>";
		/*
    	?>
		<li<?php echo ($_GET['q'] == 'vaiko_info' ? ' class="active"' : '')?>><a href="/vaiko_info">Duomenys darželiui</a></li>
		<?php if(!(KaunoVarpelis || KaunoRBaibokyne)) { ?>
			<!-- <a href="/?ataskaita">Ataskaita (Pagrindinis)</a> | --><!-- Pradžia? -->
			<?php if(!(LazdijuVyturelis || SEMINARINE)) { ?>
			<li<?php echo ($_GET['q'] == 'lankomumas' ? ' class="active"' : '')?>><a href="/lankomumas">Lankomumas</a></li>
			<?php } ?>
			<?php if(!(DruskininkuBitute || KretingosRGruslaukesPagrMok || SEMINARINE)) { ?>
			<li<?php echo ($_GET['q'] == 'pazanga' ? ' class="active"' : '')?>><a href="/pazanga">Pažanga ir pasiekimai</a></li>
			<?php } ?>
			<?php if($showOldPlanning && !LazdijuRAzuoliukas) {//&& !LazdijuVyturelis
			?>
			<li<?php echo ($_GET['q'] == 'planavimas' ? ' class="active"' : '')?>><a href="/planavimas">Planavimas</a></li>
			<?php } ?>
		<?php } ?>
		<?php if(!LazdijuRAzuoliukas) { ?>
			<?php if(!(DruskininkuBitute)) { ?>
			<li<?php echo ($_GET['q'] == 'Planavimas' ? ' class="active"' : '')?>><a href="/Planavimas">Planavimas naujų formų</a></li>
			<?php } ?>
			<?php if(!(KretingosRGruslaukesPagrMok || SEMINARINE)) { ?>
			<li<?php echo ($_GET['q'] == 'dokumentai' ? ' class="active"' : '')?>><a href="/dokumentai">Vaiko dokumentai</a></li>
			<?php } ?>
		<?php } ?>
		<?php if(TESTINIS || RODOMASIS || SEMINARINE) { ?>
			<li<?php echo ($_GET['q'] == 'activities' ? ' class="active"' : '')?>><a href="/activities">Numatomos veiklos</a></li>
		<?php } ?>
		<?php if(!LazdijuVyturelis) { ?>
			<li<?php echo ($_GET['q'] == 'tvarkarastis' ? ' class="active"' : '')?>><a href="/tvarkarastis">Užsiėmimų tvarkaraštis<!-- Pap. veiklos tvarkaraštis --></a></li>
			<?php if(!LazdijuRAzuoliukas) { ?>
				<li<?php echo ($_GET['q'] == 'daily_rhythm' ? ' class="active"' : '')?>><a href="/daily_rhythm">Vaiko dienos ritmas</a></li>
				<?php if(!(KretingosRGruslaukesPagrMok || SEMINARINE)) { ?>
				<li<?php echo ($_GET['q'] == 'documents_common' ? ' class="active"' : '')?>><a href="/documents_common">Bendri dokumentai</a></li>
				<?php } ?>
				<?php if(!(DruskininkuBitute || KretingosRGruslaukesPagrMok)) { ?>
				<li<?php echo ($_GET['q'] == 'menu' ? ' class="active"' : '')?>><a href="menu">Valgiaraštis</a></li>
				<?php } ?>
				<?php if(TESTINIS || RODOMASIS || SiauliuRSavivald || KaunoVaikystesLobiai || LazdijuKregzdute) { //!(KaunoVarpelis || KaunoZiburelis || KaunoRudnosiukas || KaunoRBaibokyne)) {
				if($showPayment) {
					?><li<?php echo ($_GET['q'] == 'mokejimai' ? ' class="active"' : '')?>><a href="/mokejimai">Mokėjimai</a></li><?php
				}
				}
			}
			?>
			<?php if(TESTINIS || RODOMASIS || SiauliuRSavivald) { //!(KaunoVarpelis || KaunoZiburelis || KaunoRudnosiukas || KaunoRBaibokyne)) { ?>
				<li<?php echo ($_GET['q'] == 'grupes_kontaktai' ? ' class="active"' : '')?>><a href="/grupes_kontaktai">Grupės tėvų kontaktai</a></li>
			<?php } ?>
		<?php } ?>
		<?php if(!KretingosRGruslaukesPagrMok) { ?>
		<li<?php echo ($_GET['q'] == 'speech_therapist_schedule' ? ' class="active"' : '')?>><a href="/speech_therapist_schedule">Logopedo tvarkaraštis</a></li>
		<li<?php echo ($_GET['q'] == 'speech_therapist_topics' ? ' class="active"' : '')?>><a href="/speech_therapist_topics">Logopedo temos</a></li>
		<?php } ?>
		*/ ?>
		<?php if(DB_PREFIX == 'knsZelmen_') { ?>
		<li<?php echo ($_GET['q'] == 'virtuali_biblioteka' ? ' class="active"' : '')?>><a href="/virtuali_biblioteka">Virtuali biblioteka</a></li>
		<?php } ?>
		
		<li<?php echo ($_GET['q'] == 'goodies' ? ' class="active"' : '')?>><a href="/goodies">Naudinga informacija</a></li>
		<li<?php echo (isset($_GET['q']) && $_GET['q'] == 'zinutes' ? ' class="active"' : '')?> style="background-position: 8px center; background-image: url(img/menu-messages.png);"><a href="/zinutes">Susirašinėjimas<?php
		$result = db_query("SELECT COUNT(*) AS cnt FROM `".DB_messages."` WHERE `toPersonId`=".(int)$vaik['parent_kid_id']." AND `toPersonType`=0 AND `read`=0");
		//if(mysqli_num_rows($result) > 0) {
			$r = mysqli_fetch_assoc($result);
			if($r['cnt'] > 0)
				echo ' <strong class="abbr" title="Naujos sisteminės žinutės">('.$r['cnt'].')</strong>';
		//}
		?></a></li>
		<li<?php echo ($_GET['q'] == 'pakeisti_slaptazodi' ? ' class="active"' : '')?> style="background-position: 8px center; background-image: url(img/menu-password.png);"><a href="pakeisti_slaptazodi">Slaptažodžio keitimas</a></li>
		<li style="background-position: 9px center; background-image: url(img/menu-logout.png);"><a href="/?atsijungti">Atsijungti</a></li>
    </ul>
    </div>
	<div id="content-wrapper">
		<div id="menu-toggle" class="menu-toggle-opened">&lt;</div>
    <?php
    $parents_routes = [
		'dokumentai' => './parents/dokumentai.php',
		'tvarkarastis' => './parents/bureliai.php',
		'vaiko_info' => './parents/vaiko_info.php',
		'mokejimai' => './parents/mokejimai.php',//WTF?
		'mokejimai' => './parents/spausdinti.php',//WTF?
		'grupes_kontaktai' => './parents/grupes_kontaktai.php',
		'planavimas' => './parents/planning.php',
		'Planavimas' => './parents/forms_preview.php',
		'lankomumas' => './parents/lankomumas.php',
		'pazanga' => './parents/pazanga.php',
		'zinutes' => './parents/messages.php',
		'menu' => './parents/food_menu.php',
		'daily_rhythm' => './parents/daily_rhythm.php',
		'pakeisti_slaptazodi' => './workers/change_password.php',
		'activities' => './parents/activities.php',
		'documents_common' => './parents/documents_common.php',
		'goodies' => './workers/useful_stuff.php',
		'speech_therapist_schedule' => './parents/speech_therapist/schedule.php',
		'speech_therapist_topics' => './parents/speech_therapist/topics.php',
		'achievements_steps' => './parents/new_achievements.php',
		'logo_achievements_view' => './parents/speech_therapist/achievements_view.php',
		'docs' => './parents/docs.php',
		'moments' => './parents/moments.php',
		'virtuali_biblioteka' => './parents/library.php',
	];
	if((isset($allowed_URL[$_GET['q']]) || $_GET['q'] == 'zinutes' || $_GET['q'] == 'goodies' || $_GET['q'] == 'pakeisti_slaptazodi') && isset($parents_routes[$_GET['q']]))
		include $parents_routes[$_GET['q']];
	else {
		echo ' <h1>Pradžia</h1>
<div id="content">';
		$result = db_query("SELECT * FROM `".DB_activities."` WHERE `kindergarten_id`=".DB_ID." AND (`group_id`=".(int)$vaik['grupes_id']/*GROUP_ID*/." OR `group_id`=0) ORDER BY `period` DESC");
		if(mysqli_num_rows($result) > 0) {
			$groups = getAllGroups();
			$groups[0] = 'Visos grupės';
			?>
			<table>
			<tr><th>Data</th><th>Vaikų grupė</th><th>Veikla</th><th>Kita</th></tr>
			<?php
			while($row = mysqli_fetch_assoc($result))
				echo "<tr><td>".filterText($row['period'])."</td><td>".$groups[$row['group_id']]."</td><td>".nl2br(filterText($row['activity']))."</td><td>".nl2br(filterText($row['other']))."</td></tr>";
			?>
			</table>
			<?php
		}
		echo '</div>';
		
		//echo '<script src="//www.paysera.lt/m/m_pp/files/js/epWidget.js" type="text/javascript"></script><a href="javascript:WebToPayWidget.load(82892, \'lit\');"><img src="//www.paysera.lt/m/m_pp/images/widget/pay_by_sms_lit.png" border="0" /></a>';
	}
    ?>
    </div>
</div>
<script>
<?php if(!(KESV || TESTINIS || RODOMASIS || NAUJAS)) { ?>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-1202195-12', 'musudarzelis.lt');
  ga('send', 'pageview');
<?php } ?>
function onLoad(){var page_load_time = new Date().getTime()-t;<?php
	if(!startsWith($_GET['q'], 'uploads/') && 'apple-touch-icon-precomposed.png' != $_GET['q'] && 'apple-touch-icon.png' != $_GET['q']) {
	$result = db_query("SELECT `menu_id` FROM `0menu` WHERE `user_type`='p' AND `menu_url`='".db_fix($_GET['q'])."'");
	if(mysqli_num_rows($result) == 0) {
		db_query("INSERT INTO `0menu` SET `user_type`='p', `menu_url`='".db_fix($_GET['q'])."'");
		$menu_id = (int)mysqli_insert_id($db_link);
	} else
		$menu_id = (int)mysqli_fetch_assoc($result)['menu_id'];
	db_query("INSERT INTO `0time` SET `ipv4`=INET_ATON('{$_SERVER['REMOTE_ADDR']}'), `time`='".(microtime(true)-$start)."', `kindergarten`=".array_search(DB_PREFIX, $db_prefixes).", `user_id`=".USER_ID.", `person_id`=".DARB_ID.", `person_login_type`=".$_SESSION['USER_DATA']['person_type'].", `menu_id`=".(int)$menu_id);
	$log_id = (int)mysqli_insert_id($db_link);
	?>$.post("/log.php",{DOMContentLoaded:DOMContentLoadedDiff,onLoad:page_load_time,id:<?=$log_id?>});
	<?php } ?>
	}</script>
</body>
</html>
