<?php if(!defined('USER')) exit;
abstract class DocumentsCommonPermissions {//Access rights
    const Workers = 0;
    const All = 1;
}
?>
<h1>Bendri dokumentai</h1>
<div id="content">
	<table>
    <caption>Įkelti dokumentai<caption>
    <tr>
    	<th title="Svarba"><span class="abbr">S</span></th>
    	<th>Atsisiųsti</th>
        <th>Pavadinimas</th>
        <th>Aprašymas</th>
        <th>Įkeltas</th>
    </tr>
    <?php
    $result = db_query("SELECT * FROM `".DB_documents_common."` WHERE `kindergarten_id`='".DB_ID."' AND `access`=".DocumentsCommonPermissions::All." AND `isArchived`=0 ORDER BY `created` DESC");
    while($row = mysqli_fetch_assoc($result))
        echo "<tr>
        	<td>".($row['isStar'] ? '<span style="color: #006600;">★</span>' : '')."</td>
        	<td><a href=\"parents/documents_download.php?id=".$row['ID']."\">".filterText($row['file_name'])."</a></td>
            <td>".filterText($row['file_name'])."</td>
            <td>".filterText($row['description'])."</td>
            <td>".$row['created']."</td>
        </tr>";
    ?>
    </table>
</div>
