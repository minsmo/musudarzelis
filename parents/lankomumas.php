<?php if(!defined('USER')) exit();
$showPayment = $showPayment && !LazdijuRAzuoliukas;
?>
<h1>Vaiko lankomumas</h1>
<div id="content">
<form method="get" action="">
	<input type="hidden" name="ataskaita">
    Nuo:
    <div class="sel"><select name="nuo">
    <?php
    /*$result = mysqli_query($db_link, "SELECT `".DB_working_d."`.* 
    FROM `".DB_working_d."` LEFT JOIN `".DB_attendance."` ON `".DB_attendance."`.`veikimo_d_id`=`".DB_working_d."`.`ID`
    WHERE `".DB_attendance."`.`vaiko_id`=".CHILD_ID."
    ORDER BY `".DB_working_d."`.`metai` ASC, `".DB_working_d."`.`menuo` ASC, `".DB_working_d."`.`diena` ASC") or logdie('Neteisinga užklausa: ' . mysqli_error($db_link));*/
    $result = db_query("SELECT * FROM `".DB_attendance."` WHERE `vaiko_id`=".CHILD_ID." ORDER BY `data` ASC");
    while ($row = mysqli_fetch_assoc($result)) {
        echo '<option value="'.$row['data'].'"'.(isset($_GET['nuo']) && $row['data'] == $_GET['nuo'] ? ' selected="selected"' : '').'>'.$row['data'].'</option>';
        //echo '<option value="'.$row['veikimo_d_id'].'"'.(isset($_GET['nuo']) && $row['veikimo_d_id'] == $_GET['nuo'] ? ' selected="selected"' : '').'>'.$row['metai'].'-'.men_dien($row['menuo'], $row['diena']).'</option>';
        /*if(isset($_GET['nuo']) && $row['veikimo_d_id'] == $_GET['nuo']) {
            $nuo_m = $row['metai'];
            $nuo_men = $row['menuo'];
            $nuo_d = $row['diena'];
        }*/
    }
    ?>
    </select></div>
    Iki: 
    <div class="sel"><select name="iki">
    <?php
   /* $result = mysqli_query($db_link, "SELECT `".DB_working_d."`.* FROM `".DB_working_d."` LEFT JOIN `".DB_attendance."` ON `".DB_attendance."`.`veikimo_d_id`=`".DB_working_d."`.`ID`
    WHERE `".DB_attendance."`.`vaiko_id`=".CHILD_ID."
    ORDER BY `".DB_working_d."`.`metai` DESC, `".DB_working_d."`.`menuo` DESC, `".DB_working_d."`.`diena` DESC") or logdie('Neteisinga užklausa: ' . mysqli_error($db_link)); */
    $result = db_query("SELECT * FROM `".DB_attendance."` WHERE `vaiko_id`=".CHILD_ID." ORDER BY `data` DESC");
    while ($row = mysqli_fetch_assoc($result)) {
        echo '<option value="'.$row['data'].'"'.(isset($_GET['iki']) && $row['data'] == $_GET['iki'] ? ' selected="selected"' : '').'>'.$row['data'].'</option>';
        //echo '<option value="'.$row['veikimo_d_id'].'"'.(isset($_GET['iki']) && $row['veikimo_d_id'] == $_GET['iki'] ? ' selected="selected"' : '').'>'.$row['metai'].'-'.men_dien($row['menuo'], $row['diena']).'</option>';
        /*if(isset($_GET['iki']) && $row['veikimo_d_id'] == $_GET['iki']) {
            $iki_m = $row['metai'];
            $iki_men = $row['menuo'];
            $iki_d = $row['diena'];
        }*/
    }
    ?>
    </select></div>
    <input type="submit" value="Filtruoti" class="filter">
</form>

<?php
$search = '';
$limit = ' LIMIT 30';
if(isset($_GET['nuo'])) {
	$search .= ' AND `data`>=\''.db_fix($_GET['nuo']).'\'';
    $limit = '';
}
if(isset($_GET['iki'])) {
	$search .= ' AND `data`<=\''.db_fix($_GET['iki']).'\'';
    $limit = '';
}
if(!empty($limit))
	echo 'Rodoma 30 naujausių dienų:';
?>
<table>
<tr>
	<th>Diena</th>
<!--	<td>Pastabos</td>
	<td>Pasiekimai</td> -->
	<th>Buvimas</th>
	<?php
	if($showPayment) {
		if(!DruskininkuBitute)
			echo '<th>Maitinimui</th>';
		else
			echo '<th>Dienos kaina</th>';
		if(KaunoVaikystesLobiai)
			echo '<th>'.getConfig('papildomos_dienos_kainos,_kuri_ivedama_zymint_lankomuma,_pavadinimas', date('Y-m-d')).'</th>';
		$result = db_query("SELECT SUM(`value`)>0 `SHOW`  FROM `".DB_config."` WHERE `title` IN ('kaina_ugdymo_reikmems_lopselyje_per_d.', 'kaina_ugdymo_reikmems_darzelyje_per_d.', 'kaina_ugdymo_reikmems_priesmokykliniame_per_d.')");
		$show_cost_of_educational_needs = mysqli_fetch_assoc($result)['SHOW'];
		if($show_cost_of_educational_needs)
			echo '<th>Ugdymo reikmėms</th>';
	}
	?>
</tr>
<?php
//echo $search;
$result = db_query("SELECT * FROM `".DB_attendance."` WHERE `vaiko_id`=".CHILD_ID." $search ORDER BY `data` DESC".$limit);

//AND (`pastabos`<>'' OR (SELECT count(*) FROM `".DB_achievements."` WHERE `".DB_achievements."`.`veikimo_d_id`=`".DB_attendance."`.`veikimo_d_id` AND `".DB_achievements."`.`vaiko_id`=`".DB_attendance."`.`vaiko_id`))
$sum = 0;
if(mysqli_num_rows($result) > 0) {
	while ($row = mysqli_fetch_assoc($result)) {
		$currency = ' Lt';
		if(substr($row['data'], 0, 4) >= '2015')
			$currency = ' € ';
	
		echo "\t<tr>
		<td>".$row['data']."</td>";
		/*<td>".filterText($row['pastabos'])."</td>
		<td>";
		$result2 = mysqli_query($db_link, "SELECT * FROM `".DB_achievements."` WHERE `vaiko_id`=".(int)$row['vaiko_id']." AND `veikimo_d_id`=".(int)$row['veikimo_d_id']);
		while ($row2 = mysqli_fetch_assoc($result2))
			echo $child_achievements[$row2['tipas']]. ": ".filterText($row2['pavadinimas']).";<br>";

		echo "</td>*/
		echo "<td>";
		
		if(!$row['yra']) {
			/*$result2 = mysqli_query($db_link, "SELECT * FROM `".DB_documents."` JOIN `".DB_justified_d."` ON `".DB_documents."`.`ID`=`".DB_justified_d."`.`dok_id` WHERE `".DB_documents."`.`vaiko_id`=".(int)$row['vaiko_id']." AND `".DB_justified_d."`.`veikimo_d_id`=".(int)$row['veikimo_d_id']);
			if(mysqli_num_rows($result2)) {
				$moketi = 0;
				while($row2 = mysqli_fetch_assoc($result2))
					echo $row2['aprasymas'];
			} else
			echo "Nepateisinta";*/
			if($row['justified_d'] > 0) {
				echo 'Pateisinta';
				$moketi = 0;
			} else {
				echo 'Nepateisinta';
				$moketi = 1;
			}
		} else {
			echo '+';
			$moketi = 1;
		}

		echo "</td>";
		if($showPayment) {
			echo "<td>";
			if(KaunoVaikystesLobiai) {
				$day_food_cost = 0;
				$kid = getKid(CHILD_ID, $row['data']);
				if($row['yra'] || !$row['yra'] && !$row['justified_d']) {//Būtoms ir nelankytoms be pateisinimo
					$day_food_cost = $kid['dienosMokestis'] ? (double)getConfig('puses_dienos_maitinimas_per_d._d.', $row['data']) : (double)getConfig('pilnos_dienos_maitinimas_per_d._d.', $row['data']);
				}
				//$day_cost = $day_food_cost+(double)$row['additional_cost_per_day'];
				$sum += $day_food_cost;
				echo price($day_food_cost).$currency;
				$discount = 1;
			} elseif(DruskininkuBitute) {
					$kid = getKid(CHILD_ID, $row['data']);
			
					$discount = $nuolaidos[$kid['arNuolaida']];// ? 0.5 : 1;
					$kid_level = array(
						0 => 'lopselyje',
						1 => 'darzelyje',
						2 => 'priesmokykliniame'
					);
					$cur_kid_level = $kid_level[$kid['arDarzelyje']];
					
					$day_cost = 0;
					if($moketi) {
						$day_cost = (double)getConfig('dienos_kaina_per_d._'.$cur_kid_level.'_'.$nuolaidos_title[$kid['arNuolaida']], $row['data']);
					}// else {
					//	$day_cost = (double)getConfig('dienos_kaina_per_d._'.$cur_kid_level.'_'.$nuolaidos_title[4], $row['data']);//80 % nuolaida => *0,2
					//}
					$sum += $day_cost;
					echo price($day_cost).$currency;
			} else {
				if($moketi) {
					$kid = getKid(CHILD_ID, $row['data']);
	
					$discount = $nuolaidos[$kid['arNuolaida']];// ? 0.5 : 1;
					$kid_level = array(
						0 => 'lopselyje',
						1 => 'darzelyje',
						2 => 'priesmokykliniame'
					);
					$cur_kid_level = $kid_level[$kid['arDarzelyje']];
					$day_food = $kid['arPusryciaus']*!$kid['free_breakfast']*(double)getConfig('vieneriu_pusryciu_kaina_'.$cur_kid_level, $row['data'])+$kid['arPietaus']*!$kid['free_lunch']*(double)getConfig('vieneriu_pietu_kaina_'.$cur_kid_level, $row['data'])+$kid['arPavakariaus']*!$kid['free_afternoon_tea']*(double)getConfig('vieneriu_pavakariu_kaina_'.$cur_kid_level, $row['data'])+$kid['arVakarieniaus']*!$kid['free_dinner']*(double)getConfig('vienerios_vakarienes_kaina_'.$cur_kid_level, $row['data']);

					$day_food_cost = $day_food*$discount;
					$sum += $day_food_cost;
					echo price($day_food_cost).$currency;
				} else {
					$discount = 0;
					echo price(0).$currency;
				}
			}
			echo "</td>\n";
			if(KaunoVaikystesLobiai) {
				$sum += (double)$row['additional_cost_per_day'];
				echo "<td>".price((double)$row['additional_cost_per_day']/*kartais nuolaida yra valstybiniams darželiams*/).$currency."</td>";
			}
			if($show_cost_of_educational_needs) {
				if($moketi) {
					if(!$apply_discount_for_educational_needs_fee_each_day)
						$discount = 1;
					$edu_needs = (double)kaina_ugdymo_reikmiu($kid['arDarzelyje'], $row['data'])*$discount;
					$sum += $edu_needs;
					echo '<td>'.price($edu_needs).$currency.'</td>';
				} else {
					echo '<td></td>';
				}
			}
		}
		echo "</tr>\n";
	}
} else echo "<tr><td colspan=\"5\"><strong>Tuščia.</span></td></tr>";
if($showPayment) {
?>
		<tr>
			<td colspan="5" class="right">Suma: <?=price($sum)?> (neatsižvelgus į valiutą)</td>
		</tr>
<?php } ?>
	</table>
</div>
