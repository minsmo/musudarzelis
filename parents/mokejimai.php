<?php if(!defined('USER')) exit; ?>
<h1>Mokėjimai</h1>
<div id="content">
<?php if(TESTINIS || RODOMASIS || NAUJAS) { ?>
	<table>
	<tr>
		<th>Laikotarpis</th>
		<th>Už darželį</th>
		<th>Ugdymo reikmėms</th>
		<th>Suma</th>
		<th>Ar apmokėta?</th>
		<th>Spausdinti</th>
	</tr>
	<?php
	$result = db_query("SELECT * FROM  ".DB_payments." WHERE `kindergarten_id`=".DB_ID." AND ".DB_payments.".vaiko_id=".(int)CHILD_ID);
	while ($row = mysqli_fetch_assoc($result)) {
	?>
	<tr>
		<td><?php echo $row['metai'].'-'.men($row['menuo']); ?></td>
		<td class="right"><?php echo (double)$row['uzDarzeli']; ?></td>
		<td class="right"><?php echo (double)$row['ugdymoReikmem']; ?></td>
		<td class="right"><?php echo (double)$row['isViso']; ?></td>
		<td><?php echo ($row['arApmoketa'] ? 'Apmokėta': 'Reikia apmokėti'); ?></td>
		<td><?php echo '<a href="/spausdinti?ID='.$row['ID'].'">Sudaryti kvitą</a>'; ?></td>
	</tr>
	<?php
	}
	?>
	</table>
<?php } else { ?>
	<table>
	<tr>
		<th>Laikotarpis</th>
		<th>Kvitas</th>
	</tr>
	<?php
	$kids = array();
	$result = db_query("SELECT * FROM `".DB_children."` WHERE `parent_kid_id`=".CHILD_ID." AND `isDeleted`=0 ORDER BY `valid_from` DESC");
	while ($row = mysqli_fetch_assoc($result)) {
		$kids[] = $row;
	}
	
	$result = db_query("SELECT YEAR(`data`) `metai`, MONTH(`data`) `menuo`, MIN(`data`) `min_date` FROM `".DB_attendance."` WHERE `vaiko_id`=".CHILD_ID." GROUP BY YEAR(`data`) DESC, MONTH(`data`) DESC");
	while ($row = mysqli_fetch_assoc($result)) {
		if(!(date('Y') == $row['metai'] && date('n') == $row['menuo'])) {
			/*$groups = array();
			foreach($kids as $kid) {
				//if(substr($kid['valid_from'], 0, 5) > $row['metai'].'-'.$row['menuo']) {
				//	$skip = true;
				//} else 
				if($kid['valid_from'] <= $row['min_date']) {
					$groups[$kid['grupes_id']] = 1;
					break;
				}
				if(substr($kid['valid_from'], 0, 5) == $row['metai'].'-'.$row['menuo']) {//There is an edge case
					$groups[$kid['grupes_id']] = 1;
				}
			}
			foreach($groups as $group_id => $tmp) {*/
				?>
				<tr>
					<td><?php echo $row['metai'].'-'.men($row['menuo']); ?></td>
					<td><?php echo '<a href="/spausdinti?period='.$row['metai'].'-'.$row['menuo']./*.'&amp;group_id='.$group_id.*/'">Sudaryti kvitą</a>'; ?></td>
				</tr>
				<?php
			//}
		}
	}
	?>
<?php } ?>
</div>
