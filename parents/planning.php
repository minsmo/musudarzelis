<?php if(!defined('USER')) exit; ?>
<h1>Planavimas</h1>
<div id="content">
<?php
$result = db_query("SELECT * FROM `".DB_planning."` WHERE groupId=".$vaik['grupes_id']." ORDER BY `created` DESC");
if(mysqli_num_rows($result) > 0) {
	?>
<table id="planning_tbl">
<tr>
	<th style="width: 161px;">Nuo–iki</th>
	<th><?=(KaunoZiburelis ? 'Turinys' : 'Tema')?></th>
	<th>Tikslas</th>
	<th>Uždaviniai</th>
	<th>Kita</th>
</tr>
<?php
	while ($row = mysqli_fetch_assoc($result)) {
		if(isset($_GET['view']) && $_GET['view'] == $row['planning_id'])
			$plan_view = $row;
		echo "		<tr".(isset($_GET['view']) && $_GET['view'] == $row['planning_id'] ? ' class="opened-row"' : '').">
		<td>".$row['fromDate']."–".$row['toDate']."</td>
		<td>".filterText($row['topic'])."</td>
		<td>".filterText($row['tikslas'])."</td>
		<td>".filterText($row['uzdaviniai'])."</td>
		<td><a href=\"?view=".$row['planning_id']."#planning-view\">Plačiau</a></td>
	</tr>";	
	}
	echo '</table>';
}

if(isset($_GET['view']) && isset($plan_view)) {
	?>
	<div id="planning-view">
		<p><span class="title"><?=(KaunoZiburelis ? 'Turinys' : 'Tema')?>:</span> <?=filterText($plan_view['topic'])?></p>
		<p><span class="title">Laikotarpis:</span> <?=date_empty($plan_view['fromDate'])?>–<?=date_empty($plan_view['toDate'])?></p>
		<p><span class="title">Planuojama (ugdomoji) veikla <span class="details">(tikslas)</span>:</span><br>
			<?=filterText($plan_view['tikslas'])?></p>
		<p><span class="title">Planuojama (ugdomoji) veikla <span class="details">(uždaviniai)</span>:</span><br>
			<?=filterText($plan_view['uzdaviniai'])?></p>
		<p><span class="title">Individuali veikla <span class="details">(Ugdymo individualizavimas pagal vaiko poreikius, gebėjimus ir kitus ypatumus)</span>:</span><br>
			<?php
	$result = mysqli_query($db_link, "SELECT `".DB_children."`.*, `".DB_planning_individual."`.`activity` 
	FROM `".DB_planning_individual."` JOIN `".DB_children."` ON `".DB_children."`.`ID`=`".DB_planning_individual."`.`child_id`
	WHERE `".DB_planning_individual."`.`plan_id`=".(int)$plan_view['planning_id']." AND `".DB_children."`.`ID`=".CHILD_ID) or logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
	while ($row = mysqli_fetch_assoc($result)) {
		echo "<p><strong>".filterText($row['vardas']." ".$row['pavarde'])."</strong> ".filterText($row['activity'])."</p>";
	}
			?></p>
		<?php if(!LazdijuVyturelis) { ?>
		<p><span class="title">(Ugdomoji) veikla vaikų grupelėms <span class="details">(Ugdymo diferencijavimas pagal vaikų poreikius, gebėjimus ir kitus ypatumus)</span>:</span><br>
			<?=filterText($plan_view['ugdomojiVeiklaVaikuGrupelems'])?></p>
		<p><span class="title">Sąveika/bendradarbiavimas su šeima <span class="details">(Auklėtojų, tėvų (globėjų) iniciatyvos, siūlymai, bendradarbiavimo būdai)</span>:</span><br>
			<?=filterText($plan_view['saveikaSuSeima'])?></p>
		<p><span class="title">Veikla visai vaikų grupei <span class="details">(Turinys, metodai, pagrindinės priemonės, vieta)</span>:</span><br>
			<?php
			if(TESTINIS/*KaunoZiburelis*/) {
				?>
				<table>
				<thead>
					<tr>
						<th>Eil.<br>Nr.</th>
						<th><!-- Veiklos pavadinimas -->Ugdomoji veikla</th>
						<!-- <th>Vaiko pasiekimai, kurie vestų į galutinį rezultatą</th> -->
						<th>Priemonės</th>
						<th>Galutinis vaiko pasiekimų rezultatas pasiekus uždavinį</th>
					</tr>
				</thead>
				<tbody id="tbody">
				<?php
				$plan_view['veiklaVisaiVaikuGrupei'] = unserialize($plan_view['veiklaVisaiVaikuGrupei']);
				//print_r($plan['veiklaVisaiVaikuGrupei']);
				$i=0;
				if(!empty($plan_view['veiklaVisaiVaikuGrupei'])) foreach($plan_view['veiklaVisaiVaikuGrupei'] as $veiklaVisaiVaikuGrupei) {
					echo '<tr><td>'.(++$i).'</td>
					<td>'.filterText($veiklaVisaiVaikuGrupei['VeiklosPavadinimas']).'</td>
					
					<td>'.filterText($veiklaVisaiVaikuGrupei['Priemones']).'</td>
					<td>'.filterText($veiklaVisaiVaikuGrupei['GalutinisRezultatas']).'</td>';
				}//<td>'.filterText($veiklaVisaiVaikuGrupei['VaikoPasiekimai']).'</td>
				?>
				</body>
				</table>
				<?php
			} else {
				echo filterText($plan_view['veiklaVisaiVaikuGrupei']);
			}
			?></p>
		<p><span class="title">Savaites apmąstymai, refleksija <span class="details">(Kaip sekėsi įgyvendinti numatytą tikslą ir uždavinius)</span>:</span><br>
			<?=filterText($plan_view['savaitesApmastymaiRefleksija'])?></p>
		<?php } ?>
	</div>
	<?php
}
?>
</div>
