<?php if(!defined('USER')) exit; ?>
<h1>Numatomos veiklos</h1>
<div id="content">
<table>
<tr><th>Data</th><th>Vaikų grupė</th><th>Veikla</th><th>Kita</th></tr>
<?php
$groups = getAllGroups();
$groups[0] = 'Visos grupės';
$result = db_query("SELECT * FROM `".DB_activities."` WHERE `kindergarten_id`=".DB_ID." AND (`group_id`=".(int)$vaik['grupes_id']/*GROUP_ID*/." OR `group_id`=0) ORDER BY `period` DESC");
while($row = mysqli_fetch_assoc($result))
	echo "<tr><td>".filterText($row['period'])."</td><td>".$groups[$row['group_id']]."</td><td>".nl2br(filterText($row['activity']))."</td><td>".nl2br(filterText($row['other']))."</td></tr>";
?>
</table>
</div>
