<?php if(!defined('USER')) exit; ?>
<h1>Vaiko dokumentai</h1>
<div id="content">
<?php
if(isset($_POST['issaugoti'])) {
    if(is_uploaded_file($_FILES['file']['tmp_name'])) {
        //if(verify_image($_FILES['file']['tmp_name'])) {
            $photo_pic = $_FILES['file'];
            $photo_ext = strrchr($photo_pic['name'], ".");

            if($photo_pic['size'] >= 1024*1024*5) {
                echo "<div class=\"red center\">Klaida: Dokumentas gali užimti iki 5 MB</div>";
            } elseif(!in_array(strtolower($photo_ext), $file_type)) {
                echo "<div class=\"red center\">Klaida: Dokumento rūšis privalo būti: ".implode(', ', $file_type)."</div>";
            } else {
                $photo_new_name = UPLOAD_DIR.(int)CHILD_ID."/".$photo_pic['name'];
	            if(is_file($photo_new_name)) {//file_exists()
	            	if(sha1_file($photo_new_name) == sha1_file($photo_pic['tmp_name'])) {
	            		msgBox('ERROR', 'Failas neišsaugotas. Prie šio vaiko toks dokumentas jau pridėtas, antrą kartą kelti tokį patį nėra prasmės. Jei reikia tik pakeiskite papildomus laukelius prie dokumento.');
	            	} else {
	            		msgBox('ERROR', 'Failas neišsaugotas. Prie šio vaiko dokumentas su tokiu pačiu pavadinimu jau yra (sistema netikrina ar toks pats turinys). 1) Jau pridėtas į sistemą toks pats dokumentas, tada dar kartą kelti jo nebereikia. 2) Pridedate kitą dokumentą į sistemą, kurio failo pavadinimas sutampa, tuomet pridedamo pakeiskite failo pavadinimą kompiuteryje ir bandykite dar kartą.');
	            	}
	            } else {
					$do_save = true;
					if(!is_dir(UPLOAD_DIR.(int)CHILD_ID))
						if(!mkdir(UPLOAD_DIR.(int)CHILD_ID, 0777)) {
							msgBox('ERROR', 'Nepavyko sukurti katalogo');
							$do_save = false;
						}
					if(move_uploaded_file($photo_pic['tmp_name'], $photo_new_name))
			            msgBox('OK', 'Dokumentas išsaugotas.');
			        else {
			        	msgBox('ERROR', 'Dokumentas neišsaugotas.');
						$do_save = false;
					}
					//chmod($photo_new_name, 0644);
					if($do_save)
						if(db_query("INSERT INTO `".DB_documents."` SET 
							failo_vardas='".db_fix($photo_pic['name'])."', aprasymas='".db_fix($_POST['aprasymas'])."', 
							tipas=".(int)$_POST['tipas'].", vaiko_id=".(int)CHILD_ID.', createdByUserId='.USER_ID))
							echo "<div class=\"green center\">Dokumento informacija išsaugota duomenų bazėje.</div>";
	            }
            }
        //} else echo "<div class=\"red center\">Dokumentas tikriausiai nėra tinkamo turinio/formato.</div>";
    } else echo "<div class=\"red center\">Nėra dokumento.</div>";
}
?>

	<table>
    <caption>Įkelti dokumentai<caption>
    <tr>
        <th>Pavadinimas</th>
        <th>Rūšis</th>
        <th>Aprašymas</th>
        <th>Įkeltas</th>
    </tr>
    <?php
    //"SELECT `".DB_children."`.*, `".DB_documents."`.* FROM `".DB_documents."` INNER JOIN `".DB_children."` ON ".DB_children.".ID=`".DB_documents."`.vaiko_id WHERE  ORDER BY `".DB_documents."`.`created` DESC");
    $result = db_query("SELECT cr.*, `".DB_documents."`.*
	FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<=CURDATE() GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
	JOIN `".DB_documents."` ON cr.`parent_kid_id`=`".DB_documents."`.vaiko_id
	WHERE `".DB_documents."`.vaiko_id=".(int)CHILD_ID." -- CHECK: is it necessary?: cr.`isDeleted`=0 AND cr.`archyvas`=0 AND 
	ORDER BY `".DB_documents."`.`created` DESC");

    while($row = mysqli_fetch_assoc($result))
        echo "<tr>
            <td>".filterText($row['failo_vardas'])."</td>
            <td>".$dok_tipai[$row['tipas']]."</td>
            <td>".filterText($row['aprasymas'])."</td>
            <td>".$row['created']."</td>
        </tr>";
    ?>
    </table>


    <fieldset style="margin-top: 40px;" id="document_form">
    <legend>Įkelti naują dokumentą</legend>
    <form enctype="multipart/form-data" method="post" action="" onsubmit="return checkUpload()">
		<p><label>Dokumentas Jūsų kompiuteryje*:<br><input type="file" style="width: 300px;" name="file"></label></p>
        <p>
            <label>Rūšis*:
            <div class="sel"><select name="tipas">
            <?php
            foreach($dok_tipai as $id=>$val)
                echo "<option value=\"$id\">$val</option>";
            ?>
            </select></div>
            </label>
        </p>
        <p><label>Trumpas aprašymas (ypač raštelio iš gydytojo atveju):<br><input type="text" style="width: 300px;" name="aprasymas" value=""></label></p>
        <p><input type="submit" name="issaugoti" value="Įkelti" class="submit"></p>
    <script>
    function checkUpload() {
		var file = document.getElementById('file').files[0];
		if(file && file.size < 2097152) { // 2 MB (this size is in bytes)
		    //Submit form        
		} else {
			alert('Failas didesnis negu 2 MB, todėl jo įkelti negalima. Jei tai paveiksliukas išsaugokite jį jpg formatu arba sumažinkite jo raišką, kad jo užimamas dydis sumažėtų. Tada bandykite dar kartą jau su mažesniu failu.');
		    //Prevent default and display error
		    //evt.preventDefault();
		    return false;
		}
    }
    </script>
	</form>
	</fieldset>
</div>
