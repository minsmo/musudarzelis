<?php if(!defined('USER')) exit;
$max_opened_msg_delay = 30;//30 s.
abstract class FieldAction
{
    const Edit = 0;
    const OnlyView = 1;
}
?>
<h1>Vaikų grupės veiklos planavimas</h1>
<div id="content">
<?php
$darbuotojai = array();
$darbuotojai[0] = '';
$result = db_query("SELECT * FROM `".DB_employees."`");
while ($row = mysqli_fetch_assoc($result))
	$darbuotojai[$row['ID']] = filterText($row['vardas'].' '.$row['pavarde']);

$result = db_query("SELECT `".DB_forms_fields_answer."`.*, `".DB_forms_fields_answers."`.`answer`, UNIX_TIMESTAMP(`".DB_forms_fields_answer."`.`openedDate`) AS `openedDateTimestamp` 
	FROM `".DB_forms_fields_answer."` JOIN `".DB_forms_fields_allowed."` ON (`".DB_forms_fields_answer."`.`form_id`=`".DB_forms_fields_allowed."`.`form_id` AND `allowed_for_person_type`=0 AND `action`=".FieldAction::OnlyView.")
	LEFT JOIN `".DB_forms."` ON `".DB_forms."`.`form_id`=`".DB_forms_fields_answer."`.`form_id`
	LEFT JOIN `".DB_forms_fields_answers."` ON `".DB_forms_fields_answers."`.`field_id`=`primary_field_id`
	WHERE `".DB_forms_fields_answer."`.`group_id`=".$vaik['grupes_id']." AND `".DB_forms_fields_answer."`.`isDeleted`=0
	GROUP BY `".DB_forms_fields_answer."`.`answer_id`
	ORDER BY `".DB_forms_fields_answer."`.`toDate` DESC");
if(mysqli_num_rows($result) > 0) {
	$form_title = array();
	$re = db_query("SELECT `form_id`, `title` FROM `".DB_forms."`");
	while ($row = mysqli_fetch_assoc($re))
		$form_title[$row['form_id']] = $row['title'];
	?>
	<table class="vertical-hover<?php if(isset($_GET['view'])) echo ' no-print'; ?>">
	<tr>
		<th style="width: 161px;">Nuo–iki</th>
		<th>Formos pavadinimas</th>
		<th>Papildoma info</th>
		<th>Sukūrė</th>
		<th>Atnaujino</th>
		<th>Veiksmai</th>
	</tr>
	<?php
	while ($row = mysqli_fetch_assoc($result)) {
		echo "		<tr".(isset($_GET['view']) && $_GET['view'] == $row['answer_id'] ? ' class="opened-row"' : '').">
		<td>".$row['fromDate']."–".$row['toDate']."</td>
		<td>".$form_title[$row['form_id']]."</td>
		<td>".filterText($row['answer'])."</td>
		<td>".$row['created']." ".$darbuotojai[$row['createdByEmployeeId']]."</td>
		<td>".date_empty($row['updated'])." ".$darbuotojai[$row['updatedByEmployeeId']]."</td>
		<td><a href=\"?view=".$row['answer_id']."#planning-view\">Peržiūrėti</a> ";
		if($row['openedDateTimestamp'] >= time()-$max_opened_msg_delay)
			echo ' [Dabar redaguoja '.$darbuotojai[$row['openedByEmployeeId']]." ".$row['openedDate']."]";
		echo "
		</td>
	</tr>";	
	}
	?>
	</table>
	<?php
}

if(isset($_GET['view'])) {
	$answer_result = db_query("SELECT * FROM `".DB_forms_fields_answer."` WHERE `isDeleted`=0 AND `answer_id`='".(int)$_GET['view']."'", 'aNeteisinga užklausa: ');
	if(mysqli_num_rows($answer_result) > 0) {
		$answer = mysqli_fetch_assoc($answer_result);
		
		$grupes = getAllGroups();
		$grupes[0] = 'Neįvesta';
		
		$res = db_query("SELECT * FROM `".DB_forms_fields_allowed."` WHERE `form_id`=".$answer['form_id']);//.' AND `action`=1'
		$ViewAllowed = array();
		while($row = mysqli_fetch_assoc($res))
			$ViewAllowed[$row['field_id']][$row['allowed_for_person_type']] = '';
		
		//DRY LEFT JOIN kadangi jei bus daug kitų answer'ių, bet maniškiam field'ui nebus įvesta tai būtent dėl to
		$result = db_query("SELECT *, `".DB_forms_fields."`.`field_id`
			FROM `".DB_forms_fields."` LEFT JOIN `".DB_forms_fields_answers."` ON (`".DB_forms_fields."`.`field_id`=`".DB_forms_fields_answers."`.`field_id` AND `".DB_forms_fields_answers."`.`answer_id`='".(int)$_GET['view']."')
			WHERE `".DB_forms_fields."`.`form_id`=".$answer['form_id']."
			ORDER BY `".DB_forms_fields."`.`order` ASC");
		?><div id="planning-view">
		<div class="no-print">Pildyta pagal formą: <?=$form_title[$answer['form_id']]?></div>
		<p><span class="title">Grupė:</span> <?=$grupes[$answer['group_id']]?></p>
		<p><span class="title">Laikotarpis:</span> <?=date_empty($answer['fromDate'])?>–<?=date_empty($answer['toDate'])?></p>
		<?php
		while($field = mysqli_fetch_assoc($result)) {
			if(isset($ViewAllowed[(int)$field['field_id']][$_SESSION['USER_DATA']['person_type']])) {
				if($field['type'] == 0) {//One line
					echo '<p><label><span class="title">'.filterText($field['title']).(!empty($field['help_text']) ? ' <span class="details">('.filterText($field['help_text']).')</span>' : '' ).($field['isRequired'] ? '<span class="required">*</span>' : '').':</span><br>'.filterText($field['answer']).'</p>';
				} elseif($field['type'] == 1) {//Paragraph text
					echo '<p><label><span class="title">'.filterText($field['title']).(!empty($field['help_text']) ? ' <span class="details">('.filterText($field['help_text']).')</span>' : '' ).($field['isRequired'] ? '<span class="required">*</span>' : '').':</span><br>'.nl2br(trim(filterText($field['answer']))).'</p>';
				} elseif($field['type'] == 2) {//Data
					echo '<p><label><span class="title">'.filterText($field['title']).(!empty($field['help_text']) ? ' <span class="details">('.filterText($field['help_text']).')</span>' : '' ).($field['isRequired'] ? '<span class="required">*</span>' : '').':</span><br>'.filterText($field['answer']).'</p>';
				} elseif($field['type'] == 3) {//Individual planning
					echo '<p><span class="title">Individuali veikla <span class="details">(Ugdymo individualizavimas pagal vaiko poreikius, gebėjimus ir kitus ypatumus)</span>:</span><br>';
		
					$res = db_query("SELECT `".DB_children."`.*, `".DB_forms_fields_answers_i."`.`activity` 
						FROM `".DB_forms_fields_answers_i."` JOIN `".DB_children."` ON `".DB_children."`.`ID`=`".DB_forms_fields_answers_i."`.`child_id`
						WHERE `".DB_forms_fields_answers_i."`.`answer_id`=".(int)$answer['answer_id']." AND `".DB_forms_fields_answers_i."`.`child_id`=".CHILD_ID." ORDER BY vardas, pavarde");
					while ($row = mysqli_fetch_assoc($res))
						echo "<p><strong>".filterText($row['vardas']." ".$row['pavarde'])."</strong>: ".filterText($row['activity'])."</p>";
					echo '</p>';
				} elseif($field['type'] == 4 && KaunoZiburelis) {
					//TODO
				} elseif($field['type'] >= 10) {//---------------------------------------------
					$res = db_query("SELECT * FROM `".DB_forms_widget_fields_allowed."` WHERE `kindergarten_id`=".DB_ID." AND `widget_id`=".(int)$field['type']);//form_widget_id .' AND `action`=1'
					$result_w = db_query("SELECT * FROM `".DB_forms_widget_fields."` WHERE `kindergarten_id`=".DB_ID." AND `widget_id`=".(int)$field['type']." ORDER BY `order` ASC");
					echo '<table class="plan">
					<tr>';
					while($row = mysqli_fetch_assoc($result_w)) {				
						echo '<th>'.$row['title'].'</th>';
					}
					echo '</tr>';
					$field['answer'] = json_decode($field['answer'], true);
					if(!empty($field['answer'])) {
						$cnt = count(reset($field['answer']));
						for($i = 0; $i < $cnt; ++$i) {
							echo '<tr>';
							foreach($field['answer'] as $tbl_col_id => $row) {
								echo '<td>'.nl2br(filterText($row[$i])).'</td>';
							}
							echo '</tr>';
						}
					}
					echo '</table>';
				}
			}
		}
		?><div class="no-print">
				<div style="float: left; width: 70px;">Sukūrė:</div><div style="float: left;"><?=$answer['created']?> <?=$darbuotojai[$answer['createdByEmployeeId']]?></div>
				<div style="clear: left; float: left; width: 70px;">Atnaujino:</div><div style="float: left;"><?=date_empty($answer['updated'])?> <?=$darbuotojai[$answer['updatedByEmployeeId']]?></div>
				<div style="clear: left;"></div>
			</div>
		</div><?php
	} else {
		echo '<p>Nėra tokio užpildyto planavimo.</p>';
	}
}
?>
</div>
