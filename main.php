<?php
//MOVED toauto_prepend.php date_default_timezone_set('Europe/Vilnius');//PHP 5.2 warning

define('PATH', dirname(__FILE__));
error_reporting(E_ALL | E_STRICT);
define('KESV', $_SERVER['REMOTE_ADDR'] === '88.222.149.115');
if($_SERVER['REMOTE_ADDR'] === '88.222.149.115' || $_SERVER['REMOTE_ADDR'] == '88.222.53.158') {
	ini_set('display_errors', '1');
} else {
	//error_reporting(0);
	ini_set('display_errors', '0');
}
ini_set('log_errors', '1');
ini_set('error_log', PATH.'/private/logs/php.log');
ini_set('error_prepend_string', $_SERVER['REMOTE_ADDR']);

set_error_handler('log_handler');
function log_handler ( $errno, $errstr,  $errfile, $errline, $errcontext ) {
	$bt = debug_backtrace();
	$bt0 = (isset($bt[0]) && isset($bt[0]['file']) ? basename(dirname($bt[0]['file'])).'/'.basename($bt[0]['file']).':'.$bt[0]['line'] : '');
	$bt1 = (isset($bt[1]) ? basename(dirname($bt[1]['file'])).'/'.basename($bt[1]['file']).':'.$bt[1]['line'] : '');
	$bt2 = (isset($bt[2]) ? basename(dirname($bt[2]['file'])).'/'.basename($bt[2]['file']).':'.$bt[2]['line'] : '');
	$errorType = array(
		E_ERROR          => 'ERROR',
		E_WARNING        => 'WARNING',
		E_PARSE          => 'PARSING ERROR',
		E_NOTICE         => 'NOTICE',
		E_CORE_ERROR     => 'CORE ERROR',
		E_CORE_WARNING   => 'CORE WARNING',
		E_COMPILE_ERROR  => 'COMPILE ERROR',
		E_COMPILE_WARNING => 'COMPILE WARNING',
		E_USER_ERROR     => 'USER ERROR',
		E_USER_WARNING   => 'USER WARNING',
		E_USER_NOTICE    => 'USER NOTICE',
		E_STRICT         => 'STRICT NOTICE',
		E_RECOVERABLE_ERROR  => 'RECOVERABLE ERROR'
	);
	file_put_contents(PATH.'/private/logs/php.log', date("y-m-d H:i:s").(defined('DB_PREFIX') ? DB_PREFIX : '').' '.$errorType[$errno]." ($errstr) ".str_replace(PATH.'/', '', $errfile).":$errline ".$_SERVER['REMOTE_ADDR'].' '.$_SERVER['REQUEST_METHOD'].' '.$_SERVER['REQUEST_URI'].' '.$_SERVER['HTTP_ACCEPT_LANGUAGE'].' '.$_SERVER['HTTP_USER_AGENT'].(isset($_SERVER['HTTP_REFERER']) ? ' REF:'.$_SERVER['HTTP_REFERER'].' ' : '').(isset($_SESSION['USER_DATA']['email']) ? ' '.$_SESSION['USER_DATA']['email'] : '')." USER_ID:".USER_ID.' '.$bt0.' '.$bt1.' '.$bt2."\n", FILE_APPEND | LOCK_EX);
	
	//E_USER_NOTICE
	//[20-Aug-2014 14:43:02 Europe/Vilnius] PHP Notice:  Use of undefined constant log_handler - assumed 'log_handler' in /home/kestutis/dev.musudarzelis.lt/main.php on line 17
	//errno:8 (Use of undefined constant aaa - assumed 'aaa') file:/home/kestutis/dev.musudarzelis.lt/main.php, line:22
	//PHP errno:$errno ($errstr) file:$errfile, line:$errline
}

function loga($rec, $type = '', $log_type = 0, $context_details = '') {
	$bt = debug_backtrace();
	$bt0 = (isset($bt[0]) ? basename(dirname($bt[0]['file'])).'/'.basename($bt[0]['file']).':'.$bt[0]['line'] : '');
	$bt1 = (isset($bt[1]) ? basename(dirname($bt[1]['file'])).'/'.basename($bt[1]['file']).':'.$bt[1]['line'] : '');
	$bt2 = (isset($bt[2]) ? basename(dirname($bt[2]['file'])).'/'.basename($bt[2]['file']).':'.$bt[2]['line'] : '');
	$email = (isset($_SESSION['USER_DATA']['email']) ? ' '.$_SESSION['USER_DATA']['email'] : '');
	$HTTP_REFERER = (isset($_SERVER['HTTP_REFERER']) ? ' REF:'.$_SERVER['HTTP_REFERER'] : '');
	if($type == '')
		file_put_contents(PATH.'/private/logs/common.log', (defined('DB_PREFIX') ? DB_PREFIX : '').date("y-m-d H:i:s").' '.$_SERVER['REMOTE_ADDR'].' '.$_SERVER['REQUEST_METHOD'].' '.$_SERVER['REQUEST_URI'].' '.$rec.' '.$_SERVER['HTTP_ACCEPT_LANGUAGE'].' '.$_SERVER['HTTP_USER_AGENT'].$HTTP_REFERER.$email." USER_ID:".USER_ID.' '.$bt0."\n", FILE_APPEND | LOCK_EX);//$_SERVER['REQUEST_TIME']
	elseif($type == 'login')
		file_put_contents(PATH.'/private/logs/login.log', (defined('DB_PREFIX') ? DB_PREFIX : '').date("y-m-d H:i:s").' '.$_SERVER['REMOTE_ADDR'].' '.$_SERVER['REQUEST_METHOD'].' '.$_SERVER['REQUEST_URI'].' '.$rec.' '.$_SERVER['HTTP_ACCEPT_LANGUAGE'].' '.$_SERVER['HTTP_USER_AGENT'].$HTTP_REFERER.$email.(defined('USER_ID') ? " USER_ID:".USER_ID : '').' '.$bt0."\n", FILE_APPEND | LOCK_EX);//$_SERVER['REQUEST_TIME']
	elseif($type == 'info')
		file_put_contents(PATH.'/private/logs/info.log', (defined('DB_PREFIX') ? DB_PREFIX : '').date("y-m-d H:i:s").' '.$_SERVER['REMOTE_ADDR'].' '.$_SERVER['REQUEST_METHOD'].' '.$_SERVER['REQUEST_URI'].' '.$rec.' '.$_SERVER['HTTP_ACCEPT_LANGUAGE'].' '.$_SERVER['HTTP_USER_AGENT'].$HTTP_REFERER.$email." USER_ID:".USER_ID.' '.$bt0."\n", FILE_APPEND | LOCK_EX);
	elseif($type == 'db')
		file_put_contents(PATH.'/private/logs/db.log', (defined('DB_PREFIX') ? DB_PREFIX : '').date("y-m-d H:i:s").' '.$_SERVER['REMOTE_ADDR'].' '.$_SERVER['REQUEST_METHOD'].' '.$rec.' '.$bt1.' '.$_SERVER['REQUEST_URI'].$HTTP_REFERER.$email.(defined('USER_ID') ? " USER_ID:".USER_ID : '')."\n\n", FILE_APPEND | LOCK_EX);//$_SERVER['REQUEST_TIME']
	elseif($type == 'db_warn')
		file_put_contents(PATH.'/private/logs/db_warn.log', (defined('DB_PREFIX') ? DB_PREFIX : '').date("y-m-d H:i:s").' '.$_SERVER['REMOTE_ADDR'].' '.$_SERVER['REQUEST_METHOD'].' '.$rec.' '.$bt1.' '.$_SERVER['REQUEST_URI'].$HTTP_REFERER.$email.(defined('USER_ID') ? " USER_ID:".USER_ID : '')."\n\n", FILE_APPEND | LOCK_EX);//$_SERVER['REQUEST_TIME']
	elseif($type == 'warn')//http://stackoverflow.com/questions/7839565/logging-levels-logback-rule-of-thumb-to-assign-log-levels
		db_query("INSERT INTO `0log` SET `ipv4`=INET_ATON('{$_SERVER['REMOTE_ADDR']}'), `kindergarten`='".array_search(DB_PREFIX, $db_prefixes)."', `user_id`='".USER_ID."', `person_id`='".DARB_ID."', `person_login_type`='".(isset($_SESSION['USER_DATA']['person_type']) ? (int)$_SESSION['USER_DATA']['person_type'] : 0)."', `log_type`=".(int)$log_type.", `text`='".db_fix($rec)."', `place`='".db_fix($bt0.' '.$bt1.' '.$bt2)."', `context_details`='".db_fix($context_details)."', `method`='".db_fix($_SERVER['REQUEST_METHOD'])."', `to_url`='".db_fix($_SERVER['REQUEST_URI'])."', `email`='".db_fix($email)."'");//maybe todo menu_id
	elseif($type == 'js') {
		file_put_contents(PATH.'/private/logs/js.log', (defined('DB_PREFIX') ? DB_PREFIX : '').date("y-m-d H:i:s").' '.$_SERVER['REMOTE_ADDR'].' '.$_SERVER['REQUEST_METHOD'].' '.$rec.' '.$bt1.' '.$_SERVER['REQUEST_URI'].$HTTP_REFERER.$email.(defined('USER_ID') ? " USER_ID:".USER_ID : '')."\n\n", FILE_APPEND | LOCK_EX);//$_SERVER['REQUEST_TIME']
	} else
		file_put_contents(PATH.'/private/logs/info.log', 'WRONG loga() function CALL '.(defined('DB_PREFIX') ? DB_PREFIX : '').date("y-m-d H:i:s").' '.$_SERVER['REMOTE_ADDR'].' '.$_SERVER['REQUEST_METHOD'].' '.$rec.' '.$bt1.' '.$_SERVER['REQUEST_URI'].$HTTP_REFERER.$email.(defined('USER_ID') ? " USER_ID:".USER_ID : '')."\n\n", FILE_APPEND | LOCK_EX);//$_SERVER['REQUEST_TIME']
}

function logdie($rec = '') {
	$bt = debug_backtrace();
	$bt0 = (isset($bt[0]) ? basename(dirname($bt[0]['file'])).'/'.basename($bt[0]['file']).':'.$bt[0]['line'] : '');
	$bt1 = (isset($bt[1]) ? basename(dirname($bt[1]['file'])).'/'.basename($bt[1]['file']).':'.$bt[1]['line'] : '');
	$bt2 = (isset($bt[2]) ? basename(dirname($bt[2]['file'])).'/'.basename($bt[2]['file']).':'.$bt[2]['line'] : '');
	$email = (isset($_SESSION['USER_DATA']['email']) ? ' '.$_SESSION['USER_DATA']['email'] : '');
	$HTTP_REFERER = (isset($_SERVER['HTTP_REFERER']) ? ' REF:'.$_SERVER['HTTP_REFERER'] : '');
	file_put_contents(PATH.'/private/logs/logdie.log', (defined('DB_PREFIX') ? DB_PREFIX : '').date("y-m-d H:i:s", time()).' '.$_SERVER['REMOTE_ADDR'].' '.$_SERVER['REQUEST_METHOD'].' '.$_SERVER['REQUEST_URI'].' '.$rec.' '.$_SERVER['HTTP_ACCEPT_LANGUAGE'].' '.$_SERVER['HTTP_USER_AGENT'].$HTTP_REFERER.' '.$bt0." ".$bt1." ".$email." USER_ID:".USER_ID."\n", FILE_APPEND | LOCK_EX);//$_SERVER['REQUEST_TIME']
	//Atsiprašome, kažkas įvyko negerai. Mūsų Darželis darbuotojams apie tai jau pranešta. Papildomai apie tai galite parašyti el. paštu <a href="mailto:info@musudarzelis.lt">info@musudarzelis.lt</a>.
	if(KESV)
		die(msgBox('ERROR', 'Atsiprašome, kažkas įvyko negerai. Susisiekite el. paštu <a href="mailto:info@musudarzelis.lt">info@musudarzelis.lt</a> ir parašykite kokius veiksmus atliekant iškilo ši problema. Ačiū.'.$rec));
	else
		die(msgBox('ERROR', 'Atsiprašome, kažkas įvyko negerai. Susisiekite el. paštu <a href="mailto:info@musudarzelis.lt">info@musudarzelis.lt</a> ir parašykite kokius veiksmus atliekant iškilo ši problema. Ačiū.<!-- '.$rec.' -->'));//Galėtų būti forma pagal kurią būtų atsakoma kada sutaisyta ta situacija arba, kad reikia detaliau papasakoti kaip įvyko tokia klaida.
		//Apibūdinimą kaip tai nutiko ir ką norite padaryti galite rašyti el. paštu <a href="mailto:info@musudarzelis.lt">info@musudarzelis.lt</a>. Padėsime.
		//„Mūsų darželis darbuotojai jau informuoti apie tai.“ siūlo ramunė vietoj „Mūsų Darželis darbuotojams apie tai jau pranešta“
		// aprašyti ir 
		
		//Pagalba el. paštu: 
		// Mūsų Darželis darbuotojai stengsis išsiaiškinti ir sutvarkyti per kelias darbo dienas, jeigu reikalingas skubus pataisymas prašome kreiptis el. paštu <a href="mailto:info@musudarzelis.lt">info@musudarzelis.lt</a>.
	/*
	500 Internal Server Error
Sorry, something went wrong.

A team of highly trained monkeys has been dispatched to deal with this situation.

Also, please include the following information in your error report://If you see them, show them this information:

http://stackoverflow.com/questions/21681084/decoding-youtubes-error-500-page
Vietoj ilgo error details geriau gal error occurance ID įdėti.

Facebook
Sorry, something went wrong.

We're working on getting this fixed as soon as we can.

Go Back

Facebook © 2014 · Help
	*/
	//Dėl atnaujinimo darbų trumpam nepasiekiama. Užeikite po minutės.
}

include 'config.php';
//$link = mysql_connect($db_host, $db_username, $db_password) or logdie('Nepavyko prisijungti prie MySQL: ' . mysqli_error($db_link));
//mysqli_query($db_link, 'SET collation_connection = utf8_lithuanian_ci') or die('Neteisinga užklausa: ' . mysqli_error($db_link));
//mb_language('uni'); 
mb_internal_encoding('UTF-8');//for mb_lcfirst and etc.
//$db_selected = mysql_select_db($db_name, $link) or logdie ('Nepavyko prieiti prie duombazės: ' . mysqli_error($db_link));
$db_link = mysqli_connect($db_host, $db_username, $db_password, $db_name) or logdie("Error " . mysqli_error($db_link)); 
mysqli_set_charset($db_link, 'utf8') or logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
//mysqli_query("set names 'utf8'") or logdie('Neteisinga užklausa: ' . mysqli_error($db_link));

$db_time = 0;
$db_query_cnt = 0;
$queries = [];
function db_query($sql, $msg = '') {
	global $db_link, $db_time, $db_query_cnt, $queries;
	$tic = microtime(true);
	$result = mysqli_query($db_link, $sql) or logdie((empty($msg) ? 'Neteisinga užklausa: ' : $msg).mysqli_error($db_link)/*.$sql*/);
	$toc = microtime(true)-$tic;
	$db_time += $toc;
	$queries[] = round($toc, 5).' '.$sql;
	++$db_query_cnt;
	if($toc > 0.2)
		loga(sprintf("%.3f", $toc) /*." " .$sql*/, 'db');
	$to = mysqli_warning_count($db_link);
	if ($to > 0) {
		for ($i = 0, $e = mysqli_get_warnings($db_link); $i < $to; ++$i, $e->next())
			loga($e->sqlstate.' '.$e->errno.': '.$e->message, 'db_warn');
	}
	return $result;
}


if(!isset($_GET['q']))
	$_GET['q'] = '';

define('CURRENT_DATE', date("Y-m-d"));//Working_d
$result = db_query("SELECT `id`, `prefix`, `allowed_parents_access`, `place` FROM `0db` WHERE `on`=1");
$db_prefixes = array();
$parents_access_allowance = [];
$place = [];
while ($row = mysqli_fetch_assoc($result)) {
	$db_prefixes[$row['id']] = $row['prefix'];
	$parents_access_allowance[$row['id']] = $row['allowed_parents_access'];
	$place[$row['id']] = $row['place'];
}

//if(!defined("WEB")) {
	include_once 'prisijungimas.php';
	//SHOW TABLES
	if(!defined("DB_PREFIX")) define('DB_PREFIX', 't_');
	define('KaunoVaikystesLobiai', DB_PREFIX == 'knsVL_');
	define('KaunoVarpelis', DB_PREFIX == 'knsVarp_');
	define('KaunoZiburelis', DB_PREFIX == 'knsZibur_');
	define('KaunoRudnosiukas', DB_PREFIX == 'knsRudno_');
	define('KaunoRBaibokyne', DB_PREFIX == 'knsrBaibok_');
	define('LazdijuVyturelis', DB_PREFIX == 'lzdVytur_');
	define('LazdijuRAzuoliukas', DB_PREFIX == 'lzdrAzuol_');
	define('SiauliuPasaka', DB_PREFIX == 'sauPasak_');
	define('SiauliuRSavivald', DB_PREFIX == 's_');
	define('SiauliuZiogelis', DB_PREFIX == 'sauZiog_');
	define('LazdijuKregzdute', DB_PREFIX == 'lzdKreg_');
	define('VilniausBaltuSalele', DB_PREFIX == 'vlnBaltu_');
	define('KaunoAtzalele', DB_PREFIX == 'knsAtzal_');//Kauno lopšelis-darželis „Atžalėlė“
	define('LazdijuRZibute', DB_PREFIX == 'lzdrZibut_');//Seirijų lopšelis-darželis „Žibutė“
	define('KretingosAzuoliukas', DB_PREFIX == 'kretAzuol_');//Kretingos lopšelis-darželis „Ąžuoliukas“
	define('TauragesRAzuoliukas', DB_PREFIX == 'tauAzuol_');//Tauragės lopšelis-darželis „Ąžuoliukas“
	define('KaunoZingsnelis', DB_PREFIX == 'knsZings_');//Kauno lopšelio-darželio „Žingsnelis“
	define('MoletuVyturelis', DB_PREFIX == 'molVytur_');//Molėtų „Vyturėlio“ vaikų lopšelis-darželis 
	define('SiauliuZiburelis', DB_PREFIX == 'sauZibur_');//Šiaulių lopšelis-darželis „Žiburėlis“
	define('VisaginoAzuoliukas', DB_PREFIX == 'visMAzuol_');//Visagino vaikų lopšelis-darželis „Ąžuoliukas“
	define('DruskininkuBitute', DB_PREFIX == 'druskBit_');//
	define('PasvalioLiepaite', DB_PREFIX == 'psvLiep_');//Pasvalio darželis-mokykla „Liepaitė“
	define('MazeikiuGiliukas', DB_PREFIX == 'mazGiliuk_');//Mažeikių r. Tirkšlių darželis „Giliukas“
	define('KelmesRTytuvenuLD', DB_PREFIX == 'klmTytuv_');//Kelmės r. Tytuvėnų lopšelis-darželis
	define('KaunoZvangutis', DB_PREFIX == 'knsZvang_');
	define('SkuodoRYlakiuVaikuLD', DB_PREFIX == 'skdrYlak_');//Skuodo r. Ylakių vaikų lopšelis darželis
	define('PanevezioVaivoryksteLD', DB_PREFIX == 'pnvVaivo_');//Panevėžio vaikų lopšelis-darželis „Vaivorykštė“
	define('KaunoSanciuLD', DB_PREFIX == 'knsSanc_');//Kauno Šančių lopšelis-darželis
	define('KaunoVaivoryksteD', DB_PREFIX == 'knsVaivo_');
	define('RokiskioVarpelisLD', DB_PREFIX == 'rokVarp_');
	define('KretingosRSalantuRasaLD', DB_PREFIX == 'kretrRasa_');
	define('KretingosPasakaLD', DB_PREFIX == 'kretPasak_');
	define('SilutesRZemaiciuNaumiescioMD', DB_PREFIX == 'silrZemNau_');//Šilutės r. Žemaičių Naumiesčio mokykla-darželis
	define('KretingosRGruslaukesPagrMok', DB_PREFIX == 'kretGrus_');
	define('TauragesKodelciusLD', DB_PREFIX == 'tauKodel_');
	define('SEMINARINE', strpos(DB_PREFIX, 'semi') === 0);
	define('TESTINIS', DB_PREFIX == 't_');
	define('RODOMASIS', DB_PREFIX == 'r_');
	define('NAUJAS', DB_PREFIX == 'naujas_');
	
	define('DB_ID', array_search(DB_PREFIX, $db_prefixes));
	define('ALLOW_PARENTS', $parents_access_allowance[DB_ID]);
	
	$showOldPlanning = (TESTINIS && KESV || NAUJAS || KaunoZiburelis/*neturi užpildytų planavimų, bet palikau dėl specifikos*/ /*|| KaunoRudnosiukasturi užpildytų planavimų*/ || LazdijuVyturelis/*turi užpildytų planavimų*/ || LazdijuRAzuoliukas/*turi užpildytų planavimų*/ || LazdijuKregzdute/*turi užpildytų planavimų*/ || LazdijuRZibute/*turi užpildytų planavimų*/); // || SiauliuPasaka/*turi tik vieną ir tai testavo tik!*/
	/*
	Neberodyti seno planavimo:
	KaunoVaikystesLobiai
	KaunoVarpelis
	KaunoRBaibokyne
	SiauliuRSavivald
	SiauliuZiogelis
	VilniausBaltuSalele
	KaunoAtzalele//Kauno lopšelis-darželis „Atžalėlė“
	KretingosAzuoliukas//Kretingos lopšelis-darželis „Ąžuoliukas“
	TauragesRAzuoliukas//Tauragės lopšelis-darželis „Ąžuoliukas“*/
	
	
	$showPaymentFor = array(
		'knsVL_',
		'knsVarp_',
		'knsRudno_',
		'knsrBaibok_',
		'lzdVytur_',
		'lzdrAzuol_',
		'lzdKreg_',
		'lzdrZibut_',
		//'naujas_',
		'tauAzuol_',
		'knsZings_',
		'molVytur_',
		//'r_',
		't_',
		'rokVarp_',
		'druskBit_'
	);
	$showPayment = in_array(DB_PREFIX, $showPaymentFor);
	/*$showPaymentForTabeliai = array(
		'knsVL_',
		'knsVarp_',
		'knsZibur_',
		'knsRudno_',
		'knsrBaibok_',
		'lzdVytur_',
		'lzdrAzuol_',
		'lzdKreg_',
		'knsAtzal_',
		'lzdrZibut_',
		//'naujas_',
		'kretAzuol_',
		'tauAzuol_',
		'knsZings_',
		'molVytur_',
		'visMAzuol_',
		//'r_',
		't_',
		'rokVarp_',
		'kretPasak_',
		'kretBaubl_',
		'kretEgl_',
		'kretVover_',
		'druskBit_'
	);
	$showPaymentInTabeliai = in_array(DB_PREFIX, $showPaymentForTabeliai);*/
	
	define('UPLOAD_DIR', './uploads/'.(DB_PREFIX == 't_' ? 'default' : substr(DB_PREFIX, 0, -1)).'/');
	
	define('DB_activities', '1activities');
	define('DB_archive', '1archive');
	define('DB_bureliai', DB_PREFIX.'bureliai');//Activities with specialists| Specialist_activities
	define('DB_bureliai_schedule', DB_PREFIX.'bureliai_schedule');//Specialist_activities_schedule //Old grupes_bureliai-> bureliai_grupems
	//RENAME TABLE  `t_grupes_bureliai` TO  `t_bureliai_schedule` ;
	define('DB_bureliai_schedule_kids', DB_PREFIX.'bureliai_schedule_kids');//Specialist_activities_schedule
	define('DB_config', DB_PREFIX.'config');
	define('DB_employees', DB_PREFIX.'darbuotojai');
	define('DB_employees_groups', DB_PREFIX.'darb_grupes');
	define('DB_daily_rhythm', '1daily_rhythm');
	define('DB_documents', DB_PREFIX.'dokumentai');
	define('DB_documents_common', '1documents_common');
	define('DB_docs', '1docs');
	define('DB_docs_templates', '1docs_templates');
	
	define('DB_food_menu', '1food_menu');	
	define('DB_forms', DB_PREFIX.'forms');
	define('DB_forms_fields', DB_PREFIX.'forms_fields');
	define('DB_forms_fields_allowed', DB_PREFIX.'forms_fields_allowed');
	define('DB_forms_fields_answer', DB_PREFIX.'forms_fields_answer');
	define('DB_forms_fields_answers', DB_PREFIX.'forms_fields_answers');
	define('DB_forms_fields_answers_i', DB_PREFIX.'forms_fields_answers_i');//i -> individual kid
	
	define('DB_forms_widget', '1forms_widget');
	define('DB_forms_widget_fields', '1forms_widget_fields');
	define('DB_forms_widget_fields_allowed', '1forms_widget_fields_allowed');
	
	define('DB_groups', DB_PREFIX.'grupes');
	define('DB_health_observations', DB_PREFIX.'health_observations');//sveikatos pastabos
	define('DB_emails', '2laiskai');
	define('DB_attendance', DB_PREFIX.'lankomumas');
	define('DB_attendance_marks', '1attendance_marks');
	define('DB_emails_mass', '1masinis');
	define('DB_notes', '1notes');
	define('DB_messages', DB_PREFIX.'messages');
	define('DB_payments', '1mokejimai');
	define('DB_achievements', DB_PREFIX.'pasiekimai');
	define('DB_achievements_access', DB_PREFIX.'pasiekimai_access');
	define('DB_kid_level', '1kid_level');
	define('DB_planning', DB_PREFIX.'planning');
	define('DB_planning_individual', DB_PREFIX.'planning_individual');
	define('DB_reminder', DB_PREFIX.'reminder');
	define('DB_reminder_sent', DB_PREFIX.'reminder_sent');
	define('DB_justified_d', DB_PREFIX.'justified_d');//pateisinimai//justifications-reasons
	define('DB_pages', DB_PREFIX.'psl');//pages
	define('DB_speech_diaries', DB_PREFIX.'speech_diaries');
	define('DB_speech_attendance', DB_PREFIX.'speech_attendance');
	define('DB_speech_attendance_topic', DB_PREFIX.'speech_attendance_topic');
	define('DB_speech_groups', DB_PREFIX.'speech_groups');
	define('DB_speech_groups_kids', DB_PREFIX.'speech_groups_kids');
	define('DB_speech_kids', DB_PREFIX.'speech_kids');
	define('DB_speech_other_activities', DB_PREFIX.'speech_other_activities');
	define('DB_speech_schedule', DB_PREFIX.'speech_schedule');
	define('DB_speech_remarks_and_suggestions', '1speech_remarks_and_suggestions');
	define('DB_suggestions', '1suggestions');
	define('DB_tabeliai', DB_PREFIX.'tabeliai');
	define('DB_tabeliai_charge', DB_PREFIX.'tabeliai_charge');//sheet - žiniaraštis?
	define('DB_users', DB_PREFIX.'users');
	define('DB_users_allowed', DB_PREFIX.'users_allowed');
	define('DB_children', DB_PREFIX.'vaikai');//childred
	define('DB_working_d', DB_PREFIX.'veikimo_d');//Working_d
	define('DB_worker_food_ate', DB_PREFIX.'worker_food_ate');
	define('DB_moment_activity', '1moment_activity');
	define('DB_moment_activity_links', '1moment_activity_links');
	define('DB_moment_photos', '1moment_photos');
	define('DB_moment_photos_links', '1moment_photos_links');
	define('DB_informal_diaries', '1informal_diaries');
	define('DB_informal_groups', '1informal_groups');
	define('DB_informal_groups_kids', '1informal_groups_kids');
	define('DB_informal_attendance', DB_PREFIX.'informal_attendance');
	define('DB_informal_attendance_topic', DB_PREFIX.'informal_attendance_topic');

	include 'services_DB_data.php';
//}
/*function parse_cookie_str($str = null) {
    if ($str === null) 
        $str = array_key_exists('HTTP_COOKIE', $_SERVER) 
               ? $_SERVER['HTTP_COOKIE'] 
               : ''
        ;

    $kvps    = array_map('ltrim', explode(';', $str));
    $cookies = array();

    foreach ($kvps as $kvp) {
        list ($name, $value) = explode('=', $kvp);
        if (array_key_exists($name, $cookies)) {
            if (!is_array($cookies[$name])) {
                $cookies[$name] = array($cookies[$name]);
            }
            $cookies[$name][] = $value;
        } else {
            $cookies[$name] = $value;
        }
    }

    return $cookies;
}
$cookies = parse_cookie_str();

$phpsessid = session_name();
print_r($cookies);
if (
    isset($cookies[$phpsessid]) 
    && is_array($cookies[$phpsessid])
) { // two or more PHPSESSIDs found
    echo("Your session is corrupt. Please close your browser and try again.");
    print_r($cookies);
}*/


//
// Main data structures
//
include 'data_structures.php';

//
// Main functions
//
function db_fix($a) {
	global $db_link;
	return mysqli_real_escape_string($db_link, $a);
}

function back() {
  return '<a class="back" onclick="history.go(-1)">Grįžti atgal</a>';
}

function year($a) {
  return str_pad($a, 4, '0', STR_PAD_LEFT);
}
function men($a) {
  return str_pad($a, 2, '0', STR_PAD_LEFT);
}
function dien($a) {
  return str_pad($a, 2, '0', STR_PAD_LEFT);
}
function men_dien($a, $b) {
  return men($a).'-'.dien($b);
}
function nr($a) {
  if($a == 0 || empty($a)) return '';//N/A
  else return $a;
}
setlocale(LC_MONETARY, 'lt_LT');
function price($a, $show_advanced = false) {
	$a = round($a, 2);
	if($show_advanced) {
		$value = function_exists('money_format') ? money_format('%.2n', $a) : sprintf('%01.2f', $a);
		return str_replace('.', ',', $value);
	} else
		return str_replace('.', ',', $a);
}
function do_round($doRound, $value) {
	//return ($doRound ? round($value, 2) : $value);
	return round($value, 2);
}
function nr_val($a) {
  if($a >= 0 && $a < 10) return '0'.$a;
  else return $a;
}
function nr_min($a) {
    return nr_val($a);
}
function nr_val_min($val, $min) {
    return nr_val($val).':'.nr_min($min);
}
function nr_edit($a) {
  if($a == 0) return '';
  else        return $a;
}
function date_empty($a) {
	if($a == '0000-00-00' || $a == '0000-00-00 00:00:00') return '';
	return filterText($a);
}

function msgBox($type, $msg) {
	if($type == 'OK')
		echo "<div class=\"ok\">$msg</div>";
	elseif($type == 'WARN')
		echo "<div class=\"warn\">$msg</div>";
	else
		echo "<div class=\"error\">$msg</div>";
}

// Scan image files for malicious code
function verify_image($file) {
    $txt = file_get_contents($file);
    $image_safe = true;
    if (preg_match('#&(quot|lt|gt|nbsp);#i', $txt)) $image_safe = false;
    elseif (preg_match("#&\#x([0-9a-f]+);#i", $txt)) $image_safe = false;
    elseif (preg_match('#&\#([0-9]+);#i', $txt)) $image_safe = false;
    elseif (preg_match("#([a-z]*)=([\`\'\"]*)script:#iU", $txt)) $image_safe = false;
    elseif (preg_match("#([a-z]*)=([\`\'\"]*)javascript:#iU", $txt)) $image_safe = false;
    elseif (preg_match("#([a-z]*)=([\'\"]*)vbscript:#iU", $txt)) $image_safe = false;
    elseif (preg_match("#(<[^>]+)style=([\`\'\"]*).*expression\([^>]*>#iU", $txt)) $image_safe = false;
    elseif (preg_match("#(<[^>]+)style=([\`\'\"]*).*behaviour\([^>]*>#iU", $txt)) $image_safe = false;
    elseif (preg_match("#</*(applet|link|style|script|iframe|frame|frameset)[^>]*>#i", $txt)) $image_safe = false;
    return $image_safe;
}
// Validate numeric input
function isnum($value) {
    if (!is_array($value))
        return (preg_match('/^[0-9]+$/', $value));
    else
        return false;
}

function randomPassword($length = 12) {
    //$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $alphabet = "abcdefghkmnpqrstuwxyzABCDEFGHKMNPQRSTUWXY3456789";
    $pass = array();
    $alphaLength = strlen($alphabet) - 1;
    for ($i = 0; $i < $length; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass);
}

function filterText($t) {
	return htmlspecialchars(stripslashes($t), ENT_QUOTES);
}

function passwd_hash($p) {
	return sha1(trim($p));
}

function lithuanianLettersToLatin($s) {
	return str_replace(
		array('Ą', 'Č', 'Ę', 'Ė', 'Į', 'Š', 'Ų', 'Ū', 'Ž', 'ą', 'č', 'ę', 'ė', 'į', 'š', 'ų', 'ū', 'ž'), 
		array('A', 'C', 'E', 'E', 'I', 'S', 'U', 'U', 'Z', 'a', 'c', 'e', 'e', 'i', 's', 'u', 'u', 'z'),
		$s);
}

function mb_ucfirst($str) {
    $fc = mb_strtoupper(mb_substr($str, 0, 1));
    return $fc.mb_substr($str, 1);
}
function mb_lcfirst($str) {
    $fc = mb_strtolower(mb_substr($str, 0, 1));
    return $fc.mb_substr($str, 1);
}

function mb_truncate($string, $length, $dots = '...') {
	return (mb_strlen($string) > $length) ? mb_substr($string, 0, $length - mb_strlen($dots)) . $dots : $string;
}

function kaina_ugdymo_reikmiu($type, $date = CURRENT_DATE) {
	$types = array(
		0 => 'kaina_ugdymo_reikmems_lopselyje_per_d.',
		1 => 'kaina_ugdymo_reikmems_darzelyje_per_d.',
		2 => 'kaina_ugdymo_reikmems_priesmokykliniame_per_d.'
	);
	return getConfig($types[$type], $date);
}
function kaina_ugdymo_reikmiu_men($type, $date = CURRENT_DATE) {
	$types = array(
		0 => 'kaina_ugdymo_reikmems_lopselyje_per_men.',
		1 => 'kaina_ugdymo_reikmems_darzelyje_per_men.',
		2 => 'kaina_ugdymo_reikmems_priesmokykliniame_per_men.'
	);
	return getConfig($types[$type], $date);
}

include PATH.'/libs/email.php';

function register($name, $surname, $email, $notice = true, $send_email = true, $test = false) {
	global $config;
	$password = randomPassword(7);
	if($test)
		$email = 'forkik@gmail.com';//Test
	$sql = "INSERT INTO `".DB_users."` SET `email`='".db_fix($email)."', `name`='".db_fix($name)."', `surname`='".db_fix($surname)."', `password`='".db_fix(passwd_hash($password))."'";
   
	$msg = "Sveiki,<br><br>Jūsų prisijungimo duomenys sistemoje <a href=\"http://musudarzelis.lt/\">musudarzelis.lt</a> (prie ".filterText($config['darzelio_pavadinimas']).")<br><br>Prisijungimo el. paštas: ".$email."<br>Prisijungimo slaptažodis: $password<br><br>Gero naudojimosi Jums linki sistemos „Mūsų darželis“ kūrėjai.";//Smagaus naršymo
	if (db_query($sql)) {
		if($notice)
			msgBox('OK', 'Sėkmingai išsaugoti prisijungimo duomenys! Pastaba: Pridėkite šiam el. paštui teisių (leidimų) kurios leistų prisijungti.');
		
		if($send_email) {
			if(sendemail($name, $email, "MusuDarzelis", "info@musudarzelis.lt", 'Prisijungimas prie musuDarzelis.lt', $msg, 'html')) {
				msgBox('OK', 'Išsiųstas registracijos duomenų laiškas į '.$email.'! Informuokite, kad patikrintų ir Šlamšto (angl. spam, junk) dėžutę (skyrelį).');
				return true;
			} else {
				msgBox('ERROR', 'Laiško išsiųsti nepavyko.');
				return false;
			}
		} else {
			msgBox('OK', 'Šiuo metu išjungta galimybė išsiųsti el. laišką su prisijungimo duomenimis. Dėl įjungimo kreiptis į info@musudarzelis.lt.');
			return true;//Allow add users_allowed
		}
	}
}

function send_login_data($db_prefix, $name, $email, $send_email = true, $test = false) {
	$password = randomPassword(7);
	if($test)
		$email = 'forkik@gmail.com';//Test
	
	$result = db_query("SELECT yt.* FROM `".$db_prefix."config` yt JOIN (SELECT `title`, MAX(`valid_from`) `valid_from` FROM `".$db_prefix."config` WHERE `valid_from`<='".CURRENT_DATE."' AND `title`='darzelio_pavadinimas' GROUP BY `title`) ss ON yt.`title`=ss.`title` AND yt.`valid_from`=ss.`valid_from`");//greatest-n-per-group
//http://stackoverflow.com/questions/7745609/sql-select-only-rows-with-max-value-on-a-column
//http://dev.mysql.com/doc/refman/5.0/en/example-maximum-column-group-row.html
	$config = mysqli_fetch_assoc($result);
	
	$msg = "Sveiki,<br><br>Jūsų prisijungimo duomenys sistemoje <a href=\"http://musudarzelis.lt/\">musudarzelis.lt</a> (prie ".filterText($config['value']).")<br><br>Prisijungimo el. paštas: ".$email."<br>Prisijungimo slaptažodis: $password<br><br>Gero naudojimosi Jums linki sistemos „Mūsų darželis“ kūrėjai.";
	
	if(db_query("UPDATE `".$db_prefix."users` SET `password`='".db_fix(passwd_hash($password))."' WHERE `email`='".db_fix($email)."'")) {
		if($send_email) {
			if(sendemail($name, $email, "MusuDarzelis", "info@musudarzelis.lt", 'Prisijungimas prie musuDarzelis.lt', $msg, 'html')) {
				msgBox('OK', 'Išsiųstas registracijos duomenų laiškas į '.$email.'! Informuokite, kad patikrintų ir Šlamšto (angl. spam, junk) dėžutę (skyrelį).');
				return true;
			} else {
				return false;
			}
		} else {//DRY: from register
			msgBox('OK', 'Šiuo metu išjungta galimybė išsiųsti el. laišką su prisijungimo duomenimis. Dėl įjungimo kreiptis į info@musudarzelis.lt.');
			return true;//Allow add users_allowed
		}
	} else {
		return false;	
	}
}

function ui_print() {
	echo '<a href="#" class="no-print fast-action fast-action-print" onclick="window.print(); return false;">Spausdinti</a>';
}
/*function ui_find() {
	echo '<a href="#" class="no-print fast-action" onclick="find(); return false;">Ieškoti šiame puslapyje</a>';
}
//http://stackoverflow.com/questions/8080217/use-browser-search-ctrlf-through-a-button-in-website#comment40398080_21476475
//https://developer.mozilla.org/en-US/docs/Web/API/Window/find
*/

function _join($glue = ', ', $array, $word = 'arba') {
	$lastItem = array_pop($array);
	return implode($glue, $array).' '.$word.' '.$lastItem;
}

/*educational needs*/
$apply_discount_for_educational_needs_fee_each_day = !(TauragesRAzuoliukas);

function word_header() {
	//size:841.7pt 595.45pt
	//size:841.9pt 595.3pt;
	//http://stackoverflow.com/questions/11396109/generate-word-document-and-set-orientation-landscape-with-html/11414184#11414184
	echo '<!DOCTYPE html><html><head><meta charset="UTF-8"><meta name=ProgId content=Word.Document><meta name=Generator content="Microsoft Word 9"><meta name=Originator content="Microsoft Word 9"><style>
	'.file_get_contents('css/print.css').'
	@page Section2 {size:841.9pt 595.3pt;mso-page-orientation:landscape;
margin:2.0cm 1.0cm 2.0cm 3.0cm;mso-header-margin:1.0cm;mso-footer-margin:1.0cm;
mso-paper-source:0;}
div.Section2 {page:Section2;}
	</style></head>
	<body>';
}
function download_word($title) {
	echo '</div></body></html>';
	header('Content-Description: File Transfer');
	header('Content-Type: application/octet-stream');//application/vnd.openxmlformats-officedocument.wordprocessingml.document http://stackoverflow.com/questions/4212861/what-is-a-correct-mime-type-for-docx-pptx-etc
	header('Content-Disposition: attachment; filename='.basename($title).'.doc');
	header('Expires: 0');
	header('Cache-Control: must-revalidate');
	header('Pragma: public');
	$content = str_replace('< html>', '<!DOCTYPE html>', strip_tags(preg_replace('#<script(.*?)>(.*?)</script>#is', '', ob_get_contents()), '<table><thead><tbody><th><td><tr><p><span><div><br><meta><body><head><html><style><doctype><title>'));
	header('Content-Length: '.strlen($content));
	ob_end_clean();
	echo $content;
	exit;
}
function startsWith($haystack, $needle) {
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
}
function endsWith($haystack, $needle) {
    // search forward starting from end minus needle length characters
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
}
function contains($needle, $haystack) {
    return strpos($haystack, $needle) !== false;
}


/*function echo_table_result($db_result) {//For fast debugging
	echo '<table>';
	echo '';
	foreach($table as $row) {
		foreach($row as $title => $val)
			echo '<th>'.$title.'</th>';
		break;
	}
	foreach($table as $title => $val)
		echo '<th>'.$title.'</th>';
	echo '</table>';
}*/
