<?php
include 'main.php';
$jslog_input = json_decode($HTTP_RAW_POST_DATA, true);
if (is_array($jslog_input) && array_key_exists('lg', $jslog_input)) {
    foreach($jslog_input['lg'] as $msgItem) {
        loga('l: '.$msgItem['l']/*level*/.', m: '.$msgItem['m']/*message*/.', n: '.$msgItem['n']/*logger name*/.', t: '.$msgItem['t']/*timestamp*/, 'js');
    }
}
