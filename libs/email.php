<?php if(!defined('DARBUOT')) exit();
function sendemail($toname, $toemail, $fromname, $fromemail, $subject, $message, $type = "plain", $cc = "", $bcc = "", $path = '', $reply_to_email = '', $reply_to_name = '') {
	require_once 'PHPMailer/class.phpmailer.php';

	$mail = new PHPMailer();
	$mail->SetLanguage("lt");

	$mail->IsMAIL();

	$mail->CharSet = 'UTF-8';
	$mail->From = $fromemail;
	$mail->FromName = $fromname;
	$mail->AddAddress($toemail, $toname);
	$mail->AddReplyTo($fromemail, $fromname);
	if(!empty($reply_to_email)) {
		$mail->AddReplyTo($reply_to_email, $reply_to_name);
	}
	if ($cc) {
		$cc = explode(", ", $cc);
		foreach ($cc as $ccaddress) {
			$mail->AddCC($ccaddress);
		}
	}
	if ($bcc) {
		$bcc = explode(", ", $bcc);
		foreach ($bcc as $bccaddress) {
			$mail->AddBCC($bccaddress);
		}
	}
	if ($type == "plain") {
		$mail->IsHTML(false);
	} else {
		$mail->IsHTML(true);
	}

		if(!empty($path)) {
			if(!is_array($path))
				$mail->AddAttachment($path);//application/pdf
			else
				foreach($path as $p)
					$mail->AddAttachment($p);
		}
		
	$mail->Subject = $subject;
	$mail->Body = $message;
	
	//if($fromemail == 'info@musudarzelis.lt') {
		//$mail->Encoding = "base64";
		$mail->DKIM_domain = 'musudarzelis.lt';
		$mail->DKIM_private = './private/etc/dkim_private.key';//$privatekeyfile;
		$mail->DKIM_selector = 'phpmailer';
		$mail->DKIM_passphrase = ''; //key is not encrypted
		//$mail->DKIM_identity = $mail->From;
    //}
    //http://www.openspf.org/SPF_Record_Syntax
    //albinas v=spf1 mx a +all
    //md.lt v=spf1 mx a ~all
    //v=spf1 mx a include:zcsend.net

	if(!$mail->Send()) {
		$mail->ErrorInfo;
		$mail->ClearAllRecipients();
		$mail->ClearReplyTos();
		return false;
	} else {
		$mail->ClearAllRecipients();
		$mail->ClearReplyTos();
		return true;
	}
}

