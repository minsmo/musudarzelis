﻿<?php
//error_reporting(E_ALL | E_STRICT);
//ini_set('display_errors', '1');
//ini_set('log_errors', '1');
//mb_internal_encoding('UTF-8');
/*	  
Microsoft Office XML ir Office Open XML yra du skirtingi formatai.

Documentation of "HTML -> Excel XML Spreadsheet 2003":
<tr> -> <Row> //tr - table row
<th> -> <Cell> //th - table header (cell)
<td> -> <Cell> //td - table data (cell)
<table> -> <Table>
<br /> -> &#10;

colspan -> ss:MergeAcross PLIUS HTML colspan value = in Excel ([HTML colspan value] - 1)

rowspan -> ???  PLIUS HTML rowspan value = in Excel ([HTML rowspan value] - 1)
rowspan (būtina nurodyti ss:MergeDown ir ss:Index):
   <Row ss:AutoFitHeight="0">
	<Cell ss:MergeDown="1" ss:StyleID="m295191908"><Data ss:Type="String">Įstaiga</Data></Cell>
	<Cell ss:StyleID="s62"><Data ss:Type="String">Pavyzdinis darželis</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0">
	<Cell ss:Index="2" ss:StyleID="s63"><Data ss:Type="String">Pavardė</Data></Cell>
	<Cell ss:StyleID="s63"><Data ss:Type="String">Prisijungimų skaičius</Data></Cell>
   </Row>

Minėjai, kad naujasnio Excel XML'as gali būti kažkoks kitoks. Kaip patestavai, nes gal pabandytume sugrūsti šitą XML'ą. Bet nežinau ar tai dar geriausias variantas, nes dar neišsiaiškinau tiksliai
į kokį formatą Tu surašai ir kaip jis atrodo. Ar galėtum aprašyti dėl rowspan ir colspan kovertavimų apibrėžimą su xlsx?
Kaip bandžiau su XLSX tai pats exelis merge iškelia atskirai. Įdomu būtų Tavo pabandymai, bet prieš tai dar galim pabandyti pagooglinti.

Useful URLs:
http://officeopenxml.com/WPtableGrid.php - WORD 2007 Tables merge
https://msdn.microsoft.com/en-us/library/aa140066(office.10).aspx#odc_xmlss_b - Excel 2002 http://stackoverflow.com/questions/10930803/microsoft-office-xml-format-schema-references

Office Open XML:
	https://en.wikipedia.org/wiki/Office_Open_XML
	https://en.wikipedia.org/wiki/Office_Open_XML_file_formats - container overview explainer
	http://standards.iso.org/ittf/PubliclyAvailableStandards/index.html
	https://msdn.microsoft.com/en-us/library/office/gg278316.aspx - apačioje hierarchinis paveiksliukas
	http://officeopenxml.com/SScontentOverview.php - bendra struktūra gerai aiškinama
	http://blogs.msdn.com/b/brian_jones/archive/2006/11/02/simple-spreadsheetml-file-part-1-of-3.aspx - neiaiškinau, gal gera nuoroda.

Microsoft Office XML:
	SpreadsheetML is the XML schema for Microsoft Office Excel 2003.
	https://en.wikipedia.org/wiki/Microsoft_Office_XML_formats#Excel_XML_Spreadsheet_example - .XML 

http://books.evc-cit.info/odbook/ch05.html#merged-spreadsheet-cells-section OpenDocument is like HTML rowspan and cellspan from the structure point of view.


OpenDocument SPreadSheet'as irgi iš esmės nėra blogai. Bet jeigu viskas gerai bus dėl "Office Open XML" tai irgi gerai.

ŠIAIP naudinga:
Gera biblioteka http://spreadsheetlight.com/
*/
//if(!isset($_GET['save']))
//	echo'<a href="compiler2.php?save">Save as XLSX</a><hr>';


/*Applied on:
<a href=test.html class=xyz>
<a href="test.html" class="xyz">
<a href='test.html' class="xyz">
it would yield:
Attribute name => attribute value (NOTICE: don't parse what is inside)
'href' => 'test.html'
'class' => 'xyz'*/
/*
?>
<style>
table{
	border: 1px solid black;
	border-spacing: 0;
	margin: 0;
	border-bottom: none;
	border-right: none;
}
td,th{
	border: 1px solid black;
	border-top: none;
	border-left: none;
}
</style>
<?php
$file = file_get_contents("trumpiausias/TestinisKaunolopselisdarzelis.html");
*/

function num2alpha($n) {
    for($r = ''; $n >= 0; $n = intval($n / 26) - 1)
        $r = chr($n%26 + 65) . $r;
    return $r;
}
//$tablecol = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U"];//TODO: fix to inclune infinite amount of values (function)

function excel($file /*content*/, $filename) {

$file =  str_replace('&nbsp;', ' ', $file);

//Remove all comments
$file = preg_replace('/<!--(.*)-->/Uis', '', $file);

//Extract first table
$file = explode("<table", $file);
$file = '<table'.$file[1];
//$file = '<table'.explode("<table", $file)[1];//Kuris variantas lengviau Tau suprantamas viena eilutė ar dvi? Lenviau suprantamas pirmas, bet šitas gražesnis

$file = explode("</table>", $file);
$file = $file[0].'</table>';

//Remove all tags which have CSS class no-print
$file = preg_replace('/<th([^>]*?)no-print(.*)\/th>/Uis', '', $file);
$file = preg_replace('/<td([^>]*?)no-print(.*)\/td>/Uis', '', $file);
$file = preg_replace('/<tr([^>]*?)no-print(.*)\/tr>/Uis', '',$file);
$file = preg_replace('/<([^>]*?)no-print(.*)\/(.*)>/Uis', '', $file);


$file = str_replace(["\n", "\t"], '', $file);//Čia man toks vaizdas, kad antras parametras galėtų būti tik '' vietoje ['','',''], bet reikėtų testuoti ar taip veiktų.
//Kodėl <br> pašalini? Tekstiniame faile buvo įrašyta į ką keisti reikėtų. Tą failą aš Tau siunčiau ar pavyktų jį surasti ir 
$file = str_replace(["<br />", "<br/>", "<br>"], '&#10;', $file);//Manau, kad šitaip turėtų būti. Ar sutinki? manau vietoj ' ' turėtų būti " ", kodėl? Tiksliai nepamenu bet lyg ir nebveikdabvo tie kodai s. Su ' kabutėmis neveikia kodai žymimi \ pvz.: \n, \t. Tik aš nesu tikras dėl ar reikia [] skliaustuose krtoti tris kartus ar užtenka vieną kartą. Ar galim patestuoti šitą kodą per naršyklę pasileidžiant šitą kodą dabar? wAMP vėl neveikia :/
//Hmz... tada vėlesniam laikui galim kol kas atidėti jei ilgai užtruktų. Ką manai? Šį kartą nepavyksta sutvarkyt. Tokią pačią klaidą meta? Taip.. Aš bandžiau googlinti pagal klaidos pranešimą, stackoverflow mačiau bent tris skirtingus variantus kas gali būti. Ar bandei pagooglinti tokiu būdu? Bandžiau visus tris būdus ar kiek jų ten buvo, tik nebandžiau dar port'o pakeitimo. Jei ką galiu pabandyti ir aš pažiūrėti, nes kartais smulkmeną kokią nors galima pražiūrėti. Kol kas dar peržiūriu kodą ir žiūrėsim ką daryti toliau.



//------------
//manau perdarysiu truputį kitai, geriau naudoti standartinius PHP daiktus.
//p.s. pasidariau full screen pas save
$xml = simplexml_load_string(strip_tags($file, "<table><tr><td><th>"));//Jeigu tag'ai kaip tbody nereikalingi strip_tags galim praleisti
//API:
	//['atributo-vardas']
	//->attributes() jei reikia atributų sąrašo
	//->tagas
	//->tagas[1] //kelintas
//Ar dabar mokėtum elgtis su simplexml_load_string? kažkiek
//Manau žymiai palengvėtų kodo skaitymas kai ne Tu pats parsintum, nors ir tai rekomenduočiau stack'ą naudoti ir laukti kitos ateinančios žymės, bet tada steck'o duomenų struktūrą reikėtų naudoti iš PHP ir vis tiek manau paprasčiau (ypač kodo skaitomumo-suprantamumo prasme) kai kokį nors standartinis simplexml_load_string naudojamas.

//echo '<pre>';
//print_r($xml);


//------------

/*
1D:
row
2D:
column
3D:
0 - value
1 - rowspan
2 - colspan
3 - width
4 - border
5 - address
6 - isMerged
*/
$tbl = [];
/*
$tbl[$row][col][X]
X:
value
rowSpan
colspan
width
border
address
//reference į pirminį langelį suliejimo. 
isMergedNo //not isset or 0 - no merge, 1 - first cell with content that is merged, 2..* - fallowing empty merged cells

lentelė  bendra
width
merge
*/

$table = [];
$rowNo = 0;
//echo $file.'<hr />';
//lentelė suskaldoma į masyvą:

//Man čia toks vaizdas, kad galima būtų parašyti paprasčiau. Gal galėtum aprašyti ko sieki kažkuriom kodo pastraipom, kad nereikėtų labai daug gilintis į Tavo kodą ir greičiau galėčiau pagalvoti ar išeitų kažkaip paprasčiau parašyti. Ką manai?

//čia viskas verčiama į masyva, nes su juo žymiai lengviau dirbti, jeigu tik tai verčiama į masyvą tai man atrodo, kad geriau tegul funkcija  simplexml_load_string paverčia į masyvą.
//Tik problema, kai kai paverčia baisiai sunku iššsiaiškinti kas kaip, o kai aš pasidarau, tai tas masyvas yra grynai ta lentelė
//Tada reikėtų bandyti ir žiūrėti kur tiksliai sunkumas ir kaip galima būtų to išvengti (būtų gerai pas Tave tą Wamp server sutvarkyti). O Tavo kodo dar pilnai neišsiaiškinau ir man atrodo dar jį būtų galima patobulinti.

//$table[$rowNo][$colNo] = "Cell address,cell content"; Merge if there are more than one the same cell address.
foreach($xml->tr as $row) {
	$colNo = 0;
	foreach($row->children() as $cell) {
		while(isset($table[$rowNo][$colNo])) {//CHECK: Man panašu, kad šitas dėl rowspan, nes tuomet kai ateini į naują eilutę būna nunulintas colNo
			++$colNo;
		}
		@$table[$rowNo][$colNo] = num2alpha($colNo).($rowNo+1).','.strip_tags($cell);
		$tbl[$rowNo][$colNo]['address'] = num2alpha($colNo).($rowNo+1);
		$tbl[$rowNo][$colNo]['value'] = strip_tags($cell);
		$ocell = $colNo;
		foreach($cell->attributes() as $name => $value) {
			if($name == 'colspan') {
				$tbl[$rowNo][$colNo]['colspan'] = (int)$value;
				$tbl[$rowNo][$colNo]['isMergedNo'] = 1;
				//TODO: merge calculation???
				for($j = 1; $j < (int)$value; ++$j) {
					++$colNo;
					@$table[$rowNo][$colNo] = num2alpha($ocell).($rowNo+1).',';
					$tbl[$rowNo][$colNo]['address'] = num2alpha($colNo).($rowNo+1);
					$tbl[$rowNo][$colNo]['isMergedNo'] = 2;
				}
			} elseif($name == 'rowspan') {
				$tbl[$rowNo][$colNo]['rowspan'] = (int)$value;
				$tbl[$rowNo][$colNo]['isMergedNo'] = 1;
				for($j = 1; $j < (int)$value; ++$j) {
					@$table[$rowNo+$j][$colNo] = num2alpha($colNo).($rowNo+$j).',';
					$tbl[$rowNo+$j][$colNo]['address'] = num2alpha($colNo).($rowNo+$j+1);
					$tbl[$rowNo+$j][$colNo]['isMergedNo'] = 2;
				}
			}
		}
		$colNo++;
	}
	$rowNo++;
}
$colMax = [];
for($c = 0; $c < count($tbl[0]); $c++) {		
	$max = 0;
	for($r = 0; $r < count($tbl); $r++) {
		if(isset($tbl[$r][$c]['value']) && (!isset($tbl[$r][$c]['isMergedNo']) || isset($tbl[$r][$c]['rowspan']) )){
			$len = max(array_map('mb_strlen', explode("\n", $tbl[$r][$c]['value'])));
			$tbl[$r][$c]['width'] = $len;
			if($max < $len){
				$max = $len;
			}
		}
	}
	$colMax[$c] = $max;
}

for($c = 0; $c < count($tbl[0]); $c++) {		
	$max = 0;
	for($r = 0; $r < count($tbl); $r++) {
		if(isset($tbl[$r][$c]['value']) && isset($tbl[$r][$c]['colspan']) && isset($tbl[$r][$c]['isMergedNo']) && $tbl[$r][$c]['isMergedNo']  == 1 /* optional */){
			//$tbl[$r][$c]['colspan']

			$sum = 0;
			
			for($i = 1; $i < $tbl[$r][$c]['colspan']; $i++){
				if(isset($colMax[$c+$i]))
					$sum += $colMax[$c+$i];
			}
			$tmp = max(array_map('mb_strlen', explode("\n", $tbl[$r][$c]['value'])))-$sum;
			$tbl[$r][$c]['width'] = $colMax[$c] > $tmp ? $colMax[$c] : $tmp;
		}
	}
}

$colMax = [];
for($c = 0; $c < count($tbl[0]); $c++) {		
	$max = 0;
	for($r = 0; $r < count($tbl); $r++) {
		if(isset($tbl[$r][$c]['width'])){
			$len = $tbl[$r][$c]['width'];
			if($max < $len){
				$max = $len;
			}
		}
	}
	$colMax[$c] = $max;
}


/*
echo'<table>';
foreach($tbl as $row){
	echo'<tr>';
	foreach($row as $col){
		echo'<td>';
		foreach($col as $key => $value){
			echo $key.': '.$value.'<br />';
		}
		echo'</td>';
	}
	echo'</tr>';
}
echo'</table>';
*/

/*
							[0] => Neatvyko:
							[1] => 1
							[2] => SimpleXMLElement Object([@attributes] => Array(
											[colspan] => 10
											[class] => 
									   )
								)
GOOD:
			[0] => A6,Neatvyko:
			[1] => A6,
			[2] => A6,
			[3] => D6,1
			[4] => E6,
			[5] => E6,
			[6] => E6,
			[7] => E6,
			[8] => E6,
			[9] => E6,
			[10] => E6,
			[11] => E6,
			[12] => E6,
			[13] => E6,
*/
/*
function extractAttributes($str) {
	preg_match_all("/(\S+)=[\"']?((?:.(?![\"']?\s+(?:\S+)=|[>\"']))+.)[\"']?/i", $str, $match);//http://stackoverflow.com/questions/317053/regular-expression-for-extracting-tag-attributes
	//preg_match_all("/(?:[^<]|<[^!]|<![^-\[]|<!\[(?!CDATA)|<!\[CDATA\[.*?\]\]>|<!--(?:[^-]|-[^-])*-->)/i", $str, $match);//http://stackoverflow.com/questions/317053/regular-expression-for-extracting-tag-attributes
	return $match;
}
$rows = explode('<tr', $file);
foreach($rows as $row) {
	if(preg_match("/tr>/i", $row)) {
		$cellNo = 0;
		$row = '<tr'.$row;
		if(preg_match("/<th/i", $row)) {//TODO low priority: pasižiūrėti ar įmanoma išvengti if'o sutrumpinant kodą ir išvengiant kodo kartojimosi.
			$cells = array_slice(explode("<th", $row), 1);//Mano galva būtų geriausia regex'ą pasirašyti tvarkingą
			foreach($cells as $cell) {
				$cell = '<th'.$cell;
				while(@$table[$rowNo][$cellNo] != '') {//Man neaišku ką šitas ciklas turi suskaičiuoti, iš pradžių galvojau, kad kiek celių nuo eilutės pradžios. Jei gerai pamenu tai čia dėl colspan
					$cellNo++;
				}
				//B - Bold.
				@$table[$rowNo][$cellNo] = num2alpha($cellNo).($rowNo+1).','.strip_tags($cell);
				$attributes = extractAttributes($cell);
				//if(!empty($attributes)) {
				//	echo '<pre>';
				//	print_r($attributes);
				//	echo htmlspecialchars($cell);
				//	die();//Paleisk
				//}
				$ocell = $cellNo;
				//Ar taip neveiktų? Nebandžiau :) Reikėtų dar pasitikrinti ar DOLERIScell yra tikrai vienas td, tuomet sumažėtų kodo.
				//Tuomet atsiųsk kodą.
				/*
				if(isset($attributes['colspan'])) {
					$attributes[2][$i] = str_replace(array('"',"'"),'',$attributes[2][$i]);
					for($j=1; $j<intval($attributes[2][$i]); $j++) {
						@$table[$rowNo][++$cellNo] .= num2alpha($ocell).''.($rowNo+1).',B,';
					}
				}elseif(isset($attributes['rowspan'])) {
					$attributes[2][$i] = str_replace(array('"',"'"),'',$attributes[2][$i]);
					for($j=1; $j < intval($attributes[2][$i]); $j++) {
						@$table[$rowNo+$j][$cellNo] .= num2alpha($cellNo).''.($rowNo+$j).',B,';
					}
				}/
				for($i = 0; $i < count($attributes[1]); $i++) {
					if($attributes[1][$i] == "colspan") {//Kadangi grąžina mums raktus kaip "attributes names" tai mes galim ieškoti kaip rakto
						$attributes[2][$i] = str_replace(['"',"'"], '', $attributes[2][$i]);
						for($j=1; $j<intval($attributes[2][$i]); $j++) {
							@$table[$rowNo][++$cellNo] = num2alpha($ocell).($rowNo+1).',';
						}
					}elseif($attributes[1][$i] == "rowspan") {
						$attributes[2][$i] = str_replace(['"',"'"], '', $attributes[2][$i]);
						for($j=1; $j < intval($attributes[2][$i]); $j++) {
							@$table[$rowNo+$j][$cellNo] = num2alpha($cellNo).($rowNo+$j).',';
						}
					}
				}
				$cellNo++;
			}
		}elseif(preg_match("/<td/i",$row)) {
			$cells = array_slice(explode("<td", $row), 1);
			foreach($cells as $cell) {
				$cell = '<td'.$cell;
				while(@$table[$rowNo][$cellNo] != '') {
					$cellNo++;
				}
				@$table[$rowNo][$cellNo] = num2alpha($cellNo).''.($rowNo+1).','.strip_tags($cell);
				$attributes = extractAttributes($cell);
				$ocell = $cellNo;
				for($i = 0; $i < count($attributes[1]); $i++) {
					if($attributes[1][$i] == "colspan") {
						$attributes[2][$i] = str_replace(['"',"'"], '', $attributes[2][$i]);
						for($j=1; $j < intval($attributes[2][$i]); $j++) {
							@$table[$rowNo][++$cellNo] = num2alpha($ocell).($rowNo+1).',';
						}
					}elseif($attributes[1][$i] == "rowspan") {
						$attributes[2][$i] = str_replace(['"',"'"], '', $attributes[2][$i]);
						for($j=1; $j < intval($attributes[2][$i]); $j++) {
							@$table[$rowNo+$j][$cellNo] = num2alpha($cellNo).($rowNo+$j).',';
						}
					}
				}
				$cellNo++;
			}
		}
		$rowNo++;
	}
}*/


//Code is used to generate part of "SharedStringTable".
/*
$cellnumber = 0;
$rownumber = 0;
ksort($table, SORT_NUMERIC);
$r = 0;//row
$strings = -1;
$stringlist = [];
foreach($table as $row) {
	$r++;
	if($r > $rownumber) $rownumber = $r;
	$c = 0;
	ksort($row, SORT_NUMERIC);
	foreach($row as $col => $cell) {
		$c++;
		if($c > $cellnumber)
			$cellnumber = $c;
		if(num2alpha($col).$r == explode(',',$cell)[0]) {
			if(substr($cell,strlen(explode(',',$cell)[0])+1) != '')//!if(empty($cell['value']))
				$stringlist[++$strings] = substr($cell,strlen(explode(',',$cell)[0])+1);//$cell['value']
		}
	}
}
*/
//END Code is used to generate part of "SharedStringTable".

//echo '<pre>';
//print_r($table);

//<col min="1" max="1" width="2.5703125" bestFit="1" customWidth="1" />
$sheet = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<worksheet xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" mc:Ignorable="x14ac" xmlns:x14ac="http://schemas.microsoft.com/office/spreadsheetml/2009/9/ac">';
	//<dimension ref="A1:'.num2alpha(count($tbl[0])).''.count($tbl).'" />
	$sheet.='<sheetViews>
		<sheetView tabSelected="1" workbookViewId="0">
			<selection activeCell="A1" sqref="A1" />
		</sheetView>
	</sheetViews>';
	
	$sheet .= '<sheetFormatPr defaultRowHeight="15" x14ac:dyDescent="0.25" />
	<cols>';
	for($c = 0; $c < count($table[0]); $c++) {		
		/*$max = 0;
		for($r = 0; $r < count($table); $r++) {
			//array_map("cube", $a);
			$len = max(array_map('mb_strlen', explode("\n", isset($tbl[$r][$c]['value']) ? $tbl[$r][$c]['value'] : '')));//&#10;
			//echo explode(',',$table[$r][$c])[1]."\n";//&#10;
			//$len = mb_strlen(explode(',', $table[$r][$c])[1]);
			if($len > $max) {
				$max = $len;
			}
		}*/
		$sheet .= '<col min="'.($c+1).'" max="'.($c+1).'" width="'.($colMax[$c]*(1)+0.875).'"  bestFit="1" customWidth="1" />';//1.28515625
		//echo $max.' ';
	}
	$sheet .= '</cols>';
	$sheet .= '<sheetData>
	';
	$r=0;
	$merge = array();
	$strings = -1;
	foreach($table as $row) {
		ksort($row, SORT_NUMERIC);
		$r++;
		$sheet .= '<row>';
		foreach($row as $key => $cell) {
			if(num2alpha($key).$r != explode(',',$cell)[0]) {
				$merge[explode(',', $cell)[0]] = num2alpha($key).$r;
				$sheet .= '
				<c r="'.num2alpha($key).''.$r.'" />';
			} else {
				
				/*
					Styling:
						used as: s="x"
						x beeing one of bellow:
						
					0 - Normal
					1 - Bold
					2 - Italic
					3 - Underline
					4 - Bold Underline
					5 - Italic Underline
					6 - Italic Bold
					7 - Italic Bold Underline
					
					8 - Normal Centered
					9 - Bold Centered
					10 - Italic Centered
					11 - Underline Centered
					12 - Bold Underline Centered
					13 - Italic Underline Centered
					14 - Italic Bold Centered
					15 - Italic Bold Underline Centered
					
					16 - Normal Right
					17 - Bold Right
					18 - Italic Right
					19 - Underline Right
					20 - Bold Underline Right
					21 - Italic Underline Right
					22 - Italic Bold Right
					23 - Italic Bold Underline Right
					
					24 - Normal Left
					25 - Bold Left
					26 - Italic Left
					27 - Underline Left
					28 - Bold Underline Left
					29 - Italic Underline Left
					30 - Italic Bold Left
					31 - Italic Bold Underline Left
					
					32 - % Normal (number as per-cent) // as per-cent is neither a number nor a text it has to be fixed with styling
					33 - % Bold
					34 - % Italic
					35 - % Underline
					36 - % Bold Underline
					37 - % Italic Underline
					38 - % Italic Bold
					39 - % Italic Bold Underline
					
					40 - % Normal Centered
					41 - % Bold Centered
					42 - % Italic Centered
					43 - % Underline Centered
					44 - % Bold Underline Centered
					45 - % Italic Underline Centered
					46 - % Italic Bold Centered
					47 - % Italic Bold Underline Centered
					
					48 - % Normal Right
					49 - % Bold Right
					50 - % Italic Right
					51 - % Underline Right
					52 - % Bold Underline Right
					53 - % Italic Underline Right
					54 - % Italic Bold Right
					55 - % Italic Bold Underline Right
					
					56 - % Normal Left
					57 - % Bold Left
					58 - % Italic Left
					59 - % Underline Left
					60 - % Bold Underline Left
					61 - % Italic Underline Left
					62 - % Italic Bold Left
					63 - % Italic Bold Underline Left
					
					64 - Normal Bordered
					
					
				*/
				//Man atrodo, kad Tau geriau duomenų struktūrą geresnę pasidaryti, nes tokie tikrinimai yra žymiai sudėtingesni negu isset(), empty() ar count() pasižiūrėjimas masyve. Kartais net kelis kintamuosius gali pasidaryti su skirtingomis duomenų struktūromis.
				
				if(substr($cell, strlen(explode(',',$cell)[0])+1) != '') {//Ką čia tikrini? Būtų gerai jei taip prisiminčiau :D Ar turinys netuščias t.y. !empty($cell['value']) va kažkas tokio man atrodo, ka čia turėtų būti parašyti vietoje viso substr($cell, strlen(explode(',',$cell)[0])+1) != ''. ir kad tai veiktų reikėtų padaryti, tuoj galiu parodyti kaip, bet pirma ar dabartinis variantas excel sudarymo veikia? Veikia. Pabandyk dabar
					if(preg_match("/(([[0-9]+?[,|.][0-9]+?)|[0-9]+?)(?![A-Za-z])/Uis", substr($cell,strlen(explode(',',$cell)[0])+1)) && !preg_match("/[a-zA-Z#]+?/Uis", substr($cell,strlen(explode(',',$cell)[0])+1))){
						if(preg_match("/[0-9 ]+?%/i", substr($cell,strlen(explode(',',$cell)[0])+1))){
							$sheet .= '
							<c r="'.num2alpha($key).''.$r.'" t="n" s="32">
								<v>'.intVal(substr($cell,strlen(explode(',',$cell)[0])+1))/100 .'</v>
							</c>';
						}else{
							$sheet .= '
							<c r="'.num2alpha($key).''.$r.'" t="n" s="64">
								<v>'.str_replace(",", ".", substr($cell,strlen(explode(',',$cell)[0])+1)).'</v>
							</c>';
						}
					}else{
						$sheet .= '
						<c r="'.num2alpha($key).''.$r.'" t="inlineStr" s="64">
							<is>
								<t>'.substr($cell,strlen(explode(',',$cell)[0])+1).'</t>
							</is>
						</c>';
					}/*
<c t[ype of content]="
	b for boolean
	d for date
	e for error
	inlineStr for an inline string (i.e., not stored in the shared strings part, but directly in the cell)
	n for number
	s for shared string (so stored in the shared strings part and not in the cell)
	str for a formula (a string representing the formula)">
When a cell is a number, then the value is stored in the <v> element as a child of <c> (the cell element).
For inline strings, the value is within an <is> element. But of course the actual text is further nested within a t since the text can be formatted.
<c r="C4" s="2" t="inlineStr">
	<is>
		<t>my string</t>
	</is>
</c>
http://officeopenxml.com/SScontentOverview.php
Common sense - v - value
*/
				} else {
					$sheet .= '
					<c r="'.num2alpha($key).''.$r.'" />';
				}
			}
		}
		$sheet .= '</row>
		';
	}
	$sheet .= '</sheetData>
	';
	if(count($merge) > 0){
		$sheet .= '<mergeCells count="'.count($merge).'">';
		foreach($merge as $key => $value) {
			$sheet .= '
			<mergeCell ref="'.$key.':'.$value.'" />';
		}
		$sheet .= '
		</mergeCells>';
	}
	$sheet.='
	<pageMargins left="0.7" right="0.7" top="0.75" bottom="0.75" header="0.3" footer="0.3" />
</worksheet>';


/*
A workbook can contain thousands of cells containing string (non-numeric) data. Furthermore, this data is very likely to be repeated across many rows or columns. The goal of implementing a single string table that is shared across the workbook is to improve performance in opening and saving the file by only reading and writing the repetitive information once.

Important sentence - Excel always creates a shared string table when it saves a file. However, using the shared string table is not required to create a valid SpreadsheetML file
https://msdn.microsoft.com/en-us/library/office/gg278314.aspx
http://officeopenxml.com/SScontentOverview.php
*/
$SharedStringTable = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<sst xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" count="0" uniqueCount="0">';
//<sst xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" count="'.count($stringlist).'" uniqueCount="'.count($stringlist).'">';
/*foreach($stringlist as $text)
	$SharedStringTable .= '<si><t>'.$text.'</t></si>';//si - SharedStringItem, t - text*/
$SharedStringTable .= '</sst>';
//Mano pagrindinė mintis - kodą padaryti aiškesnį. Iš esmės kaip suprantu SharedStringTable galima išvis praleisti jeigu tiesiogiai tekstą įrašai. Jei tiesiog tekstas įrašytas klaidas meta. Turėtų mesti klaidas pagal dabartinį kodą, nes visur nurodyta, kad yra <v>

//if(isset($_GET['save'])) {
	$newfile = dirname(__FILE__).'/'.date("YmdHis").rand().".xlsx";
	//$newfile = tmpfile();
	copy(dirname(__FILE__)."/template.xlsx", $newfile);
	//http://stackoverflow.com/questions/15140063/extract-a-file-from-a-zip-string
	//To speed up: gzinflate
	$zip = new ZipArchive;
	$fileToModify = 'xl/sharedStrings.xml';
	$fileToModify2 = 'xl/worksheets/sheet1.xml';
	if ($zip->open($newfile) === TRUE) {
		$zip->deleteName($fileToModify);
		$zip->addFromString($fileToModify, $SharedStringTable);
		$zip->deleteName($fileToModify2);
		$zip->addFromString($fileToModify2, $sheet);
		$zip->close();
		//echo 'ok';
	} else {
		echo 'failed';
	}
	
	//header("Location: $newfile");
	header('Content-Description: File Transfer');
	header('Content-Type: application/octet-stream');
	header('Content-Disposition: attachment; filename='.basename($filename));
	header('Content-Transfer-Encoding: binary');
	header('Expires: 0');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	header('Pragma: public');
	header('Content-Length: ' . filesize($newfile));
	//Can be improved see more from in pages which links in http://stackoverflow.com/questions/2882472/php-send-file-to-user
	ob_clean();
	flush();
	readfile($newfile);
	//echo $newfile;
	unlink($newfile);
	//$temp = tmpfile();
	exit;
//}
}
