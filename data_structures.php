<?php

$selectedMark = '➤ ';//-&gt; #  → 

// Allowed file types for documents
$file_type = array(
	0 => '.jpg',
	1 => '.jpeg',
	2 => '.png',
	3 => '.gif',
	4 => '.doc',
	5 => '.docx',
	6 => '.odt',
	7 => '.pdf',
	8 => '.xls',
	9 => '.xlsx',
	10 => '.txt'
);
/*/doc, docx, txt,/ msg, pps, ppsx, ppt, pptx, /pdf, xls, xlsx, jpg, png,/ rar, zip, cpp*/

// Types of documents
$dok_tipai = array(
	0 => 'Anketa',
	1 => 'Gimimo liudijimas',
	2 => 'Lengvata',
	3 => 'Pažyma apie nelaimę',//3
	4 => 'Pažyma apie šeimos sudėtį',
	5 => 'Pažyma iš darbovietės',//5
	6 => 'Prašymas',
	7 => 'Raštelis iš gydytojo',//7
	8 => 'Sveikatos būklės pažymėjimas (027-1/a)',
	9 => 'Šeimos socialinė padėtis'
);
define('SERGA', 7);
$reminder_document_types = array(
	0 => 'Šeimos sudėties pažyma',
	1 => 'Gydytojo pažyma',// (forma Nr. 094/a ir pan.)
	2 => 'Deklaracijos pažyma',
	//3 => 'Anketa',
	4 => 'Gimimo liudijimas',
	5 => 'Lengvata',
	10 => 'Pažyma apie mokymąsi',//meniu punkte PRIMINIMAI, prie pasirenkamų dokumentų pridėti "Pažyma apie mokymąsi". Kadangi skaičiuojant 50 proc. lengvatą vaikams, žiūrima, ar jo broliams/seserims nėra 18 metų ir/ar kur nors mokosi. Molėtų „Vyturėlio“ vaikų l./d. Apskaitininkė Kristina Ališauskienė Tel. nr. 8 612 42939
	6 => 'Pažyma apie nelaimę',//3
	//'Pažyma apie šeimos sudėtį',
	7 => 'Pažyma iš darbovietės',//5
	//'Prašymas koks?',
	//'Raštelis iš gydytojo',//7
	8 => 'Sveikatos būklės pažymėjimas (027-1/a)',
	9 => 'Šeimos socialinė padėtis'
);

// Vaiko pažanga
$child_achievements = array(
	0 => 'Išmoko',//žalia
	1 => 'Įpusėjo',//geltona
	2 => 'Nemoka'//raudona
);

$vaik_darzelio_tipas = array(//Kid official/formal level in school by law)
	0 => 'Lopšelį',
	1 => 'Darželį',
	2 => 'Priešmokyklinį',
	3 => 'Pradinė klasė',
);
$vaik_darzelio_tipas_kas = array(
	0 => 'Lopšelis',
	1 => 'Darželis',
	2 => 'Priešmokyklinis',
	3 => 'Pradinė klasė',
	4 => 'Mišrus'
);

$kid_age_group_kieno = [//vaikai.php
	0 => 'Lopšelio',//Ikimokyklinis: Lopšelyje
	1 => 'Ikimokyklinis darželio',
	2 => 'Priešmokyklinė',
	3 => 'Pradinė klasė'
];
$dienos = array(
	'-',
	'Pirmadienis',
	'Antradienis',
	'Trečiadienis',
	'Ketvirtadienis',
	'Penktadienis',
	'Šeštadienis',
	'Sekmadienis'
);
$dienos_kada = array(
	'-',
	'Pirmadieniais',
	'Antradieniais',
	'Trečiadieniais',
	'Ketvirtadieniais',
	'Penktadieniais',
	'Šeštadieniais',
	'Sekmadieniais'
);

//if(!defined("WEB") && DARBUOT) {
    $kvalifikacijos = array(
        0 => 'Auklėtoja-pedagogė',
        1 => 'Vyr. auklėtoja-pedagogė',
        2 => 'Metodininkė',
        3 => 'Ekspertas'
    );
    //Ir profesijos (profession) //http://profesijuklasifikatorius.lt/?q=lt/node/13441
	$pareigybes = array(
		1 => 'Akompaniatorius',
		2 => 'Anglų k. specialistas',
		3 => 'Archyvaras',
		5 => 'Auklėtojas-pedagogas',
		6 => 'Auklėtojo padėjėjas',
		7 => 'Bibliotekininkas',
		65 => 'Bendrosios praktikos slaugytojas',
		9 => 'Buhalteris',//Accountant
		8 => 'Choreografas',
		11 => 'Dailės pedagogas', 
		12 => 'Dietistas', 
		14 => 'Direktoriaus pavaduotojas',
		29 => 'Direktoriaus pavaduotojas ugdymui', 
		15 => 'Direktoriaus pavaduotojas ikimokykliniam ugdymui',
		10 => 'Direktoriaus pavaduotojas ūkiui',
		17 => 'Direktorius', 
		19 => 'Ikimokyklinio ugdymo auklėtojas',
		4 => 'IKT (Informacinių ir komunikacinių technologijų) specialistas',
		170 => 'Judesio korekcijos mokytojas (pedagogas)',//Pedagoginis
		113 => 'Kiemsargis',
		13 => 'Kineziterapeutas',//Physiotherapist http://ec.europa.eu/internal_market/qualifications/regprof/index.cfm?action=regprof&id_regprof=4812&tab=general  medikas, negali 
		16 => 'Kūno kultūros pedagogas',
		18 => 'Logopedas',//Speech therapist (generic Speech and language therapist) http://ec.europa.eu/internal_market/qualifications/regprof/index.cfm?action=regprof&id_regprof=14045&id_profession=1090&tab=countries&quid=2&mode=asc&pagenum=1
		20 => 'Medicinos darbuotojas',
		21 => 'Meninio ugdymo mokytojas',
		22 => 'Meninio ugdymo pedagogas (ikimok. ugd.)',
		23 => 'Metodininkas',
		61 => 'Mokytojas',//30
		26 => 'Muzikos pedagogas',
		126 => 'Naktinis auklėtojas',
		64 => 'Pagalbinis darbininkas',
		46 => 'Pastatų priežiūros darbininkas',
		63 => 'Plaukimo instruktorius',
		27 => 'Priešmokyklinio ugdymo pedagogas',
		28 => 'Priešmokyklinio ugdymo auklėtojas',
		41 => 'Projektų vadovas',
		36 => 'Psichologas',
		50 => 'Raštvedys',
		30 => 'Sandėlininkas, apskaitininkas',
		44 => 'Santechnikas',
		47 => 'Sargas',
		37 => 'Sekretorius',
		131 => 'Skalbėjas',
		31 => 'Socialinis pedagogas',
		34 => 'Specialusis pedagogas',//oligofrenopedagogas //Special pedagogue //http://ec.europa.eu/internal_market/qualifications/regprof/index.cfm?action=regprof&id_regprof=4850&id_profession=3214&tab=countries&regprof=Specialusis%20pedagogas&quid=2&mode=asc&pagenum=1
		25 => 'Surdopedagogas',//Sordopedagogue http://ec.europa.eu/internal_market/qualifications/regprof/index.cfm?action=regprof&id_regprof=14047&id_profession=3214&tab=countries&quid=2&mode=asc&pagenum=1
		32 => 'Sveikos gyvensenos specialistas',
		33 => 'Sveikatos priežiūros specialistas',
		38 => 'Tiflopedagogas',//Tiflopedagogue http://ec.europa.eu/internal_market/qualifications/regprof/index.cfm?action=regprof&id_regprof=14048&id_profession=3214&tab=countries&quid=2&mode=asc&pagenum=1
		35 => 'Ūkvedys',
		40 => 'Valytojas',//Nepedagoginė iš http://profesijuklasifikatorius.lt/?q=lt/node/13441
		60 => 'Skyriaus, susijusio su ugdymu, vedėjas',//6 - nors čia https://www.e-tar.lt/acc/legalAct.html?documentId=TAR.31FB968FF63A matau, kad tik vedėjas
		39 => 'Virėjas',
		42 => 'Vyr. auklėtojas-pedagogas',
		43 => 'Vyr. buhalteris',//?chief accountant?
		45 => 'Vyr. virėjas', 
		48 => 'Žaislininkas',
		62 => 'Užsienio k. specialistas',
	);
	$allowed_positions_speech_diareis = array(34, 25, 18, 38, 36/*psichologas*/, 13/*Kineziterapeutas*/, 63/*Plaukimo instruktorius*/);
	//Off topic //https://www.ldb.lt/jaunimui/jdc/Dalomoji/siauliai/Asmenyb%C4%97s%20tipo%20ir%20profesijos%20nustatymo%20testas.pdf
//}

//https://www.krisin.smm.lt/aikos2-krisin/public/klasifikatoriausPerziura.xhtml?id=160
$KL_PEDAG_PAREIG = array(
	25 => 'Meno vadovas',
	26 => 'Metodininkas',
	27 => 'Metodinio darbo vadovas',
	28 => 'Mokomosios laboratorijos vedėjas (vadovas, viršininkas)',
	30 => 'Mokytojas',
	52 => 'Studijų centro vadovas (vedėjas)',
	53 => 'Surdopedagogas',
	54 => 'Tiflopedagogas',
	55 => 'Treneris',
	56 => 'Vairavimo instruktorius',
	57 => 'Koncertmeisteris',
	1 => 'Asistentas',
	2 => 'Auklėtojas',
	5 => 'Bendrabučio auklėtojas',
	6 => 'Skyriaus, susijusio su ugdymu, vedėjas',
	7 => 'Būrelio vadovas',
	8 => 'Dėstytojas',
	9 => 'Direktoriaus pavaduotojas akademinei veiklai',
	10 => 'Direktoriaus pavaduotojas neformaliajam (papildomajam) ugdymui',
	12 => 'Direktoriaus pavaduotojas ugdymui',
	13 => 'Direktorius',
//	14 => 'Docentas',
//	15 => 'Fakulteto dekanas',
	16 => 'Grupės auklėtojas',
	17 => 'Judesio korekcijos mokytojas (pedagogas)',
	18 => 'Kabineto vedėjas',
//	19 => 'Katedros vedėjas',
//	20 => 'Laboratorijos vedėjas',
	21 => 'Laikinasis direktorius',
	22 => 'Lektorius',
	23 => 'Logopedas',
	24 => 'Meninio ugdymo mokytojas (pedagogas)',
	31 => 'Mokytojas konsultantas',
	32 => 'Direktoriaus pavaduotojas (išskyrus ūkio reikalams)',
	34 => 'Neformaliojo (papildomojo) ugdymo mokytojas (pedagogas)',
	35 => 'Neformaliojo (papildomojo) ugdymo organizatorius',
	36 => 'Padalinio, susijusio su mokslu ar studijomis vedėjas (vadovas, viršininkas)',
	37 => 'Padalinio, susijusio su mokslu ar studijomis vedėjo (vadovo, viršininko) pavaduotojas',
	38 => 'Plaukimo instruktorius',
	39 => 'Praktikos vadovas',
	40 => 'Praktinio mokymo firmos vadovas (vedėjas)',
	41 => 'Priešmokyklinio ugdymo pedagogas',
//	42 => 'Prodekanas',
	43 => 'Profesijos mokytojas',
	44 => 'Direktoriaus pavaduotojas praktiniam mokymui',
	45 => 'Mokymo firmos vadovas',
	46 => 'Padalinio, susijusio su ugdymu, vedėjas',
	47 => 'Profesorius',
	48 => 'Psichologas',
	49 => 'Skraidymo instruktorius',
	50 => 'Socialinis pedagogas',
	51 => 'Specialusis pedagogas',
//	58 => 'Doktorantūros studijų vedėjas (vadovas, viršininkas)',
	59 => 'Kvalifikacijos kėlimo centro direktorius (vadovas)',
	60 => 'Direktoriaus pavaduotojas pedagoginiam psichologiniam darbui',
//	61 => 'Klasių kuratorius',
//	62 => 'Katedros vedėjo pavaduotojas',
//	63 => 'Mokymo centro regioninio skyriaus viršininkas',
	64 => 'Akompaniatorius',
	65 => 'Mokomosios laboratorijos vedėjo (vadovo, viršininko) pavaduotojas',
	66 => 'Muzikos vadovas',
	67 => 'Profesijos patarėjas',
	68 => 'Psichologo asistentas',
	69 => 'Padalinio, susijusio su studijomis ar mokslo taikomąja veikla, vadovas (vedėjas)',
	70 => 'Padalinio, susijusio su studijomis ar mokslo taikomąja veikla, vadovo (vedėjo) pavaduotojas',
	71 => 'Padalinio, susijusio su mokymu, vedėjas',
	72 => 'Padalinio, susijusio su pedagoginiu procesu, vedėjas',
	73 => 'Praktikos vedėjo (vadovo, viršininko) pavaduotojas',
	74 => 'Praktikos vedėjas (vadovas, viršininkas)',
	75 => 'Praktikos (praktinių užsiėmimų, praktikumų) vadovas (vedėjas)',
	76 => 'Praktinio mokymo ar bandymų centro vedėjas (vadovas, viršininkas)',
	77 => 'Praktinio mokymo ar bandymų centro vedėjo (vadovo, viršininko) pavaduotojas',
	78 => 'Prorektoriaus pavaduotojas (padėjėjas) studijoms, mokslui',
	79 => 'Prorektorius (išskyrus ūkio reikalams)',
//	80 => 'Rektorius',
//	81 => 'Specialistas praktiniam studentų mokymui'
);

if(KaunoRBaibokyne) {
	$nuolaidos = array(
		0 => 1,
		1 => 0.9,
		2 => 0.8,
		3 => 0.5
	);
	$nuolaidos_txt = array(
		0 => 'Be nuolaidos',
		1 => '10 % nuolaida',
		2 => '20 % nuolaida',
		3 => '50 % nuolaida'
	);
	$nuolaidos_title = array(
		0 => 'be_nuolaidos',
		1 => '10_%_nuolaida',
		2 => '20_%_nuolaida',
		3 => '50_%_nuolaida'
	);
	$nuolaidos_dyd = array(
		0 => '-',
		1 => '10 %',
		2 => '20 %',
		3 => '50 %'
	);
} else {
	$nuolaidos = array(
		0 => 1,
		6 => 0.8,
		1 => 0.5,
		3 => 0.3,
		4 => 0.2,//Druskininkų Bitutė
		7 => 0.15,//Tauragės vaikų reabilitacijos centras-mokykla „Pušelė“
		5 => 0.1,//Skuodo r. Ylakių vaikų lopšelis darželis
		2 => 0
	);
	$nuolaidos_txt = array(
		0 => 'Be nuolaidos',
		6 => '20 % nuolaida',
		1 => '50 % nuolaida',
		3 => '70 % nuolaida',
		4 => '80 % nuolaida',//Druskininkų Bitutė
		7 => '85 % nuolaida',//Tauragės vaikų reabilitacijos centras-mokykla „Pušelė“
		5 => '90 % nuolaida',//Skuodo r. Ylakių vaikų lopšelis darželis
		2 => '100 % nuolaida'
	);
	$nuolaidos_title = array(
		0 => 'be_nuolaidos',
		6 => '20_%_nuolaida',
		1 => '50_%_nuolaida',
		3 => '70_%_nuolaida',
		4 => '80_%_nuolaida',//Druskininkų Bitutė
		7 => '85_%_nuolaida',//Tauragės vaikų reabilitacijos centras-mokykla „Pušelė“
		5 => '90_%_nuolaida',//Skuodo r. Ylakių vaikų lopšelis darželis
		2 => '100_%_nuolaida'
	);
	$nuolaidos_dyd = array(
		0 => '-',
		6 => '20 %',
		1 => '50 %',
		3 => '70 %',
		4 => '80 %',//Druskininkų Bitutė
		7 => '85 %',//Tauragės vaikų reabilitacijos centras-mokykla „Pušelė“
		5 => '90 %',//Skuodo r. Ylakių vaikų lopšelis darželis
		2 => '100 %'
	);
}

$achievements_areas = array(
	1 => '1. Kasdieniai gyvenimo įgūdžiai',
	2 => '2. Fizinis aktyvumas',
	3 => '3. Emocijų suvokimas ir raiška',
	4 => '4. Savireguliacija ir savikontrolė',
	5 => '5. Savivoka ir savigarba',
	6 => '6. Santykiai su suaugusiaisiais',
	7 => '7. Santykiai su bendraamžiais',
	8 => '8. Sakytinė kalba',
	9 => '9. Rašytinė kalba',
	10 => '10. Aplinkos pažinimas',
	11 => '11. Skaičiavimas ir matavimas',
	12 => '12. Meninė raiška',
	13 => '13. Estetinis suvokimas',
	14 => '14. Iniciatyvumas ir atkaklumas',
	15 => '15. Tyrinėjimas',
	16 => '16. Problemų sprendimas',
	17 => '17. Kūrybiškumas',
	18 => '18. Mokėjimas mokytis'
);

$parents_menu_items = [
	1 => ['title' => 'Duomenys darželiui', 'url' => 'vaiko_info'],
	2 => ['title' => 'Lankomumas', 'url' => 'lankomumas'],
	16 => ['title' => 'Pasiekimų žingsneliai', 'url' => 'achievements_steps'],
	3 => ['title' => 'Pažanga ir pasiekimai', 'url' => 'pazanga'],
	18 => ['title' => 'Tekstas', 'url' => 'docs'],
	5 => ['title' => 'Planai formose (<span class="abbr" title="ugdomosios veiklos">ugdymo</span>)', 'url' => 'Planavimas'],
	6 => ['title' => 'Vaiko dokumentai', 'url' => 'dokumentai'],
	7 => ['title' => 'Numatomos veiklos', 'url' => 'activities'],
	8 => ['title' => 'Užsiėmimų tvarkaraštis', 'url' => 'tvarkarastis'],
	9 => ['title' => 'Vaiko dienos ritmas', 'url' => 'daily_rhythm'],
	10 => ['title' => 'Bendri dokumentai', 'url' => 'documents_common'],
	11 => ['title' => 'Valgiaraštis', 'url' => 'menu'],
	14 => ['title' => 'Logopedo tvarkaraštis', 'url' => 'speech_therapist_schedule'],
	15 => ['title' => 'Logopedo temos', 'url' => 'speech_therapist_topics'],
	19 => ['title' => 'Vaiko veikla', 'url' => 'moments'],
];
if($showOldPlanning)
	$parents_menu_items[4] = ['title' => 'Planavimas', 'url' => 'planavimas'];
if((TESTINIS || RODOMASIS || SiauliuRSavivald || KaunoVaikystesLobiai || LazdijuKregzdute) && $showPayment)
	$parents_menu_items[12] = ['title' => 'Mokėjimai', 'url' => 'mokejimai'];
if(TESTINIS || RODOMASIS || SiauliuRSavivald)
	$parents_menu_items[13] = ['title' => 'Grupės tėvų kontaktai', 'url' => 'grupes_kontaktai'];
if(TESTINIS || PasvalioLiepaite)
	$parents_menu_items[17] = ['title' => 'Logo. pasiekimai', 'url' => 'logo_achievements_view'];

if(DB_PREFIX == 'knsZelmen_')
	$parents_menu_items[20] = ['title' => 'Virtuali biblioteka', 'url' => 'virtuali_biblioteka'];

abstract class moments_visibility {//http://stackoverflow.com/questions/254514/php-and-enumerations
    const kindergarten = 0;
    const kids_group = 1;
    const parents = 2;
}
$moments_visibility = ['Visa darželio bendruomenė', 'Vaikų grupės pedagogai ir tėvai', 'Vaikų grupės pedagogai ir pasirinkti tėvai'];

$library_objects_types = ['Knyga', 'Žurnalas', 'Metodinis leidinys', 'Metodinė priemonė'];

$bank_operation_type = [
	'Z02' => 'Automatinė nacionalinės valiutos keitimo operacija dėl euro įvedimo Lietuvoje',
	
	'F26' => 'Ateinantis mokėjimas',
	'C68' => 'Ateinantis mokėjimas',
	'C69' => 'Ateinantis mokėjimas',
	
	'F40' => 'Išeinantis mokėjimas',
	'CA5' => 'Pervedimas tarp savo sąskaitų',
	'F45' => 'Mokestis už įeinančius mokėjimus',
	
	'F24' => 'Mokestis už išeinančius mokėjimus',
	'C33' => 'Mokestis už išeinančius mokėjimus',
	
	'I06' => 'Sąskaitos aptarnavimo mokestis',
	'F43' => 'BANKLINK IV.lt',
];
