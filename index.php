<?php
if(isset($_GET['q'])) {
	if($_GET['q'] == 'favicon.png') {
		header('Content-Type: image/png');
		readfile('./img/favicon.png');
		exit();
	}
	if($_GET['q'] == 'apple-touch-icon-precomposed.png' || $_GET['q'] == 'apple-touch-icon.png') {//browserconfig.xml
		header('HTTP/1.0 404 Not Found');
		die("<h1>Error 404 Nerasta</h1>Užklaustas puslapis nerastas.");
	}
}
$start = microtime(true);
ob_start();
include 'main.php';
if(USER) {
	if(DARBUOT || ADMIN)
		include 'workers/index.php';
	else if(CHILD_ID > 0) {
		include 'parents/index.php';
	}
} else {
	include 'public/index.php';
}
$gen_time = (microtime(true)-$start);
$debug_info = '';
if(KESV) {
	$debug_info = ' ('.round($db_time, 3).' '.round($db_time/$gen_time*100).'% '.$db_query_cnt.")\n";
	if(isset($_GET['debug']) && arsort($queries)) {
		foreach($queries as $id => $val)
			$debug_info .= str_pad($id, 2, ' ', STR_PAD_LEFT).'='.
				str_pad(round((float)$val/$gen_time*100), 2, ' ', STR_PAD_LEFT).'% '.
				str_pad(round((float)$val/$db_time*100), 2, ' ', STR_PAD_LEFT).'% '."$val \n";
	}
}
echo '<!-- '.round($gen_time, 3).$debug_info.' -->';
ob_end_flush();
if($gen_time > 1) {
	$email = (isset($_SESSION['USER_DATA']['email']) ? $_SESSION['USER_DATA']['email'] : '');
	$HTTP_REFERER = (isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
	db_query("INSERT INTO `0time_long` SET `ipv4`=INET_ATON('{$_SERVER['REMOTE_ADDR']}'), `time`='".$gen_time."', `db_time`='".(float)$db_time."', `db_queries_cnt`='".(int)$db_query_cnt."', `request_time`='".date('Y-m-d H:i:s', $_SERVER['REQUEST_TIME'])."', `kindergarten`='".array_search(DB_PREFIX, $db_prefixes)."', `user_id`='".USER_ID."', `person_id`='".DARB_ID."', `person_login_type`='".(isset($_SESSION['USER_DATA']['person_type']) ? (int)$_SESSION['USER_DATA']['person_type'] : 0)."', `method`='".db_fix($_SERVER['REQUEST_METHOD'])."', `from_url`='".db_fix($HTTP_REFERER)."', `to_url`='".db_fix($_SERVER['REQUEST_URI'])."', `user_agent`='".db_fix($_SERVER['HTTP_USER_AGENT'])."', `email`='".db_fix($email)."', `log_id`=".(isset($log_id) ? (int)$log_id : 0));
}
