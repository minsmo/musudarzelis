<?php
$start = microtime(true);
header('Content-Type: text/html; charset=utf-8');
include '../../main.php';

$result = db_query("SELECT `id`, `prefix` FROM `0db`");
$db_prefixes = array();
while ($row = mysqli_fetch_assoc($result))
	$db_prefixes[$row['id']] = $row['prefix'];

?>
<style>.cur {font-weight: bold}</style>
Started to log from 2014-11-11 07:02:52<br>
<a href="?load_by_user_id"<?=(isset($_GET['load_by_user_id']) ? ' class="cur"' : '')?>>Load by user_id</a> |  
<a href="?load_by_ip"<?=(isset($_GET['load_by_ip']) ? ' class="cur"' : '')?>>Load by IP</a> | 
<a href="?load_by_kindergarten"<?=(isset($_GET['load_by_kindergarten']) ? ' class="cur"' : '')?>>Load by kindergarten</a> |
<a href="?long_gen_times"<?=(isset($_GET['long_gen_times']) ? ' class="cur"' : '')?>>Long generation times</a> |
<a href="?long_gen_time_details"<?=(isset($_GET['long_gen_time_details']) ? ' class="cur"' : '')?>>Long generation times details</a> |
<a href="?long_gen_times_details_by_date"<?=(isset($_GET['long_gen_times_details_by_date']) ? ' class="cur"' : '')?>>Long generation time details by date</a> |
<a href="?long_gen_times_details_by_hour"<?=(isset($_GET['long_gen_times_details_by_hour']) ? ' class="cur"' : '')?>>Long generation time details by hour</a> |
<a href="?long_gen_times_by_kindergarten"<?=(isset($_GET['long_gen_times_by_kindergarten']) ? ' class="cur"' : '')?>>Long generation time by kindergarten</a> |
<a href="?req_averages_by_hour"<?=(isset($_GET['req_averages_by_hour']) ? ' class="cur"' : '')?>>Requests averages by hour</a> |
<a href="?positions"<?=(isset($_GET['positions']) ? ' class="cur"' : '')?>>Positions populiarity</a> |
<a href="?rejection"<?=(isset($_GET['rejection']) ? ' class="cur"' : '')?>>Rejections by gen. time</a> |
<a href="?menu_visits"<?=(isset($_GET['menu_visits']) ? ' class="cur"' : '')?>>Menu visits</a> |

<?php
if(isset($_GET['load_by_user_id'])) {
	$rez = db_query( "SELECT `kindergarten`, `user_id`, INET_NTOA(`ipv4`) `ipv4`,
	ROUND(AVG(`time`), 2) `generated_AVG`,
	MIN(`DOMContentLoaded`) `DOM_MIN`,
	ROUND(AVG(`DOMContentLoaded`)) `DOM_AVG`,
	MAX(`DOMContentLoaded`) `DOM_MAX`,
	MIN(`load`) `loadMIN`,
	ROUND(AVG(`load`)) `loadAVG`,
	MAX(`load`) `loadMAX`,
	COUNT(`load`) `cnt`,
	MAX(`created`) `max_date`
	FROM `0time`
	WHERE `ipv4`<>INET_ATON('88.222.149.115')
	GROUP BY `kindergarten`, `user_id`
	ORDER BY AVG(`load`) DESC");
	echo '<table>';
	$first = true;
	$i = 1;
	while ($row = mysqli_fetch_assoc($rez)) {
		if($first) {
			echo "<tr><td>Nr</td>";
			foreach($row as $title => $val)
				echo "<td>".$title."</td>";
			echo "</tr>";
			$first = false;
		}
		//echo str_pad($db_prefixes[$row['kindergarten']], 11, ' ', STR_PAD_LEFT).'  '.str_pad($row['user_id'], 2, ' ', STR_PAD_LEFT).str_pad($row['time'], 5, ' ', STR_PAD_LEFT).str_pad($row['fifteen'], 5, ' ', STR_PAD_LEFT)."\n";
		echo "<tr><td>".$i++."</td>";
		foreach($row as $title => $val) {
			if($title == 'kindergarten')
				echo "<td>".$db_prefixes[$val].$val."</td>";
			elseif($title == 'user_id')
				echo "<td><a href=\"?kindergarten=".$row['kindergarten']."&amp;user_id=$val\">".$val."</a></td>";
			else
				echo "<td>".$val."</td>";
		}
		echo "</tr>";
	}
	echo "</table>";
} elseif(isset($_GET['load_by_ip'])) {
	$rez = db_query( "SELECT `kindergarten`, `user_id`, INET_NTOA(`ipv4`) `ipv4`,
	ROUND(AVG(`time`), 2) `generated_AVG`,
	MIN(`DOMContentLoaded`) `DOM_MIN`,
	ROUND(AVG(`DOMContentLoaded`)) `DOM_AVG`,
	MAX(`DOMContentLoaded`) `DOM_MAX`,
	MIN(`load`) `loadMIN`,
	ROUND(AVG(`load`)) `loadAVG`,
	MAX(`load`) `loadMAX`,
	COUNT(`load`) `cnt`,
	MAX(`created`) `max_date`
	FROM `0time`
	WHERE `ipv4`<>INET_ATON('88.222.149.115')
	GROUP BY `ipv4`
	ORDER BY AVG(`load`) DESC");
	echo '<table>';
	$first = true;
	$i = 1;
	while ($row = mysqli_fetch_assoc($rez)) {
		if($first) {
			echo "<tr><td>Nr</td>";
			foreach($row as $title => $val)
				echo "<td>".$title."</td>";
			echo "</tr>";
			$first = false;
		}
		//echo str_pad($db_prefixes[$row['kindergarten']], 11, ' ', STR_PAD_LEFT).'  '.str_pad($row['user_id'], 2, ' ', STR_PAD_LEFT).str_pad($row['time'], 5, ' ', STR_PAD_LEFT).str_pad($row['fifteen'], 5, ' ', STR_PAD_LEFT)."\n";
		echo "<tr><td>".$i++."</td>";
		foreach($row as $title => $val) {
			if($title == 'kindergarten')
				echo "<td>".$db_prefixes[$val].$val."</td>";
			else
				echo "<td>".$val."</td>";
		}
		echo "</tr>";
	}
	echo "</table>";
} elseif(isset($_GET['load_by_kindergarten'])) {
	$rez = db_query( "SELECT `kindergarten`, INET_NTOA(`ipv4`) `ipv4`,
	ROUND(AVG(`time`), 2) `generated_AVG`,
	MIN(`DOMContentLoaded`) `DOM_MIN`,
	ROUND(AVG(`DOMContentLoaded`)) `DOM_AVG`,
	MAX(`DOMContentLoaded`) `DOM_MAX`,
	MIN(`load`) `loadMIN`,
	ROUND(AVG(`load`)) `loadAVG`,
	MAX(`load`) `loadMAX`,
	COUNT(`load`) `cnt`,
	MIN(`created`) `min_date`,
	MAX(`created`) `max_date`
	FROM `0time`
	WHERE `ipv4`<>INET_ATON('88.222.149.115')
	GROUP BY `kindergarten`
	ORDER BY AVG(`load`) DESC");
	echo '<table>';
	$first = true;
	$i = 1;
	while ($row = mysqli_fetch_assoc($rez)) {
		if($first) {
			echo "<tr><td>Nr</td>";
			foreach($row as $title => $val)
				echo "<td>".$title."</td>";
			echo "</tr>";
			$first = false;
		}
		//echo str_pad($db_prefixes[$row['kindergarten']], 11, ' ', STR_PAD_LEFT).'  '.str_pad($row['user_id'], 2, ' ', STR_PAD_LEFT).str_pad($row['time'], 5, ' ', STR_PAD_LEFT).str_pad($row['fifteen'], 5, ' ', STR_PAD_LEFT)."\n";
		echo "<tr><td>".$i++."</td>";
		foreach($row as $title => $val) {
			if($title == 'kindergarten')
				echo "<td>".$db_prefixes[$val].$val."</td>";
			else
				echo "<td>".$val."</td>";
		}
		echo "</tr>";
	}
	echo "</table>";
} elseif(isset($_GET['kindergarten']) && isset($_GET['user_id'])) {
	$rez = db_query( "SELECT *, INET_NTOA(`ipv4`) `ipv4`
	FROM `0time`
	WHERE `ipv4`<>INET_ATON('88.222.149.115') AND `kindergarten`=".(int)$_GET['kindergarten']." AND `user_id`=".(int)$_GET['user_id']."
	ORDER BY `created` DESC");
	echo '<table>';
	$first = true;
	$i = 1;
	while ($row = mysqli_fetch_assoc($rez)) {
		if($first) {
			echo "<tr><td>Nr</td>";
			foreach($row as $title => $val)
				echo "<td>".$title."</td>";
			echo "</tr>";
			$first = false;
		}
		echo "<tr><td>".$i++."</td>";
		foreach($row as $title => $val) {
			if($title == 'kindergarten')
				echo "<td>".$db_prefixes[$val].$val."</td>";
			else
				echo "<td>".$val."</td>";
		}
		echo "</tr>";
	}
	echo "</table>";
} elseif(isset($_GET['long_gen_times_by_kindergarten']) ) {
	$rez = db_query( "SELECT *, INET_NTOA(`ipv4`) `ipv4`, ROUND(AVG(`time`), 2) `time`, ROUND(AVG(`DOMContentLoaded`)) `DOMContentLoaded`, ROUND(AVG(`load`)) `load`, COUNT(*) `cnt`, MIN(`created`) `min_created`, MAX(`created`) `max_created`
	FROM `0time`
	WHERE `ipv4`<>INET_ATON('88.222.149.115') AND `time`>1
	GROUP BY `kindergarten`
	ORDER BY `time` DESC");
	echo '<table>';
	$first = true;
	$i = 1;
	while ($row = mysqli_fetch_assoc($rez)) {
		if($first) {
			echo "<tr><td>Nr</td>";
			foreach($row as $title => $val)
				if($title == 'time')
					echo "<td>avg_time</td>";
				elseif($title == 'DOMContentLoaded')
					echo "<td>avg_DOM</td>";
				elseif($title == 'load')
					echo "<td>avg_load</td>";
				else
					echo "<td>".$title."</td>";
			echo "</tr>";
			$first = false;
		}
		echo "<tr><td>".$i++."</td>";
		foreach($row as $title => $val) {
			if($title == 'kindergarten')
				echo "<td>".$db_prefixes[$val].$val."</td>";
			else
				echo "<td>".$val."</td>";
		}
		echo "</tr>";
	}
	echo "</table>";
	
	echo "From 2014-11-27 (2014-11-26 index on: vaikai.(`parent_kid_id`, `valid_from`) AND dokumentai.vaiko_id)\n";
	$rez = db_query( "SELECT *, INET_NTOA(`ipv4`) `ipv4`, ROUND(AVG(`time`), 2) `time`, ROUND(AVG(`DOMContentLoaded`)) `DOMContentLoaded`, ROUND(AVG(`load`)) `load`, COUNT(*) `cnt`, MIN(`created`) `min_created`, MAX(`created`) `max_created`
	FROM `0time`
	WHERE `ipv4`<>INET_ATON('88.222.149.115') AND `time`>1 AND `created`>='2014-11-27'
	GROUP BY `kindergarten`
	ORDER BY `time` DESC");
	echo '<table>';
	$first = true;
	$i = 1;
	while ($row = mysqli_fetch_assoc($rez)) {
		if($first) {
			echo "<tr><td>Nr</td>";
			foreach($row as $title => $val)
				if($title == 'time')
					echo "<td>avg_time</td>";
				elseif($title == 'DOMContentLoaded')
					echo "<td>avg_DOM</td>";
				elseif($title == 'load')
					echo "<td>avg_load</td>";
				else
					echo "<td>".$title."</td>";
			echo "</tr>";
			$first = false;
		}
		echo "<tr><td>".$i++."</td>";
		foreach($row as $title => $val) {
			if($title == 'kindergarten')
				echo "<td>".$db_prefixes[$val].$val."</td>";
			else
				echo "<td>".$val."</td>";
		}
		echo "</tr>";
	}
	echo "</table>
	From 2015-01-19 (2015-01-18 index on forms_fields_answers.answer_id)<br><br>";
} elseif(isset($_GET['long_gen_times'])) {
	$_GET['gen_time'] = (!empty($_GET['gen_time']) ? (float)$_GET['gen_time'] : 1.5);
	?><form method="get">
	<input type="text" name="gen_time" value="<?=$_GET['gen_time']?>" placeholder="gen time more than"> recommendation 1.5
	<input type="hidden" name="long_gen_times" value="">
	<input type="submit">
	</form><?php
	$rez = db_query( "SELECT *, INET_NTOA(`ipv4`) `ipv4`
	FROM `0time`
	WHERE `ipv4`<>INET_ATON('88.222.149.115') AND `time`>".(float)$_GET['gen_time']."
	ORDER BY `time` DESC");
	echo '<table>';
	$first = true;
	$i = 1;
	while ($row = mysqli_fetch_assoc($rez)) {
		if($first) {
			echo "<tr><td>Nr</td>";
			foreach($row as $title => $val)
				echo "<td>".$title."</td>";
			echo "</tr>";
			$first = false;
		}
		echo "<tr><td>".$i++."</td>";
		foreach($row as $title => $val) {
			if($title == 'kindergarten')
				echo "<td>".$db_prefixes[$val].$val."</td>";
			else
				echo "<td>".$val."</td>";
		}
		echo "</tr>";
	}
	echo "</table>";
} elseif(isset($_GET['req_averages_by_hour'])) {
	$rez = db_query( "SELECT DATE_FORMAT(`created`,  '%H') `hour`, COUNT(*) `counter`, ROUND(AVG(`time`), 2) `avg_gen_s.`, ROUND(MAX(`time`), 2) `max_gen_s.`, ROUND(AVG(`DOMContentLoaded`)) `DOM`, ROUND(AVG(`load`)) `load`, INET_NTOA(`ipv4`) `ipv4`, `kindergarten` -- id, user_id
	FROM `0time`
	WHERE `ipv4`<>INET_ATON('88.222.149.115') AND `time`<30 -- AND DATE(`created`)>='2015-12-01'
	GROUP BY DATE_FORMAT(`created`,  '%H') 
	ORDER BY DATE_FORMAT(`created`,  '%H')");
	echo '<table>';
	$first = true;
	while ($row = mysqli_fetch_assoc($rez)) {
		if($first) {
			foreach($row as $title => $val)
				echo "<td>".$title."</td>";
			echo "</tr>";
			$first = false;
		}
		echo "<tr>";
		foreach($row as $title => $val) {
			if($title == 'kindergarten')
				echo "<td>".$db_prefixes[$val].$val."</td>";
			else
				echo "<td>".$val."</td>";
		}
		echo "</tr>";
	}
	echo "</table>";
} elseif(isset($_GET['parents_access'])) {
	$iter_db_prefixes = array();
	/*$result = db_query("SELECT `id`, `prefix` FROM `0db`");
	$db_prefixes = array();
	while ($row = mysqli_fetch_assoc($result))
		$db_prefixes[$row['id']] = $row['prefix'];*/
	$iter_db_prefixes = $db_prefixes;
	//for($i=4; $i <= 10/*23*/; ++$i) {
	for($i=1; $i <= 31; ++$i) {
		$prefix = 'semi'.$i.'_';
		if(!in_array($prefix, $iter_db_prefixes))
			$iter_db_prefixes[] = $prefix;
		//Order:
		unset($iter_db_prefixes[array_search($prefix, $iter_db_prefixes)]);
		$iter_db_prefixes[] = $prefix;
	}
	foreach($iter_db_prefixes as $prefix) {
		$result = db_query("SELECT * FROM `".$prefix."users` WHERE `login_counter`<>0");
		if ($row = mysqli_fetch_assoc($result))
			echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; on: ".$row['last_login_time']."; counter: ".$row['login_counter']." \n";
	}
} elseif(isset($_GET['positions'])) {
	echo '<br><strong>Exluding s_ r_ t_ semi*:</strong>';
//NOT: id, ipv4, kindergarten	user_id	person_id
	$rez = db_query( "SELECT ROUND(AVG(`time`), 2) `avg_gen`, ROUND(AVG(`DOMContentLoaded`)/1000, 2) `avg_DOM`, ROUND(AVG(`load`)/1000, 2) `avg_load`, MAX(`created`) `max_created`, DATE(MIN(`created`)) `min_created`, `person_login_type` `PLT`, COUNT(*) `cnt`
	FROM `0time`
	WHERE `ipv4`<>INET_ATON('88.222.149.115') AND (`kindergarten`<21 -- semi1
	OR `kindergarten`>99) -- exluding s_ r_ t_ 
	".(!empty($_GET['positions']) ? " AND `kindergarten`=".(int)$_GET['positions'] : '')."
	-- AND `created`>'2015-11-16 00:00:00'
	GROUP BY `person_login_type` 
	ORDER BY COUNT(*) DESC");
	echo '<table>';
	$first = true;
	$rows = [];
	$total = 0;
	while ($row = mysqli_fetch_assoc($rez)) {
		$rows[] = $row;
		$total += $row['cnt'];
	}
	foreach($rows as $row) {
		if($first) {
			foreach($row as $title => $val)
				if($title == 'cnt')
					echo "<th style=\"text-align: right\">".$title."</th>";
				else
					echo "<th>".$title."</th>";
			echo "<th>%</th>
			<th>Prieigos rūšis</th>
			</tr>";
			$first = false;
		}
		echo "<tr>";
		foreach($row as $title => $val) {
			if($title == 'kindergarten')
				echo "<td>".$db_prefixes[$val].$val."</td>";
			elseif($title == 'cnt')
				echo "<td style=\"text-align: right\">".$val."</td>";
			else
				echo "<td>".$val."</td>";
		}
		echo "<td>".round($row['cnt']/$total*100, 2)."</td>
			<td>".$person_type_who[$row['PLT']]."</td>
		</tr>";
	}
	echo "</table>";
} elseif(isset($_GET['rejection'])) {
	?><form method="get">
	<input type="text" name="gen_time" value="<?=(!empty($_GET['gen_time']) ? (float)$_GET['gen_time'] : '')?>" placeholder="gen time more than">
	<input type="text" name="gen_time_less" value="<?=(!empty($_GET['gen_time_less']) ? (float)$_GET['gen_time_less'] : '')?>" placeholder="gen time less than"><br>
	<input type="text" name="from" value="<?=(!empty($_GET['from']) ? (float)$_GET['from'] : '')?>" placeholder="gen time from">
	<input type="text" name="to" value="<?=(!empty($_GET['to']) ? (float)$_GET['to'] : '')?>" placeholder="gen time to">
	<input type="hidden" name="rejection" value="">
	<input type="submit">
	</form>
	<strong>Exluding s_ r_ t_ semi*:</strong>
	<?php
	$rez = db_query( "SELECT
	SUM(IF(`load`=0, 1, 0)) `exited`,
	SUM(IF(`load` <>0, 1, 0)) `waited`,
	COUNT(`load`) `all`,
	ROUND(SUM(IF(`load`=0, 1, 0)) / COUNT(`load` ), 2) `exited_percent`,
	ROUND(AVG(`time`), 2) `avg_gen`,
	ROUND(AVG(`DOMContentLoaded`)/1000, 2) `avg_DOM`,
	ROUND(AVG(`load`)/1000, 2) `avg_load`,
	MAX(`created`) `max_created`
	FROM  `0time`
	WHERE `ipv4`<>INET_ATON('88.222.149.115') AND (`kindergarten`<21 -- semi1
	OR `kindergarten`>99)
	".(!empty($_GET['gen_time']) ? 'AND `time`>'.(float)$_GET['gen_time'] : '')."
	".(!empty($_GET['gen_time_less']) ? 'AND `time`<'.(float)$_GET['gen_time_less'] : '')."
	".(!empty($_GET['from']) ? 'AND `time` BETWEEN '.(float)$_GET['from'].' AND '.(float)$_GET['to'] : '')."
	-- exluding s_ r_ t_ ");
	echo '<table>';
	$first = true;
	while ($row = mysqli_fetch_assoc($rez)) {
		if($first) {
			foreach($row as $title => $val)
				echo "<td>".$title."</td>";
			echo "</tr>";
			$first = false;
		}
		echo "<tr>";
		foreach($row as $title => $val) {
			if($title == 'kindergarten')
				echo "<td>".$db_prefixes[$val].$val."</td>";
			else
				echo "<td>".$val."</td>";
		}
		//echo "<td>".$person_type_who[$row['PLT']]."</td>";
		echo "</tr>";
	}
	echo "</table>";
} elseif(isset($_GET['long_gen_time_details'])) {
	?><form method="get">
	<input type="text" name="gen_time" value="<?=(isset($_GET['gen_time']) ? (float)$_GET['gen_time'] : '')?>" placeholder="gen time more than"> recommendation 1.5
	<input type="hidden" name="long_gen_time_details" value="">
	<input type="submit">
	</form>
	<strong>Exluding s_ r_ t_ semi*:</strong>
	<?php
	$rez = db_query("SELECT *, INET_NTOA(`ipv4`) `ipv4`, ROUND(`time`, 2) `time`,
	SUBSTRING(`user_agent`, 1, 10) `user_agent`, -- 40
	 ROUND(`db_time`, 2) `db_time`
	FROM  `0time_long`
	WHERE `ipv4`<>INET_ATON('88.222.149.115') AND (`kindergarten`<21 -- semi1
	OR `kindergarten`>99) AND `exclude`=0
	".(!empty($_GET['gen_time']) ? 'AND `time`>'.$_GET['gen_time'] : '')."
	".(!empty($_GET['long_gen_time_details']) && strlen($_GET['long_gen_time_details']) == 19 ? " AND `request_time`>='".db_fix($_GET['long_gen_time_details'])."'" : '')."
	".(!empty($_GET['long_gen_time_details']) && strlen($_GET['long_gen_time_details']) < 19 ? ' ORDER BY `id` DESC LIMIT 0,'.(int)$_GET['long_gen_time_details'].'' : '')."
	-- exluding s_ r_ t_ ");
	echo '<table>';
	$first = true;
	$TO = [];
	$TO_s = [];
	$method = ['GET' => 0, 'POST' => 0];
	$method_s = ['GET' => 0, 'POST' => 0];
	$last_to_url = '';
	$last_email = '';
	$cnt = 0;
	while ($row = mysqli_fetch_assoc($rez)) {
		unset($row['user_agent'], $row['ipv4']);
		if($first) {
			foreach($row as $title => $val)
				if($title == 'person_login_type')
					echo "<td title=\"person_login_type\">PT</td>";
				elseif($title == 'db_time')
					echo "<td title=\"db_time\">db_t</td>";
				elseif($title == 'method')
					echo "<td title=\"method\">HTTP</td>";
				elseif($title == 'user_id')
					echo "<td title=\"user_id\">usrId</td>";
				elseif($title == 'person_id')
					echo "<td title=\"person_id\">prsnId</td>";
				elseif($title == 'db_queries_cnt')
					echo "<td title=\"db_queries_cnt\">qrs</td>";
				else
					echo "<td>".$title."</td>";
			echo "</tr>";
			$first = false;
		}
		$cnt += $multiple = $last_to_url == $row['to_url'] && $last_email == $row['email'];
		echo "<tr".($multiple ? ' style="color: red;"' : '').">";
		foreach($row as $title => $val) {
			if($title == 'kindergarten')
				echo "<td>".$db_prefixes[$val].$val."</td>";
			elseif($title == 'from_url')
				echo "<td style=\"white-space:nowrap\">F"/*FROM*/.mb_substr(str_replace(['http://musudarzelis.lt', 'http://www.musudarzelis.lt'], ['', ''], $val), 0, 30)."</td>";
			elseif($title == 'to_url')
				echo "<td>TO".$val."Q</td>";
			elseif($title == 'request_time')
				echo "<td style=\"white-space:nowrap\">".mb_substr($val, 2)."</td>";
			elseif($title == 'created')
				echo "<td style=\"white-space:nowrap\">".mb_substr($val, 2)."</td>";
			elseif($title == 'person_login_type')
				echo "<td>".mb_substr($person_type_who[$val], 0, 2)."</td>";
			else
				echo "<td>".$val."</td>";
		}
		if(isset($TO[explode('?', $row['to_url'])[0]]))
			++$TO[explode('?', $row['to_url'])[0]];
		else
			$TO[explode('?', $row['to_url'])[0]] = 0;
		++$method[$row['method']];
		if(isset($TO_s[explode('?', $row['to_url'])[0]]))
			$TO_s[explode('?', $row['to_url'])[0]] += $row['time'];
		else
			$TO_s[explode('?', $row['to_url'])[0]] = $row['time'];
		$method_s[$row['method']] += $row['time'];
		//echo "<td>".$person_type_who[$row['PLT']]."</td>";
		echo "</tr>";
		$last_to_url = $row['to_url'];
		$last_email = $row['email'];
	}
	echo "</table>";
	echo '<pre>';
	echo '2–X-th click '.$cnt."\n";
	arsort($TO);
	print_r($method);
	print_r($TO);
	arsort($TO_s);
	print_r($method_s);
	print_r($TO_s);
	
} elseif(isset($_GET['long_gen_times_details_by_date'])) {
	?><form method="get">
	<input type="text" name="gen_time" value="<?=(isset($_GET['gen_time']) ? (float)$_GET['gen_time'] : '')?>" placeholder="gen time more than"> recommendation 1.5
	<input type="hidden" name="long_gen_times_details_by_date" value="">
	<input type="submit">
	</form>
	<strong>Exluding s_ r_ t_ semi*:</strong>
	<?php
	$rez = db_query("SELECT DATE(`created`) `date`, COUNT(*) `cnt`, ROUND(AVG(`time`), 2) `avg_time`, ROUND(MAX(`time`), 2) `max_time`,  
	 ROUND(AVG(`db_time`), 2) `avg_db_time`, ROUND(MAX(`db_time`), 2) `max_db_time`
	FROM  `0time_long`
	WHERE `ipv4`<>INET_ATON('88.222.149.115') AND (`kindergarten`<21 -- semi1
	OR `kindergarten`>99)
	".(!empty($_GET['gen_time']) ? 'AND `time`>'.$_GET['gen_time'] : '')."
	GROUP BY DATE(`created`)
	ORDER BY DATE(`created`) DESC
	-- exluding s_ r_ t_ ");
	echo '<table>';
	$first = true;
	while ($row = mysqli_fetch_assoc($rez)) {
		if($first) {
			foreach($row as $title => $val)
				if($title == 'person_login_type')
					echo "<td title=\"person_login_type\">PLT</td>";
				else
					echo "<td>".$title."</td>";
			echo "</tr>";
			$first = false;
		}
		$weekday = date('N', strtotime($row['date']));
		echo "<tr".($weekday > 5 ? ' style="color: grey;"' : '').">";
		foreach($row as $title => $val) {
			if($title == 'kindergarten')
				echo "<td>".$db_prefixes[$val].$val."</td>";
			elseif($title == 'from_url')
				echo "<td>F"/*FROM*/.mb_substr(str_replace(['http://musudarzelis.lt', 'http://www.musudarzelis.lt'], ['', ''], $val), 0, 30)."</td>";
			elseif($title == 'to_url')
				echo "<td>TO".$val."</td>";
			else
				echo "<td>".$val."</td>";
		}
		//echo "<td>".$person_type_who[$row['PLT']]."</td>";
		echo "</tr>";
	}
	echo "</table>";
} elseif(isset($_GET['long_gen_times_details_by_hour'])) {
	?><form method="get">
	<input type="text" name="gen_time" value="<?=(isset($_GET['gen_time']) ? (float)$_GET['gen_time'] : '')?>" placeholder="gen time more than"> recommendation 1.5
	<input type="hidden" name="long_gen_times_details_by_hour" value="">
	<input type="submit">
	</form>
	<strong>Exluding s_ r_ t_ semi*:</strong>
	<?php
	$rez = db_query("SELECT HOUR(`created`) `date`, COUNT(*) `cnt`, ROUND(AVG(`time`), 2) `avg_time`, ROUND(MAX(`time`), 2) `max_time`,  
	 ROUND(AVG(`db_time`), 2) `avg_db_time`, ROUND(MAX(`db_time`), 2) `max_db_time`
	FROM  `0time_long`
	WHERE `ipv4`<>INET_ATON('88.222.149.115') AND (`kindergarten`<21 -- semi1
	OR `kindergarten`>99) AND `time`<5
	".(!empty($_GET['gen_time']) ? 'AND `time`>'.$_GET['gen_time'] : '')."
	GROUP BY HOUR(`created`)
	ORDER BY HOUR(`created`)
	-- exluding s_ r_ t_ ");
	echo '<table>';
	$first = true;
	$cnt = 0;
	while ($row = mysqli_fetch_assoc($rez)) {
		$cnt += $row['cnt'];
		if($first) {
			foreach($row as $title => $val)
				if($title == 'person_login_type')
					echo "<td title=\"person_login_type\">PLT</td>";
				else
					echo "<td>".$title."</td>";
			echo "</tr>";
			$first = false;
		}
		$weekday = date('N', strtotime($row['date']));
		echo "<tr".($weekday > 5 ? ' style="color: grey;"' : '').">";
		foreach($row as $title => $val) {
			if($title == 'kindergarten')
				echo "<td>".$db_prefixes[$val].$val."</td>";
			elseif($title == 'from_url')
				echo "<td>F"/*FROM*/.mb_substr(str_replace(['http://musudarzelis.lt', 'http://www.musudarzelis.lt'], ['', ''], $val), 0, 30)."</td>";
			elseif($title == 'to_url')
				echo "<td>TO".$val."</td>";
			else
				echo "<td>".$val."</td>";
		}
		//echo "<td>".$person_type_who[$row['PLT']]."</td>";
		echo "</tr>";
	}
	echo "</table>Total cnt - ".$cnt."<br>\n";
} elseif(isset($_GET['menu_visits'])) {
	$r = db_query("SELECT COUNT(`0time`.`menu_id`) `cnt`, `user_type`, `menu_url`
	FROM  `0time` 
		-- LEFT
		JOIN `0menu` ON `0menu`.`menu_id`=`0time`.`menu_id`
	WHERE `ipv4`<>INET_ATON('88.222.149.115') AND (`kindergarten`<21 -- semi1
													OR `kindergarten`>99)
	GROUP BY `0time`.`menu_id`
	ORDER BY `cnt` DESC");
	echo '<table>';
	$first = true;
	$cnt = 0;
	while ($row = mysqli_fetch_assoc($r)) {
		$cnt += $row['cnt'];
		if($first) {
			foreach($row as $title => $val)
				echo "<td>".$title."</td>";
			echo "</tr>";
			$first = false;
		}
		echo "<tr>";
		foreach($row as $title => $val) {
			//if($title == 'kindergarten')
			//	echo "<td>".$db_prefixes[$val].$val."</td>";//TODO: filter by kindergarten
				echo "<td>".$val."</td>";
		}
		//echo "<td>".$person_type_who[$row['PLT']]."</td>";//TODO: filter by person type
		echo "</tr>";
	}
	echo "</table>Total cnt - ".$cnt."<br>\n";
}

echo "\n".round(microtime(true)-$start, 3);
