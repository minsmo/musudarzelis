<?php
$start = microtime(true);
header('Content-Type: text/html; charset=utf-8');
include '../../main.php';
$prefix = isset($_GET['prefix']) && in_array($_GET['prefix'], $db_prefixes) ? $_GET['prefix'] : '';//lzdVytur_
?>
<form>
<input type="number" name="kid_id" title="kid_id" value="<?=(isset($_GET['kid_id']) ? (int)$_GET['kid_id'] : '')?>" placeholder="kid id">
<input type="text" name="prefix" title="prefix" value="<?=(isset($_GET['prefix']) ? $prefix : '')?>" placeholder="prefix">
<input type="submit">
</form>
<?php
echo '<pre>';
echo date('Y-m-d H:i l')."\n";

if(isset($_GET['kid_id']) && isset($_GET['prefix'])) {
	$result = db_query("SELECT * FROM `".$prefix."vaikai` WHERE `parent_kid_id`=".(int)$_GET['kid_id']);//60-70ms
	while($row = mysqli_fetch_assoc($result))
		print_r($row);

	echo "\nReminders:\n";//10ms
	$result = db_query("SELECT COUNT(*) as `CNT` FROM `".$prefix."reminder` WHERE `kid_id`=".(int)$_GET['kid_id']." HAVING COUNT(*)>0");
	while ($row = mysqli_fetch_assoc($result))
		//if($row['CNT'])
			echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; reminders: ".$row['CNT']." \n";
	$result = db_query("SELECT COUNT(*) as `CNT` FROM `".$prefix."reminder_sent` WHERE `kid_id`=".(int)$_GET['kid_id']." HAVING COUNT(*)>0");
	while ($row = mysqli_fetch_assoc($result))
		//if($row['CNT'])
			echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; reminders SENT: ".$row['CNT']." \n";

	echo "\nNew planning:\n";//10ms
	$result = db_query("SELECT COUNT(*) as `CNT` FROM `".$prefix."forms_fields_answers_i` WHERE `child_id`=".(int)$_GET['kid_id']." HAVING COUNT(*)>0");
	if ($row = mysqli_fetch_assoc($result))
			echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; plans WRITTEN: ".$row['CNT']." \n";
	echo "\nOld planning:\n";//10ms
	$result = mysqli_query($db_link, "SELECT 1 FROM `".$prefix."planning_individual` LIMIT 1");
	if($result) {
		$result = db_query("SELECT COUNT(*) as `CNT` FROM `".$prefix."planning_individual` WHERE `child_id`=".(int)$_GET['kid_id']." HAVING COUNT(*)>0");
		if ($row = mysqli_fetch_assoc($result))
				echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; plans WRITTEN: ".$row['CNT']." \n";
	}

	echo "\nDocuments:\n";//~5ms
	$result = db_query("SELECT COUNT(*) AS `SUM` FROM `".$prefix."dokumentai` WHERE `vaiko_id`=".(int)$_GET['kid_id']." HAVING COUNT(*)>0");
	if ($row = mysqli_fetch_assoc($result))
		echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; docs: ".$row['SUM']." \n";

	/*
	echo "\nSchedule of groups activities:\n";
	foreach($iter_db_prefixes as $prefix) {
		$result = db_query("SELECT COUNT(*) AS `SUM` FROM `".$prefix."grupes_bureliai` HAVING COUNT(*)>1");
		if ($row = mysqli_fetch_assoc($result))
			echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; groups activities: ".$row['SUM']." \n";
	}*/
	echo "\nAttendance (not normalized):\n";
	$result = db_query("SELECT COUNT(*) AS `SUM` FROM `".$prefix."lankomumas` WHERE `vaiko_id`=".(int)$_GET['kid_id']." HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result))
		echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; attendance marked: ".$row['SUM']." \n";
	
	echo "\nTabeliai:\n";
	$result = db_query("SELECT COUNT(*) AS `SUM` FROM `".$prefix."tabeliai` WHERE `vaiko_id`=".(int)$_GET['kid_id']." HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result))
		echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; tabelis: ".$row['SUM']." \n";
	
	echo "\nMessages:\n";
	$result = db_query("SELECT COUNT(*) AS `SUM` FROM `".$prefix."messages` WHERE `toPersonId`=".(int)$_GET['kid_id']." AND `toPersonType`=0 OR `fromPersonId`=".(int)$_GET['kid_id']." AND `fromPersonType`=0 HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result))
		echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; sent messages: ".$row['SUM']." \n";
	echo "\nAchievements and progress:\n";
	$result = db_query("SELECT COUNT(*) AS `SUM` FROM `".$prefix."pasiekimai` WHERE `vaiko_id`=".(int)$_GET['kid_id']." HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result))
		echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; Achievements: ".$row['SUM']." \n";

	/*echo "\nLast login (started to log from 2014-10-27 19:57:06):\n";
	foreach($iter_db_prefixes as $prefix) {
		$result = db_query("SELECT MAX(`last_login_time`) `last_login_time`, SUM(`login_counter`) `login_counter` FROM `".$prefix."users`");
		if ($row = mysqli_fetch_assoc($result))
			echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; on: ".$row['last_login_time']."; counter: ".$row['login_counter']." \n";
	}
	*/
	echo "\nHealth observations
	(teachers see this menu item only of these DB [last updated 2014-11-21]:
	TESTINIS || RODOMASIS || NAUJAS || KaunoZiburelis || KaunoRudnosiukas || SiauliuZiogelis || LazdijuRZibute || TauragesRAzuoliukas):\n";//10ms
	$result = mysqli_query($db_link, "SELECT 1 FROM `".$prefix."health_observations` LIMIT 1");
	if($result) {
		$result = db_query("SELECT COUNT(*) as `CNT` FROM `".$prefix."health_observations` WHERE `child_id`=".(int)$_GET['kid_id']." HAVING COUNT(*)>1");
		if ($row = mysqli_fetch_assoc($result))
			echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; health observations: ".$row['CNT']." \n";
	} else
		echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; denied\n";
}
echo "\n".round(microtime(true)-$start, 3);
