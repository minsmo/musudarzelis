<?php
$start = microtime(true);
header('Content-Type: text/html; charset=utf-8');
include '../../main.php';
echo '<pre>';
echo date('Y-m-d H:i l')."\n";
$iter_db_prefixes = array();
/*$result = db_query("SELECT `id`, `prefix` FROM `0db`");
$db_prefixes = array();
while ($row = mysqli_fetch_assoc($result))
	$db_prefixes[$row['id']] = $row['prefix'];*/
$iter_db_prefixes = $db_prefixes;
//for($i=4; $i <= 10/*23*/; ++$i) {
/*for($i=1; $i <= 31; ++$i) {
	$prefix = 'semi'.$i.'_';
	if(!in_array($prefix, $iter_db_prefixes))
		$iter_db_prefixes[] = $prefix;
	//Order:
	unset($iter_db_prefixes[array_search($prefix, $iter_db_prefixes)]);
	$iter_db_prefixes[] = $prefix;
}*/
//print_r($iter_db_prefixes);

$checked_kid = array(
	'knsVL_' => array(
		39 => '2014-11-12 13:39:32',//Iš perliukų į deimančiukus
		48 => '2014-11-03 09:39:15',//Išregistruotas
		28 => '2015-01-05 09:49:41',//vaiko išregistravimas
		29 => '2015-01-05 09:48:48',//vaiko išregistravimas
	),
	'knsVarp_' => array(
		119 => '2014-11-03 09:15:36', //Išregistruotas 2014-10-01
		191 => '2014-10-28 20:25:16',//Išregistruotas 2014-10-16
		58 => '2014-11-17 13:12:38',//Išvyko 2014-11-04
		154 => '2014-11-24 21:26:49',//soc. remtina šeima
	),
	'lzdVytur_' => array(
		37 => '2014-10-17 11:32:45',//Atisakyta pavakarių 2014-10-01
		117 => '2014-11-21 09:25:42',//Atsisakė pusryčių 2014-10-16 //2014-10-27 11:30:25//Išėjo
		125 => '2015-01-09 13:08:22'/*2014-10-27 12:11:24 kažkas pasikeitė*/,//Tikriausiai gerai. Pasikeitė nuolaida.
		121 => '2014-11-21 09:31:07',//Išėjo nuo 2014-11-10
		123 => '2015-01-09 13:04:11',//Pasikeitė matinimas
		186 => '2015-01-29 19:02:40',//pavakariai
	),
	'lzdKreg_' => array(
		4 => '2014-12-01 13:51:08',//'2014-11-12 11:57:43',Atbuline data išminuosuoti reikia ir nesutampa su buhalterės duomenimis//Šį kart gavo mėnesiu atgal, bet būna ir trim mėnesiais gauna
		7 => '2014-11-30 20:58:35',//2014-11-11 08:27:33
		12 => '2014-12-01 13:44:57',//'2014-11-11 10:47:12',Atbuline data išminuosuoti reikia ir nesutampa su buhalterės duomenimis
		41 => '2014-11-11 10:45:44',
		62 => '2014-11-11 11:07:07',
		128 => '2014-11-11 11:16:47',
		86 => '2014-12-12 10:50:48'/*2014-11-19 14:01:44*/,//Nereik tėvams mokėti už pietus//2014-11-13 14:12:57//Socialinė pažyma - 2014-12-12 10:50:48
		90 => '2014-11-12 10:30:52',//Nereik tėvams mokėti už pietus
		96 => '2014-11-13 14:13:48',//Nereik tėvams mokėti už pietus
		154 => '2014-11-26 16:45:08',//Išregistruotas
		88 => '2015-01-02 09:25:02',//Valgo pavakarius
		5 => '2014-12-31 12:40:10',//Pasibaigė soc. pašalpa
		9 => '2014-12-31 12:38:21',//Sumažėjo nuolaida?
		10 => '2014-12-31 12:45:05',//Pasibaigė soc. pašalpa
		121 => '2015-01-20 07:54:44',//prašymas, dėl pusryčių ir pavakarių valgymo
		48 => '2015-02-02 13:41:49',//Išėjo iš darželio
		139 => '2015-02-09 12:45:21',//Išėjo iš darželio
	),
	'lzdrZibut_' => array(
		2 => '2014-10-03 15:37:09',//Išregistruotas 2014-09-24
		28 => '2014-10-03 12:13:46',//Išregistruotas 2014-09-22
		30 => '2014-10-06 20:31:28',//Išregistruotas 2014-10-01
		//53 => '',//ID54 - Pasiskambinau 865662245 3min.
		23 => '2015-01-09 14:54:46',//perkėlimas į kitą grupę
		5 => '2015-01-30 15:35:37',//Pradėjo valgyti pavakarius
		19 => '2015-02-05 12:13:40', //archyvas
	),
	'tauAzuol_' => array(
		27 => '2014-11-09 13:29:54',
		166 => '2014-12-30 17:51:01',//Įsak. Nr. U-203 suteikiantis galimybę nevalgyti pusryčių
		281 => '2015-01-05 12:04:58',//Pagal įsakymą išjungtas
		280 => '2015-01-05 12:12:06',//Adaptacijai nedavė maitinimo, bet tikriausiai čia gerai yra
		154 => '2015-01-09 09:24:03',//Išjungiama iš grupės pagal Įsak. Nr
		220 => '2015-01-13 15:17:14',//Išjungta pagal Įsak.Nr
		259 => '2015-01-14 09:44:31',//Išjungtas iš grupės pagal Įsak.Nr.
		291 => '2015-01-28 09:28:03',//Adaptacijos pabaiga
	),
	'knsZings_' => array(
		149 => '2015-01-06 09:44:39',//Dingo nuolaida
		231 => '2015-01-06 09:41:46',
	),
	'molVytur_' => array(
		107 => '2015-01-05 10:17:39',//perkėlimas į kitą grupę
		6 => '2015-01-07 12:36:20',//Pridėti pusryčiai nuo 2015-01-02
		179 => '2015-01-12 16:04:57',//Pridėti pusryčiai nuo 2015-01-13
		36 => '2015-01-15 08:26:06',//prašant tėveliam atsisako vakarienės
		28 => '2015-01-30 13:08:52',//pagal prašymą
		157 => '2015-01-30 13:37:04',//pagal prašymą
		
	),
	'naujas_' => array(
		1 => '2014-10-03 23:08:52'
	),
	'rokVarp_' => array(
		152 => '2015-01-05 15:12:10',//Pradėjo valgyti pusryčius ir pietus//Pradėjo valgyt pavakarius
		6 => '2014-12-30 16:25:19',//Valgys pavakarius
		9 => '2014-12-31 12:25:28',//Mokestis be nuolaidos
		12 => '2015-01-27 16:47:42',//Išbraukiamas iš darželio sąrašų
		214 => '2015-02-02 12:18:24',//Pradėjo valgyt pusryčius ir pietus
	),
	'druskBit_' => array(
		65 => '2015-01-21 14:34:04'/*2015-01-15 13:44:43*/,//Išėjo iš darželio
		243 => '2015-01-15 11:13:14',//Išėjo iš darželio
		174 => '2015-01-23 13:36:10',//Išėjo iš darželio
		80 => '2015-01-30 08:45:39',/*2015-01-29 14:09:59*///Išėjo iš darželio
		222 => '2015-02-17 14:09:52',//Išėjo iš darželio
		64 => '2015-02-17 14:34:51',//Išėjo iš darželio
	),
	'knsrEglut_' => array(
		4 => '2015-02-18 10:06:08',//Atsisakė vakarienės
	),
);
$vain_deletion = [
	'lzdKreg_' => [
		122 => ''//Tada pridėjo 139 ir pažymėjo vėl lankomumą iš naujo
	],
];

if(!isset($_GET['deleted']))
	echo "<a href=\"?deleted\">show deleted</a>\n";
else
	echo "<a href=\"?\">hide deleted</a>\n";

if(!isset($_GET['kids_versions']))
	echo "<a href=\"?kids_versions\">show kids_versions</a>\n";
else
	echo "<a href=\"?\">hide kids_versions</a>\n";

if(isset($_GET['kids_versions'])) {
	$TempStart = microtime(true);
	echo "Kids data versions:\n";//~5ms
	$i = 1;
	foreach($iter_db_prefixes as $prefix) {
		if(only_kindergartens($prefix)) {
			//echo $prefix."\n";
			//echo 'In config for floats "," instead of ".".';
			/*$result = db_query("SELECT * FROM `".$prefix."config`");
			while ($row = mysqli_fetch_assoc($result))
				if($allowed_options[$row['title']] == 'float' && strpos($row['value'], ',') !== false)
					echo 'BUG'.$prefix.$row['title']."\n";
			*/
	
			//TODO: Gal atskiram faile kaip statistiką
			//echo 'Multiple kid versions:';
			$result = db_query("SELECT MIN(`parent_kid_id`) `parent_kid_id`, COUNT(*) as `CNT`, MAX(`created`) AS Mcreated, GREATEST(MAX(`created`), MAX(`updated`)) MAX_DATE FROM `".$prefix."vaikai` GROUP BY `parent_kid_id` HAVING COUNT(*)>1");//60-70ms
			while ($row = mysqli_fetch_assoc($result)) {
				echo str_pad($i++, 5, ' ', STR_PAD_RIGHT).'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; Kid main id: ".str_pad($row['parent_kid_id'], 3, ' ', STR_PAD_RIGHT)."; versions: ".$row['CNT']."; Max created: ".$row['Mcreated'].'; MAX: '.$row['MAX_DATE'];
				if(isset($checked_kid[$prefix][$row['parent_kid_id']]) && $row['MAX_DATE'] == $checked_kid[$prefix][$row['parent_kid_id']]) echo " OK";
				elseif(isset($checked_kid[$prefix][$row['parent_kid_id']]) && $row['MAX_DATE'] != $checked_kid[$prefix][$row['parent_kid_id']]) echo " RECHECK";
		
				if($row['CNT'] == 2) {
					$res = db_query("SELECT `ID` FROM `".$prefix."vaikai` WHERE `parent_kid_id`=".$row['parent_kid_id']);
					$f = mysqli_fetch_assoc($res)['ID'];
					$second = mysqli_fetch_assoc($res)['ID'];
					echo ' <a href="/private/tools/kid_version_diff.php?first='.$f.'&second='.$second.'&prefix='.$prefix.'">Diff</a>';
				} else {
					$res = db_query("SELECT `ID` FROM `".$prefix."vaikai` WHERE `parent_kid_id`=".$row['parent_kid_id']);
					while($r = mysqli_fetch_assoc($res))
						echo ' '.$r['ID'];
				}
				echo "\n";
			}
			if(isset($_GET['deleted'])) {
				$result = db_query("SELECT * FROM `".$prefix."vaikai` WHERE `isDeleted`=1");//
				while ($row = mysqli_fetch_assoc($result)) {
					if(!isset($vain_deletion[$prefix][$row['parent_kid_id']])) {
						$grey = ($row['created'] < '2014-11-27' && $row['updated'] < '2014-11-27' && $row['deleted'] < '2014-11-27' ? ' style="color: grey"' : '');
						$res = db_query("SELECT COUNT(*) AS `SUM` FROM `".$prefix."lankomumas` WHERE `vaiko_id`=".$row['parent_kid_id']." GROUP BY `vaiko_id`");
						if ($r = mysqli_fetch_assoc($res))
							echo '<del'.$grey.'>DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; Kid main id: ".str_pad($row['parent_kid_id'], 3, ' ', STR_PAD_RIGHT)."; ID: ".str_pad($row['ID'], 3, ' ', STR_PAD_RIGHT)."; created: ".$row['created'].'; updated: '.$row['updated'].'; deleted: '.$row['deleted'].'; Attendance: '.$r['SUM'].'</del>';
						else
							echo '<del'.$grey.'>DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; Kid main id: ".str_pad($row['parent_kid_id'], 3, ' ', STR_PAD_RIGHT)."; ID: ".str_pad($row['ID'], 3, ' ', STR_PAD_RIGHT)."; created: ".$row['created'].'; updated: '.$row['updated'].'; deleted: '.$row['deleted'].'</del>';
						echo "\n";
					}
				}
			}
		}
	
		/*$result = db_query("SELECT f.*, s.ID `sID`, s.`created` `screated`,  s.parent_kid_id `sparent_kid_id`, s.isDeleted `sisDeleted`
		FROM `".$prefix."vaikai` f JOIN `".$prefix."vaikai` s 
		ON TRIM(f.vardas)=TRIM(s.vardas) AND TRIM(f.pavarde)=TRIM(s.pavarde) AND f.parent_kid_id!=s.parent_kid_id");//
		while ($row = mysqli_fetch_assoc($result)) {
			echo '<span style="color: red">DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; Kid main id: <a href=\"kid_info_diff.php?kid_id=${row['parent_kid_id']}&amp;prefix=$prefix\">".str_pad($row['parent_kid_id'], 3, ' ', STR_PAD_RIGHT)."</a>; ID: ".str_pad($row['ID'], 3, ' ', STR_PAD_RIGHT)."; <a href=\"/private/tools/kid_version_diff.php?first=${row['parent_kid_id']}&amp;second=${row['sparent_kid_id']}&amp;prefix=$prefix\">Diff</a> Kid main id: ".str_pad($row['sparent_kid_id'], 3, ' ', STR_PAD_RIGHT)."; ID: ".str_pad($row['sID'], 3, ' ', STR_PAD_RIGHT)."; created: ".$row['created'].'; created: '.$row['screated'].'; isDeleted: '.$row['isDeleted'].'; isDeleted: '.$row['sisDeleted'].'</span>'."\n";
		}*/
		if($prefix != 't_' && $prefix != 'r_') {
			//$result = db_query("SELECT * FROM `".$prefix."vaikai` WHERE `valid_from`>='2014-10-03'");
			//while ($row = mysqli_fetch_assoc($result))
			//	echo 'VALID DB: '.$prefix."; Kid: ".$row['parent_kid_id']."; \n";
			//$result = db_query("SELECT * FROM `".$prefix."vaikai` WHERE DATE(`created`)>='2014-10-03'");
			//while ($row = mysqli_fetch_assoc($result))
			//	echo 'VALID DATE DB: '.$prefix."; Kid: ".$row['parent_kid_id']."; created: ".$row['created']."; \n";
		}
	}
	echo round(microtime(true)-$TempStart, 3);
}
function only_kindergartens($prefix) {
	return $prefix != 't_' && $prefix != 'r_' && /*substr($prefix, 0, 4) != 'semi' &&*/ $prefix != 'naujas_';
}
function showInStats($prefix) {
	return $prefix != 't_' && $prefix != 'r_' && /*substr($prefix, 0, 4) != 'semi' &&*/ $prefix != 's_' && $prefix != 'naujas_';
}
function no($i) {
	return str_pad($i, 4, ' ', STR_PAD_RIGHT);
}

$TempStart = microtime(true);
$days_if_not_using = 3;
echo "\nLast login (started to log from 2014-10-27 19:57:06): <span style=\"color: green;\">GREEN IF LESS than $days_if_not_using days from last login</span>\n";
$cnt = 0;
$cntTotal = 0;
$cntTotalLogedAtLeastOnce = 0;

$invoices = [];
$result = db_query("SELECT * FROM `0crm_kindergartens`");
while($row = mysqli_fetch_assoc($result))
	if($row['generate_invoice'])
		$invoices[$row['prefix']] = $row;
		
$i = 1;
$sf = 0;
$login_counter = 0;
foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT MAX(`last_login_time`) `last_login_time`, DATE(MIN(`created`)) `created`, SUM(`login_counter`) `login_counter` FROM `".$prefix."users`");
	if ($row = mysqli_fetch_assoc($result)) {
		$seconds_diff = time() - strtotime($row['created']);
		$seconds_diff_last = '';
		if($row['last_login_time'] != '0000-00-00 00:00:00')
			$seconds_diff_last = (time() - strtotime($row['last_login_time']))/(3600*24);
		$is_using = ($seconds_diff_last != '' && $seconds_diff_last < $days_if_not_using);
		if($is_using)
			++$cnt;
		echo ($is_using ? '<span style="color: green;">' : '').no($i++).'DB: '.str_pad($prefix, 12, ' ', STR_PAD_RIGHT)."; on: ".substr($row['last_login_time'], 0, -3)."; Passed days: ".str_pad(round($seconds_diff_last, 0), 3, ' ', STR_PAD_RIGHT)."; cnt: ".str_pad($row['login_counter'], 5, ' ', STR_PAD_RIGHT)."; created: ".$row['created']."; Passed days: ".round($seconds_diff/(3600*24), 0).($is_using ? '</span>' : '').(isset($invoices[$prefix]) ? '█SF'.++$sf : '')."\n";
		$login_counter += $row['login_counter'];
		if(showInStats($prefix)) {
			++$cntTotal;
			if($row['last_login_time'] != '0000-00-00 00:00:00')
				++$cntTotalLogedAtLeastOnce;
		}
	}
}
echo "Green cnt ".$cnt."; total $cntTotal; logged in at least once $cntTotalLogedAtLeastOnce; SF $sf; green w/o SF: ".($cnt-$sf)." (login_counter ".$login_counter.")<br>";
echo round(microtime(true)-$TempStart, 3);
flush();

echo "\nDublis:\n";
$i = 0;
$cnt = 0;
foreach($iter_db_prefixes as $prefix) if(showInStats($prefix)) {
	$result = db_query("SELECT *, COUNT(*) `cnt` FROM `".$prefix."users` GROUP BY `email` HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result)) {
		$rows_cnt = mysqli_num_rows($result);
		echo no(++$i).'DB: '.str_pad($prefix, 12, ' ', STR_PAD_RIGHT)."; on: ".substr($row['last_login_time'], 0, -3)."; cnt: ".$rows_cnt." login_counter: ".str_pad($row['login_counter'], 4, ' ', STR_PAD_RIGHT)."; created: ".$row['created']."; email: ".$row['email']."; \n";
		$cnt += $rows_cnt; 
	}
}
echo "Dublis cnt ".$cnt.";\n";//Total: rows 211 => Dublis cnt 13977; rows 24 => Dublis cnt 29;


$TempStart = microtime(true);
echo "\nKids:\n";//~5ms
$i = 0;
$kids = 0;
foreach($iter_db_prefixes as $prefix) if(only_kindergartens($prefix)) {
	if(showInStats($prefix)) {
		$result = db_query("SELECT COUNT(*) AS `SUM` FROM `".$prefix."vaikai` WHERE isDeleted=0 AND archyvas=0 HAVING COUNT(*)>0");
		if ($row = mysqli_fetch_assoc($result)) {
			echo no(++$i).'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; kids: ".$row['SUM']." \n";
			$kids += $row['SUM'];
		}
	}
}
$records['Kids'] = $kids;
$total_kindergartens['Kids'] = $i;
echo "Total: $kids\n";//14'643 -> 12'217 (isDeleted=0 and archyvas=0)
echo round(microtime(true)-$TempStart, 3);

echo "\nKids ages:\n";
foreach($iter_db_prefixes as $prefix) if(only_kindergartens($prefix)) {
	if(showInStats($prefix)) {
		$result = db_query("SELECT MIN(TIMESTAMPDIFF(YEAR, cr.`gim_data`, CURDATE())) AS `min`, MAX(TIMESTAMPDIFF(YEAR, cr.`gim_data`, CURDATE())) `max` FROM `".$prefix."vaikai` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".$prefix."vaikai` WHERE `valid_from`<='".date('Y-m-d')."' GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from` AND cr.`isDeleted`=0 AND cr.`archyvas`=0 AND cr.`gim_data`<>''");
		if ($row = mysqli_fetch_assoc($result)) {
			echo no(++$i).'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; min: ".$row['min']."; max: ".$row['max']." \n";
		}
	}
}


echo "\nAttendance (not normalized):\n";
$i = 0; $s = 0;
$startTMP = microtime(true);
foreach($iter_db_prefixes as $prefix) if(only_kindergartens($prefix)) {
	$result = db_query("SELECT COUNT(*) AS `SUM`, MAX(`data`) as `max`, MIN(`data`) as `min` FROM `".$prefix."lankomumas` HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result)) {
		echo no(++$i).'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; attendance marked: ".str_pad($row['SUM'], 5, ' ', STR_PAD_RIGHT)."; max: ".$row['max']." ; min: ".$row['min']." \n";
		$s += $row['SUM'];
	}
		
}
$records['Attendance (not normalized)'] = $s;
$total_kindergartens['Attendance (not normalized)'] = $i;
echo round(microtime(true)-$startTMP, 3);



echo "\nAttendance marks:\n";
$i = 0; $s = 0;
$result = db_query("SELECT *, COUNT(*) `CNT` FROM `".DB_attendance_marks."` GROUP BY `kindergarten_id`");
while ($row = mysqli_fetch_assoc($result)) {// if(only_kindergartens($prefix)) {
	echo no(++$i).'DB: '.str_pad(isset($db_prefixes[$row['kindergarten_id']]) ? $db_prefixes[$row['kindergarten_id']] : 'N/A', 11, ' ', STR_PAD_RIGHT)/*str_pad($prefix, 11, ' ', STR_PAD_RIGHT)*/."; attendance marks: ".str_pad($row['CNT'], 5, ' ', STR_PAD_RIGHT).";\n";
	$s += $row['CNT'];
}
$records['Attendance marks'] = $s;
$total_kindergartens['Attendance marks'] = $i;

echo "\nWorkers:\n";
$i = 0; $s = 0;
$cnt = 0;
foreach($iter_db_prefixes as $prefix) if(only_kindergartens($prefix)) {
	if(showInStats($prefix)) {
		$result = db_query("SELECT COUNT(*) AS `SUM`, MIN(`created`) `created` FROM `".$prefix."darbuotojai` HAVING COUNT(*)>0");
		if ($row = mysqli_fetch_assoc($result)) {
			echo no(++$i).' DB: '.str_pad($prefix, 13, ' ', STR_PAD_RIGHT)."; workers: ".str_pad($row['SUM'], 2, ' ', STR_PAD_RIGHT)."; min created: ".$row['created']." \n";
			if($row['SUM'] > 1) ++$cnt;
			$s += $row['SUM'];
		}
	}
}
$records['Workers'] = $s;
$total_kindergartens['Workers'] = $i;
echo '# of workers with more than one person '.$cnt."\n";

$pareigybes_cnt = [];
foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT `pareigybes`, COUNT(*) AS `SUM` FROM `".$prefix."darbuotojai` GROUP BY `pareigybes`");
	while ($row = mysqli_fetch_assoc($result)) {
		if(!isset($pareigybes_cnt[$row['pareigybes']]))
			$pareigybes_cnt[$row['pareigybes']] = 0;
		$pareigybes_cnt[$row['pareigybes']] += $row['SUM'];
	}
}
print_r($pareigybes_cnt);


echo "\nGroups:\n";
$i = 0; $s = 0;
foreach($iter_db_prefixes as $prefix) if(only_kindergartens($prefix)) {
	$result = db_query("SELECT COUNT(*) AS `SUM` FROM `".$prefix."grupes` HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result)) {
		echo no(++$i).'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; groups: ".$row['SUM']." \n";
		$s += $row['SUM'];
	}
}
$records['Groups'] = $s;
$total_kindergartens['Groups'] = $i;


echo "\nNew planning:\n";//10ms
$i = 1; $j = 1;
foreach($iter_db_prefixes as $prefix) if(only_kindergartens($prefix)) {
	$result = db_query("SELECT SUM(`isPublished`) AS `SUM` FROM `".$prefix."forms` HAVING SUM(`isPublished`)>0");
	if ($row = mysqli_fetch_assoc($result))
			echo no($i++).'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; published: ".$row['SUM']." \n";
	$result = db_query("SELECT COUNT(*) as `CNT` FROM `".$prefix."forms_fields_answer` HAVING COUNT(*)>0");
	if ($row = mysqli_fetch_assoc($result))
			echo no($j++).'+DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; plans WRITTEN: ".$row['CNT']." \n";
}

echo "\nOld planning:\n";//10ms
$i = 1;
foreach($iter_db_prefixes as $prefix) if(only_kindergartens($prefix)) {
	$result = mysqli_query($db_link, "SELECT 1 FROM `".$prefix."planning` LIMIT 1");
	if($result) {
		$result = db_query("SELECT COUNT(*) as `CNT` FROM `".$prefix."planning` HAVING COUNT(*)>0");
		if ($row = mysqli_fetch_assoc($result))
				echo no($i++).'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; plans WRITTEN: ".$row['CNT']." \n";
	}
}
echo "\nPlanning widgets:\n";//?ms
$i = 0; $s = 0;
$result = db_query("SELECT `kindergarten_id`, COUNT(*) as `CNT` FROM `1forms_widget` GROUP BY `kindergarten_id`");
while ($row = mysqli_fetch_assoc($result)) {
	echo no(++$i).'DB: '.str_pad(isset($db_prefixes[$row['kindergarten_id']]) ? $db_prefixes[$row['kindergarten_id']] : 'N/A', 11, ' ', STR_PAD_RIGHT)."; table : ".$row['CNT']." \n";
	$s += $row['CNT'];
}
$records['Planning widgets'] = $s;
$total_kindergartens['Planning widgets'] = $i;
	

echo "\nPlanning widget fields allowed:\n";//?ms
$i = 1;
$result = db_query("SELECT `kindergarten_id`, COUNT(*) as `CNT` FROM `1forms_widget_fields_allowed` GROUP BY `kindergarten_id`");
while ($row = mysqli_fetch_assoc($result))
	echo no($i++).'DB: '.str_pad(isset($db_prefixes[$row['kindergarten_id']]) ? $db_prefixes[$row['kindergarten_id']] : 'N/A', 11, ' ', STR_PAD_RIGHT).$row['kindergarten_id']."; table : ".$row['CNT']." \n";


echo "\nActivities:\n";//?ms
$i = 0; $s = 0;
$result = db_query("SELECT `kindergarten_id`, COUNT(*) as `CNT` FROM `1activities` GROUP BY `kindergarten_id`");
while ($row = mysqli_fetch_assoc($result)) {
	echo no(++$i).'DB: '.str_pad(isset($db_prefixes[$row['kindergarten_id']]) ? $db_prefixes[$row['kindergarten_id']] : 'N/A', 11, ' ', STR_PAD_RIGHT)."; activities : ".$row['CNT']." \n";
	$s += $row['CNT'];
}
$records['Activities'] = $s;
$total_kindergartens['Activities'] = $i;

echo "\nReminders:\n";//10ms
$i = 0; $j = 0; $s = 0; $s2 = 0;
foreach($iter_db_prefixes as $prefix) if(only_kindergartens($prefix)) {
	$result = db_query("SELECT COUNT(*) as `CNT` FROM `".$prefix."reminder` HAVING COUNT(*)>0");
	while ($row = mysqli_fetch_assoc($result)) {
		//if($row['CNT'])
		echo no(++$i).'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; reminders: ".$row['CNT']." \n";
		$s += $row['CNT'];
	}
	$result = db_query("SELECT COUNT(*) as `CNT` FROM `".$prefix."reminder_sent` HAVING COUNT(*)>0");
	while ($row = mysqli_fetch_assoc($result)) {
		//if($row['CNT'])
			echo no(++$j).'+DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; reminders SENT: ".$row['CNT']." \n";
		$s2 += $row['CNT'];
	}
}
$records['Reminders'] = $s;
$total_kindergartens['Reminders'] = $i;
$records['Reminders sent'] = $s2;
$total_kindergartens['Reminders sent'] = $j;

echo "\nKid's documents:\n";//~5ms
$i = 0; $s = 0;
foreach($iter_db_prefixes as $prefix) if(only_kindergartens($prefix)) {
	$result = db_query("SELECT COUNT(*) AS `SUM` FROM `".$prefix."dokumentai` HAVING COUNT(*)>0");
	if ($row = mysqli_fetch_assoc($result)) {
		echo no(++$i).'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; docs: ".$row['SUM']." \n";
		$s += $row['SUM'];
	}
}
$records['Kid\'s documents'] = $s;
$total_kindergartens['Kid\'s documents'] = $i;

echo "\nCommon documents:\n";//~5ms
$i = 0; $s = 0;
//foreach($iter_db_prefixes as $prefix) if(only_kindergartens($prefix)) {
	$result = db_query("SELECT `kindergarten_id`, COUNT(*) AS `SUM` FROM `1documents_common` GROUP BY `kindergarten_id`");// HAVING COUNT(*)>1
	while ($row = mysqli_fetch_assoc($result)) {
		echo no(++$i).'DB: '.str_pad((isset($db_prefixes[$row['kindergarten_id']]) ? $db_prefixes[$row['kindergarten_id']] : 'N/A'), 11, ' ', STR_PAD_RIGHT)."; docs: ".$row['SUM']." \n";
		$s += $row['SUM'];
	}
//}
$records['Common documents'] = $s;
$total_kindergartens['Common documents'] = $i;

echo "\nSchedule of activities with specialists:\n";
$i = 0; $s = 0;
foreach($iter_db_prefixes as $prefix) if(only_kindergartens($prefix)) {
	if(mysqli_query($db_link, "SELECT 1 FROM `".$prefix."bureliai_schedule` LIMIT 1")) {
		$result = db_query("SELECT COUNT(*) AS `SUM` FROM `".$prefix."bureliai_schedule` HAVING COUNT(*)>1");
		if ($row = mysqli_fetch_assoc($result)) {
			echo no(++$i).'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; groups activities: ".$row['SUM']." \n";
			$s += $row['SUM'];
		}
	}
}
$records['Schedule of activities with specialists'] = $s;
$total_kindergartens['Schedule of activities with specialists'] = $i;

echo "\nMessages:\n";
$i = 0; $s = 0;
foreach($iter_db_prefixes as $prefix) if(only_kindergartens($prefix)) {
	$result = db_query("SELECT COUNT(*) AS `SUM` FROM `".$prefix."messages` HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result)) {
		echo no(++$i).'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; sent messages: ".$row['SUM']." \n";
		$s += $row['SUM'];
	}
}
$records['Messages'] = $s;
$total_kindergartens['Messages'] = $i;

echo "\nGroup emails:\n";
$i = 0; $s = 0;
//foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT `kindergarten_id`, COUNT(*) AS `SUM` FROM `1masinis` GROUP BY `kindergarten_id`");// HAVING COUNT(*)>1
	while ($row = mysqli_fetch_assoc($result)) {
		echo no(++$i).'DB: '.str_pad((isset($db_prefixes[$row['kindergarten_id']]) ? $db_prefixes[$row['kindergarten_id']] : 'N/A'), 11, ' ', STR_PAD_RIGHT)."; sent emails: ".$row['SUM']." \n";
		$s += $row['SUM'];
	}
//}
$records['Group emails'] = $s;
$total_kindergartens['Group emails'] = $i;


echo "\nDocs / texts:\n";
$texts = [];
$i = 0; $s = 0;
//foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT `kindergarten_id`, COUNT(*) AS `SUM` FROM `1docs` GROUP BY `kindergarten_id`");// HAVING COUNT(*)>1
	while ($row = mysqli_fetch_assoc($result)) {
		echo no(++$i).'DB: '.str_pad($db_prefixes[$row['kindergarten_id']], 11, ' ', STR_PAD_RIGHT)."; Texts: ".$row['SUM']." \n";
		$s += $row['SUM'];
		$texts[$db_prefixes[$row['kindergarten_id']]] = ['texts' => $row['SUM']];
	}
	$result = db_query("SELECT `group_id`, COUNT(*) AS `SUM` FROM `1docs` GROUP BY `group_id`");// HAVING COUNT(*)>1
	while ($row = mysqli_fetch_assoc($result)) {
		echo 'group_id: '.str_pad($row['group_id'], 3, ' ', STR_PAD_RIGHT)."; cnt: ".$row['SUM']." \n";
		$s += $row['SUM'];
	}
$records['Docs / texts'] = $s;
$total_kindergartens['Docs / texts'] = $i;

echo "\nDocs / texts templates:\n";
$i = 0; $s = 0;
//foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT `kindergarten_id`, COUNT(*) AS `SUM` FROM `1docs_templates` GROUP BY `kindergarten_id`");// HAVING COUNT(*)>1
	while ($row = mysqli_fetch_assoc($result)) {
		echo no(++$i).'DB: '.str_pad($db_prefixes[$row['kindergarten_id']], 11, ' ', STR_PAD_RIGHT)."; Templates: ".$row['SUM']." \n";
		$s += $row['SUM'];
		$texts[$db_prefixes[$row['kindergarten_id']]]['templates'] = $row['SUM'];
		if(isset($texts[$db_prefixes[$row['kindergarten_id']]]['texts']))
			$texts[$db_prefixes[$row['kindergarten_id']]]['ratio'] = round($texts[$db_prefixes[$row['kindergarten_id']]]['texts']/$row['SUM'], 2);
	}
$records['Docs / texts templates'] = $s;
$total_kindergartens['Docs / texts templates'] = $i;

$i = 0;
foreach($texts as $key => $val)
	echo no(++$i).str_pad($key, 11, ' ', STR_PAD_RIGHT)."; Texts: ".str_pad(isset($val['texts']) ? $val['texts'] : '', 3, ' ', STR_PAD_RIGHT)."; Templates: ".str_pad(isset($val['templates']) ? $val['templates'] : '', 3, ' ', STR_PAD_RIGHT)."; Ratio: ".str_pad(isset($val['ratio']) ? $val['ratio'] : '', 4, ' ', STR_PAD_RIGHT)." \n";
	
echo "\nNew achievements and progress:\n";
$i = 0; $s = 0;
//foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT `kindergarten_id`, COUNT(*) AS `SUM` FROM `1kid_level` GROUP BY `kindergarten_id`");// HAVING COUNT(*)>1
	while ($row = mysqli_fetch_assoc($result)) {
		echo no(++$i).'DB: '.str_pad((isset($db_prefixes[$row['kindergarten_id']]) ? $db_prefixes[$row['kindergarten_id']] : 'N/A'), 11, ' ', STR_PAD_RIGHT)."; Achievements: ".$row['SUM']." \n";
		$s += $row['SUM'];
	}
$records['New achievements and progress'] = $s;
$total_kindergartens['New achievements and progress'] = $i;

echo "\nAchievements and progress:\n";
$i = 0; $s = 0;
foreach($iter_db_prefixes as $prefix) if(only_kindergartens($prefix)) {
	$result = db_query("SELECT COUNT(*) AS `SUM` FROM `".$prefix."pasiekimai` HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result)) {
		echo no(++$i).'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; Achievements: ".$row['SUM']." \n";
		$s += $row['SUM'];
	}
}
$records['Achievements and progress'] = $s;
$total_kindergartens['Achievements and progress'] = $i;
/*abstract class ProgressAndAchievementsFieldType
{
    const Note = 1;
    const Achievement = 0;
}
echo "\nMore than one achievement:\n";
foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT COUNT(*) AS `SUM`, `vaiko_id` FROM `".$prefix."pasiekimai` WHERE `type`=".ProgressAndAchievementsFieldType::Achievement." GROUP BY `vaiko_id`, `data` HAVING COUNT(*)>1");
	while ($row = mysqli_fetch_assoc($result))
		echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; More achievements: ".$row['SUM']."; Kid id: ".$row['vaiko_id']." \n";
}*/

echo "\nHealth observations
(teachers see this menu item only of these DB [last updated 2014-12-08]:
TESTINIS || RODOMASIS || NAUJAS || KaunoZiburelis || KaunoRudnosiukas || SiauliuZiogelis || LazdijuRZibute || TauragesRAzuoliukas || KaunoVaikystesLobiai):\n";//10ms
$i = 1;
foreach($iter_db_prefixes as $prefix) if(only_kindergartens($prefix)) {
	//$result = mysqli_query($db_link, "SELECT 1 FROM `".$prefix."health_observations` LIMIT 1");
	//if($result) {
		$result = db_query("SELECT COUNT(*) as `CNT` FROM `".$prefix."health_observations`");
		if ($row = mysqli_fetch_assoc($result))
			if($row['CNT'] > 0)
			echo no($i++).'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; health observations: ".$row['CNT']." \n";
	//} else
	//	echo no($i++).'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; denied\n";
}
/*
5   DB: knsrBaibok_; denied
6   DB: lzdVytur_  ; denied
8   DB: sauPasak_  ; denied
10  DB: lzdKreg_   ; denied
13  DB: kretAzuol_ ; denied
16  DB: molVytur_  ; denied
*/



echo "\nDaily rhythm (individualized by group):\n";
$i = 0; $s = 0;
//foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT `kindergarten_id`, COUNT(*) AS `SUM`, `created` FROM `1daily_rhythm` GROUP BY `kindergarten_id`");// HAVING COUNT(*)>1
	while ($row = mysqli_fetch_assoc($result)) {
		echo no(++$i).'DB: '.str_pad((isset($db_prefixes[$row['kindergarten_id']]) ? $db_prefixes[$row['kindergarten_id']] : 'N/A'), 11, ' ', STR_PAD_RIGHT)."; daily rhythms: ".str_pad($row['SUM'], 3, ' ', STR_PAD_RIGHT)."; created: ".$row['created']." \n";
		$s += $row['SUM'];
	}
//}
$records['Daily rhythm (individualized by group)'] = $s;
$total_kindergartens['Daily rhythm (individualized by group)'] = $i;

echo "\nFood menu:\n";
$i = 0; $s = 0;
//foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT `kindergarten_id`, COUNT(*) AS `SUM` FROM `1food_menu` GROUP BY `kindergarten_id`");// HAVING COUNT(*)>1
	while ($row = mysqli_fetch_assoc($result)) {
		echo no(++$i).'DB: '.str_pad(isset($db_prefixes[$row['kindergarten_id']]) ? $db_prefixes[$row['kindergarten_id']] : 'N/A', 11, ' ', STR_PAD_RIGHT)."; Food menus: ".$row['SUM']." \n";
		$s += $row['SUM'];
	}
//}
$records['Food menu'] = $s;
$total_kindergartens['Food menu'] = $i;

echo "\nSpeech therapist kids:\n";
$i = 0; $s = 0;
foreach($iter_db_prefixes as $prefix) if(only_kindergartens($prefix)) {
	$result = db_query("SELECT COUNT(*) AS `SUM`, `created` FROM `".$prefix."speech_kids` HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result)) {
		echo no(++$i).'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; cnt: ".str_pad($row['SUM'], 3, ' ', STR_PAD_RIGHT)."; created: ".$row['created']." \n";
		$s += $row['SUM'];
	}
}
$records['Speech therapist kids'] = $s;
$total_kindergartens['Speech therapist kids'] = $i;

echo "\nSpeech therapist groups_kids:\n";
$i = 0; $s = 0;
foreach($iter_db_prefixes as $prefix) if(only_kindergartens($prefix)) {
	$result = db_query("SELECT COUNT(*) AS `SUM`, `created` FROM `".$prefix."speech_groups_kids` HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result)) {
		echo no(++$i).'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; cnt: ".str_pad($row['SUM'], 3, ' ', STR_PAD_RIGHT)."; created: ".$row['created']." \n";
		$s += $row['SUM'];
	}
}
$records['Speech therapist groups_kids'] = $s;
$total_kindergartens['Speech therapist groups_kids'] = $i;

echo "\nSpeech therapist groups:\n";
$i = 0; $s = 0;
foreach($iter_db_prefixes as $prefix) if(only_kindergartens($prefix)) {
	$result = db_query("SELECT COUNT(*) AS `SUM`, `created` FROM `".$prefix."speech_groups` HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result)) {
		echo no(++$i).'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; cnt: ".str_pad($row['SUM'], 3, ' ', STR_PAD_RIGHT)."; created: ".$row['created']." \n";
		$s += $row['SUM'];
	}
}
$records['Speech therapist groups'] = $s;
$total_kindergartens['Speech therapist groups'] = $i;

echo "\nSpeech therapist attendance:\n";
$i = 0; $s = 0;
foreach($iter_db_prefixes as $prefix) if(only_kindergartens($prefix)) {
	$result = db_query("SELECT COUNT(*) AS `SUM`, `created` FROM `".$prefix."speech_attendance` HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result)) {
		echo no(++$i).'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; cnt: ".str_pad($row['SUM'], 3, ' ', STR_PAD_RIGHT)."; created: ".$row['created']." \n";
		$s += $row['SUM'];
	}
}
$records['Speech therapist attendance'] = $s;
$total_kindergartens['Speech therapist attendance'] = $i;


echo "\nSpeech therapist attendance_topic:\n";
$i = 0; $s = 0;
foreach($iter_db_prefixes as $prefix) if(only_kindergartens($prefix)) {
	$result = db_query("SELECT COUNT(*) AS `SUM`, `created` FROM `".$prefix."speech_attendance_topic` HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result)) {
		echo no(++$i).'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; cnt: ".str_pad($row['SUM'], 3, ' ', STR_PAD_RIGHT)."; created: ".$row['created']." \n";
		$s += $row['SUM'];
	}
}
$records['Speech therapist other_activities'] = $s;
$total_kindergartens['Speech therapist other_activities'] = $i;

echo "\nSpeech therapist other_activities:\n";
$i = 0; $s = 0;
foreach($iter_db_prefixes as $prefix) if(only_kindergartens($prefix)) {
	$result = db_query("SELECT COUNT(*) AS `SUM`, `created` FROM `".$prefix."speech_other_activities` HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result)) {
		echo no(++$i).'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; cnt: ".str_pad($row['SUM'], 3, ' ', STR_PAD_RIGHT)."; created: ".$row['created']." \n";
		$s += $row['SUM'];
	}
}
$records['Speech therapist other_activities'] = $s;
$total_kindergartens['Speech therapist other_activities'] = $i;

echo "\nSpeech therapist schedule:\n";
$i = 0; $s = 0;
foreach($iter_db_prefixes as $prefix) if(only_kindergartens($prefix)) {
	$result = db_query("SELECT COUNT(*) AS `SUM`, `created` FROM `".$prefix."speech_schedule` HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result)) {
		echo no(++$i).'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; cnt: ".str_pad($row['SUM'], 3, ' ', STR_PAD_RIGHT)."; created: ".$row['created']." \n";
		$s += $row['SUM'];
	}
}
$records['Speech therapist schedule'] = $s;
$total_kindergartens['Speech therapist schedule'] = $i;

echo "\nTabaliai charge:\n";
$i = 1;
foreach($iter_db_prefixes as $prefix) if(only_kindergartens($prefix)) {
	$result = db_query("SELECT COUNT(*) AS `SUM`, `created` FROM `".$prefix."tabeliai_charge` HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result))
		echo no($i++).'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; charges: ".str_pad($row['SUM'], 3, ' ', STR_PAD_RIGHT)."; created: ".$row['created']." \n";
}
echo "\nKids fee for place:\n";
$i = 0;
foreach($iter_db_prefixes as $prefix) if(only_kindergartens($prefix)) {
	$result = db_query("SELECT COUNT(*) AS `SUM`, `created` FROM `".$prefix."vaikai` WHERE `feeForPlace`<>0 HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result))
		echo no(++$i).'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; feeForPlace: ".str_pad($row['SUM'], 3, ' ', STR_PAD_RIGHT)."; created: ".$row['created']." \n";
}

echo "\nKids Mizaras:\n";
$i = 0;
foreach($iter_db_prefixes as $prefix) if(only_kindergartens($prefix)) {
	$result = db_query("SELECT COUNT(*) AS `SUM`, `created` FROM `".$prefix."vaikai` WHERE `pavarde` LIKE  '%Mizaras%' HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result))
		echo no(++$i).'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; Mizaras: ".str_pad($row['SUM'], 3, ' ', STR_PAD_RIGHT)."; created: ".$row['created']." \n";
}


/*foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT COUNT(*) AS `SUM`, `created` FROM `".$prefix."vaikai` WHERE `feeForPlace`<>0 HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result))
		echo "'".$prefix."', ";
}*/

echo "\nNotes:\n";//?ms
$i = 0; $s = 0;
$result = db_query("SELECT `kindergarten_id`, COUNT(*) as `CNT` FROM `1notes` GROUP BY `kindergarten_id`");
while ($row = mysqli_fetch_assoc($result)) {
	echo no(++$i).'DB: '.str_pad($db_prefixes[$row['kindergarten_id']], 11, ' ', STR_PAD_RIGHT)."; cnt : ".$row['CNT']." \n";
	$s += $row['CNT'];
}
$records['Notes'] = $s;
$total_kindergartens['Notes'] = $i;

echo "\nWorkers food:\n";
$i = 0; $s = 0;
foreach($iter_db_prefixes as $prefix) if(only_kindergartens($prefix)) {
	$result = db_query("SELECT COUNT(*) AS `SUM`, MIN(`created`) MIN_created, MAX(`created`) MAX_created FROM `".$prefix."worker_food_ate` HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result)) {
		echo no(++$i).'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; worker_food_ate: ".str_pad($row['SUM'], 3, ' ', STR_PAD_RIGHT)."; created: ".$row['MIN_created']."; last: ".$row['MAX_created']." \n";
		$s += $row['SUM'];
	}
}
$records['Workers food'] = $s;
$total_kindergartens['Workers food'] = $i;

echo "\nPreordered workers food:\n";
$i = 0; $s = 0;
foreach($iter_db_prefixes as $prefix) if(only_kindergartens($prefix)) {
	$result = db_query("SELECT COUNT(*) AS `SUM`, MIN(`created`) MIN_created, MAX(`created`) MAX_created FROM `".$prefix."worker_food_ate` WHERE `orderedDate` IS NOT NULL HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result)) {
		echo no(++$i).'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; worker_food_ate: ".str_pad($row['SUM'], 3, ' ', STR_PAD_RIGHT)."; created: ".$row['MIN_created']."; last: ".$row['MAX_created']." \n";
		$s += $row['SUM'];
	}
}
//$records['Preordered workers food'] = $s;
//$total_kindergartens['Preordered workers food'] = $i;

echo "\nMoment activities:\n";//?ms
$i = 0; $s = 0;
$result = db_query("SELECT `kindergarten_id`, COUNT(*) as `CNT` FROM `1moment_activity` GROUP BY `kindergarten_id`");
while ($row = mysqli_fetch_assoc($result)) {
	echo no(++$i).'DB: '.str_pad($db_prefixes[$row['kindergarten_id']], 11, ' ', STR_PAD_RIGHT)."; cnt : ".$row['CNT']." \n";
	$s += $row['CNT'];
}
$records['Moment activities'] = $s;
$total_kindergartens['Moment activities'] = $i;

echo "\nMoment activities photos:\n";//?ms
$i = 0; $s = 0;
$result = db_query("SELECT `kindergarten_id`, COUNT(*) as `CNT` FROM `1moment_photos` JOIN `1moment_activity` ON `1moment_activity`.`ID`=`1moment_photos`.`moment_activity_id` GROUP BY `kindergarten_id`");
while ($row = mysqli_fetch_assoc($result)) {
	echo no(++$i).'DB: '.str_pad($db_prefixes[$row['kindergarten_id']], 11, ' ', STR_PAD_RIGHT)."; cnt : ".$row['CNT']." \n";
	$s += $row['CNT'];
}
$records['Moment activities photos'] = $s;
$total_kindergartens['Moment activities photos'] = $i;

echo "\nWorks reminders:\n";//?ms
$i = 0; $s = 0;
$result = db_query("SELECT `kindergarten_id`, COUNT(*) as `CNT` FROM `1reminders` GROUP BY `kindergarten_id`");
while ($row = mysqli_fetch_assoc($result)) {
	echo no(++$i).'DB: '.str_pad($db_prefixes[$row['kindergarten_id']], 11, ' ', STR_PAD_RIGHT)."; cnt : ".$row['CNT']." \n";
	$s += $row['CNT'];
}
$records['Works reminders'] = $s;
$total_kindergartens['Works reminders'] = $i;

echo "\n1ikimokyklinis_lt:\n";//?ms
$i = 0; $s = 0;
$result = db_query("SELECT `kindergarten_id`, COUNT(*) as `CNT` FROM `1ikimokyklinis_lt` GROUP BY `kindergarten_id`");
while ($row = mysqli_fetch_assoc($result)) {
	echo no(++$i).'DB: '.str_pad($db_prefixes[$row['kindergarten_id']], 11, ' ', STR_PAD_RIGHT)."; cnt : ".$row['CNT']." \n";
	$s += $row['CNT'];
}
$records['1ikimokyklinis_lt'] = $s;
$total_kindergartens['1ikimokyklinis_lt'] = $i;

echo "\n1ikimokyklinis_lt_click:\n";//?ms
$i = 0; $s = 0;
$result = db_query("SELECT `kindergarten_id`, COUNT(*) as `CNT` FROM `1ikimokyklinis_lt_click` GROUP BY `kindergarten_id`");
while ($row = mysqli_fetch_assoc($result)) {
	echo no(++$i).'DB: '.str_pad($db_prefixes[$row['kindergarten_id']], 11, ' ', STR_PAD_RIGHT)."; cnt : ".$row['CNT']." \n";
	$s += $row['CNT'];
}
$records['1ikimokyklinis_lt_click'] = $s;
$total_kindergartens['1ikimokyklinis_lt_click'] = $i;


echo "\nAdditional columns set in config:\n";
foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT COUNT(*) AS `CNT` FROM  `".$prefix."config` WHERE 
		`title`='tabelyje_papildomo_stulpelio_nr._1_pavadinimas' AND `value`<>'' OR
		`title`='tabelyje_papildomo_stulpelio_nr._1_apačioje_ar_skaičiuoti_sumą/kiekį' AND `value`<>'' OR
		`title`='tabelyje_papildomo_stulpelio_nr._2_pavadinimas' AND `value`<>'' OR
		`title`='tabelyje_papildomo_stulpelio_nr._2_apačioje_ar_skaičiuoti_sumą/kiekį' AND `value`<>'' OR
		`title`='tabelyje_papildomo_stulpelio_nr._3_pavadinimas' AND `value`<>'' OR
		`title`='tabelyje_papildomo_stulpelio_nr._3_apačioje_ar_skaičiuoti_sumą/kiekį' AND `value`<>''
		HAVING COUNT(*)>1");
	while ($row = mysqli_fetch_assoc($result))
		echo 'DB:'.$prefix."; ${row['CNT']} \n";
}

$result = db_query("SELECT `id`, `allowed_parents_access`, `plan_to_allow_parents` FROM `0db`");
$allowed_parents_access = [];
$plan_to_allow_parents = [];
while ($row = mysqli_fetch_assoc($result)) {
	$allowed_parents_access[$row['id']] = $row['allowed_parents_access'];
	$plan_to_allow_parents[$row['id']] = $row['plan_to_allow_parents'];
}
$i = 0;
echo "\nParents menu (knsZibur_ po naujų):\n";
$result = db_query("SELECT `kindergarten_id`, COUNT(*) as `CNT`, MIN(`created`) `MINcreated`, MAX(`created`) `MAXcreated` FROM `1parents_menu_access` GROUP BY `kindergarten_id`");
while ($row = mysqli_fetch_assoc($result))
	echo no(++$i).'DB: '.str_pad($db_prefixes[$row['kindergarten_id']], 11, ' ', STR_PAD_RIGHT)."; cnt : ".str_pad($row['CNT'], 2, ' ', STR_PAD_RIGHT)."; MINcreated : ".$row['MINcreated']."; MAXcreated : ".$row['MAXcreated']."; allowed_parents_access : ".$allowed_parents_access[$row['kindergarten_id']]." plan_to_allow_parents : ".$plan_to_allow_parents[$row['kindergarten_id']]." \n";


/*echo "\nsuggestions:\n";//?ms TODO: changed to a style like "notes"
foreach($iter_db_prefixes as $prefix) {
	if($prefix != 'knsVL_' && $prefix != 'knsVarp_' && $prefix != 'knsZibur_' && $prefix != 'knsRudno_'  && $prefix != 'knsrBaibok_' && $prefix != 'lzdVytur_' && $prefix != 'lzdrAzuol_'  && $prefix != 'sauPasak_' && $prefix != 'sauZiog_' && substr($prefix, 0, 4) != 'semi') {
		$result = db_query("SELECT COUNT(*) AS `SUM`, `created` FROM `".$prefix."suggestions` HAVING COUNT(*)>1");
		if ($row = mysqli_fetch_assoc($result))
			echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; suggestions: ".str_pad($row['SUM'], 3, ' ', STR_PAD_RIGHT)."; created: ".$row['created']." \n";
	}
}*/
/*echo "\nmokejimai:\n";//?ms TODO: changed to a style like "notes"
foreach($iter_db_prefixes as $prefix) {
	if($prefix != 'knsVL_' && $prefix != 'knsVarp_' && $prefix != 'knsZibur_' && $prefix != 'knsRudno_' && $prefix != 'knsrBaibok_' && $prefix != 'lzdVytur_'&& $prefix != 'lzdrAzuol_' && $prefix != 'sauPasak_' && $prefix != 'sauZiog_'&& substr($prefix, 0, 4) != 'semi') {
		$result = db_query("SELECT COUNT(*) AS `SUM` FROM `".$prefix."mokejimai` HAVING COUNT(*)>1");
		if ($row = mysqli_fetch_assoc($result))
			echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; mokejimai: ".str_pad($row['SUM'], 3, ' ', STR_PAD_RIGHT)."; created: ".$row['created']." \n";
	}
}*/

/*
echo "\nlaiskai:\n";//?ms TODO: changed to a style like "notes"
foreach($iter_db_prefixes as $prefix) {
	$table = 'laiskai';
	$result = db_query("SHOW TABLES LIKE '${prefix}$table'");
	if(mysqli_num_rows($result) > 0) {
		$result = db_query("SELECT COUNT(*) AS `SUM` FROM `".$prefix."$table` HAVING COUNT(*)>1");
		if ($row = mysqli_fetch_assoc($result))
			echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; $table: ".str_pad($row['SUM'], 3, ' ', STR_PAD_RIGHT)."; created: ".$row['created']." \n";
	}
}*/

//
//

echo "\nFind email:\n";
foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT * FROM  `".$prefix."users` WHERE  `email` LIKE  'joriukasvoriukas@gmail.com'");//gintaras.puras@gmail.com//rutaalija@gmail.com
	while ($row = mysqli_fetch_assoc($result))
		echo 'DB:'.$prefix."; published: ".print_r($row, 1)." \n";
}



/*
//TODO: check what to do
echo "\nFind config darzelio_nr:\n";
foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT * FROM  `".$prefix."config` WHERE  `title`='darzelio_nr'");
	while ($row = mysqli_fetch_assoc($result))
		echo 'DB:'.$prefix."; ${row['value']} \n";
}
echo "\nFind config banko_skyrius:\n";
foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT * FROM  `".$prefix."config` WHERE  `title`='banko_skyrius'");
	while ($row = mysqli_fetch_assoc($result))
		echo 'DB:'.$prefix."; ${row['value']} \n";
}
*/

/*echo "\nFind darbuotojai:\n";
foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT * FROM `".$prefix."darbuotojai` WHERE `vardas`='Violeta'");
	while ($row = mysqli_fetch_assoc($result))
		echo 'DB:'.$prefix."; ${row['vardas']} ${row['pavarde']} \n";
}*/

/*$result = db_query("SELECT `AUTO_INCREMENT`
FROM  INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA = 'darzelis'
AND   TABLE_NAME   LIKE '%forms'");
	while ($row = mysqli_fetch_assoc($result))
		echo 'DB:'.$prefix."; ${row['AUTO_INCREMENT']} \n";*/
/*
echo "\nFind config:\n";
foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT * FROM  `".$prefix."config` WHERE  `value` LIKE  'LT554010042500020356'");
	while ($row = mysqli_fetch_assoc($result))
		echo 'DB:'.$prefix."; \n";
}
echo "\nFind bug:\n";
foreach($iter_db_prefixes as $prefix) {
	if($prefix != 's_') {
		$result = db_query("SELECT * FROM  `".$prefix."forms_fields_allowed` WHERE `field_allow_id`=8");
		if(mysqli_num_rows($result)) {
			while ($row = mysqli_fetch_assoc($result)) {
				echo 'DB:'.$prefix."; ".$row['allowed_for_person_type']." \n";
				//db_query("UPDATE `".$prefix."forms_fields_allowed` SET `allowed_for_person_type`='1' WHERE `field_allow_id` =8");
			}
		} else {
			//OK: DB:molVytur_ DB:knsZings_
			echo 'DB:'.$prefix."; --\n";
		}
	}
}*/


//SELECT *, INET_NTOA( last_login_ipv4 ) last_login_ipv4 FROM  `t_users`

echo 'records count:';
print_r($records);
echo 'Kindergartens:';
print_r($total_kindergartens);
echo 'Kindergartens (sorted):';
arsort($total_kindergartens);
print_r($total_kindergartens);
echo "\n";
$gen_time = round(microtime(true)-$start, 3);
echo $gen_time;

$debug_info = '';
if(KESV) {
	$debug_info = ' ('.round($db_time, 3).' DB_time '.round($db_time/$gen_time*100).'%_DB_time/gen_time '.$db_query_cnt."_DB_cnt)\n";
	if(isset($_GET['debug']) && arsort($queries)) {
		foreach($queries as $id => $val)
			$debug_info .= str_pad($id, 2, ' ', STR_PAD_LEFT).'='.
				str_pad(round((float)$val/$gen_time*100), 2, ' ', STR_PAD_LEFT).'%gen_time '.
				str_pad(round((float)$val/$db_time*100), 2, ' ', STR_PAD_LEFT).'%db_time '."$val \n";
	}
}
echo $debug_info;
