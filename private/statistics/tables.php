<?php
$start = microtime(true);
header('Content-Type: text/html; charset=utf-8');
include '../../main.php';
echo '<pre>';

$tables = ['bureliai',
'bureliai_schedule',
'bureliai_schedule_kids',
'config',
'darbuotojai',
'darb_grupes',
'dokumentai',
'forms',
'forms_fields',
'forms_fields_allowed',
'forms_fields_answer',
'forms_fields_answers',
'forms_fields_answers_i',
'grupes',
'health_observations',
'lankomumas',
'messages',
'pasiekimai',
'pasiekimai_access',
'planning',
'planning_individual',
'reminder',
'reminder_sent',
'speech_attendance',
'speech_attendance_topic',
'speech_diaries',
'speech_groups',
'speech_groups_kids',
'speech_kids',
'speech_other_activities',
'speech_schedule',
'tabeliai',
'tabeliai_charge',
'users',
'users_allowed',
'vaikai',
'worker_food_ate'];

$rows = [];
foreach($tables as $table) {
	foreach($db_prefixes as $prefix) {
		$tableName = $prefix.$table;
		$result = mysqli_query($db_link, "SELECT 1 FROM `".$tableName."` LIMIT 1");
		if($result) {
			$result = db_query("SELECT COUNT(*) as `CNT` FROM `".$tableName."`");
			if ($row = mysqli_fetch_assoc($result)) {
				if(!isset($rows[$table]))
					$rows[$table] = 0;
				$rows[$table] += $row['CNT'];
			}
		} else
			echo str_pad($tableName, 20, ' ', STR_PAD_RIGHT)." denied\n";
	}
}
print_r($rows);
asort($rows);
print_r($rows);
echo 'Tables cnt - '.count($tables);
echo "\n".round(microtime(true)-$start, 3);
