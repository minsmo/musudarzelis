<?php
$start = microtime(true);
header('Content-Type: text/html; charset=utf-8');
include '../../main.php';
echo '<pre>';
echo date('Y-m-d H:i l')."\n";
$iter_db_prefixes = array();
/*$result = db_query("SELECT `id`, `prefix` FROM `0db`");
$db_prefixes = array();
while ($row = mysqli_fetch_assoc($result))
	$db_prefixes[$row['id']] = $row['prefix'];*/
$iter_db_prefixes = $db_prefixes;
$prefix = $_GET['prefix'];
//for($i=4; $i <= 10/*23*/; ++$i) {
/*for($i=1; $i <= 31; ++$i) {
	$prefix = 'semi'.$i.'_';
	if(!in_array($prefix, $iter_db_prefixes))
		$iter_db_prefixes[] = $prefix;
	//Order:
	unset($iter_db_prefixes[array_search($prefix, $iter_db_prefixes)]);
	$iter_db_prefixes[] = $prefix;
}*/
//print_r($iter_db_prefixes);

echo "\nKids:\n";//~5ms
//foreach($iter_db_prefixes as $prefix) {
	if($prefix != 't_' && $prefix != 'r_' && substr($prefix, 0, 4) != 'semi' && $prefix != 's_' && $prefix != 'naujas_') {
		$result = db_query("SELECT COUNT(*) AS `SUM` FROM `".$prefix."vaikai` HAVING COUNT(*)>0");
		if ($row = mysqli_fetch_assoc($result))
			echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; kids: ".$row['SUM']." \n";
	}
//}

echo "\nReminders:\n";//10ms
//foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT COUNT(*) as `CNT` FROM `".$prefix."reminder` HAVING COUNT(*)>0");
	while ($row = mysqli_fetch_assoc($result))
		//if($row['CNT'])
			echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; reminders: ".$row['CNT']." \n";
	$result = db_query("SELECT COUNT(*) as `CNT` FROM `".$prefix."reminder_sent` HAVING COUNT(*)>0");
	while ($row = mysqli_fetch_assoc($result))
		//if($row['CNT'])
			echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; reminders SENT: ".$row['CNT']." \n";
//}


echo "\nNew planning:\n";//10ms
//foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT SUM(`isPublished`) AS `SUM` FROM `".$prefix."forms` HAVING SUM(`isPublished`)>0");
	if ($row = mysqli_fetch_assoc($result))
			echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; published: ".$row['SUM']." \n";
	$result = db_query("SELECT COUNT(*) as `CNT` FROM `".$prefix."forms_fields_answer` HAVING COUNT(*)>0");
	if ($row = mysqli_fetch_assoc($result))
			echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; plans WRITTEN: ".$row['CNT']." \n";
//}
echo "\nOld planning:\n";//10ms
//foreach($iter_db_prefixes as $prefix) {
	$result = mysqli_query($db_link, "SELECT 1 FROM `".$prefix."planning` LIMIT 1");
	if($result) {
		$result = db_query("SELECT COUNT(*) as `CNT` FROM `".$prefix."planning` HAVING COUNT(*)>0");
		if ($row = mysqli_fetch_assoc($result))
				echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; plans WRITTEN: ".$row['CNT']." \n";
	}
//}
echo "\nPlanning widgets:\n";//?ms
$result = db_query("SELECT `kindergarten_id`, COUNT(*) as `CNT` FROM `1forms_widget` GROUP BY `kindergarten_id`");
while ($row = mysqli_fetch_assoc($result))
	if($prefix == $db_prefixes[$row['kindergarten_id']])
		echo 'DB: '.str_pad($db_prefixes[$row['kindergarten_id']], 11, ' ', STR_PAD_RIGHT)."; table : ".$row['CNT']." \n";

echo "\nKid's documents:\n";//~5ms
//foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT COUNT(*) AS `SUM` FROM `".$prefix."dokumentai` HAVING COUNT(*)>0");
	if ($row = mysqli_fetch_assoc($result))
		echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; docs: ".$row['SUM']." \n";
//}
echo "\nCommon documents:\n";//~5ms
//foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT COUNT(*) AS `SUM` FROM `".$prefix."documents_common` HAVING COUNT(*)>0");
	if ($row = mysqli_fetch_assoc($result))
		echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; docs: ".$row['SUM']." \n";
//}
echo "\nWorkers:\n";
$i = 1;
//foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT COUNT(*) AS `SUM`, MIN(`created`) `created` FROM `".$prefix."darbuotojai` HAVING COUNT(*)>0");
	if ($row = mysqli_fetch_assoc($result))
		echo str_pad($i++, 3, ' ', STR_PAD_RIGHT).' DB: '.str_pad($prefix, 13, ' ', STR_PAD_RIGHT)."; workers: ".str_pad($row['SUM'], 2, ' ', STR_PAD_RIGHT)."; min created: ".$row['created']." \n";
//}
echo "\nGroups:\n";
//foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT COUNT(*) AS `SUM` FROM `".$prefix."grupes` HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result))
		echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; groups: ".$row['SUM']." \n";
//}
echo "\nSchedule of activities with specialists:\n";
//foreach($iter_db_prefixes as $prefix) {
	if(mysqli_query($db_link, "SELECT 1 FROM `".$prefix."bureliai_schedule` LIMIT 1")) {
		$result = db_query("SELECT COUNT(*) AS `SUM` FROM `".$prefix."bureliai_schedule` HAVING COUNT(*)>1");
		if ($row = mysqli_fetch_assoc($result))
			echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; groups activities: ".$row['SUM']." \n";
	}
//}
echo "\nAttendance (not normalized):\n";
$startTMP = microtime(true);
//foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT COUNT(*) AS `SUM`, MAX(`data`) as `max`, MIN(`data`) as `min` FROM `".$prefix."lankomumas` HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result))
		echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; attendance marked: ".str_pad($row['SUM'], 5, ' ', STR_PAD_RIGHT)."; max: ".$row['max']." ; min: ".$row['min']." \n";
//}
echo round(microtime(true)-$startTMP, 3);
echo "\nGroup emails:\n";
//foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT COUNT(*) AS `SUM` FROM `".$prefix."masinis` HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result))
		echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; sent emails: ".$row['SUM']." \n";
//}
echo "\nMessages:\n";
//foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT COUNT(*) AS `SUM` FROM `".$prefix."messages` HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result))
		echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; sent messages: ".$row['SUM']." \n";
//}
echo "\nAchievements and progress:\n";
//foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT COUNT(*) AS `SUM` FROM `".$prefix."pasiekimai` HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result))
		echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; Achievements: ".$row['SUM']." \n";
//}

/*abstract class ProgressAndAchievementsFieldType
{
    const Note = 1;
    const Achievement = 0;
}
echo "\nMore than one achievement:\n";
foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT COUNT(*) AS `SUM`, `vaiko_id` FROM `".$prefix."pasiekimai` WHERE `type`=".ProgressAndAchievementsFieldType::Achievement." GROUP BY `vaiko_id`, `data` HAVING COUNT(*)>1");
	while ($row = mysqli_fetch_assoc($result))
		echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; More achievements: ".$row['SUM']."; Kid id: ".$row['vaiko_id']." \n";
}*/
$TempStart = microtime(true);
echo "\nLast login (started to log from 2014-10-27 19:57:06):\n";
//foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT MAX(`last_login_time`) `last_login_time`, SUM(`login_counter`) `login_counter` FROM `".$prefix."users`");
	if ($row = mysqli_fetch_assoc($result))
		echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; on: ".$row['last_login_time']."; counter: ".$row['login_counter']." \n";
//}
echo round(microtime(true)-$TempStart, 3);

echo "\nHealth observations
(teachers see this menu item only of these DB [last updated 2014-12-08]:
TESTINIS || RODOMASIS || NAUJAS || KaunoZiburelis || KaunoRudnosiukas || SiauliuZiogelis || LazdijuRZibute || TauragesRAzuoliukas || KaunoVaikystesLobiai):\n";//10ms
//foreach($iter_db_prefixes as $prefix) {
	$result = mysqli_query($db_link, "SELECT 1 FROM `".$prefix."health_observations` LIMIT 1");
	if($result) {
		$result = db_query("SELECT COUNT(*) as `CNT` FROM `".$prefix."health_observations`");
		if ($row = mysqli_fetch_assoc($result))
			echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; health observations: ".$row['CNT']." \n";
	} else
		echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; denied\n";
//}

echo "\nFood menu:\n";
//foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT COUNT(*) AS `SUM` FROM `".$prefix."food_menu` HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result))
		echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; Food menus: ".$row['SUM']." \n";
//}
echo "\nDaily rhythm (individualized by group):\n";
//foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT COUNT(*) AS `SUM`, `created` FROM `".$prefix."daily_rhythm` HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result))
		echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; daily rhythms: ".str_pad($row['SUM'], 3, ' ', STR_PAD_RIGHT)."; created: ".$row['created']." \n";
//}
echo "\nSpeech therapist kids:\n";
//foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT COUNT(*) AS `SUM`, `created` FROM `".$prefix."speech_kids` HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result))
		echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; cnt: ".str_pad($row['SUM'], 3, ' ', STR_PAD_RIGHT)."; created: ".$row['created']." \n";
//}
echo "\nSpeech therapist groups_kids:\n";
//foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT COUNT(*) AS `SUM`, `created` FROM `".$prefix."speech_groups_kids` HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result))
		echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; cnt: ".str_pad($row['SUM'], 3, ' ', STR_PAD_RIGHT)."; created: ".$row['created']." \n";
//}
echo "\nSpeech therapist groups:\n";
//foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT COUNT(*) AS `SUM`, `created` FROM `".$prefix."speech_groups` HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result))
		echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; cnt: ".str_pad($row['SUM'], 3, ' ', STR_PAD_RIGHT)."; created: ".$row['created']." \n";
//}
echo "\nSpeech therapist attendance:\n";
//foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT COUNT(*) AS `SUM`, `created` FROM `".$prefix."speech_attendance` HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result))
		echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; cnt: ".str_pad($row['SUM'], 3, ' ', STR_PAD_RIGHT)."; created: ".$row['created']." \n";
//}
echo "\nSpeech therapist attendance_topic:\n";
//foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT COUNT(*) AS `SUM`, `created` FROM `".$prefix."speech_attendance_topic` HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result))
		echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; cnt: ".str_pad($row['SUM'], 3, ' ', STR_PAD_RIGHT)."; created: ".$row['created']." \n";
//}
echo "\nSpeech therapist other_activities:\n";
//foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT COUNT(*) AS `SUM`, `created` FROM `".$prefix."speech_other_activities` HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result))
		echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; cnt: ".str_pad($row['SUM'], 3, ' ', STR_PAD_RIGHT)."; created: ".$row['created']." \n";
//}

echo "\nSpeech therapist schedule:\n";
//foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT COUNT(*) AS `SUM`, `created` FROM `".$prefix."speech_schedule` HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result))
		echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; cnt: ".str_pad($row['SUM'], 3, ' ', STR_PAD_RIGHT)."; created: ".$row['created']." \n";
//}

echo "\nTabaliai charge:\n";
//foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT COUNT(*) AS `SUM`, `created` FROM `".$prefix."tabeliai_charge` HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result))
		echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; charges: ".str_pad($row['SUM'], 3, ' ', STR_PAD_RIGHT)."; created: ".$row['created']." \n";
//}
echo "\nKids fee for place:\n";
//foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT COUNT(*) AS `SUM`, `created` FROM `".$prefix."vaikai` WHERE `feeForPlace`<>0 HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result))
		echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; feeForPlace: ".str_pad($row['SUM'], 3, ' ', STR_PAD_RIGHT)."; created: ".$row['created']." \n";
//}

echo "\nWorkers food:\n";
//foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT COUNT(*) AS `SUM`, `created` FROM `".$prefix."worker_food_ate` HAVING COUNT(*)>1");
	if ($row = mysqli_fetch_assoc($result))
		echo 'DB: '.str_pad($prefix, 11, ' ', STR_PAD_RIGHT)."; worker_food_ate: ".str_pad($row['SUM'], 3, ' ', STR_PAD_RIGHT)."; created: ".$row['created']." \n";
//}

echo "\nNotes:\n";//?ms
$result = db_query("SELECT `kindergarten_id`, COUNT(*) as `CNT` FROM `1notes` GROUP BY `kindergarten_id`");
while ($row = mysqli_fetch_assoc($result))
	if($db_prefixes[$row['kindergarten_id']] == $prefix)
		echo 'DB: '.str_pad($db_prefixes[$row['kindergarten_id']], 11, ' ', STR_PAD_RIGHT)."; cnt : ".$row['CNT']." \n";
	
//
//

/*echo "\nFind email:\n";
foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT * FROM  `".$prefix."users` WHERE  `email` LIKE  'rita.miliukeviciene@gmail.com'");
	while ($row = mysqli_fetch_assoc($result))
		echo 'DB:'.$prefix."; published: ".print_r($row, 1)." \n";
}*/
echo "\nFind config darzelio_nr:\n";
//foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT * FROM  `".$prefix."config` WHERE  `title`='darzelio_nr'");
	while ($row = mysqli_fetch_assoc($result))
		echo 'DB:'.$prefix."; ${row['value']} \n";
//}
echo "\nFind config banko_skyrius:\n";
//foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT * FROM  `".$prefix."config` WHERE  `title`='banko_skyrius'");
	while ($row = mysqli_fetch_assoc($result))
		echo 'DB:'.$prefix."; ${row['value']} \n";
//}

/*$result = db_query("SELECT `AUTO_INCREMENT`
FROM  INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA = 'darzelis'
AND   TABLE_NAME   LIKE '%forms'");
	while ($row = mysqli_fetch_assoc($result))
		echo 'DB:'.$prefix."; ${row['AUTO_INCREMENT']} \n";*/
/*
echo "\nFind config:\n";
foreach($iter_db_prefixes as $prefix) {
	$result = db_query("SELECT * FROM  `".$prefix."config` WHERE  `value` LIKE  'LT554010042500020356'");
	while ($row = mysqli_fetch_assoc($result))
		echo 'DB:'.$prefix."; \n";
}
echo "\nFind bug:\n";
foreach($iter_db_prefixes as $prefix) {
	if($prefix != 's_') {
		$result = db_query("SELECT * FROM  `".$prefix."forms_fields_allowed` WHERE `field_allow_id`=8");
		if(mysqli_num_rows($result)) {
			while ($row = mysqli_fetch_assoc($result)) {
				echo 'DB:'.$prefix."; ".$row['allowed_for_person_type']." \n";
				//db_query("UPDATE `".$prefix."forms_fields_allowed` SET `allowed_for_person_type`='1' WHERE `field_allow_id` =8");
			}
		} else {
			//OK: DB:molVytur_ DB:knsZings_
			echo 'DB:'.$prefix."; --\n";
		}
	}
}*/


//SELECT *, INET_NTOA( last_login_ipv4 ) last_login_ipv4 FROM  `t_users`

echo "\n".round(microtime(true)-$start, 3);
