<?php
$privatekeyfile = 'dkim_private.key';
//Make a new key pair
//(2048 bits is the recommended minimum key length -
//gmail won't accept less than 1024 bits)
$pk = openssl_pkey_new(
	array(
        	'private_key_bits' => 2048,
                'private_key_type' => OPENSSL_KEYTYPE_RSA
        )
);
openssl_pkey_export_to_file($pk, $privatekeyfile);
