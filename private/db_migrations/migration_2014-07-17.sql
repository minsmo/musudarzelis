CREATE TABLE IF NOT EXISTS `t_archive` (
  `archive_id` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `attendance_report` tinyint(1) unsigned NOT NULL,
  `attendance_report_type` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
  `planning` tinyint(1) unsigned NOT NULL,
  `kids` tinyint(1) unsigned NOT NULL,
  `kids_groups` tinyint(1) unsigned NOT NULL,
  `kids_progress_and_achievements` tinyint(1) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdByUserId` int(7) unsigned NOT NULL,
  `createdByEmployeeId` int(7) unsigned NOT NULL,
  `generated` date NOT NULL DEFAULT '0000-00-00',
  `reports` int(7) unsigned NOT NULL DEFAULT 0,
  `url` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
  PRIMARY KEY (`archive_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;

ALTER TABLE `t_masinis` ADD  `isPublic` TINYINT( 1 ) UNSIGNED NOT NULL AFTER  `attachments`;
