ALTER TABLE  `t_dokumentai` CHANGE  `createdByUserId`  `createdByUserId` INT( 7 ) UNSIGNED NOT NULL AFTER  `created`;
RENAME TABLE  `t_grupes_bureliai` TO  `t_bureliai_schedule` ;

CREATE TABLE IF NOT EXISTS `t_bureliai_schedule_kids` (
  `ID` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `bureliai_schedule_id` int(7) unsigned NOT NULL,
  `kid_id` int(7) unsigned NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;
