ALTER TABLE  `${prefix}vaikai` ADD  `monthly_discount_sum` FLOAT NOT NULL AFTER  `dienosMokestis` ;
ALTER TABLE  `${prefix}vaikai` ADD  `percentage_discount_from_day_tax_per_month` TINYINT( 3 ) UNSIGNED NOT NULL AFTER  `monthly_discount_sum` ;
-- visur


-- tik VL:
INSERT INTO  `darzelis`.`knsVL_config` (`title` ,`value`) VALUES ('pavadinimas_laukeliui_menesines_nuolaidos_suma',  'Nuolaida Kauno rajono gyventojams');
ALTER TABLE  `knsVL_vaikai` CHANGE  `dienosMokestis`  `dienosMokestis` TINYINT( 1 ) UNSIGNED NOT NULL COMMENT  'KaunoRBaibokyne KaunoVaikystesLobiai';
INSERT INTO  `darzelis`.`knsVL_config` (`title` ,`value`) VALUES ('pavadinimas_laukeliui_procentine_nuolaida_nuo_ugdymo_ir_prieziuros_kainos_per_men.', 'Nuolaida antram vaikui');
INSERT INTO  `darzelis`.`knsVL_config` (`title` ,`value`) VALUES ('pilnos_dienos_maitinimo_per_men._sumos_riba',  '300');
INSERT INTO  `darzelis`.`knsVL_config` (`title` ,`value`) VALUES ('puses_dienos_maitinimo_per_men._sumos_riba',  '200');


-- Išskyrus VL ir Baibokynė
INSERT INTO `${prefix}config` (`title` ,`value`) VALUES ('kaina_ugdymo_reikmems_lopselyje_per_men.', '0');
INSERT INTO `${prefix}config` (`title` ,`value`) VALUES ('kaina_ugdymo_reikmems_darzelyje_per_men.', '0');
INSERT INTO `${prefix}config` (`title` ,`value`) VALUES ('kaina_ugdymo_reikmems_priesmokykliniame_per_men.', '0');


-- Papildoma dienos kaina additional_cost_per_day
-- INSERT INTO  `darzelis`.`knsVL_config` (`title` ,`value`) VALUES ('pavadinimas_laukeliui_papildoma_dienos_kaina', 'Papildoma dienos kaina');
-- INSERT INTO  `darzelis`.`knsVL_config` (`title` ,`value`) VALUES ('pavadinimas_laukeliui_papildoma_dienos_kaina_irasoma_zymint_lankomuma', 'Papildoma dienos kaina');
-- INSERT INTO  `darzelis`.`knsVL_config` (`title` ,`value`) VALUES ('pavadinimas_laukeliui_papildoma_dienos_kaina_irasoma_zymint_lankomuma', 'Papildoma dienos kaina');
-- INSERT INTO  `darzelis`.`knsVL_config` (`title` ,`value`) VALUES ('pavadinimas_laukelio_papildomos_dienos_kainos_irasomos_zymint_lankomuma', 'Papildoma dienos kaina');
-- INSERT INTO  `darzelis`.`knsVL_config` (`title` ,`value`) VALUES ('pavadinimas_laukelio_papildomos_dienos_kainos,_o_kaina_irasoma_lankomume', 'Papildoma dienos kaina');
-- INSERT INTO  `darzelis`.`knsVL_config` (`title` ,`value`) VALUES ('pavadinimas_laukelio_papildomos_dienos_kainos_irasomos_lankomume', 'Papildoma dienos kaina');
-- INSERT INTO  `darzelis`.`knsVL_config` (`title` ,`value`) VALUES ('pavadinimas_laukeliui_irasoma_papildoma_dienos_kaina_zymint_lankomuma', 'Papildoma dienos kaina');
-- INSERT INTO `${prefix}config` (`title` ,`value`) VALUES ('papildomos_dienos_kainos_pavadinimas', 'Papildoma dienos kaina');
-- GALUTINIS: INSERT INTO `${prefix}config` (`title` ,`value`) VALUES ('papildomos_dienos_kainos_(ivedamos_zymint_lankomuma)_pavadinimas', 'Papildoma dienos kaina');
-- GALUTINIS: INSERT INTO `${prefix}config` (`title` ,`value`) VALUES ('papildomos_dienos_kainos,_kuri_ivedama_zymint_lankomuma,_pavadinimas', 'Papildoma dienos kaina');
-- laukelio pavadinimas mėnesinės nuolaidos sumos
-- mėnesinės nuolaidos sumos laukelio pavadinimas

-- Išskyrus VL ir Baibokynė
INSERT INTO  `${prefix}config` (`title` ,`value`) VALUES ('papildoma_kaina_uz_patiekalu_gamyba_per_d._d.', '0');

--INSERT INTO `${prefix}config` (`title` ,`value`) VALUES ('papildoma_men._kaina_laukelio_pavadinimas', 'Aplinkos tvarkymas');
--INSERT INTO `${prefix}config` (`title` ,`value`) VALUES ('papildoma_men._kaina', '0');
