CREATE TABLE IF NOT EXISTS `1docs` (
  `kindergarten_id` smallint(5) unsigned NOT NULL,
  `answer_id` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` int(7) unsigned NOT NULL, -- Or if 0 then custom
  `group_id` int(7) unsigned NOT NULL, -- Or if 0 then personal
  `date` date NOT NULL,
  `title` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
  `content` MEDIUMTEXT COLLATE utf8_lithuanian_ci NOT NULL,
  `openedDate` timestamp NULL DEFAULT NULL,
  `openedByUserId` int(7) unsigned DEFAULT NULL,
  `openedByEmployeeId` int(7) unsigned DEFAULT NULL,
  `isDeleted` tinyint(1) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdByUserId` int(7) unsigned NOT NULL,
  `createdByEmployeeId` int(7) unsigned NOT NULL,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedByUserId` int(7) unsigned NOT NULL,
  `updatedByEmployeeId` int(7) unsigned NOT NULL,
  `updatedCounter` int(4) unsigned NOT NULL,
  PRIMARY KEY (`answer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `1docs_templates` (
  `kindergarten_id` smallint(5) unsigned NOT NULL,
  `form_id` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `title` text COLLATE utf8_lithuanian_ci NOT NULL,
  `content` MEDIUMTEXT COLLATE utf8_lithuanian_ci NOT NULL,
  `isPublished` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `isDeleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdByUserId` int(7) unsigned NOT NULL,
  `createdByEmployeeId` int(7) unsigned NOT NULL,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedByUserId` int(7) unsigned NOT NULL,
  `updatedByEmployeeId` int(7) unsigned NOT NULL,
  PRIMARY KEY (`form_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;
