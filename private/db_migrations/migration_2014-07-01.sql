CREATE TABLE IF NOT EXISTS `t_forms` (
  `form_id` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
  `isPublished` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT  '0',
  `isDeleted` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdByUserId` int(7) unsigned NOT NULL,
  `createdByEmployeeId` int(7) unsigned NOT NULL,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedByUserId` int(7) unsigned NOT NULL,
  `updatedByEmployeeId` int(7) unsigned NOT NULL,
  PRIMARY KEY (`form_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `t_forms_fields` (
  `field_id` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` int(7) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
  `help_text` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
  `type` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `order` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `isRequired` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT 0,
  -- `person_type` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`field_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `t_forms_fields_allowed` (
  `field_allow_id` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `field_id` int(7) unsigned NOT NULL,
  `form_id` int(7) unsigned NOT NULL, -- Redundant
  `allowed_for_person_type` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`field_allow_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `t_forms_fields_answer` (
  `answer_id` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` int(7) unsigned NOT NULL,
  `group_id` int(7) unsigned NOT NULL,-- kids group id
  `fromDate` date NOT NULL,
  `toDate` date NOT NULL,
  `openedDate` timestamp NULL,
  `openedByUserId` int(7) unsigned NULL,
  `openedByEmployeeId` int(7) unsigned NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdByUserId` int(7) unsigned NOT NULL,
  `createdByEmployeeId` int(7) unsigned NOT NULL,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedByUserId` int(7) unsigned NOT NULL,
  `updatedByEmployeeId` int(7) unsigned NOT NULL,
  PRIMARY KEY (`answer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `t_forms_fields_answers` (
  `answer_detail_id` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `answer_id` int(7) unsigned NOT NULL,
  `field_id` int(7) unsigned NOT NULL,
  `answer` mediumtext COLLATE utf8_lithuanian_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdByUserId` int(7) unsigned NOT NULL,
  `createdByEmployeeId` int(7) unsigned NOT NULL,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedByUserId` int(7) unsigned NOT NULL,
  `updatedByEmployeeId` int(7) unsigned NOT NULL,
  PRIMARY KEY (`answer_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `t_forms_fields_answers_i` (
  `answers_individual_id` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `answer_id` int(7) unsigned NOT NULL,
  `child_id` int(7) unsigned NOT NULL,
  `activity` text COLLATE utf8_lithuanian_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdByUserId` int(7) unsigned NOT NULL,
  `createdByEmployeeId` int(7) unsigned NOT NULL,
  PRIMARY KEY (`answers_individual_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;

-- ALTER TABLE `t_pasiekimai` ADD `data` date NOT NULL AFTER `veikimo_d_id`;

-- update t_pasiekimai a left join t_veikimo_d b on a.veikimo_d_id = b.ID set a.`data` = b.`data`

-- ALTER TABLE `t_pasiekimai` CHANGE `tipas` `achievementType` TINYINT(1) UNSIGNED NOT NULL;
-- ALTER TABLE `t_pasiekimai` CHANGE `pavadinimas` `value` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_lithuanian_ci NOT NULL;
-- ALTER TABLE `t_pasiekimai` ADD `type` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `vaiko_id`;

-- INSERT INTO t_pasiekimai (vaiko_id, `type`, achievementType, `value`, `data`, `created`, createdByUserId, createdByEmployeeId) 
-- 					SELECT vaiko_id, '1', '0', pastabos, `data`, `created`, createdByUserId, createdByEmployeeId FROM t_lankomumas WHERE pastabos<>''
