ALTER TABLE  `lzdVytur_forms` CHANGE  `title`  `title` TEXT CHARACTER SET utf8 COLLATE utf8_lithuanian_ci NOT NULL ;
ALTER TABLE  `t_config` CHANGE  `title`  `title` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_lithuanian_ci NOT NULL ;
-- daugiau atlikti nebereikės


-- Tik vienam darželiui:
INSERT INTO `darzelis`.`lzdVytur_config` SET `title`='2_banko_kodas';
INSERT INTO `darzelis`.`lzdVytur_config` SET `title`='2_banko_saskaita';
INSERT INTO `darzelis`.`lzdVytur_config` SET `title`='2_banko_pavadinimas';
INSERT INTO `darzelis`.`lzdVytur_config` SET `title`='2_banko_skyrius';
-- jau visiems.

INSERT INTO `darzelis`.`lzdVytur_config` SET `title`='vieneriu_pietu_kaina_priesmokykliniame_4_val._neapmokestinant_tevu';
-- Visiems lazdijų.
