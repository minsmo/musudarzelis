ALTER TABLE  `t_vaikai` CHANGE  `ID`  `ID` INT( 7 ) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT  'šis local_id naudojamas tik šioje lentelėje';
ALTER TABLE  `t_vaikai` CHANGE  `parent_kid_id`  `parent_kid_id` INT( 7 ) UNSIGNED NOT NULL DEFAULT  '0' COMMENT 'naudojamas kitose SQL lentelėse (main_kid_id)';
