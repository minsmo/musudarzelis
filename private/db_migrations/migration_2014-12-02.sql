CREATE TABLE IF NOT EXISTS `t_pasiekimai_access` (
  `ID` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `achievement_id` int(7) NOT NULL,
  `allowed_for_person_type` tinyint(2) unsigned NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;
ALTER TABLE `t_activities` ADD `updatedCounter` INT(4) UNSIGNED NOT NULL ;
ALTER TABLE `t_dokumentai` ADD `createdByEmployeeId` INT(7) UNSIGNED NOT NULL , ADD `updated` TIMESTAMP NOT NULL , ADD `updatedByUserId` INT(7) UNSIGNED NOT NULL , ADD `updatedByEmployeeId` INT(7) UNSIGNED NOT NULL , ADD `updatedCounter` INT(4) UNSIGNED NOT NULL ;