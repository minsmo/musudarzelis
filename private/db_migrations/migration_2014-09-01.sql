ALTER TABLE  `t_lankomumas` ADD  `kid_group_id` INT( 7 ) UNSIGNED NOT NULL AFTER  `vaiko_id`;


UPDATE `t_lankomumas` JOIN `t_vaikai` cr ON (cr.`parent_kid_id`=`t_lankomumas`.`vaiko_id` AND cr.`valid_from`<=`t_lankomumas`.`data`)
LEFT JOIN `t_vaikai` mx ON cr.`parent_kid_id`=mx.`parent_kid_id` AND cr.`valid_from`<mx.`valid_from` AND cr.`valid_from`<=`t_lankomumas`.`data` AND mx.`valid_from`<=`t_lankomumas`.`data`
SET `t_lankomumas`.`kid_group_id` = cr.`grupes_id`
WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 AND mx.`parent_kid_id` IS NULL;
-- su 692 pakeista 
-- be +50 pakeista
-- 785 iš viso

UPDATE `r_lankomumas` JOIN `r_vaikai` cr ON (cr.`parent_kid_id`=`r_lankomumas`.`vaiko_id` AND cr.`valid_from`<=`r_lankomumas`.`data`)
LEFT JOIN `r_vaikai` mx ON cr.`parent_kid_id`=mx.`parent_kid_id` AND cr.`valid_from`<mx.`valid_from` AND cr.`valid_from`<=`r_lankomumas`.`data` AND mx.`valid_from`<=`r_lankomumas`.`data`
SET `r_lankomumas`.`kid_group_id` = cr.`grupes_id`
WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 AND mx.`parent_kid_id` IS NULL;
-- su 1712 pakeista
-- be 70 pakeista
-- likęs vaikas buvo visiškai ištrintas
-- 1798 iš viso
-- 15 archyvuotas 43 ištrintas


Būtinai reikės padaryti:
echo "UPDATE `${prefix}lankomumas` JOIN `${prefix}vaikai` ON `${prefix}lankomumas`.`vaiko_id`=`${prefix}vaikai`.`ID` SET `${prefix}lankomumas`.`kid_group_id` = `${prefix}vaikai`.`grupes_id`;\n";
