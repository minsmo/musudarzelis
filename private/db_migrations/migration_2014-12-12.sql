CREATE TABLE IF NOT EXISTS `t_speech_kids` (
  `ID` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `order` int(4) unsigned NOT NULL,
  `kid_id` int(7) unsigned NOT NULL,
  `recommendations` text COLLATE utf8_lithuanian_ci NOT NULL,
  `edu_program` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
  `groups` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
  `attendance_start` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
  `attendance_end` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
  `achievements` text COLLATE utf8_lithuanian_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdByUserId` int(7) unsigned NOT NULL,
  `createdByEmployeeId` int(7) unsigned NOT NULL,
  `updated` timestamp NOT NULL,
  `updatedByUserId` int(7) unsigned NOT NULL,
  `updatedByEmployeeId` int(7) unsigned NOT NULL,
  `updatedCounter` int(4) unsigned NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;