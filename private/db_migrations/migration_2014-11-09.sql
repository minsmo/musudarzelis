CREATE TABLE IF NOT EXISTS `0time` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,-- 4
  `ipv4` int(10) unsigned NOT NULL,-- 4
  `time` float NOT NULL,-- 4
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,-- 4 bytes
  `kindergarten` SMALLINT unsigned NOT NULL,-- 2 instead of 11 varchar
  `user_id` int(7) unsigned NOT NULL,-- 4
  `DOMContentLoaded` float NOT NULL,-- 4
  `load` float NOT NULL,-- 4
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `0db` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `prefix` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
  `on` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;
