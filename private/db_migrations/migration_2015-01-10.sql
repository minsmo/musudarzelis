CREATE TABLE IF NOT EXISTS `t_tabeliai_charge` (
  `ID` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(7) unsigned NOT NULL,
  `kid_id` int(7) unsigned NOT NULL,
  `date` date NOT NULL,
  `charge` float,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdByUserId` int(7) unsigned NOT NULL,
  `createdByEmployeeId` int(7) unsigned NOT NULL,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedByUserId` int(7) unsigned NOT NULL,
  `updatedByEmployeeId` int(7) unsigned NOT NULL,
  `updatedCounter` int(4) unsigned NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `kidId_date` (`kid_id`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;

