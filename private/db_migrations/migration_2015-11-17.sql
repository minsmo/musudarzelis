ALTER TABLE  `1kid_level` CHANGE  `updatedByUserId`  `updatedByUserId` INT( 7 ) UNSIGNED NOT NULL DEFAULT  '0';
ALTER TABLE  `1kid_level` CHANGE  `updatedByEmployeeId`  `updatedByEmployeeId` INT( 7 ) UNSIGNED NOT NULL DEFAULT  '0';
ALTER TABLE  `1kid_level` CHANGE  `updatedCounter`  `updatedCounter` INT( 4 ) UNSIGNED NOT NULL DEFAULT  '0';

ALTER TABLE  `t_lankomumas` CHANGE  `veikimo_d_id`  `veikimo_d_id` INT( 7 ) UNSIGNED NOT NULL DEFAULT  '0';
ALTER TABLE  `t_lankomumas` CHANGE  `justified_d`  `justified_d` INT( 7 ) UNSIGNED NOT NULL DEFAULT  '0';
ALTER TABLE  `t_lankomumas` CHANGE  `additional_cost_per_day`  `additional_cost_per_day` FLOAT NOT NULL DEFAULT  '0';

ALTER TABLE  `t_lankomumas` CHANGE  `updatedByUserId`  `updatedByUserId` INT( 7 ) UNSIGNED NOT NULL DEFAULT  '0';
ALTER TABLE  `t_lankomumas` CHANGE  `updatedByEmployeeId`  `updatedByEmployeeId` INT( 7 ) UNSIGNED NOT NULL DEFAULT  '0';
ALTER TABLE  `t_lankomumas` CHANGE  `updatedCounter`  `updatedCounter` INT( 4 ) UNSIGNED NOT NULL DEFAULT  '0';

ALTER TABLE  `t_speech_attendance` CHANGE  `updatedByUserId`  `updatedByUserId` INT( 7 ) UNSIGNED NOT NULL DEFAULT  '0';
ALTER TABLE  `t_speech_attendance` CHANGE  `updatedByEmployeeId`  `updatedByEmployeeId` INT( 7 ) UNSIGNED NOT NULL DEFAULT  '0';
ALTER TABLE  `t_speech_attendance` CHANGE  `updatedCounter`  `updatedCounter` INT( 4 ) UNSIGNED NOT NULL DEFAULT  '0';
ALTER TABLE  `t_speech_attendance` CHANGE  `justified_d`  `justified_d` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT  '0';


ALTER TABLE  `t_pasiekimai` CHANGE  `updatedByUserId`  `updatedByUserId` INT( 7 ) UNSIGNED NOT NULL DEFAULT  '0';
ALTER TABLE  `t_pasiekimai` CHANGE  `updatedByEmployeeId`  `updatedByEmployeeId` INT( 7 ) UNSIGNED NOT NULL DEFAULT  '0';
ALTER TABLE  `t_pasiekimai` CHANGE  `updatedCounter`  `updatedCounter` INT( 4 ) UNSIGNED NOT NULL DEFAULT  '0';
ALTER TABLE  `t_pasiekimai` CHANGE  `attachment`  `attachment` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_lithuanian_ci NOT NULL DEFAULT  '';
