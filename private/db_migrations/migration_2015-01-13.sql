CREATE TABLE IF NOT EXISTS `t_speech_diaries` (
  `ID` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `position_id` tinyint(2) unsigned NOT NULL,
  `employee_id` int(7) unsigned NOT NULL,
  `notes` text COLLATE utf8_lithuanian_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdByUserId` int(7) unsigned NOT NULL,
  `createdByEmployeeId` int(7) unsigned NOT NULL,
  `updated` timestamp NOT NULL,
  `updatedByUserId` int(7) unsigned NOT NULL,
  `updatedByEmployeeId` int(7) unsigned NOT NULL,
  `updatedCounter` int(4) unsigned NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;

ALTER TABLE  `t_speech_attendance` ADD  `diary_id` SMALLINT( 5 ) UNSIGNED NOT NULL AFTER  `ID` ;
ALTER TABLE  `t_speech_attendance_topic` ADD  `diary_id` SMALLINT( 5 ) UNSIGNED NOT NULL AFTER  `ID` ;
ALTER TABLE  `t_speech_groups` ADD  `diary_id` SMALLINT( 5 ) UNSIGNED NOT NULL AFTER  `ID` ;
ALTER TABLE  `t_speech_groups_kids` ADD  `diary_id` SMALLINT( 5 ) UNSIGNED NOT NULL AFTER  `ID` ;
ALTER TABLE  `t_speech_kids` ADD  `diary_id` SMALLINT( 5 ) UNSIGNED NOT NULL AFTER  `ID` ;
ALTER TABLE  `t_speech_other_activities` ADD  `diary_id` SMALLINT( 5 ) UNSIGNED NOT NULL AFTER  `ID` ;
ALTER TABLE  `t_speech_remarks_and_suggestions` ADD  `diary_id` SMALLINT( 5 ) UNSIGNED NOT NULL AFTER  `ID` ;
ALTER TABLE  `t_speech_schedule` ADD  `diary_id` SMALLINT( 5 ) UNSIGNED NOT NULL AFTER  `ID` ;
