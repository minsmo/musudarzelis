CREATE TABLE IF NOT EXISTS `t_speech_attendance` (
  `ID` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `kid_id` int(7) unsigned NOT NULL,
  `kid_group_id` int(7) unsigned NOT NULL,
  `date` date NOT NULL,
  `yra` tinyint(1) unsigned zerofill NOT NULL,
  `justified_d` tinyint(1) unsigned NOT NULL,
  `created` timestamp NOT NULL,
  `createdByUserId` int(7) unsigned NOT NULL,
  `createdByEmployeeId` int(7) unsigned NOT NULL,
  `updated` timestamp NOT NULL,
  `updatedByUserId` int(7) unsigned NOT NULL,
  `updatedByEmployeeId` int(7) unsigned NOT NULL,
  `updatedCounter` int(4) unsigned NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=84 ;

CREATE TABLE IF NOT EXISTS `t_speech_attendance_topic` (
  `ID` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `kid_group_id` int(7) unsigned NOT NULL,
  `date` date NOT NULL,
  `topic` text COLLATE utf8_lithuanian_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdByUserId` int(7) unsigned NOT NULL,
  `createdByEmployeeId` int(7) unsigned NOT NULL,
  `updated` timestamp NOT NULL,
  `updatedByUserId` int(7) unsigned NOT NULL,
  `updatedByEmployeeId` int(7) unsigned NOT NULL,
  `updatedCounter` int(4) unsigned NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=5 ;
