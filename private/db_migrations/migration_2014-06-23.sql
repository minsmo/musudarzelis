-- CREATE TABLE IF NOT EXISTS `t_notes` (
--   `note_id` int(7) unsigned NOT NULL AUTO_INCREMENT,
--   `child_id` int(7) unsigned NOT NULL,
--   `note` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
--   `data` date NOT NULL,
--   `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
--   `createdByUserId` int(7) unsigned NOT NULL,
--   `createdByEmployeeId` int(7) unsigned NOT NULL,
--   PRIMARY KEY (`note_id`)
-- ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;

-- INSERT INTO t_notes (child_id, note, `data`, `created`, createdByUserId, createdByEmployeeId) SELECT vaiko_id, pastabos, `data`, `created`, createdByUserId, createdByEmployeeId FROM t_lankomumas WHERE pastabos<>''


ALTER TABLE `t_pasiekimai` ADD `data` date NOT NULL AFTER `veikimo_d_id`;

update t_pasiekimai a left join t_veikimo_d b on a.veikimo_d_id = b.ID set a.`data` = b.`data`

ALTER TABLE `t_pasiekimai` CHANGE `tipas` `achievementType` TINYINT(1) UNSIGNED NOT NULL;
ALTER TABLE `t_pasiekimai` CHANGE `pavadinimas` `value` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_lithuanian_ci NOT NULL;
ALTER TABLE `t_pasiekimai` ADD `type` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `vaiko_id`;

INSERT INTO t_pasiekimai (vaiko_id, `type`, achievementType, `value`, `data`, `created`, createdByUserId, createdByEmployeeId) 
					SELECT vaiko_id, '1', '0', pastabos, `data`, `created`, createdByUserId, createdByEmployeeId FROM t_lankomumas WHERE pastabos<>''