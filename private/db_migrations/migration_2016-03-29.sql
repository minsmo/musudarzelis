CREATE TABLE IF NOT EXISTS `1ikimokyklinis_lt` (
  `kindergarten_id` smallint(5) unsigned NOT NULL,
  `id` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `content_id` int(7) unsigned NOT NULL,
  `type` int(7) NOT NULL, -- 0 - perskaitė; 1 - patiko; 2 - praktiška; 3 - naudinga, 4 - komentaras.
  `content` mediumtext COLLATE utf8_lithuanian_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdByUserId` int(7) unsigned NOT NULL,
  `createdByEmployeeId` int(7) unsigned NOT NULL,
  PRIMARY KEY (`vote_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=0 ;
CREATE TABLE IF NOT EXISTS `0wrong_emails` (
  `email` mediumtext COLLATE utf8_lithuanian_ci NOT NULL,
  `last_date` date NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci ;

