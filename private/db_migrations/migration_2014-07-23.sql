CREATE TABLE IF NOT EXISTS `t_reminder` (
  `reminder_id` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `document_type` tinyint(1) unsigned NOT NULL,
  `group_id` int(7) unsigned NOT NULL,-- kids group id
  `kid_id` int(7) unsigned NOT NULL,
  `expiry_date` date NOT NULL,-- due date -- Expiration date -- deadline
  `remind_before` int(7) unsigned NOT NULL,
  
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdByUserId` int(7) unsigned NOT NULL,
  `createdByEmployeeId` int(7) unsigned NOT NULL,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedByUserId` int(7) unsigned NOT NULL,
  `updatedByEmployeeId` int(7) unsigned NOT NULL,
  PRIMARY KEY (`reminder_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `t_reminder_sent` (
  `reminder_sent_id` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `reminder_id` int(7) unsigned NOT NULL,
  `document_type` tinyint(1) unsigned NOT NULL,
  `group_id` int(7) unsigned NOT NULL,-- kids group id
  `kid_id` int(7) unsigned NOT NULL,
  `sent_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sent_emails` mediumtext COLLATE utf8_lithuanian_ci NOT NULL,
  `sent_text` mediumtext COLLATE utf8_lithuanian_ci NOT NULL,
  
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdByUserId` int(7) unsigned NOT NULL,
  `createdByEmployeeId` int(7) unsigned NOT NULL,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedByUserId` int(7) unsigned NOT NULL,
  `updatedByEmployeeId` int(7) unsigned NOT NULL,
  PRIMARY KEY (`reminder_sent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;
