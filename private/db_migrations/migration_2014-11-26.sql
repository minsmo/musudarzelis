ALTER TABLE  `t_vaikai` ADD  `deleted` TIMESTAMP NOT NULL ;
ALTER TABLE  `t_vaikai` ADD  `updatedCounter` INT( 4 ) UNSIGNED NOT NULL AFTER  `updatedByEmployeeId` ;
ALTER TABLE  `${prefix}vaikai` ADD INDEX (  `parent_kid_id` ,  `valid_from` ) ;
ALTER TABLE  `${prefix}dokumentai` ADD INDEX (  `vaiko_id` ) ;
