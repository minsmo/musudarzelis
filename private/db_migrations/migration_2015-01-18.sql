ALTER TABLE  `t_vaikai` ADD  `attend_sport` TINYINT( 1 ) UNSIGNED NOT NULL AFTER  `allergic` ;
INSERT INTO `t_config` (`ID`, `title`, `value`, `valid_from`, `created`, `createdByUserId`, `createdByEmployeeId`, `updated`, `updatedByUserId`, `updatedByEmployeeId`, `updatedCounter`) VALUES (NULL, 'kaina_uz_sporta_per_men.,_taikoma_pazymejus_vaiko_duomenyse', '', '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '', '', '');

ALTER TABLE  `t_speech_remarks_and_suggestions` ADD  `position_id` TINYINT( 2 ) UNSIGNED NOT NULL AFTER  `remarks_and_suggestions` ;
ALTER TABLE  `t_speech_remarks_and_suggestions` CHANGE  `position_id`  `createdByPosition_id` TINYINT( 2 ) UNSIGNED NOT NULL AFTER `createdByEmployeeId` ;

ALTER TABLE  `t_forms_fields_answer` ADD  `isDeleted` TINYINT( 1 ) UNSIGNED NOT NULL AFTER  `openedByEmployeeId` ;
