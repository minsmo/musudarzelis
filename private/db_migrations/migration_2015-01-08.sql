CREATE TABLE IF NOT EXISTS `t_speech_schedule` (
  `ID` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `time` varchar(20) COLLATE utf8_lithuanian_ci NOT NULL,
  `day` tinyint(1) unsigned NOT NULL,
  `speech_group_id` int(7) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdByUserId` int(7) unsigned NOT NULL,
  `createdByEmployeeId` int(7) unsigned NOT NULL,
  `updated` timestamp NOT NULL,
  `updatedByUserId` int(7) unsigned NOT NULL,
  `updatedByEmployeeId` int(7) unsigned NOT NULL,
  `updatedCounter` int(4) unsigned NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;