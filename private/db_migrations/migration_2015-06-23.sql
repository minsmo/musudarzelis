CREATE TABLE IF NOT EXISTS `1kid_level` (
  `kindergarten_id` smallint(5) unsigned NOT NULL,
  `kid_id` int(7) unsigned NOT NULL,
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `area` tinyint(2) unsigned NOT NULL,
  `level` tinyint(1) unsigned NOT NULL,
  `notes` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdByUserId` int(7) unsigned NOT NULL,
  `createdByEmployeeId` int(7) unsigned NOT NULL,
  `updated` timestamp NOT NULL,
  `updatedByUserId` int(7) unsigned NOT NULL,
  `updatedByEmployeeId` int(7) unsigned NOT NULL,
  `updatedCounter` int(4) unsigned NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;
