INSERT INTO  `darzelis`.`druskBit_config` (
`title` ,
`value` ,
`valid_from` ,
`created` ,
`createdByUserId` ,
`createdByEmployeeId` ,
`updated` ,
`updatedByUserId` ,
`updatedByEmployeeId` ,
`updatedCounter`
)
VALUES ('dienos_kaina_per_d._lopselyje',  '2.43',  '', CURRENT_TIMESTAMP ,  '',  '',  '0000-00-00 00:00:00',  '',  '',  ''),
VALUES ('dienos_kaina_per_d._darzelyje',  '2.61',  '', CURRENT_TIMESTAMP ,  '',  '',  '0000-00-00 00:00:00',  '',  '',  ''),
VALUES ('dienos_kaina_per_d._priesmokykliniame',  '2.03',  '', CURRENT_TIMESTAMP ,  '',  '',  '0000-00-00 00:00:00',  '',  '',  '');


ALTER TABLE  `t_lankomumas` ADD INDEX (  `data` ) ;
