CREATE TABLE IF NOT EXISTS `t_food_menu` (
  `ID` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `breakfast` mediumtext COLLATE utf8_lithuanian_ci NOT NULL,
  `lunch` mediumtext COLLATE utf8_lithuanian_ci NOT NULL,
  `afternoon_tea` mediumtext COLLATE utf8_lithuanian_ci NOT NULL,
  `dinner` mediumtext COLLATE utf8_lithuanian_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdByUserId` int(7) unsigned NOT NULL,
  `createdByEmployeeId` int(7) unsigned NOT NULL,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedByUserId` int(7) unsigned NOT NULL,
  `updatedByEmployeeId` int(7) unsigned NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;
