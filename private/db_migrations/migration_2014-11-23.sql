CREATE TABLE IF NOT EXISTS `t_daily_rhythm` (
  `ID` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(7) unsigned NOT NULL,
  `period` varchar(50) COLLATE utf8_lithuanian_ci NOT NULL,
  `activity` mediumtext COLLATE utf8_lithuanian_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdByUserId` int(7) unsigned NOT NULL,
  `createdByEmployeeId` int(7) unsigned NOT NULL,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedByUserId` int(7) unsigned NOT NULL,
  `updatedByEmployeeId` int(7) unsigned NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;

ALTER TABLE `${prefix}pasiekimai` DROP `veikimo_d_id`;
ALTER TABLE `${prefix}pasiekimai` DROP `achievementType`;
ALTER TABLE  `t_pasiekimai` ADD  `updated` TIMESTAMP NOT NULL ,
ADD  `updatedByUserId` INT( 7 ) UNSIGNED NOT NULL ,
ADD  `updatedByEmployeeId` INT( 7 ) UNSIGNED NOT NULL ,
ADD  `updatedCounter` INT( 4 ) UNSIGNED NOT NULL ;
ALTER TABLE  `t_pasiekimai` ADD  `achievement` MEDIUMTEXT NOT NULL AFTER  `value` ;





UPDATE `t_pasiekimai` `f` JOIN `t_pasiekimai` `s` ON (`f`.`data`=`s`.`data` AND `f`.`vaiko_id`=`s`.`vaiko_id` AND `f`.`type`=1 AND `s`.`type`=0) SET `f`.`achievement` = `s`.`value`;
DELETE FROM `t_pasiekimai` WHERE `type`=0;
