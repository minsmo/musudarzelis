CREATE TABLE IF NOT EXISTS `0time` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ipv4` int(10) unsigned NOT NULL,
  `time` float NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `kindergarten` smallint(5) unsigned NOT NULL,
  `user_id` int(7) unsigned NOT NULL,
  `person_id` int(7) unsigned NOT NULL,
  `person_login_type` tinyint(2) NOT NULL DEFAULT '-1',
  `menu_id` tinyint(1) unsigned NOT NULL, -- ADDED
  `DOMContentLoaded` float NOT NULL DEFAULT '0',
  `load` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=2216785 ;

CREATE TABLE IF NOT EXISTS `0menu` (
  `menu_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `user_type` char(1) COLLATE utf8_lithuanian_ci NOT NULL,
  `menu_url` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=220 ;

--
-- Sukurta duomenų kopija lentelei `0menu`
--

INSERT INTO `0menu` (`menu_id`, `user_type`, `menu_url`) VALUES
(1, 'w', ''),
(2, 'w', 'grupes'),
(3, 'w', 'vaikai'),
(4, 'w', 'darbuotojai'),
(5, 'w', 'prisijungimai'),
(6, 'w', 'tabeliai'),
(7, 'w', 'veikla'),
(8, 'w', 'vaiko_pazanga_ir_pasiekimai'),
(9, 'w', 'vaiko_pasiekimai_pagal_nauja_aprasa'),
(10, 'w', 'forms_fill_out'),
(11, 'w', 'forms_make'),
(12, 'w', 'planavimas'),
(13, 'w', 'planavimo_formos_pildymas'),
(14, 'w', 'planavimo_formos'),
(15, 'w', 'daily_rhythm'),
(16, 'w', 'bureliai'),
(17, 'w', 'grupiu_bureliai'),
(18, 'w', 'activities'),
(19, 'w', 'dokumentai'),
(20, 'w', 'sveikatos_pastabos'),
(21, 'w', 'kvitai'),
(22, 'w', 'mokejimai'),
(23, 'w', 'zinutes'),
(24, 'w', 'laiskai_grupems'),
(25, 'w', 'menu'),
(26, 'w', 'maitinimas'),
(27, 'w', 'worker_food'),
(28, 'w', 'documents_common'),
(29, 'w', 'parents_menu'),
(30, 'w', 'tabelio_marks'),
(31, 'w', 'nustatymai'),
(32, 'w', 'archyvas'),
(33, 'w', 'page'),
(34, 'w', 'speech_diaries'),
(35, 'w', 'speech_kids'),
(36, 'w', 'speech_groups'),
(37, 'w', 'speech_attendance_report'),
(38, 'w', 'speech_topics'),
(39, 'w', 'speech_other_activities'),
(40, 'w', 'remarks_and_suggestions'),
(41, 'w', 'speech_schedule'),
(42, 'w', 'informal_diaries'),
(43, 'w', 'wishes'),
(44, 'w', 'goodies'),
(45, 'w', 'spausdinti'),
(46, 'w', 'kontaktai'),
(47, 'w', 'pakeisti_slaptazodi'),
(200, 'p', ''),
(201, 'p', 'vaiko_info'),
(202, 'p', 'lankomumas'),
(203, 'p', 'achievements_steps'),
(204, 'p', 'pazanga'),
(205, 'p', 'Planavimas'),
(206, 'p', 'dokumentai'),
(207, 'p', 'activities'),
(208, 'p', 'tvarkarastis'),
(209, 'p', 'daily_rhythm'),
(210, 'p', 'documents_common'),
(211, 'p', 'menu'),
(212, 'p', 'speech_therapist_schedule'),
(213, 'p', 'speech_therapist_topics'),
(214, 'p', 'mokejimai'),
(215, 'p', 'grupes_kontaktai'),
(216, 'p', 'logo_achievements_view'),
(217, 'p', 'goodies'),
(218, 'p', 'zinutes'),
(219, 'p', 'pakeisti_slaptazodi');
