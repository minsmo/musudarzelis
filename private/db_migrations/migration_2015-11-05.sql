CREATE TABLE IF NOT EXISTS `1moment_activity` (
  `kindergarten_id` smallint(5) unsigned NOT NULL,
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` DATE NOT NULL,
  `title` text COLLATE utf8_lithuanian_ci NOT NULL,
  `description` text COLLATE utf8_lithuanian_ci NOT NULL,
  `visibility` tinyint(1) unsigned NOT NULL, -- 0 all kindergarten, 1 only group (DEFAULT), 2 only parents
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdByUserId` int(7) unsigned NOT NULL,
  `createdByEmployeeId` int(7) unsigned NOT NULL,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedByUserId` int(7) unsigned NOT NULL,
  `updatedByEmployeeId` int(7) unsigned NOT NULL,
  `updatedCounter` int(4) unsigned NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `1moment_activity_links` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `moment_activity_id` int(10) unsigned NOT NULL,
  `link_type` varchar(20) COLLATE utf8_lithuanian_ci NOT NULL, -- =[group_id, kid_id, achievement, achievement_cat_id, keyword, event? (tik 20)]
  `value` int(7) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdByUserId` int(7) unsigned NOT NULL,
  `createdByEmployeeId` int(7) unsigned NOT NULL,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedByUserId` int(7) unsigned NOT NULL,
  `updatedByEmployeeId` int(7) unsigned NOT NULL,
  `updatedCounter` int(4) unsigned NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `1moment_photos` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `moment_activity_id` int(10) unsigned NOT NULL,
  `url` text COLLATE utf8_lithuanian_ci NOT NULL,
  `title` text COLLATE utf8_lithuanian_ci NOT NULL,
  `visibility` tinyint(1) unsigned NOT NULL, -- 0 all kindergarten, 1 only group (DEFAULT), 2 only parents
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdByUserId` int(7) unsigned NOT NULL,
  `createdByEmployeeId` int(7) unsigned NOT NULL,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedByUserId` int(7) unsigned NOT NULL,
  `updatedByEmployeeId` int(7) unsigned NOT NULL,
  `updatedCounter` int(4) unsigned NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `1moment_photos_links` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `moment_photo_id` int(10) unsigned NOT NULL,
  `link_type` varchar(20) COLLATE utf8_lithuanian_ci NOT NULL, -- =[group_id, kid_id, achievement, achievement_cat_id, keyword, event? (tik 20)]
  `value` int(7) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdByUserId` int(7) unsigned NOT NULL,
  `createdByEmployeeId` int(7) unsigned NOT NULL,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedByUserId` int(7) unsigned NOT NULL,
  `updatedByEmployeeId` int(7) unsigned NOT NULL,
  `updatedCounter` int(4) unsigned NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;
