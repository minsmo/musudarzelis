ALTER TABLE  `t_lankomumas` ADD  `additional_mark_id` SMALLINT( 5 ) UNSIGNED NOT NULL AFTER  `justified_d` ;
CREATE TABLE IF NOT EXISTS `1attendance_marks` (
  `kindergarten_id` smallint(5) unsigned NOT NULL,
  `ID` SMALLINT(5) unsigned NOT NULL AUTO_INCREMENT,
  `abbr` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
  `title` mediumtext COLLATE utf8_lithuanian_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdByUserId` int(7) unsigned NOT NULL,
  `createdByEmployeeId` int(7) unsigned NOT NULL,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedByUserId` int(7) unsigned NOT NULL,
  `updatedByEmployeeId` int(7) unsigned NOT NULL,
  `updatedCounter` int(4) unsigned NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;
