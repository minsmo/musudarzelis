ALTER TABLE `knsVarp_darbuotojai` ADD `ASM_ID` INT( 10 ) UNSIGNED NOT NULL AFTER `ID` ;
ALTER TABLE `knsVarp_darbuotojai` ADD `ASM_KODAS` BIGINT( 11 ) UNSIGNED NOT NULL AFTER `ASM_ID` ;



CREATE TABLE IF NOT EXISTS `1archive` (
  `kindergarten_id` smallint(5) unsigned NOT NULL,
  `archive_id` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `attendance_report` tinyint(1) unsigned NOT NULL,
  `attendance_report_type` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
  `planning` tinyint(1) unsigned NOT NULL,
  `kids` tinyint(1) unsigned NOT NULL,
  `kids_groups` tinyint(1) unsigned NOT NULL,
  `kids_progress_and_achievements` tinyint(1) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdByUserId` int(7) unsigned NOT NULL,
  `createdByEmployeeId` int(7) unsigned NOT NULL,
  `generated` date NOT NULL DEFAULT '0000-00-00',
  `reports` int(7) unsigned NOT NULL DEFAULT '0',
  `url` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
  PRIMARY KEY (`archive_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `2suggestions` (
  `kindergarten_id` smallint(5) unsigned NOT NULL,
  `id` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(7) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
  `content` mediumtext COLLATE utf8_lithuanian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `1mokejimai` (
  `kindergarten_id` smallint(5) unsigned NOT NULL,
  `ID` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `vaiko_id` int(7) unsigned NOT NULL,
  `metai` smallint(4) unsigned NOT NULL,
  `menuo` tinyint(2) unsigned NOT NULL,
  `uzDarzeli` double NOT NULL,
  `ugdymoReikmem` double NOT NULL,
  `isViso` double NOT NULL,
  `arApmoketa` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `2laiskai` (
  `kindergarten_id` smallint(5) unsigned NOT NULL,
  `ID` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `vaiko_id` int(7) unsigned NOT NULL,
  `tema` varchar(100) COLLATE utf8_lithuanian_ci NOT NULL,
  `turinys` text COLLATE utf8_lithuanian_ci NOT NULL,
  `data` int(11) unsigned NOT NULL,
  `createdByUserId` int(7) unsigned NOT NULL,
  `createdByEmployeeId` int(7) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `2justified_d` (
  `ID` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `dok_id` int(7) unsigned NOT NULL,
  `veikimo_d_id` int(7) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdByUserId` int(7) unsigned NOT NULL,
  `createdByEmployeeId` int(7) unsigned NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci COMMENT='Priezastys, Excused, granted' AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `1activities` (
  `kindergarten_id` smallint(5) unsigned NOT NULL,
  `ID` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `period` varchar(50) COLLATE utf8_lithuanian_ci NOT NULL,
  `activity` mediumtext COLLATE utf8_lithuanian_ci NOT NULL,
  `other` mediumtext COLLATE utf8_lithuanian_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdByUserId` int(7) unsigned NOT NULL,
  `createdByEmployeeId` int(7) unsigned NOT NULL,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedByUserId` int(7) unsigned NOT NULL,
  `updatedByEmployeeId` int(7) unsigned NOT NULL,
  `updatedCounter` int(4) unsigned NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `1daily_rhythm` (
  `kindergarten_id` smallint(5) unsigned NOT NULL,
  `ID` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(7) unsigned NOT NULL,
  `period` varchar(50) COLLATE utf8_lithuanian_ci NOT NULL,
  `activity` mediumtext COLLATE utf8_lithuanian_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdByUserId` int(7) unsigned NOT NULL,
  `createdByEmployeeId` int(7) unsigned NOT NULL,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedByUserId` int(7) unsigned NOT NULL,
  `updatedByEmployeeId` int(7) unsigned NOT NULL,
  `updatedCounter` int(4) unsigned NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `1masinis` (
  `kindergarten_id` smallint(5) unsigned NOT NULL,
  `ID` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `grup_id` int(7) NOT NULL,
  `tema` varchar(100) COLLATE utf8_lithuanian_ci NOT NULL,
  `tekstas` text COLLATE utf8_lithuanian_ci NOT NULL,
  `attachments` text COLLATE utf8_lithuanian_ci NOT NULL,
  `isPublic` tinyint(1) unsigned NOT NULL,
  `data` int(11) unsigned NOT NULL,
  `createdByUserId` int(7) unsigned NOT NULL,
  `createdByEmployeeId` int(7) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `1food_menu` (
  `kindergarten_id` smallint(5) unsigned NOT NULL,
  `ID` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `breakfast` mediumtext COLLATE utf8_lithuanian_ci NOT NULL,
  `brunch` mediumtext COLLATE utf8_lithuanian_ci NOT NULL,
  `lunch` mediumtext COLLATE utf8_lithuanian_ci NOT NULL,
  `afternoon_tea` mediumtext COLLATE utf8_lithuanian_ci NOT NULL,
  `dinner` mediumtext COLLATE utf8_lithuanian_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdByUserId` int(7) unsigned NOT NULL,
  `createdByEmployeeId` int(7) unsigned NOT NULL,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedByUserId` int(7) unsigned NOT NULL,
  `updatedByEmployeeId` int(7) unsigned NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `1speech_remarks_and_suggestions` (
  `kindergarten_id` smallint(5) unsigned NOT NULL,
  `ID` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `diary_id` smallint(5) unsigned NOT NULL,
  `date` date NOT NULL,
  `remarks_and_suggestions` mediumtext COLLATE utf8_lithuanian_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdByUserId` int(7) unsigned NOT NULL,
  `createdByEmployeeId` int(7) unsigned NOT NULL,
  `createdByPosition_id` tinyint(2) unsigned NOT NULL,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedByUserId` int(7) unsigned NOT NULL,
  `updatedByEmployeeId` int(7) unsigned NOT NULL,
  `updatedByPosition_id` tinyint(2) unsigned NOT NULL,
  `updatedCounter` int(4) unsigned NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;
