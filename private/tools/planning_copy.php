<?php
$start = microtime(true);
header('Content-Type: text/html; charset=utf-8');
include '../../main.php';

$prefix = 'knsRudno_';//knsVarp_
$form_id = 2;
$to_new_field_id = array(
	'topic' => 32,//248,
	'tikslas' => 33,//249,
	'uzdaviniai' => 34,//250,
	'ugdomojiVeiklaVaikuGrupelems' => 36,//252,
	'saveikaSuSeima' => 37,//257,
	'veiklaVisaiVaikuGrupei' => 38,//253,
	'savaitesApmastymaiRefleksija' => 39,//258 
);
$execute_migration = false;

echo '<table>';
echo '<tr>';
echo '<th>Title</th>';
echo '<th>Alternative</th>';
$result = db_query("SHOW COLUMNS FROM `".$prefix."forms_fields`");
while ($row = mysqli_fetch_assoc($result)) {
	echo '<th>'.$row['Field'].'</th>';
	//print_r($row);
}
echo '</tr>';
$result = db_query("SELECT * FROM `".$prefix."forms_fields` WHERE `form_id`=".$form_id);
while ($row = mysqli_fetch_assoc($result)) {
	//print_r($row);
	echo '<tr>';
	echo '<td>'.filterText($row['title']).'</td>';
	echo '<td>'.array_search($row['field_id'], $to_new_field_id).'</td>';
	foreach($row as $element)
		echo '<td>'.filterText($element).'</td>';
	echo '</tr>';
}
echo '</table>';

if($execute_migration) {
	$result = db_query("SELECT * FROM `".$prefix."planning`");
	while($plan = mysqli_fetch_assoc($result)) {
	//$plan = mysqli_fetch_assoc($result); {
		if(db_query("INSERT INTO `".$prefix."forms_fields_answer` SET `form_id`='".(int)$form_id."',
			`group_id`=".$plan['groupId'].", `fromDate`='".db_fix($plan['fromDate'])."', `toDate`='".db_fix($plan['toDate'])."',
			`updatedByUserId`='".$plan['updatedByUserId']."', `updatedByEmployeeId`='".$plan['updatedByEmployeeId']."', `updated`='".$plan['updated']."',
			`createdByUserId`='".$plan['createdByUserId']."', `createdByEmployeeId`='".$plan['createdByEmployeeId']."', `created`='".$plan['created']."'")) {
			$answer_id = (int)mysqli_insert_id($db_link);
	
			//$res = db_query("SELECT * FROM `".$prefix."forms_fields_allowed` WHERE `form_id`=".(int)$form_id);
			//$EditAllowed = array();
			//while($row = mysqli_fetch_assoc($res))
			//	$EditAllowed[$row['field_id']][$row['allowed_for_person_type']] = '';

			//if(isset($_POST['f'])) foreach($_POST['f'] as $field_id => $answer) {
				//if( ADMIN || isset($EditAllowed[$field_id][$_SESSION['USER_DATA']['person_type']]) ) {
			foreach($to_new_field_id as $old_name => $new_field_id) if(!is_null($new_field_id))
				db_query("INSERT INTO `".$prefix."forms_fields_answers` SET 
					`answer_id`=".$answer_id.", `field_id`='".(int)$new_field_id."',
					`answer`='".db_fix($plan[$old_name])."', `createdByUserId`='".$plan['createdByUserId']."', `createdByEmployeeId`='".$plan['createdByEmployeeId']."'");
				//}
			//}
	
			//ID,plan_id,child_id,activity,created,createdByUserId,createdByEmployeeId
			$result_ind = db_query("SELECT * FROM `".$prefix."planning_individual` WHERE `plan_id`=".$plan['planning_id']);
			while($plan_i = mysqli_fetch_assoc($result_ind)) {
			//if(isset($_POST['individuali_veikla'])) foreach($_POST['individuali_veikla'] as $kid_id => $activity) {
				db_query("INSERT INTO `".$prefix."forms_fields_answers_i` SET `answer_id`='".(int)$answer_id."', `child_id`='".(int)$plan_i['child_id']."',  
					`activity`='".db_fix($plan_i['activity'])."', `createdByUserId`='".(int)$plan_i['createdByUserId']."', `createdByEmployeeId`='".(int)$plan_i['createdByEmployeeId']."'");
			//}
			}
		}
	}
}


echo "\n".round(microtime(true)-$start, 3);
