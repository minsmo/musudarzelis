<?php
$start = microtime(true);
include '../../main.php';
header('Content-Type: text/html; charset=utf-8');

if( isset($_POST['archive_id']) ) {
	db_query("UPDATE `1archive` SET `generated`='".db_fix($_POST['generated'])."',
			`reports`='".(int)$_POST['reports']."',
			`url`='".db_fix($_POST['url'])."'
			WHERE `archive_id`=".(int)$_POST['archive_id']);
	msgBox('OK', 'Informacija išsaugota!');
}

if( isset($_GET['archive_id']) ) {
	$result = db_query("SELECT * FROM `1archive` WHERE `archive_id`=".(int)$_GET['archive_id']);
	$row = mysqli_fetch_assoc($result);
	?>
	<fieldset>
		<form method="post">
			<p>Sugeneravimo data: <input type="date" name="generated" value="<?=filterText($row['generated'])?>"></p>
			<p>Ataskaitų: <input type="number" name="reports" value="<?=(isset($row['reports']) ? (int)$row['reports'] : '')?>"></p>
			<p>URL: <input type="text" name="url" value="<?=filterText($row['url'])?>"></p>
			<p><input type="hidden" name="archive_id" value="<?=filterText($_GET['archive_id'])?>">
				<input type="submit" value="Išsaugoti"></p>
		</form>
	</fieldset>
	<?php
}

?>
<h2>Reikia sugeneruoti:</h2>
<table border="1">
<tr>
	<th>DB</th>
	<th>archive ID</th>
	<th>Created</th>
	<th title="attendance_report">At</th>
	<th title="attendance_report_type">Re</th>
	<th title="planning">P</th>
	<th title="kids">K</th>
	<th title="kids_groups">G</th>
	<th title="kids_progress_and_achievements">A</th>
	<th>Actions</th>
	
</tr>
<?php
$result = db_query("SELECT * FROM `1archive` WHERE `generated` IS NULL ORDER BY `generated` DESC");
while ($row = mysqli_fetch_assoc($result)) {
	echo '<tr>
		<td>'.$db_prefixes[$row['kindergarten_id']].'</td>
		<td><a href="http://dev.musudarzelis.lt/archyvas?generate='.$row['archive_id'].'">'.$row['archive_id'].'</a></td>
		<td>'.$row['created'].'</td>
		<td>'.$row['attendance_report'].'</td>
		<td>'.$row['attendance_report_type'].'</td>
 		<td>'.$row['planning'].'</td>
		<td>'.$row['kids'].'</td>
		<td>'.$row['kids_groups'].'</td>
		<td>'.$row['kids_progress_and_achievements'].'</td>
		<td><a href="http://dev.musudarzelis.lt/private/tools/archive.php?archive_id='.$row['archive_id'].'">Įdėti</a></td>
	</tr>';	
}

?>
</table>

<h2>Jau sugeneruota:</h2>
<table border="1">
<tr>
	<th>DB</th>
	<th>archive ID</th>
	<th>Created</th>
	<th>Generated</th>
	<th>Reports</th>
	<th>URL</th>
	<th>is</th>
	<th>Actions</th>
	
</tr>
<?php
$result = db_query("SELECT * FROM `1archive` WHERE `generated`<>'0000-00-00' ORDER BY `generated` DESC");
while ($row = mysqli_fetch_assoc($result)) {
	echo '<tr>
		<td>'.$db_prefixes[$row['kindergarten_id']].'</td>
		<td><a href="http://dev.musudarzelis.lt/archyvas?generate='.$row['archive_id'].'">'.$row['archive_id'].'</a></td>
		<td>'.$row['created'].'</td>
		<td>'.$row['generated'].'</td>
		<td>'.$row['reports'].'</td>
		<td>'.$row['url'].'</td>
		<td>'.(is_file('/home/kestutis/musudarzelis.lt/uploads/'.substr($db_prefixes[$row['kindergarten_id']], 0, -1).'/archive/'.$row['url'].'.zip') ? '' : 'N/A').'</td>
		<td><a href="http://dev.musudarzelis.lt/private/tools/archive.php?archive_id='.$row['archive_id'].'">Keisti</a></td>
	</tr>';	
}
?>
</table>
<?php
echo round(microtime(true)-$start, 2).'; DB: '.count($db_prefixes);
