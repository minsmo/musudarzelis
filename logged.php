<?php
include 'main.php';
if(!defined('DARBUOT') || !DARBUOT) exit();
echo 'OK';
//print ini_get("session.gc_maxlifetime");
//http://www.laurencegellert.com/2012/08/php-sessions-not-being-deleted-on-ubuntu-server/

//https://bugs.launchpad.net/ubuntu/+source/php5/+bug/316441
//php session delete bug

//Vietoje 24 min. į 4 h t.y. iš 1440 į 14400

//sudo find /var/lib/php5/ -depth -mindepth 1 -maxdepth 1 -type f -cmin +$(/usr/lib/php5/maxlifetime)
//sudo ls -l /var/lib/php5/
//cat /usr/lib/php5/maxlifetime
//    /usr/lib/php5/maxlifetime
/*
cat /etc/cron.d/php5
# /etc/cron.d/php5: crontab fragment for php5
#  This purges session files older than X, where X is defined in seconds
#  as the largest value of session.gc_maxlifetime from all your php.ini
#  files, or 24 minutes if not defined.  See /usr/lib/php5/maxlifetime

# Look for and purge old sessions every 30 minutes
09,39 *     * * *     root   [ -x /usr/lib/php5/maxlifetime ] && [ -d /var/lib/php5 ] && find /var/lib/php5/ -depth -mindepth 1 -maxdepth 1 -type f -cmin +$(/usr/lib/php5/maxlifetime) ! -execdir fuser -s {} 2>/dev/null \; -delete
*/
