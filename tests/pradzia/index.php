<!DOCTYPE html>
<html dir="ltr" lang="lt-lt">
<head>
    <meta charset="utf-8">
    <!-- <base href="http://musudarzelis.lt/" /> -->
    <title>MūsųDarželis.lt - </title>
    <meta name="description" content="MūsųDarželis.lt - šiuolaikinė priemonė, kurioje tėvai, auklėtojai ir vadovybė bendrauja tarpusavyje siekdami bendro tikslo - kuo geriau išugdyti vaikus."><!-- ška vieta internete -->
    <link rel="stylesheet" href="css/index.css">
    <link type="text/plain" rel="author" href="/humans.txt">
    <!--[if lte IE 8]>
	<script src="/libs/html5.js"></script>
	<![endif]-->
</head>
<body>
	<div id="wrapper">
		<!-- <header> musu_darzelis.gif -->
			<a href=""><img src="img/musuDarzelis.png" id="logo" alt="MūsųDarželis – tai šiuolaikinė priemonė, kurioje tėvai, auklėtojai ir vadovybė bendrauja tarpusavyje siekdami bendro tikslo - kuo geriau išugdyti vaikus." title="MūsųDarželis – tai šiuolaikinė priemonė, kurioje tėvai, auklėtojai ir vadovybė bendrauja tarpusavyje siekdami bendro tikslo - kuo geriau išugdyti vaikus."></a> <!-- ška vieta internete -->
			<nav>
				<ul>
					<li><a href="?"<?php if(!(isset($_GET['pradeti']) || isset($_GET['privalumai']) || isset($_GET['kontaktai']) || isset($_GET['atsiliepimai']))) echo ' class="current"'; ?>>Pradžia</a></li> <!-- Namai -->
					<li><a href="?pradeti"<?php if(isset($_GET['pradeti'])) echo ' class="current"'; ?>>Kaip pradėti?</a></li>
					<?php /* <li><a href="?privalumai"<?php if(isset($_GET['privalumai'])) echo ' class="current"'; ?>>Privalumai</a></li>
					<li><a href="?atsiliepimai"<?php if(isset($_GET['atsiliepimai'])) echo ' class="current"'; ?>>Atsiliepimai</a></li> */ ?>
					<li><a href="?kontaktai"<?php if(isset($_GET['kontaktai'])) echo ' class="current"'; ?>>Kontaktai</a></li>
				</ul>
			</nav>
			<!-- 
			<nav>
				<ul>
					<li><a style="font-weight: normal !important;" href="" class="current">Namai</a></li>
					<li><a style="font-weight: normal !important;" href="">Kaip pradėti?</a></li>
					<li><a style="font-weight: normal !important;" href="">Privalumai</a></li>
					<li><a style="font-weight: normal !important;" href="">Atsiliepimai</a></li>
					<li><a style="font-weight: normal !important;" href="">Kontaktai</a></li>
				</ul>
			</nav>
			-->
		<!-- </header> -->
		<div id="content">
			<div style="height: 1px;"></div><!-- Fx & Chrome bug -->
			<!--
			<img src="img/header.png" style="width: 914px; margin: 0 auto; display: block;">
			
			<div style="width: 886px; height: 231px; margin: 0 auto; display: block; margin-top: 10px;
/*
-webkit-box-shadow: 3px 0px 13px 2px rgba(0, 0, 0, 0.22);
-moz-box-shadow:    3px 0px 13px 2px rgba(0, 0, 0, 0.22);
box-shadow:         3px 0px 13px 2px rgba(0, 0, 0, 0.22);*/

-webkit-box-shadow: 0px 0px 10px 3.9px rgba(0,0,0,0.22);
-moz-box-shadow:    0px 0px 10px 3.9px rgba(0,0,0,0.22);
box-shadow:         0px 0px 10px 3.9px rgba(0,0,0,0.22);
background-image: url('img/header_without_shadow.png');
">
				<div style="border:1px solid #a8bb85; width: 884px; height: 229px;">
				</div>
			</div>
			-->
			
			<div id="main-header"><?php //http://stackoverflow.com/questions/9428773/converting-photoshop-drop-shadow-into-css3-box-shadow
?>
				<div id="main-header-inner">
					<form id="login" method="post">
						<div id="email-box"><label for="email-field">Jūsų el. paštas</label><input id="email-field" type="email" name="pastas" value=""></div>
						<div id="passwd-box"><label for="passwd-field">Slaptažodis</label><input id="passwd-field" type="password" name="slaptazodis" value=""></div>
						<div class="cl"></div>
						<a href="pamirsau_slaptazodi" id="lost-password">Pamiršau slaptažodį</a>
						<div><input type="submit" id="login-btn" name="prisijungimas" value="Prisijungti"></div>
					</form>
					<div class="cl"></div>
				</div>
			</div>
			<?php if(isset($_GET['pradeti'])) {	?>
				<div id="text">
					<p>Duodame 3 mėn. bandomąjį laikotarpį nemokamai.</p>
					<p>Vedame seminarus: <!-- ne tik --> švietimo centruose<!-- , bet --> ir individualius darželiams<!-- individualius kiekvienam darželiui atskirai -->.</p>
					<p>Esant poreikiui atvykstame pristatyti „Mūsų darželis“ sistemos.</p>
					
					<p>Norint pradėti naudoti arba išbandyti „Mūsų darželis“ sistemą būtina, kad pagrindinis žmogus, kuris dažniausiai būna direktorius arba direktoriaus pavaduotojas ugdymui, parašytų mums el. laišką adresu <a href="mailto:info@musudarzelis.lt">info@musudarzelis.lt</a>  <!-- bent jau -->su: savo vardu ir pavarde, darželio pavadinimu ir el. pašto adresu kuriuo norės gauti prieigą prie sistemos.</p>
					<p>Gavę Jūsų el. laišką su būtinais duomenimis mes sukūrę sistemos aplinką atsiųsime prisijungimo duomenis pagrindiniam ar keliems pagrindiniams žmonėms.</p>
					
					<!-- <p>Konsultuojame nemokamai, prašome bet kokiems asmenims susijusiems su darželiais kontaktuoti el. paštu <a href="mailto:info@musudarzelis.lt">info@musudarzelis.lt</a>. Kartu stengsimės surasti sprendimą! :)</p> -->
					<p><!-- Maloniai prašome asmenis, susijusius su darželiais, rašyti -->Rašykite el. paštu <a href="mailto:info@musudarzelis.lt">info@musudarzelis.lt</a>. Kartu stengsimės rasti bendrą sprendimą!</p>
					<!-- <p>Norint pradėti pilnai naudoti „Mūsų darželis“ reikia, kad ugdymo įstaiga (kurioje dirbate ar ugdosi Jūsų vaikai) būtų pasirašiusi naudojimosi „Mūsų darželis“ sistema sutartį.</p> -->
					
				</div>
			<?php } elseif(isset($_GET['privalumai'])) {?>
				<div id="text">
					<p>Svarbu paminėti sistemos teikiamas galimybes. Viena šios sistemos galimybių - sumažina laiko sąnaudas pildant dokumentaciją. Tai labai reikalinga pedagogams, nes pildant ugdymo veiklos planus, rengiant vaiko pasiekimų ir gebėjimų ataskaitas bei rašant įvairią informaciją tėvams ir t. t. sugaištama daug laiko. Būtent ši sistema pedagogams palengvintų rašymo darbų krūvį.</p>
					<p>Antroji galimybė - įsitraukti į kasdieninį darbą nuotoliniu būdu. Darželio vadovai naudojantys šią sistemą galės stebėti ir kartu dalyvauti kiekvieno pedagogo veikloje. Pvz.: pavaduotoja ugdymui gali peržiūrėti savaitės planą, teikti pedagogui pasiūlymus ir rekomendacijas.</p>
					<p>Trečioji galimybė - suteikia tėvams tikslingą ir nepertraukiamą informaciją apie vaiką. Tėvai šios sistemos pagalba galės matyti ką veikė darželyje jo vaikas tam tikrą dieną, galės susipažinti su individualia veikla, jos priemonėmis, pamatyti vaiko pasiekimus ir nesėkmes. Tai suteiks galimybę pakartoti tas užduotis, įtvirtinti įgūdžius ir padėti vaikui pašalinti nesėkmes.</p>
					<p>Ketvirtoji galimybė - tėvų bendradarbiavimas su pedagogais tampa daug paprastesnis. Visa tėvams reikalinga informacija yra patalpinama į sistemą todėl tėvai gali bet kada su ja susipažinti. Pvz.: kvietimas į susirinkimą, parašomas edukacinis straipsnis, pildoma anketa.</p>
					<p>Penktoji galimybė - sistema lanksčiai pritaikoma prie darželio poreikių.</p>

<h2>„MŪSŲ DARŽELIS“ FUNKCIONALUMAS</h2>

<h3>Darželio vadovas elektroniniame dienyne „Mūsų darželis“ gali naudotis įvairiomis funkcijomis:</h3>
<ul>
	<li>gauti darželio veiklos ataskaitas bei spausdinti jas, būtinas dienyno archyvavimui (veiklos ataskaitas, kiekvienos grupės tabelius ir pan.);</li>
	<li>stebėti auklėtojų darbo aktyvumą (matyti veiklos ataskaitas, kiekvienos grupės tabelius ir kt.);</li>
	<li>greitai surasti ir matyti darželio darbuotojų kontaktus;</li>
	<li>matyti darželio grupių sąrašus;</li>
	<li>greitai informuoti visus darželio tėvus apie vyksiantį visuotinį susirinkimą, apie šventes ir t. t.;</li>
	<li>susisiekti su pedagogais rašant žinutes per sistemą, teikti pasiūlymus/pastabas ir pan.</li>
	<li>ir kt.</li>
</ul>


<h3>Auklėtojai ir kiti ugdomąja veikla užsiimantys darželio pedagogai elektroniniame dienyne „Mūsų darželis“ gali naudotis įvairiomis funkcijomis:</h3>
<ul>
	<li>patogiai pildyti dienyną ir matyti visą reikiamą informaciją viename lange;</li>
	<li>matyti kiekvieno vaiko mokėtiną sumą, nes sistema automatiškai apskaičiuoja už kiekvieną mėnesį kiekvieno vaiko mokėtiną sumą už darželį;</li>
	<li>paprastai ir greitai suvesti savaitės/metų planus ir siekius; </li>
	<li>matyti visą informaciją apie vaikų sveikatos duomenis; </li>
	<li>matyti tėvų kontaktinę informaciją; </li>
	<li>gauti reikiamas ataskaitas; </li>
	<li>bendrauti žinutėmis, dalintis naujienomis ir mintimis su kolegomis, vadovybe bei vaikų tėvais;</li>
	<li><em>buhalteris</em> matydamas grupių tabelius sistemoje gali lengvai susitikrinti savo turimą informaciją;</li>
	<li><em>medicinos seselė</em> gali suvesti sveikatos pastabas;</li>
	<li>ir kt.</li>
</ul>

<h3>Tėvai elektroniniame dienyne „Mūsų darželis“ gali naudotis įvairiomis funkcijomis:</h3>
<ul>
	<li>matyti savo vaiko/vaikų suvestus duomenis darželyje;
	<li>pasižiūrėti lankomumą, pastabas, pasiekimus ir kt.;
	<li>susipažinti su neformaliojo ugdymo veiklomis ir matyti kada vyksta;
	<li>susipažinti su grupės veikla ar vaiko individualia veikla;
	<li>matyti grupės tėvų kontaktus (kontaktai rodomi tik su tėvų sutikimais);
	<li>bendrauti tiek su pedagogais tiek su tėvais;
	<li>gauna visą aktualią ir nepertraukiamą informaciją iš darželio (susirinkimai, šventės ir t. t.);
	<li>ir kt.
</ul>

<h2>KODĖL „MŪSŲ DARŽELIS“?</h2>
<ul>
	<li>„Mūsų darželis“ komanda, stengiasi atsižvelgti į kiekvieno darželio poreikius.</li>
	<li>Darželiams duodama galimybė pasirinkti kokiais sistemos moduliais jie nori naudotis.</li>
	<li>Suteikiama galimybė 3 mėn. naudotis sistema nemokamai ir išbandyti jos privalumus.</li>
	<li>Galime atvykti ir nemokamai pristatyti sistemą.</li>
	<li>Naudotis sistema „Mūsų darželis“ paprasta ir nesudėtinga - užtenka pagrindinių kompiuterio įgūdžių.</li>
	<li>Rengiame mokymus, pavyzdžiui, „Sistemos naudojimo praktinės užduotys“ Kauno pedagogų kvalifikacijos centre (KPKC), seminarą ikimokyklinio ugdymo įstaigų vadovams, pedagogams ir specialistams „IS MŪSŲ DARŽELIS diegimas ikimokyklinio ugdymo įstaigoje“ Šiaulių miesto savivaldybės Švietimo centre.</li>
	<li>„Mūsų darželis“ atitinka saugaus ir modernaus tinklalapio standartus, tad jūsų duomenys ir dokumentai bus saugūs.</li>
</ul>

<h2>LAUKIAMI REZULTATAI</h2>
<p>Siekiama sukurti tokią sistemą, kurioje darželis, grupės vaikai ir tėvai sudarytų vieną darnią bendruomenę.</p>
<p>Laukiami geresni pasiekimai šių tikslų:
<ul>
	<li>Pagerinti tėvų ir vaikų partnerystę;</li>
	<li>Įtraukti tėvus į grupės gyvenimą;</li>
	<li>Padėti sukurti ugdymo tęstinumą namuose;</li>
	<li>Stengtis atitikti pedagogų ir tėvų lūkesčius.</li>
</ul>
</p>

<h2>SUSISIEKITE SU MUMIS</h2>
<p>Jei norite plačiau sužinoti apie sistemą ar išbandyti nemokamai, skambinkite telefonu +3706 07 72346 (Ramunei Rupeikaitei) arba rašykite el. paštu <a href="mailto:info@musudarzelis.lt">info@musudarzelis.lt</a>. Esant poreikiui galime atvykti.</p>
				</div>
				
			<?php } elseif(isset($_GET['atsiliepimai'])) { ?>
				<div id="text">
<p>„Vien suvokus, tai, kad aš galėsiu būdama darbe prisijungti prie sistemos ir matyti informaciją apie savo vaiką, aš iškart sakau „Taip“ tokiai idėjai“.<br>
 - Mamos atsiliepimas iš užpildytos anketos</p>

<p>„Manau tai reikalingas dalykas mūsų gyvenime, ypač dabar kai su vaikais vis mažiau praleidžiu laiko. Kadangi šiuo metu gyvenu užsienyje, tai džiaugčiausi jei galėčiau pažiūrėti nuotoliniu būdu kaip mano vaikui sekasi.“<br>
 - Mamos atsiliepimas iš užpildytos anketos</p>

<p>„Tokia sistema yra puikus būdas pagerinti ugdymo proceso kokybę, nes aš turėsiu ne tik nuolatinę sąveiką su darželiu, bet ir žinosiu ką mano vaikas veikia darželyje ir tai galėsiu pritaikyti savo namuose.“<br>
 - Tėvelio atsiliepimas iš užpildytos anketos</p>
				</div>
			<?php } elseif(isset($_GET['kontaktai'])) {?>
				<div id="text">
					<p>Maloniai prašome iškilus klausimams, susijusiems su darželiais ar kompiuteriais ar programine įranga, kreiptis:</p>
					<p>El. paštu <a href="mailto:info@musudarzelis.lt">info@musudarzelis.lt</a></p>
					<p>Konsultacijų tel. +370 07 72 346 (Ramunė)</p>
				</div>
			<?php } else { ?>
			<div id="intro">
				<p style="font-size: 19px;"><strong class="first">Mūsų Darželis</strong> – tai šiuolaikinė <!-- ška vieta/buveinė --> priemonė<!-- internete -->, kurioje tėvai, darželio pedagogai ir vadovai dirba bei bendrauja perduodami žinias <!-- tarpusavyje --> siekdami bendro tikslo - kuo geriau išugdyti vaikus.</p><!-- tai šiuolaikiška vieta, kurioje tėvai, auklėtojai ir vadovybė bendrauja siekdama bendro tikslo (kuo geriau išugdyti vaikus). -->
				<p>Ši internetinė priemonė<!-- vieta -->:
				<ul>
					<li>palengvina informacijos prieinamumą (kuriems ji skirta) iš bet kurios pasaulio vietos;</li><!-- bendravimą -->
					<!-- -->
					<li>suteikia papildomą bendravimo galimybę iš bet kurios pasaulio vietos;</li>
					<li><!-- su -->mažina rašymą ranka;</li>
					<li>informuoja ir primena<!-- padidina informuotumą -->;</li>
					<li>palengvina dokumentų perdavimą.</li>
				</ul></p>
		    </div>
			<div id="block-wrapper">
				<div class="block">
					<img src="img/secure.gif" width="66" alt="Saugu">
					<span class="first">Saugu.</span> Prisijungti prie sistemos gali tik registruoti naudotojai, o kiekvienas iš tėvų gali matyti tik savo vaiko informaciją. Prisijungimą prie sistemos suteikia darželio vadovai<!-- Sistemos prisijungimai suteikiami darželio vadovybės -->, todėl išoriniai naudotojai negali prisiregistruoti ir prisijungti prie šios sistemos.
					<!-- <br><br>
					Įeiti į sistemą gali tik registruoti naudotojai. Naujus naudotojus užregistruoja tik tam teisę turintys naudotojai. Kiekvienas naudotojas mato tik jam skirtą informaciją. -->
					<br><br>
					Įeiti į darželio sistemą gali tik naudotojai, kuriuos užregistruoja <!-- tik --> tam teisę turintys naudotojai. Kiekvienas naudotojas mato tik jam skirtą informaciją. Duomenis galima atsispausdinti. Serveryje daromos atsarginės duomenų kopijos. <!--    bet kada -->
					<!-- Prisijungti prie sistemos -->
				</div>
				<div class="block">
					<img src="img/discuss.gif" width="75" alt="Diskusijos tarp tėvų ir darželio darbuotojų">
					<span class="first"><!-- Tėvų ir darželio darbuotojų bendravimas -->Diskusijos tarp tėvų ir darželio darbuotojų.</span>
					<!-- Sistema skatina tėvų ir darželio darbuotojų bendravimą. --> Darželio darbuotojai gali <!-- greitai ir efektyviai --> perduoti tėvams aktualią informaciją susijusią su jų vaiku, taip pat atsakyti į tėvams iškilusius klausimus. Tėvai <!-- paprastai ir greitai --> patogiu metu gali perduoti darželio darbuotojams reikalingą informaciją susijusią su jų vaiku.
					<!-- <br><span class="first">Pokalbiai ir diskusijos tarp tėvų ir darželio darbuotojų</span> - Taip pat sistema leidžia --> <!-- Darbuotojų ir/ar tėvų grupė gali bendrauti visiems kartu vienoje diskusijų lentoje. Naudotojų grupė gali dalintis aktualia informacija, dalyvauti diskusijose sprendžiant įvairias iškilusias problemas. -->
				</div>
				<div class="block">
					<img src="img/home.gif" width="85" alt="Lengvai prieinama nesant darželyje">
					<span class="first">Lengvai prieinama nesant darželyje.</span> Prisijungti prie sistemos galima iš bet kurios pasaulio vietos ir <!-- bet kurio --> tuo metu patogaus įrenginio, turinčio interneto prieigą ir šiuolaikinę programinę įrangą. <!-- Tėvai informaciją apie savo vaiką gali matyti prisijungę iš kompiuterio namuose ar darbo vietoje, taip pat iš bet kurios kitos vietos prisijungę išmaniuoju telefonu ar planšete. -->
				</div>
			</div>
			<!--
			
			SAUGU

• prisijungti prie sistemos gali tik registruoti vartotojai, 

• kiekvienas iš tėvų gali matyti tik savo vaiko informaciją. 

• prisijungimą prie sistemos suteikia darželio vadovybė, todėl išoriniai vartotojai negali prisiregistruoti ir prisijungti prie šios sistemos. 

DISKUSIJOS 

• Sistema skatina tėvų ir darželio darbuotojų bendravimą. 

• Darželio darbuotojai gali greitai ir efektyviai perduoti tėvams aktualią informaciją susijusią su jų vaiku, taip pat atsakyti į tėvams iškilusius klausimus. 

• Tėvai paprastai ir greitai gali perduoti darželio darbuotojams reikalingą informaciją susijusią su jų vaiku. 

• darbuotojai ir/ar tėvų grupė gali bendrauti visi kartu vienoje diskusijų lentoje. 

• vartotojų grupė gali dalintis aktualia informacija, dalyvauti diskusijose sprendžiant įvairias iškilusias problemas. 

PRIĖJIMAS

• lengvai prieinama nesant darželyje. 

• prisijungti prie sistemos galima iš bet kurios pasaulio vietos ir bet kurio įrenginio, svarbu, kad būtų interneto prieiga.
			-->
			<?php } ?>
		</div>
		<img src="img/footer.png" id="footer-img" alt="">
		<div id="footer">
			<!-- MūsųDarželis &copy; 2010–2014; &nbsp; El. paštas - info@musudarzelis.lt<br>
			&copy; 2010–2014 www.musudarzelis.lt Visos teisės saugomos<br>
			&copy; 2010–2014 www.musudarzelis.lt<br> -->
			&copy; 2010–2014 „Mūsų darželis“<br>El. paštas - <a href="mailto:info@musudarzelis.lt">info@musudarzelis.lt</a>
		</div>
	</div>
<?php
/*
	<script>
// Assume we have the following values in Photoshop
// Blend Mode: Normal (no other blend mode is supported in CSS)
// Color: 0,0,0
// Opacity: 25%
// Angle: 135deg
// Distance: 4px
// Spread: 0%
// Size: 4px

// Here's some JavaScript that would do the math
function photoshopDropShadow2CSSBoxShadow(color, opacity, angle, distance, spread, size) {
  // convert the angle to radians
  angle = (180 - angle) * Math.PI / 180;

  // the color is just an rgba() color with the opacity.
  // for simplicity this function expects color to be an rgb string
  // in CSS, opacity is a decimal between 0 and 1 instead of a percentage
  color = "rgba(" + color + "," + opacity/100 + ")";

  // other calculations
  var offsetX = Math.round(Math.cos(angle) * distance) + "px",
      offsetY = Math.round(Math.sin(angle) * distance) + "px",
      spreadRadius = (size * spread / 100) + "px",
      blurRadius = (size - parseInt(spreadRadius, 10)) + "px";
  return offsetX + " " + offsetY + " " + blurRadius + " " + spreadRadius + " " + color;
}

// let's try it
// for simplicity drop all of the units
console.log(photoshopDropShadow2CSSBoxShadow("0,0,0", 22, 0, 3, 30, 13));

	</script>
*/
?>
</body>
</html>
