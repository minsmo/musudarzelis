<!DOCTYPE html>
<html dir="ltr" lang="lt-LT">
<head>
	<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="X-UA-Compatible" content="chrome=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	-->
	<meta charset="UTF-8">
	<title>Kauno Montesori mokykla-darželis „Meškutis“</title>
	<link rel="stylesheet" href="/za/is.css">
	<link rel="stylesheet" href="/css/jquery-ui-1.10.4.min.css">
	<script>var ie = false;</script>
	<!--[if lt IE 12]>
	<style type="text/css">
	select {width: 100%;}
	.sel {background-image:none;}
	</style>
	<![endif]-->
	<!--[if lt IE 8]>
	<style type="text/css">
	select { width: auto; }
	</style>
	<![endif]-->
	<!--[if lt IE 7]>
	<style type="text/css">
	#nav {position: absolute; top: 0;}
	#content-sidebar {left: 0;}
	#content-wrapper {margin-left: 229px;}
	</style>
	<script>ie = true;if (!window.console) console = {log: function() {}};</script>
	<![endif]-->
	<?php
	/*
	http://www.quirksmode.org/css/condcom.html
	http://msdn.microsoft.com/en-us/library/ms537512(v=vs.85).aspx
	http://www.sitepoint.com/web-foundations/internet-explorer-conditional-comments/
	http://webdesignerwall.com/tutorials/css-specific-for-internet-explorer
	https://www.google.lt/search?q=different+browser+view&oq=browser+view+different+&aqs=chrome.1.69i57j0l3.12758j0j7&sourceid=chrome&es_sm=93&ie=UTF-8
	
	*/
	?>
	<script type="text/javascript" src="/libs/jquery-1.11.1.min.js"></script><!-- jquery-1.10.2.min.js -->
	<script type="text/javascript" src="/admin/common.js?2"></script>
	<script type="text/javascript" src="/libs/jquery.ui.datepicker-lt.js"></script>
	<script type="text/javascript" src="/libs/jquery-ui-1.10.4.min.js"></script>
	<script type="text/javascript">
	$.datepicker.setDefaults(
		$.extend(
			{'dateFormat':'yy-mm-dd'},
			$.datepicker.regional['lt']
		)
	);
	$(function() {
		$(".datepicker").datepicker();
		var open = true;
		$('#menu-toggle').click(function() {
			$('#nav').toggle();
			if(open) {
				console.log('0px');
				if(ie)
					$('#content-wrapper').css('marginLeft', 0);
				else
					$('#content-sidebar').css('left', '0');
				$('#menu-toggle').css('transform', 'rotate(180deg)');
				$('#menu-toggle').addClass('menu-toggle-closed');
				$('#menu-toggle').removeClass('menu-toggle-opened');
				
			} else {
				if(ie)
					$('#content-wrapper').css('marginLeft', '229px');
				else
					$('#content-sidebar').css('left', '229px');
				$('#menu-toggle').css('transform', 'rotate(0deg)');
				$('#menu-toggle').addClass('menu-toggle-opened').removeClass('menu-toggle-closed');
			}
			open = !open;
		});
	});
	</script>
	
	</head>
<body>

<div id="header">
	<div id="title" class="no-print">
		Kauno lopšelis-darželis 
	</div>
	Sveika, auklėtoja Danute Cvirkiene
</div>
<div id="content-sidebar">
	<div id="nav">
		Auklėtoja
		<ul>
			<li class="active" style="background-image: url(img/menu-home.png);"><a href="">Pagrindinis</a></li>
			<li style="background-image: url(img/menu-password.png);"><a href="">Profilio nustatymai</a></li>
			<li style="background-image: url(img/menu-groups.png); height: auto; background-position: left 2px;">
				<div style="background-image: url(img/menu-opened1.png);  background-position: 165px; 9px; background-repeat: no-repeat;"><a href="">Grupės</a></div>
				<ul>
					<li style="background-image: url(img/menu-current.png);  background-position: left 7px;"><a href="">Žvirbliukai</a></li>
					<li style="background-image: url(img/menu-opened.png);  background-position: left 9px;">Drambliukai
						<ul>
							<li><a href="">Lankomumas</a></li>
							<li><a href="">Pateisinimai</a></li>
							<li class="active"><a href="">Dokumentai</a></li>
							<li><a href="">Sveikatos pastabos</a></li>
							<li><a href="">Tabeliai</a></li>
							<li><a href="">Planavimas</a></li>
							<li><a href="">Vaikai</a></li>
							<li><a href="">Vaiko pasiekimai</a></li>
							<li><a href="">Grupės veikla</a></li>
							<li><a href="">Priminimai</a></li>
						</ul>
					</li>
				</ul>
			</li>
			<li style="background-image: url(img/menu-messages.png);"><a href="">Žinutės (<strong>1</strong>)</a></li>
			<li style="background-image: url(img/menu-letters.png);"><a href="">Masinis laiškas</a></li>
			<li style="background-image: url(img/menu-logout.png);"><a href="">Atsijungti</a></li>
		</ul>
		
		
		<hr>
		Vadovybė
		<ul>
			<li class="active" style="background-image: url(img/menu-home.png);"><a href="">Pagrindinis</a></li>
			<li style="background-image: url(img/menu-password.png);"><a href="">Profilio nustatymai</a></li>
			<li>Vaikai</li>
			<li>Darbuotojai</li>
			<li>Prieiga<!-- Raktas --></li>
			<li class="active">Dokumentai</li>
			<li style="background-image: url(img/menu-groups.png); background-position: left 2px;">Grupės</li>
			<li title="Papildomos grupių veiklos sąrašas"><a href="" class="abbr">Papildoma veikla</li>
			<li title="Papildomos grupių veiklos tvarkaraštis"><a href="" class="abbr">Pap. veiklos tvarkaraštis</a></li>
			<li>Tabeliai</li>
			<li><del>Planavimas</del></li>
			<li>Planavimo formos</li>
			<li>Planavimo dok.</li>
			<li title="Vaiko pažanga ir pasiekimai"><a href="" class="abbr">Vaiko pasiekimai</a></li>
			<li><del>Kvitai</del></li>
			<li><del>Mokėjimai</del></li>
			<li style="background-image: url(img/menu-messages.png);"><a href="">Žinutės (<strong>1</strong>)</a></li>
			<li style="background-image: url(img/menu-letters.png);"><a href="">Masinis laiškas</a></li>
			<li><a href="">Nustatymai</a></li>
			<li>Archyvas (Archyvavimas)</li>
			<li><del>Puslapiai</del></li>
			<li style="background-image: url(img/menu-logout.png);"><a href="">Atsijungti</a></li>
		</ul>
	</div>
	<div id="content-wrapper">
		<!-- <div class="ok" style="margin: 0px;">Duomenų bazėje dokumento informacija atnaujinta.</div> -->
		<div id="menu-toggle" class="menu-toggle-opened">&lt;</div>
		<h1>Vaiko dokumentai</h1>
	
		<div id="content">
			<?php if(isset($_GET['save'])) { ?> <div class="ok">Duomenų bazėje dokumento informacija atnaujinta.</div><?php } else echo '<br>' ?>
			Greitasis meniu: <a href="?#document-form">+ Įkelti vaiko dokumentą</a>
		
			<h2>Vaiko dokumentai</h2>
			<div style="float: left; *float: none;">
				<form style="float: right; margin-bottom: 7px;">
					<div class="sel" style="margin-right: 10px;"><select>
					<option>Jonas Jonaitis</option>
					</select></div>
					<input type="submit" class="filter" value="Filtruoti">
				</form>
				<table class="vertical-hover">
				<tr>
					<th>Dokumentas</th>
					<th>Vaikas</th>
					<th>Tipas</th>
					<th>Aprašymas</th>
					<th>Aprašymas darbuotojams</th>
					<th>Įkeltas</th>
					<th>Veiksmai</th>
				</tr>
				<tr>
					<td><a href="./uploads/default/41/ramintai.docx">ramintai.docx</a></td>
					<td>Skaistė Jagalevičiūtė</td>
					<td>Gimimo liudijimas</td>
					<td>Gimimo liudijimas</td>
					<td></td>
					<td>2014-02-10 22:28:14</td>
					<td><a href="?dok&amp;id=26#document_form">Keisti</a> <a href="?dok&amp;del_id=26" onclick="return confirm('Ar tikrai norite ištrinti?');">Trinti</a></td>
				</tr>
				<tr>
					<td><a href="./uploads/default/3/DSC08119.JPG">DSC08119.JPG</a></td>
					<td>Rasa Rupeikaitė</td>
					<td>Raštelis iš gydytojo</td>
					<td>2010-10-11 sirgo</td>
					<td></td>
					<td></td>
					<td><a href="?dok&amp;id=1#document_form">Keisti</a> <a href="?dok&amp;del_id=1" onclick="return confirm('Ar tikrai norite ištrinti?');">Trinti</a></td>
				</tr>
				<tr>
					<td><a href="./uploads/default/9/ramunes.doc">ramunes.doc</a></td>
					<td>Rivilė Sakalauskaitė</td>
					<td>Anketa</td>
					<td>2011-05-05</td>
					<td></td>
					<td></td>
					<td><a href="?dok&amp;id=6#document_form">Keisti</a> <a href="?dok&amp;del_id=6" onclick="return confirm('Ar tikrai norite ištrinti?');">Trinti</a></td>
				</tr>
				</table>
			</div>
			<div style="clear: left"></div>
		
		
		
		
		
			<h2>+ Įkelti vaiko dokumentą</h2>
			<form enctype="multipart/form-data" method="post" action="?dok&amp;pridetiNauja" id="document-form">
			<p><label style="width: 65px;" for="vaiko_id">Vaikas<span class="required">*</span>:</label>
				<div class="sel"><select name="vaiko_id" id="vaiko_id">
				<option value="3">Rasa Rupeikaitė</option><option value="8">Miglė Linkaitė</option><option value="9">Rivilė Sakalauskaitė</option><option value="10">Danutė Petraitytė</option><option value="15">Larisa Kalpokaitė</option><option value="17">Ramunė Rugytė</option><option value="18">Aistė Ivonytė</option><option value="22">Lina Lavičiūtė</option><option value="19">Emilija Akoličaitė</option><option value="20">Ignas Voropaj</option><option value="21">Sandra Rybokaitė</option><option value="23">Tapilė Los</option><option value="24">Jonas Palys</option><option value="25">Karolis Milcevičius</option><option value="26">Saulė Stankevičiūtė</option><option value="27">Apolonas Narakovičius</option><option value="29">Arūnas Rozenbergas</option><option value="30">Justė Banavičiūtė</option><option value="31">Gintarė Stangvilaitė</option><option value="32">Oksna Barojan</option><option value="33">Sigitas Adomaitis</option><option value="34">Rasa Girkštaitė</option><option value="36">Jurgita Krukonytė</option><option value="37">Alma Kavaliauskaitė</option><option value="40">BirR VašŠ</option><option value="41">Skaistė Jagalevičiūtė</option><option value="42">Skaistėėėė Jagalevičiūtėėėėė</option>        </select></div>
				
			</p>
			<p>
				<label style="width: 65px;" for="tipas">Tipas<span class="required">*</span>:</label>
				<div class="sel"><select name="tipas" id="tipas">
				<option value="0">Anketa</option><option value="1">Gimimo liudijimas</option><option value="2">Lengvata</option><option value="3">Pažyma apie nelaimę</option><option value="4">Pažyma apie šeimos sudėtį</option><option value="5">Pažyma iš darbovietės</option><option value="6">Prašymas</option><option value="7">Raštelis iš gydytojo</option><option value="8">Sveikatos būklės pažymėjimas</option><option value="9">Šeimos socialinė padėtis</option>        </select></div>
			</p>
			<p><label for="aprasymas">Trumpas aprašymas (raštelio iš gydytojo atveju):</label><input id="aprasymas" type="text" style="clear: left; width: 300px;" name="aprasymas" value=""><!-- <span style="text-decoration: line-through">Pastaba: tai ką įvesite rodys ataskaitose kai susiesite dokumentą su nelankytomis dienomis, todėl reikėtų įvesti laikotarpį nuo kada iki kada.</span> --></p>
			<p><label for="spec_aprasymas">Aprašymas matomas tik darbuotojams:</label><input type="text" style="clear: left; width: 300px;" name="spec_aprasymas" id="spec_aprasymas" value=""></p>
			<p><label for="file">Dokumentas Jūsų kompiuteryje<span class="required">*</span>:</label><input required="required" type="file" style="clear: left; width: 300px;" name="file" id="file"></p>
			<p><input type="submit" class="submit" value="Išsaugoti"></p>
			</form>
		</div>
	</div>
</div>
</body>
</html>
