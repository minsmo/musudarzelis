<?php
//Function services returning data from data storage (databases)

//
// Main config
//
$result = db_query("SELECT yt.* FROM `".DB_config."` yt JOIN (SELECT `title`, MAX(`valid_from`) `valid_from` FROM `".DB_config."` WHERE `valid_from`<='".CURRENT_DATE."' GROUP BY `title`) ss ON yt.`title`=ss.`title` AND yt.`valid_from`=ss.`valid_from`");//greatest-n-per-group
//http://stackoverflow.com/questions/7745609/sql-select-only-rows-with-max-value-on-a-column
//http://dev.mysql.com/doc/refman/5.0/en/example-maximum-column-group-row.html
$config = [];
while ($row = mysqli_fetch_assoc($result))
	$config[$row['title']] = $row['value'];

//Global data functions
function getConfig($title, $date = CURRENT_DATE) {
	static $config = null;
	if(empty($date))
		$date = CURRENT_DATE;
	if($config === null) {
		$config = [];
		$result = db_query("SELECT * FROM `".DB_config."` ORDER BY `valid_from` DESC");// WHERE `valid_from`<='".CURRENT_DATE."'
		while ($row = mysqli_fetch_assoc($result))
			$config[$row['title']][$row['valid_from']] = $row['value'];
	}
	foreach($config[$title] as $from_date => $value) {
		if($from_date <= $date) {
			return $value;
		}
	}
}


function getAllGroups($id = '') {
	static $groups = null;
	if($groups === null) {
		$groups = [];
		$result = db_query("SELECT `ID`, `pavadinimas` FROM `".DB_groups."` WHERE `isHidden`=0 ORDER BY `pavadinimas`");
		while ($row = mysqli_fetch_assoc($result))
			$groups[$row['ID']] = $row['pavadinimas'];
	}
	if($id == '')
		return $groups;
	else
		return isset($groups[$id]) ? $groups[$id] : 'Visos';
}

function getAllowedGroups() {
	static $groups = null;
	if($groups === null) {
		global $all_groups_for_person_type;
		if(isset($all_groups_for_person_type[$_SESSION['USER_DATA']['person_type']])) {//ADMIN || BUHALT
			$groups = getAllGroups();
		} else {
			$groups = [];
			$result = db_query("SELECT `".DB_groups."`.`ID`, `".DB_groups."`.`pavadinimas`
			FROM `".DB_groups."` JOIN `".DB_employees_groups."` ON `".DB_groups."`.ID=`".DB_employees_groups."`.grup_id
			WHERE `".DB_employees_groups."`.darb_id=".(int)DARB_ID." AND `".DB_groups."`.`isHidden`=0
			ORDER BY `".DB_groups."`.`pavadinimas`");
			while ($row = mysqli_fetch_assoc($result))
				$groups[$row['ID']] = $row['pavadinimas'];
		}
	}
	return $groups;
}

function hasGroup($group_id) {/*isGroupAllowed*/
	static $groups = null;
	if($groups === null) {
		$groups = getAllowedGroups();
	}
	if(ADMIN || BUHALT) {
		return true;
	} else {
		return isset($groups[$group_id]);
	}
}


function getAllEmployees($id = '') {
	static $employees = null;
	if($employees === null) {
		$employees = array(0 => '');
		$result = db_query("SELECT `ID`, `vardas`, `pavarde`, `isDeleted` FROM `".DB_employees."` ORDER BY `vardas`, `pavarde`");
		while ($row = mysqli_fetch_assoc($result))
			$employees[$row['ID']] = filterText($row['vardas'].' '.$row['pavarde']).($row['isDeleted'] ? ' (ištrintas)' : '');
	}
	if($id == '')
		return $employees;
	else
		return (isset($employees[$id]) ? $employees[$id] : $id);
}

function getValidEmployees($id = '') {
	static $employees = null;
	if($employees === null) {
		//$employees = array(0 => '');
		$result = db_query("SELECT `ID`, `vardas`, `pavarde` FROM `".DB_employees."` WHERE `isDeleted`=0 ORDER BY `vardas`, `pavarde`");
		while ($row = mysqli_fetch_assoc($result))
			$employees[$row['ID']] = filterText($row['vardas'].' '.$row['pavarde']);
	}
	if($id == '')
		return $employees;
	else
		return (isset($employees[$id]) ? $employees[$id] : $id);
}

function getValidEmployeesOrdered_db_query() {
	return db_query("SELECT * FROM `".DB_employees."` WHERE `isDeleted`=0 ORDER BY `vardas`, `pavarde`");
}

function getName($first, $last, $date = CURRENT_DATE) {
	global $config;
	if(getConfig('vaiku_saraso_eiluteje_pirmiau_rodyti_(vardas_ar_pavarde)', $date) == 'vardas')
		return $first.' '.$last;
	return $last.' '.$first;
}
function orderName($prefix = '', $date = CURRENT_DATE, $first = 'vardas', $last = 'pavarde') {
	global $config;
	if(getConfig('vaiku_sarasa_pradeti_rikiuoti_nuo_(vardas_ar_pavarde)', $date) == 'vardas')
		return (!empty($prefix) ? "`".$prefix."`." : '')."`".$first."`, ".(!empty($prefix) ? "`".$prefix."`." : '')."`".$last."`";
	return (!empty($prefix) ? "`".$prefix."`." : '')."`".$last."`, ".(!empty($prefix) ? "`".$prefix."`." : '')."`".$first."`";
}
/*function isAllowedKidId($id) {
	static $kids = null;
	if(ADMIN)
		return true;
	else {
		if($kids === null) {
			$kids = [];
			$result = db_query("SELECT cr.* FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<=".CURRENT_DATE." GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from` WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 AND cr.`grupes_id`=".(int)GROUP_ID." ORDER BY ".orderName('cr'));
			while ($row = mysqli_fetch_assoc($result))
				$kids[$row['parent_kid_id']] = '';
		}
		return isset($kids[$id]);
	}
}*/

function getAllKidName($id = '') {
	static $kids = null;
	if($kids === null) {
		$kids = [];
		$all_kids_newest_valid_info = db_query("SELECT cr.* FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `isDeleted`=0 AND `archyvas`=0 GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from` WHERE cr.`isDeleted`=0 ORDER BY ".orderName('cr'));
		while($row = mysqli_fetch_assoc($all_kids_newest_valid_info))
			$kids[$row['parent_kid_id']] = filterText(getName($row['vardas'], $row['pavarde']));
	}
	if($id == '')
		return $kids;
	else
		return $kids[$id];
}
function getKidName($id = '') {
	static $kids = null;
	if($kids === null) {
		$kids = [];
		$all_kids_newest_valid_info = db_query("SELECT cr.* FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `isDeleted`=0 AND `archyvas`=0 GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from` WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 ORDER BY ".orderName('cr'));
		while($row = mysqli_fetch_assoc($all_kids_newest_valid_info))
			$kids[$row['parent_kid_id']] = filterText(getName($row['vardas'], $row['pavarde']));
	}
	if($id == '')
		return $kids;
	else
		return $kids[$id];
}
function getKid($id, $date = CURRENT_DATE) {
	static $kid = null;
	if($kid === null) {
		$kid = [];
		$result = db_query("SELECT * FROM `".DB_children."` WHERE `isDeleted`=0 ORDER BY `valid_from` DESC");// `valid_from`<='".CURRENT_DATE."' AND
		while ($row = mysqli_fetch_assoc($result))
			$kid[$row['parent_kid_id']][$row['valid_from']] = $row;
	}
	foreach($kid[$id] as $cdate => $value) {
		if($cdate <= $date) {
			return $value;
		}
	}
	//loga("Not found kid id $id");
	
	return false;
}
//SQL string services to get data from SQL DB

function getDbQuerySpeechGroups() {
	global $_SESSION;
	return db_query("SELECT * FROM `".DB_speech_groups."` WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `isArchived`=0 ORDER BY `title`");
}

//Turėtų būti vykdoma ir naudojama tik darbuotojams, bet ne tėvams:
if(ADMIN)
	//"SELECT * FROM `".DB_children."` WHERE `isDeleted`=0 AND `archyvas`=0"
	$get_kids_sql = "SELECT cr.* FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<=CURDATE() GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from` WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 ORDER BY ".orderName('cr');//cr.`vardas` ASC, cr.`pavarde` ASC"
else
	//"SELECT * FROM `".DB_children."` WHERE `isDeleted`=0 AND `archyvas`=0 AND `grupes_id`=".GROUP_ID
	$get_kids_sql = "SELECT cr.* FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<=CURDATE() GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from` WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 AND cr.`grupes_id`=".GROUP_ID." ORDER BY ".orderName('cr');//cr.`vardas` ASC, cr.`pavarde` ASC

function getValidSelectedKidName($id = '') {
	global $get_kids_sql;
	static $kids = null;
	if($kids === null) {
		$kids = [];
		$result = db_query($get_kids_sql);
		while($row = mysqli_fetch_assoc($result))
			$kids[$row['parent_kid_id']] = filterText(getName($row['vardas'], $row['pavarde']));
	}
	if($id == '')
		return $kids;
	else
		return $kids[$id];
}

if(ADMIN)
	//"SELECT * FROM `".DB_children."` WHERE `isDeleted`=0 AND `archyvas`=0"
	$get_kids_sql_with_age = "SELECT cr.*, TIMESTAMPDIFF(YEAR, cr.`gim_data`, CURDATE()) AS age FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<=CURDATE() GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from` WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 ORDER BY ".orderName('cr');//cr.`vardas` ASC, cr.`pavarde` ASC"
else
	//"SELECT * FROM `".DB_children."` WHERE `isDeleted`=0 AND `archyvas`=0 AND `grupes_id`=".GROUP_ID
	$get_kids_sql_with_age = "SELECT cr.*, TIMESTAMPDIFF(YEAR, cr.`gim_data`, CURDATE()) AS age FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<=CURDATE() GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from` WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 AND cr.`grupes_id`=".GROUP_ID." ORDER BY ".orderName('cr');//cr.`vardas` ASC, cr.`pavarde` ASC


//Only attendance justification
if(AUKLETOJA)
	$get_archived_kids_sql = "SELECT cr.* FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<=CURDATE() GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from` WHERE cr.`isDeleted`=0 AND cr.`archyvas`=1  ORDER BY ".orderName('cr');//AND cr.`grupes_id`=".GROUP_ID."
if(AUKLETOJA)
	$get_all_selected_group_kids_sql = "SELECT * FROM `".DB_children."` WHERE `isDeleted`=0 AND `archyvas`=0 AND `grupes_id`=".GROUP_ID." GROUP BY `parent_kid_id` ORDER BY ".orderName();



function getKidsSql($group_id = '', $valid_from = null/*'CURDATE()'*/) {
	if(is_null($valid_from)) //!= 'CURDATE()'
		$valid_from = 'CURDATE()';//"'".date('Y-m-d')."'" - optimization
	else
		$valid_from = "'".db_fix($valid_from)."'";
	if($group_id == '')
		return "SELECT cr.* FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<=".$valid_from." GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from` WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 ORDER BY ".orderName('cr', ($valid_from == null ? CURRENT_DATE : $valid_from));//cr.`vardas` ASC, cr.`pavarde` ASC
	return "SELECT cr.* FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<=".$valid_from." GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from` WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 AND cr.`grupes_id`=".(int)$group_id." ORDER BY ".orderName('cr', ($valid_from == null ? CURRENT_DATE : $valid_from));//cr.`vardas` ASC, cr.`pavarde` ASC
}//Kaip dėl CURDATE.
