<?php
if(!defined('DARBUOT') || ADMIN) exit();
?>
<h1>Lankomumo žymėjimas</h1>
<div id="content">
<?php
if(isset($_POST['data']) && !preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $_POST['data'])) logdie(msgBox('ERROR', "Blogas datos formatas: Jis turi būti „".date('Y-m-d')."“. ".back()));

$not_allowed_to_change = false;

if(isset($_GET['delete']) || (isset($_GET['zymeti']) && isset($_POST['del']))) {
	$mark_date = (isset($_GET['zymeti']) ? $_GET['zymeti'] : $_GET['delete']);
	$currently_locked = db_query("SELECT * FROM `1lock_kids_attendance_food` WHERE `kindergarten_id`=".DB_ID." AND `date`='".db_fix(substr($mark_date, 0, 7))."-01'  AND `group_id`=".(int)GROUP_ID);
	if(!mysqli_num_rows($currently_locked)) {
		$result = db_query("SELECT * FROM `1attendance_mark_lock` WHERE `kindergarten_id`=".DB_ID." AND `date`='".db_fix($mark_date)."'");
		if(!mysqli_num_rows($result)) {
			//Kids: Valid, not archived
			$result = db_query("SELECT cr.`parent_kid_id` FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<='".db_fix($mark_date)."' GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
				WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 AND cr.`grupes_id`=".GROUP_ID."
				ORDER BY cr.`vardas`, cr.`pavarde`");
			$kids = array();
			while($row = mysqli_fetch_assoc($result))
				if(isset($_GET['delete']) || (isset($_GET['zymeti']) && isset($_POST['del']) && array_search($row['parent_kid_id'], $_POST['vaiko_id']) === FALSE))
					$kids[] = $row['parent_kid_id'];
			if(!empty($kids))
				db_query("DELETE FROM `".DB_attendance."` WHERE `data`='".db_fix($mark_date)."' AND `vaiko_id` IN (".implode(',', $kids).")");
			if(!empty($_POST['vaiko_id']))
				db_query("DELETE FROM `".DB_attendance."` WHERE `data`='".db_fix($mark_date)."' AND `kid_group_id`=".GROUP_ID.
					(isset($_GET['zymeti']) && isset($_POST['del']) ? ' AND `vaiko_id` NOT IN ('.implode(',', $_POST['vaiko_id']).')' : ''));
			msgBox('OK', "Lankomumas ištrintas!");
		} else {
			$not_allowed_to_change = true;
			msgBox('ERROR', "Dietistės-maitinimo organizavimo specialistės užrakinto lankomumo ištrinti neleidžiama. Kreipkitės į ją dėl atrakinimo.");
		}
	} else {
		msgBox('ERROR', "Užrakintas šio mėnesio lankomumas. Kreipkitės į vadovą dėl atrakinimo.");
	}
}
/*if (isset($_GET['update'])) {
    $data = explode('-', $_POST['data']);
    if(!mysqli_query($db_link, "UPDATE `".DB_working_d."` SET `data`='".db_fix($_POST['data'])."', `metai`=".(int)$data[0].", `menuo`=".(int)$data[1].", `diena`=".(int)$data[2]." WHERE `ID`=".(int)$_GET['update'])) {
        logdie('Neteisinga užklausa: '. mysqli_error($db_link));
    } else {
    	if(!mysqli_query($db_link, "UPDATE `".DB_attendance."` SET `data`='".db_fix($_POST['data'])."' WHERE `veikimo_d_id`=".(int)$_GET['update']))
			logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
    	msgBox('OK', "Informacija išsaugota!");
    }
}*/
/*if (isset($_GET['save'])) {
    $result = db_query("SELECT * FROM `".DB_working_d."` WHERE `data`='".db_fix($_POST['data'])."'");
    if(mysqli_num_rows($result)) {
		msgBox('ERROR', "Ši diena jau užregistruota sistemoje.");//Norint šią dieną naujai sukurti, reikėtų pirmiau pakeisti jau esamą šią dieną sistemoje į kitą dieną ir tik tada bandyti iš naujo užregistruoti šią dieną.
    } else {
        $data = explode('-', $_POST['data']);
	    if(!mysqli_query($db_link, "INSERT INTO `".DB_working_d."` SET `data`='".db_fix($_POST['data'])."', `metai`=".(int)$data[0].", `menuo`=".(int)$data[1].", `diena`=".(int)$data[2].", `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'")) {
		    logdie('Neteisinga užklausa: '. mysqli_error($db_link));
	    } else {
		    msgBox('OK', "Informacija išsaugota!");
		    $work_day = mysqli_insert_id($db_link);
			?>
			<script>window.location.replace("?zymeti=<?php echo $work_day; ?>#attendance-form");</script>
			<noscript><meta http-equiv="refresh" content="0;URL='?zymeti=<?php echo $work_day; ?>#attendance-form'"></noscript>
			<?php
			//By default does't work in firefox (because of security config)
			//Reason: http://stackoverflow.com/questions/503093/how-can-i-make-a-redirect-page-in-jquery-javascript
	    }
	}
}*/

if(isset($_POST['save']) && isset($_GET['zymeti'])) {
	$currently_locked = db_query("SELECT * FROM `1lock_kids_attendance_food` WHERE `kindergarten_id`=".DB_ID." AND `date`='".db_fix(substr($_GET['zymeti'], 0, 7))."-01'  AND `group_id`=".(int)GROUP_ID);
	if(!mysqli_num_rows($currently_locked)) {
		$result = db_query("SELECT * FROM `1attendance_mark_lock` WHERE `kindergarten_id`=".DB_ID." AND `date`='".db_fix($_GET['zymeti'])."'");
		if(!mysqli_num_rows($result)) {
			//for($i=0; $i < (int)$_POST['kiek']; $i++) {
			$vaikas_counter = 0;
			//BULK INSERT UPDATE http://stackoverflow.com/questions/6030071/mysql-table-insert-if-not-exist-otherwise-update
			//http://stackoverflow.com/questions/11664684/how-to-bulk-update-mysql-data-with-one-query
			if(isset($_POST['vaiko_id'])) {
				foreach($_POST['vaiko_id'] as $i => &$child_id) {
					$child_id = (int)$child_id;
				}
				unset($child_id);//Warning http://php.net/manual/en/control-structures.foreach.php
		
				$kids = array();
				$result = db_query("SELECT `vaiko_id` FROM `".DB_attendance."` WHERE `data`='".db_fix($_GET['zymeti'])."' AND `vaiko_id` IN (".implode(',', $_POST['vaiko_id']).")");
				while($row = mysqli_fetch_assoc($result))
					$kids[$row['vaiko_id']] = '';
				$sql_insert = array();
				foreach($_POST['vaiko_id'] as $i => $child_id) {
					//$result = db_query("SELECT `data` FROM `".DB_attendance."` WHERE `data`='".db_fix($_GET['zymeti'])."' AND `vaiko_id`=".(int)$child_id);
					//if(mysqli_num_rows($result)) {
					if(isset($kids[$child_id])) {
						db_query("UPDATE `".DB_attendance."` SET
						`kid_group_id`=".GROUP_ID.", `yra`=".(isset($_POST['yra'][$i]) ? (int)$_POST['yra'][$i] : 0).",
						".(KaunoVaikystesLobiai ? "`additional_cost_per_day`='".(float)str_replace(',', '.', $_POST['additional_cost_per_day'][$i])."', " : '')."
						`additional_mark_id`='".(isset($_POST['additional_mark'][$i]) ? (int)$_POST['additional_mark'][$i] : 0)."',
						`updated`=CURRENT_TIMESTAMP, `updatedByUserId`=".USER_ID.", `updatedByEmployeeId`=".DARB_ID.",
						`updatedCounter`=`updatedCounter`+1
						WHERE `data`='".db_fix($_GET['zymeti'])."' AND `vaiko_id`=".(int)$child_id);
					} else {//INSERT ... ON DUPLICATE KEY UPDATE   OR select WHERE data`='".db_fix($_GET['zymeti'])."' AND `vaiko_id` IN () THEN UPDATE/insert
						/*db_query("INSERT INTO `".DB_attendance."` SET 
						`kid_group_id`=".GROUP_ID.", 
						`yra`=".(int)(isset($_POST['yra'][$i]) ? $_POST['yra'][$i] : 0).", `data`='".db_fix($_GET['zymeti'])."',
						`vaiko_id`=".(int)$child_id.", `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'
						".(KaunoVaikystesLobiai ? ", `additional_cost_per_day`='".(float)str_replace(',', '.', $_POST['additional_cost_per_day'][$i])."'" : '')
						);*/
						$sql_insert[] = "(".GROUP_ID.", ".(int)(isset($_POST['yra'][$i]) ? $_POST['yra'][$i] : 0).", '".db_fix($_GET['zymeti'])."',
						".(int)$child_id.", '"./*createdByUserId*/USER_ID."', '"./*createdByEmployeeId*/DARB_ID."',
						".(KaunoVaikystesLobiai ? "'"./*additional_cost_per_day*/(float)str_replace(',', '.', $_POST['additional_cost_per_day'][$i])."', " : '')."
						".(isset($_POST['additional_mark'][$i]) ? (int)$_POST['additional_mark'][$i] : 0).")";
					}
					$vaikas_counter++;
					/*
					$result = db_query("SELECT * FROM `".DB_attendance."` WHERE `veikimo_d_id`=".(int)$_GET['zymeti']." AND `vaiko_id`=".(int)$child_id);
					if(mysqli_num_rows($result) > 0) {
					                //`priezastis`='".db_fix($_POST['priezastis'][$i])."',
						db_query("UPDATE `".DB_attendance."` SET `yra`=".(isset($_POST['yra'][$i]) ? (int)$_POST['yra'][$i] : 0)." WHERE `veikimo_d_id`=".(int)$_GET['zymeti']." AND `vaiko_id`=".(int)$child_id));//`pastabos`='".db_fix($_POST['pastabos'][$i])."', 
						$vaikas_counter++; //echo "Vaiko lankomumo informacija sėkmingai atnaujinta!<br>";
					} else {
						$result = db_query("SELECT * FROM `".DB_working_d."` WHERE `ID`=".(int)$_GET['zymeti']);
						$row = mysqli_fetch_assoc($result);
						$data = $row['data'];
						//Needed to execute: update r_lankomumas a left join r_veikimo_d b on a.veikimo_d_id = b.ID set a.`data` = b.`data`
					                //`priezastis`='".db_fix($_POST['priezastis'][$i])."'
						db_query("INSERT INTO `".DB_attendance."` SET `data`='$data', 
						`yra`=".(int)(isset($_POST['yra'][$i]) ? $_POST['yra'][$i] : 0).", `veikimo_d_id`=".(int)$_GET['zymeti'].", 
						`vaiko_id`=".(int)$child_id.", `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'"));// `pastabos`='".db_fix($_POST['pastabos'][$i])."',
						$vaikas_counter++; //echo "Vaiko lankomumo informacija sėkmingai įtraukta į duombazę!<br>";
					}*/

					/*mysqli_query($db_link, "DELETE FROM `".DB_achievements."` WHERE `veikimo_d_id`=".(int)$_GET['zymeti']." AND
						`vaiko_id`=".(int)$child_id) or logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
					if(isset($_POST['tipas'][$i])) for($k=0; $k<count($_POST['tipas'][$i]); $k++) {
						mysqli_query($db_link, "INSERT INTO `".DB_achievements."` SET `tipas`='".(int)$_POST['tipas'][$i][$k]."', 
						`pavadinimas`='".db_fix($_POST['pavadinimas'][$i][$k])."', `veikimo_d_id`=".(int)$_GET['zymeti'].", 
						`vaiko_id`=".(int)$child_id.", `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'") or logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
					}*/
				}
				if(!empty($sql_insert))
					db_query("INSERT INTO `".DB_attendance."` (`kid_group_id`, `yra`, `data`, `vaiko_id`, `createdByUserId`, `createdByEmployeeId`".(KaunoVaikystesLobiai ? ", `additional_cost_per_day`" : '').", `additional_mark_id`) VALUES ".implode(',', $sql_insert));
				msgbox('OK', "Grupės (". $vaikas_counter . " vaikų) lankomumas sėkmingai išsaugotas!");
			}
		} else {
			$not_allowed_to_change = true;
			msgBox('ERROR', "Dietistės-maitinimo organizavimo specialistės užrakinto lankomumo keisti nebegalima. Kreipkitės į ją dėl atrakinimo.");
		}
	} else {
		msgBox('ERROR', "Užrakintas šio mėnesio lankomumas. Kreipkitės į vadovą dėl atrakinimo.");
	}
}

/*if(isset($_GET ['delete'])) {
	if(!mysqli_query($db_link, "DELETE FROM `".DB_working_d."` WHERE `ID`=".(int)$_GET['delete'])) {
		logdie('Neteisinga užklausa: '. mysqli_error($db_link));
	} else echo "Informacija ištrinta!"; ////TODO:patikrinti
}*/

if(isset($_GET['zymeti'])) {
	?><a href="?<?=(isset($_GET['data']) ? 'data=' . filterText($_GET['data']).'&amp;' : '')?>new_date#attendance-day-form" id="new-day" class="no-print fast-action fast-action-add">Nauja lankomumo diena</a>
	<?php
}
/*
<a href="#" id="remove-attendance" class="no-print fast-action">Lankomumo trynimas vaikui</a>
<div id="remove-attendance-form" style="display: none">
	<a href="?<?=(isset($_GET['data']) ? 'data=' . filterText($_GET['data']).'&amp;' : '')?>remove#attendance-remove" id="new-day" class="no-print fast-action fast-action-add">Ištrynimas lankomumo dienos</a>
</div>
<script>
$( "#remove-attendance" ).click(function() {
	$( "#remove-attendance-form" ).toggle();//'slow'
});
</script>
*/ ?>


<?php
/*if( isset($_GET['edit']) || isset($_GET['new_date']) ) {
	if( isset($_GET['edit']) ) {
		$result = db_query("SELECT * FROM `".DB_working_d."` WHERE `ID`=".(int)$_GET['edit']);
		$row = mysqli_fetch_assoc($result);
		$data = $row['data'];
	}
	?>
	<fieldset id="attendance-day-form">
	<legend><?=(isset($_GET['edit']) ? "Darbo dienos redagavimas (įvedus netinkamą darbo dieną)":"Naujos darbo dienos lankomumas")?></legend>
	<form action="?<?=(isset($_GET['edit']) ? 'update='.(int)$_GET['edit'] : 'save')?>" method="post" onsubmit="return validateDate(this)">
		<!-- <?=(isset($_GET['edit'])  ? 'Paredagavus darbo dieną jeigu joje buvo įvestas lankomumas jis automatiškai persikelia į pakeistą dieną.':'')?> -->
		<p><span class="abbr" title="Datos įprastas formatas <?=date('Y-m-d')?>"><!-- Data: --></span> <input class="datepicker" type="text" name="data" value="<?=(isset($_GET['edit']) ? $data : date('Y-m-d'))?>">  <input type="submit" value="Išsaugoti dieną" class="submit"></p>
	</form>
	</fieldset>
	<?php
}*/
//if( isset($_GET['new_date']) ) {
if(!isset($_GET['zymeti'])) {
	?>
	<fieldset id="attendance-day-form">
	<legend><?=(isset($_GET['edit']) ? "Darbo dienos redagavimas (įvedus netinkamą darbo dieną)":"Naujos darbo dienos lankomumas")?></legend>
	<form method="get" onsubmit="return validateDate(this)">
		<!-- <?=(isset($_GET['edit'])  ? 'Paredagavus darbo dieną jeigu joje buvo įvestas lankomumas jis automatiškai persikelia į pakeistą dieną.':'')?> -->
		<p>Šiandien - <?=date('Y-m-d')?> (<?=date('N').' '.mb_lcfirst($dienos[date('N')])?>)</p>
		<p><span class="abbr" title="Datos įprastas formatas <?=date('Y-m-d')?>"><!-- Data: --></span> <input class="datepicker abbr" type="text" name="zymeti" value="<?=(/*isset($_GET['edit']) ? $data :*/ date('Y-m-d'))?>" data-date="<?=date('Y-m-d')?>" id="suggested-date" title="Žalia pariebinta data - pasiūlyta šiandiena. Kita nurodoma spustelėjus pele."> <input style="margin:0" type="submit" value="Pereiti į lankomumo žymėjimą" class="submit"></p>
	</form>
	</fieldset>
	<?php
}
//else
{


	if(isset($_GET['zymeti'])) {
		$mark_date = $_GET['zymeti'];
		?>
		<script type="text/javascript" src="/libs/jquery.checkboxes.min.js"></script>
		<script type="text/javascript">
		$(function($) {
			$('#grupes_lankomumas').checkboxes('range', true);
		});
		</script>
		<fieldset id="attendance-form">
		<legend>Žurnalo redagavimas: <?=($mark_date == date('Y-m-d') ? 'šiandien ' : '').filterText($mark_date)?> (<?=$dienos[date('N', strtotime($mark_date))]?>) <?=$_SESSION['GROUPS'][GROUP_ID]?></legend>
		<form method="post" action="?zymeti=<?=filterText($mark_date)?><?php /* remember filter parameters: */ if(isset($_GET['data'])) echo '&amp;data='.filterText($_GET['data']); ?>" class="not-saved-reminder">
			<?php //if($mark_date == '2014-10-03') echo 'Svarbu, kad rodytų tik tuos vaikus kurie nėra išregistruoti.';//Žymėkite kai žymėjimo sąrašo vaikai yra vis dar registruoti darželyje ?>
			<a href="#attendance-form" class="attendance-checkboxes" data-toggle="checkboxes" data-action="check">Žymėti </a><a href="#attendance-form" class="attendance-checkboxes" data-toggle="checkboxes" data-action="check" style="font-weight: bold;">yra visi vaikai</a>
			<?php
			//Buvo „Pažymėti visus vaikus, kad yra“. „Žymėti Yra visi“ atrodo, kad kažkuo pažymėti, Žymėjimas
			$marks = [];
			$result = db_query("SELECT * FROM `".DB_attendance_marks."` WHERE `kindergarten_id`=".DB_ID." AND `isDeleted`=0 ORDER BY `abbr`");
			while($row = mysqli_fetch_assoc($result))
				$marks[] = $row;//abbr, title
			?>
			<!--<a href="#attendance-form" data-toggle="checkboxes" data-action="uncheck">Pažymėti, kad nė vieno nėra</a> -->
			<table id="grupes_lankomumas">
			<tr>
				<th title="Imami iš meniu punkto „Vaikai“ pagal duomenų galiojimą"><span class="abbr">Vaikas</span></th>
				<th>Yra<!-- Ar yra? --></th>
				<?php
				if(!empty($marks)) echo '<th title="Darželio susikurtas papildomas žymėjimas"><span class="abbr">Papildomas žymėjimas</span></th>';
				if(KaunoVaikystesLobiai) echo '<th>'.getConfig('papildomos_dienos_kainos,_kuri_ivedama_zymint_lankomuma,_pavadinimas', substr($mark_date, 0, 7).'-01').'</th>'; ?>
				<!-- <th>Pastabos</th>
				<th>Pasiekimai<!--Pažangumas-></th> -->
			<?php
			$i = 0;
			//Kids: Valid, not archived
			$result = db_query("SELECT cr.* FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<='".db_fix($mark_date)."' GROUP BY `parent_kid_id`) fi ON (cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`)
			WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 AND cr.`grupes_id`=".GROUP_ID."
			ORDER BY ".(/*KaunoVaivoryksteD ?	 'cr.`parent_kid_id`,'.orderName('cr', $mark_date) :*/ orderName('cr', $mark_date)));//(int)$row['grup_id']//cr.`vardas`, cr.`pavarde`
			//"SELECT * FROM `".DB_children."` WHERE `grupes_id`=".GROUP_ID." AND `isDeleted`=0"
			//$sk = mysqli_num_rows($result);//TODO: Jeigu perdarysiu JS
			$vaiko_id = array();
			$kids = array();
			while($row = mysqli_fetch_assoc($result)) {
				$vaiko_id[] = $row['parent_kid_id'];
				$kids[$row['parent_kid_id']] = getName($row['vardas'], $row['pavarde'], $mark_date);
			}
			$kid_attendance = array();
			if(!empty($vaiko_id)) {
				$sel = db_query("SELECT * FROM `".DB_attendance."` WHERE `vaiko_id` IN (".implode(',', $vaiko_id).") AND `data`='".db_fix($mark_date)."'");
				while($lankomumas = mysqli_fetch_assoc($sel))
					$kid_attendance[$lankomumas['vaiko_id']] = $lankomumas;
			}
			
			
			foreach($kids as $kid_id => $name) {
				//$sel = db_query("SELECT * FROM `".DB_attendance."` WHERE `vaiko_id`=".(int)$row['parent_kid_id']." AND `veikimo_d_id`=".(int)$_GET['zymeti']);
				echo "\t<tr><td>".filterText($name)." <input type=\"hidden\" name=\"vaiko_id[".$i."]\" value=\"".$kid_id."\"></td>
					<td><input type=\"checkbox\" name=\"yra[".$i."]\" value=\"1\"".(isset($kid_attendance[$kid_id]) && $kid_attendance[$kid_id]['yra'] && !$not_allowed_to_change || $not_allowed_to_change && isset($_POST['yra'][$i])  ? ' checked="checked"' : '')."></td>";
				if(!empty($marks)) {
					echo "<td>";
					foreach($marks as $mark) {
						echo ' <label class="additional-mark"><input type="radio" name="additional_mark['.$i.']" value="'.$mark['ID'].'"'.(isset($kid_attendance[$kid_id]) && $kid_attendance[$kid_id]['additional_mark_id'] == $mark['ID'] && !$not_allowed_to_change || $not_allowed_to_change && isset($_POST['additional_mark'][$i]) && $_POST['additional_mark'][$i] == $mark['ID']  ? ' checked="checked"' : '').'><span class="abbr" title="'.$mark['title'].'">'.$mark['abbr'].'</span></label>';
					}
					echo "</td>";
					//http://stackoverflow.com/questions/4957207/how-to-check-uncheck-radio-button-on-click
				}
				
				if(KaunoVaikystesLobiai)
					echo "<td><input type=\"text\" name=\"additional_cost_per_day[".$i."]\" value=\"".(isset($kid_attendance[$kid_id])/*mysqli_num_rows($sel) > 0*/ ? price($kid_attendance[$kid_id]['additional_cost_per_day']) : '')."\" style=\"width: 150px;\"></td>";
					//<!-- Priežastis: <input type=\"text\" name=\"priezastis[".$i."]\" value=\"".(mysqli_num_rows($sel) > 0 ? filterText($lankomumas['priezastis']) : '')."\"> -->";
					/*
					<td><textarea name=\"pastabos[".$i."]\" class=\"pastabos\">".(mysqli_num_rows($sel) > 0 ? filterText($lankomumas['pastabos']) : '')."</textarea></td>
					<td>
					";		    
					//Pasiekė, siekia
					*/
				echo "</tr>\n";
				++$i;
			}
			if($i != 0)
				echo '<tr><td colspan="5" id="counter"></td></tr>';
			
			$show_delete_remaining = false;
			if(!empty($vaiko_id)) {
				$res = db_query("SELECT * FROM `".DB_attendance."` JOIN `".DB_children."` ON `vaiko_id`=`parent_kid_id`
					WHERE `data`='".db_fix($mark_date)."' AND `kid_group_id`=".GROUP_ID.' AND `vaiko_id` NOT IN ('.implode(',', $vaiko_id).')
					GROUP BY `parent_kid_id`');
				if(mysqli_num_rows($res) > 0) {
					$show_delete_remaining = true;
					/*Išsaugotas vaikų lankomumas, kuriems buvo pakeisti duomenys:
					Išsaugoti likučiai (dėl duomenų meniu punkte „Vaikai“ pakeitimų t. y. įsigaliojimo datos, išregistravimo, grupės ar/ir kt.):
					*/
					echo '<tr><td colspan="5">Likęs vaikų lankomumas po pakeitimų meniu punkte „Vaikai“ (pvz., įsigaliojimo datos, išregistravimo, grupės ar/ir kt.):<br>';
					while($row = mysqli_fetch_assoc($res)) {
						echo filterText($row['vardas'].' '.$row['pavarde']).($row['grupes_id'] != $row['kid_group_id'] ? ' <span class="abbr" title="Greičiausiai šio vaiko kitos grupės auklėtoja turėtų iš naujo išsaugoti lankomumą, kad pereitų šis lankomumo pažymėjimas į naują grupę">šiam vaikui pakeista grupė</span>' : '').'<br>';
					}
					echo '</td></tr>';
				}
			}
		
			if($i == 0) {
				echo '<tr><td colspan="2">Nėra vaikų šiai dienai oficialiai registruotų darželyje.<br>Vaikai imami iš meniu punkto „Vaikai“ pagal jų duomenų galiojimą.<br>Jei jau įvedėte vaikų, o jų nerodo, nors turėtų rodyti, tai pataisykite vaikuose jų duomenų galiojimą atsidarę to vaiko duomenis ir išsaugokite.</td></tr>';
				//Edge case when you cannot delete remaining, but displied in the report (tabelis), kids list should or may be taken from tabelis.
			}
			?>
			</table>
			<?php
			if($i != 0) {
				if(!empty($marks))
					foreach($marks as $mark)
						echo '<strong>'.$mark['abbr'].'</strong> - '.$mark['title'].'<br>';//<dl><dt>Pav</dt><dd>aprašymas</dd></dl>
			?>
			<p>
				<input type="submit" value="Išsaugoti lankomumą" class="submit">
				<?php if($show_delete_remaining) { ?> <input type="submit" name="del" value="Ištrinti likučius ir išsaugoti lankomumą" style="margin-bottom: 30px;"><?php } ?><!-- liekanas -->
				<input type="hidden" name="save">
			</p>
			<?php } ?>
		</form>
		</fieldset>
		<?php
	}
	?>


	
	<?php
	$result = db_query("SELECT YEAR(`data`) AS `metai`, MONTH(`data`) AS `menuo` FROM `".DB_attendance."` WHERE `kid_group_id`=".GROUP_ID." GROUP BY YEAR(`data`) DESC, MONTH(`data`) DESC");
	if(mysqli_num_rows($result)) {
		?>
		<h2>Žymėtos lankomumo dienos</h2>
		<form method="get">
			<div class="sel" style="float: left; margin-right: 5px; line-height: 33px;">
			<select name="data">
				<?php
				if(isset($_POST['data'])) {
					list($year, $month) = explode('-', $_POST['data']);//, $day
					$year = (int) $year;
					$month = (int) $month;
				}
				if(isset($_GET['data'])) {
					list($year, $month) = explode('-', $_GET['data']);//, $day
					$year = (int) $year;
					$month = (int) $month;
				}
				while ($row = mysqli_fetch_assoc($result))
					echo "<option value=\"".$row['metai']."-".$row['menuo']."\"".((isset($_GET['data']) || isset($_POST['data'])) && $row['metai'] == $year && $row['menuo'] == $month ? ' selected="selected" class="selected">'.$selectedMark : '>').$row['metai']."-".men($row['menuo'])."</option>";
				?>
			</select></div>
			<input type="submit" value="Filtruoti" class="filter">
		</form>
		<?php
	

		if(isset($_GET['data'])) {
			list($year, $month) = explode('-', $_GET['data']);
			$result = db_query("SELECT * FROM `".DB_attendance."` WHERE YEAR(`data`)=".(int)$year." AND MONTH(`data`)=".(int)$month." AND `kid_group_id`=".GROUP_ID." GROUP BY `data` ORDER BY `data` DESC");
		} else
			$result = db_query("SELECT * FROM `".DB_attendance."` WHERE `kid_group_id`=".GROUP_ID." GROUP BY `data` ORDER BY `data` DESC LIMIT 0,30");
		if(mysqli_num_rows($result) > 0) {
			if(!isset($_GET['data']))
				echo '<em>Sąraše dabar rodoma 30 naujausių dienų.</em>';
			?>
			<table border="1">
			<tr>
				<th>Data</th>
				<th>Savaitės diena</th>
				<th>Veiksmai</th>
			</tr>
			<?php
			while ($row = mysqli_fetch_assoc($result)) {
				//if(isset($_GET['zymeti']) && $_GET['zymeti'] == $row['ID'])
				//	$data = $row['data'];
				echo "\t<tr".(isset($_GET['zymeti']) && $_GET['zymeti'] == $row['data'] ? ' class="opened-row"' : '').">
				<td>".$row['data'];
				/*if($row['data'] == date('Y-m-d')) {
					$res = db_query("SELECT cr.* FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<='".db_fix($row['data'])."' GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
					WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 AND cr.`grupes_id`=".GROUP_ID."
					ORDER BY cr.`vardas`, cr.`pavarde`");
					$row_c = mysqli_fetch_assoc($res);
					$sel = db_query("SELECT * FROM `".DB_attendance."` WHERE `vaiko_id`=".(int)$row_c['parent_kid_id']." AND `veikimo_d_id`=".(int)$row['ID']);
					echo ' Šiandien '.(mysqli_num_rows($sel) ? ' žymėtas' : 'nežymėtas');
				}*/
				//<a href=\"?edit=".$row['ID'].(isset($_GET['data']) ? "&amp;data=".filterText($_GET['data']) : '')."#attendance_day_form\">Keisti darbo dienos datą</a>
				$week_day = date('N', strtotime($row['data']));
				echo "</td>
				<td>".$week_day.' '.mb_lcfirst($dienos[$week_day])."</td>
				<td><a href=\"?zymeti=".$row['data'].(isset($_GET['data']) ? "&amp;data=".filterText($_GET['data']) : '')."#attendance-form\">Žymėti lankomumą<!-- Lankomumas/žurnalas --></a>
					<a href=\"?delete=".$row['data']."\" onclick=\"return confirm('Ar tikrai norite ištrinti?')\">Trinti</a>
			
				</td>
			</tr>";
	
			}
			?>
			</table>
			<?php
		}
	}

}
?>
</div>
<script type="text/javascript" src="/workers/lankomumas.js?3"></script>
