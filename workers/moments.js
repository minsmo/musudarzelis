$(function() {
	$('#kids').checkboxes('range', true);
	$("#visibility").change(function() {
		change($(this).val());
	});
	
	function change(val) {
		if(val != 0)
			$("#kids-group").show();
		else
			$("#kids-group").hide();
		if(val == 2)
			$("#kids").show();
		else
			$("#kids").hide();
	}
	change($("#visibility").val());
});
