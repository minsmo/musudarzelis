<?php
	if (!empty($_GET['q'])) {
		if($_GET['q'] == 'vaikai') include 'vaikai.php';
		elseif($_GET['q'] == 'pateisinimai') include 'pateisinimai.php';
		elseif($_GET['q'] == 'zinutes') include 'messages.php';
		elseif($_GET['q'] == 'grupes') include 'grupes.php';
		elseif($_GET['q'] == 'planavimas') include 'planning.php';
		elseif($_GET['q'] == 'lankomumas') include 'lankomumas.php';
		elseif($_GET['q'] == 'vaiko_pazanga_ir_pasiekimai') include 'progress_and_achievements.php';
		elseif($_GET['q'] == 'vaiko_pasiekimai_pagal_nauja_aprasa') include 'new_achievements.php';
		elseif($_GET['q'] == 'archyvas') include 'archive.php';
		elseif($_GET['q'] == 'planavimo_formos') include 'forms.php';
		elseif($_GET['q'] == 'planavimo_formos_pildymas') include 'forms_fill.php';
		elseif($_GET['q'] == 'tabeliai') include 'tabeliai.php';//&& ADMIN
		elseif($_GET['q'] == 'tabeliai/rakinimas') include 'lock_kids_attendance_food.php';
		elseif($_GET['q'] == 'maitinimas') include 'feeding.php';//&& ADMIN
		elseif($_GET['q'] == 'priminimai') include 'reminders.php';
		elseif($_GET['q'] == 'pakeisti_slaptazodi') include 'change_password.php';
		elseif($_GET['q'] == 'dokumentai') include 'dokumentai_vaiko.php';
		elseif($_GET['q'] == 'documents_common') include 'documents_common.php';
		elseif($_GET['q'] == 'sveikatos_pastabos') include 'sveikatos_pastabos.php';
		elseif($_GET['q'] == 'nustatymai' && ADMIN) include 'nustatymai.php';
		elseif($_GET['q'] == 'grupiu_bureliai') include 'bureliai_schedule.php';
		elseif($_GET['q'] == 'bureliai') include 'bureliai.php';
		elseif($_GET['q'] == 'laiskai_grupems') include 'masinis_laiskas.php';
		elseif($_GET['q'] == 'darbuotojai' && ADMIN) include 'darbuotojai.php';
		elseif($_GET['q'] == 'prisijungimai' && ADMIN) include 'logins.php';
		elseif($_GET['q'] == 'kontaktai') include 'contacts.php';
		elseif($_GET['q'] == 'kvitai' && ADMIN) include 'kvitai.php';
		elseif($_GET['q'] == 'menu') include 'food_menu.php';
		elseif($_GET['q'] == 'spausdinti') include 'how_to_print.php';
		elseif($_GET['q'] == 'daily_rhythm') include 'daily_rhythm.php';
		elseif($_GET['q'] == 'activities') include 'activities.php';
		elseif($_GET['q'] == 'speech_therapist_kids') include 'speech_therapist/speech_therapist_kids.php';
		elseif($_GET['q'] == 'speech_kids') include 'speech_therapist/kids.php';
		elseif($_GET['q'] == 'speech_achievements') include 'speech_therapist/achievements_view.php';
		elseif($_GET['q'] == 'speech_groups') include 'speech_therapist/groups.php';
		elseif($_GET['q'] == 'speech_attendance') include 'speech_therapist/attendance.php';
		elseif($_GET['q'] == 'speech_attendance_report') include 'speech_therapist/attendance_report.php';
		elseif($_GET['q'] == 'speech_topics') include 'speech_therapist/topic.php';
		elseif($_GET['q'] == 'speech_other_activities') include 'speech_therapist/other_activities.php';
		elseif($_GET['q'] == 'remarks_and_suggestions') include 'speech_therapist/remarks_and_suggestions.php';
		elseif($_GET['q'] == 'speech_schedule') include 'speech_therapist/schedule.php';
		elseif($_GET['q'] == 'speech_diaries') include 'speech_therapist/diaries.php';
		elseif($_GET['q'] == 'worker_food') include 'worker_food_ate.php';
		elseif($_GET['q'] == 'notes') include 'notes.php';
		elseif($_GET['q'] == 'forms_fill_out') include 'docs_fill_out.php';
		elseif($_GET['q'] == 'forms_make') include 'docs_templates.php';
		elseif($_GET['q'] == 'goodies') include 'useful_stuff.php';
		elseif($_GET['q'] == 'tabelio_marks') include 'tabelio_marks.php';
		elseif($_GET['q'] == 'parents_menu') include 'parents.php';
		elseif($_GET['q'] == 'veikla') include 'moments.php';
		elseif($_GET['q'] == 'informal_diaries') include 'informal/diaries.php';
		elseif($_GET['q'] == 'informal_groups') include 'informal/groups.php';
		elseif($_GET['q'] == 'wishes') include 'suggestions.php';
		elseif($_GET['q'] == 'parents_logins_stats') include 'parents_login_statistics.php';
		elseif($_GET['q'] == 'attendance_lock') include 'attendance_lock.php';
		elseif($_GET['q'] == 'work_reminders') include 'remind_work.php';
		elseif($_GET['q'] == 'ikimokyklinis') include 'ikimokyklinis.php';
		elseif($_GET['q'] == 'virtuali_biblioteka' && DB_PREFIX == 'knsZelmen_') include 'library.php';
		
		elseif($_GET['q'] == 'page' && ADMIN) include 'psl.php';
		elseif($_GET['q'] == 'laiskai') include 'laiskai.php';
		elseif($_GET['q'] == 'mokejimai' && ADMIN) include 'mokejimai.php';
		
	}
    else include 'index_default.php';
