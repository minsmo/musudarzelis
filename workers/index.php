<?php if(!defined('DARBUOT') || !DARBUOT) exit(); ?>
<!DOCTYPE html>
<html dir="ltr" lang="lt-LT">
<head>
	<meta charset="UTF-8">
	<?php if(KESV) {
		/*
		http://www.html5rocks.com/en/tutorials/webperformance/usertiming/
		http://javascript.info/tutorial/onload-ondomcontentloaded
		http://stackoverflow.com/questions/21204612/javascript-load-vs-ready-vs-domready-vs-domcontentloaded-events
		
		http://stackoverflow.com/questions/15952883/loadspeed-js-timings-onload-domcontentloaded-or-something-else
		http://phantomjs.org/examples/index.html
		
		http://blog.trasatti.it/2012/11/measuring-site-performance-with-javascript-on-mobile.html
		
		
		http://www.stevesouders.com/blog/2013/05/13/moving-beyond-window-onload/
		Ypač: http://yahoo.github.io/boomerang/doc/
		
		Not so useful:
		http://www.stevesouders.com/blog/2014/08/21/resource-timing-practical-tips/
		performance.timing cross browser
		performance.timing Javascript
		http://stackoverflow.com/questions/9944170/using-the-new-javascript-performance-timing-api-on-iframes
		http://logme.mobi/jsnt/JS_PerfAPI.html
		
		https://developer.mozilla.org/en-US/docs/Web/Events/DOMContentLoaded IE 9.0
		http://www.softwareishard.com/blog/firebug/page-load-analysis-using-firebug/
		https://getfirebug.com/wiki/index.php/Net_Panel
		onload log time javascript ajax
		onload load time javascript ajax
		http://ablogaboutcode.com/2011/06/14/how-javascript-loading-works-domcontentloaded-and-onload/
		DOMContentLoaded onload log page time
		http://radar.oreilly.com/2013/10/going-beyond-onload-measuring-performance-that-matters.html
		
		a bit more useful:
		http://assets.en.oreilly.com/1/event/62/Above%20the%20Fold%20Time_%20Measuring%20Web%20Page%20Performance%20Visually%20Presentation.pdf
		
		https://dvcs.w3.org/hg/webperf/raw-file/tip/specs/NavigationTiming/Overview.html#introduction
		
		Good stuff: http://yahoo.github.io/
		*/
	}
	?>
	<script type="text/javascript">
	var start = new Date().getTime();
	function prefixedPerfSupport() {
	  if (!!window.msPerformance) {
		return "ms";
	  } else if (!!window.webkitPerformance) {
		return "webKit";
	  } else {
		// Chrome and IE are the only browsers that ever implemented this API with a
		//   prefix, so no need to test for other prefixes. Source: http://caniuse.com/nav-timing
		return false;
	  }
	};
	var featureDetect = {};
	featureDetect.supported = (window.msPerformance || window.webkitPerformance || window.performance) ? true : false;
	featureDetect.prefixed = prefixedPerfSupport();
	featureDetect.unprefixed = (self.performance) ? true : false;
	
  if (featureDetect.supported) {
	if (featureDetect.unprefixed) {
	  var t = window.performance.timing.navigationStart;
	} else if (featureDetect.prefixed == "ms") {
	  var t = window.msPerformance.timing.navigationStart;
	} else if (featureDetect.prefixed == "webkit") {
	  var t = window.webkitPerformance.timing.navigationStart;
	} else {
	  var t = start;
	}
  } else {
  	 var t = start;
  }  
  //console.log(new Date().getTime() - performance.timing.navigationStart)
  var DOMContentLoadedDiff;
  /*		document.addEventListener("DOMContentLoaded", function(event) {
	console.log(new Date().getTime() - performance.timing.navigationStart, "DDOM fully loaded and parsed");
  });*/
  </script>
<!--  <script type="text/javascript" src="/libs/jsnlog.min.js"></script> -->
	<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="X-UA-Compatible" content="chrome=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	-->
	<meta name="viewport" content="width=device-width">
	<meta name="msapplication-config" content="none"/>
	
	<title><?php echo $config['darzelio_pavadinimas']; ?></title>
	<?php if($_GET['q'] == 'tabeliai') { ?>
		<link rel="stylesheet" href="/css/is-new.css">
		<link rel="stylesheet" href="/css/jquery-ui-1.11.2.min.css">
	<?php } else { ?>
		<link rel="stylesheet" href="/css/jquery-ui-1.11.4.min.css">
		<link rel="stylesheet" href="/css/is-new.css">
	<?php } ?>
	<?php if(isset($_GET['printArchive'])) { ?>
		<style>
		#content-wrapper {margin: 0.75cm;}
		</style>
	<?php } ?>
	<script>var ie = false;</script>
	<!--[if lt IE 12]>
	<style type="text/css">
	select {width: 100%;}
	.sel {background-image:none;}
	</style>
	<![endif]-->
	<!--[if lt IE 8]>
	<style type="text/css">
	select { width: auto; }
	</style>
	<![endif]-->
	<!--[if lt IE 7]>
	<style type="text/css">
	#nav {position: absolute; top: 0;}
	#content-sidebar {left: 0;}
	#content-wrapper {margin-left: 229px;}
	</style>
	<script>ie = true;if (!window.console) console = {log: function() {}};</script>
	<![endif]-->
	<?php
	/*
	http://www.quirksmode.org/css/condcom.html
	http://msdn.microsoft.com/en-us/library/ms537512(v=vs.85).aspx
	http://www.sitepoint.com/web-foundations/internet-explorer-conditional-comments/
	http://webdesignerwall.com/tutorials/css-specific-for-internet-explorer
	https://www.google.lt/search?q=different+browser+view&oq=browser+view+different+&aqs=chrome.1.69i57j0l3.12758j0j7&sourceid=chrome&es_sm=93&ie=UTF-8
	
	*/
	
	if(isset($_GET['psl'])) { ?>
		<script type="text/javascript" src="/libs/ckeditor/ckeditor.js"></script>
		<!--
		<script type="text/javascript" src="/libs/wymeditor/jquery.js"></script>
		<script type="text/javascript" src="/libs/jquery.wymeditor.min.js"></script><!-- wymeditor-1.0.0b2 ->
		<script>
		jQuery(function() {
			jQuery('.wymeditor').wymeditor({
				stylesheet: 'wym-styles.css'
			});
		});
		</script>
		-->
	<?php } else { ?>
	<script type="text/javascript" src="/libs/jquery-1.11.3.min.js"></script><!-- instead of jquery-1.10.2.min.js -->
	<script>
	<?php
	/*				document.addEventListener("DOMContentLoaded", function(event) {
		console.log(new Date().getTime() - performance.timing.navigationStart, "DOM fully loaded and parsed");
	  });*/
	?>
	$(document).on('DOMContentLoaded', function () {
		DOMContentLoadedDiff = new Date().getTime() - t;// performance.timing.navigationStart
		<?php //console.log(diff, 'DOMContentLoaded')   // 10 ms ?>
	});
	<?php
	/*		document.addEventListener("DOMContentLoaded", function(event) {
		console.log(new Date().getTime() - performance.timing.navigationStart, "DOM fully loaded and parsed");
	  });*/
	?>
	</script>
	<?php } ?>
	<?php if($_GET['q'] == 'forms_fill_out' || $_GET['q'] == 'forms_make') { //ckeditorStandard ?>
	<script type="text/javascript" src="/libs/ckeditorCustom/ckeditor.js"></script>
	<?php } ?>
	<script type="text/javascript" src="/libs/jquery.ui.datepicker-lt.js"></script>
	<script type="text/javascript" src="/workers/common.js?5"></script>
<?php // <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.dirtyforms/2.0.0/jquery.dirtyforms.min.js"></script> ?>
	<script type="text/javascript" src="/libs/jquery-ui-1.11.4.min.js"></script><!-- instead of jquery-ui-1.10.4.min.js -->
	<script type="text/javascript" src="/libs/sisyphus.min.js"></script>
	<script type="text/javascript" src="/libs/introjs/2.2.0/intro.min.js"></script>
	<link rel="stylesheet" href="/libs/introjs/2.2.0/introjs.min.css"><!--  -->
	
	<script type="text/javascript">
	$.datepicker.setDefaults(
		$.extend(
			{'dateFormat':'yy-mm-dd'},
			$.datepicker.regional['lt']
		)
	);
	$(function() {
		$(".datepicker").datepicker();
		var open = true;
		$('#menu-toggle').click(function() {
			$('#nav').toggle();
			if(open) {
				//console.log('0px');
				if(ie)
					$('#content-wrapper').css('marginLeft', 0);
				else
					$('#content-sidebar').css('left', '0');
				$('#menu-toggle').css('transform', 'rotate(180deg)');
				$('#menu-toggle').addClass('menu-toggle-closed');
				$('#menu-toggle').removeClass('menu-toggle-opened').attr('title', 'Vėl rodyti meniu juostelę');
				
			} else {
				if(ie)
					$('#content-wrapper').css('marginLeft', '229px');
				else
					$('#content-sidebar').css('left', '229px');
				$('#menu-toggle').css('transform', 'rotate(0deg)');
				$('#menu-toggle').addClass('menu-toggle-opened').removeClass('menu-toggle-closed').attr('title', 'Laikinai paslėpti meniu juostelę');
			}
			open = !open;
		});
		$('textarea').autosize();
		$(document).on("click", "a", function() {
			$('textarea').autosize();//First reason was planning table rows dynamic addition
		});
		<?php /*
		$('body').append('<div id="dirty-dialog" style="display:none;"></div>');
		$.DirtyForms.dialog = {
			// Custom properties to allow overriding later using 
			// the syntax $.DirtyForms.dialog.title = 'custom title';

			title: 'Ar tikrai norite neišsaugoti?',//Are you sure you want to do that
			proceedButtonText: 'Palikti šį puslapį (neišsauogti)',
			stayButtonText: 'Išlikti (norėsiu išsaugoti)',//Stay Here
			preMessageText: '<span class="ui-icon ui-icon-alert" style="float:left; margin:2px 7px 25px 0;"></span>',
			postMessageText: '',
			width: 430,

			// Dirty Forms Methods
			open: function (choice, message) {
				$('#dirty-dialog').dialog({
					open: function () {
						// Set the focus on close button. This takes care of the 
						// default action by the Enter key, ensuring a stay choice
						// is made by default.
						$(this).parents('.ui-dialog')
							   .find('.ui-dialog-buttonpane button:eq(1)')
							   .focus();
					},

					// Whenever the dialog closes, we commit the choice
					close: choice.commit,
					title: this.title,
					width: this.width,
					modal: true,
					buttons: [
						{
							text: this.stayButtonText,
							click: function () {
								// We don't need to take any action here because
								// this will fire the close event handler and
								// commit the choice (stay) for us automatically.
								$(this).dialog('close');
							}
						},
						{
							text: this.proceedButtonText,
							click: function () {
								// Indicate the choice is the proceed action
								choice.proceed = true;
								$(this).dialog('close');
							}
						}
					]
				});

				// Inject the content of the dialog using jQuery .html() method.
				$('#dirty-dialog').html(this.preMessageText + message + this.postMessageText);
			},
			close: function () {
				// This is called by Dirty Forms when the 
				// Escape key is pressed, so we will close
				// the dialog manually. This overrides the default
				// Escape key behavior of jQuery UI, which would
				// ordinarily not fire the close: event handler 
				// declared above.
				$('#dirty-dialog').dialog('close');
			}
		};
		$('form').dirtyForms({
			//dialog: { title: 'Wait!' },
			message: 'NEišsaugojote naujų duomenų pakeitimų/įvedimų. Norint, kad jie išliktų turite likti šiame puslapyje (palikti tinklalapį atvertą jo neįkeliant iš naujo) ir šio puslapio apačioje spustelėti ant mygtuko „Išsaugoti“.',
			//ignoreSelector: '.ignore-dirty'
		});
		*/ ?>
	});
	</script>
	<?php
	if(!(/*$_GET['q'] == '' ||*/ $_GET['q'] == 'pakeisti_slaptazodi'))
		echo "<style>#nav .active:before {content: '➤ '; margin-left: -20px; width: 20px; display: block; float: left;}</style>";
	?>
</head>
<body onload="onLoad()">
<?php
$result = db_query("SELECT COUNT(*) AS cnt FROM `".DB_messages."` WHERE `toPersonId`=".DARB_ID." AND `toPersonType`>0 AND `read`=0");
$r = mysqli_fetch_assoc($result);
$newMsg = '';
if($r['cnt'] > 0)
	 $newMsg = ' (<strong class="abbr" title="Naujos žinutės">'.$r['cnt'].'</strong>)';
?>
<div id="header" class="no-print">
	<div id="title" style="max-width: 490px/*475*/;">
		<img src="/img/musuDarzelisB.png" id="logo" alt="MūsųDarželis" height="40" style="vertical-align: middle; margin-top: -5px;">
		
		<?php /* <span style="font-size: 40px;">❄☃</span> */ ?><span style="vertical-align: top"><?=(KESV ? array_search(DB_PREFIX, $db_prefixes).' ' : '').$config['darzelio_pavadinimas']?></span> <!-- ❆❊❅❉ -->
	</div>
	<?php
	include './libs/linksniai.php';
	$l = new Linksniai;
	//echo 'Laba diena, '. .'!' ;
	?>
	Sveiki, <?=lcfirst($person_type_sau[$_SESSION['USER_DATA']['person_type']]).($_SESSION['USER_DATA']['person_type'] == 3 ? ' (administratoriau) ' : '')?> <!-- auklėtoja --> 
	<?=$l->getName(filterText($_SESSION['USER_DATA']['name'].' '.$_SESSION['USER_DATA']['surname']))?><!-- Danute Cvirkiene -->
</div>
<div id="content-sidebar">
	<div id="nav" class="no-print">
		<?php
		include 'index_menu.php';
		?>
	</div>
	<div id="content-wrapper">
		<!-- <div class="ok" style="margin: 0px;">Duomenų bazėje dokumento informacija atnaujinta.</div> -->
		<div id="menu-toggle" class="menu-toggle-opened" title="Laikinai paslėpti meniu juostelę">&lt;</div>
	
		<?php
		if(KESV && isset($_GET['debug'])) echo '<span id="res"></span><script>$("#res").html(
		$(window).width()+" "+   // returns width of browser viewport
		$(document).width()+" "+ // returns width of HTML document
		$(window).height()+" "+   // returns height of browser viewport
		$(document).height()+" "+ // returns height of HTML document

		screen.width+" "+screen.height);
		</script>';
		include 'index_routes.php';
		?>
		<div id="autoLogout" style="height: 100%; width: 100%; background-color: rgba(255, 255, 255, 0.9); position: fixed; top: 0%; display: none;">
			<div style="width: 500px; height: 100px; position: absolute; top: 50%; left: 50%; margin-top: -50px; margin-left: -250px;">
				Kadangi per ilgai ką nors darėte Jūs buvote automatiškai atjungtas. Todėl prašome prisijungti iš naujo <a href="/" target="_blank" onclick=" $('#autoLogout').hide();" >naujoje kortelėje/lange (paspaudus ant šios nuorodos šis pranešimas dings, kad galėtumėte vėl prisijungus išsaugoti šiame lange esančius duomenis)</a>, tą padarius grįškite į šį langą/kortelę kuriame ir galėsite išsaugoti šiuos duomenis.<br>
				<button onclick="$('#autoLogout').hide();">Uždaryti šį pranešimą rankiniu būdu</button>
			</div>
		</div>
		<div id="dataChangeBar" style="height: 30px; width: 100%;  background-color: rgba(255, 255, 255, 0.9); position: fixed; top: 0%; display: none;">
			<div style="padding: 3px;">
				Kopijuoti iš šios formos. <button onclick="document.editForm.submit();">Išsaugoti ant viršaus</button>
			</div>
		</div>
		<div id="dataChange" style="height: 100%; width: 100%; background-color: rgba(255, 255, 255, 0.8); position: fixed; top: 0%; display: none;">
			<div style="width: 500px; height: 100px; position: absolute; top: 50%; left: 50%; margin-top: -50px; margin-left: -250px;">
				<p style="font-weight: bold;">Atšauktas išsaugojimas ant viršaus</p>
				Nuo redagavimo pradžios šis planavimas jau buvo paredaguotas, todėl čia redaguojant nematote tik ką padarytų pakeitimų. Kad galėtumėte susitikrinti ir išsaugoti suliejant šiuos pakeitimus su tik ką (t.y. <span class="updated"></span>) padarytais pakeitimais (Jūsų arba kieno nors kito) reikėtų sulieti abi redagavimo versijas, todėl turėtumėte norimus pakeitimus nukopijuoti į <a href="" target="_blank" onclick="$('#dataChange').hide();" >naujoje kortelėje/lange esančią formą ir per ją išsaugoti duomenis (paspaudus ant šios nuorodos šiame lange/kortelėje pranešimas turėtų būti neberodomas, bet bus rodoma viršuje esanti juostelė, o naujoje lange/kortelėje atsidarys ta pati forma su naujausia redagavimo versija)</a>.<br><br>
				<!-- Pastaba: Šis pranešimas netikrina ar Jūs ką nors keitėte formoje.<br><br> -->
				<button onclick="$('#dataChange').hide();">Uždaryti šį pranešimą rankiniu būdu</button>
				<button onclick="document.editForm.submit();">Išsaugoti ant viršaus</button>
			</div>
		</div>
		<script src="/libs/placeholders.jquery.min.js"></script>
		<div class="not-saved" style="display: none;">Įvestų duomenų dar neišsaugojote.<!-- Kažką įvedėte. Įspėjame, kad šio įvedimo dar neišsaugojote Tad išsaugokite kai baigsite įvedimą. --></div>
		<div style="position: fixed; top: 0; height: 40px; left: 50px; right: 50px; text-align: center; background: #357d32; border: 2px solid #255622; border-top: none; color: white; line-height: 40px; font-weight: bold; border-radius: 0 0 10px 10px; z-index: 10; display: none;" id="DONE"></div>
		<?php
		//http://jamesallardice.github.io/Placeholders.js/
		
		/*
		textarea auto height expand
		autoExpand DEMO PAGE http://stackoverflow.com/questions/2948230/auto-expand-a-textarea-using-jquery
		https://github.com/jaz303/jquery-grab-bag
		Geriausias 3.2 KB: http://www.jacklmoore.com/autosize/
		*/
?>
<script type="text/javascript" src="/libs/jquery.autosize.min.js"></script>
<script>
<?php if(!(KESV || TESTINIS || RODOMASIS || NAUJAS)) { ?>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-1202195-12', 'musudarzelis.lt');
ga('send', 'pageview');
<?php } ?>
function onLoad() {
	var page_load_time = new Date().getTime() - t;//performance.timing.navigationStart
	<?php
	if(!startsWith($_GET['q'], 'uploads/') && 'apple-touch-icon-precomposed.png' != $_GET['q'] && 'apple-touch-icon.png' != $_GET['q'] && 'browserconfig.xml' != $_GET['q'] && 0 != $_GET['q']) {
		$result = db_query("SELECT `menu_id` FROM `0menu` WHERE `user_type`='w' AND `menu_url`='".db_fix($_GET['q'])."'");
		if(mysqli_num_rows($result) == 0) {
			db_query("INSERT INTO `0menu` SET `user_type`='w', `menu_url`='".db_fix($_GET['q'])."'");
			$menu_id = (int)mysqli_insert_id($db_link);
		} else
			$menu_id = (int)mysqli_fetch_assoc($result)['menu_id'];
	
		db_query("INSERT INTO `0time` SET `ipv4`=INET_ATON('{$_SERVER['REMOTE_ADDR']}'), `time`='".(microtime(true)-$start)."', `kindergarten`=".array_search(DB_PREFIX, $db_prefixes).", `user_id`=".USER_ID.", `person_id`=".DARB_ID.", `person_login_type`=".$_SESSION['USER_DATA']['person_type'].", `menu_id`=".(int)$menu_id);//INSERT DELAYED
		$log_id = (int)mysqli_insert_id($db_link);
		?>
		$.post( "/log.php", { DOMContentLoaded: DOMContentLoadedDiff, onLoad: page_load_time, id: <?=$log_id?> } );
	<?php } ?>
//	console.log("User-perceived page loading time: " + page_load_time);
}
</script>
	</div>
</div>
</body>
</html>
