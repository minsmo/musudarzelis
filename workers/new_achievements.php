<?php if(!defined('ADMIN')) exit();
//Idea: spalvos blokelio nurodo, kad esi viduje kažko. kaip matrioška.
//Problema: 

$edit_save_delete = !SOCIALINIS_PEDAGOGAS;

$chart_types = ['Kreivė', 'Stulpeliai', 'Voratinklis'];;//['Linijų', 'Stulpelių', 'Voratinklio'];
?>
<script src="workers/new_achievements.js?v3"></script>
<script src="libs/Chart.min.js"></script>

<h1>Vaiko pasiekimai pagal naują  ikimokyklinį <span class="abbr" title="Lietuvos Respublikos švietimo ir mokslo ministerija">ŠMM</span> aprašą</h1>
<div id="content">
<?=ui_print()?> &nbsp; 
<?php if(isset($_GET['date']) && isset($_GET['dateWith'])) { ?>
<a href="#" class="no-print fast-action abbr" title="Jei nepavyktų šis, tada bandykite: 1) iš naujo atidarykite puslapį 2) sumažinkite naršyklės plotį, kad sumažėtų diagrama 3) pasitikrinkite ar telpa į 1-ą A4 lapą" onclick="$('body').append('<style>@page { size: landscape; padding: 27mm 16mm 27mm 16mm;} @media print { html, body { width: 297mm; height: 210mm; } }</style>');">Bandyti pritaikyti spausdinimui 1-ame A4 lape</a>
<?php } ?>

<?php
function canvas($labels, $data1, $data2) {
	global $_GET;
	?>
			<canvas id="achievementsChart" width="767" height="400"></canvas>
			<script>
			// Get the context of the canvas element we want to select
			var ctx = document.getElementById("achievementsChart").getContext("2d");
			Chart.defaults.global.scaleFontSize = 16;
			Chart.defaults.global.scaleFontColor = "#000";
			Chart.defaults.global.responsive = true;
			Chart.defaults.global.scaleOverride = true;
			Chart.defaults.global.scaleSteps = 7,
			Chart.defaults.global.scaleStepWidth = 1;
			Chart.defaults.global.scaleStartValue = 0;
			var data = {
				labels: ["<?php echo $labels; ?>"],
				datasets: [
					{
						fillColor: "<?php echo ($_GET['chart_type'] == 1 ? '#666' : 'transparent'); ?>",//"#666",//"rgba(220,220,220,0.2)",
						strokeColor: "<?php echo ($_GET['chart_type'] == 1 ? '#666' : '#000'); ?>",//"rgba(220,220,220,1)",
						pointColor: "#000",//"rgba(220,220,220,1)",
						pointStrokeColor: "#000",
						pointHighlightFill: "#000",
						//pointHighlightStroke: "rgba(220,220,220,1)",
						data: [<?php echo $data1 ?>]
					}
					<?php if(!empty($_GET['dateWith'])) { ?>
					, {
						////fillColor: "#666",//"rgba(220,220,220,0.2)",
						//strokeColor: "rgba(220,220,220,1)",
						////pointColor: "rgba(220,220,220,1)",
						//pointStrokeColor: "#fff",
						//pointHighlightFill: "#fff",
						//pointHighlightStroke: "rgba(220,220,220,1)",
						fillColor: "<?php echo ($_GET['chart_type'] == 1 ? '#357d32' : 'transparent'); ?>",//'rgba(188,216,188,0.5)',//'#bcd8bc',//"rgba(151,187,205,0.2)",
						strokeColor: '#357d32',//"rgba(151,187,205,1)",
						pointColor: 'rgba(53,125,50,0.8)',//'#357d32',//"rgba(151,187,205,1)",
						pointStrokeColor: 'rgba(53,125,50,1)',//'#357d32',//"#fff",
						pointHighlightFill: 'rgba(53,125,50,1)',//'#357d32',//"#fff",
						pointHighlightStroke: 'rgba(53,125,50,1)',//'#357d32',//"rgba(151,187,205,1)",
					
						data: [<?php echo $data2 ?>]
					}
					<?php } ?>
				]
			};
			//var myNewChart = new Chart(ctx).PolarArea(data);
			<?php if($_GET['chart_type'] == 0) { ?>
			var myLineChart = new Chart(ctx).Line(data/*, options*/);
			<?php } ?>
			<?php if($_GET['chart_type'] == 1) { ?>
			var myLineChart = new Chart(ctx).Bar(data/*, options*/);
			<?php } ?>
			<?php if($_GET['chart_type'] == 2) { ?>
			var radarOptions = {
				//String - Colour of the scale line	
				scaleLineColor : "#000",
				//String - Colour of the angle line
				angleLineColor : "rgba(0,0,0,1)",
				//Number - Point label font size in pixels	
				pointLabelFontSize : 20,
				//String - Point label font colour	
				pointLabelFontColor : "#000",
				datasetStrokeWidth : 5,
				pointDotRadius : 10,
			}
			var myLineChart = new Chart(ctx).Radar(data, radarOptions/*, options*/);
			<?php } ?>
			</script>
			<?php
}

if(isset($_POST['update']) && isset($_GET['kid_id']) && $edit_save_delete) {
	$exists = array();
	$result = db_query("SELECT * FROM `".DB_kid_level."` WHERE `kindergarten_id`=".DB_ID." AND `kid_id`='".(int)$_GET['kid_id']."' AND `date`='".db_fix($_POST['date'])."'");
	while($row = mysqli_fetch_assoc($result))
		$exists[$row['area']] = $row['ID'];//['level' => $row['level'], 'group_id' => $row['group_id'], 'notes' => $row['notes'],];//TODO: smart update, only when level and 
	$sql_insert	= [];
	foreach($_POST['achievement'] as $area => $level)
		if((int)$level != 0 || !empty($_POST['notes'][$area])) {
			if(isset($exists[$area])) {
				db_query("UPDATE `".DB_kid_level."` SET 
				`group_id`=".GROUP_ID.", `level`=".(int)$level.", `notes`='".db_fix($_POST['notes'][$area])."',
				`updatedByUserId`='".USER_ID."' AND `updatedByEmployeeId`='".DARB_ID."', `updated`=CURRENT_TIMESTAMP()
				WHERE `ID`=".$exists[$area]);//`kindergarten_id`=".DB_ID." AND `kid_id`='".(int)$_GET['kid_id']."' AND `date`='".db_fix($_POST['date'])."' AND `area`='".(int)$area."'"
			} else {
				$sql_insert[] = "(".DB_ID.", ".GROUP_ID.", ".(int)$_GET['kid_id'].", '".db_fix($_POST['date'])."', 
				".(int)$area.", ".(int)$level.", '".db_fix($_POST['notes'][$area])."', ".USER_ID.", ".DARB_ID.")";
			}
		}
	if(!empty($sql_insert))
		db_query("INSERT INTO `".DB_kid_level."` (`kindergarten_id`, `group_id`, `kid_id`, `date`, `area`, `level`, `notes`, `createdByUserId`, `createdByEmployeeId`) VALUES ".implode(',', $sql_insert));
	msgBox('OK', "Informacija išsaugota.");
}

if(isset($_POST['updateGroup']) && $edit_save_delete) {
	$exists = array();
	$ids = [];
	foreach($_POST['achievement'] as $id => $level)
		$ids[] = (int)$id;
	if(!empty($ids)) {
		$result = db_query("SELECT * FROM `".DB_kid_level."` WHERE `kindergarten_id`=".DB_ID." AND `kid_id` IN (".implode(',', $ids).") AND `date`='".db_fix($_POST['date'])."' AND `area`='".(int)$_POST['area']."'");
		while($row = mysqli_fetch_assoc($result))
			$exists[$row['kid_id']] = $row['ID'];
		$sql_insert	= [];
		foreach($_POST['achievement'] as $kid_id => $level)
			if((int)$level != 0 || !empty($_POST['notes'][$kid_id])) {
				if(isset($exists[$kid_id])) {
					db_query("UPDATE `".DB_kid_level."` SET 
					`group_id`=".GROUP_ID.", `level`=".(int)$level.", `notes`='".db_fix($_POST['notes'][$kid_id])."',
					`updatedByUserId`='".USER_ID."' AND `updatedByEmployeeId`='".DARB_ID."', `updated`=CURRENT_TIMESTAMP()
					WHERE `ID`=".$exists[$kid_id]);//`kindergarten_id`=".DB_ID." AND `kid_id`='".(int)$kid_id."' AND `date`='".db_fix($_POST['date'])."' AND `area`='".(int)$_POST['area']."'"
				} else {
					$sql_insert[] = "(".DB_ID.", ".GROUP_ID.", ".(int)$kid_id.", '".db_fix($_POST['date'])."', 
				".(int)$_POST['area'].", ".(int)$level.", '".db_fix($_POST['notes'][$kid_id])."', ".USER_ID.", ".DARB_ID.")";
				}
			}
		if(!empty($sql_insert))
			db_query("INSERT INTO `".DB_kid_level."` (`kindergarten_id`, `group_id`, `kid_id`, `date`, `area`, `level`, `notes`, `createdByUserId`, `createdByEmployeeId`) VALUES ".implode(',', $sql_insert));
		msgBox('OK', "Informacija išsaugota.");
	}
}
if(!empty($_GET['delete']) && !empty($_GET['kid_id']) && $edit_save_delete) {
	//Only who created
	db_query("DELETE FROM `".DB_kid_level."` WHERE `kindergarten_id`=".DB_ID." AND `kid_id`='".(int)$_GET['kid_id']."' AND `date`='".db_fix($_GET['delete'])."'".(ADMIN ? '' : " AND `createdByEmployeeId`='".DARB_ID."'"));//`createdByUserId`='".USER_ID."' 
	msgBox('OK', "Informacija (".mysqli_affected_rows($db_link)." vnt.) ištrinta. Leidžiama ištrinti tik sukūrėjui ir vadovui.");
}
if(!empty($_GET['delete']) && !isset($_GET['kid_id']) && $edit_save_delete) {
	//Only who created
	db_query("DELETE FROM `".DB_kid_level."` WHERE `kindergarten_id`=".DB_ID." AND `group_id`=".GROUP_ID." AND `ID`='".db_fix($_GET['delete'])."'".(ADMIN ? '' : " AND `createdByEmployeeId`='".DARB_ID."'"));//`createdByUserId`='".USER_ID."' 
	msgBox('OK', "Informacija (".mysqli_affected_rows($db_link)." vnt.) ištrinta. Leidžiama ištrinti tik sukūrėjui ir vadovui.");
}
if(isset($_GET['deleteGroupDate']) && isset($_GET['area']) && $edit_save_delete) {
	//Only who created
	db_query("DELETE FROM `".DB_kid_level."` WHERE `kindergarten_id`=".DB_ID." AND `group_id`=".GROUP_ID." AND `date`='".db_fix($_GET['deleteGroupDate'])."' AND `area`='".(int)$_GET['area']."'".(ADMIN ? '' : " AND `createdByEmployeeId`='".DARB_ID."'"));//`createdByUserId`='".USER_ID."' //`kid_id`='".(int)$_GET['kid_id']."' AND
	msgBox('OK', "Informacija (".mysqli_affected_rows($db_link)." vnt.) ištrinta. Leidžiama ištrinti tik sukūrėjui ir vadovui.");
}



if(/*!ADMIN &&*/ isset($_POST['move']) && $edit_save_delete) {
	$exists = [];
	$result = db_query("SELECT * FROM `".DB_kid_level."` WHERE `kindergarten_id`=".DB_ID." AND `group_id`=".GROUP_ID." AND `date`='".db_fix($_POST['toDate'])."'");
	while($row = mysqli_fetch_assoc($result))
		$exists[$row['kid_id'].'.'.$row['area']] = '';
	if(isset($_POST['merge']) && !empty($exists) || empty($exists)) {
		$result = db_query("SELECT * FROM `".DB_kid_level."` WHERE `kindergarten_id`=".DB_ID." AND `group_id`=".GROUP_ID." AND `date`='".db_fix($_POST['fromDate'])."'");
		$updated_cnt = 0;
		$not_updated_cnt = 0;
		$not_updated_areas = [];
		while($row = mysqli_fetch_assoc($result)) {
			if(!isset($exists[$row['kid_id'].'.'.$row['area']])) {
				++$updated_cnt;
				db_query("UPDATE `".DB_kid_level."` SET `date`='".db_fix($_POST['toDate'])."' WHERE `ID`='".(int)$row['ID']."'");
			} else {
				++$not_updated_cnt;
				$not_updated_areas[$row['area']] = $achievements_areas[$row['area']];
			}
		}
		//Blogas kodas: db_query("UPDATE `".DB_kid_level."` SET `date`='".db_fix($_POST['toDate'])."' WHERE `kindergarten_id`=".DB_ID." AND `group_id`=".GROUP_ID." AND `date`='".db_fix($_POST['fromDate'])."'");// AND `createdByEmployeeId`='".DARB_ID."' //`createdByUserId`='".USER_ID."' //`kid_id`='".(int)$_GET['kid_id']."' AND
		msgBox('OK', "Informacija ($updated_cnt vnt.) perkelta.".($not_updated_cnt > 0 ? " Neperkelta ($not_updated_cnt vnt.), nes naujoje datoje jau yra įvertinimų srityse: ".implode(', ', $not_updated_areas).'.' : ''));
		//kai jau yra keli to pačio (sutapus darželiui, vaikui, sričiai) įvertinimai ir esminis duomuo t. y. data skiriasi (dar gali ir pats įvertinimas skirtis ir užrašai), tad pagal numatymą įrašyti pačią naujausią datą (jei yra šio mėn. ar praeito mėn. vertinimų) ir šalia komentarą parašyti „automatiškai įrašėme naujausio vertinimo datą, kad ateityje galėtumėte lengvai lyginti diagramas skirtingų datų (dažniausiai viena data  - rudeniui, kita data - pavasariui)“.
	} else {
		msgBox('WARN', 'Datoje '.filterText($_POST['fromDate']).' (į kurią bandote perkelti) jau yra '.count($exists).' įvertinimų (-ai), ar tikrai norite perkelti įvertinimus iš '.filterText($_POST['toDate']).'
				<form method="post">
					<input type="hidden" name="toDate" value="'.filterText($_POST['toDate']).'">
					<input type="hidden" name="merge" value="1">
					<input type="hidden" name="fromDate" value="'.filterText($_POST['fromDate']).'">
					<input type="submit" name="move" class="submit no-print" style="margin:0" value="Patvirtinti perkėlimą">
				</form>
		');
	}
}

if(/*!ADMIN &&*/ isset($_POST['copy']) && $edit_save_delete) {
	$exists = [];
	$result = db_query("SELECT * FROM `".DB_kid_level."` WHERE `kindergarten_id`=".DB_ID." AND `group_id`=".GROUP_ID." AND `date`='".db_fix($_POST['toDate'])."'");
	while($row = mysqli_fetch_assoc($result))
		$exists[$row['kid_id'].'.'.$row['area']] = '';
	if(isset($_POST['merge']) && !empty($exists) || empty($exists)) {
		$result = db_query("SELECT * FROM `".DB_kid_level."` WHERE `kindergarten_id`=".DB_ID." AND `group_id`=".GROUP_ID." AND `date`='".db_fix($_POST['fromDate'])."'");
		$updated_cnt = 0;
		$not_updated_cnt = 0;
		$not_updated_areas = [];
		while($row = mysqli_fetch_assoc($result)) {
			if(!isset($exists[$row['kid_id'].'.'.$row['area']])) {
				++$updated_cnt;
				$row['date'] = db_fix($_POST['toDate']);
				unset($row['ID']);
				$set = [];
				foreach($row as $key => $val)
					$set[] = "`$key`='$val'";
				db_query("INSERT INTO `".DB_kid_level."` SET ".implode(',', $set));
			} else {
				++$not_updated_cnt;
				$not_updated_areas[$row['area']] = $achievements_areas[$row['area']];
			}
		}
		//Blogas kodas: db_query("UPDATE `".DB_kid_level."` SET `date`='".db_fix($_POST['toDate'])."' WHERE `kindergarten_id`=".DB_ID." AND `group_id`=".GROUP_ID." AND `date`='".db_fix($_POST['fromDate'])."'");// AND `createdByEmployeeId`='".DARB_ID."' //`createdByUserId`='".USER_ID."' //`kid_id`='".(int)$_GET['kid_id']."' AND
		msgBox('OK', "Informacija ($updated_cnt vnt.) nukopijuota.".($not_updated_cnt > 0 ? " Nenukopijuota ($not_updated_cnt vnt.), nes naujoje datoje jau yra įvertinimų srityse: ".implode(', ', $not_updated_areas).'.' : ''));
		//kai jau yra keli to pačio (sutapus darželiui, vaikui, sričiai) įvertinimai ir esminis duomuo t. y. data skiriasi (dar gali ir pats įvertinimas skirtis ir užrašai), tad pagal numatymą įrašyti pačią naujausią datą (jei yra šio mėn. ar praeito mėn. vertinimų) ir šalia komentarą parašyti „automatiškai įrašėme naujausio vertinimo datą, kad ateityje galėtumėte lengvai lyginti diagramas skirtingų datų (dažniausiai viena data  - rudeniui, kita data - pavasariui)“.
	} else {
		msgBox('WARN', 'Datoje '.filterText($_POST['fromDate']).' (į kurią bandote kopijuoti) jau yra '.count($exists).' įvertinimų (-ai), ar tikrai norite nukopijuoti įvertinimus iš '.filterText($_POST['toDate']).'
				<form method="post">
					<input type="hidden" name="toDate" value="'.filterText($_POST['toDate']).'">
					<input type="hidden" name="merge" value="1">
					<input type="hidden" name="fromDate" value="'.filterText($_POST['fromDate']).'">
					<input type="submit" name="copy" class="submit no-print" style="margin:0" value="Patvirtinti kopijavimą">
				</form>
		');
	}
}

$suggested_date = '';
$result = db_query("SELECT MAX(`date`) as `date` FROM `".DB_kid_level."` WHERE `kindergarten_id`=".DB_ID);
if(mysqli_num_rows($result) > 0) {
	$row = mysqli_fetch_assoc($result);
	$suggested_date = '
	Siūloma data '.$row['date'].' <span class="abbr" title="Ateities lengvam lyginimui diagramose skirtingų ilgųjų laikotarpių (dažniausiai viena data - rudeniui, kita data - pavasariui)">(priežastis)</span>';
}


// change GROUP
//$URL = '?';
//if( isset($_GET['group_id']) )
//	$URL .= 'group_id='.(int)$_GET['group_id'].'&amp;';
if(ADMIN) {
	?>
	<form method="get">
		<?php /*Užėję į*/ ?>
		<div class="sel" style="float: left"><select name="change_group_id" onchange="if(this.value != '') this.form.submit()" required="required">
		<?php
		//echo "<option value=\"\">Nepažymėta</option>";//Neparinkta
		foreach(getAllGroups() as $ID => $title)
			echo "<option value=\"".$ID."\"".($ID == GROUP_ID/*isset($_GET['group_id']) && $ID == $_GET['group_id']*/ ? ' selected="selected" class="selected">'.$selectedMark : '>').$title."</option>";
		?>
		</select></div>
		<input type="submit" class="filter no-print" value="Pereiti">
	</form>
	<?php //(ADMIN ? (int)$_GET['group_id'] : GROUP_ID)
} else echo '<div class="on-print">Vaikų grupės „'.getAllGroups(GROUP_ID).'“</div>';

//Move evaluation
if(/*!ADMIN &&*/ $edit_save_delete) {
	echo '<p><a href="?move" class="no-print">Senų įvertinimų perkėlimas arba kopijavimas į kitą datą</a></p>';//Ankstesnių
	
	if(isset($_GET['move'])) {
		$result = db_query("SELECT *, COUNT(*) `cnt` FROM `".DB_kid_level."` WHERE `kindergarten_id`=".DB_ID." AND `group_id`=".GROUP_ID." GROUP BY `date` ORDER BY `date` DESC");
		//echo '<div style="margin-left: 50px;">';
		if(mysqli_num_rows($result) > 0) {
			//<p class="notice">Atidžiai parinkite datą perkelėlimui arba kopijavimui.</p>
			?>
			<table>
			<!-- <caption><strong>Jau įvertinti pasiekimai:</strong></caption> -->
			<tr>
				<th class="date-cell">Iš datos</th>
				<th class="date-cell">Įvertinimų</th>
				<th class="no-print">Į datą</th>
			</tr>
			<?php
			while ($row = mysqli_fetch_assoc($result)) {
				echo "\t<tr>
				<td>".$row['date']."</td>
				<td>".$row['cnt']."</td>
				<td class=\"no-print\">";
				//<label><input type="hidden" name="merge" value="1" style="width: 33px;"> <span class="abbr" title="Pažymėjus priverstinai kopijuos/perkels į datą įvertinimus, kurių dar nėra.">Sulieti</span></label>
				?>
				<form method="post">
					<input type="text" name="toDate" class="datepicker" required="required">
					<input type="hidden" name="fromDate" value="<?=filterText($row['date'])?>">
					<input type="submit" name="move" class="submit no-print" style="margin:0" value="Perkelti"><!-- Pereiti -->
					<input type="submit" name="copy" class="submit no-print" style="margin:0" value="Kopijuoti"><!-- Pereiti -->
				</form>
				</td>
				</tr>
				<?php
			}
			echo "</table><br><br>";
		} else {
			echo '<p>Nėra išsaugotų įvertinimų.</p>';
		}
	}
}

//Show table
$result = db_query("SELECT *, COUNT(*) `cnt` FROM `".DB_kid_level."` WHERE `kindergarten_id`=".DB_ID." AND `group_id`=".GROUP_ID." GROUP BY `date` ORDER BY `date` DESC");
if(mysqli_num_rows($result) > 0) {
	?>
	<form method="get"<?=(isset($_GET['show_table']) ? '' : ' class="no-print"')?> style="margin-top: 10px;">
		<div style="float: left; line-height: 33px;">Įvertinimų peržiūra lentele<!-- rodymas lentelėje --> &nbsp;</div> <div class="sel" style="float: left"><select name="show_table" onchange="if(this.value != '') this.form.submit()" required="required">
		<option value="" disabled<?=(isset($_GET['show_table']) ? '' : ' selected')?> hidden>Pasirinkite datą</option>
		<?php
		while ($row = mysqli_fetch_assoc($result))
			echo "<option value=\"".$row['date']."\"".(isset($_GET['show_table']) && $row['date'] == $_GET['show_table'] ? ' selected="selected" class="selected">'.$selectedMark : '>').$row['date']."</option>";
		?>
		</select></div>
		<!-- <input type="submit" class="filter no-print" value="Rodyti lentelėje"> -->
	</form><div class="cl"></div>
	<?php
}
if(isset($_GET['show_table'])) {
	include 'new_achievements_descriptions.php';
	$date = $_GET['show_table'];
	$result = db_query("SELECT cr.*, `".DB_kid_level."`.*
		FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<='".db_fix($date)."' GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
		LEFT JOIN `".DB_kid_level."` ON cr.`parent_kid_id`=`".DB_kid_level."`.kid_id AND `".DB_kid_level."`.`kindergarten_id`=".DB_ID." AND `".DB_kid_level."`.`date`='".db_fix($date)."'
		WHERE cr.`isDeleted`=0 AND cr.`grupes_id`=".GROUP_ID." AND cr.`archyvas`=0
		-- GROUP BY cr.parent_kid_id
		ORDER BY ".orderName('cr', $date).", `".DB_kid_level."`.`area`");//cr.`vardas` ASC, cr.`pavarde` ASC");
	$kids_level = [];
	$kids = [];
	$min_level = 7;
	$max_level = 0;
	while($row = mysqli_fetch_assoc($result)) {
		$kids_level[$row['parent_kid_id']][$row['area']] = $row['level'];
		if($row['level'] != 0)
			$min_level = min($min_level, $row['level']);
		$max_level = max($max_level, $row['level']);
		if(!empty($row['gim_data'])) {
			$start_date = new DateTime($row['gim_data']);
			$since_start = $start_date->diff(new DateTime());
			$kids[$row['parent_kid_id']] = '<td>'.$row['gim_data'].' (<span style="font-family:\'Lucida Console\', monospace">'.str_pad($since_start->y, 2, " ", STR_PAD_LEFT).'</span> m.&nbsp;<span style="font-family:\'Lucida Console\', monospace">'.str_pad($since_start->m, 2, " ", STR_PAD_LEFT).'</span> mėn.)</td>';
		} else 
			$kids[$row['parent_kid_id']] = '<td></td>';
		$kids[$row['parent_kid_id']] .= '<td><strong>'.filterText(getName($row['vardas'], $row['pavarde'])).'</strong></td>';
	}
	$diff_colors = ($max_level - $min_level);
	//echo $min_level.$max_level;
	$diff_colors_step = ($diff_colors > 0 ? round(255 / $diff_colors) : 255);
	$diff_colors_bg = [$min_level => 0, $max_level => 255];
	for($i = $min_level+1; $i < $max_level; ++$i)
		$diff_colors_bg[$i] = $diff_colors_bg[$i-1]+$diff_colors_step;
	$diff_colors_dechex = [];
	for($i = $min_level; $i <= $max_level; ++$i)
		$diff_colors_dechex[$i] = dechex($diff_colors_bg[$i]).dechex($diff_colors_bg[$i]).dechex($diff_colors_bg[$i]);
	//print_r($diff_colors_bg);
	echo '<table class="print-bg">
	<tr>
		<th colspan="4" style="text-align: right;border-right: 2px solid #000;">Lūkesčiai: Sveikas</th>
		<th colspan="5" style="text-align: center;border-right: 2px solid #000;">Orus</th>
		<th colspan="2" style="border-right: 2px solid #000;">Ben-<br>drau-<br>jan-<br>tis</th>
		<th colspan="2" style="border-right: 2px solid #000;">Smal-<br>sus</th>
		<th colspan="2" style="border-right: 2px solid #000;">Ku-<br>rian-<br>tis</th>
		<th colspan="5" style="text-align: center;">Sėkmingai besiugdantis</th>
	</tr>
	<tr><th>Amžius<br>iki<br>dabar/'.date('Y-m-d').'</th><th></th>';
	foreach($achievements_areas as $area => $title) {
		$border = '';
		if($area == 2 || $area == 7 || $area == 9 || $area == 11 || $area == 13)
			$border = 'border-right: 2px solid #000000;';
		echo "<th style='height: 255px; width: 15px; $border'><div style='transform: rotate(-90deg); width: 250px; position: absolute; z-index:1; margin-left: -118px; margin-top: -10px;'><span data-id=\"dialog-".$area."\" class=\"href dialog-opener\">$title</span></div></th>";
	}
	echo '</tr>';
	$areas_sum = [];
	$areas_cnt = [];
	foreach($kids as $ID => $kid) {
		echo '<tr>'.$kids[$ID];
			foreach($achievements_areas as $area => $title) {
				$border = '';
				if($area == 2 || $area == 7 || $area == 9 || $area == 11 || $area == 13)
					$border = 'border-right: 2px solid #000000;';
				if(isset($kids_level[$ID][$area]) && $kids_level[$ID][$area] != 0) {
					if(!isset($areas_sum[$area])) {
						$areas_sum[$area] = 0;
						$areas_cnt[$area] = 0;
					}
					$areas_sum[$area] += $kids_level[$ID][$area];
					++$areas_cnt[$area];
					echo '<td style="'.$border.'text-align: center; background-color: #'.$diff_colors_dechex[$kids_level[$ID][$area]].'; box-shadow: inset 0 0 0 1000px #'.$diff_colors_dechex[$kids_level[$ID][$area]].'; color: #'.($diff_colors_bg[$kids_level[$ID][$area]] < 128 ? 'fff' : '000').'; -ms-filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=\'#'.$diff_colors_dechex[$kids_level[$ID][$area]].'\', endColorstr=\'#'.$diff_colors_dechex[$kids_level[$ID][$area]].'\'); /* IE8 */"><span style="text-align: center; background-color: inherit; color: #'.($diff_colors_bg[$kids_level[$ID][$area]] < 128 ? 'fff' : '000').';">'.$kids_level[$ID][$area].'</span></td>';
					//http://stackoverflow.com/questions/3893986/css-media-print-issues-with-background-color
				} else
					echo '<td style="'.$border.'"></td>';
			}
		echo '</tr>';
	}
	echo '<tr><td colspan="2" style="text-align: right">Vidurkis:</td>';
	foreach($achievements_areas as $area => $title) {
		if(isset($areas_sum[$area])) {
			$avg = $areas_sum[$area]/(1.0*$areas_cnt[$area]);
			$avg_level = round($avg, 0);
			$border = '';
			if($area == 2 || $area == 7 || $area == 9 || $area == 11 || $area == 13)
				$border = 'border-right: 2px solid #000000;';
			echo '<td style="padding: 3px 0px 2px 0px;'.$border.'text-align: center; background-color: #'.$diff_colors_dechex[$avg_level].'; box-shadow: inset 0 0 0 1000px #'.$diff_colors_dechex[$avg_level].'; color: #'.($diff_colors_bg[$avg_level] < 128 ? 'fff' : '000').'; -ms-filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=\'#'.$diff_colors_dechex[$avg_level].'\', endColorstr=\'#'.$diff_colors_dechex[$avg_level].'\'); /* IE8 */"><span style="text-align: center; background-color: inherit; color: #'.($diff_colors_bg[$avg_level] < 128 ? 'fff' : '000').';">'.str_replace('.', ',<span style="font-size: 12px;">', round($avg, 1)).'</span></td>';
		} else
			echo '<td></td>';
	}
	echo '</tr></table>';
}










?>
<div<?=(isset($_GET['area']) ? '' : ' class="no-print"')?>>
<h2><!-- Grupės vaikai / Grupei  Vaikų grupė (daug) -->Visi grupės vaikai, bet viena pasiekimų sritis (patogiau įvertinti)</h2>
<form method="get">
	<!-- <div style="float: left; line-height: 32px;">Visai grupei:&nbsp;</div> -->
	<div class="sel" style="margin-right: 5px; float: left"><select name="area">
	<?php
	foreach($achievements_areas as $id => $val)
		echo "<option value=\"".$id."\"".(isset($_GET['area']) && $_GET['area'] == $id ? ' selected="selected" class="selected">'.$selectedMark : '>').$val."</option>";
	?>
	</select></div>
	<div style="float: left">
	<input id="showForGroup" class="no-print filter" type="submit" value="Išskleisti">
	
	
	<?php
	if(!ADMIN && $edit_save_delete) 
		echo '<input type="submit" id="newForGroup" class="no-print" value="Vertinsiu pasiekimus">';//Įvertinti pasiekimus //type="submit" name...
	?>
	</div>
	<div class="date-dialog" style="display: none; float: left; margin-left: 7px;">
		<?=$suggested_date?>
		<input type="text" name="date" class="datepicker" disabled title="Pasirinkite datą vertinimui">
		<input type="submit" name="newForGroup" class="no-print submit" style="margin:0" value="Į pasiekimų vertinimą"><!-- Pereiti -->
	</div>
	<div class="cl"></div>
</form>
</div>

<?php
if((isset($_GET['newForGroup']) || isset($_GET['editForGroup'])) && $edit_save_delete) {
	include 'new_achievements_descriptions.php';
	$mark_date = isset($_GET['editForGroup']) ? $_GET['editForGroup'] : $_GET['date'];
	?>
	<fieldset id="new-report-form">
	<legend><?=(isset($_GET['editForGroup']) ? "Grupės vertinimo redagavimas" : "Naujas grupės vertinimas")?></legend>
		<form <?=''//(isset($_GET['group_id']) ? ' action="?group_id='.(int)$_GET['group_id'].'"' : '')?>method="post" class="not-saved-reminder">
			<p>Data: <input readonly="readonly" class="dateInputReadOnly" required="required" type="text" name="date" value="<?=filterText($mark_date)?>"></p>
			<table>
				<tr>
					<th colspan="4">Pasiekimų sritis - <span data-id="dialog-<?=(int)$_GET['area']?>" class="href dialog-opener">Žiūrėti aprašymą - <?=$achievements_areas[(int)$_GET['area']]?></span></th>
				</tr>
				<tr>
					<th>Vaikas</th>
					<th>Įvertinimas</th>
					<th>Užrašai</th>
					<th>Trinti</th>
				</tr>
				<?php
				/*
				if(ADMIN)
	$result = db_query("SELECT cr.* -- , COUNT(*) `cnt`
	FROM `".DB_children."` cr
	-- LEFT JOIN `".DB_children."` fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`<fi.`valid_from` AND cr.`valid_from`<=CURDATE() AND fi.`valid_from`<=CURDATE()
	JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<=CURDATE() GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
	JOIN `".DB_documents."` ON cr.`parent_kid_id`=`".DB_documents."`.vaiko_id
	WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 -- AND fi.`valid_from` IS NULL
	GROUP BY cr.parent_kid_id
	ORDER BY cr.`vardas` ASC, cr.`pavarde` ASC");
else
	$result = db_query("SELECT cr.*
	FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<=CURDATE() GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
	JOIN `".DB_documents."` ON cr.`parent_kid_id`=`".DB_documents."`.vaiko_id
	WHERE cr.`isDeleted`=0 AND cr.`grupes_id`=".GROUP_ID." AND cr.`archyvas`=0
	GROUP BY cr.parent_kid_id
	ORDER BY cr.`vardas` ASC, cr.`pavarde` ASC");
	
	$result = db_query("SELECT cr.* FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<='".db_fix($mark_date)."' GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
	WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 AND cr.`grupes_id`=".GROUP_ID."
	ORDER BY ".orderName('cr', $mark_date));//(int)$row['grup_id']//cr.`vardas`, cr.`pavarde`
				*/
				//DRY: sveikatos_pastabos.php
				//$result = db_query(getKidsSql(GROUP_ID));
				$result = db_query("SELECT cr.*, `".DB_kid_level."`.*
	FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<='".db_fix($mark_date)."' GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
	LEFT JOIN `".DB_kid_level."` ON cr.`parent_kid_id`=`".DB_kid_level."`.kid_id AND `".DB_kid_level."`.`kindergarten_id`=".DB_ID." AND `".DB_kid_level."`.`area`=".(int)$_GET['area']." AND `".DB_kid_level."`.`date`='".db_fix($mark_date)."'
	WHERE cr.`isDeleted`=0 AND cr.`grupes_id`=".GROUP_ID." AND cr.`archyvas`=0
	-- GROUP BY cr.parent_kid_id
	ORDER BY ".orderName('cr', $mark_date));//cr.`vardas` ASC, cr.`pavarde` ASC");
				while($row = mysqli_fetch_assoc($result)) {
					echo "<tr>
						<td>".filterText(getName($row['vardas'], $row['pavarde']))."</td>
						<td>";
						for($i = 1; $i <= 7; ++$i)
							echo "<button type=\"button\" class=\"progress-selection".(isset($row['level']) && $row['level'] == $i ? ' selected-level' : '')."\">$i</button> ";//1–7. Pasirinktas turėtų pariebėti
					echo "	<input type=\"hidden\" name=\"achievement[".$row['parent_kid_id']."]\"".(isset($row['level']) ? ' value="'.(int)$row['level'].'"' : '')."></td>
						<td><textarea name=\"notes[".$row['parent_kid_id']."]\" style=\"height: 42px;\">".(isset($row['notes']) ? filterText($row['notes']) : '')."</textarea></td>
						".(isset($row['level']) ? "<td><a href='?area=".(int)$row['area']."&amp;editForGroup=".filterText($mark_date)."&amp;delete=".filterText($row['ID'])."' onclick=\"return confirm('Ar tikrai norite ištrinti?')\">Trinti</a></td>" : '<td></td>')."
					</tr>";
					$res = $row;
				}
				?>
			</table>
			<?php
			if(KESV && !empty($res['created']) && !isset($_GET['print'])) {
				echo 'Sukurta: &nbsp;'.$res['created'].' '.getAllEmployees($res['createdByEmployeeId']).';<br>Pakeista: '.$res['updated'].' '. getAllEmployees($res['updatedByEmployeeId']);
			}
			?>
			<p><input type="hidden" name="area" value="<?=(isset($_GET['area']) ? (int)$_GET['area'] : '')?>"><input type="submit" name="updateGroup" value="<?=(isset($_GET['editForGroup']) ? 'Atnaujinti' : 'Išsaugoti įvertinimus')?>" class="submit"></p>
		</form>
	</fieldset>
	<?php
}



if(isset($_GET['area']) && !(isset($_GET['newForGroup']) || isset($_GET['editForGroup']))) {
	$result = db_query("SELECT *, COUNT(*) `cnt` FROM `".DB_kid_level."` WHERE `kindergarten_id`=".DB_ID." AND `group_id`=".GROUP_ID." AND `area`='".(int)$_GET['area']."' GROUP BY `date` ORDER BY `date` DESC");
	//echo '<div style="margin-left: 50px;">';
	if(mysqli_num_rows($result) > 0) {
		$list = [];
		while ($row = mysqli_fetch_assoc($result))
			$list[] = $row;
		//include 'new_achievements_descriptions.php';
		?>
		<!-- Pasiekimų sritis - <span data-id="dialog-<?=(int)$_GET['area']?>" class="href dialog-opener">Žiūrėti aprašymą - <?=$achievements_areas[(int)$_GET['area']]?></span> -->
		<form method="get" style="margin-top: 7px;">
			<div style="float: left; line-height: 32px; margin-left: 25px; margin-right: 5px;">Nustatymai diagramai:</div> <input type="hidden" name="area" value="<?=(int)$_GET['area']?>">
			<div style="float: left; margin-right: 5px;">
				<div class="sel primary-data-group"><select name="date">
				<?php
				foreach($list as $row)
					echo "<option value=\"".$row['date']."\"".(isset($_GET['date']) && $_GET['date'] == $row['date'] ? ' selected="selected" class="selected">'.$selectedMark : '>').$row['date']."</option>";
				?>
				</select></div>
				<div class="primary-data-label" title="Diagramos pagrindinės kreivės spalva">juoda -</div>
			</div>
			<div style="float: left; margin-right: 5px;">
				<div class="sel secondary-data-group"><select name="dateWith">
				<?php
				echo "<option value=\"\"".(isset($_GET['dateWith']) && $_GET['dateWith'] == '' ? ' selected="selected" class="selected">'.$selectedMark : '>')."Be antros datos</option>";//Nelyginti su kita data
				foreach($list as $row)
					echo "<option value=\"".$row['date']."\"".(isset($_GET['dateWith']) && $_GET['dateWith'] == $row['date'] ? ' selected="selected" class="selected">'.$selectedMark : '>').$row['date']."</option>";
				?>
				</select></div>
				<div class="secondary-data-label" title="Diagramos antros kreivės spalva">žalia -</div>
			</div>
			<div class="sel no-print " style="float: left; margin-right: 5px;"><select name="chart_type">
			<?php
			foreach($chart_types as $id => $val)
				echo "<option value=\"".$id."\"".(isset($_GET['chart_type']) && $_GET['chart_type'] == $id ? ' selected="selected" class="selected">'.$selectedMark : '>').$val."</option>";
			?>
			</select></div>
			<input class="no-print filter fl" type="submit" id="view-group" value="Braižyti">
		</form>
		
		<?php
		if(isset($_GET['chart_type'])) {
			$all_kids = array();
			$kids = array();
			//$result = db_query("SELECT * FROM `".DB_kid_level."` WHERE `kindergarten_id`=".DB_ID." AND `group_id`=".(int)GROUP_ID." AND `date`='".db_fix($_GET['date'])."' AND `area`='".(int)$_GET['area']."'");
			$result = db_query("SELECT cr.*, `".DB_kid_level."`.*
			FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<='".db_fix($_GET['date'])."' GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
			LEFT JOIN `".DB_kid_level."` ON cr.`parent_kid_id`=`".DB_kid_level."`.kid_id AND `".DB_kid_level."`.`kindergarten_id`=".DB_ID." AND `".DB_kid_level."`.`area`=".(int)$_GET['area']." AND `".DB_kid_level."`.`date`='".db_fix($_GET['date'])."'
			WHERE cr.`isDeleted`=0 AND cr.`grupes_id`=".GROUP_ID." AND cr.`archyvas`=0
			-- GROUP BY cr.parent_kid_id
			ORDER BY ".orderName('cr', $_GET['date']));
			while ($row = mysqli_fetch_assoc($result)) {
				$all_kids[$row['parent_kid_id']] = getName($row['vardas'], $row['pavarde']);
				$kids[$row['parent_kid_id']] = $row;
			}
			
			$kids2 = [];
			if(!empty($_GET['dateWith'])) {
				//$result = db_query("SELECT * FROM `".DB_kid_level."` WHERE `kindergarten_id`=".DB_ID." AND `group_id`=".(int)GROUP_ID." AND `date`='".db_fix($_GET['dateWith'])."' AND `area`='".(int)$_GET['area']."'");
				$result = db_query("SELECT cr.*, `".DB_kid_level."`.*
				FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<='".db_fix($_GET['dateWith'])."' GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
				LEFT JOIN `".DB_kid_level."` ON cr.`parent_kid_id`=`".DB_kid_level."`.kid_id AND `".DB_kid_level."`.`kindergarten_id`=".DB_ID." AND `".DB_kid_level."`.`area`=".(int)$_GET['area']." AND `".DB_kid_level."`.`date`='".db_fix($_GET['dateWith'])."'
				WHERE cr.`isDeleted`=0 AND cr.`grupes_id`=".GROUP_ID." AND cr.`archyvas`=0
				-- GROUP BY cr.parent_kid_id
				ORDER BY ".orderName('cr', $_GET['dateWith']));
				while ($row = mysqli_fetch_assoc($result)) {
					$all_kids[$row['parent_kid_id']] = getName($row['vardas'], $row['pavarde']);
					$kids2[$row['parent_kid_id']] = $row;
				}
			}
			asort($all_kids, SORT_LOCALE_STRING);
			
			echo '<br>';
			$data1 = '';
			foreach($all_kids as $id => $name)
				$data1 .= (isset($kids[$id]) ? (int)$kids[$id]['level'].',' : '0,');
			$data2 = '';
			if(!empty($_GET['dateWith'])) {
				foreach($all_kids as $id => $name)
					$data2 .= (isset($kids2[$id]) ? (int)$kids2[$id]['level'].',' : '0,');
			}
			$labels = implode('","', $all_kids);
			echo canvas($labels, $data1, $data2);
		}
		if(!isset($_GET['chart_type']) && $edit_save_delete) {
			?>
			<table>
			<caption><strong>Jau įvertinti pasiekimai:</strong></caption>
			<tr>
				<th class="date-cell">Data</th>
				<th class="no-print">Veiksmas</th>
			</tr>
			<?php
			//while ($row = mysqli_fetch_assoc($result)) {
			foreach($list as $row) {
				echo "\t<tr>
				<td>".$row['date']."</td>
				<td class=\"no-print\">";
			
				echo "<a href='?area=".(int)$row['area']."&amp;editForGroup=".filterText($row['date'])."'>Keisti</a>";
				if($row['createdByEmployeeId'] == DARB_ID || ADMIN)
					echo " <a href='?area=".(int)$row['area']."&amp;deleteGroupDate=".filterText($row['date'])."' onclick=\"return confirm('Ar tikrai norite ištrinti?')\">Trinti</a>";
				echo "</td></tr>";
			}
			echo "</table>";
		}
	} else
		echo 'Nėra išsaugotų įvertinimų.';//Grupės vaikai dar neįvertinti
	//echo '</div>';
}
?>
















<!--
<br>
<hr>
<br> -->
<div<?=(isset($_GET['kid_id']) ? '' : ' class="no-print"')?>>
<h2>Vienas vaikas, bet visos pasiekimų sritys</h2>
<form method="get">
	<!-- <div style="float: left; line-height: 32px;">Vaikui:&nbsp;</div> -->
	<div class="sel" style="float: left; margin-right: 5px;"><select id="kid_id" name="kid_id">
		<?php
		
		//DRY: sveikatos_pastabos.php
		if(ADMIN)
			$result = db_query(getKidsSql(GROUP_ID));
			//"SELECT * FROM `".DB_children."` WHERE".(isset($_GET['grupes_id']) && $_GET['grupes_id'] != 0 ? " grupes_id=".(int)$_GET['grupes_id']." AND" : '')." `isDeleted`=0 AND `archyvas`=0 AND `parent_kid_id`=0 ORDER BY vardas, pavarde"
		else
			$result = db_query($get_kids_sql);
			//"SELECT * FROM `".DB_children."` WHERE `grupes_id`=".GROUP_ID." AND `isDeleted`=0 AND `archyvas`=0 AND `parent_kid_id`=0 ORDER BY vardas, pavarde"
			//JOIN ".DB_employees_groups." ON ".DB_children.".grupes_id=".DB_employees_groups.".grup_id WHERE ".DB_employees_groups.".darb_id=".(int)DARB_ID."
		$kid = '';
		$selectedKid = '';
		$gim_data = '';
		while($row = mysqli_fetch_assoc($result)) {
			$kid = filterText(getName($row['vardas'], $row['pavarde']));
			$isSelected = isset($_GET['kid_id']) && $_GET['kid_id'] == $row['parent_kid_id'];
			if($isSelected) {
				$selectedKid = $kid;
				$gim_data = $row['gim_data'];
			}
			echo "<option value=\"".$row['parent_kid_id']."\"".($isSelected ? ' selected="selected" class="selected">'.$selectedMark : '>').$kid."</option>";
		}
		?>
	</select></div>
	<div style="float: left">
	<input class="no-print filter" type="submit" id="showKids" value="Išskleisti"><?php //Rodyti sąrašą; Rodymas, keitimas, trynimas ?>
	<?php
	if(!ADMIN && $edit_save_delete) 
		echo '<input type="submit" id="newForKid" class="no-print" value="Vertinsiu pasiekimus">';//Įvertinti pasiekimus //type="submit" name...
	?>
	</div>
	<div class="date-dialog" style="display: none; float: left; margin-left: 7px;">
		<?=$suggested_date?>
		<input type="text" name="date" class="datepicker" disabled title="Pasirinkite datą vertinimui">
		<input type="submit" name="newForKid" class="no-print submit" style="margin:0" value="Į pasiekimų vertinimą"><!-- Pereiti -->
	</div>
</form>
<div class="cl"></div>
</div>



<?php
if(isset($_GET['kid_id']) && !(isset($_GET['newForKid']) || isset($_GET['edit']) || isset($_GET['view']))) {
	$result = db_query("SELECT * FROM `".DB_kid_level."` WHERE `kindergarten_id`=".DB_ID." AND `kid_id`=".(int)$_GET['kid_id']." GROUP BY `date` ORDER BY `date` DESC");
	//echo '<div style="margin-left: 50px;">';
	if(mysqli_num_rows($result) > 0) {
		if(!empty($selectedKid)) {
			$additional = '';
			if(!empty($gim_data)) {
				$start_date = new DateTime($gim_data);
				$since_start = $start_date->diff(new DateTime());
				$additional = ' '.$gim_data.' (dabar t. y. '.date('Y-m-d').' vaikui '.$since_start->y.' m. '.$since_start->m.' mėn.)';
			}
			echo '<h3 style="margin-left: 25px;">'.filterText(/*$l->getName(*/$selectedKid/*, 'nau')*/).$additional.':</h3>';
		}
	
		$list = [];
		while ($row = mysqli_fetch_assoc($result))
			$list[] = $row;
		?>
		<form method="get" style="margin-top: 7px;">
			<div style="float: left; line-height: 32px; margin-left: 25px; margin-right: 5px;">Nustatymai vaiko diagramai:</div> <input type="hidden" name="kid_id" value="<?=(int)$_GET['kid_id']?>">
			<div style="float: left; margin-right: 5px;">
				<div class="sel"><select name="date">
				<?php
				foreach($list as $row)
					echo "<option value=\"".$row['date']."\"".(isset($_GET['date']) && $_GET['date'] == $row['date'] ? ' selected="selected" class="selected">'.$selectedMark : '>').$row['date']."</option>";
				?>
				</select></div>
				<div class="primary-data-label" title="Diagramos pagrindinės kreivės spalva">juoda - </div>
			</div>
			<div style="float: left; margin-right: 5px;">
				<div class="sel"><select name="dateWith">
				<?php
				echo "<option value=\"\"".(isset($_GET['dateWith']) && $_GET['dateWith'] == '' ? ' selected="selected" class="selected">'.$selectedMark : '>')."Be antros datos</option>";
				foreach($list as $row)
					echo "<option value=\"".$row['date']."\"".(isset($_GET['dateWith']) && $_GET['dateWith'] == $row['date'] ? ' selected="selected" class="selected">'.$selectedMark : '>').$row['date']."</option>";
				?>
				</select></div>
				<div class="secondary-data-label" title="Diagramos antros kreivės spalva">žalia -</div>
			</div>
			<div class="sel no-print" style="float: left; margin-right: 5px;"><select name="chart_type">
			<?php
			foreach($chart_types as $id => $val)
				echo "<option value=\"".$id."\"".(isset($_GET['chart_type']) && $_GET['chart_type'] == $id ? ' selected="selected" class="selected">'.$selectedMark : '>').$val."</option>";
			?>
			</select></div>
			<input class="no-print filter fl" type="submit" value="Braižyti">
		</form>
		
		<?php
		if(isset($_GET['date'])) {
			$result = db_query("SELECT * FROM `".DB_kid_level."` WHERE `kindergarten_id`=".DB_ID." AND `kid_id`=".(int)$_GET['kid_id']." AND `date`='".db_fix($_GET['date'])."'");
			$level = array();
			while ($row = mysqli_fetch_assoc($result))
				$level[$row['area']] = $row['level'];
			$level2 = array();
			if(!empty($_GET['dateWith'])) {
				$result = db_query("SELECT * FROM `".DB_kid_level."` WHERE `kindergarten_id`=".DB_ID." AND `kid_id`=".(int)$_GET['kid_id']." AND `date`='".db_fix($_GET['dateWith'])."'");
				while ($row = mysqli_fetch_assoc($result))
					$level2[$row['area']] = $row['level'];
			}
			?>
			<br>
			<?php
			$data1 = '';
			foreach($achievements_areas as $area => $title)
				$data1 .= (isset($level[$area]) ? $level[$area].',' : '0,');
			$data2 = '';
			if(!empty($_GET['dateWith'])) {
				foreach($achievements_areas as $area => $title)
					$data2 .= (isset($level2[$area]) ? $level2[$area].',' : '0,');
			}
			echo canvas(implode('","', $achievements_areas), $data1, $data2);
		}
		
		if(!isset($_GET['chart_type']) && $edit_save_delete) {
			?>
			<table>
			<caption><strong>Jau įvertinti pasiekimai:</strong></caption>
			<tr>
				<th class="date-cell">Data</th>
				<th class="no-print">Veiksmas</th>
			</tr>
			<?php
			//while ($row = mysqli_fetch_assoc($result)) {
			foreach($list as $row) {
				echo "\t<tr>
				<td>".$row['date']."</td>
				<td class=\"no-print\">";
					echo "<a href='?kid_id=".(int)$row['kid_id']."&amp;view=".filterText($row['date'])."#new-report-form'>Peržiūrėti</a>
						<a href='?kid_id=".(int)$row['kid_id']."&amp;edit=".filterText($row['date'])."#new-report-form'>Keisti</a>";
					if($row['createdByEmployeeId'] == DARB_ID || ADMIN)
						echo " <a href='?kid_id=".(int)$row['kid_id']."&amp;delete=".filterText($row['date'])."' onclick=\"return confirm('Ar tikrai norite ištrinti?')\">Trinti</a>";
				echo "</td></tr>";
			}
			echo "</table>";
		}
	} else 
		echo 'Nėra išsaugotų įvertinimų.';
	//echo '</div>';
}

if((isset($_GET['newForKid']) || isset($_GET['edit']) || isset($_GET['view'])) && $edit_save_delete) {
	$date = (isset($_GET['edit']) ? $_GET['edit'] : (isset($_GET['newForKid']) ? $_GET['date'] : $_GET['view']));
	$result = db_query("SELECT * FROM `".DB_kid_level."` WHERE `kindergarten_id`=".DB_ID." AND `kid_id`=".(int)$_GET['kid_id']." AND `date`='".db_fix($date)."'");
	$level = [];
	$notes = [];
	$res = [];
	while ($row = mysqli_fetch_assoc($result)) {
		$level[$row['area']] = $row['level'];
		$notes[$row['area']] = $row['notes'];
		$res = $row;
	}
	include 'new_achievements_descriptions.php';
	?>
	<fieldset id="new-report-form">
	<legend><?=(isset($_GET['edit']) ? "Vertinimo redagavimas" : (isset($_GET['view']) ? "Vaiko pasiekimo sričių įvertinimai" : "Naujas vertinimas"))?></legend>
		<form<?=''//(isset($_GET['kid_id']) ? ' action="?kid_id='.(int)$_GET['kid_id'].'"' : '')?> method="post" class="not-saved-reminder">
			<p>Data <?php
		if(!isset($_GET['view'])) {
			?><input readonly="readonly" class="dateInputReadOnly" required="required" type="text" name="date" value="<?=filterText($date)?>"><?php
		} else
			echo filterText($_GET['view']);
		?></p>
		<table>
			<tr>
				<th>Pasiekimų sritis</th>
				<th>Įvertinimas</th>
				<th>Užrašai</th>
			</tr>
			<?php
			if(isset($_GET['view'])) {
				foreach($achievements_areas as $area => $title) {
					echo "<tr>
						<td><span data-id=\"dialog-$area\" class=\"href dialog-opener\" title=\"Žiūrėti aprašymą\">".$title."</span></td>
						<td>";
						for($i = 1; $i <= 7; ++$i)
							echo (isset($level[$area]) && $level[$area] == $i ? $i : '');
					echo "</td>
						<td>".(isset($notes[$area]) ? filterText($notes[$area]) : '')."</td>
					</tr>";
				}
			} else {
				foreach($achievements_areas as $area => $title) {
					echo "<tr>
						<td><span data-id=\"dialog-$area\" class=\"href dialog-opener\" title=\"Žiūrėti aprašymą\">".$title."</span></td>
						<td>";
						for($i = 1; $i <= 7; ++$i)
							echo "<button type=\"button\" class=\"progress-selection".(isset($level[$area]) && $level[$area] == $i ? ' selected-level' : '')."\">$i</button> ";//1–7. Pasirinktas turėtų pariebėti
					echo "	<input type=\"hidden\" name=\"achievement[".$area."]\"".(isset($level[$area]) ? ' value="'.(int)$level[$area].'"' : '')."></td>
						<td><textarea name=\"notes[".$area."]\" style=\"height: 42px;\">".(isset($notes[$area]) ? filterText($notes[$area]) : '')."</textarea></td>
					</tr>";
				}
			}
			?>
		</table>
		<?php
		if(KESV && !empty($res['created']) && !isset($_GET['print'])) {
			echo 'Sukurta: &nbsp;'.$res['created'].' '.getAllEmployees($res['createdByEmployeeId']).';<br>Pakeista: '.$res['updated'].' '.getAllEmployees($res['updatedByEmployeeId']);
		}
		if(!isset($_GET['view'])) { ?>
			<p><?=''/*(isset($_GET['edit']) ? '<input type="hidden" name="old_date" value="'.filterText($_GET['edit']).'">' : '')*/?><input type="submit" name="update" value="<?=(isset($_GET['edit']) ? 'Atnaujinti' : 'Išsaugoti įvertinimus')?>" class="submit no-print"></p><?php
		} ?>
		</form>
	</fieldset>
	<?php
}
?>
</div>
<!-- <script src="/libs/jquery.dialogextend.min.js"></script> -->
