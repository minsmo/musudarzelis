<!-- 	<script type="text/javascript" src="http://musudarzelis.lt/libs/jquery-1.11.3.min.js"></script> -->
	<link rel="stylesheet" href="/libs/fancyBox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen">
	<script type="text/javascript" src="/libs/fancyBox/jquery.fancybox.pack.js?v=2.1.5"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		$(".fancybox").fancybox({
			openEffect	: 'none',
			closeEffect	: 'none',
			tpl: {
				error    : '<p class="fancybox-error">Užklaustas turinys negali būti užkrautas.<br>Prašome bandyti dar kartą vėliau.</p>',
				closeBtn : '<a title="Uždaryti" class="fancybox-item fancybox-close" href="javascript:;"></a>',
				next     : '<a title="Kitas" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
				prev     : '<a title="Ankstesnis" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
			}

		});
	});
	</script>
<h1>Naudinga informacija</h1>
<div id="content">
<h2>Naudingos nuorodos</h2>
<h3>Geros nuorodos</h3>
<ul>
	<li><a target="_blank" href="http://www.ikimokyklinis.lt/">Ikimokyklinis.lt</a></li>
	<li><a target="_blank" href="http://www.upc.smm.lt/">Ugdymo plėtotės centras</a></li>
	<li><a target="_blank" href="http://www.lyderiulaikas.smm.lt/">Lyderių laikas</a></li>
</ul>
<h3>Svarbiausias reglamentas</h3>
<ul>
	<li><a target="_blank" href="https://www.e-tar.lt/portal/lt/legalAct/TAR.47BB952431DA/bHVIIlKgQy">LR Konstitucija</a></li>
	<li><a target="_blank" href="https://www.e-tar.lt/portal/lt/legalAct/TAR.8A39C83848CB/GoZTrTvqvS">Civilinis kodeksas</a></li>
	<?php // https://lt.wikipedia.org/wiki/Teis%C4%97s_kodeksas
	//https://www.e-tar.lt/portal/lt/legalAct/TAR.31185A622C9F/JFHLvHtKgB ?>
	<li><a target="_blank" href="https://www.e-tar.lt/portal/lt/legalAct/TAR.9A3AD08EA5D0/obKiqknvql">LR švietimo įstatymas</a></li>
	<li><a target="_blank" href="https://www.e-tar.lt/portal/legalAct.html?documentId=b1fb6cc089d911e397b5c02d3197f382">Valstybinės švietimo 2013-2022 metų strategija</a></li>
	<li><a target="_blank" href="https://www.e-tar.lt/portal/lt/legalAct/TAR.1882ABF8B6AB">Vaiko gerovės valstybės politikos koncepcija</a></li>
	<li><a target="_blank" href="https://www.e-tar.lt/portal/lt/legalAct/TAR.D0CD0966D67F/GDAmujogsL">LR vietos savivaldos įstatymas</a></li>
</ul>
<h3><span class="abbr" title="Švietimo ir mokslo ministerija">ŠMM</span>, tėvams</h3>
<ul>
	<li><a target="_blank" href="http://www.smm.lt/web/lt/tevams/ka-svarbu-zinoti">Ką svabu žinoti tėvams</a></li>
	<li><a target="_blank" href="http://www.smm.lt/web/lt/tevams/ikimokyklinis-ugdymas">Ikimokyklinis ugdymas</a></li>
	<li><a target="_blank" href="http://www.smm.lt/web/lt/tevams/priesmokyklinis-ugdymas">Priešmokyklinis ugdymas</a></li>
	<li><a target="_blank" href="http://www.smm.lt/web/lt/tevams/pagrindinis-ugdymas">Pradinis ugdymas</a></li>
</ul>
<?php
if(startsWith(DB_PREFIX, 'pnv') && !startsWith(DB_PREFIX, 'pnvr')) {
	?>
	<h3>Panevėžio miesto savivaldybės</h3>
	<ul>
		<li><a target="_blank" href="http://195.182.86.148/aktai/Default.aspx?Id=2&tekstas=ikimokyklinio">Teisės aktai</a></li>
		<li><a target="_blank" href="http://panevezys.lt/lt/veikla/veiklos-sritys/svietimas-237/neformalus-vaiku-svietimas.html">Skyrius - NEFORMALUS VAIKŲ ŠVIETIMAS</a></li>
	</ul>
	<?php
}

if(startsWith(DB_PREFIX, 'kns') && !startsWith(DB_PREFIX, 'knsr')) {
	?>
	<h3>Kauno miesto savivaldybės</h3>
	<ul>
		<li><a target="_blank" href="http://www.kaunas.lt/svietimas/seimoms-auginancioms-ikimokyklinio-amziaus-vaikus/">Informacija šeimoms, auginančioms ikimokyklinio amžiaus vaikus. Su PATARIMAIS ir ŽAIDIMAIS</a></li>
		<li><a target="_blank" href="http://www.kaunas.lt/svietimas/ikimokyklinis-ugdymas/">Skyrius - Ikimokyklinis ugdymas</a></li>
		<li><a target="_blank" href="http://www.kaunas.lt/svietimas/priesmokyklinis-ugdymas/">Skyrius - Priešmokyklinis ugdymas</a></li>
		<li><a target="_blank" href="http://www.kaunas.lt/wp-content/uploads/sites/8/2015/04/nevalstybini%C5%B3%C4%AFstaig%C5%B3mokestistvarkosapra%C5%A1asinternetui.pdf">Kompensavimo tvarka tėvams leidžiantiems vaikus į nevalstybines ikimokyklinio ugdymo įstaigas</a></li>
		<li><a target="_blank" href="https://www.e-tar.lt/portal/lt/legalAct/SAV.503438/CTeKuJigXj">Centralizuoto vaikų priėmimo į Kauno m. sav. įsteigtų biudžetinių švietimo įstaigų ikimokyklinio ir priešmokyklinio ugdymo grupes tvarkos aprašas</a></li>
		<li><a target="_blank" href="https://www.e-tar.lt/portal/lt/legalAct/SAV.503556/cWEoehELSl">Atlyginimo dydžio už vaikų, ugdomų pagal ikimokyklinio ir (ar) priešmokyklinio ugdymo programas, išlaikymą nustatymo ir mokėjimo tvarkos aprašas</a></li>
		<li><a target="_blank" href="https://www.e-tar.lt/portal/lt/legalAct/SAV.503485/qMgaHDDRfy">Maitinimo paslaugų teikimas Kauno m. sav. įsteigtų švietimo įstaigų ikimokyklinėse ir priešmokyklinėse grupėse</a></li>
	</ul>
	<?php
}
?>
<h3><span class="abbr" title="Švietimo ir mokslo ministerija">ŠMM</span></h3>
<ul>
	<li><a target="_blank" href="https://www.e-tar.lt/portal/lt/legalAct?documentId=7e9cee00eb4611e58deaaf0783ebf65b">Dienynų sudarymo elektroninio dienyno duomenų pagrindu tvarkos aprašas</a></li>
</ul>
<h3><span class="abbr" title="Švietimo ir mokslo ministerija">ŠMM</span>, ikimokyklinis</h3>
<ul>
	<li><a target="_blank" href="https://www.smm.lt/uploads/documents/Pedagogams/ikimok_pasiekimu_aprasas.pdf"><strong>Ikimokyklinio amžiaus vaikų pasiekimų aprašas</strong>. Švietimo ir mokslo ministerijos Švietimo aprūpinimo centras. 2014 m.</a></li>
	<li><a target="_blank" href="http://www.ikimokyklinis.lt/uploads/files/dir1049/dir52/dir2/17_0.php"><strong>Ikimokyklinio ugdymo metodinės rekomendacijos</strong>. Švietimo ir mokslo ministerijos Švietimo aprūpinimo centras. 2015 m.</a></li>
</ul>
Iš <a target="_blank" href="http://www.ikimokyklinis.lt/index.php/ipup-projektas/projekto-rezultatai/11677">IPUP (Ikimokyklinio ir priešmokyklinio ugdymo plėtra) projekto rezultatai</a>
<h3><span class="abbr" title="Švietimo ir mokslo ministerija">ŠMM</span>, priešmokyklinis</h3>
<ul>
	<li><a target="_blank" href="https://www.smm.lt/uploads/documents/Prie%C5%A1mokyklinio%20ugdymo%20bendroji%20programa(3).pdf"><strong>Priešmokyklinio
ugdymo bendroji programa. 2014 m.</strong>. Švietimo ir mokslo ministerijos Švietimo aprūpinimo centras. 2014 m.</a></li>
	<li><a target="_blank" href="https://www.e-tar.lt/portal/lt/legalAct/TAR.3BD8DC009FDA/VWiawOEyMc">Priešmokyklinio ugdymo tvarkos aprašas</a></li>
</ul>
<?php
/*
https://www.e-tar.lt/portal/lt/legalAct/TAR.A4A7F7C56568/wHvcEstHvj Švietimo įstaigų darbuotojų ir kitų įstaigų pedagoginių darbuotojų darbo apmokėjimo tvarkos aprašas
https://www.e-tar.lt/portal/lt/legalAct/TAR.84D7BEFD0F2A/eHvCOLqGbK Bendrojo ugdymo tarybos nuostatai
https://www.e-tar.lt/portal/lt/legalAct/TAR.0773CD0EBED3/fQwCzZWWpI Visuomenės sveikatos priežiūros specialisto, vykdančio sveikatos priežiūrą ikimokyklinio ugdymo įstaigoje, kvalifikacinių reikalavimų aprašas
https://www.e-tar.lt/portal/lt/legalAct/08080d00b2a911e48296d11f563abfb0/LusDknVocF Dėl 2014–2020 metų Europos Sąjungos fondų investicijų veiksmų programos 9 prioriteto „Visuomenės švietimas ir Žmogiškųjų išteklių potencialo didinimas“ 9.1.3 konkretaus uždavinio „Padidinti bendrojo ugdymo ir neformaliojo švietimo įstaigų (ypač vykdančių ikimokyklinio ir priešmokyklinio ugdymo programas) tinklo veiklos efektyvumą“ projektų finansavimo sąlygų aprašo Nr. 1 patvirtinimo
https://www.e-tar.lt/portal/lt/legalAct/98d1f1d0ece111e4927fda1d051299fb Dėl tėvų atstovų dalyvavimo neplaniniuose ikimokyklinio ir bendrojo ugdymo įstaigų maisto tvarkymo skyrių patikrinimuose
*/
?>
<h3>LR sveikatos apsaugos ministerija</h3>
<ul>
	<li><a target="_blank" href="<?php if(date('Y-m-d') >= '2016-09-01') echo 'https://www.e-tar.lt/portal/lt/legalAct/TAR.AF02472A1EBF/LCRkRWMidR'; else echo 'https://www.e-tar.lt/portal/lt/legalAct/TAR.AF02472A1EBF/BfIYhvFYnF' ?>">Suvestinė redakcija - Lietuvos higienos norma HN 75:2010 „Įstaiga, vykdanti ikimokyklinio ir (ar) priešmokyklinio ugdymo programą. Bendrieji sveikatos saugos reikalavimai“</a><!--
	https://www.e-tar.lt/portal/lt/legalAct/TAR.AF02472A1EBF/uRqaIKBrEk
	<a target="_blank" href="https://www.e-tar.lt/portal/lt/legalAct/8fb4d450f2d711e3bb22becb572235f5"><strong>Įsakymas įsigaliojantis nuo 2016-01-01, papildo Lietuvos higienos normą HN 75:2010 „Įstaiga, vykdanti ikimokyklinio ir (ar) priešmokyklinio ugdymo programą. Bendrieji sveikatos saugos reikalavimai“</strong>, <...> ir 94 punktą išdėsto taip:  „94. Priimant vaiką į įstaigą ir vėliau kiekvienais metais turi būti pateiktas Vaiko sveikatos pažymėjimas (forma Nr. 027-1/a) [4.5, 4.10]. <strong>Jeigu pažymėjime nurodyta, kad vaikas nepaskiepytas pagal Lietuvos Respublikos sveikatos apsaugos ministro patvirtintą Lietuvos Respublikos vaikų profilaktinių skiepijimų kalendorių nuo tymų, raudonukės ir poliomielito, nesant skiepų kontraindikacijų, į įstaigą toks vaikas nepriimamas.</strong>“</a><br><a href="https://www.e-tar.lt/portal/lt/legalAct/cf1f5ab0c4c811e583a295d9366c7ab3">2016-05-01 įsigalios nauja suvestinė redakcija.</a> --><ul><li><a href="https://www.e-tar.lt/portal/lt/legalAct/ed586880177711e68eb0b4a9a30fc97f" target="_blank">Nuo 2016-09-01 įsigalios naujas pakeitimas.</a></li></ul></li>
	<li><a target="_blank" href="https://www.e-tar.lt/portal/lt/legalAct/TAR.3B14F18E2B3C/SRQIpgLVoH">Maitinimo organizavimo ikimokyklinio ugdymo, bendrojo ugdymo mokyklose ir vaikų socialinės globos įstaigose tvarkos aprašas</a></li>
	<li><a target="_blank" href="https://www.e-tar.lt/portal/lt/legalAct/763aaae018e211e58569be21ff080a8c">Vaikų ugdymo įstaigų<span class="notice">, vaikų socialinės globos įstaigų ir vaikų poilsio stovyklų</span> valgiaraščių derinimo tvarkos aprašas</a></li>
	<li><a target="_blank" href="https://www.e-tar.lt/portal/lt/legalAct/83eca29088ad11e397b5c02d3197f382">Sveikatos priežiūros ikimokyklinio ugdymo įstaigose tvarkos aprašas ir Vaikų sveikatos priežiūros ikimokyklinio ugdymo įstaigose rekomendacijos</a></li>
</ul>
<h3>Sveikatos mokymo ir ligų prevencijos centras (LR sveikatos apsaugos ministerija)</h3>
<ul>
	<li><a target="_blank" href="http://www.smlpc.lt/media/media/vaiko_saugos_vadovas_LT_sumazintas.pdf"><strong>Produktų vaikams saugos vadovas.</strong> Potencialiai pavojingi produktai.</a> Lietuviškas vertimas. Birmingemas: Europos vaiko saugos aljansas, EuroSafe, 2013. <a target="_blank" href="http://www.smlpc.lt/lt/neinfekciniu_ligu_profilaktika/suzalojimu_prevencija/produktu_vaikams_saugos_vadovas__jau_ir_lietuviu_kalba.html">Naujiena</a></li>
</ul>
<h3>Užkrečiamųjų ligų ir AIDS centras (LR sveikatos apsaugos ministerija)</h3>
<ul>
	<li><a target="_blank" href="http://www.ulac.lt/uploads/downloads/Pedikulioze%20WEB.pdf">Visuomenės sveikatos priežiūros specialistams: Metodines rekomendacijas „Pedikuliozės profilaktika ir kontrolė“ atnaujintos 2015 m.
Metodinėse rekomendacijose pateikiamas algoritmas, kaip elgtis pedikuliozės atveju ir pateikti laiškų tėvams pavyzdžiai bei informacija apie pedikuliozės plitimą, ligos platintojus, klinikinius požymius, profilaktiką. Laiku suteiktos profilaktinės priemonės didelėse vaikų susibūrimo vietose, užkirs šios ligos plitimą.</a></li>
</ul>
<h3>Buhalterija</h3>
<ul>
	<li><a target="_blank" href="https://www.e-tar.lt/portal/lt/legalAct/TAR.43178AA9832E/hMlYFfoVxx">LR buhalterinės apskaitos įstatymas</a></li>
	<!--
	https://www.e-tar.lt/portal/lt/legalAct/TAR.A5ACBDA529A9/nDmlUuROkE Pelno mokesčio
	
	https://www.e-tar.lt/portal/lt/legalAct/TAR.ED68997709F5/EKdufeNewD PVM
	 -->
	<li><a target="_blank" href="https://www.e-tar.lt/portal/lt/legalAct/TAR.E2CE2C82DA9E/HKHuFgdvsx">LR viešojo sektoriaus atskaitomybės įstatymas</a></li>
	<li>Privačioms įmonėms <a target="_blank" href="https://www.e-tar.lt/portal/lt/legalAct/883083f0046f11e588da8908dfa91cac">LR įmonių finansinės atskaitomybės įstatymas</a></li>
	<li>Privačioms įmonėms <a target="_blank" href="http://avnt.lt/veiklos-sritys/apskaita/">Apskaita iš Audito, apskaitos, turto vertinimo ir nemokumo valdymo tarnyba prie LR <span class="abbr" title="Finansų ministerija">FM</span></a></li>
	<li><a target="_blank" href="https://finmin.lrv.lt/lt/veiklos-sritys/apskaita-ir-atskaitomybe/viesojo-sektoriaus-apskaita-ir-atsakomybe/finansine-atskaitomybe/viesojo-sektoriaus-apskaitos-ir-finansines-atskaitomybes-standartai-vsafas">Viešojo sektoriaus apskaitos ir finansinės atskaitomybės standartai (VSAFAS)</span></a></li>
	<li><a target="_blank" href="https://www.e-tar.lt/portal/lt/legalAct/TAR.2F286B29068D/DondEqlEeC">Mokesčiams apskaičiuoti naudojamų apskaitos dokumentų išrašymo ir pripažinimo taisyklės</a></li>
	<li><a target="_blank" href="https://www.vmi.lt/cms/imas">„Išmaniosios mokesčių administravimo (i.MAS) sistemos“ aprašymas</a></li>
	<li><a target="_blank" href="https://e-tar.lt/portal/lt/legalAct/TAR.A7D14A4D66D5/zmhuvuQFGn">Pridėtinės vertės mokesčio sąskaitų faktūrų registrų tvarkymas</a>. VMI - <a target="_blank" href="https://www.vmi.lt/cms/pvm-saskaitu-fakturu-registru-duomenu-tvarkymo-ir-pateikimo-taisykliu-projektas">PVM sąskaitų faktūrų registrų duomenų teikimas VMI nuo 2016-10-01 (i.SAF)</a></li>
	<li><a target="_blank" href="https://www.e-tar.lt/portal/lt/legalAct/1c44f7f02f9611e5b1be8e104a145478/JBYdGjzXrj">Standartinės apskaitos duomenų rinkmenos techninės specifikacijos ir techninių reikalavimų aprašas</a>. VMI - <a target="_blank" href="https://www.vmi.lt/cms/saf-t">Standartinės apskaitos duomenų rinkmenos techninė specifikacija ir techniniai reikalavimai</a></li>
	<li><a target="_blank" href="https://www.e-tar.lt/portal/lt/legalAct/6e49e820219111e5b336e9064144f02a">Apskaitos dokumentų duomenų teikimo standartinėje apskaitos duomenų rinkmenoje tvarkos aprašas</a></li>
</ul>
<?php if(KESV) { ?>
<h3>KESV Viešieji pirkimai</h3>
<ul>
	<li><a target="_blank" href="https://www.e-tar.lt/portal/lt/legalAct/TAR.C54AFFAA7622/eJMdGqxbgP">LR viešojo pirkimo įstatymas</a></li>
	<li><a target="_blank" href="https://www.e-tar.lt/portal/lt/legalAct/TAR.B02063FFE50A/SvvBBPLXTH">Viešųjų pirkimų pasiūlymų vertinimo rekomendacijos</a></li>
	<li><a target="_blank" href="https://www.e-tar.lt/portal/lt/legalAct/TAR.FF1BFE7DEA44/xMfNlrEfgN">Perkančiųjų organizacijų viešųjų pirkimų organizavimo ir vidaus kontrolės rekomendacijos</a></li>
</ul>
<?php } ?>
<h3>Dokumentai</h3>
<ul>
	<li><a target="_blank" href="https://www.e-tar.lt/portal/lt/legalAct/TAR.E60E6A140217/zBRUiIfluw">Dokumentų rengimo taisyklės</a></li>
	<li><a target="_blank" href="https://www.e-tar.lt/portal/lt/legalAct/TAR.C21AD42B2592">Dokumentų tvarkymo ir apskaitos taisyklės</a></li>
	<li><a target="_blank" href="https://www.e-tar.lt/portal/lt/legalAct/TAR.F76F0B2DC482/FcshQDjeVd">Elektroninių dokumentų valdymo taisyklės</a></li>
	<li>Privačioms įmonėms <a target="_blank" href="https://www.e-tar.lt/portal/lt/legalAct/TAR.0B8AC41336C9">Nevalstybinių organizacijų ir privačių juridinių asmenų dokumentų rengimo, tvarkymo ir apskaitos taisyklės</a></li>
	<li><a target="_blank" href="https://www.e-tar.lt/portal/lt/legalAct/TAR.1FEF229DA7C6/azGpUSPzkH">LR Dokumentų ir archyvų įstatymas</a></li>
	<li><a target="_blank" href="https://www.e-tar.lt/portal/lt/legalAct/TAR.E1A996F775F1">Elektroninių dokumentų nuorašų ir išrašų spausdinimo rekomendacijų patvirtinimas</a></li>
	<li><a target="_blank" href="https://www.e-tar.lt/portal/lt/legalAct/TAR.382345294FBF">LR elektroninio parašo įstatymas</a></li>
</ul>
<h3>Darbas</h3>
<ul>
	<li><a target="_blank" href="https://www.e-tar.lt/portal/lt/legalAct/TAR.31185A622C9F/kBHCbuIxdP">Darbo kodeksas</a></li>
	<li><a target="_blank" href="https://www.e-tar.lt/portal/lt/legalAct/TAR.389ACB3D850E/TAIS_286896">Darbo laiko apskaitos žiniaraščio pavyzdinės formos ir jo pildymo tvarkos aprašas (Tabelis)</a></li>
	<li><a target="_blank" href="https://www.e-tar.lt/portal/lt/legalAct/TAR.A4A7F7C56568/zgynGBIDFC">Švietimo įstaigų darbuotojų ir kitų įstaigų pedagoginių darbuotojų darbo apmokėjimo tvarkos aprašas</a></li>
</ul>

<h2>Knygų sąrašas</h2>
<h3>El. knygos atsisiuntimui</h3>
<ul>
	<li><a target="_blank" href="http://www.viltis.lt/leidiniai/knygos/item/download/19_32b7c0e5f4466a03bf743cf52350e64f">„Dauno sindromas ir vaiko raida. Kompleksinė vaiko negalia. Vaikų elgesio sutrikimai“ išleido Lietuvos sutrikusio intelekto žmonių globos bendrijos „Viltis, 2007</a> <a target="_blank" href="http://www.viltis.lt/leidiniai/knygos/item/1329-dauno-sindromas-ir-vaiko-raida-kompleksine-vaiko-negalia-vaiko-elgesio-sutrikimai">Naujiena</a></li>
	<li><a target="_blank" href="http://csunplugged.org/wp-content/uploads/2014/12/Unplugged-LT.pdf">Informatika be kompiuterio. Turininga informatikos mokymosi medžiaga pradinukams ir vyresniems</a></li>
	<li><a target="_blank" href="https://code.org/curriculum/unplugged">Angliškai. Informatika. Ugdymo turinys nuo 4 metų.</a></li>
</ul>
<?php
/*
iki 7 muzikuotui
nuo 6 mėn. skaityti
https://www.youtube.com/watch?v=CIl7EdB7ZGw

<h3>Ugdoma savivoka, savigarba ir kt.</h3>
<ol class="goodies">
	<li><a class="fancybox" rel="gallery1" href="/img/knygos/1_Krukauskienė_Lina._Pasakos_apie_tave_ir_mane.jpg" title="Krukauskienė Lina. Pasakos apie tave ir mane. (Terapinio pobūdžio pasakos su užduotėlėmis).- Vilnius: Presvika, 2014, - 118 p."><span class="img"><img alt="" src="/img/knygos/1_Krukauskienė_Lina._Pasakos_apie_tave_ir_mane.jpg"></span>Krukauskienė Lina. Pasakos apie tave ir mane. (Terapinio pobūdžio pasakos su užduotėlėmis).- Vilnius: Presvika, 2014, - 118 p.</a></li>
	<li><a class="fancybox" rel="gallery1" href="/img/knygos/2_Collins_Jonathan._Malio_ir_Kilos_nuotykiai.jpg" title="Collins Jonathan. Malio ir Kilos nuotykiai. Knyga vaikams apie dorybes. – Vilnius: Psichologija Tau, 2013, - 144 p."><span class="img"><img alt="" src="/img/knygos/2_Collins_Jonathan._Malio_ir_Kilos_nuotykiai.jpg" style=";"></span>Collins Jonathan. Malio ir Kilos nuotykiai. Knyga vaikams apie dorybes. – Vilnius: Psichologija Tau, 2013, - 144 p.</a></li>
	<li><a class="fancybox" rel="gallery1" href="/img/knygos/3.jpg" title="Trace Moroney. Kai jaučiuosi GERAS. – „Educata“, 2013 Padėti kitiems, išklausyti draugą, mylėti save – tai ir yra <em>gerumas</em>. Ir dar daugiau...	Knygelės pabaigoje Pastabos tėveliams rasite psichologų įžvalgų ir patarimų, kaip padėti vaikui suprasti, kas yra gerumas"><span class="img"><img alt="" src="/img/knygos/3.jpg"></span>Trace Moroney. Kai jaučiuosi GERAS. – „Educata“, 2013<br>
		Padėti kitiems, išklausyti draugą, mylėti save – tai ir yra <em>gerumas</em>. Ir dar daugiau...<br>
		Knygelės pabaigoje <em>Pastabos tėveliams</em> rasite psichologų įžvalgų ir patarimų, kaip padėti vaikui suprasti, kas yra gerumas.</a></li>
	<li><a class="fancybox" rel="gallery1" href="/img/knygos/4.jpg" title="Trace Moroney. Kai jaučiu PAVYDĄ. – „Educata“, 2013<br>
		Kartais <em>Pavydo</em> jausmas gali pastūmėti tave krėsti kvailystes. Tačiau atmink, kad kiekvienas iš mūsų esam vieninteliai ir ypatingi. Ir tu toks esi!"><span class="img"><img alt="" src="/img/knygos/4.jpg"></span>Trace Moroney. Kai jaučiu PAVYDĄ. – „Educata“, 2013<br>
		Kartais <em>Pavydo</em> jausmas gali pastūmėti tave krėsti kvailystes. Tačiau atmink, kad kiekvienas iš mūsų esam vieninteliai ir ypatingi. Ir tu toks esi!</a></li>
	<li><a class="fancybox" rel="gallery1" href="/img/knygos/5.jpg" title="Trace Moroney. Kai jaučiuosi PIKTAS. – „Educata“, 2013. Kai esi labai <em>Piktas,</em> atrodo, kad greit susprogsi. Nieko blogo kartais pykti, kol neįskaudini kitų."><span class="img"><img alt="" src="/img/knygos/5.jpg"></span>Trace Moroney. Kai jaučiuosi PIKTAS. – „Educata“, 2013<br>
		Kai esi labai <em>Piktas,</em> atrodo, kad greit susprogsi. Nieko blogo kartais pykti, kol neįskaudini kitų.</a></li>
	<li><a class="fancybox" rel="gallery1" href="/img/knygos/6.jpg" title="Trace Moroney. Kai jaučiuosi IŠSIGANDĘS. – „Educata“, 2014.
		Kas nori gali bijoti, pavyzdžiui, pavojingų gyvūnų. Bet daugybė dalykų nėra tokie baisūs, kaip juos įsivaizduojame."><span class="img"><img alt="" src="/img/knygos/6.jpg"></span>Trace Moroney. Kai jaučiuosi IŠSIGANDĘS. – „Educata“, 2014<br>
		Kas nori gali bijoti, pavyzdžiui, pavojingų gyvūnų. Bet daugybė dalykų nėra tokie baisūs, kaip juos įsivaizduojame.</a></li>
	<li><a class="fancybox" rel="gallery1" href="/img/knygos/7.jpg" title="Trace Moroney.  Kai jaučiuosi VIENIŠAS. – „Educata“, 2014.
		Dėl daugybės dalykų gali jaustis<em> vienišas. </em>Tai nėra malonu, tačiau prisimink, mes visi kartais taip jaučiamės."><span class="img"><img alt="" src="/img/knygos/7.jpg"></span>Trace Moroney.  Kai jaučiuosi VIENIŠAS. – „Educata“, 2014<br>
		Dėl daugybės dalykų gali jaustis<em> vienišas. </em>Tai nėra malonu, tačiau prisimink, mes visi kartais taip jaučiamės.</a></li>
	<li><a class="fancybox" rel="gallery1" href="/img/knygos/8.jpg" title="Trace Moroney. Kai jaučiuosi LIŪDNAS. – „Educata“, 2014.
		Visiems būna <em>liūdna.</em> Liūdesį išgyvename vieni, uždarai. Tačiau pokalbis su artimuoju gali šiek tiek numaldyti šį jausmą."><span class="img"><img alt="" src="/img/knygos/8.jpg"></span>Trace Moroney. Kai jaučiuosi LIŪDNAS. – „Educata“, 2014<br>
		Visiems būna <em>liūdna.</em> Liūdesį išgyvename vieni, uždarai. Tačiau pokalbis su artimuoju gali šiek tiek numaldyti šį jausmą.</a></li>
	<li><a class="fancybox" rel="gallery1" href="/img/knygos/9.jpg" title="Trace Moroney. Kai jaučiuosi MYLIMAS.- „Educata“, 2014.
		Būti <em>mylimam</em> yra pats nuostabiausias jausmas. Tai suteikia saugumo, šilumos ir pojūtį, kad esi kitam brangus."><span class="img"><img alt="" src="/img/knygos/9.jpg"></span>Trace Moroney. Kai jaučiuosi MYLIMAS.- „Educata“, 2014<br>
		Būti <em>mylimam</em> yra pats nuostabiausias jausmas. Tai suteikia saugumo, šilumos ir pojūtį, kad esi kitam brangus.</a></li>
	<li><a class="fancybox" rel="gallery1" href="/img/knygos/10.jpg" title="Trace Moroney. Kai jaučiuosi LAIMINGAS. – „Educata“, 2013. Laimės jausmą suteikia daugybė dalykų. Pavyzdžiui, žaidimai su draugais, netikėta dovanėlė ir ypač kai kas nors stipriai apkabina..."><span class="img"><img alt="" src="/img/knygos/10.jpg"></span>Trace Moroney. Kai jaučiuosi LAIMINGAS. – „Educata“, 2013<br>
		<em>Laimės</em> jausmą suteikia daugybė dalykų. Pavyzdžiui, žaidimai su draugais, netikėta dovanėlė ir ypač kai kas nors stipriai apkabina...</a></li>
	<li><a class="fancybox" rel="gallery1" href="/img/knygos/11.jpg" title="Trace Moroney. Kodėl myliu save. Ugdomoji paveikslėlių knygelė. – „Educata“, 2014. Kodėl myliu save. Sau gali būti mielas dėl daugelio dalykų: miklių pirščiukų, vikrių kojyčių, plačios šypsenos, ateities svajonių."><span class="img"><img alt="" src="/img/knygos/11.jpg"></span>Trace Moroney. <strong>Kodėl myliu save. </strong>Ugdomoji paveikslėlių knygelė. – „Educata“, 2014<br>
		<em>Kodėl myliu save. </em>Sau gali būti mielas dėl daugelio dalykų: miklių pirščiukų, vikrių kojyčių, plačios šypsenos, ateities
svajonių.</a></li>
	<li><a class="fancybox" rel="gallery1" href="/img/knygos/12.jpg" title="Trace Moroney. Kodėl myliu draugus. Ugdomoji paveikslėlių knygelė. – „Educata“, 2014. Kodėl myliu draugus. Draugus myliu dėl daugybės dalykų. Su jais dalijamės savo sumanymais, pomėgiais, jausmais, smagiai leidžiame laiką – mokomės draugiškumo"><span class="img"><img alt="" src="/img/knygos/12.jpg"></span>Trace Moroney. <strong>Kodėl myliu draugus. </strong>Ugdomoji paveikslėlių knygelė. – „Educata“, 2014<br>
		<em>Kodėl myliu draugus.</em> Draugus myliu dėl daugybės dalykų. Su jais dalijamės savo sumanymais, pomėgiais, jausmais, smagiai leidžiame laiką – mokomės <em>draugiškumo.</em></a></li>
	<li><a class="fancybox" rel="gallery1" href="/img/knygos/13_Trace_Moroney._Kodėl_man_patinka_miego_metas.jpg" title="Trace Moroney. Kodėl man patinka miego metas. Ugdomoji paveikslėlių knygelė. – „Educata“, 2014. Kodėl man patinka miego metas. Kaip gera įsirangius į lovytę pasiklausyti pasakos, pasišnekėti ir nugrimzti į sapnų karalystę."><span class="img"><img alt="" src="/img/knygos/13_Trace_Moroney._Kodėl_man_patinka_miego_metas.jpg"></span>Trace Moroney. <strong>Kodėl man patinka miego metas.</strong> Ugdomoji	paveikslėlių knygelė. – „Educata“, 2014<br>
		<em>Kodėl man patinka miego metas.</em> Kaip gera įsirangius į lovytę pasiklausyti pasakos, pasišnekėti ir nugrimzti į sapnų karalystę.</a></li>
	<li><a class="fancybox" rel="gallery1" href="/img/knygos/14.jpg" title="Trace Moroney. Kodėl myliu savo šeimą. Ugdomoji paveikslėlių knygelė. – „Educata“, 2014. Kodėl myliu savo šeimą. Šeima kiekvienam brangi dėl daugelio dalykų, bet svarbiausia – joje jautiesi mylimas, vertinama, savas ir saugus"><span class="img"><img alt="" src="/img/knygos/14.jpg"></span>Trace Moroney. <strong>Kodėl myliu savo šeimą. </strong>Ugdomoji paveikslėlių knygelė. – „Educata“, 2014<br>
		<em>Kodėl myliu savo šeimą. </em>Šeima kiekvienam brangi dėl daugelio dalykų, bet svarbiausia – joje jautiesi mylimas, vertinama, savas ir saugus.</a></li>
</ol>
*/ ?>
<h3>Knygos pedagogams ir tėveliams</h3>
<ol class="goodies">
	<li><a class="fancybox" rel="gallery1" href="/img/knygos/2_1Org.png" title="Bronson Po, Merryman Ashley. <strong>Šalin mitus!</strong> : naujas požiūris į vaikų auklėjimą. – Vilnius: Alma littera, 2015. – 320 p."><span class="img"><img alt="" src="/img/knygos/2_1Org.png"></span>Bronson Po, Merryman Ashley. <strong>Šalin mitus!</strong> : naujas požiūris į vaikų auklėjimą. – Vilnius: Alma littera, 2015. – 320 p.<br>
<span class="desc">„Šalin mitus!” – tai naujas požiūris į vaikystę, pranokstantis mūsų sukauptą tradicinę išmintį.  Kitaip nei dažnas tėvystės vadovėlis, „Šalin mitus!” paliečia pačią mūsų augimo, mokymosi,  gyvenimo esmę.  Autoriai remiasi įvairių sričių mokslininkų atliktais tyrimais ir paneigia per ilgą laiką nusistovėjusius vaikų auklėjimo mitus.  Tėvai karštai trokšta paspartinti vaikų kalbos raidą nuo pat jauniausio amžiaus, bet ar tai vertinga. Nieko panašaus nesate girdėję, tai tikrai naujas požiūris į vaikų auklėjimą.</span></a></li>
	<li><a class="fancybox" rel="gallery1" href="/img/knygos/2_2Org.png" title="Cameron Julia. <strong>Kaip ugdyti vaikų kūrybingumą.</strong> – Vilnius: Alma littera, 2015. – 232 p."><span class="img"><img alt="" src="/img/knygos/2_2Org.png"></span>Cameron Julia. <strong>Kaip ugdyti vaikų kūrybingumą.</strong> – Vilnius: Alma littera, 2015. – 232 p.<br>
<span class="desc">Knygoje išdėstyta ugdymo metodika, vienodai skirta pedagogams, tėvams ir vaikams nuo gimimo iki 12 metų. Kalbama apie bendriausius etikos aspektus: apie dėkingumą ir padėką, apie ribų nubrėžimą, apie netvarkos ir tvarkos suvokimą, pagarbą (mes esame daug kūrybiškesni, kai esame gerbiami), saviraišką, tarpusavio pasitikėjimą, siūloma daugybė idėjų, kaip skatinti išradingumą, kodėl svarbu įsivesti ritualus ir daugybę kitų dalykų.</span></a></li>
	<li><a class="fancybox" rel="gallery1" href="/img/knygos/2_3Org.jpg" title="<strong>NEPAMESK GALVOS!</strong>  Praktinis vadovas vaikams, kaip elgtis įvairiose situacijose/ Sud. Danguolė Kandrotienė. – Kaunas: 2014. – 239 p."><span class="img"><img alt="" src="/img/knygos/2_3Org.jpg"></span><strong>NEPAMESK GALVOS!</strong>  Praktinis vadovas vaikams, kaip elgtis įvairiose situacijose/ Sud. Danguolė Kandrotienė. – Kaunas: 2014. – 239 p.<br>
<span class="desc">„Nepamesk galvos! Praktinis vadovas vaikams, kaip elgtis įvairiose situacijose” – knyga, padėsianti vaikams, pedagogams paaiškinti kaip spręsti kasdieniniame gyvenime kylančias problemas.  Leidinyje pristatomos šių dienų vaikams aktualios temos: saugumas gatvėje ir namuose, priklausomybė nuo kompiuterio, internete tykantys pavojai.  Informacija pateikta vaikams patraukliu stiliumi, iliustruota žaismingomis iliustracijomis.</span><br>
<span class="important">(puiki knyga! Labai tinkama ir kasdieniams gyvenimo  įgūdžiams, problemų sprendimui, bendravimui su bendraamžiais ir suaugusiais).</span></a></li>
	<li><a class="fancybox" rel="gallery1" href="/img/knygos/2_4Org.jpg" title="Ambrose Jamie.  365 užsiėmimai kiekvienai dienai. Turite išbandyti!- Vilnius: Alma littera. – 2015."><span class="img"><img alt="" src="/img/knygos/2_4Org.jpg"></span>Ambrose Jamie.  365 užsiėmimai kiekvienai dienai. Turite išbandyti!- Vilnius: Alma littera. – 2015.<br>
<span class="desc">Pastatyti viešbutį vabalams, sukurti debesį, rasti fosiliją. Atpažinti gyvūnų pėdsakus, patyrinėti tvenkinio , pievų gyvūnus – ir dar daug kitų įdomių dalykų galima nuveikti vasarą darželyje ir namuose.<br>
Ši knyga neleis nė akimirkos nuobodžiauti ir supažindina su nuostabiu gamtos pasauliu, kuris prasideda čia pat, už jūsų namų, darželio durų.</span><br>
<span class="important">(senai buvo išleista PUIKI knyga, tai ir aplinkos pažinimas, tyrinėjimas, mokėjimas mokytis, kūrybiškumas.  Tiek daug idėjų vienoje vietoje!)</span></a></li>


<li><a class="fancybox" rel="gallery1" href="/img/knygos/15.jpg" title="Auklėjimas be dramų: kaip visapusiškai lavinti vaiko protą ir numaldyti kylančias audras. Siegel, Daniel J. Vaga. 2015 m. 304 psl."><span class="img"><img alt="" src="/img/knygos/15.jpg"></span><strong>Auklėjimas be dramų: kaip visapusiškai lavinti vaiko protą ir numaldyti kylančias audras.</strong> Siegel, Daniel J. Vaga. 2015 m. 304 psl.<br>
Žaismingai iliustruotoje knygoje tėvai ir pedagogai ras paprastų ir veiksmingų patarimų, kaip elgtis prasidėjus vaiko pykčio priepuoliui ar agresijai, išmokys švelniai numalšinti jo emocines audras ir nerimus. Išsamiai aprašytos įvairios probleminės situacijos ir jų sprendimų būdai suteiks tėvams ir pedagogams daugiau žinių apie vaiko nervinę sistemą ir jo amžiui tinkamus auklėjimo metodus.</a></li>

<li><a class="fancybox" rel="gallery1" href="/img/knygos/16.jpg" title="Pypliotyra: psichologų patarimai nuo lopšio iki mokyklos. Jūratė Jadkonytė Petraitienė. AJA viešieji ryšiai. 2015 m. 333 psl."><span class="img"><img alt="" src="/img/knygos/16.jpg"></span><strong>Pypliotyra: psichologų patarimai nuo lopšio iki mokyklos.</strong> Jūratė Jadkonytė Petraitienė. AJA viešieji ryšiai. 2015 m. 333 psl.<br>
Knygoje žurnalistė, leidinių tėvams redaktorė Jūratė Jadkonytė Patraitienė aptaria temas, aktualias ikimokyklinio amžiaus vaikus auginantiems tėvams. Pasakojamos istorijos - iš mūsų gyvenimo, jų herojai  - mūsų vaikai. Kaip išgyventi pirmųjų ir trečiųjų metų krizes? Kodėl mažylis nesėda ant puoduko? Atsakymus į šiuos ir daugybę kitų klausimų padeda rasti žinomi Lietuvos konsultantai, psichologai ir psichoterapeutai.</a></li>
 
<li><a class="fancybox" rel="gallery1" href="/img/knygos/17.jpg" title="3-6 metų vaiko raida: psichologijos pagrindai ikimokyklinio amžiaus vaikų ugdytojams. Hille Katrin. Tyto alba. 2015 m. 204 psl."><span class="img"><img alt="" src="/img/knygos/17.jpg"></span><strong>3-6 metų vaiko raida: psichologijos pagrindai ikimokyklinio amžiaus vaikų ugdytojams.</strong> Hille Katrin. Tyto alba. 2015 m. 204 psl.<br>
Knygoje aprašoma trejų - šešerių metų vaiko raida. Pateikiamos pagrindinės žinios apie mokymąsi, savireguliaciją ir motyvaciją. Ypatingas dėmesys skiriamas darželius lankančių vaikų raidai ir jų lavinimui. Ikimokyklinio ugdymo pedagogai, tėvai ras svarbiausių žinių apie vaiko motorikos, kalbos raidą, kognityvinius, socioemocinius gebėjimus.</a></li>
 
<li><a class="fancybox" rel="gallery1" href="/img/knygos/18.jpg" title="Sumanūs tėvai - saugūs vaikai. Rebecca Bailey, Elizabeth Bailey. Mano knyga ir Vitae Litera. 2015 m. 224 psl."><span class="img"><img alt="" src="/img/knygos/18.jpg"></span><strong>Sumanūs tėvai - saugūs vaikai.</strong> Rebecca Bailey, Elizabeth Bailey. Mano knyga ir Vitae Litera. 2015 m. 224 psl.<br>
Skaitydami knygą sužinosite, kaip: - išmokyti vaikus (tiek 2 m. pyplius, tiek 18-mečius paauglius laikytis pagrindinių saugumo taisyklių; - apsaugoti vaikus nuo išnaudojimo, galimo pagrobimo ar patyčių pavojaus; - apsaugoti vaiką nuo internete, „Facebook‘e“ tykančių pavojų; - paskatinti bendravimą ir „jokių paslapčių“ politiką namuose; - kur kreiptis ir kokių priemonių imtis, susidūrus su Jūsų vaikui pavojų keliančiomis situacijomis.</a></li>

<li><a class="fancybox" rel="gallery1" href="/img/knygos/19.jpg" title="Ikimokyklinio amžiaus vaikų kūrybiškumo ugdymas, Diane Trister Dodge, Laura j. Colker, Cate Heroman, Presvika, 2007 m. (2015 m. leidimas)"><span class="img"><img alt="" src="/img/knygos/19.jpg"></span><strong>Ikimokyklinio amžiaus vaikų kūrybiškumo ugdymas.</strong> Diane Trister Dodge, Laura j. Colker, Cate Heroman. Presvika. 2007 m. (2015 m. leidimas). 480 psl.<br>
Ugdymo metodų praktinių rekomendacijų šaltinis; Aptariamos temos - ugdymo(si) aplinkas, ugdymo(si) turinį, pedagogo vaidmenį ugdymo procese, šeimos vaidmenį ugdymo procese. Knygos antroje dalyje išsamiai pristatomos veiklos erdvės - priemonių parinkimas ir išdėstymas, nauda vaikams, pedagogo vaidmuo, informacijos apie vaikų dienos veiklą pateikimas tėvams  (konstravimas, vaidmenų žaidimai, žaislai ir žaidimai, menas, grupės biblioteka, tyrinėjimai, smėlis ir vanduo, muzika ir judesys, maisto gaminimas, kompiuteriai / naujosios technologijos, lauko aikštelė).
</a></li>
</ol>
Norėdami įdėti informaciją rašykite ją į <a href="mailto:info@musudarzelis.lt">info@musudarzelis.lt</a> mes pasirūpinsime Jos tinkamu patalpinimu.<br><br>
</div>
