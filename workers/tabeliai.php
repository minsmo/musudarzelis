<?php if(!defined('DARBUOT')) exit();
$sum_a_day = false; //DB_PREFIX == 'knsrAzuol_';
$eat_print = PasvalioLiepaite || KelmesRTytuvenuLD || SkuodoRYlakiuVaikuLD || DB_PREFIX == 'kedPur_' || DB_PREFIX == 'pnvLin_' || DB_PREFIX == 'pnvZil_' || DB_PREFIX == 'klpSau_'/*pas juos maitinimas pagal įstaigos nuostatus gali keistis tik 2 kart į metus rugsėjo 1 d. ir sausio 1 d. */ || DB_PREFIX == 'kedPas_' || DB_PREFIX == 'sakBerz_' || DB_PREFIX == 'knsSpind_' || DB_PREFIX == 'rokVarp_';
?>
<h1 class="tabeliai no-print">Vaikų lankomumo apskaitos žiniaraštis<?php if(AUKLETOJA || ADMIN) { ?> <a href="/tabeliai/rakinimas">-&gt; Užbaigimas</a><?php } ?></h1>
<div id="content" class="tabeliai">
<?=ui_print()?>

<a class="notice no-print" onclick="$('#printNotice').toggle(); return false;" href="#">Kaip spausdinti geriau?</a>
<p class="notice no-print" id="printNotice" style="margin-bottom: 10px; display: none;">Tai aprašėme meniu punkte „<a href="/spausdinti">Spausdinimo nustatymai</a>“. <strong>Dėmesio!</strong> Galite atsispausdinti <strong>didesnio šrifto</strong> žiniaraštį/tabelį su saityno naršykle „Mozilla Firefox“ didindami mastelį.
</p>
<p><a class="notice no-print" onclick="$('#notice').toggle(); return false;" href="#">Iš kur imami duomenys skaičiuojant žiniaraščius?</a></p>
<p class="notice no-print" id="notice" style="margin-bottom: 5px; display: none;">Žiniaraščius skaičiuojant duomenys imami iš meniu punktų „Vaikai“ pagal galiojimo datą (nurodytą laukelyje „Duomenys galioja nuo“), „Lankomumas“, „Pateisinimai“ ir „Nustatymai“ (tik šį meniu punktą pildo darželio vadovai).<br>
Spausdinant mokėjimo suma imama iš naudotojo išsaugotos sumos (dešinesnis stulpelis pavadintas „Mokėjimui (valiuta)“), o jei jos nėra tada sistema suskaičiuotą sumą (kairesnis stulpelis pavadintas „Mokėjimui (valiuta)“). Pakeitus vieną iš mokėjimo sumų reikia paspausti dar kartą, kad iš naujo suskaičiuotų žiniaraštį.
</p>

<form method="get" style="padding-bottom:10px;" class="no-print">
	Suskaičiuoti žiniaraštį:<br>
	<!-- <div class="fl" style="line-height: 33px; margin-right: 7px;"></div> -->
	<div class="sel" style="float: left; margin-right: 7px;"><select name="data" required="required"><!--  title="Žymėto lankomumo laikotarpis" -->
	<?php
	if(isset($_GET['data'])) {
		list($year, $month) = explode('-', $_GET['data']);//, $day
		$year = (int) $year;
		$month = (int) $month;
	}
	$result = db_query("SELECT YEAR(`data`) `metai`, MONTH(`data`) `menuo` FROM `".DB_attendance."` GROUP BY YEAR(`data`) DESC, MONTH(`data`) DESC");
	//$result = db_query("SELECT YEAR(`data`) `metai`, MONTH(`data`) `menuo` FROM `".DB_attendance."` GROUP BY YEAR(`data`), MONTH(`data`) ORDER BY `data` DESC");
	//Neisigilinau, bet kažkas įdomaus: http://stackoverflow.com/questions/15390303/how-to-group-by-desc-order
	//http://use-the-index-luke.com/3-minute-test/mysql?quizkey=d2a088303b955b839e29cb8711a85235
	/*
	SELECT text, date_column
	  FROM tbl
	 WHERE date_column >= STR_TO_DATE('2012-01-01', '%Y-%m-%d')
	   AND date_column <  STR_TO_DATE('2013-01-01', '%Y-%m-%d');
	*/
	while ($row = mysqli_fetch_assoc($result))
		echo "<option value=\"".$row['metai']."-".$row['menuo']."\"".(isset($_GET['grupes_id']) && isset($_GET['data']) && $row['metai'] == $year && $row['menuo'] == $month ? ' selected="selected">'.$selectedMark : '>').year($row['metai'])."-".men($row['menuo'])."</option>";
	?>
	</select></div>
	<div class="sel" style="float: left; margin-right: 7px;"><select name="grupes_id">
	<?php 
	foreach(getAllowedGroups() as $ID => $title) {
		echo "<option value=\"".$ID."\"".(isset($_GET['data']) && isset($_GET['grupes_id']) && $ID == $_GET['grupes_id'] || !isset($_GET['data']) && !(ADMIN /*|| BUHALT*/) && $ID == GROUP_ID ? ' selected="selected">'.$selectedMark : '>').$title."</option>";
		if(isset($_GET['grupes_id']) && $ID == $_GET['grupes_id'])
			$grupes_pav = $title;
	}
	?>
	</select></div>
	<?php
	$allowedToEditPayment = ADMIN || BUHALT;
	if($showPayment && $allowedToEditPayment) { ?>
	<div><label><input type="checkbox" name="save" value="1" style="width: 33px;"> Baigtas pildyti viso mėn. tabelis, išsaugoti mokėjimų sumas</label></div>
	<?php } ?>
	<div><label><input type="checkbox" name="pre_school_free_lunch" value="1" style="width: 33px;"<?=(isset($_GET['pre_school_free_lunch']) ? ' checked="checkded"' : '')?>> Tik piešmokyklinukai nemokantys pietų</label></div>
	<input type="submit" class="filter" name="full" value="Išplėstinį"><!-- Sudaryti, Automatiškai sudaryti -->
	<input type="submit" class="filter" name="svietimui" value="Pagal „švietimą“"><!-- Sudaryti, Automatiškai sudaryti -->
	<input type="submit" class="filter" name="summary" value="Sutrumpintą"><!-- Sudaryti, Automatiškai sudaryti -->
	<input type="submit" class="filter" name="food" value="Maitinimo">
	<input type="submit" class="filter" name="foodDetails" value="Maitinimas rodomas juodai">
	
</form>
<!-- <script src="/libs/jquery.table2excel.js"></script>
<button class="excel">Excel</button> -->
<?php
if(isset($_GET['save']))
	msgBox('OK', 'Išsaugota mokėjimų informacija');
?>

<?php
if(isset($_GET['grupes_id']) && isset($_GET['data']) && hasGroup($_GET['grupes_id'])/*redundant because of the next condition*/ && isset($grupes_pav)) {
	if (isset($_GET['download'])) {
		ob_clean();
		word_header();
		echo '<div class=Section2>';
	}
	$data_max= $year.'-'.men($month).'-'.date('t', strtotime($year.'-'.men($month).'-01'));
	$data_min = $year.'-'.men($month).'-01';
	
	$currently_locked = db_query("SELECT * FROM `1lock_kids_attendance_food` WHERE `kindergarten_id`=".DB_ID." AND `date`='".db_fix($data_min)."'  AND `group_id`=".(int)$_GET['grupes_id']);
	$currently_locked = mysqli_num_rows($currently_locked) ? true : false;
	
	$html = '';
	if(DB_PREFIX == 'kaiZas_' || DB_PREFIX == 'kretPasak_' || DB_PREFIX == 'kretAzuol_' || DB_PREFIX == 'pnvPus_') {
		?>
		<div style="float: right;">TVIRTINU<br>
Direktorius<br>
_______________<br>
(parašas)<br>
_______________<br>
(data)<br>
<?php if(DB_PREFIX == 'kretPasak_') { ?>Zita Domarkienė<?php } ?></div>
		<div class="cl"></div>
		<?php
	}
	$darzelio_nr = getConfig('darzelio_nr', $data_max);
	echo (!empty($darzelio_nr) ? "Darželio nr. ".filterText($darzelio_nr).", " : '').filterText(getConfig('darzelio_pavadinimas', $data_max)).". <span style=\"text-transform:uppercase\">".filterText($grupes_pav)."</span> grupės lankomumo apskaitos žiniaraštis (tabelis) už ".year($year)."-".men($month).'.<br>';

	$darbo_dienos = db_query("SELECT `".DB_attendance."`.`data`, DAY(`".DB_attendance."`.`data`) AS `diena`
		FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<='".db_fix($data_max)."' AND `grupes_id`=".(int)$_GET['grupes_id']." GROUP BY `parent_kid_id` --  AND `archyvas`=0
		) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
			JOIN `".DB_attendance."` ON cr.`parent_kid_id`=`".DB_attendance."`.vaiko_id
		WHERE cr.`isDeleted`= 0 AND YEAR(`".DB_attendance."`.`data`)=".year($year)." AND MONTH(`".DB_attendance."`.`data`)=".$month."
		GROUP BY DAY(`".DB_attendance."`.`data`)");
	$kiek_darbo_dienu = mysqli_num_rows($darbo_dienos);
	
	
	/*$sql = "SELECT `".DB_attendance."`.*, cr.* -- , `".DB_tabeliai."`.`pastabos`, COUNT(*) AS `kiek`, SUM(`yra`) AS `buvo`
		FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<='".db_fix($data_max)."' AND `grupes_id`=".(int)$_GET['grupes_id']." GROUP BY `parent_kid_id` --  AND `archyvas`=0
		) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
			JOIN `".DB_attendance."` ON cr.`parent_kid_id`=`".DB_attendance."`.vaiko_id AND cr.`valid_from`<=`".DB_attendance."`.`data`
		LEFT JOIN `".DB_tabeliai."` ON (`".DB_tabeliai."`.`vaiko_id`=cr.`parent_kid_id` AND `".DB_tabeliai."`.`data`='".$data_min."' AND `".DB_tabeliai."`.`group_id`=".(int)$_GET['grupes_id'].")
		WHERE cr.`isDeleted`= 0 AND YEAR(`".DB_attendance."`.`data`)=".$year." AND MONTH(`".DB_attendance."`.`data`)=".$month."
		AND cr.`parent_kid_id`=29
		ORDER BY `vardas`, `pavarde`";*/
	/******$sql = "SELECT `".DB_attendance."`.`vaiko_id`, `".DB_attendance."`.`data`, `".DB_attendance."`.`yra`, `".DB_attendance."`.`justified_d`, `cr`.`ID`, `cr`.`parent_kid_id`, cr.`valid_from`, mx.`valid_from` AS `valid_frommx`
		FROM `".DB_attendance."` 
		JOIN `".DB_children."` cr ON (cr.`parent_kid_id`=`".DB_attendance."`.vaiko_id AND cr.`valid_from`<=`".DB_attendance."`.`data`)
		LEFT JOIN `".DB_children."` mx ON cr.`parent_kid_id`=mx.`parent_kid_id` AND cr.`valid_from`<mx.`valid_from` AND cr.`valid_from`<=`".DB_attendance."`.`data` AND mx.`valid_from`<=`".DB_attendance."`.`data`
		WHERE cr.`isDeleted`= 0 AND YEAR(`".DB_attendance."`.`data`)=".$year." AND MONTH(`".DB_attendance."`.`data`)=".$month." AND cr.`parent_kid_id`=29 AND mx.`parent_kid_id` IS NULL
		-- GROUP BY cr.`parent_kid_id` -- `vardas`, `pavarde`
		ORDER BY cr.`vardas`, cr.`pavarde`, `".DB_attendance."`.`data`";
	$r_vaikas = db_query($sql);
	while ($row = mysqli_fetch_assoc($r_vaikas)) {
		print_r($row);
		//echo $row['data'].' '.$row['yra'];
		echo '<br>';
	}*****/
	
   //SUM(`priezastis`<>'' AND `yra`=0) AS `pateisino`)
   //SUM((SELECT * FROM `".DB_documents."` `d`, `".DB_justified_d."` `p` WHERE `d`.`ID`=`p`.`dok_id` AND `d`.`vaiko_id`=`".DB_attendance."`.`vaiko_id`) AND `yra`=0) AS `pateisino`
   
	/*
	Visi lankomumai:
	SELECT `parent_kid_id`, MAX(`valid_from`)
	FROM `".DB_attendance."` JOIN `".DB_children."` cr ON cr.`parent_kid_id`=`".DB_attendance."`.vaiko_id AND cr.`valid_from`<=`".DB_attendance."`.`data)
	GROUP BY `".DB_attendance."`.`parent_kid_id`
	
	SELECT `".DB_attendance."`.*, cr.*,
	FROM `".DB_attendance."` 
	JOIN `".DB_children."` cr ON cr.`parent_kid_id`=`".DB_attendance."`.vaiko_id AND cr.`valid_from`<=`".DB_attendance."`.`data
	LEFT JOIN `".DB_children."` mx ON cr.`parent_kid_id`=mx.`parent_kid_id` AND cr.`valid_from`<mx.`valid_from`
	LEFT JOIN `".DB_tabeliai."` ON (`".DB_tabeliai."`.`vaiko_id`=cr.`parent_kid_id` AND `".DB_tabeliai."`.`data`='".$data_min."' AND `".DB_tabeliai."`.`group_id`=".(int)$_GET['grupes_id'].")
	WHERE mx.`parent_kid_id` IS NULL AND cr.`isDeleted`= 0 AND YEAR(`".DB_attendance."`.`data`)=".$year." AND MONTH(`".DB_attendance."`.`data`)=".$month."
	GROUP BY cr.`parent_kid_id` -- `vardas`, `pavarde`
	ORDER BY `vardas`, `pavarde`
	//Pagal http://dev.mysql.com/doc/refman/5.5/en/example-maximum-column-group-row.html
	//site:mysql.com correlated subquery JOIN MAX
	*/
	
	//Jeigu vaikas ištrintas ką padaryti, kad 
	//if(KaunoVaivoryksteD)
	//	$order = 'cr.`parent_kid_id`,'.orderName('cr', $data_max);
	//else
	if(KaunoVaivoryksteD || KaunoZvangutis || KaunoSanciuLD || KaunoZiburelis || DB_PREFIX == 'knsRok_' || DB_PREFIX == 'pnvKast_')
		$order = 'cr.`asm_sas`,'.orderName('cr', $data_max);
	else
		$order = orderName('cr', $data_max);
		
	$r_vaikai = db_query("SELECT `".DB_attendance."`.*, cr.*, `".DB_tabeliai."`.`pastabos`, `".DB_tabeliai."`.`cell1`, `".DB_tabeliai."`.`cell2`, `".DB_tabeliai."`.`cell3`, `".DB_tabeliai_charge."`.`charge`, COUNT(*) AS `kiek`, SUM(`yra`) AS `buvo`
		FROM `".DB_attendance."` 
		JOIN `".DB_children."` cr ON (cr.`parent_kid_id`=`".DB_attendance."`.vaiko_id AND cr.`valid_from`<=`".DB_attendance."`.`data`)
		LEFT JOIN `".DB_children."` mx ON cr.`parent_kid_id`=mx.`parent_kid_id` AND cr.`valid_from`<mx.`valid_from` AND cr.`valid_from`<=`".DB_attendance."`.`data` AND mx.`valid_from`<=`".DB_attendance."`.`data`
		LEFT JOIN `".DB_tabeliai."` ON (`".DB_tabeliai."`.`vaiko_id`=cr.`parent_kid_id` AND `".DB_tabeliai."`.`data`='".$data_min."' AND `".DB_tabeliai."`.`group_id`=".(int)$_GET['grupes_id'].")
		LEFT JOIN `".DB_tabeliai_charge."` ON (`".DB_tabeliai_charge."`.`kid_id`=cr.`parent_kid_id` AND `".DB_tabeliai_charge."`.`date`='".$data_min."' AND `".DB_tabeliai_charge."`.`group_id`=".(int)$_GET['grupes_id'].")
		WHERE cr.`isDeleted`= 0 AND 
		
		`".DB_attendance."`.`data` BETWEEN '".$data_min."' AND '".$data_max."'
		-- YEAR(`".DB_attendance."`.`data`)=".year($year)." AND MONTH(`".DB_attendance."`.`data`)=".$month."
		 AND cr.`grupes_id`=".(int)$_GET['grupes_id']." AND mx.`parent_kid_id` IS NULL
		GROUP BY cr.`parent_kid_id` -- `vardas`, `pavarde`
		ORDER BY ".$order);//cr.`vardas`, cr.`pavarde`
		
	/*$r_vaikai = db_query("SELECT `".DB_attendance."`.*, cr.*, `".DB_tabeliai."`.`pastabos`, COUNT(*) AS `kiek`, SUM(`yra`) AS `buvo`
		FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<='".db_fix($data_max)."' AND `grupes_id`=".(int)$_GET['grupes_id']." GROUP BY `parent_kid_id` --  AND `archyvas`=0
		) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
			JOIN `".DB_attendance."` ON cr.`parent_kid_id`=`".DB_attendance."`.vaiko_id AND cr.`valid_from`<=`".DB_attendance."`.`data`
		LEFT JOIN `".DB_tabeliai."` ON (`".DB_tabeliai."`.`vaiko_id`=cr.`parent_kid_id` AND `".DB_tabeliai."`.`data`='".$data_min."' AND `".DB_tabeliai."`.`group_id`=".(int)$_GET['grupes_id'].")
		WHERE cr.`isDeleted`= 0 AND YEAR(`".DB_attendance."`.`data`)=".$year." AND MONTH(`".DB_attendance."`.`data`)=".$month."
		GROUP BY cr.`parent_kid_id` -- `vardas`, `pavarde`
		ORDER BY `vardas`, `pavarde`");*///`grupes_id`=".(int)$_GET['grupes_id']." AND 
		// http://stackoverflow.com/questions/5528854/usage-of-mysqls-if-exists
	?>
	<script src="/libs/tablehover/jquery.tablehover.pack.js"></script>
	<!-- <script src="/libs/sortable/stupidtable.min.js"></script> -->
	<?php /* <script src="/libs/jquery-ui/jquery-ui-1.10.4.tooltip.min.js"></script>
	<style type="text/css">
	@import url("/libs/jquery-ui/jquery-ui-1.10.4.tooltip.css");
	</style> */ ?>
	<script>
	var year = <?=$year?>, month = <?=$month?>, group_id = <?=(int)$_GET['grupes_id']?>;
	$(function() {
		$('.editable').tooltip();
		//$("#tabelis-tbl").stupidtable();
	});
	</script>
	<?php
	if(isset($_GET['excel'])) {
		ob_clean();
		ob_start();
		/*echo '<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\">
	<html>
		<head><meta http-equiv=\"Content-type\" content=\"text/html;charset=utf-8\" /></head>
		<body>';*/
	}
	//if(isset($_GET['full']) && KretingosPasakaLD) {
	//	echo '<form method="post">';
	//}
	
	$marks = [];
	$calcDaysSumDefault = [];
	$result = db_query("SELECT * FROM `".DB_attendance_marks."` WHERE `kindergarten_id`=".DB_ID." AND `isDeleted`=0 ORDER BY `abbr`");
	while($row = mysqli_fetch_assoc($result)) {
		$marks[$row['ID']] = $row;//abbr, title
		if($row['calcDaysSum'])
			$calcDaysSumDefault[$row['ID']] = 0;
	}
	$calcDaysSumTotal = $calcDaysSumDefault;
	?>
	<table id="tabelis-tbl">
	<tr>
		<?=(isset($_GET['full']) ? '<th rowspan="2">Eil.<br>nr.</th>' : '')?>
		<th rowspan="2" data-sort="int">Ko-<br>das<!-- Asm.<br>sąsk. --></th>
		<th rowspan="2"><?=(KaunoZiburelis ? 'Pavardė Vardas' : getName('Vardas', 'Pavardė', $data_max) )?></th>
		<th colspan="<?php echo ($kiek_darbo_dienu+(isset($_GET['summary']) ? 0 : (isset($_GET['svietimui']) ? 1 : (isset($_GET['full']) ? 2 : 1)))); ?>">Dirbtos&nbsp;darželio&nbsp;darbo&nbsp;dienos</th>
		<?=(!isset($_GET['summary']) ? '<th colspan="2">Dienos</th>' : '')?>
		<th colspan="2">Praleista</th>
		<?php
		if(!isset($_GET['summary'])) {
			if($showPayment || PanevezioVaivoryksteLD) {
				$currency = 'Lt';
				if($year.'-'.men($month) >= '2015-01')
					$currency = '€';
			
				if(!(isset($_GET['food']) || isset($_GET['foodDetails']))) {
					if(KaunoRBaibokyne) {
						echo '<th colspan="3">Mokėjimui ('.$currency.')</th>';
					} else {
						if(!PanevezioVaivoryksteLD)
							echo '<th rowspan="2" title="Sistemos suskaičiuota reikšmė"'.(true || KESV ? ' class="no-print"' : '').'><span class="abbr">Mokėji-<br>mui ('.$currency.')</span> <span class="no-print">Sist. sk.</span></th><!-- Mokėjimas -->';
						//if(KESV)
							echo '<th rowspan="2" title="Reikšmė, kurią naudotojas nukopijavo mėnesio gale iš sistemos suskaičiuotos reikšmės arba žmogaus įrašytos"><span class="abbr">Mokėji-<br>mui ('.$currency.')</span> <span class="no-print">Išsaug.</span></th><!-- Mokėjimas -->';
							//Reikšmė, kurią naudotojas išsaugojo mėnesio gale arba naudotas įrašė
					}
				}
			}
		} else
			echo '<th rowspan="2">Ap-<br>mo-<br>ka-<br>mos</th>';
		
		$additional_columns = 0;
		$additional_columns_without_calc = [1 => 0, 2 => 0, 3 => 0];
		if(isset($_GET['full']) || isset($_GET['svietimui']) || isset($_GET['summary'])) {
			if(!empty(getConfig('tabelyje_papildomo_stulpelio_nr._1_pavadinimas', $data_max))) {
				echo '<th rowspan="2">'.filterText(getConfig('tabelyje_papildomo_stulpelio_nr._1_pavadinimas', $data_max)).'</th>';
				++$additional_columns;
				$additional_columns_without_calc[1] = 1;
				if(getConfig('tabelyje_papildomo_stulpelio_nr._1_apačioje_ar_skaičiuoti_sumą/kiekį', $data_max))
					$additional_columns_without_calc[1] = 2;
			}
			if(!empty(getConfig('tabelyje_papildomo_stulpelio_nr._2_pavadinimas', $data_max))) {
				echo '<th rowspan="2">'.filterText(getConfig('tabelyje_papildomo_stulpelio_nr._2_pavadinimas', $data_max)).'</th>';
				++$additional_columns;
				$additional_columns_without_calc[2] = 1;
				if(getConfig('tabelyje_papildomo_stulpelio_nr._1_apačioje_ar_skaičiuoti_sumą/kiekį', $data_max))
					$additional_columns_without_calc[2] = 2;
			}
			if(!empty(getConfig('tabelyje_papildomo_stulpelio_nr._3_pavadinimas', $data_max))) {
				echo '<th rowspan="2">'.filterText(getConfig('tabelyje_papildomo_stulpelio_nr._3_pavadinimas', $data_max)).'</th>';
				++$additional_columns;
				$additional_columns_without_calc[3] = 1;
				if(getConfig('tabelyje_papildomo_stulpelio_nr._1_apačioje_ar_skaičiuoti_sumą/kiekį', $data_max))
					$additional_columns_without_calc[3] = 2;
			}
		}
		
		//if(isset($_GET['full']) && DB_PREFIX == 'sauZiog_')
		//	echo '<th rowspan="2">Li-<br>ga</th>';
		foreach($calcDaysSumDefault as $ID => $dumb)
			echo '<th rowspan="2">'.$marks[$ID]['calcDaysSumTitle'].'</th>';
			
		if(isset($_GET['full']) || (isset($_GET['summary']) && DB_PREFIX == 'tauKodel_') || (isset($_GET['foodDetails']) && DB_PREFIX == 'molSau_'))
			echo '<th rowspan="2"><span class="abbr" title="Sumažintas mokestis">Maž.<br>mok.</span>'.($eat_print ? ', mait.' : '').'</th>';//Soc.<br>rem.
		
		if(isset($_GET['food'])) {
			echo '<th rowspan="2">Pus-<br>ryčių<br>iš viso</th>';
			echo '<th rowspan="2">Pietų<br>iš<br>viso</th>';
			echo '<th rowspan="2">Pava-<br>karių<br>iš viso</th>';
			echo '<th rowspan="2">Vaka-<br>rienių<br>iš viso</th>';
			echo '<th rowspan="2">Mait-<br>inimų<br>iš viso</th>';
			echo '<th rowspan="2">Aler-<br>giškų<br>iš viso</th>';
		}
		if($sum_a_day) {
			echo '<th rowspan="2"><span class="abbr">Suma už dieną</span></th>';
		}
		
		//
		?>
		<th rowspan="2" title="Nelankymo priežastys (pagrindas)">Pastabos</th>
	</tr>
	<tr>
		<?php
		$weekDay = array();
		$dienu_arr = array();
		$lanke_dienu = array();
		$neatvyko_dienu = array();
		
		$pusryciu = array();
		$pietu = array();
		$pavakariu = array();
		$vakarieniu = array();
		while ($row = mysqli_fetch_assoc($darbo_dienos)) {
			$dayOfWeek = date('N', strtotime($row['data']));
			echo '<th class="days">'.mb_substr(mb_lcfirst($dienos[$dayOfWeek]), 0, 2).'<br>'.$row['diena']."</th>";
			$dienu_arr[] = $row['diena'];
			$weekDay[$row['diena']] = $dayOfWeek;
			$lanke_dienu[$row['diena']] = 0;
			$neatvyko_dienu[$row['diena']] = 0;
			
			$pusryciu[$row['diena']] = 0;
			$pietu[$row['diena']] = 0;
			$pavakariu[$row['diena']] = 0;
			$vakarieniu[$row['diena']] = 0;
		}
		?>
		<?=(!isset($_GET['summary']) ? '<th>Iš<br>viso</th>' : '')?>
		<?=(isset($_GET['full']) ? '<th title="Kiek dienų žymėtas lankomumas"><span class="abbr">Žy-<br>mėta</span></th>' : '')?>
		<?=(!isset($_GET['summary']) ? ' <th>Lanky-<br>tos</th><th>Apmo-<br>kamos</th>' : '')?>
		<th>Iš<br>viso</th>
		<th>Patei-<br>sinta</th>
		<?php
			if(!isset($_GET['summary'])) {
				//if($showPayment) {
				if(KaunoRBaibokyne) {
					echo '<th>Maistas</th><!-- Maistas<br>ir<br>ugdymas -->
						  <th>Dienos<br>mokestis</th><!-- Išlaikymas -->
						  <th>Iš viso</th>';
				}
				//}
			}
		?>
	</tr>
	<?php
	$i = 1;
	$sum_lankytos = 0;
	$sum_apmokamos = 0;
	$sum_praleista_isviso = 0;
	$sum_praleista_pateisinta = 0;
	$sum_mokejimui = 0;
	$sum_mokejimui_saved = 0;
	$sum_disease = 0;
	$column = [1 => 0, 2 => 0, 3 => 0];
	//$kids = [];
	//$kids_ids = [];
	while ($row = mysqli_fetch_assoc($r_vaikai)) {
		//if(isset($_GET['full']) && KretingosPasakaLD && isset($_POST['kid']) && !in_array($row['parent_kid_id'], $_POST['kid']))
		//	continue;
		//Validation/Checking
		$exists_in_group = false;
		$pre_date = $year.'-'.men($month).'-';
		foreach($dienu_arr as $diena) {
			$kid = getKid($row['parent_kid_id'], $pre_date.dien($diena));
			if( $kid != false && $kid['grupes_id'] == (int)$_GET['grupes_id'] && !$kid['isDeleted']
				&& (!isset($_GET['pre_school_free_lunch']) || isset($_GET['pre_school_free_lunch']) && $kid['arDarzelyje'] == 2 && $kid['free_lunch']))//Pagal idėją turėtų būti && !$kid['archyvas']
				$exists_in_group = true;
		}
		if(!$exists_in_group) continue;
		
	/*	$kids[] = $row;
		$kids_ids[] = (int)$row['parent_kid_id'];
	}
	if(!empty($kids)) {
		$vaiko_lankomumas = db_query("SELECT *, DAY(`".DB_attendance."`.`data`) AS `diena`
				FROM `".DB_attendance."` 
				WHERE `".DB_attendance."`.`vaiko_id` IN (".implode(',', $kids_ids).") AND 
				-- YEAR(`".DB_attendance."`.`data`)=".year($year)." AND MONTH(`".DB_attendance."`.`data`)=".$month."
				`".DB_attendance."`.`data` BETWEEN '".$data_min."' AND '".$data_max."'
				ORDER BY `".DB_attendance."`.`data`");
		$attendance = [];
		while ($lankomumas = mysqli_fetch_assoc($vaiko_lankomumas))
			$attendance[$lankomumas['vaiko_id']][] = $lankomumas;
	}
	if(!empty($kids)) foreach($kids as $row) {*/
			
			//TODO: adapt to revisions:
		   /* $vaiko_lankomumas = db_query("SELECT *, DAY(`".DB_attendance."`.`data`) AS `diena`
				FROM `".DB_attendance."` JOIN `".DB_children."` cr ON cr.`parent_kid_id`=`".DB_attendance."`.vaiko_id
				JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<=`".DB_attendance."`.`data` GROUP BY `parent_kid_id` -- man rodos dar turi būti .`isDeleted`= 0 AND .`archyvas`=0, jeigu vaikas išeitų viduryje mėnesio, bet tada vėlgi...
		-- http://stackoverflow.com/questions/5528854/usage-of-mysqls-if-exists
		) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
		
				WHERE `".DB_attendance."`.`vaiko_id`=".(int)$row['vaiko_id']." AND YEAR(`".DB_attendance."`.`data`)=".$year." AND MONTH(`".DB_attendance."`.`data`)=".$month."
				ORDER BY `".DB_attendance."`.`data`");
				*/
			/*$vaiko_lankomumas = db_query("SELECT *, DAY(`".DB_attendance."`.`data`) AS `diena`
				FROM `".DB_attendance."` JOIN `".DB_children."` cr ON `".DB_attendance."`.vaiko_id=cr.`parent_kid_id`
				JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` GROUP BY `parent_kid_id` -- man rodos dar turi būti .`isDeleted`= 0 AND .`archyvas`=0, jeigu vaikas išeitų viduryje mėnesio, bet tada vėlgi...
		-- http://stackoverflow.com/questions/5528854/usage-of-mysqls-if-exists
		) fi ON (cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from` AND fi.`valid_from`<=`".DB_attendance."`.`data`)
				WHERE `".DB_attendance."`.`vaiko_id`=".(int)$row['vaiko_id']." AND YEAR(`".DB_attendance."`.`data`)=".$year." AND MONTH(`".DB_attendance."`.`data`)=".$month."
				ORDER BY `".DB_attendance."`.`data`");*/
				
			/* Dar ne $vaiko_lankomumas = db_query("SELECT `".DB_attendance."`.*, cr.*, DAY(`".DB_attendance."`.`data`) AS `diena`
				FROM `".DB_attendance."` 
				JOIN `".DB_children."` cr ON (`".DB_attendance."`.vaiko_id=cr.`parent_kid_id`)
				LEFT JOIN `".DB_children."` fi ON cr.`parent_kid_id` = fi.`parent_kid_id` AND cr.`valid_from` < fi.`valid_from` AND fi.`valid_from`<=`".DB_attendance."`.`data` AND cr.`valid_from`<=`".DB_attendance."`.`data`
				WHERE fi.`parent_kid_id` IS NULL AND `".DB_attendance."`.`vaiko_id`=".(int)$row['vaiko_id']." AND YEAR(`".DB_attendance."`.`data`)=".$year." AND MONTH(`".DB_attendance."`.`data`)=".$month." AND cr.`grupes_id`=".(int)$_GET['grupes_id']."
				ORDER BY `".DB_attendance."`.`data`");*/
				
				$vaiko_lankomumas = db_query("SELECT *, DAY(`".DB_attendance."`.`data`) AS `diena`
				FROM `".DB_attendance."` 
				WHERE `".DB_attendance."`.`vaiko_id`=".(int)$row['parent_kid_id']." AND 
				-- YEAR(`".DB_attendance."`.`data`)=".year($year)." AND MONTH(`".DB_attendance."`.`data`)=".$month."
				`".DB_attendance."`.`data` BETWEEN '".$data_min."' AND '".$data_max."'
				ORDER BY `".DB_attendance."`.`data`");
				
				/****
				$vaiko_lankomumas = db_query("SELECT `".DB_attendance."`.*, `DAY(`".DB_attendance."`.`data`) AS `diena`
				FROM `".DB_attendance."` 
				JOIN `".DB_children."` cr ON (cr.`parent_kid_id`=`".DB_attendance."`.vaiko_id AND cr.`valid_from`<=`".DB_attendance."`.`data`)
				LEFT JOIN `".DB_children."` mx ON cr.`parent_kid_id`=mx.`parent_kid_id` AND cr.`valid_from`<mx.`valid_from` AND cr.`valid_from`<=`".DB_attendance."`.`data` AND mx.`valid_from`<=`".DB_attendance."`.`data`
				WHERE cr.`isDeleted`= 0 AND YEAR(`".DB_attendance."`.`data`)=".$year." AND MONTH(`".DB_attendance."`.`data`)=".$month." AND cr.`parent_kid_id`=29 AND mx.`parent_kid_id` IS NULL
				-- GROUP BY cr.`parent_kid_id` -- `vardas`, `pavarde`
				ORDER BY cr.`vardas`, cr.`pavarde`, `".DB_attendance."`.`data`"
				****/
				
			/*$yra = array();
			while ($row2 = mysqli_fetch_assoc($vaiko_lankomumas))
				$yra[$row2['diena']] = $row2['yra'] == 0 && in_array($row2['diena'], $rastelis) ? (in_array($row2['diena'], $rastelis) && $row2['yra'] == 0 ? -1 : 0) : $row2['yra'];
				//$yra[$row2['diena']] = $row2['yra'] == 0 && $row2['priezastis'] != '' ? ($row2['priezastis'] != '' && $row2['yra'] == 0 ? -1 : 0) : $row2['yra'];
			foreach($dienu_arr as $val)
				echo "<td>".(array_key_exists($val, $yra) ? ($yra[$val] == 1 ? '+' : ($yra[$val] == -1 ? 'p' : 'n')) : '')."</td>";*/
				
			$yra = array();
			$mark = array();
			$justifies = array();
			if(KaunoRBaibokyne) {
				$maistas_ugdymas = 0;
				$islaikymas = 0;
			} else {
				$total_kid_cost = 0;
			}
			$buvo = 0;//Lankytos
			$zymeta = 0;//Žymėta
			$kid_food = 0;
			$additional_cost_per_day = 0;
			$mismatch_warning = array();
			$real_payment_days = 0;
			$soc_rem = array();
			$total_eat = array();
			$eat = array();
			$eated = array();
			$eatedVisual = [];
			$eatedPus = 0;
			$eatedPiet = 0;
			$eatedPavak = 0;
			$eatedVakar = 0;
			$allergicCnt = 0;
			$calcDaysSum = $calcDaysSumDefault;
			while ($lankomumas = mysqli_fetch_assoc($vaiko_lankomumas)) {
			//foreach($attendance[$row['parent_kid_id']] as $lankomumas) {
				//Prepare cost calculation
				$data_dabar = $year.'-'.men($month).'-'.dien($lankomumas['diena']);
				$kid = getKid($lankomumas['vaiko_id'], $data_dabar);
				if($kid == false || $kid['archyvas'] || $kid['isDeleted']/*Just to make sure*/) {
					$mismatch_warning[$lankomumas['diena']] = ($kid == false ? 'neįregistruotas' : ($kid['archyvas'] ? 'išregistruotas' : 'ištrintas'));
				}
				if($kid['grupes_id'] == (int)$_GET['grupes_id']) {
					$lankomumas = array_merge($kid, $lankomumas);
					//print_r($lankomumas);
					
					$soc_rem[$lankomumas['arNuolaida']] = 1;
					if($lankomumas['arPusryciaus'])
						$total_eat[0] = 'pusryt.'.($lankomumas['free_breakfast'] ? ' (100 %)' : '');//pusr/psr //nemok.
					if($lankomumas['arPietaus'])
						$total_eat[1] = 'piet.'.($lankomumas['free_lunch'] ? ' (100 %)' : '');//pt
					if($lankomumas['arPavakariaus'])
						$total_eat[2] = 'pavak.'.($lankomumas['free_afternoon_tea'] ? ' (100 %)' : '');//pav
					if($lankomumas['arVakarieniaus'])
						$total_eat[4] = 'vakarien.'.($lankomumas['free_dinner'] ? ' (100 %)' : '');
					$buvo += $lankomumas['yra'];
					++$zymeta;
					$yra[$lankomumas['diena']] = $lankomumas['yra'];
					$mark[$lankomumas['diena']] = $lankomumas['additional_mark_id'];
					//if(isset($calcDaysSum[$lankomumas['additional_mark_id']]))
					//	++$calcDaysSum[$lankomumas['additional_mark_id']];
					$justifies[$lankomumas['diena']] = $lankomumas['justified_d'];
					if(!KaunoVaikystesLobiai) {
						$eat[$lankomumas['diena']] = ($lankomumas['arPusryciaus'] ? 'Pusryčiauja'.($lankomumas['free_breakfast'] ? ' 100 % nuolaida' : '').'. ' : '').
						($lankomumas['arPietaus'] ? 'Pietauja'.($lankomumas['free_lunch'] ? ' nuolaida 100 %' : '').'. ' : '').
						($lankomumas['arPavakariaus'] ? 'Pavakarieniauja'.($lankomumas['free_afternoon_tea'] ? ' nuolaida 100 %' : '').'. ' : '').
						($lankomumas['arVakarieniaus'] ? 'Vakarieniauja'.($lankomumas['free_dinner'] ? ' nuolaida 100 %' : '').'. ' : '').' Mokestis mažinamas '.$nuolaidos_dyd[$lankomumas['arNuolaida']].'.';//Soc. rem.
						
						$eat[$lankomumas['diena']] = ($lankomumas['arPusryciaus'] ? 'Pusryčiauja'.($lankomumas['free_breakfast'] ? ' 100 % nuolaida' : '').'. ' : '').
						($lankomumas['arPietaus'] ? 'Pietauja'.($lankomumas['free_lunch'] ? ' nuolaida 100 %' : '').'. ' : '').
						($lankomumas['arPavakariaus'] ? 'Pavakarieniauja'.($lankomumas['free_afternoon_tea'] ? ' nuolaida 100 %' : '').'. ' : '').
						($lankomumas['arVakarieniaus'] ? 'Vakarieniauja'.($lankomumas['free_dinner'] ? ' nuolaida 100 %' : '').'. ' : '').' Mokestis mažinamas '.$nuolaidos_dyd[$lankomumas['arNuolaida']].'.';//Soc. rem.
						
					}
					if(isset($yra[$lankomumas['diena']]) && $yra[$lankomumas['diena']] == 1) {
						if($lankomumas['arPusryciaus']) {
							++$eatedPus;
							++$pusryciu[$lankomumas['diena']];
						}
						if($lankomumas['arPietaus']) {
							++$eatedPiet;
							++$pietu[$lankomumas['diena']];
						}
						if($lankomumas['arPavakariaus']) {
							++$eatedPavak;
							++$pavakariu[$lankomumas['diena']];
						}
						if($lankomumas['arVakarieniaus']) {
							++$eatedVakar;
							++$vakarieniu[$lankomumas['diena']];
						}
						if($lankomumas['allergic'])
							++$allergicCnt;
						$eated[$lankomumas['diena']] = $lankomumas['arPusryciaus']+$lankomumas['arPietaus']+$lankomumas['arPavakariaus']+$lankomumas['arVakarieniaus'];
						//$size = 7;
						$y = function($name){ global $size; return '<div class="ordered"><div>'.$name.'</div></div>'; };
						$n = function(){ global $size; return '<div class="notOrdered">&nbsp;</div>';};
						$eatedVisual[$lankomumas['diena']] = ($lankomumas['arPusryciaus'] ? $y('1.Pus.') : $n()).($lankomumas['arPietaus'] ? $y('2.Piet.') : $n()).($lankomumas['arPavakariaus'] ? $y('3.Pav.') : $n()).($lankomumas['arVakarieniaus'] ? $y('4.Vak.') : $n());
					}
					
					//Cost calculation
					$nuolaid = $nuolaidos[$lankomumas['arNuolaida']];
					
					$kid_level = array(
						0 => 'lopselyje',
						1 => 'darzelyje',
						2 => 'priesmokykliniame',
						//3 => 'pradiniame'
					);
					if(isset($kid_level[$lankomumas['arDarzelyje']])) {//Else may be pradinis ugdymas
						$cur_kid_level = $kid_level[$lankomumas['arDarzelyje']];
					
						$doRound = getConfig('atskirai_apvalinti_kiekvieno_maitinimo_karto_ir_ugdymo_kainas', $data_dabar);
						$day_food = $lankomumas['arPusryciaus']*!$lankomumas['free_breakfast']*(double)getConfig('vieneriu_pusryciu_kaina_'.$cur_kid_level, $data_dabar)+$lankomumas['arPietaus']*!$lankomumas['free_lunch']*(double)getConfig('vieneriu_pietu_kaina_'.$cur_kid_level, $data_dabar)+$lankomumas['arPavakariaus']*!$lankomumas['free_afternoon_tea']*(double)getConfig('vieneriu_pavakariu_kaina_'.$cur_kid_level, $data_dabar)+$lankomumas['arVakarieniaus']*!$lankomumas['free_dinner']*(double)getConfig('vienerios_vakarienes_kaina_'.$cur_kid_level, $data_dabar);
						$ugdym = (double)kaina_ugdymo_reikmiu($lankomumas['arDarzelyje'], $data_dabar);
					
						if(KaunoRBaibokyne) {
							if($lankomumas['yra'] || !$lankomumas['yra'] && !$lankomumas['justified_d']) {
								$maistas_ugdymas += $day_food + $nuolaid*((double)getConfig('ugdymo_dienos_kaina', $data_dabar));
								$dienos_mokestis = $lankomumas['dienosMokestis'] ? (double)getConfig('puses_dienos_kaina', $data_dabar) : (double)getConfig('pilnos_dienos_kaina', $data_dabar);
								$islaikymas += $nuolaid*((double)getConfig('islaikymo_dienos_kaina', $data_dabar)+$dienos_mokestis);
								//Maistas - $maistas_ugdymas
								//Dienos mokestis - $islaikymas = išlaikymo dienos kaina=0 + dienos mokestis
								//Iš viso
							}
						} elseif(KaunoVaikystesLobiai) {
							$nuolaid = 1;
							if($lankomumas['yra'] || !$lankomumas['yra'] && !$lankomumas['justified_d']) {//Būtoms ir nelankytoms be pateisinimo
								$kid_food += $lankomumas['dienosMokestis'] ? (double)getConfig('puses_dienos_maitinimas_per_d._d.', $data_dabar) : (double)getConfig('pilnos_dienos_maitinimas_per_d._d.', $data_dabar);
							}
						} elseif(DruskininkuBitute) {
							//nauja: jeigu serga ir pateisinta tada nemoka
							if($lankomumas['yra'] || !$lankomumas['yra'] && !$lankomumas['justified_d']) {//Būtoms ir nelankytoms be pateisinimo
								if($nuolaidos_title[$lankomumas['arNuolaida']] != '100_%_nuolaida')
									$total_kid_cost += (double)getConfig('dienos_kaina_per_d._'.$cur_kid_level.'_'.$nuolaidos_title[$lankomumas['arNuolaida']], $data_dabar);
							} /*else {
								if($cur_kid_level != 'priesmokykliniame' && $nuolaidos_title[$lankomumas['arNuolaida']] != '100_%_nuolaida')
									$total_kid_cost += (double)getConfig('dienos_kaina_per_d._'.$cur_kid_level.'_'.$nuolaidos_title[4], $data_dabar);//80 % nuolaida => *0,2
							}*/
						} else {
							if($lankomumas['yra'] || !$lankomumas['yra'] && !$lankomumas['justified_d']) {//Būtoms ir nelankytoms be pateisinimo
								if($doRound) {
									$day_food = round($lankomumas['arPusryciaus']*!$lankomumas['free_breakfast']*(double)getConfig('vieneriu_pusryciu_kaina_'.$cur_kid_level, $data_dabar)*$nuolaid, 2);
									$day_food += round($lankomumas['arPietaus']*!$lankomumas['free_lunch']*(double)getConfig('vieneriu_pietu_kaina_'.$cur_kid_level, $data_dabar)*$nuolaid, 2);
									$day_food += round($lankomumas['arPavakariaus']*!$lankomumas['free_afternoon_tea']*(double)getConfig('vieneriu_pavakariu_kaina_'.$cur_kid_level, $data_dabar)*$nuolaid, 2);
									$day_food += round($lankomumas['arVakarieniaus']*!$lankomumas['free_dinner']*(double)getConfig('vienerios_vakarienes_kaina_'.$cur_kid_level, $data_dabar)*$nuolaid, 2);
								
									if($apply_discount_for_educational_needs_fee_each_day) {
										$ugdym = round($ugdym*$nuolaid, 2);
										$total_kid_cost += $day_food + $ugdym + (double)getConfig('papildoma_kaina_uz_patiekalu_gamyba_per_d._d.', $data_dabar)*$nuolaid;
									} else {
										$total_kid_cost += ($day_food + (double)getConfig('papildoma_kaina_uz_patiekalu_gamyba_per_d._d.', $data_dabar))*$nuolaid;
										$total_kid_cost += $ugdym;
									}
								} else {
									if($apply_discount_for_educational_needs_fee_each_day) {
										$total_kid_cost += ($day_food+$ugdym+(double)getConfig('papildoma_kaina_uz_patiekalu_gamyba_per_d._d.', $data_dabar)) *$nuolaid;
									} else {
										$total_kid_cost += ($day_food+(double)getConfig('papildoma_kaina_uz_patiekalu_gamyba_per_d._d.', $data_dabar)) *$nuolaid;
										$total_kid_cost += $ugdym;
									}
								}
							}
						}
					}
					$additional_cost_per_day += (double)$lankomumas['additional_cost_per_day']*$nuolaid;
					if($nuolaidos_title[$lankomumas['arNuolaida']] != '100_%_nuolaida')
						$real_payment_days += $lankomumas['yra'] || !$lankomumas['yra'] && !$lankomumas['justified_d'];
				}
			}
			echo "<tr>
			".(isset($_GET['full']) ? "<td>".$i++/*.(isset($_GET['full']) && KretingosPasakaLD ? ' <input type="checkbox" name="kid[]" value="'.$row['parent_kid_id'].'" class="no-print">' : '')*/."</td>" : '')."
			<td>".filterText($row['asm_sas'])."</td>
			<td>".( KaunoZiburelis ? filterText($row['pavarde']).' '.filterText($row['vardas']) : filterText(getName($row['vardas'], $row['pavarde'], $data_max)) )."</td>";
			
			$pateisino_dienu = 0;
			$disease = 0;
			foreach($dienu_arr as $diena) {
				//if(DB_PREFIX == 'sauZiog_' && isset($mark[$diena]) && $mark[$diena] == 5) {
				//	++$disease;
				//}
				if(isset($mark[$diena]) && isset($calcDaysSum[$mark[$diena]]))
					++$calcDaysSum[$mark[$diena]];
				//Priklausomai nuo to kaip turėtų būti 
				echo "<td".($weekDay[$diena] == 1 ? ' class="Monday"' : '').
					(isset($_GET['foodDetails']) && isset($eatedVisual[$diena]) ? ' style="padding: 0px;"' : '').
					(isset($_GET['foodDetails']) && !isset($eatedVisual[$diena]) ? ' style="text-align: center;"' : '').
					
					(isset($_GET['food']) && isset($eat[$diena]) ? ' title="'.$eat[$diena].'"><span class="abbr"' : '').">".
					(isset($_GET['food']) ? (isset($eated[$diena]) ? $eated[$diena] : '') : 
						(isset($_GET['foodDetails']) && isset($eatedVisual[$diena]) ? (isset($eatedVisual[$diena]) ? $eatedVisual[$diena] : '') : 
							(isset($yra[$diena]) ? ($yra[$diena] == 1 ? ' ' : ($justifies[$diena] > 0 ? 'p' : 'n')) : '-')
						)
					).
					(isset($mark[$diena]) && isset($marks[$mark[$diena]]) ? '<span class="abbr" title="'.$marks[$mark[$diena]]['title'].'">'.$marks[$mark[$diena]]['abbr'].'</span>' : '').
					//Pažymėtas dienos lankomumas, nors vaiko oficialiai jau nėra darželyje (pagal duomenis meniu punkte „Vaikai“). Gali reikėti: 1. Pakeisti duomenis meniu punkte „Vaikai“ (jei netinka duomenų įsigaliojimo data ar išregistravimas). 2. Ištrinti likučius pedagogui peržymint lankomumą meniu punkte „Lankomumas“ (jei lankomumas neturėjo būti pažymėtas).
					//(isset($mismatch_warning[$diena]) ? '<span class="no-print notice abbr" title="Pažymėtas dienos lankomumas neįtrauktas, nors vaikas oficialiai jau '.$mismatch_warning[$diena].' darželyje (pagal duomenis meniu punkte „Vaikai“). Gali reikėti: 1. Pakeisti duomenis meniu punkte „Vaikai“ (jei netinka duomenų įsigaliojimo data ar išregistravimas). 2. Ištrinti likučius pedagogui peržymint lankomumą meniu punkte „Lankomumas“ (jei lankomumas neturėjo būti pažymėtas).">Ž</span>' : '').
					(isset($mismatch_warning[$diena]) ? 
						'<span class="no-print notice '.( (ADMIN || AUKLETOJA) ? ' nZ"' : ' abbr" title="Žymėtas lankomumas neįtrauktas, nes vaikas '.$mismatch_warning[$diena].' pagal duomenis meniu punkte „Vaikai“. Gali reikėti: 1) pakeisti duomenis meniu punkte „Vaikai“ (jei netinka duomenų įsigaliojimo data ar išregistravimas); 2) Ištrinti likučius peržymint lankomumą meniu punkte „Lankomumas“ (jei lankomumas neturėjo būti pažymėtas)."').
						( (ADMIN || AUKLETOJA) ? 
							' data-name="'.$kid['vardas'].' '.$kid['pavarde'].'" data-date="'.$year.'-'.men($month).'-'.$diena.'" data-parent-kid-id="'.$kid['parent_kid_id'].'" data-kid-id="'.$kid['ID'].'"' : 
							'' ).
						'>Ž</span>'
						: '').
					(isset($_GET['food']) && isset($eat[$diena]) ? '</span>' : '')."</td>";
				(isset($yra[$diena]) ? ($yra[$diena] == 1 ? $lanke_dienu[$diena]++ : ($justifies[$diena] > 0 ? $neatvyko_dienu[$diena]++ : $neatvyko_dienu[$diena]++)) : '-');
				if(isset($yra[$diena]))
					$pateisino_dienu += $yra[$diena] == 0 && $justifies[$diena] > 0;
			}
			$apmokama_dienu = /*$row['kiek']*/$zymeta-$pateisino_dienu;
			$sum_disease += $disease;
			foreach($calcDaysSum as $ID => $val)
				$calcDaysSumTotal[$ID] += $val;

			if(MazeikiuGiliukas)
				$apmokamos = "<td>".($apmokama_dienu != $real_payment_days ? '<span title="Būtų '.$apmokama_dienu.' apmokamos dienos jei nebūtų 100 % nuolaidos" class="abbr">'.$real_payment_days.'</span>' : $real_payment_days)."</td>";//Apmokamos
			else
				$apmokamos = "<td>".$apmokama_dienu."</td>";//Apmokamos

			if(!isset($_GET['summary'])) {
				echo '<td>'.$kiek_darbo_dienu.'</td>';//Dirbtos darželio darbo dienos: Iš viso
				
				if(isset($_GET['full']))
					echo '<td>'.$zymeta.' <!-- '.$row['kiek'].' --></td>';//Kiek dienų žymėtas lankomumas
				
				echo "<td>".$buvo." <!-- ".$row['buvo']." --></td>";//Dienos: Lankytos
				echo $apmokamos;//Dienos: Apmokamos
				$sum_lankytos += $buvo;
				if(MazeikiuGiliukas)
					$sum_apmokamos += $real_payment_days;
				else
					$sum_apmokamos += $apmokama_dienu;
			}

			$praleista_is_viso = $row['kiek']-$row['buvo'];
			$sum_praleista_isviso += $praleista_is_viso;
			/*Praleista:Iš viso	*/echo "<td>".$praleista_is_viso."</td>
			<td>".$pateisino_dienu."</td>";//$row['pateisino']."</td>";//Praleista: Pateisinta
			$sum_praleista_pateisinta += $pateisino_dienu;
			
			if(!(isset($_GET['food']) || isset($_GET['foodDetails']))) {
				//Kaina
				//echo "<td>".price(($kiek_dienu-$pateisino_dienu)*$day_food*$nuolaid+$ugdym)."</td>";//Užregistruotos dienos, bet nėra lankomumo
				if(!isset($_GET['summary'])) {
					if($showPayment || PanevezioVaivoryksteLD) {
						if(KaunoRBaibokyne) {
							//$dienos_mokestis = $row['dienosMokestis'] ? (double)getConfig('puses_dienos_kaina', $data_min) : (double)getConfig('pilnos_dienos_kaina', $data_min);
							//$maistas_ugdymas = ($row['kiek']-$pateisino_dienu/*Žymėtos dienos-pateisintos dienos*/) * ($day_food + $nuolaid*((double)getConfig('ugdymo_dienos_kaina', $data_min)/*+$dienos_mokestis*/));
							//$islaikymas = ($row['kiek']-$pateisino_dienu/*Žymėtos dienos-pateisintos dienos*/) * $nuolaid*((double)getConfig('islaikymo_dienos_kaina', $data_min)+$dienos_mokestis);
							//Nuolaida skaičiuojasi tik nuo ugdymo, išlaikymo, bet ne nuo maisto.
							echo "<td class=\"money\">".price($maistas_ugdymas, true)."</td>
								  <td class=\"money\">".price($islaikymas, true)."</td>
								  <td class=\"money\">".price($maistas_ugdymas+$islaikymas, true)."</td>";
						} elseif(KaunoVaikystesLobiai) {
							$kid = getKid($row['parent_kid_id'], $data_max);
							$max_food_cost = $kid['dienosMokestis'] ? (double)getConfig('puses_dienos_maitinimo_per_men._sumos_riba', $data_max) : (double)getConfig('pilnos_dienos_maitinimo_per_men._sumos_riba', $data_max);
							if($kid_food > $max_food_cost) {
								$kid_food = $max_food_cost;
							}
							$montly_day_cost = ($kid['dienosMokestis'] ? (double)getConfig('puses_dienos_kaina_per_men.', $data_max) : (double)getConfig('pilnos_dienos_kaina_per_men.', $data_max) * ((100-(double)$kid['percentage_discount_from_day_tax_per_month'])/100));
							$montly_day_cost -= (double)$kid['monthly_discount_sum'];
							$total_kid_cost = $montly_day_cost + $kid_food + $additional_cost_per_day;		 		
							/* (pilnos_ar_pusės_dienos_kaina_per_mėn.*((100 - nuolaidos_procentai_nuo_dienos_mokesčio_per_mėn.)/100) +
		 (vaikui_žymėtos_lankomumo_dienos - vaikui_pateisintos_dienos)*maitinimas_vienam_vaikui_per_pilną/pusę_dienos +
		  suma_lankomumo_laukelio_kiekvienos_dienos_„Papildoma kaina“)
		 - mėnesinės_nuolaidos_suma */
							echo "<td class=\"money".(true || KESV ? ' no-print' : '')."\">".price($total_kid_cost, true)."=".price($montly_day_cost, true)."(be <span title=\"".getConfig('pavadinimas_laukeliui_menesines_nuolaidos_suma', $data_max)."\">".price($kid['monthly_discount_sum'], true)."</span>)+".price($kid_food, true)."+".price($additional_cost_per_day, true)."</td>";//Mokėjimui (Lt) 
							//if(KESV) {
								if(isset($_GET['save']))
									$row['charge'] = $total_kid_cost;
								$show_saved_price = ($row['charge'] == '' ? '' : filterText(price($row['charge'], true)));
								$print_price = ($show_saved_price == '' ? price($total_kid_cost, true) : $show_saved_price);
								
								$editable = $allowedToEditPayment && !$currently_locked ? ' class="money click-to-edit-money editable" title="Paspauskite, kad keistumėte"' : ' class="money"';
								echo "<td$editable data-id=\"".$row['parent_kid_id']."\"><span class=\"no-print\">".$show_saved_price."</span><span class=\"on-print\">".$print_price."</span></td>";//Mokėjimui (Lt) 
							//}
							$sum_mokejimui += round($total_kid_cost, 2);
							$sum_mokejimui_saved += round($show_saved_price == '' ? $total_kid_cost : (double)str_replace(',', '.', $show_saved_price), 2);
						} else {
							$kid = getKid($row['parent_kid_id'], $data_max);
							if($kid['arDarzelyje'] <= 2)//Iki priešmokyklinio
								$total_kid_cost += (double)kaina_ugdymo_reikmiu_men($kid['arDarzelyje'], $data_max);
							$total_kid_cost += $additional_cost_per_day;
							$total_kid_cost += (double)$kid['feeForPlace'];
							if(TauragesRAzuoliukas || TESTINIS) {
								$sport = (double)((int)$kid['attend_sport']*(double)getConfig('kaina_uz_sporta_per_men.,_taikoma_pazymejus_vaiko_duomenyse', $data_max));
								$total_kid_cost += $sport;
							}
							//$kaina2 = ($row['kiek']-$pateisino_dienu/*Žymėtos dienos-pateisintos dienos*/) * ($day_food+$ugdym) *$nuolaid;
							//<!-- ".(($row['kiek']-$pateisino_dienu/*Žymėtos dienos-pateisintos dienos*/) * ($day_food+$ugdym))." -->
							if(!PanevezioVaivoryksteLD)
								echo "<td class=\"money".(true || KESV ? ' no-print' : '')."\">".price($total_kid_cost, true)."</td>";//Mokėjimui (Lt) 
							//if(KESV) {
								if(isset($_GET['save']))
									$row['charge'] = $total_kid_cost;
								$show_saved_price = ($row['charge'] == '' ? '' : filterText(price($row['charge'], true)));
								if(PanevezioVaivoryksteLD)
									$print_price = ($show_saved_price == '' ? '' : $show_saved_price);
								else
									$print_price = ($show_saved_price == '' ? price($total_kid_cost, true) : $show_saved_price);
								$editable = $allowedToEditPayment && !$currently_locked ? ' class="money click-to-edit-money editable" title="Paspauskite, kad keistumėte"' : ' class="money"';
								echo "<td$editable data-id=\"".$row['parent_kid_id']."\"><span class=\"no-print\">".$show_saved_price."</span><span class=\"on-print\">".$print_price."</span></td>";//Mokėjimui (Lt) 
							//}
							$sum_mokejimui += round($total_kid_cost, 2);
							if(PanevezioVaivoryksteLD)
								$sum_mokejimui_saved += round($show_saved_price == '' ? 0 : (double)str_replace(',', '.', $show_saved_price), 2);
							else
								$sum_mokejimui_saved += round($show_saved_price == '' ? $total_kid_cost : (double)str_replace(',', '.', $show_saved_price), 2);
						}
					}
					if(!KaunoRBaibokyne) {
						if(isset($_GET['save']) && !$currently_locked) {
							if(!mysqli_query($db_link, "INSERT INTO ".DB_tabeliai_charge." 
								SET `group_id`=".(int)$_GET['grupes_id'].", `kid_id`=".(int)$row['parent_kid_id'].", `date`='".$data_min."',
								`charge`='".db_fix($total_kid_cost)."', `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."',
								`updated`=CURRENT_TIMESTAMP, `updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."'"))
							{
								db_query("UPDATE ".DB_tabeliai_charge." 
									SET `charge`='".db_fix($total_kid_cost)."', `updated`=CURRENT_TIMESTAMP, `updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."', `updatedCounter`=`updatedCounter`+1
									WHERE `group_id`=".(int)$_GET['grupes_id']." AND `kid_id`=".(int)$row['parent_kid_id']." AND `date`='".$data_min."'");
							}
						}
					}
					//echo "<td>".(($row['kiek']-$row['pateisino'])*$day_food*$nuolaid+$ugdym)."</td>";
				} else
					echo $apmokamos;//Apmokamos dienos
			}
			if(isset($_GET['full']) || isset($_GET['svietimui']) || isset($_GET['summary'])) {
				if(!empty(getConfig('tabelyje_papildomo_stulpelio_nr._1_pavadinimas', $data_max))) {
					if($currently_locked)
						echo '<td>'.filterText($row['cell1']).'</td>';
					else
						echo '<td class="click-to-edit editable" title="Paspauskite, kad redaguotumėte" data-id="'.$row['parent_kid_id'].'" data-type="cell1">'.filterText($row['cell1']).'</td>';
						
					$column[1] += (float)$row['cell1'];
				}
				if(!empty(getConfig('tabelyje_papildomo_stulpelio_nr._2_pavadinimas', $data_max))) {
					if($currently_locked)
						echo '<td>'.filterText($row['cell2']).'</td>';
					else
						echo '<td class="click-to-edit editable" title="Paspauskite, kad redaguotumėte" data-id="'.$row['parent_kid_id'].'" data-type="cell2">'.filterText($row['cell2']).'</td>';
					$column[2] += (float)$row['cell2'];
				}
				if(!empty(getConfig('tabelyje_papildomo_stulpelio_nr._3_pavadinimas', $data_max))) {
					if($currently_locked)
						echo '<td>'.filterText($row['cell3']).'</td>';
					else
						echo '<td class="click-to-edit editable" title="Paspauskite, kad redaguotumėte" data-id="'.$row['parent_kid_id'].'" data-type="cell3">'.filterText($row['cell3']).'</td>';
					$column[3] += (float)$row['cell3'];
				}
			}
			foreach($soc_rem as $key => &$val)
				$val = $nuolaidos_dyd[$key];
			//unset($soc_rem);
			//if(isset($_GET['full']) && DB_PREFIX == 'sauZiog_')
			//	echo '<td>'.$disease.'</td>';
			foreach($calcDaysSum as $ID => $sum)
				echo '<td>'.$sum.'</td>';
			if(isset($_GET['full']) || (isset($_GET['summary']) && DB_PREFIX == 'tauKodel_') || (isset($_GET['foodDetails']) && DB_PREFIX == 'molSau_'))
				echo '<td>'.implode(', ', $soc_rem).($eat_print ? '. '.count($total_eat).': '.implode(', ', $total_eat) : '').'</td>';
			
			if(isset($_GET['food'])) {
				echo '<td>'.$eatedPus.'</td>';
				echo '<td>'.$eatedPiet.'</td>';
				echo '<td>'.$eatedPavak.'</td>';
				echo '<td>'.$eatedVakar.'</td>';
				echo '<td>'.array_sum($eated).'</td>';
				echo '<td>'.$allergicCnt.'</td>';
			}
			if($sum_a_day) {
				echo '<td>'.price($day_food+$ugdym).'</td>';
			}
			if($currently_locked)
				echo '<td>'.filterText($row['pastabos']).'</td>';
			else
				echo '<td class="click-to-edit editable" title="Paspauskite, kad redaguotumėte" data-id="'.$row['parent_kid_id'].'" data-type="pastabos">'.filterText($row['pastabos']).'</td>';
			//on hover show content jquery
			/*echo "<td>";
			/*
			$result2 = mysqli_query($db_link, "SELECT DISTINCT aprasymas FROM ".DB_documents." d, ".DB_justified_d." p 
				WHERE
				p.veikimo_d_id IN (SELECT ID FROM ".DB_working_d." WHERE metai=".$year." AND menuo =".$month.")
				AND d.ID=p.dok_id AND d.vaiko_id=".(int)$row['vaiko_id']."
				") or logdie('Neteisinga užklausa: '.mysqli_error($db_link));
			while ($row2 = mysqli_fetch_assoc($result2))
				echo $row2['aprasymas'].' ';
				/
				echo implode(', ', $aprasymas);
			echo "</td>";*/
		echo "</tr>";
	}
	if(isset($_GET['full'])) {
		//TR--------
		echo '<tr><td colspan="3" style="text-align:right; font-weight: bold">Lankė ('.array_sum($lanke_dienu).'):</td>';
		$sum = 0;
		foreach($lanke_dienu as $diena) {
			echo "<td>".$diena."</td>";
			$sum += $diena;
		}//Iš viso'.$sum.'
		
		if(KaunoRBaibokyne) {
			echo '<td colspan="10"></td></tr>';
		} else {
			echo '<td style="font-weight: bold; text-align: right;" colspan="2">Iš viso:</td>';
			echo '<td>'.$sum_lankytos.'</td>';
			echo '<td>'.$sum_apmokamos.'</td>';
			echo '<td>'.$sum_praleista_isviso.'</td>';
			echo '<td>'.$sum_praleista_pateisinta.'</td>';
			//if(DB_PREFIX == 'sauZiog_')
			//	echo '<td>'.$sum_disease.'</td>';
			foreach($calcDaysSumTotal as $ID => $sum)
				echo '<td>'.$sum.'</td>';
			foreach($additional_columns_without_calc as $id => $val)
				if($val > 0)
					echo '<td>'.($val == 2 ? $column[$id] : '').'</td>';
			if($showPayment || PanevezioVaivoryksteLD) {
				if(!PanevezioVaivoryksteLD)
				echo '<td class="money'.(true || KESV ? ' no-print' : '').'">'.price($sum_mokejimui, true).'</td>';
				//if(KESV)
					echo '<td class="money">'.price($sum_mokejimui_saved, true).'</td>';
			}
			echo '<td colspan="'.($sum_a_day ? 3 : 2).'"></td>';
			echo '</tr>';
		}
		
		//TR--------
		echo '<tr><td colspan="3" style="text-align:right; font-weight: bold">Neatvyko ('.array_sum($neatvyko_dienu).'):</td>';
		$sum = 0;
		foreach($neatvyko_dienu as $diena) {
			echo "<td>".$diena."</td>";
			$sum += $diena;
		}//Iš viso'.$sum.'
		//if(DB_PREFIX == 'sauZiog_')
		//	++$additional_columns;
		$additional_columns += count($calcDaysSumDefault);
		if($sum_a_day)
			++$additional_columns;
		if($showPayment || PanevezioVaivoryksteLD) {
			if(KaunoRBaibokyne) {
				$fill = '<td colspan="'.(11+$additional_columns).'"></td></tr>';
			} elseif(PanevezioVaivoryksteLD) {
				$fill = '<td colspan="'.(9+$additional_columns).'"></td></tr>';
			} else {
				$fill = '<td colspan="'.(10+$additional_columns).'"></td></tr>';
			}
		} else {
			$fill = '<td colspan="'.(8+$additional_columns).'"></td></tr>';
		}
		echo $fill;
		//Dar gali būti eilutė „neturėjo atvykti“ kitaip vadinant „neregistruota darželyje“.
		
		
		
		if(KretingosPasakaLD || DB_PREFIX == 'kretBaubl_' || DB_PREFIX == 'pnvLin_' || DB_PREFIX == 'kedPas_' || DB_PREFIX == 'kretJokub_') {
			//TR--------
			echo '<tr><td colspan="'.(3+$additional_columns).'" style="text-align:right; font-weight: bold">Pusryčių ('.array_sum($pusryciu).'):</td>';
			$sum = 0;
			foreach($pusryciu as $diena) {
				echo "<td>".$diena."</td>";
				$sum += $diena;
			}//Iš viso'.$sum.'
			echo $fill;
			//TR--------
			echo '<tr><td colspan="'.(3+$additional_columns).'" style="text-align:right; font-weight: bold">Pietų ('.array_sum($pietu).'):</td>';
			$sum = 0;
			foreach($pietu as $diena) {
				echo "<td>".$diena."</td>";
				$sum += $diena;
			}//Iš viso'.$sum.'
			echo $fill;
			//TR--------
			echo '<tr><td colspan="'.(3+$additional_columns).'" style="text-align:right; font-weight: bold">Pavakarių ('.array_sum($pavakariu).'):</td>';
			$sum = 0;
			foreach($pavakariu as $diena) {
				echo "<td>".$diena."</td>";
				$sum += $diena;
			}//Iš viso'.$sum.'
			echo $fill;
			//TR--------
			echo '<tr><td colspan="'.(3+$additional_columns).'" style="text-align:right; font-weight: bold">Vakarienių ('.array_sum($vakarieniu).'):</td>';
			$sum = 0;
			foreach($vakarieniu as $diena) {
				echo "<td>".$diena."</td>";
				$sum += $diena;
			}//Iš viso'.$sum.'
			echo $fill;	
		}
	}
	?></table>
	[p] - nelankyta pateisinta diena; [n] - nelankyta nepateisinta diena; [ ] - lankyta diena; [-] - vaiko oficialiai darželyje nėra; <?php
	foreach($marks as $mark)
		echo '['.$mark['abbr'].'] - '.$mark['title'].'; ';
		
	
	if(DruskininkuBitute || KaunoZvangutis || DB_PREFIX == 'kaiZas_' || DB_PREFIX == 'kretPasak_' || DB_PREFIX == 'kretAzuol_' || DB_PREFIX == 'pnvPus_' || DB_PREFIX == 'knsrAzuol_' || DB_PREFIX == 'pnvLin_') {
		?>
		<div style="text-align: right;">Auklėtoja ________________________</div>
		<?php
	}
	if(DB_PREFIX == 'kedPur_') {
		?>
		<div style="float: left;">Tabelį sudarė auklėtoja: ___________________<br><br>
Maisto organizavimo specialistė: ___________________</div>
<div style="float: right;">Tabelį suderino: ___________________<br><br>
Tabelį patvirtino: ___________________</div>
		<?php
	}
	if(DB_PREFIX == 'pnvPus_') {
		?>
		Tikrinta:<br>
		___________________<br>
		(parašas)<br>
		___________________<br>
		(parašas)
		<?php
	}
	
	//if(isset($_GET['full']) && KretingosPasakaLD) {
	//	echo '<input type="submit" class="submit" value="Filtruoti"></form>';
	//}
	if (isset($_GET['download'])) {
		download_word('Tabelis '.str_replace([',,', '"'], ['„', '“'], $grupes_pav)." ".year($year)."-".men($month));
	} else {
		echo '<a href="?'.http_build_query(array_merge($_GET, ['download' => ''])).'" class="no-print">Atsisiųsti Word</a>';
	}
	echo ' <a href="?'.http_build_query(array_merge($_GET, ['Excel' => ''])).'" class="no-print">Atsisiųsti Excel</a>';
	
	if(isset($_GET['Excel'])) {
		$html = ob_get_contents();
		ob_clean();
		//echo getcwd();
		include './libs/self/compiler.php';
		excel($html, 'Tabelis.xlsx');
	}
	
	if(isset($_GET['excel'])) {
		//require_once('./libs/vendor/autoload.php');
		$html = ob_get_contents();
		ob_clean();
		/*$htmlPhpExcel = new \Ticketpark\HtmlPhpExcel\HtmlPhpExcel($html);//."</body></html>"
		$htmlPhpExcel->utf8DecodeValues();//$htmlPhpExcel->utf8EncodeValues();
		// Create and output the excel file to the browser
		//echo "\xEF\xBB\xBF";
		$htmlPhpExcel->process()->output('excel.xlsx');*/
		
		
		require_once ('./libs/vendor/phpoffice/phpexcel/Classes/PHPExcel.php');
		//file_put_contents('tmp.html','<table border="1"><tr><td>123</td></tr></table>');

		$tmpfname = tempnam("/tmp", "FOO");

		$handle = fopen($tmpfname, "w");
		fwrite($handle, $html);
		fclose($handle);

		// do here something



		$objReader = new PHPExcel_Reader_HTML;
		$objReader->setInputEncoding('UTF-8');
		$objPHPExcel = $objReader->load($tmpfname);//'tmp.html');
		unlink($tmpfname);

		//$filename = 'excel.xlsx';
		header("Content-Type: application/vnd.ms-excel");
		header('Content-Disposition: attachment; filename="sample.xls"');
		echo "
			<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\">
			<html>
				<head><meta http-equiv=\"Content-type\" content=\"text/html;charset=utf-8\" /></head>
				<body>
		";



		$filename = 'excel.html';
		//header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="'.$filename.'"');
			header('Cache-Control: max-age=1');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		//$objWriter->save('php://output');//"myExcelFile.xlsx");
		echo $html.'</body></html>';
		
		//echo $html;
		exit();
	}
	?>
	<script type="text/javascript" src="/workers/tabeliai.js?1"></script>
	<?php
}
?>
</div>
<div id='blackScreen'></div>
<div id='nZ'></div>
