<?php if(!defined('DARBUOT') && !DARBUOT) exit(); /*TODO: tik dietistei */ ?>
<h1>Lankomumo žymėjimo užrakinimas (negalima keisti)</h1>
<div id="content">
<?php
if(isset($_POST['save'])) {
	$result = db_query("SELECT * FROM `1attendance_mark_lock` WHERE `kindergarten_id`=".DB_ID." AND `date`='".db_fix($_POST['date'])."'");
	if(!mysqli_num_rows($result)) {
		db_query("INSERT INTO `1attendance_mark_lock` SET
			`kindergarten_id`=".DB_ID.",
			`date`='".db_fix($_POST['date'])."',	
		    `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
		msgBox('OK', 'Informacija išsaugota sėkmingai.');
	} else {
		msgBox('ERROR', 'Jau užrakinta. Užrakinti užrakintą nėra prasmės.');
	}
}
if(isset($_GET['delete'])) {
	db_query("DELETE FROM `1attendance_mark_lock` WHERE `kindergarten_id`=".DB_ID." AND `date`='".db_fix($_GET['delete'])."'");// `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'
	msgBox('OK', 'Informacija ištrinta sėkmingai.'.mysqli_affected_rows($db_link));
}
?>

<fieldset id="attendance-day-form">
	<legend>Užrakinti naują dieną</legend>
	<form method="post">
		<p>Šiandien - <?=date('Y-m-d')?> (<?=date('N').' '.mb_lcfirst($dienos[date('N')])?>)</p>
		<p><span class="abbr" title="Datos įprastas formatas <?=date('Y-m-d')?>"><!-- Data: --></span> <input class="datepicker abbr" type="text" name="date" value="<?=(/*isset($_GET['edit']) ? $data :*/ date('Y-m-d'))?>" data-date="<?=date('Y-m-d')?>" id="suggested-date" title="Žalia pariebinta data - pasiūlyta šiandiena. Kita nurodoma spustelėjus pele."> <input style="margin:0" type="submit" name="save" value="Užrakinti" class="submit"></p>
	</form>
</fieldset>
	
<?php
$result = db_query("SELECT YEAR(`date`) AS `metai`, MONTH(`date`) AS `menuo` FROM `1attendance_mark_lock` WHERE `kindergarten_id`=".DB_ID." GROUP BY YEAR(`date`) DESC, MONTH(`date`) DESC");
if(mysqli_num_rows($result)) {
	?>
	<h2>Užrakintos dienos</h2>
	<form method="get">
		<div class="sel" style="float: left; margin-right: 5px; line-height: 33px;">
		<select name="date">
			<?php
			if(isset($_POST['date'])) {
				list($year, $month) = explode('-', $_POST['date']);//, $day
				$year = (int) $year;
				$month = (int) $month;
			}
			if(isset($_GET['date'])) {
				list($year, $month) = explode('-', $_GET['date']);//, $day
				$year = (int) $year;
				$month = (int) $month;
			}
			while ($row = mysqli_fetch_assoc($result))
				echo "<option value=\"".$row['metai']."-".$row['menuo']."\"".((isset($_GET['date']) || isset($_POST['date'])) && $row['metai'] == $year && $row['menuo'] == $month ? ' selected="selected" class="selected">'.$selectedMark : '>').$row['metai']."-".men($row['menuo'])."</option>";
			?>
		</select></div>
		<input type="submit" value="Filtruoti" class="filter">
	</form>
	<?php


	if(isset($_GET['date'])) {
		list($year, $month) = explode('-', $_GET['date']);
		$result = db_query("SELECT * FROM `1attendance_mark_lock` WHERE `kindergarten_id`=".DB_ID." AND YEAR(`date`)=".(int)$year." AND MONTH(`date`)=".(int)$month." GROUP BY `date` ORDER BY `date` DESC");
	} else
		$result = db_query("SELECT * FROM `1attendance_mark_lock` WHERE `kindergarten_id`=".DB_ID." GROUP BY `date` ORDER BY `date` DESC LIMIT 0,30");
	if(mysqli_num_rows($result) > 0) {
		if(!isset($_GET['date']))
			echo '<em>Sąraše dabar rodoma 30 naujausių dienų.</em>';
		?>
		<table border="1">
		<tr>
			<th>Data</th>
			<th>Savaitės diena</th>
			<th>Veiksmai</th>
		</tr>
		<?php
		while ($row = mysqli_fetch_assoc($result)) {
			$week_day = date('N', strtotime($row['date']));
			echo "\t<tr>
				<td>".$row['date']."</td>
				<td>".$week_day.' '.mb_lcfirst($dienos[$week_day])."</td>
				<td><a href=\"?delete=".$row['date']."\" onclick=\"return confirm('Ar tikrai norite atrakinti?')\">Atrakinti</a></td>
			</tr>";
		}
		?>
		</table>
	<?php
	}
}
?>
</div>
