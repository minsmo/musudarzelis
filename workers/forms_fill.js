$(function() {
	var answer_id = $('#answer_id').val();
	if(answer_id) {
		function answer_editing() {
			$.get('/workers/forms_fill_edit.php?answer_id='+answer_id);
		}
		answer_editing();
		setInterval(answer_editing, 10000);//5000 - 20 concurrent people - 0.05 s.
		window.onbeforeunload = function() {
			$.ajax('/workers/forms_fill_edit.php?remove&answer_id='+answer_id, {async : false});
		};
	}
	//START DRY: from forms.js
	$('.addRow').on('click', function(event) {
		event.preventDefault();
		var sizeTh = $(this).prev().find('th').length;
		var el = $(this).prev().find('th').first();
		var nextTr = el.data('name').substr(0,-2)+$(this).prev().find('tr').length-1;
		//console.log(sizeTh);
		var tr = $('<tr></tr>');
		for(var i = 0; i < sizeTh; ++i) {
			var type = el.data('type');
			var disabled = '';
			if(el.prop('disabled'))
				disabled = ' disabled="disabled"';//Check: does it work?
			if(type == 0)
				tr.append('<td><input name="'+el.data('name')+'" id="'+nextTr+'" type="number" min="1"'+disabled+'></td>');
			else if(type == 1)
				tr.append('<td><textarea name="'+el.data('name')+'" id="'+nextTr+'"'+disabled+'></textarea></td>');
			else
				tr.append('<td><a href="#" class="remRow abbr" style="color: red" title="Trinti šią eilutę" onclick="return confirm(\'Ar tikrai norite ištrinti eilutę?\')">X</a></td>');
			el = el.next();
		}
		$(this).prev().append(tr);
	});
	$(document).on('click', '.remRow', function (event) {
		event.preventDefault();//return false;
		$(this).parents('tr').remove();
	});
	//END DRY: from forms.js
});
