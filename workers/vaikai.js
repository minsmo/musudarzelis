'use strict';
function validateSearch(thisForm) {
	var error="";
	var dataFilter = /^[A-Ž]+$/i;
	var gimFilter = /^[0-9\-]{2,10}$/;
	var addressFilter = /^[A-Ž0-9\-\., ]+$/i;
	var telFilter = /^[0-9]{2,11}$/;
	//var emailFilter   = /^[-A-Ž0-9~!!$%^&*_=+}{\'?@\.]+$/i;
	//var emailFilter = /^[A-Z0-9\-\.]{1,20}@[.]*$/i;
	//var emailFilter = /^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]\{2,9\})$/;
	var emailFilter = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i; //http://fightingforalostcause.net/misc/2006/compare-email-regex.php


	/*if (thisForm.vardas.value != '' && !dataFilter.test(thisForm.vardas.value)) {//Vardas
		thisForm.vardas.style.background = 'Yellow';
		error += "Vardas privalo susidaryti tik iš raidžių.\n";
	}
	if (thisForm.pavarde.value != '' && !dataFilter.test(thisForm.pavarde.value)) {//Pavardė
		thisForm.pavarde.style.background = 'Yellow';
		error += "Pavardė privalo susidaryti tik iš raidžių.\n";
	}
	if (thisForm.mamos_vardas.value !=  '' && !dataFilter.test(thisForm.mamos_vardas.value)) {//Mamos vardas
		thisForm.mamos_vardas.style.background = 'Yellow';
		error += "Vardas privalo susidaryti tik iš raidžių.\n";
	}
	if (thisForm.mamos_pavarde.value !=  '' && !dataFilter.test(thisForm.mamos_pavarde.value)) {//Mamos pavardė
		thisForm.mamos_pavarde.style.background = 'Yellow';
		error += "Mamos vardas privalo susidaryti tik iš raidžių.\n";
	}
	if (thisForm.tevo_vardas.value !=  '' && !dataFilter.test(thisForm.tevo_vardas.value)) {//Tėvo vardas'
		thisForm.tevo_vardas.style.background = 'Yellow';
		error += "Tėvo vardas privalo susidaryti tik iš raidžių.\n";
	}
	if (thisForm.tevo_pavarde.value !=  '' && !dataFilter.test(thisForm.tevo_pavarde.value)) {//Tėvo pavardė
		thisForm.tevo_pavarde.style.background = 'Yellow';
		error += "Tėvo pavardė privalo susidaryti tik iš raidžių.\n";
	}*/
	if (thisForm.gim_data.value !=  '' && !gimFilter.test(thisForm.gim_data.value)) {//9999-12-31
		thisForm.gim_data.style.background = 'Yellow';
		error += "Blogas datos formatas: jis turi būti „****-**-**“ (metai-mėnuo-diena).\n";
	}
	if (thisForm.tevu_adresas.value !=  '' && !addressFilter.test(thisForm.tevu_adresas.value)) {//Tėvų adresas
		thisForm.tevu_adresas.style.background = 'Yellow';
		error += "Adrese gali būti tik raidės, skaičiai, taškas, kablelis, trumpas brūkšnelis ir tarpas.\n";
	}
	thisForm.mam_tel.value = thisForm.mam_tel.value.replace(' ', '');
	if (thisForm.mam_tel.value !=  '' && !telFilter.test(thisForm.mam_tel.value)) {//Mamos tel.
		thisForm.mam_tel.style.background = 'Yellow';
		error += "Mamos telefonas gali būti sudarytas tik iš skaičių. Galite ieškoti iki 9 skaitmenų.\n";
	}
	thisForm.tecio_tel.value = thisForm.tecio_tel.value.replace(' ', '');
	if (thisForm.tecio_tel.value !=  '' && !telFilter.test(thisForm.tecio_tel.value)) {//Tėvo tel.
		thisForm.tecio_tel.style.background = 'Yellow';
		error += "Tėčio telefonas gali būti sudarytas tik iš skaičių. Galite ieškoti iki 9 skaitmenų.\n";
	}
	thisForm.glob_tel.value = thisForm.glob_tel.value.replace(' ', '');
	if (thisForm.glob_tel.value !=  '' && !telFilter.test(thisForm.glob_tel.value)) {//Globėjų tel.
		thisForm.glob_tel.style.background = 'Yellow';
		error += "Globėjų telefonas gali būti sudarytas tik iš skaičių. Galite ieškoti iki 9 skaitmenų.\n";
	}
	thisForm.namu_tel.value = thisForm.namu_tel.value.replace(' ', '');
	if (thisForm.namu_tel.value !=  '' && !telFilter.test(thisForm.namu_tel.value)) {//Namų tel.
		thisForm.namu_tel.style.background = 'Yellow';
		error += "Namų telefonas gali būti sudarytas tik iš skaičių. Galite ieškoti iki 9 skaitmenų.\n";
	}
	/*
	if (thisForm.pastas.value !=  'El. paštas' && !emailFilter.test(thisForm.pastas.value)) {
		thisForm.pastas.style.background = 'Yellow';
		error += "El. pašto adrese įvesti neleistini simboliai.\n";
	}*/
	if (error != "") {
		alert("Blogai užpildyta:\n" + error);
		return false;
	}

	return true;
}

function validateInput(thisForm) {
	$(thisForm).find('input').each(function() {
		$(this).val($.trim($(this).val()));
	});
	var error="";
	var dataFilter = /^.{3,60}$/i;//[A-Ž]
	var gimFilter = /^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$/;
	var addressFilter = /^[A-Ž0-9\-\., ]{7,100}$/i;
	var telFilter = /^[0-9]{3,18}$/;//Allow short and long numbers
	//var emailFilter   = /^[-A-Ž0-9~!!$%^&*_=+}{\'?@\.]+$/i;
	var emailFilter = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i; //http://fightingforalostcause.net/misc/2006/compare-email-regex.php
	
	if(thisForm.vardas.value.length === 0) {
		thisForm.vardas.style.background = 'Yellow';
		error += "Privalo būti vaiko vardas užpildytas.\n";
	}
	if(thisForm.pavarde.value.length === 0) {
		thisForm.pavarde.style.background = 'Yellow';
		error += "Privalo būti vaiko pavardė užpildyta.\n";
	}
	

	/*if (thisForm.vardas.value != '' && !dataFilter.test(thisForm.vardas.value)) {
		thisForm.vardas.style.background = 'Yellow';
		error += "Vardas privalo susidaryti tik iš raidžių ir turi būti mažiausiai 3 ir daugiausiai 60 simbolių.\n";
	}

	if (thisForm.pavarde.value != '' && !dataFilter.test(thisForm.pavarde.value)) {
		thisForm.pavarde.style.background = 'Yellow';
		error += "Pavardė privalo susidaryti tik iš raidžių ir turi būti mažiausiai 3 ir daugiausiai 60 simbolių.\n";
	}
	if (thisForm.mamos_vardas.value !=  '' && !dataFilter.test(thisForm.mamos_vardas.value)) {
		thisForm.mamos_vardas.style.background = 'Yellow';
		error += "Vardas privalo susidaryti tik iš raidžių ir turi būti mažiausiai 3 ir daugiausiai 60 simbolių.\n";
	}
	if (thisForm.mamos_pavarde.value !=  '' && !dataFilter.test(thisForm.mamos_pavarde.value)) {
		thisForm.mamos_pavarde.style.background = 'Yellow';
		error += "Mamos vardas privalo susidaryti tik iš raidžių ir turi būti mažiausiai 3 ir daugiausiai 60 simbolių.\n";
	}
	if (thisForm.tevo_vardas.value !=  '' && !dataFilter.test(thisForm.tevo_vardas.value)) {
		thisForm.tevo_vardas.style.background = 'Yellow';
		error += "Tėvo vardas privalo susidaryti tik iš raidžių ir turi būti mažiausiai 3 ir daugiausiai 60 simbolių.\n";
	}
	if (thisForm.tevo_pavarde.value !=  '' && !dataFilter.test(thisForm.tevo_pavarde.value)) {
		thisForm.tevo_pavarde.style.background = 'Yellow';
		error += "Tėvo pavardė privalo susidaryti tik iš raidžių ir turi būti mažiausiai 3 ir daugiausiai 60 simbolių.\n";
	}*/
	if (thisForm.gim_data.value !=  '' && !gimFilter.test(thisForm.gim_data.value)) {
		thisForm.gim_data.style.background = 'Yellow';
		error += "Blogas datos formatas: jis turi būti „****-**-**“ (metai-mėnuo-diena).\n";
	}
	/*if (thisForm.tevu_adresas.value !=  '' && !addressFilter.test(thisForm.tevu_adresas.value)) {
		thisForm.tevu_adresas.style.background = 'Yellow';
		error += "Adrese gali būti tik raidės, skaičiai, taškas, kablelis, trumpas brūkšnelis ir tarpas bei turi būti sudarytas iš mažiausiai 7 ir daugiausiai 100 simbolių.\n";
	}*/
	thisForm.mam_tel.value = thisForm.mam_tel.value.replace(' ', '');
	if (thisForm.mam_tel.value !=  '370' && thisForm.mam_tel.value !=  '' && !telFilter.test(thisForm.mam_tel.value)) {
		thisForm.mam_tel.style.background = 'Yellow';
		error += "Mamos telefonas gali būti sudarytas tik iš skaičių ir jis turi būti sudarytas iš mažiauiai 7 ir daugiausiai 11 skaitmenų.\n";
	}
	thisForm.tecio_tel.value = thisForm.tecio_tel.value.replace(' ', '');
	if (thisForm.tecio_tel.value !=  '370' && thisForm.tecio_tel.value !=  '' && !telFilter.test(thisForm.tecio_tel.value)) {
		thisForm.tecio_tel.style.background = 'Yellow';
		error += "Tėčio telefonas gali būti sudarytas tik iš skaičių ir jis turi būti sudarytas iš mažiauiai 7 ir daugiausiai 11 skaitmenų.\n";
	}
	thisForm.glob_tel.value = thisForm.glob_tel.value.replace(' ', '');
	if (thisForm.glob_tel.value !=  '370' && thisForm.glob_tel.value !=  '' && !telFilter.test(thisForm.glob_tel.value)) {
		thisForm.glob_tel.style.background = 'Yellow';
		error += "Globėjų telefonas gali būti sudarytas tik iš skaičių ir jis turi būti sudarytas iš mažiauiai 7 ir daugiausiai 11 skaitmenų.\n";
	}
	thisForm.namu_tel.value = thisForm.namu_tel.value.replace(' ', '');
	if (thisForm.namu_tel.value !=  '370' && thisForm.namu_tel.value !=  '' && !telFilter.test(thisForm.namu_tel.value)) {
		thisForm.namu_tel.style.background = 'Yellow';
		error += "Namų telefonas gali būti sudarytas tik iš skaičių ir jis turi būti sudarytas iš mažiauiai 7 ir daugiausiai 11 skaitmenų.\n";
	}
	/*if (thisForm.pastas.value !=  '' && !emailFilter.test(thisForm.pastas.value)) {
		thisForm.pastas.style.background = 'Yellow';
		error += "Įvestasis el. pašto adresas yra neteisingas.\n";
	}
	if (thisForm.slapyvardis.value !=  '' && !dataFilter.test(thisForm.slapyvardis.value)) {
		thisForm.slapyvardis.style.background = 'Yellow';
		error += "Slapyvardis turi būti sudarytas tik iš raidžių.\n";
	}*/

	if (error != "") {
		alert("Netinkamai užpildyta, prašome pasitikrinti duomenis:\n" + error);
		return false;
	}

	return true;
}

$( "#search_btn" ).click(function() {
	$( "#search_form" ).toggle();//'slow'
});
$( "#targetcol_btn" ).click(function() {
	$( "#targetcol" ).toggle();//'slow'
});

function archived(archived) {
	archived ? $('#when-archived-not-needed').hide() : $('#when-archived-not-needed').show();
}

$(function() {
	$('#vaikai-tbl').columnManager({listTargetID:'targetcol', onClass: 'simpleon', offClass: 'simpleoff', saveState: true});
	
	$('#new-version').click(function() {
		if(!$('#valid-from-new').val())
		{
			alert("Neįvedėte datos „nuo“");
			return false;
		}
		return true;
	});
	
	archived($('#archive-input').prop('checked'));
	$('#archive-input').click(function() {
		archived($('#archive-input').prop('checked'));
	});
	
	//http://upshots.org/javascript/jquery-change-tag-name
	$.fn.changeTag = function(tag){
		// create the new, empty shim
		var replacement = $('<' + tag + '>');
		// empty container to hold attributes
		var attributes = {};
		// copy all the attributes to the shell
		$.each(this.get(0).attributes, function(index, attribute) {
		   attributes[attribute.name] = attribute.value;
		}); //http://stackoverflow.com/questions/6753362/jquery-how-to-copy-all-the-attributes-of-one-element-and-apply-them-to-another
		// assign attributes to replacement
		replacement.attr(attributes);
		// copy the data
		replacement.data(this.data());
		// get all the kids, with data and events
		var contents = this.children().clone(true);
		// inseminate
		replacement.append(contents);
		// swap it out
		this.replaceWith(replacement);
	}
	//Fx bug http://stackoverflow.com/questions/7336586/printing-fieldsets-in-firefox
	$(window).bind('beforeprint', function(){
		$('fieldset').each(
			function(item) {
				$(this).addClass('fieldset').changeTag('div');//replaceWith($('<div class="fieldset">' + this.innerHTML + '</div>'));
			}
		)
	});
	$(window).bind('afterprint', function(){
		$('.fieldset').each(
			function(item) {
				//$(this).replaceWith($('<fieldset>' + this.innerHTML + '</fieldset>'));
				$(this).removeClass('fieldset').changeTag('fieldset');
			}
		)
	});
	
	$('#form-new-version').submit(function () {
		if ($('#valid-form-'+$('#valid-from-new').val()).length) {
		    alert('Duomenys galiojantys nuo '+$('#valid-from-new').val()+' jau yra. Jei jie neteisingi tuomet juos atsidarę keiskite ir po to išsaugokite.');
		    return false;
		}
	});
	
	$('#kid-form').submit(function () {
		if ($('#archive-input').is(':checked') && $('#versions tr').length == 2) {
		    alert('Jei norite išregistruoti vaiką, tada sudarykite naują dokumentą, spustelint ant mygtuko „Reikia pakeitimo, galiosiančio nuo naujos datos“, įvedant datą (nuo kurios vaiko oficialiai nėra darželyje), patvirtinant šią datą, pažymint varnelę ties „Vaikas išregistruotas (neberodomas)“, papildomai galima įrašyti išregistravimo priežastį į laukelį pavadinimu „Duomenų įvedimo priežastis“ ir galiausiai spustelint mygtuką „Išsaugoti“');
		    //Jei norite išregistruoti vaiką, tada sudarykite naują dokumentą tokia seka: 1. spustelint ant mygtuko „Reikia pakeitimo, galiosiančio nuo naujos datos“, įvedant datą (nuo kurios vaiko oficialiai nėra darželyje), patvirtinant šią datą, 2. pažymint varnelę ties „Vaikas išregistruotas (neberodomas)“, 3. spustelint mygtuką „Išsaugoti“
		    //Jei norite išregistruoti vaiką, tada sudarykite naują dokumentą, spustelint ant mygtuko „Reikia pakeitimo, galiosiančio nuo naujos datos“, įvedant datą (nuo kurios vaiko oficialiai nėra darželyje), patvirtinant šią datą, pažymint varnelę ties „Vaikas išregistruotas (neberodomas)“, papildomai galima įrašyti išregistravimo priežastį į laukelį pavadinimu „Duomenų įvedimo priežastis“ ir galiausiai spustelint mygtuką „Išsaugoti“
		    return false;
		}
	});
	//setTimeout(function(){
	var versions_show_on_top = $.remember({name: 'versions-show-on-top'});
	//console.log(versions_show_on_top);
	if(versions_show_on_top === null || versions_show_on_top === 'true')
		versions_show_on_top = true;
	else
		versions_show_on_top = false;
	//console.log(versions_show_on_top);
	$('#versions-on-top-switch').prop('checked', versions_show_on_top);//== 1*
	$('#versions-on-top-switch').click(function() {
		versions_show_on_top = this.checked;//? 1 : 0
		$.remember({name: 'versions-show-on-top', value: versions_show_on_top});
		//console.log(versions_show_on_top);
		//console.log('ėėėė', $.remember({name: 'versions-show-on-top'}));
	});
	
	//Can be optimized by js cache variables
	$(window).scroll(function() {
		if(versions_show_on_top) {//== 1
			//console.log("More");
			if ($(window).scrollTop() > 200) {
				// > 100px from top - show div
				//console.log("More");
				$('#versions').addClass('versions-on-top');
				$('#kid-form').css('margin-top', $('#versions').outerHeight()-30+'px');
			} else {
				$('#versions').removeClass('versions-on-top');
				$('#kid-form').css('margin-top', 0);
				 //console.log("Less");
				// <= 100px from top - hide div
			}
		} else {
			$('#versions').removeClass('versions-on-top');
		}
	});
	
	function getParameterByName(name) {
		name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		    results = regex.exec(location.search);
		return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
	var scrollToY = parseInt(getParameterByName('scrollTo'));
	//console.log(scrollToY);
	//console.log(typeof scrollToY);
	if(scrollToY) {
		window.scrollTo( 0, scrollToY );
		$(document).scrollTop(scrollToY);
	}
	$('a[data-scrollTo]').on('click', function(e){
		//console.log(this.getAttribute('href'));
		//console.log(location.href);
		//console.log(location.href.split('?')[0]+'?'+this.getAttribute('href'));
		//console.log($(this).attr("href"));
		//e.preventDefault();
		//var page = $(this).data('scrollTo');
		//location.href += ( location.search.length ? '&' : '?' ) + 'scrollTo=' + $(window).scrollTop();//$(document).scrollTop()
		location.href = addParameter(this.getAttribute('href'), 'scrollTo', $(document).scrollTop(), false);
		return false;
		//middle-click works on Fx, but not on Chrome, but right click -> on new tab works
	});
	
	//http://stackoverflow.com/questions/6953944/how-to-add-parameters-to-a-url-that-already-contains-other-parameters-and-maybe
	function addParameter(url, parameterName, parameterValue, atStart/*Add param before others*/){
		var replaceDuplicates = true;
		if(url.indexOf('#') > 0){
		    var cl = url.indexOf('#');
			var urlhash = url.substring(url.indexOf('#'),url.length);
		} else {
		    var urlhash = '';
		    var cl = url.length;
		}
		var sourceUrl = url.substring(0,cl);

		var urlParts = sourceUrl.split("?");
		var newQueryString = "";

		if (urlParts.length > 1)
		{
		    var parameters = urlParts[1].split("&");
		    for (var i=0; (i < parameters.length); i++)
		    {
		        var parameterParts = parameters[i].split("=");
		        if (!(replaceDuplicates && parameterParts[0] == parameterName))
		        {
		            if (newQueryString == "")
		                newQueryString = "?";
		            else
		                newQueryString += "&";
		            newQueryString += parameterParts[0] + "=" + (parameterParts[1]?parameterParts[1]:'');
		        }
		    }
		}
		if (newQueryString == "")
		    newQueryString = "?";

		if(atStart){
		    newQueryString = '?'+ parameterName + "=" + parameterValue + (newQueryString.length>1?'&'+newQueryString.substring(1):'');
		} else {
		    if (newQueryString !== "" && newQueryString != '?')
		        newQueryString += "&";
		    newQueryString += parameterName + "=" + (parameterValue?parameterValue:'');
		}
		return urlParts[0] + newQueryString + urlhash;
	};
	//http://stackoverflow.com/questions/5997450/append-to-url-and-refresh-page
	//http://stackoverflow.com/questions/21970975/append-string-to-url-using-jquery-or-javascript
	//scroolTo the same position onclick
	//http://mrcoles.com/scroll-sneak/
	//http://en.wikipedia.org/wiki/HTTP_cookie#Alternatives_to_cookies
	//http://stackoverflow.com/questions/486896/adding-a-parameter-to-the-url-with-javascript
	//http://stackoverflow.com/questions/13063838/add-change-parameter-of-url-and-redirect-to-the-new-url
	//add parameter to url javascript

	//offtopic http://www.secalert.net/
	//}, 3000);
});
