$(function() {
	$('.addScnt').on('click', function(event) {
		event.preventDefault();
		var id = $(this).data("id");
		var scntDiv = $(this).next('.p_scents');
		//
		//$('<p><label>'+$("#kidId option:selected").text()+' <input type="text" class="individual_plan" name="individuali_veikla['+id+']"></label> <a href="#" class="remScnt" id="individual'+id+'">Trinti</a></p>').appendTo(scntDiv);// data-id="'+id+'"
		$('<p><select name="tipas['+id+'][]"><option class="green" value="0" selected="selected">Išmoko</option><option class="yellow" value="1">Įpusėjo</option><option class="red" value="2">Neišmoko</option></select><input type="text" name="pavadinimas['+id+'][]"> <a href="#" class="remScnt">Trinti</a></p>').appendTo(scntDiv);
	});

	$(document).on('click', '.remScnt', function (event) {
		event.preventDefault();
		$(this).parents('p').remove();
	});
	
	
	function eachCheckbox() {
		var attended = 0, not_attended = 0;
		$("input[type='checkbox']").each(function() {
			//var thisCheck = $(this);
			if ( this.checked ) {//thischeck.is(':checked')
				$(this).parent().removeClass("absent");
				$(this).parent().prev().removeClass("absent");
				$(this).parent().addClass("present");
				$(this).parent().prev().addClass("present");
				++attended;
			} else {
				$(this).parent().addClass("absent");
				$(this).parent().prev().addClass("absent");
				$(this).parent().removeClass("present");
				$(this).parent().prev().removeClass("present");
				++not_attended;
			}
			$('#counter').html('Atėjo <strong>'+attended+'</strong>; Neatėjo <strong>'+not_attended+'</strong>; Iš viso <strong>'+(attended+not_attended)+'</strong>.'+(not_attended == 0 ? ' ☺' : ''));
		});
	}
	eachCheckbox();
	$( ".attendance-checkboxes" ).click(function() {
		setTimeout(eachCheckbox, 1);
	});
	$(":checkbox").click(function() {
		setTimeout(eachCheckbox, 1);
	});
	
	$("#suggested-date").change(suggestedDate).map(suggestedDate);
	function suggestedDate() {
		if($(this).val() == $(this).data('date'))
			$(this).addClass('suggested');
		else
			$(this).removeClass('suggested');
	}
	
	
	/*$(':radio').mousedown(function(e){
	  var $self = $(this);
	  if( $self.is(':checked') ){
		var uncheck = function(){
		  setTimeout(function(){$self.removeAttr('checked');},0);
		};
		var unbind = function(){
		  $self.unbind('mouseup',up);
		};
		var up = function(){
		  uncheck();
		  unbind();
		};
		$self.bind('mouseup',up);
		$self.one('mouseout', unbind);
	  }
	});*/
	$("input:radio").on("click",function (e) {
		var inp=$(this); //cache the selector
		if (inp.is(".theone")) { //see if it has the selected class
		    inp.prop("checked",false).removeClass("theone");
		    return;
		}
		$("input:radio[name='"+inp.prop("name")+"'].theone").removeClass("theone");
		inp.addClass("theone");
	});
});
function validateDate(thisForm) {
    var error="";
    var dataFilter = /^\d{4}-\d{1,2}-\d{1,2}$/;
    //if (!dataFilter.test(thisForm.data.value)) {
    //    thisForm.data.style.background = 'Yellow';
    if (!dataFilter.test(thisForm.zymeti.value)) {
        thisForm.zymeti.style.background = 'Yellow';
        error += "Blogas datos formatas: jis turi būti „****-**-**“.\n";
    }

    if (error != "") {
        alert("Blogai užpildyta:\n" + error);
        return false;
    }

    return true;
}
