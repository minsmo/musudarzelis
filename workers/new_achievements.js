"use strict";
$(function() {
	$('.progress-selection').click(function() {
		//console.log($(this).text());
		$(this).parent().children().removeClass('selected-level');
		$(this).addClass('selected-level');
		$(this).parent().find('input').val($(this).text());
	});
	$.ui.dialog.prototype._focusTabbable = $.noop;
	/*var dia = $( ".dialog" ).dialog({
		autoOpen: false,
		//resizable: false,
		modal: false,
		closeText: "Uždaryti",
		width: '95%',
		buttons: {
			//text: "Uždaryti"
			'Uždaryti': function() {
				$(this).dialog( "close" );
			}
		},
		
	});*/
	var last = '';
	var isHidden = true;
	$('#dialog-control #close').click(function() {
		$('#'+last).hide();
		$('#dialog-control').hide();
		isHidden = true;
	});
	$('#dialog-control #toggle').click(function() {
		if(isHidden)  {
			$('#'+last).show();
			$('#dialog-control #toggle').html('_(sumažinti)');
		} else {
			$('#'+last).hide();
			$('#dialog-control #toggle').html('□(išdidinti)');
		}
		isHidden = !isHidden;
	});
	$( ".dialog-opener" ).click(function() {
		//$('#'+$(this).data('id')).dialog( "open" ).dialogExtend({
       //  "minimizable" : true,
   //   });
		last = $(this).data('id');
		isHidden = false;
		$('#dialog-control #dialog-title').html($('#'+$(this).data('id')).data('title'));
		$('#dialog-control #toggle').html('_(sumažinti)');
		$('#'+$(this).data('id')).show();
		$('#dialog-control').show();
		//$('.ui-dialog-titlebar-close').focus();
	});
	//$('#group-main-form').submit(function() {
		//alert('aaa');
		//return confirm("aaa?");
	//});
	$('#showForGroup').click(to_default);
	$('#showKids').click(to_default);
	function to_default() {
		$(this).next().show();
		$(this).parent().next().find('[name=date]').prop('disabled', true).removeAttr('required');
		$(this).parent().next('.date-dialog').hide();
		$(this).parent('form').prop('action', '');
	}
	
	$('#newForGroup').click(expand);
	$('#newForKid').click(expand);
	function expand(e) {//[name=newForGroup]
		e.preventDefault();
		$(this).hide();
		$(this).parent().next().show().find('[name=date]').prop('required',true).prop('disabled', false).focus();
		$(this).parents('form').prop('action',"#new-report-form");
		/*$("#date-dialog").dialog({
			closeText: "Uždaryti",
			width: '400',
		});*/
		//	$('[name=newForGroup]').submit();
	}
	$('#view-group').click(function(e) {
		if($(".primary-data-group option:selected").text() == $(".secondary-data-group option:selected").text())
			alert('Pastebėjimas. Pasirinkote lyginti tos pačios datos vertinimus.');
	});
});
