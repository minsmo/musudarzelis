<?php if(!defined('ADMIN') || !ADMIN) exit(); ?>
<h1>Vaikų grupės</h1>
<div id="content">
<?php
if(isset($_POST['save']) && isset($_POST['pavadinimas'])) {
	db_query("INSERT INTO `".DB_groups."` SET `pavadinimas`='".db_fix($_POST['pavadinimas'])."', `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
	msgBox('OK', "Grupės pavadinimas sėkmingai išsaugotas!");
}
if(isset($_POST['update']) && isset($_POST['pavadinimas'])) {
	db_query("UPDATE `".DB_groups."` SET `pavadinimas`='".db_fix($_POST['pavadinimas'])."',
		`updated`=CURRENT_TIMESTAMP, `updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."',
		`updatedCounter`=`updatedCounter`+1
		WHERE `ID`=".(int)$_POST['update']);
	msgBox('OK', "Grupės pavadinimas sėkmingai pakeistas!");
}

if(isset($_GET['delete'])) {
	db_query("UPDATE `".DB_groups."` SET `isHidden`=1 WHERE `ID`=".(int)$_GET['delete']);
	//db_query("DELETE FROM `".DB_groups."` WHERE `ID`=".(int)$_GET['delete']) && !mysqli_query($db_link, "DELETE FROM `".DB_employees_groups."` WHERE `grup_id`=".(int)$_GET['delete']);
	msgBox('OK', "Grupė paslėpta!");//ištrinta
}
if(isset($_POST['move_kids']) && !empty($_POST['valid_from']) && !empty($_POST['kid_id']) && !empty($_POST['group_id'])) {
	$result = db_query("SELECT cr.* FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<='".CURRENT_DATE."' GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
		WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 AND cr.`grupes_id`=".(int)$_POST['move_kids']."
		ORDER BY ".orderName('cr'/*, CURRENT_DATE*/));
	$sql_insert = '';
	$names = '';
	while($row = mysqli_fetch_assoc($result))
		if(in_array($row['parent_kid_id'], $_POST['kid_id'])) {
			unset($row['ID'], $row['created'], $row['updated'], $row['updatedByUserId'], $row['updatedByEmployeeId'], $row['updatedCounter']);
			$row['createdByUserId'] = USER_ID;
			$row['createdByEmployeeId'] = DARB_ID;
			$row['valid_from'] = $_POST['valid_from'];
			$row['version_comment'] = $_POST['version_comment'];
			$row['grupes_id'] = $_POST['group_id'];
			if(!empty($_POST['age_group']))
				$row['arDarzelyje'] = $_POST['age_group'];
			$sql_insert .= "(";
			foreach($row as $val)
				$sql_insert .= "'".db_fix($val)."',";
			$sql_insert = substr($sql_insert, 0, -1).'),';
			if(empty($names))
				foreach($row as $name => $val)
					$names .= "`$name`,";
		}
	//db_query("INSERT INTO `".DB_children."` SET `pavadinimas`='".db_fix($_POST['pavadinimas'])."', `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
	db_query("INSERT INTO `".DB_children."` (".substr($names, 0, -1).") VALUES ".substr($sql_insert, 0, -1));
	msgBox('OK', "Vaikai perkelti iš grupės „".getAllGroups()[(int)$_POST['move_kids']]."“ į „".getAllGroups()[(int)$_POST['group_id']]."“");
}//Gali būti dar ir išregistravimas ir grupės tipo keitimas.

if(isset($_POST['unregister_kids_from_group']) && !empty($_POST['valid_from']) && !empty($_POST['kid_id'])) {
	$result = db_query("SELECT cr.* FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<='".CURRENT_DATE."' GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
		WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 AND cr.`grupes_id`=".(int)$_POST['unregister_kids_from_group']."
		ORDER BY ".orderName('cr'/*, CURRENT_DATE*/));
	$sql_insert = '';
	$names = '';
	while($row = mysqli_fetch_assoc($result))
		if(in_array($row['parent_kid_id'], $_POST['kid_id'])) {
			unset($row['ID'], $row['created'], $row['updated'], $row['updatedByUserId'], $row['updatedByEmployeeId'], $row['updatedCounter']);
			$row['createdByUserId'] = USER_ID;
			$row['createdByEmployeeId'] = DARB_ID;
			$row['valid_from'] = $_POST['valid_from'];
			$row['version_comment'] = $_POST['version_comment'];
			$row['archyvas'] = 1;
			$sql_insert .= "(";
			foreach($row as $val)
				$sql_insert .= "'".db_fix($val)."',";
			$sql_insert = substr($sql_insert, 0, -1).'),';
			if(empty($names))
				foreach($row as $name => $val)
					$names .= "`$name`,";
		}
	//db_query("INSERT INTO `".DB_children."` SET `pavadinimas`='".db_fix($_POST['pavadinimas'])."', `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
	db_query("INSERT INTO `".DB_children."` (".substr($names, 0, -1).") VALUES ".substr($sql_insert, 0, -1));
	msgBox('OK', "Vaikai išregistruoti iš grupės „".getAllGroups()[(int)$_POST['unregister_kids_from_group']]."“");
}//Gali būti dar ir išregistravimas ir grupės tipo keitimas.

//archyvas



if(isset($_POST['assign_save'])) {//isset($_POST['priskyrimas'])
	foreach($_POST['priskyrimas'] as &$val)
		$val = (int)$val;
	unset($val);// break/destroy the reference with the last element
	//http://php.net/manual/en/control-structures.foreach.php
	
	if(empty($_POST['priskyrimas'])) {
		db_query("DELETE FROM `".DB_employees_groups."` WHERE `grup_id`=".(int)$_POST['assign_save']);
	} else {
		db_query("DELETE FROM `".DB_employees_groups."` WHERE `grup_id`=".(int)$_POST['assign_save']." AND `darb_id` NOT IN (".implode(',', $_POST['priskyrimas']).")");
		foreach($_POST['priskyrimas'] as $val) {
			$result = db_query("SELECT * FROM `".DB_employees_groups."` WHERE `grup_id`='".(int)$_POST['assign_save']."' AND `darb_id`=".(int)$val);
			if(mysqli_num_rows($result) >= 1) {
				db_query("UPDATE `".DB_employees_groups."` SET `updated`=CURRENT_TIMESTAMP, `updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."',
			`updatedCounter`=`updatedCounter`+1 WHERE `grup_id`=".(int)$_POST['assign_save']." AND `darb_id`=".(int)$val);
			} else {
				db_query("INSERT INTO `".DB_employees_groups."` SET `grup_id`=".(int)$_POST['assign_save'].", `darb_id`=".(int)$val.", `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
			}
		}
	}
	msgBox('OK', "Grupei priskirtų darbuotojų informacija išsaugota!");
}


function URL($new_parameters = [], $unset_parameters = []) {
	global $_GET;
	$__GET = $_GET;//Keep what is saved in URL
	unset($__GET['q'], $__GET['delete'], $__GET['new']);//1. Clean garbage
	$__GET = array_merge($__GET, $new_parameters);//2. Add new
	foreach($unset_parameters as $val)//3. Remove unused
		unset($__GET[$val]);
	return '?'.http_build_query($__GET);
}
?>

<div class="no-print"><a href="<?=URL(['view' => ''])?>" class="fast-action<?=(isset($_GET['view']) ? ' a-opened' : '')?>">Grupių lentelė (glausta)</a> | <a href="<?=URL([], ['view'])?>" class="fast-action<?=(!isset($_GET['view']) ? ' a-opened' : '')?>">Grupių lentelė (išplėsta)</a><?php if(isset($_GET['edit'])) { ?> | <a href="?new" class="no-print fast-action fast-action-add">Nauja grupė</a><?php } ?>
<?=(isset($_GET['archive']) && $_GET['archive'] == 1 ? ' <a href="'.URL(['archive' => 0]).'" class="no-print fast-action fast-action-archive a-opened">✖ Uždaryti archyvą</a>' : ' <a href="'.URL(['archive' => 1]).'" class="no-print fast-action fast-action-archive">Archyvas</a>')?>
<?=ui_print()?></div>


<?php
if(isset($_GET['edit'])) {
	$result = db_query("SELECT * FROM `".DB_groups."` WHERE `ID`=".(int)$_GET['edit']);
	$grupes = mysqli_fetch_assoc($result);
}
?>
<fieldset id="newGroup-form" style="margin-top: 0px;">
<form action="<?=URL()?>" method="post" class="not-saved-reminder">
	<p <?=(isset($_GET['edit']) ? ' class="opened-row" style="padding: 7px;"' : '')?>><label><?=(isset($_GET['edit']) ? "Grupės pavadinimo keitimas" : "Naujas grupės pavadinimas")?><span class="required">*</span>: <input<?=(isset($_GET['edit']) ? ' autofocus' : '')?> required="required" type="text" name="pavadinimas" value="<?=(isset($_GET['edit']) ? filterText($grupes['pavadinimas']) : '')?>"></label>
	<input type="hidden" <?=(!isset($_GET['edit']) ? 'name="save"' : 'name="update" value="'.(int)$grupes['ID'].'"')?>>
	<input type="submit" value="Išsaugoti" class="submit" style="margin: 0;"></p>
</form>
</fieldset>


<table>
<tr>
	<th>Nr.</th>
	<th>Grupės pavadinimas <span title="Rikiuota natūralia tvarka">↓</span></th>
	<th>Grupės darbuotojai (prileidimui)</th><!--	<th class="no-print">Grupės darbuotojų priskyrimas</th> -->
	<th>Kita informacija</th>
</tr>
<?php
$result = db_query("SELECT cr.`grupes_id`, cr.`arDarzelyje`, COUNT(cr.`arDarzelyje`) cnt FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<='".CURRENT_DATE."' GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from` WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 GROUP BY cr.`grupes_id`, cr.`arDarzelyje`");
//SELECT `grupes_id`, `arDarzelyje`, COUNT(`arDarzelyje`) AS cnt FROM `".DB_children."` WHERE `archyvas`=0 AND `isDeleted`=0 GROUP BY `grupes_id`, `arDarzelyje`
$group_type = array();
while ($row = mysqli_fetch_assoc($result))
	$group_type[$row['grupes_id']][] = $vaik_darzelio_tipas_kas[$row['arDarzelyje']];

$result = db_query("SELECT cr.`grupes_id`, COUNT(*) cnt FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<='".CURRENT_DATE."' GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from` WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 GROUP BY cr.`grupes_id`");
//SELECT grupes_id, COUNT(*) AS cnt FROM `".DB_children."` WHERE `archyvas`=0 AND `isDeleted`=0 GROUP BY `grupes_id`
$kids_in_group = array();
while ($row = mysqli_fetch_assoc($result))
	$kids_in_group[$row['grupes_id']] = $row['cnt'];
	
$result = db_query("SELECT cr.`grupes_id`, COUNT(*) cnt FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<='".CURRENT_DATE."' GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from` WHERE cr.`isDeleted`=0 AND cr.`archyvas`<>0 GROUP BY `grupes_id`");
//SELECT grupes_id, COUNT(*) AS cnt FROM `".DB_children."` WHERE `archyvas`<>0 AND `isDeleted`=0 GROUP BY `grupes_id`
$archived_kids_in_group = array();
while ($row = mysqli_fetch_assoc($result))
	$archived_kids_in_group[$row['grupes_id']] = $row['cnt'];
	
$result = db_query("
SELECT cr.`grupes_id`, MIN(TIMESTAMPDIFF(YEAR, cr.`gim_data`, '".CURRENT_DATE."')) AS `minAge`, MAX(TIMESTAMPDIFF(YEAR, cr.`gim_data`, '".CURRENT_DATE."')) AS `maxAge`, AVG(TIMESTAMPDIFF(YEAR, cr.`gim_data`, '".CURRENT_DATE."')) AS `avgAge`
FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<='".CURRENT_DATE."' GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from` WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 GROUP BY `grupes_id`
-- SELECT `grupes_id`, MIN(TIMESTAMPDIFF(YEAR, `gim_data`, '".CURRENT_DATE."')) AS `minAge`, MAX(TIMESTAMPDIFF(YEAR, `gim_data`, '".CURRENT_DATE."')) AS `maxAge`, AVG(TIMESTAMPDIFF(YEAR, `gim_data`, '".CURRENT_DATE."')) AS `avgAge` 
-- FROM `".DB_children."` WHERE `archyvas`=0 AND `isDeleted`=0 GROUP BY `grupes_id`");
//SELECT cr.*, TIMESTAMPDIFF(YEAR, cr.`gim_data`, '".CURRENT_DATE."') AS age FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<='".CURRENT_DATE."' GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from` WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 ORDER BY cr.`vardas` ASC, cr.`pavarde` ASC
$kids_age = array();
while ($row = mysqli_fetch_assoc($result))
	$kids_age[$row['grupes_id']] = '<span class="abbr" title="Amžius skaičiuojamas pagal įvestas gimimo datas (jei įvestos gimimo datos)">Amžiai</span>: mažiausias '.$row['minAge'].', vidurkis '.round($row['avgAge'], 2).', didžiausias '.$row['maxAge'];

$result = db_query("SELECT * FROM `".DB_groups."` WHERE `isHidden`=".(isset($_GET['archive']) ? (int)$_GET['archive'] : 0 )." ORDER BY `pavadinimas`");
if(mysqli_num_rows($result) > 0) {
	$depend_on_groups = array();
	foreach($login_depends_on_assigned_groups as $id => $dumb) {
		$depend_on_groups[] = mb_lcfirst($person_type[$id]);
	}
	
	$i = 0;
	$rows = [];
	//$sort_by_field = [];
	while ($row = mysqli_fetch_assoc($result)) {
		$rows[filterText($row['pavadinimas']).$row['ID']] = $row;
		//$sort_by_field[] = filterText($row['pavadinimas']);
	}
	//array_multisort($sort_by_field, SORT_ASC, SORT_NATURAL | SORT_FLAG_CASE, $rows);
	ksort($rows, SORT_NATURAL | SORT_FLAG_CASE);//SORT_LOCALE_STRING
	foreach($rows as $row) {
		if(isset($_GET['priskirti_grupei']) && $_GET['priskirti_grupei'] == $row['ID'])
			$group_name = filterText($row['pavadinimas']);
		echo "		<tr".(isset($_GET['priskirti_grupei']) && $_GET['priskirti_grupei'] == $row['ID'] || isset($_GET['edit']) && $_GET['edit'] == $row['ID'] ? ' class="opened-row"' : '').">
		<td>".++$i."</td>
		<td>".filterText($row['pavadinimas']).(!isset($_GET['view']) ?
		"&nbsp;
			<div class=\"no-print\" style=\"float: right;\"><a href=\"".URL(['edit' => $row['ID']])."\">Pavadinimo keitimas</a>
			<a href=\"".URL(['delete' => $row['ID']])."\" onclick=\"return confirm('Ar tikrai norite slėpti grupę?')\">Slėpti</a></div><br><div class=\"no-print abbr\" title=\"Grupė sukurta ".$row['created']."\" style=\"float: right;\">".substr($row['created'], 0, 10).'</div>' : '')."
		</td>
		<td>";//ištrinti Trinti
		$result_d = mysqli_query($db_link, "SELECT `".DB_employees."`.* 
		FROM `".DB_employees_groups."` JOIN `".DB_employees."`  ON `".DB_employees_groups."`.`darb_id`=`".DB_employees."`.`ID` AND `".DB_employees."`.`isDeleted`=0
		WHERE `grup_id`=".(int)$row['ID'].' ORDER BY `vardas`, `pavarde`') or logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
		if(mysqli_num_rows($result_d) > 0) {
			$tmp = array();
			while ($d = mysqli_fetch_assoc($result_d))
				$tmp[] = filterText($d['vardas']).' '.filterText($d['pavarde']);
			echo implode(",<br>", $tmp);
		} else {
			echo '<div style="max-width: 400px;">Dėmesio. Darbuotojai turintys leidimų grupę: <strong>'._join('</strong>, <strong>', $depend_on_groups, 'ir').'</strong> galės dirbti<!-- prisijungti ir prisijungusios dirbti --> su šia vaikų grupe tik juos priskyrus grupėje. Tai padaryti galite spustelėjus ant nuorodos, esančios dešiniau, pavadinimu „Darbuotojų priskyrimas“.</div>';
		}
		if(!isset($_GET['view']))
			echo "<div class=\"no-print\" style=\"float: right;\"><a href=\"".URL(['priskirti_grupei' => $row['ID']])."#assignEmployees\">Darbuotojų priskyrimas</a></div>";	
		echo "</td>
		<td>".(!isset($_GET['view']) ?
			"<a href=\"/vaikai?ieskoti&amp;grupes_id=".$row['ID']."\" class=\"no-print\">Vaikų peržiūra</a><br><!-- Peržiūrėti grupės vaikus -->
			<a href=\"".URL(['move_kids' => $row['ID']])."#moveKids\" class=\"no-print abbr\" title=\"tarp grupių\">Vaikų perkėlimas</a><br>
			<a href=\"".URL(['unregister_kids_from_group' => $row['ID']])."#unregister\" class=\"no-print\">Vaikų išregistravimas</a><br>" : '')."
			".(isset($group_type[$row['ID']]) ? "<span class=\"abbr\" title=\"Prie vaikų nurodyta „Grupės rūšis“ meniu punkte „Vaikai“\">Grupės rūšis</span>: ".implode(", ", $group_type[$row['ID']]) : "")."<br>
			".(isset($kids_in_group[$row['ID']]) ? "Vaikų: ".$kids_in_group[$row['ID']] : '').(isset($archived_kids_in_group[$row['ID']]) ? ", dar archyvuotų vaikų: ".$archived_kids_in_group[$row['ID']] : '')."<br>
			".(isset($kids_age[$row['ID']]) ? $kids_age[$row['ID']] : '' )."
			</td>
	</tr>";	
	}
}
?>
</table>

<?php
if(isset($_GET['priskirti_grupei']) && isset($group_name)) {
?>
<fieldset style="margin-top: 40px;" id="assignEmployees">
<legend>Darbuotojų prileidimo grupei „<?=$group_name?>“ redagavimas</legend>
<form action="<?=URL()?>" method="post">
	<input type="hidden" name="assign_save" value="<?=(int)$_GET['priskirti_grupei']?>">
	<!-- <a href="#" class="addEmployee">Pridėti darbuotoją</a>
	<div id="employees"> -->
	<?php
	$darbuotojai = [];
	$res = getValidEmployeesOrdered_db_query();
	while ($row2 = mysqli_fetch_assoc($res))
		$darbuotojai[$row2['ID']] = filterText($row2['vardas']." ".$row2['pavarde']);
	
	?>
	<div>
		<div class="sel" style="margin-right: 5px;"><select id="personId">
		<option value="" disabled selected hidden>Pasirinkite darbuotoją priskyrimui</option>
		<?php
		foreach($darbuotojai as $key => $value)
			echo "<option value=\"$key\">$value</option>";
		?>
		</select></div> <!-- <button id="addPerson">Priskirti darbuotoją</button> -->
	</div>
	<h3>Prileidžiamų darbuotojų sąrašas:</h3>
	<div id="persons" style="clear: left">
	<?php
	$result = db_query("SELECT * FROM `".DB_employees_groups."` WHERE `grup_id`=".(int)$_GET['priskirti_grupei']);
	//$i = 1;
	if(mysqli_num_rows($result)) {
		while ($row = mysqli_fetch_assoc($result)) {
			if(isset($darbuotojai[$row['darb_id']]))
				echo '<p><label>'.$darbuotojai[$row['darb_id']].' <input type="hidden" name="priskyrimas[]" value="'.$row['darb_id'].'"></label> <a href="#" class="remPerson" id="person'.$row['darb_id'].'">- Ištrinti susiejimą</a></p>';
			else
				echo '<p><input type="hidden" name="priskyrimas[]" value="'.$row['darb_id'].'"></p>';
			/*?><p style="clear: left;">
			<div class="sel" style="float: left; margin-right: 5px;"><select name="priskyrimas[]">
			<?php
			foreach($darbuotojai as $key => $value)
				echo "<option value=\"$key\"".($key == $row['darb_id'] ? ' selected="true"' : '').">$value</option>";
			?>
			</select></div> <a href="#assignEmployees" class="remEmployee">Ištrinti susiejimą</a>
			</p><?php
			$i++;*/	
		}
	}
	/*while($i++ <= 4) {
		?><p style="clear: left;">
			<div class="sel" style="float: left; margin-right: 5px;"><select name="priskyrimas[]"><?php
			foreach($darbuotojai as $key => $value)
				echo "<option value=\"$key\">$value</option>";
			?></select></div> <a href="#assignEmployees" class="remEmployee" style="display: none;">Ištrinti susiejimą</a>
		</p><?php
	}*/
	?>
	</div>
	<!-- </div> -->
	<p style="clear: left;"><input type="submit" value="Išsaugoti" class="submit"></p>
	<script src="/workers/grupes.js?1"></script>
</form>
	<!-- P.S. Norint ištrinti darbuotojo susiejimą su grupe reikėtų vietoje to darbuotojo pasirinkti pirmąjį užrašą „Nepasirinktas darbuotojas“ ir išsaugoti. -->
</fieldset>
<?php
}



//TODO: merge code of move_kids and unregister_kids_from_group
if(isset($_GET['move_kids'])) {
	?>
	<fieldset id="moveKids">
	<legend>Vaikų perkėlimas iš grupės „<?=getAllGroups()[(int)$_GET['move_kids']]?>“</legend>
	<script type="text/javascript" src="/libs/jquery.checkboxes.min.js"></script>
		<script type="text/javascript">
		$(function($) {
			$('#moveKids').checkboxes('range', true);
		});
		</script>
	<form action="<?=URL()?>" method="post" class="not-saved-reminder">
		<div><label>Iš grupės „<?=getAllGroups()[(int)$_GET['move_kids']]?>“ perkelti į <div class="sel"><select name="group_id"><?php
			foreach(getAllGroups() as $ID => $title)
				//if($ID != $_GET['move_kids'])
					echo "<option value=\"$ID\">$title</option>";
			?></select></div></label></div>
		<div><label>Vaiko amžiaus grupę pakeisti į <div class="sel"><select name="age_group"><option value="">Nekeisti</option>
		<?php
			foreach($kid_age_group_kieno as $ID => $title)
				//if($ID != $_GET['move_kids'])
					echo "<option value=\"$ID\">$title</option>";
			?></select></div></label></div>
		<p><label>Nuo kada vaikai naujoje grupėje<?php // Iki kada vaikui paskutinį kartą žymėti lankomumą (Kol kas dar: Nuo kada vaikui jau nebežymėti lankomumo) ?><span class="required">*</span> <input class="datepicker" type="text" name="valid_from" required="required"></label></p>
		<p><label>Duomenų įvedimo priežastis</span><!-- galiojimo pradžios pastaba Šito laikotarpio pastaba --> <input maxlength="255" type="text" id="version-comment" name="version_comment"></label></p>
		<div>Vaikai perkėlimui:<?php //Pasirinkti vaikus perkėlimui ?><br>
		<?php
		$result = db_query("SELECT cr.* FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<='".CURRENT_DATE."' GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
			WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 AND cr.`grupes_id`=".(int)$_GET['move_kids']."
			ORDER BY ".orderName('cr'/*, CURRENT_DATE*/));
		while($row = mysqli_fetch_assoc($result))
			echo '<label><input type="checkbox" name="kid_id[]" value="'.$row['parent_kid_id'].'"> '.getName($row['vardas'], $row['pavarde']).' <em>'.$vaik_darzelio_tipas_kas[$row['arDarzelyje']].'<em></label><br>';
		?>
		</div>
		<p><input type="hidden" name="move_kids" value="<?=(int)$_GET['move_kids']?>"><input type="submit" value="Perkelti" class="submit"></p>
	</form>
	</fieldset>
	<?php
}

if(isset($_GET['unregister_kids_from_group'])) {
	?>
	<fieldset id="unregister">
	<legend>Išregistravimas iš darželio vaikų esančių grupėje „<?=getAllGroups()[(int)$_GET['unregister_kids_from_group']]?>“</legend>
	<script type="text/javascript" src="/libs/jquery.checkboxes.min.js"></script>
		<script type="text/javascript">
		$(function($) {
			$('#unregister').checkboxes('range', true);
		});
		</script>
	<form action="<?=URL()?>" method="post" class="not-saved-reminder">
		<p><label>Nuo kada vaiko oficialiai nėra šioje įstaigoje <?php // Iki kada vaikui paskutinį kartą žymėti lankomumą (Kol kas dar: Nuo kada vaikui jau nebežymėti lankomumo) ?><span class="required">*</span> <input class="datepicker" type="text" name="valid_from" required="required"></label></p>
		<p><label>Duomenų įvedimo (išregistravimo) priežastis</span><!-- galiojimo pradžios pastaba Šito laikotarpio pastaba --> <input maxlength="255" type="text" id="version-comment" name="version_comment"></label></p>
		<div>Vaikai perkėlimui:<?php //Pasirinkti vaikus perkėlimui ?><br>
		<?php
		$result = db_query("SELECT cr.* FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<='".CURRENT_DATE."' GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
			WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 AND cr.`grupes_id`=".(int)$_GET['unregister_kids_from_group']."
			ORDER BY ".orderName('cr'/*, CURRENT_DATE*/));
		while($row = mysqli_fetch_assoc($result))
			echo '<label><input type="checkbox" name="kid_id[]" value="'.$row['parent_kid_id'].'"> '.getName($row['vardas'], $row['pavarde']).' <em>'.$vaik_darzelio_tipas_kas[$row['arDarzelyje']].'<em></label><br>';
		?>
		</div>
		<p><input type="hidden" name="unregister_kids_from_group" value="<?=(int)$_GET['unregister_kids_from_group']?>"><input type="submit" value="Išregistruoti" class="submit"></p>
	</form>
	</fieldset>
	<?php
}



?>
</div>
