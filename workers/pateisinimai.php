<?php if(!defined('DARBUOT') || ADMIN) exit();
?>
<h1>Lankymo pateisinimų žymėjimas</h1>
<div id="content">
<?php
/*
if(isset($_POST['issaugoti'])) {
	mysqli_query($db_link, "DELETE FROM ".DB_justified_d." WHERE dok_id=".(int)$_POST['dok_id']) or logdie(mysqli_error($db_link));
	if(count($_POST['veikimo_d_id']) > 0)
		foreach($_POST['veikimo_d_id'] as $id=>$val)
			mysqli_query($db_link, "INSERT INTO ".DB_justified_d." SET dok_id=".(int)$_POST['dok_id'].", veikimo_d_id=".(int)$id.", `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
	msgBox('OK', "Informacija išsaugota!");
}
?>
<form action="/pateisinimai" method="post">
	Raštelis:
	<select name="dok_id">
	<?php
	if(ADMIN)
		$result = mysqli_query($db_link, "SELECT *, ".DB_documents.".ID AS dID 
			FROM ".DB_documents." 
			JOIN ".DB_children." ON ".DB_children.".ID=".DB_documents.".vaiko_id 
			WHERE ".DB_documents.".tipas=".(int)SERGA." OR ".DB_documents.".tipas=3 OR ".DB_documents.".tipas=5") or logdie(mysqli_error($db_link));
	else
		$result = mysqli_query($db_link, "SELECT *, ".DB_documents.".ID AS dID
			FROM ".DB_documents." 
			JOIN ".DB_children." ON ".DB_children.".ID=".DB_documents.".vaiko_id 
			WHERE (".DB_documents.".tipas=".(int)SERGA." OR ".DB_documents.".tipas=3 OR ".DB_documents.".tipas=5) AND ".DB_children.".grupes_id=".(int)GROUP_ID) or logdie(mysqli_error($db_link));

	while($row = mysqli_fetch_assoc($result)) {
		echo "<option value=\"".$row['dID']."\"".(isset($_POST['dok_id']) && $row['dID']===$_POST['dok_id'] ? ' selected="selected"' : '' ).">".$row['vardas']." ".$row['pavarde']." ".$row['aprasymas']."</option>";
		if(isset($_POST['dok_id']) && $_POST['dok_id'] == $row['dID'])
			$vaiko_id = $row['vaiko_id'];
	}
	?>
	</select>
	<input type="submit" name="pateisinti" value="Pasirinkti dienų pateisinimui pagal raštelį">
</form>
*/
if(isset($_POST['data']))
	$data = explode('-', $_POST['data']);
		
if(isset($_POST['issaugoti'])) {
	$currently_locked = db_query("SELECT * FROM `1lock_kids_attendance_food` WHERE `kindergarten_id`=".DB_ID." AND `date`='".db_fix(substr($_POST['data'], 0, 7))."-01'  AND `group_id`=".(int)GROUP_ID);
	if(!mysqli_num_rows($currently_locked)) {
		db_query("UPDATE ".DB_attendance." SET justified_d=0
			WHERE ".DB_attendance.".yra=0 AND vaiko_id=".(int)$_POST['vaiko_id']." AND 
				YEAR(`".DB_attendance."`.`data`)=".(int)$data[0]." AND MONTH(`".DB_attendance."`.`data`)=".(int)$data[1]."");
		if(isset($_POST['pateisinimoData'])) {//count($_POST['pateisinimoData']) > 0
			$ids = array();
			foreach($_POST['pateisinimoData'] as $id => $val)
				$ids[] = db_fix($id);//date
			db_query("UPDATE ".DB_attendance." SET justified_d=1, `updated`=CURRENT_TIMESTAMP, `updatedByUserId`=".USER_ID.", `updatedByEmployeeId`=".DARB_ID.", `updatedCounter`=`updatedCounter`+1 WHERE vaiko_id=".(int)$_POST['vaiko_id']." AND `data` IN ('".implode("','", $ids)."')");
		}
		if(isset($_POST['additional_mark'])) {
			db_query("UPDATE ".DB_attendance." SET `additional_mark_id`=0
			WHERE ".DB_attendance.".yra=0 AND vaiko_id=".(int)$_POST['vaiko_id']." AND 
				YEAR(`".DB_attendance."`.`data`)=".(int)$data[0]." AND MONTH(`".DB_attendance."`.`data`)=".(int)$data[1]."");
			
			foreach($_POST['additional_mark'] as $date => $val)
				db_query("UPDATE `".DB_attendance."` SET
					`additional_mark_id`='".(int)$val."',
					`updated`=CURRENT_TIMESTAMP, `updatedByUserId`=".USER_ID.", `updatedByEmployeeId`=".DARB_ID.",
					`updatedCounter`=`updatedCounter`+1
					WHERE `data`='".db_fix($date)."' AND `vaiko_id`=".(int)$_POST['vaiko_id']);
		}
		msgBox('OK', "Informacija išsaugota!");
	} else {
		msgBox('ERROR', "Užrakintas šio mėnesio lankomumas. Kreipkitės į vadovą dėl atrakinimo.");
	}
}
if(isset($_GET['archived']))
	echo '<a href="/pateisinimai">Rodyti tik dabar oficialiai esančius vaikus</a>';
else 
	echo '<a href="/pateisinimai?archived">Rodyti archyvuotus (išregistruotus) vaikus</a>';
if(isset($_GET['all']))
	echo ' | <a href="/pateisinimai">Rodyti tik dabar oficialiai esančius vaikus</a><br /><br />';
else 
	echo ' | <a href="/pateisinimai?all">Rodyti visus vaikus</a><br /><br />';
?>
<form action="/pateisinimai<?php if(isset($_GET['archived'])) echo '?archived'; ?><?php if(isset($_GET['all'])) echo '?all'; ?>" method="post">
	<!-- Laikotarpis: -->
	<div class="sel" style="float: left; margin-right: 5px;"><select name="data">
	<?php
	//$result = db_query("SELECT `metai`, `menuo` FROM `".DB_working_d."` GROUP BY `metai`, `menuo` ORDER BY `data` DESC");
	$result = db_query("SELECT YEAR(`data`) `metai`, MONTH(`data`) `menuo` FROM `".DB_attendance."` GROUP BY YEAR(`data`), MONTH(`data`) ORDER BY `data` DESC");
	while ($row = mysqli_fetch_assoc($result))
		echo "<option value=\"".$row['metai']."-".$row['menuo']."\"".(isset($_POST['data']) && $row['metai'] == (int)$data[0] && $row['menuo'] == (int)$data[1] ? ' selected="selected" class="selected">'.$selectedMark : '>').$row['metai']."-".men($row['menuo'])."</option>";
	?>
	</select></div>
	<!--Vaikas:-->
	<div class="sel" style="float: left; margin-right: 5px;"><select name="vaiko_id">
	<?php
	if(isset($_GET['archived']))
		$result = db_query($get_archived_kids_sql);
	elseif(isset($_GET['all']))
		$result = db_query($get_all_selected_group_kids_sql);
	else
		$result = db_query($get_kids_sql);
	while($row = mysqli_fetch_assoc($result))
		echo "<option value=\"".$row['parent_kid_id']."\"".(isset($_POST['vaiko_id']) && $_POST['vaiko_id'] == $row['parent_kid_id'] ? ' selected="selected" class="selected">'.$selectedMark : '>').filterText(getName($row['vardas'], $row['pavarde']))."</option>";
	?>
	</select></div>
	<!--
	<div class="sel" style="float: left; margin-right: 5px;"><select name="type">
	<option value="0"<?=(isset($_POST['type']) && $_POST['type'] == 0 ? ' selected="selected" class="selected">'.$selectedMark : '>')?>Nelankytos</option>
	<option value="1"<?=(isset($_POST['type']) && $_POST['type'] == 1 ? ' selected="selected" class="selected">'.$selectedMark : '>')?>Visos</option>
	</select></div> -->
	<input type="submit" name="pateisinti" value="Rodyti nelankytas dienas" class="filter"><!-- Rodyti sąrašą dienų pateisinimui  (nesusiejant su dokumentu) -->
</form>

<?php
if(isset($_POST['pateisinti']) && isset($_POST['vaiko_id']) && isset($_POST['data'])) {
	echo '<p><!-- Raštelio p --><strong>Pateisinta diena žymima varnele.</strong><br>Vaiko '.(isset($_POST['type']) && $_POST['type'] == 0 || !isset($_POST['type']) ? 'nelankytos' : '').' dienos (iš lankomumo žurnalo):</p>';//užregistravimą
	$add = '';
	if(!isset($_POST['type']) || isset($_POST['type']) && $_POST['type'] == 0)
		$add = DB_attendance.".yra=0 AND ";
	$result = db_query("SELECT *	FROM ".DB_attendance."
		WHERE ".$add."vaiko_id=".(int)$_POST['vaiko_id']." AND 
			YEAR(`".DB_attendance."`.`data`)=".(int)$data[0]." AND MONTH(`".DB_attendance."`.`data`)=".(int)$data[1]."
		ORDER BY ".DB_attendance.".`data` DESC");
	if(mysqli_num_rows($result)) {
	?>
	<script type="text/javascript" src="/libs/jquery.checkboxes.min.js"></script>
	<script type="text/javascript">
	$(function() {
		$('#justifies').checkboxes('range', true);
	});
	</script>
	<form action="/pateisinimai<?php if(isset($_GET['archived'])) echo '?archived'; ?>" method="post" id="justifies" class="not-saved-reminder">
		<a href="#justifies" class="justifies-checkboxes" data-toggle="checkboxes" data-action="check">Žymėti </a><a href="#justifies" class="justifies-checkboxes" data-toggle="checkboxes" data-action="check" style="font-weight: bold;">visos dienos pateisintos</a>
		<?php
		$marks = [];
		$r = db_query("SELECT * FROM `".DB_attendance_marks."` WHERE `kindergarten_id`=".DB_ID." AND `isDeleted`=0 ORDER BY `abbr`");
		while($row = mysqli_fetch_assoc($r))
			$marks[] = $row;//abbr, title
		while($row = mysqli_fetch_assoc($result)) {
			echo "<p><label>".$row['data']." <input type=\"checkbox\" name=\"pateisinimoData[".$row['data']."]\" value=\"1\"".($row['justified_d'] == 1 ? 'checked="checked"' : '')."></label>";
			if(!empty($marks)) {
				echo "<td>";
				foreach($marks as $mark) {
					echo ' <label class="additional-mark"><input type="radio" name="additional_mark['.$row['data'].']" value="'.$mark['ID'].'"'.($row['additional_mark_id'] == $mark['ID']  ? ' checked="checked"' : '').'><span class="abbr" title="'.$mark['title'].'">'.$mark['abbr'].'</span></label>';
				}
				echo "</td>";
			}
			echo "</p>";
		}
		?>
		<input type="hidden" name="data" value="<?php echo $_POST['data']; ?>">
		<input type="hidden" name="vaiko_id" value="<?php echo (int)$_POST['vaiko_id']; ?>">
		<input type="submit" name="issaugoti" value="Išsaugoti pateisintas dienas" class="submit"><!-- (Dienų pateisinimus) Išsaugoti pasirinktas pateisinimui dienas -->
	</form>
	<script>
	$(function() {
		$("input:radio").on("click",function (e) {
			var inp=$(this); //cache the selector
			if (inp.is(".theone")) { //see if it has the selected class
				inp.prop("checked",false).removeClass("theone");
				return;
			}
			$("input:radio[name='"+inp.prop("name")+"'].theone").removeClass("theone");
			inp.addClass("theone");
		});
	});
	</script>
	<?php
	} else
		echo "<p> &nbsp; &nbsp; <strong>Nerasta ".(!isset($_POST['type']) || isset($_POST['type']) && $_POST['type'] == 0 ? 'pateisinimui galimų nelankytų' : 'papildomam žymėjimui galimų')." dienų, pažymėtų meniu punkte „Lankomumas“. Galite pasitikrinti tabeliuose arba lankomumo žymėjime.</strong></p>";
}
//justified_d
/*
if(isset($_POST['pateisinti']) && isset($_POST['dok_id']) && isset($vaiko_id)) {
//	echo "Derinimo informacija: Vaiko id: ".$vaiko_id."; dok id: ".(int)$_POST['dok_id'];
	echo '<p>Raštelio pateisinta diena žymima varnele.<br>
		Sąrašas dienų kurias pagal užregistravimą vaikas nebuvo (išskyrus tas kurias pateisino kitas raštelis):</p>';
	$result = mysqli_query($db_link, "SELECT *, ".DB_attendance.".veikimo_d_id, ".DB_justified_d.".ID IS NOT NULL AS priezastis_ivesta
		FROM ".DB_attendance." 
			JOIN ".DB_working_d." ON ".DB_working_d.".ID=".DB_attendance.".veikimo_d_id
			LEFT JOIN ".DB_justified_d." ON ".DB_attendance.".veikimo_d_id=".DB_justified_d.".veikimo_d_id
		WHERE ".DB_attendance.".yra=0 AND vaiko_id=".$vaiko_id." AND ".DB_attendance.".veikimo_d_id NOT IN (SELECT veikimo_d_id FROM ".DB_justified_d." WHERE dok_id<>".(int)$_POST['dok_id'].")
		ORDER BY ".DB_working_d.".`data` DESC") or logdie(mysqli_error($db_link));//Subquery tam, kad nebūtų tų dienų kurias pateisino kiti dokumentai
		// 
		
	if(mysqli_num_rows($result)) {
	?>
	<form action="/pateisinimai" method="post">
		<?php
		while($row = mysqli_fetch_assoc($result)) {
			//echo $row['veikimo_d_id'];
			//echo '.'.$row['priezastis_ivesta'];
			//echo "SELECT * FROM ".DB_justified_d." WHERE veikimo_d_id=".$row['veikimo_d_id']." AND dok_id=".(int)$_POST['dok_id'];
			//$result2 = mysqli_query($db_link, "SELECT * FROM ".DB_justified_d." WHERE veikimo_d_id=".$row['veikimo_d_id']." AND dok_id=".(int)$_POST['dok_id']) or logdie(mysqli_error($db_link));
			//$row2 = mysqli_fetch_assoc($result2);
			//!empty($row2) 
			echo "<p><label>".$row['metai']."-".men($row['menuo'])."-".dien($row['diena'])." <input type=\"checkbox\" name=\"veikimo_d_id[".$row['veikimo_d_id']."]\" value=\"1\"".($row['priezastis_ivesta'] ? 'checked="checked"' : '')."></label></p>";
		}
		?>
		<input type="hidden" name="dok_id" value="<?php echo (int)$_POST['dok_id']; ?>">
		<input type="submit" name="issaugoti" value="Išsaugoti pasirinktas pateisinimui dienas">
	</form>
	<?php
	} else echo "<p>Nėra pateisinimui galimų dienų! Pastaba: arba nieko nepraleidęs vaikas arba kiti rašteliai pateisino.</p>";
}*/
?>
</div>
