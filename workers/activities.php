<?php if(!defined('DARBUOT') && !DARBUOT) exit(); /*TODO: tik auklėtojoms? */ ?>
<h1>Numatomos veiklos (renginiai) <span class="abbr no-print" title="tėvai"><!-- Auklėtojos ir -->(matomumas)</span></h1>
<div id="content">
<?php
if(isset($_POST['save'])) {
	db_query("INSERT INTO `".DB_activities."` SET 
		`kindergarten_id`=".DB_ID.",
		`group_id`='".(int)$_POST['group_id']."', -- GROUP_ID
		`period`='".db_fix($_POST['period'])."',
		`activity`='".db_fix($_POST['activity'])."',
		`where`='".db_fix($_POST['where'])."',
		`other`='".db_fix($_POST['other'])."',
		`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
	msgBox('OK', 'Numatomos veiklos įrašas išsaugotas.');
}
if(isset($_POST['edit'])) {
	db_query("UPDATE `".DB_activities."` SET 
		`period`='".db_fix($_POST['period'])."',
		`activity`='".db_fix($_POST['activity'])."',
		`where`='".db_fix($_POST['where'])."',
		`other`='".db_fix($_POST['other'])."',
		`updated`=CURRENT_TIMESTAMP, `updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."',
		`updatedCounter`=`updatedCounter`+1
		WHERE `kindergarten_id`=".DB_ID." AND `ID`=".(int)$_POST['edit']);
	msgBox('OK', 'Numatomos veiklos įrašas atnaujintas.');
}
if(isset($_GET['delete'])) {
	db_query("DELETE FROM `".DB_activities."` WHERE `kindergarten_id`=".DB_ID." AND `ID`=".(int)$_GET['delete']);
	msgBox('OK', 'Numatomos veiklos įrašas ištrintas.');
}
?>
<a href="?new#menu-form" class="no-print fast-action fast-action-add">Naujas numatomos veiklos įrašas</a> <?=ui_print()?>
<table class="activities">
<tr>
	<th class="date-cell">Data</th>
	<th>Vaikų grupė</th>
	<th>Veikla</th>
	<th>Vieta</th>
	<th>Kita</th>
	<th class="no-print">Veiksmai</th>
</tr>
<?php
$result = db_query("SELECT * FROM `".DB_activities."` WHERE `kindergarten_id`=".DB_ID." ORDER BY `period` DESC");
while($row = mysqli_fetch_assoc($result)) {
	echo "<tr><td>".filterText($row['period'])."</td><td>".getAllGroups($row['group_id'])."</td><td>".nl2br(filterText($row['activity']))."</td><td>".nl2br(filterText($row['where']))."</td><td>".nl2br(filterText($row['other']))."</td><td class=\"no-print\"><a href=\"?edit=".$row['ID']."\">Keisti</a> <a href=\"?delete=".$row['ID']."\" onclick=\"return confirm('Ar tikrai norite ištrinti numatomos veiklos įrašą?')\">Trinti</a></td></tr>";
}
echo '</table>';

if (isset($_GET['edit']) || isset($_GET['new'])) {
	if (isset($_GET['edit'])) {
		$result = db_query("SELECT * FROM `".DB_activities."` WHERE `kindergarten_id`=".DB_ID.(!ADMIN ? " AND (`group_id`=".GROUP_ID." OR `group_id`=0)" : '')." AND `ID`=".(int)$_GET['edit']);
		$activity_plan = mysqli_fetch_assoc($result);
	}
	?>
	<fieldset id="menu-form">
	<legend><?=(isset($_GET['edit']) ? 'Numatomos veiklos įrašo keitimas' : 'Naujas numatomos veiklos įrašas')?></legend>
	<form method="post" class="not-saved-reminder no-print">
		<p><label>Data <span class="notice abbr" title="Datas rašyti tokiu formatu: META-ME-DI (Pvz., 2015-07-13)">(tvarkingumui)</span> <input type="text" name="period" value="<?=(isset($activity_plan) ? filterText($activity_plan['period']) : '')?>" autofocus></label></p>
		<?php
		if(isset($activity_plan)) {
			echo '<p>Vaikų grupė <strong>'.getAllGroups($activity_plan['group_id']).'</strong></p>';
		} else { ?>
		<div><label for="group-id">Grupė<span class="required">*</span></label> <div class="sel"><select id="group-id" name="group_id" required>
			<option value="" selected hidden disabled>Pasirinkite grupę</option>
			<option value="0"<?=(isset($activity_plan) && $ID == 0 ? ' selected="selected"' : '')?>>Visoms grupėms</option>
			<?php
			foreach(getAllowedGroups() as $ID => $title)
				echo '<option value="'.$ID.'"'.(isset($activity_plan) && $ID == $activity_plan['group_id'] ? ' selected="selected"' : '').">".$title."</option>";
			?>
			</select></div></div>
		<?php } ?>
		<p><label>Veikla <textarea name="activity"><?=(isset($activity_plan) ? filterText($activity_plan['activity']) : '')?></textarea></label></p>
		<p><label>Kur? Vieta <textarea name="where"><?=(isset($activity_plan) ? filterText($activity_plan['where']) : '')?></textarea></label></p>
		<p><label>Kita <textarea name="other"><?=(isset($activity_plan) ? filterText($activity_plan['other']) : '')?></textarea></label></p>
		<p><input type="hidden" <?=(!isset($_GET['edit']) ? 'name="save"' : 'name="edit" value="'.(int)$activity_plan['ID'].'"')?>><input type="submit" value="Išsaugoti" class="submit"></p>
	</form>
	</fieldset>
<?php
}
?>
</div>
