<?php if(!ADMIN) exit();
?>
<h1>Mokėjimai</h1>
<div id="content">
<?php
if(isset($_GET['update'])) {
    if(mysqli_query($db_link, "UPDATE ".DB_payments." SET `kindergarten_id`=".DB_ID.", arApmoketa=1 WHERE ID=".(int)$_GET['ID']))
        echo '<div class="green center">Išsaugota</div>';
    else logdie(mysqli_error($db_link));
}
?>
<h2>Neapmokėję:</h2>
<table class="vertical-hover">
<tr>
    <th>Vaikas</th>
    <th>Laikotarpis</th>
    <th>Už darželį</th>
    <th>Ugdymo reikmėms</th>
    <th>Suma</th>
    <th>Veiksmai</th>
</tr>
<?php    
$result = db_query("SELECT *, m.ID FROM ".DB_children." v, ".DB_payments." m WHERE v.ID=m.vaiko_id AND arApmoketa=0 AND m.`kindergarten_id`=".DB_ID);
while ($row = mysqli_fetch_assoc($result)) {
    ?>
    <tr>
        <td><?php echo filterText(getName($row['vardas'], $row['pavarde'])); ?></td>
        <td><?php echo $row['metai'].'-'.men($row['menuo']); ?></td>
        <td class="right"><?php echo (double)$row['uzDarzeli']; ?></td>
        <td class="right"><?php echo (double)$row['ugdymoReikmem']; ?></td>
        <td class="right"><?php echo (double)$row['isViso']; ?></td>
        <td><a href="?mokejimai&amp;update&amp;ID=<?php echo $row['ID']; ?>">Pažymėti, kad apmokėjo</a></td>
    </tr>
    <?php
}
?>
</table>
</div>
