<?php if(!defined('ADMIN') || !ADMIN) exit();
$allowed_options = array(
	'kaina_ugdymo_reikmems_lopselyje_per_d.' => 'float',
	'kaina_ugdymo_reikmems_darzelyje_per_d.' => 'float',
	'darzelio_pavadinimas' => '',
	'banko_kodas' => '',
	'banko_saskaita' => '',
	'banko_pavadinimas' => '',
	'banko_skyrius' => '',
	'vieneriu_pusryciu_kaina_lopselyje' => 'float',
	'vieneriu_pusryciu_kaina_darzelyje' => 'float',
	'vieneriu_pietu_kaina_lopselyje' => 'float',
	'vieneriu_pavakariu_kaina_lopselyje' => 'float',
	'vieneriu_pietu_kaina_darzelyje' => 'float',
	'vieneriu_pavakariu_kaina_darzelyje' => 'float',
	'darzelio_nr' => '',
	'vienerios_vakarienes_kaina_lopselyje' => 'float',
	'vienerios_vakarienes_kaina_darzelyje' => 'float',
	'kaina_ugdymo_reikmems_priesmokykliniame_per_d.' => 'float',
	'vieneriu_pusryciu_kaina_priesmokykliniame' => 'float',
	'vieneriu_pietu_kaina_priesmokykliniame' => 'float',
	'vieneriu_pavakariu_kaina_priesmokykliniame' => 'float',
	'vienerios_vakarienes_kaina_priesmokykliniame' => 'float',
	
	'vaiku_saraso_eiluteje_pirmiau_rodyti_(vardas_ar_pavarde)' => '',
	'vaiku_sarasa_pradeti_rikiuoti_nuo_(vardas_ar_pavarde)' => '',
	
	'papildomos_dienos_kainos,_kuri_ivedama_zymint_lankomuma,_pavadinimas' => '',
	
	'kaina_ugdymo_reikmems_lopselyje_per_men.' => 'float',
	'kaina_ugdymo_reikmems_darzelyje_per_men.' => 'float',
	'kaina_ugdymo_reikmems_priesmokykliniame_per_men.' => 'float',
	
	'2_banko_kodas' => '',
	'2_banko_saskaita' => '',
	'2_banko_pavadinimas' => '',
	'2_banko_skyrius' => '',
	
	'3_banko_kodas' => '',
	'3_banko_saskaita' => '',
	'3_banko_pavadinimas' => '',
	'3_banko_skyrius' => '',
	
	'4_banko_kodas' => '',
	'4_banko_saskaita' => '',
	'4_banko_pavadinimas' => '',
	'4_banko_skyrius' => '',
	
	//Vyturėlis
	'vieneriu_pietu_kaina_priesmokykliniame_4_val._neapmokestinant_tevu' => 'float',
	
	//Baibokynė
	'ugdymo_dienos_kaina' => 'float',
	'islaikymo_dienos_kaina' => 'float',
	'pilnos_dienos_kaina' => 'float',
	'puses_dienos_kaina' => 'float',
	
	//Tik Vaikystės lobiai:
	'pavadinimas_laukeliui_menesines_nuolaidos_suma' => '',
	'pilnos_dienos_kaina_per_men.' => 'float',
	'puses_dienos_kaina_per_men.' => 'float',
	'pilnos_dienos_maitinimas_per_d._d.' => 'float',
	'puses_dienos_maitinimas_per_d._d.' => 'float',
	'pavadinimas_laukeliui_procentine_nuolaida_nuo_ugdymo_ir_prieziuros_kainos_per_men.' => '',
	'pilnos_dienos_maitinimo_per_men._sumos_riba' => 'float',
	'puses_dienos_maitinimo_per_men._sumos_riba' => 'float',
	
	//Šiauliai Žiogelis:
	'papildoma_kaina_uz_patiekalu_gamyba_per_d._d.' => 'float',
	
	//Tauragės r. Ąžuoliukas
	'kaina_uz_sporta_per_men.,_taikoma_pazymejus_vaiko_duomenyse' => 'float',
	
	//Druskininkų Bitutė
	'dienos_kaina_per_d._lopselyje_be_nuolaidos' => 'float',
	'dienos_kaina_per_d._lopselyje_50_%_nuolaida' => 'float',
	'dienos_kaina_per_d._lopselyje_70_%_nuolaida' => 'float',
	'dienos_kaina_per_d._lopselyje_80_%_nuolaida' => 'float',
	'dienos_kaina_per_d._lopselyje_100_%_nuolaida' => 'float',
	'dienos_kaina_per_d._darzelyje_be_nuolaidos' => 'float',
	'dienos_kaina_per_d._darzelyje_50_%_nuolaida' => 'float',
	'dienos_kaina_per_d._darzelyje_70_%_nuolaida' => 'float',
	'dienos_kaina_per_d._darzelyje_80_%_nuolaida' => 'float',
	'dienos_kaina_per_d._darzelyje_100_%_nuolaida' => 'float',
	'dienos_kaina_per_d._priesmokykliniame_be_nuolaidos' => 'float',
	'dienos_kaina_per_d._priesmokykliniame_50_%_nuolaida' => 'float',
	'dienos_kaina_per_d._priesmokykliniame_70_%_nuolaida' => 'float',
	'dienos_kaina_per_d._priesmokykliniame_80_%_nuolaida' => 'float',
	'dienos_kaina_per_d._priesmokykliniame_100_%_nuolaida' => 'float',
	
	//Kns Žingsnelis ir Drusk Bitutė
	'atskirai_apvalinti_kiekvieno_maitinimo_karto_ir_ugdymo_kainas' => 'bool',
	
	'tabelyje_papildomo_stulpelio_nr._1_pavadinimas' => '',
	'tabelyje_papildomo_stulpelio_nr._1_apačioje_ar_skaičiuoti_sumą/kiekį' => 'bool',
	'tabelyje_papildomo_stulpelio_nr._2_pavadinimas' => '',
	'tabelyje_papildomo_stulpelio_nr._2_apačioje_ar_skaičiuoti_sumą/kiekį' => 'bool',
	'tabelyje_papildomo_stulpelio_nr._3_pavadinimas' => '',
	'tabelyje_papildomo_stulpelio_nr._3_apačioje_ar_skaičiuoti_sumą/kiekį' => 'bool',
	
	'darbuotojo_maitinimo_kaina_dienai' => 'float',
	
	'ar_istaigoje_yra_pusryciai?' => 'bool',
	'ar_istaigoje_yra_priespieciai?' => 'bool',
	'ar_istaigoje_yra_pietus?' => 'bool',
	'ar_istaigoje_yra_pavakariai?' => 'bool',
	'ar_istaigoje_yra_vakariene?' => 'bool',
	'ar_istaigoje_yra_naktipieciai?' => 'bool',
);
?>
<h1>Nustatymai</h1>
<div id="content">
<?php

function config_conv_to_point($option, $value) {
	global $allowed_options;
	if( $allowed_options[$option] == 'float' )
		return str_replace(',', '.', $value);
	return $value;
}
function config_conv_to_comma($option, $value) {
	global $allowed_options;
	if( $allowed_options[$option] == 'float' )
		return str_replace('.', ',', $value);
	return $value;
}

if(isset($_POST['save']) && isset($allowed_options[$_POST['title']])) {
	if (!mysqli_query($db_link, "INSERT INTO `".DB_config."` SET `title`='".db_fix($_POST['title'])."', `value`='".db_fix(config_conv_to_point($_POST['title'], $_POST['value']))."', `valid_from`='".db_fix($_POST['valid_from'])."', `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'"))
		logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
	else msgBox('OK', "Informacija išsaugota!");
} elseif(isset($_POST['update']) && isset($_POST['change']) && isset($allowed_options[$_POST['title']])) {
	if (!mysqli_query($db_link, "UPDATE `".DB_config."` SET `value`='".db_fix(config_conv_to_point($_POST['title'], $_POST['value']))."', `valid_from`='".db_fix($_POST['valid_from'])."',
		`updated`=CURRENT_TIMESTAMP, `updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."',
		`updatedCounter`=`updatedCounter`+1
		WHERE `ID`=".(int)$_POST['update']))
		logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
	else msgBox('OK', "Informacija atnaujinta!");
}

if(isset($_GET['delete']) && KESV) {
	if(db_query("DELETE FROM `".DB_config."` WHERE `ID`=".(int)$_GET['delete']))
		msgBox('OK', "Informacija ištrinta.");
}
?>

<a href="?#configuration-form" class="no-print fast-action fast-action-add">Naujas nustatymas</a>

<table>
<tr>
	<th>Nustatymo pavadinimas</th>
	<th>Reikšmė</th>
	<th>Galioja nuo</th>
	<th>Veiksmai</th>
	<?=(KESV ? '<th>Sukurta *keista</th>' : '')?>
</tr>
<?php
//config title
function ctitle($text) {
	$from = array(
		'vieneriu',
		'pusryciu',
		'pietu_',
		'pavakariu',
		'vakarienes',
		'priesmokykliniame',
		'lopselyje',
		'darzelyje',
		'reikmems',
		'darzelio',
		'saskaita',
		'puses',
		'islaikymo',
		'tevu',
		'saraso',
		'sarasa',
		'vaiku',
		'pavarde',
		'pradeti',
		'eiluteje',
		'men.',
		'menesines',
		'procentine',
		'uz',
		'patiekalu',
		'gamyba',
		'ivedama',
		'zymint',
		'lankomuma',
		'prieziuros',
		'pazymejus',
		'uz',
		'sporta',
		'istaigoje',
		'naktipieciai',
		'pavakariai',
		'pietus',
		'priespieciai',
		'pusryciai',
		'vakariene'
	);
	$to = array(
		'vienerių',
		'pusryčių',
		'pietų_',
		'pavakarių',
		'vakarienės',
		'priešmokykliniame',
		'lopšelyje',
		'darželyje',
		'reikmėms',
		'darželio',
		'sąskaita',
		'pusės',
		'išlaikymo',
		'tėvų',
		'sąrašo',
		'sąrašą',
		'vaikų',
		'pavardė',
		'pradėti',
		'eilutėje',
		'mėn.',
		'mėnesinės',
		'procentinė',
		'už',
		'patiekalų',
		'gamybą',
		'įvedama',
		'žymint',
		'lankomumą',
		'priežiūros',
		'pažymėjus',
		'už',
		'sportą',
		'įstaigoje',
		'naktipiečiai',
		'pavakariai',
		'pietūs',
		'priešpiečiai',
		'pusryčiai',
		'vakarienė'
	);
    return mb_ucfirst(str_replace('_', ' ', str_replace($from, $to, $text)));
}
$result = db_query("SELECT * FROM `".DB_config."` ORDER BY `title` ASC, `valid_from` ASC, `ID` ASC");
if(mysqli_num_rows($result) > 0) {
	while ($row = mysqli_fetch_assoc($result)) {
		echo "		<tr".(isset($_GET['ID']) && $_GET['ID'] == $row['ID'] ? ' class="opened-row"' : '').">
		<td>".ctitle(filterText($row['title']))."</td>
		<td>".(
		isset($allowed_options[$row['title']]) && $allowed_options[$row['title']] == 'bool' ? 
		($row['value'] ? 'Taip' : 'Ne')
		:
		filterText(config_conv_to_comma($row['title'], $row['value']))
		)."</td>
		<td>".($row['valid_from'] == '0000-00-00' ? 'Nuo pradžios' : filterText($row['valid_from']))."</td>
		<td><a href=\"?ID=".$row['ID']."#configuration-form\">Taisyti</a> <a href=\"?ID=".$row['ID']."&DUPLICATE#configuration-form\">Kopijuoti</a>";
		if(KESV)
			echo " <a href=\"?delete=".$row['ID']."\" onclick=\"return alert('Ar tikrai ištrinti?');\">Ištrinti</a>";
		echo "</td>";
		if(KESV)
			echo "<td>".($row['created'] != '0000-00-00 00:00:00' ? $row['created'] : '').($row['updated'] != '0000-00-00 00:00:00' ? ' *'.$row['updated'] : '')."</td>";
		echo "
	</tr>";	
	}
}
?>
</table>

<?php
if( isset($_GET['ID']) ) {
	$result = db_query("SELECT * FROM `".DB_config."` WHERE `ID`=".(int)$_GET['ID']);
	$edit = mysqli_fetch_assoc($result);
}
?>
	<fieldset id="configuration-form">
	<legend><?=(isset($_GET['ID']) ? "Nustatymo „<strong>".ctitle($edit['title'])."</strong>“ ".( (isset($_GET['DUPLICATE']))? "kopija" : "redagavimas" ) : "Naujas nustatymas")?></legend>
	<form action="#" method="post" class="not-saved-reminder">
		<?php
		if( !isset($_GET['ID']) ) {
			echo '<div>Pavadinimas: <div class="sel"><select name="title">';
			$result = db_query("SELECT DISTINCT `title` FROM `".DB_config."` ORDER BY `title`");
			while($row = mysqli_fetch_assoc($result))
				echo '<option value="'.filterText($row['title']).'">'.ctitle(filterText($row['title'])).'</option>';
			echo '</select></div></div>';
		} else
			echo '<input type="hidden" name="title" value="'.filterText($edit['title']).'">';
		//Vaikų sąrašą pirmiau rikiuoti pagal
		//Vaikų_identifikavimo / vaikų atskyrimo / vaikų atpažinimo // vaikų sąraše pirmiau / vaikų sąrašo rikiavime pirmiau
		if(isset($_GET['ID']) && ($edit['title'] == 'vaiku_saraso_eiluteje_pirmiau_rodyti_(vardas_ar_pavarde)' || $edit['title'] == 'vaiku_sarasa_pradeti_rikiuoti_nuo_(vardas_ar_pavarde)')) {
			echo '<div class="sel"><select name="value"><option value="vardas"'.($edit['value'] == 'vardas' ? ' selected="selected"' : '').'>Vardas</option><option value="pavarde"'.($edit['value'] == 'pavarde' ? ' selected="selected"' : '').'>Pavardė</option></select></div>';
		} elseif(isset($_GET['ID']) && isset($allowed_options[$edit['title']]) && $allowed_options[$edit['title']] == 'bool' ) {
			echo '<div class="sel"><select name="value"><option value="1"'.($edit['value'] == '1' ? ' selected="selected"' : '').'>Taip</option><option value="0"'.($edit['value'] == '0' ? ' selected="selected"' : '').'>Ne</option></select></div>';
		} else { ?>
			<p>Reikšmė: <input type="text" name="value" value="<?=(isset($_GET['ID']) ? filterText(config_conv_to_comma($edit['title'], $edit['value'])) : '')?>" style="width: 300px;"></p>
		<?php } ?>
		<p>Galiojimo pradžia: <input type="text" class="datepicker" name="valid_from" value="<?php if( isset($_GET['ID']) ) echo filterText($edit['valid_from']); ?>"><?=(isset($_GET['ID']) ? ' (pradžia 0000-00-00)' : '')?></p>
		<p><?php
		if( isset($_GET['ID']) ) {
			if(isset($_GET['DUPLICATE'])) echo'<input type="submit" name="save" class="submit" value="Išsaugoti naują eilutę">';
			else echo'<input type="hidden" name="update" value="'.(int)$_GET['ID'].'"><input type="submit" name="change" class="submit" value="Išsaugoti">';
		} else
			echo '<input type="submit" name="save" class="submit" value="Išsaugoti naujame laikotarpyje (su nauja galiojimo pradžia)">';
		?></p>
	</form>
	</fieldset>
</div>
