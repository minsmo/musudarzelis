<?php
if(!defined('ADMIN') || !ADMIN) exit();
?>
<h1>Vaikų grupių veiklos planavimo formų kūrimas vadovams</h1>
<div id="content">
<!--
<p><a class="notice no-print" onclick="$('#notice').toggle(); return false;" href="#">Paaiškinimai</a></p>
<p class="notice no-print" id="notice" style="margin-bottom: 5px; display: none;">
	Standartinėse formose duomenų rodymas tėvams neįjungtas, bet jei ateityje norėsite rodyti, tai prieš paskelbiant formą pridėkite peržiūros leidimą tėvams nuėję į formos sudėties keitimą ir tėvus pridėję skyreliuose „Laukelį gali tik peržiūrėti“.
	<br>
	Negalima keisti paskelbtos formos pagal kurią jau yra užpildytas dokumentas, nes užpildę žmonės nesitiki, kad forma pasikeis. Jei norite pakeisti tokią jau esančią formą, turėtumėte sukurti naują formą ir ją įjungti, o senąją išjungti.
	<br>
</p> -->
<?php
//after (un)publish inform other workers about it
//TODO: limit individual planning to one (JS)
//Problem what to show in table? (<select>)
//TODO minor: create new form from other form fields (copy).
//TODO: jei jau yra užpildytų forma rodyti, leisti keisti tik pavadinimus ir kartu parodyti, kad galima keisti tik pavadinimus t.y. tik smulkiai keisti

if(isset($_GET['createSwimmingTrack'])) {
	db_query("INSERT INTO `".DB_forms_widget."` (`kindergarten_id`, `title`, `isDeleted`) VALUES (".DB_ID.", '„Plaukimo takelio“ lentelė', 0)");
	$widget_id = (int)mysqli_insert_id($db_link);
	
	db_query("INSERT INTO `".DB_forms_widget_fields."` (`kindergarten_id`, `widget_id`, `title`, `type`, `order`) VALUES (".DB_ID.", $widget_id, 'Eil. nr.', 0, 0)");
	$field_id = (int)mysqli_insert_id($db_link);
	db_query("INSERT INTO `".DB_forms_widget_fields_allowed."` (`kindergarten_id`, `field_id`, `widget_id`, `allowed_for_person_type`, `action`) VALUES (".DB_ID.", $field_id, $widget_id, 1, 0)");
	
	db_query("INSERT INTO `".DB_forms_widget_fields."` (`kindergarten_id`, `widget_id`, `title`, `type`, `order`) VALUES (".DB_ID.", $widget_id, 'Veiklos pavadinimas', 1, 1)");
	$field_id = (int)mysqli_insert_id($db_link);
	db_query("INSERT INTO `".DB_forms_widget_fields_allowed."` (`kindergarten_id`, `field_id`, `widget_id`, `allowed_for_person_type`, `action`) VALUES (".DB_ID.", $field_id, $widget_id, 1, 0)");
	
	db_query("INSERT INTO `".DB_forms_widget_fields."` (`kindergarten_id`, `widget_id`, `title`, `type`, `order`) VALUES (".DB_ID.", $widget_id, 'Vaiko pasiekimai, kurie vestų į galutinį rezultatą', 1, 2)");
	$field_id = (int)mysqli_insert_id($db_link);
	db_query("INSERT INTO `".DB_forms_widget_fields_allowed."` (`kindergarten_id`, `field_id`, `widget_id`, `allowed_for_person_type`, `action`) VALUES (".DB_ID.", $field_id, $widget_id, 1, 0)");
	
	db_query("INSERT INTO `".DB_forms_widget_fields."` (`kindergarten_id`, `widget_id`, `title`, `type`, `order`) VALUES (".DB_ID.", $widget_id, 'Priemonės', 1, 3)");
	$field_id = (int)mysqli_insert_id($db_link);
	db_query("INSERT INTO `".DB_forms_widget_fields_allowed."` (`kindergarten_id`, `field_id`, `widget_id`, `allowed_for_person_type`, `action`) VALUES (".DB_ID.", $field_id, $widget_id, 1, 0)");
	
	/*db_query("INSERT INTO `".DB_forms_widget_fields."` (`kindergarten_id`, `widget_id`, `title`, `type`, `order`) VALUES (".DB_ID.", $widget_id, 'Galutinis vaiko pasiekimų rezultatas pasiekus uždavinį', 1, 4)");
	$field_id = (int)mysqli_insert_id($db_link);
	db_query("INSERT INTO `".DB_forms_widget_fields_allowed."` (`kindergarten_id`, `field_id`, `widget_id`, `allowed_for_person_type`, `action`) VALUES (".DB_ID.", $field_id, $widget_id, 1, 0)");*/
	//--
	
	db_query("INSERT INTO `".DB_forms."` (`title`, `isPublished`, `isDeleted`) VALUES ('„Plaukimo takelis“', 0, 0)");
	$form_id = (int)mysqli_insert_id($db_link);
	//--
	db_query("INSERT INTO `".DB_forms_fields."` (`form_id`, `title`, `help_text`, `type`, `order`, `isRequired`) VALUES ($form_id, 'Tema/Idėja', '', 1, 0, 0)");
	$field_id = (int)mysqli_insert_id($db_link);
	db_query("INSERT INTO `".DB_forms_fields_allowed."` (`field_id`, `form_id`, `allowed_for_person_type`, `action`) VALUES ($field_id, $form_id, 1, 0)");
	db_query("INSERT INTO `".DB_forms_fields."` (`form_id`, `title`, `help_text`, `type`, `order`, `isRequired`) VALUES ($form_id, 'Vaikų gebėjimų, pasiekimų sritys', '', 1, 1, 0)");
	$field_id = (int)mysqli_insert_id($db_link);
	db_query("INSERT INTO `".DB_forms_fields_allowed."` (`field_id`, `form_id`, `allowed_for_person_type`, `action`) VALUES ($field_id, $form_id, 1, 0)");
	db_query("INSERT INTO `".DB_forms_fields."` (`form_id`, `title`, `help_text`, `type`, `order`, `isRequired`) VALUES ($form_id, 'Amžius', '', 0, 2, 0)");
	$field_id = (int)mysqli_insert_id($db_link);
	db_query("INSERT INTO `".DB_forms_fields_allowed."` (`field_id`, `form_id`, `allowed_for_person_type`, `action`) VALUES ($field_id, $form_id, 1, 0)");
	db_query("INSERT INTO `".DB_forms_fields."` (`form_id`, `title`, `help_text`, `type`, `order`, `isRequired`) VALUES ($form_id, 'Ugdymo(si) uždavinys(iai)', '', 1, 3, 0)");
	$field_id = (int)mysqli_insert_id($db_link);
	db_query("INSERT INTO `".DB_forms_fields_allowed."` (`field_id`, `form_id`, `allowed_for_person_type`, `action`) VALUES ($field_id, $form_id, 1, 0)");
	db_query("INSERT INTO `".DB_forms_fields."` (`form_id`, `title`, `help_text`, `type`, `order`, `isRequired`) VALUES ($form_id, 'Galutinis vaiko pasiekimų rezultatas pasiekus uždavinį', '', 1, 4, 0)");
	$field_id = (int)mysqli_insert_id($db_link);
	db_query("INSERT INTO `".DB_forms_fields_allowed."` (`field_id`, `form_id`, `allowed_for_person_type`, `action`) VALUES ($field_id, $form_id, 1, 0)");
	db_query("INSERT INTO `".DB_forms_fields."` (`form_id`, `title`, `help_text`, `type`, `order`, `isRequired`) VALUES ($form_id, '„Plaukimo takelio“ lentelė', '', $widget_id, 5, 0)");
	$field_id = (int)mysqli_insert_id($db_link);
	db_query("INSERT INTO `".DB_forms_fields_allowed."` (`field_id`, `form_id`, `allowed_for_person_type`, `action`) VALUES ($field_id, $form_id, 1, 0)");
	db_query("INSERT INTO `".DB_forms_fields."` (`form_id`, `title`, `help_text`, `type`, `order`, `isRequired`) VALUES ($form_id, 'Tėvų, kitų darželio pedagogų, specialistų pasiūlymai, pagalba vaikų veiklai', '', 1, 6, 0)");
	$field_id = (int)mysqli_insert_id($db_link);
	db_query("INSERT INTO `".DB_forms_fields_allowed."` (`field_id`, `form_id`, `allowed_for_person_type`, `action`) VALUES ($field_id, $form_id, 1, 0)");
	db_query("INSERT INTO `".DB_forms_fields."` (`form_id`, `title`, `help_text`, `type`, `order`, `isRequired`) VALUES ($form_id, 'Vaikų idėjos, jų sumanyta veikla', '', 1, 7, 0)");
	$field_id = (int)mysqli_insert_id($db_link);
	db_query("INSERT INTO `".DB_forms_fields_allowed."` (`field_id`, `form_id`, `allowed_for_person_type`, `action`) VALUES ($field_id, $form_id, 1, 0)");
	db_query("INSERT INTO `".DB_forms_fields."` (`form_id`, `title`, `help_text`, `type`, `order`, `isRequired`) VALUES ($form_id, 'Veiklos refleksija ir idėjos ateičiai', '', 1, 8, 0)");
	$field_id = (int)mysqli_insert_id($db_link);
	db_query("INSERT INTO `".DB_forms_fields_allowed."` (`field_id`, `form_id`, `allowed_for_person_type`, `action`) VALUES ($field_id, $form_id, 1, 0)");
	header('Location: ?createdSwimmingTrack');
}
if(isset($_GET['createdSwimmingTrack'])) {
	msgBox('OK', 'Sukurta plaukimo takelio forma ir plaukimo takelio lentelė!');
}

if(isset($_POST['save'])) {
	if (!mysqli_query($db_link, "INSERT INTO `".DB_forms."` SET `title`='".db_fix($_POST['main_title'])."',
		`isPublished`=".(isset($_POST['isPublished']) ? 1 : 0).",
        `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'")) {
		logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
	} else {
		$form_id = (int)mysqli_insert_id($db_link);
		if(isset($_POST['title'])) for($i = 0; $i < count($_POST['title']); ++$i) {
			if(!mysqli_query($db_link, "INSERT INTO `".DB_forms_fields."` SET 
				`form_id`=".$form_id.", `title`='".db_fix($_POST['title'][$i])."',
				`help_text`='".db_fix($_POST['help_text'][$i])."', `type`='".(int)$_POST['type'][$i]."',
				`order`='".(int)$i."', `isRequired`=".(int)$_POST['isRequired'][$i]))
		    	logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
		    $field_id = (int)mysqli_insert_id($db_link);
			if(isset($_POST['editPersonType'][$i]) && $_POST['editPersonType'][$i] != '') {
				$arr = explode(',', $_POST['editPersonType'][$i]);
				foreach($arr as $val)
					db_query("INSERT INTO `".DB_forms_fields_allowed."` SET 
						`field_id`=".$field_id.", `form_id`=".$form_id.", `allowed_for_person_type`=".(int)$val);
			}
			if(isset($_POST['viewPersonType'][$i]) && $_POST['viewPersonType'][$i] != '') {
				$arr = explode(',', $_POST['viewPersonType'][$i]);
				foreach($arr as $val)
					db_query("INSERT INTO `".DB_forms_fields_allowed."` SET 
						`field_id`=".$field_id.", `form_id`=".$form_id.", `allowed_for_person_type`=".(int)$val.", `action`=1");
			}
		}
		msgBox('OK', 'Formos informacija išsaugota!');
	}
}

if(isset($_POST['update'])) {
	$form_id = (int)$_POST['update'];
	//someone already answered.
	$result = db_query("SELECT * FROM `".DB_forms_fields_answer."` WHERE `form_id`=".(int)$form_id);
	if(mysqli_num_rows($result) > 0) {
		msgBox('ERROR', 'Formos keisti nebegalima, kažkas pagal ją jau užpildė<!-- atsakė --> dokumentą! Jei norite, kad darbuotojai galėtų pradėti pildyti pagal kitokią formą, sukurkite naują formą ir ją paskelbkite.');
	} else {
		if (!mysqli_query($db_link, "UPDATE `".DB_forms."` SET `title`='".db_fix($_POST['main_title'])."',	
			`isPublished`=".(isset($_POST['isPublished']) ? 1 : 0).",
		    `updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."',
			`updated`=CURRENT_TIMESTAMP()
			WHERE `form_id`=".(int)$form_id)) {
			logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
		} else {
			db_query("DELETE FROM `".DB_forms_fields."` WHERE `form_id`=".(int)$form_id);
			db_query("DELETE FROM `".DB_forms_fields_allowed."` WHERE `form_id`=".(int)$form_id);
			
			for($i = 0; $i < count($_POST['title']); ++$i) {
				db_query("INSERT INTO `".DB_forms_fields."` SET 
					`form_id`=".(int)$form_id.", `title`='".db_fix($_POST['title'][$i])."',
					`help_text`='".db_fix($_POST['help_text'][$i])."',`type`='".(int)$_POST['type'][$i]."',
					`order`='".(int)$i."', `isRequired`=".(int)$_POST['isRequired'][$i]);
				$field_id = (int)mysqli_insert_id($db_link);
				if(isset($_POST['editPersonType'][$i]) && $_POST['editPersonType'][$i] != '') {
					$arr = explode(',', $_POST['editPersonType'][$i]);
					foreach($arr as $val)
						db_query("INSERT INTO `".DB_forms_fields_allowed."` SET 
							`field_id`=".$field_id.", `form_id`=".$form_id.", `allowed_for_person_type`=".(int)$val);
				}
				if(isset($_POST['viewPersonType'][$i]) && $_POST['viewPersonType'][$i] != '') {
					$arr = explode(',', $_POST['viewPersonType'][$i]);
					foreach($arr as $val)
						db_query("INSERT INTO `".DB_forms_fields_allowed."` SET 
							`field_id`=".$field_id.", `form_id`=".$form_id.", `allowed_for_person_type`=".(int)$val.", `action`=1");
				}
			}
			msgBox('OK', 'Formos informacija sėkmingai atnaujinta!');
		}
	}
}

if(isset($_GET['copy'])) {
	$form_id = (int)$_GET['copy'];
	$result = db_query("SELECT * FROM `".DB_forms."` WHERE `form_id`=".(int)$form_id);
	if(mysqli_num_rows($result) > 0) {
		$row = mysqli_fetch_assoc($result);
		db_query("INSERT INTO `".DB_forms."` SET `title`='".db_fix($row['title'])." (kopija)',
			`primary_field_id`='".db_fix($row['primary_field_id'])."',
			`isPublished`=0,
		    `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
		$new_form_id = (int)mysqli_insert_id($db_link);

		$old_fields = [];
		$result = db_query("SELECT * FROM `".DB_forms_fields_allowed."` WHERE `form_id`=".(int)$form_id);
		while($row = mysqli_fetch_assoc($result))
			$old_fields[$row['field_id']][] = $row;
		$result = db_query("SELECT * FROM `".DB_forms_fields."` WHERE `form_id`=".(int)$form_id);
		while($row = mysqli_fetch_assoc($result)) {
			db_query("INSERT INTO `".DB_forms_fields."` SET 
				`form_id`=".(int)$new_form_id.", `title`='".db_fix($row['title'])."',
				`help_text`='".db_fix($row['help_text'])."',`type`='".(int)$row['type']."',
				`order`='".(int)$row['order']."', `isRequired`=".(int)$row['isRequired']);
			$new_field_id = (int)mysqli_insert_id($db_link);
			if(isset($old_fields[$row['field_id']])) foreach($old_fields[$row['field_id']] as $sub_row)
				db_query("INSERT INTO `".DB_forms_fields_allowed."` SET 
				`field_id`=".$new_field_id.", `form_id`=".$new_form_id.", `allowed_for_person_type`=".(int)$sub_row['allowed_for_person_type'].",
				`action`=".(int)$sub_row['action']);
		}
		msgBox('OK', 'Formos informacija sėkmingai nukopijuota!');
	} else {
		msgBox('ERROR', 'Tokios formos nėra.');
	}
}

if(isset($_POST['updateR'])) {
	$form_id = (int)$_POST['updateR'];
	//someone already answered.
	if (!mysqli_query($db_link, "UPDATE `".DB_forms."` SET `title`='".db_fix($_POST['main_title'])."',	
		`isPublished`=".(isset($_POST['isPublished']) ? 1 : 0).",
	    `updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."',
		`updated`=CURRENT_TIMESTAMP()
		WHERE `form_id`=".(int)$form_id)) {
		logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
	} else {
		if(!mysqli_query($db_link, "DELETE FROM `".DB_forms_fields_allowed."` WHERE `form_id`=".(int)$form_id))
			logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
		for($i = 0; $i < count($_POST['title']); ++$i) {
			$field_id = (int)$_POST['field_id'][$i];
			if(!mysqli_query($db_link, "UPDATE `".DB_forms_fields."` SET 
				`title`='".db_fix($_POST['title'][$i])."', `help_text`='".db_fix($_POST['help_text'][$i])."'
				".(KESV ? ", `type`='".db_fix($_POST['type'][$i])."'" : '')."
				WHERE `form_id`=".(int)$form_id." AND `field_id`='".$field_id."'"))
				logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
			
			if(isset($_POST['editPersonType'][$i]) && $_POST['editPersonType'][$i] != '') {
				$arr = explode(',', $_POST['editPersonType'][$i]);
				foreach($arr as $val)
					db_query("INSERT INTO `".DB_forms_fields_allowed."` SET 
						`field_id`=".$field_id.", `form_id`=".$form_id.", `allowed_for_person_type`=".(int)$val);
			}
			if(isset($_POST['viewPersonType'][$i]) && $_POST['viewPersonType'][$i] != '') {
				$arr = explode(',', $_POST['viewPersonType'][$i]);
				foreach($arr as $val)
					db_query("INSERT INTO `".DB_forms_fields_allowed."` SET 
						`field_id`=".$field_id.", `form_id`=".$form_id.", `allowed_for_person_type`=".(int)$val.", `action`=1");
			}
		}
		msgBox('OK', 'Formos informacija sėkmingai atnaujinta!');
	}
}

if(isset($_GET['delete'])) {
	if (!mysqli_query($db_link, "UPDATE `".DB_forms."` SET `isDeleted`=1 WHERE `form_id`=".(int)$_GET['delete'])) {
		logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
	} else
		msgBox('OK', 'Forma sėkmingai ištrinta!');
}

if(isset($_POST['saveTbl'])) {
	if (!mysqli_query($db_link, "INSERT INTO `".DB_forms_widget."` SET `title`='".db_fix($_POST['main_title'])."',
		`kindergarten_id`=".DB_ID.", `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'")) {
		logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
	} else {
		$tbl_id = (int)mysqli_insert_id($db_link);
		if(isset($_POST['title'])) for($i = 0; $i < count($_POST['title']); ++$i) {
			if(!mysqli_query($db_link, "INSERT INTO `".DB_forms_widget_fields."` SET 
				`kindergarten_id`=".DB_ID.",
				`widget_id`=".$tbl_id.", `title`='".db_fix($_POST['title'][$i])."',
				`type`='".(int)$_POST['type'][$i]."',
				`order`='".(int)$i."'"))
		    	logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
		    /*$field_id = (int)mysqli_insert_id($db_link);
			if(isset($_POST['viewPersonType'][$i]) && $_POST['viewPersonType'][$i] != '') {
				$arr = explode(',', $_POST['viewPersonType'][$i]);
				foreach($arr as $val)
					db_query("INSERT INTO `".DB_forms_widget_fields_allowed."` SET 
						`kindergarten_id`=".DB_ID.", `field_id`=".$field_id.", `widget_id`=".$tbl_id.", `allowed_for_person_type`=".(int)$val.", `action`=1");
			}*/
		}
		msgBox('OK', 'Lenetelės informacija išsaugota!');
	}
}
if(isset($_POST['updateTbl'])) {
	$widget_id = (int)$_POST['updateTbl'];
	//someone already answered.
	$forms_with_such_table = array();
	$result = db_query("SELECT `form_id` FROM `".DB_forms_fields."` WHERE `type`=".(int)$widget_id." GROUP BY `form_id`");
	while($row = mysqli_fetch_assoc($result))
		$forms_with_such_table[] = $row['form_id'];
	if(count($forms_with_such_table) > 0)
		$result = db_query("SELECT * FROM `".DB_forms_fields_answer."` WHERE `form_id` IN (".implode(',', $forms_with_such_table).")");//TODO: FIX
	if(count($forms_with_such_table) > 0 && mysqli_num_rows($result) > 0) {
		msgBox('ERROR', 'Lentelės keisti nebegalima, kažkas pagal ją jau užpildė<!-- atsakė --> dokumentą, kurioje ši lentelė yra. Jei norite, kad galėtų pradėti pildyti formą su kitokia lentele, sukurkite naują lentelę ir formą su ja, tada formą paskelbkite.');
	} else {
		if (!mysqli_query($db_link, "UPDATE `".DB_forms_widget."` SET `title`='".db_fix($_POST['main_title'])."'
		    -- , `updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."', `updated`=CURRENT_TIMESTAMP()
			WHERE `widget_id`=".(int)$widget_id." AND `kindergarten_id`=".DB_ID)) {
			logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
		} else {
			if(!mysqli_query($db_link, "DELETE FROM `".DB_forms_widget_fields."` WHERE `widget_id`=".(int)$widget_id." AND `kindergarten_id`=".DB_ID))
				logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
			//if(!mysqli_query($db_link, "DELETE FROM `".DB_forms_widget_fields_allowed."` WHERE `widget_id`=".(int)$widget_id." AND `kindergarten_id`=".DB_ID))
			//	logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
			
			for($i = 0; $i < count($_POST['title']); ++$i) {
				db_query("INSERT INTO `".DB_forms_widget_fields."` SET 
					`kindergarten_id`=".DB_ID.",
					`widget_id`=".(int)$widget_id.", `title`='".db_fix($_POST['title'][$i])."',
					`type`='".(int)$_POST['type'][$i]."', `order`='".(int)$i."'
					-- , `updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."', `updated`=CURRENT_TIMESTAMP()");
				/*$field_id = (int)mysqli_insert_id($db_link);
				if(isset($_POST['viewPersonType'][$i]) && $_POST['viewPersonType'][$i] != '') {
					$arr = explode(',', $_POST['viewPersonType'][$i]);
					foreach($arr as $val)
						db_query("INSERT INTO `".DB_forms_widget_fields_allowed."` SET `kindergarten_id`=".DB_ID.",
							`field_id`=".$field_id.", `widget_id`=".$widget_id.", `allowed_for_person_type`=".(int)$val.", `action`=1");
				}*/
			}
			msgBox('OK', 'Lentelės informacija sėkmingai atnaujinta!');
		}
	}
}
if(isset($_GET['deleteTbl'])) {
	if (!mysqli_query($db_link, "UPDATE `".DB_forms_widget."` SET `isDeleted`=1 WHERE `widget_id`=".(int)$_GET['deleteTbl']." AND `kindergarten_id`=".DB_ID)) {
		logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
	} else
		msgBox('OK', 'Lentelė sėkmingai ištrinta!');
}

if(isset($_GET['publish_form_id'])) {
	db_query("UPDATE `".DB_forms."` SET
			`isPublished`=".(isset($_GET['publish']) ? 1 : 0).",
		    `updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."',
			`updated`=CURRENT_TIMESTAMP()
			WHERE `form_id`=".(int)$_GET['publish_form_id']);
	msgBox('OK', 'Forma sėkmingai '.(isset($_GET['publish']) ? 'paskelbta/įjungta pildymui' : 'išjungta').'.');
}

if(isset($_POST['updateSelect'])) {
		db_query("UPDATE `".DB_forms."` SET
			`primary_field_id`=".((int)$_POST['field_id']).",
		    `updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."',
			`updated`=CURRENT_TIMESTAMP()
			WHERE `form_id`=".(int)$_POST['updateSelect']);
	msgBox('OK', 'Formai pakeistas laukelis, kurio reikšmė rodoma bendroje lentelėje/sąraše.');
}

$field_types = array(
	1 => 'Daug teksto eilučių',
	0 => 'Viena teksto eilutė',
	2 => 'Data',
	3 => 'Vaiko individualus planavimas'
);
$widget_field_types = array(
	0 => 'Numeris',
	1 => 'Daug teksto eilučių',
);
//
if(KaunoZiburelis)
	$field_types[4] = 'Žiburėlio lentelė';
//if(TESTINIS)
//	$field_types[5] = '„Plaukimo takelis“';
$res = db_query("SELECT * FROM `".DB_forms_widget."` WHERE `isDeleted`=0 AND `kindergarten_id`=".DB_ID);
while($row = mysqli_fetch_assoc($res))
	$field_types[$row['widget_id']] = $row['title'];
?>
<a href="?new#forms" class="no-print fast-action fast-action-add">Nauja planavimo forma</a>
<a href="?newTbl#forms" class="no-print fast-action fast-action-add">Nauja lentelė</a>
<a href="?createSwimmingTrack" class="no-print fast-action fast-action-add" onclick="return confirm('Ar tikrai sukurti standartinę „Plaukimo takelio“ lentelę (bus pavadinta „Plaukimo takelio“ lentelė) ir formą (bus pavadinta „Plaukimo takelis“)?')">Sukurti standartinę „Plaukimo takelio“ lentelę ir formą</a><?php /*Galėtų būti  */ ?>


<?php
$result = db_query("SELECT `".DB_forms."`.*, `".DB_forms_fields."`.`title` `titleField` FROM `".DB_forms."` LEFT JOIN `".DB_forms_fields."` ON `".DB_forms_fields."`.field_id=primary_field_id WHERE isDeleted=0 ORDER BY `created` DESC");
if(mysqli_num_rows($result) > 0) {
	?>
<h2>Formos</h2>
<table id="forms-tbl"<?=(isset($_GET['preview']) || isset($_GET['previewTbl']) ? ' class="no-print"' : '')?>>
<tr>
	<th>Formos pavadinimas</th>
	<th title="Laukelis, kurio reikšmė rodoma bendroje lentelėje/sąraše esančiame stulpelyje „Papildoma info“ meniu punkte „Planavimas formose“"><span class="abbr">Rodmuo</span></th>
	<th>Ar paskelbta pildymui?</th>
	<th>Sukurta <span title="Nuo naujausiai sukurto iki vėliausiai">↓</span></th>
	<?php if(KESV) { ?>
	<th>Pakeista</th>
	<?php } ?>
	<th>Veiksmai</th>
</tr>
	<?php
	$opened_form_id = 0;//opened-row
	if(isset($_GET['preview']))
		$opened_form_id = (int)$_GET['preview'];
	if(isset($_GET['edit']))
		$opened_form_id = (int)$_GET['edit'];
		if(isset($_GET['editR']))
		$opened_form_id = (int)$_GET['editR'];
	if(isset($_POST['save']))
		$opened_form_id = (int)$_POST['save'];
	if(isset($_POST['update']))
		$opened_form_id = (int)$_POST['update'];
	if(isset($_GET['select']))
		$opened_form_id = (int)$_GET['select'];
	while ($row = mysqli_fetch_assoc($result)) {
		?><tr<?=($opened_form_id == $row['form_id'] ? ' class="opened-row"' : '')?>>
		<td><?=filterText($row['title'])?></td>
		<td><?=filterText($row['titleField'])?></td>
		<td><?=($row['isPublished'] ? '<span class="green" style="padding: 2px 3px;">Paskelbta</span>' : '<span class="warning" style="padding: 2px 3px;">Išjungta</span>')?> <a href="?publish_form_id=<?=$row['form_id']?><?=($row['isPublished'] ? '' : '&amp;publish')?>"><?=($row['isPublished'] ? 'Išjungti' : 'Paskelbti')?></a></td>
		<td><?=substr($row['created'], 0, -3)?></td>
		<?php if(KESV) { ?>
		<td><?=substr($row['updated'], 0, -3)?></td>
		<?php } ?>
		<td><a href="?preview=<?=filterText($row['form_id'])?>#form-preview">Išankstinė peržiūra</a> | <a href="?edit=<?=filterText($row['form_id'])?>#forms">Keisti sudėtį</a> | <a href="?select=<?=filterText($row['form_id'])?>#forms">Rinktis rodmenį</a> | <a href="?copy=<?=filterText($row['form_id'])?>" title="Kopijuoti formą">Kopijuoti</a> | <a href="?delete=<?=filterText($row['form_id'])?>" onclick="return confirm('Ar tikrai norite ištrinti?')">Trinti</a></td>
		</tr>
		<?php
	}
}
?>
</table>

<?php
$result = db_query("SELECT * FROM `".DB_forms_widget."` WHERE isDeleted=0 AND `kindergarten_id`=".DB_ID." ORDER BY `created` DESC");
if( mysqli_num_rows($result) > 0 && !( isset($_GET['editR']) || isset($_GET['edit']) || isset($_GET['new']) ) ) {
	?>
	<h2>Formų lentelės (skirta tik įterpti į formą)</h2>
	<table id="forms-tbl">
	<tr>
		<th>Lentelės pavadinimas (lentelė įdedama formoje pasirinkus laukelio rūšį)</th>
		<th>Sukurta</th>
		<th>Veiksmai</th>
	</tr>
	<?php
	$opened_tbl_id = 0;//opened-row
	if(isset($_GET['previewTbl']))
		$opened_tbl_id = (int)$_GET['previewTbl'];
	if(isset($_GET['editTbl']))
		$opened_tbl_id = (int)$_GET['editTbl'];
	if(isset($_POST['saveTbl']))
		$opened_tbl_id = (int)$_POST['saveTbl'];
	if(isset($_POST['updateTbl']))
		$opened_tbl_id = (int)$_POST['updateTbl'];
	while ($row = mysqli_fetch_assoc($result)) {
		?><tr<?=($opened_tbl_id == $row['widget_id'] ? ' class="opened-row"' : '')?>>
		<td><?=filterText($row['title'])?></td>
		<td><?=$row['created']?></td>
		<td><a href="?previewTbl=<?=filterText($row['widget_id'])?>#tbl-preview">Išankstinė peržiūra</a> <a href="?editTbl=<?=filterText($row['widget_id'])?>#forms">Keisti sudėtį</a> <a href="?deleteTbl=<?=filterText($row['widget_id'])?>" onclick="return confirm('Ar tikrai norite ištrinti?')">Trinti</a></td>
		</tr>
		<?php
	}
	?></table><?php
}

include 'forms.html_forms.php';
include 'forms.tbl.php';

?>


</div>
<script src="/workers/forms.js?v2"></script>
