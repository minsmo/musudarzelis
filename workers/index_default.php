		<?php
		//echo '<!--[if lte IE 6]><script src="libs/ie6/warning.js"></script><script>window.onload=function(){e("libs/ie6/")}</script><![endif]-->';
		//❄❆❊❅❉☃
		
		//Like button for each news item
		//demo visai gražus http://www.webcodo.net/like-dislike-system-with-jquery-ajax-and-php-youtube-like-design/
		//dar variantas filter: opacity(0.5)
		?>
		<h1>Pradžia</h1>
		<div id="content">
		<?php
		if(substr(CURRENT_DATE, 5, 5) == '10-05') {
			//echo '<<h1>☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺</h1><br>';
			echo '<h1 class="center" style="float: none;">Sveikiname su tarptautine Mokytojų diena!<br>Tebūna Jums šie darbo metai kūrybingi ir sėkmingi, teišsipildo Jūsų troškimai ir viltys.<br>Gražios šventės!</h1>';
			//echo '<h1>☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺</h1>';
		}
		
		if(ADMIN) {
			$todo = array();

			$result = getValidEmployeesOrdered_db_query();
			if(mysqli_num_rows($result) <= 1) {
				$todo[] = "Darbuotojus meniu punkte „Darbuotojai“. <a href=\"/darbuotojai?#employee-form\">Įvesti darbuotoją</a>";//Neįvesti darbuotojai meniu punkte
			}
			if(count(getAllGroups()) == 0) {
				$todo[] = "Auklėtojų grupių pavadinimus ir prie jų priskirti darbuotojus meniu punkte „Grupės“.";//Neįvesti grupių pavadinimai meniu punkte
			}
			$result = db_query("SELECT * FROM `".DB_forms."` WHERE `isPublished`=1 AND `isDeleted`=0");
			if(mysqli_num_rows($result) == 0) {
				$todo[] = "Patvirtinti standartines planavimo formas, o jei jos netinka prisitaikyti ar iš naujo sudaryti meniu punkte „Planavimo formų kūrimas“. Tai reikalinga, kad pedagogai pagal šią (-as) formą (-as) galėtų pradėti pildyti planavimus.";
				//$todo[] = "Patvirtinti standartines planavimo formas, o jei jos netinka prisitaikyti ar iš naujo sudaryti planavimo formą (-as) meniu punkte „Planavimo formų kūrimas“, kad pedagogai pagal šią (-as) formą (-as) galėtų pradėti pildyti planavimus.";//Pedagogai negali pildyti planavimo, nes vadovai nepaskelbė planavimo formų, pagal kurias pedagogai pildys planavimą (jei netinka standartinės planavimo formos tuomet dar reikia ir sudaryti). Vadovai tai atlikti turi meniu punkte „Planavimo formų kūrimas“
			}
			
			if(!empty($todo)) {
				echo "<div style=\"background-color: #bcd8bc; padding: 7px;\">
				Prašome pradžiai įvesti duomenis<!-- Pradžioje sistemoje labiausiai trūksta duomenų: -->
				<ul><li>";
				echo implode('</li><li>', $todo);
				echo '</li></ul>
				Iškilus klausimams, pasiūlymams prašome kreiptis į „Mūsų darželis“.
				</div>';
			}
		}
		
		function downloadData() {
			global $place;
			$ctx = stream_context_create([
				'http' => [
					'timeout' => 1 
					]
				] 
			);
			return file_get_contents('http://api.openweathermap.org/data/2.5/weather?q='.$place[DB_ID].'&units=metric&APPID=8a4e3d9bd0d2d89728a003b3485dc934', false, $ctx);
		}
		
		if(/*TESTINIS &&*/ !empty($place[DB_ID])) {
			$result = db_query("SELECT * FROM `0weather` WHERE `place`='".$place[DB_ID]."'");
			if(mysqli_num_rows($result)) {
				$row = mysqli_fetch_assoc($result);
				$seconds_diff = time() - strtotime($row['updated']);
				if($seconds_diff >= 60*10/*min*/) {
					$data = downloadData();
					if($data !== false) {
						db_query("UPDATE `0weather` SET `data`='".db_fix($data)."', `requests`=`requests`+1 WHERE `place`='".$place[DB_ID]."'");
						$seconds_diff = 0;
					}
				} else {
					$data = $row['data'];
				}
			} else {
				$seconds_diff = 0;
				$data = downloadData();
				if($data !== false)
					db_query("INSERT INTO `0weather` SET `place`='".$place[DB_ID]."', `data`='".db_fix($data)."', `updated`=CURRENT_TIMESTAMP, `requests`=1");
			}
			if($data === false)
				$data = $row['data'];
			$data = json_decode($data, true);
			//echo "<pre>";print_r($data);echo "</pre>";
			function windDirection($deg) {
				$arrows = ["↓", "↙", "←", "↖", "↑", "↗", "→", "↘"];
				$idx = round($deg / 45) % count($arrows);
				return $arrows[$idx];
			}
			if(isset($data['main']['temp']))
				echo '<div class="weather-term">Temperatūra</div><div class="weather-term-desc">'.str_replace('.', ',', $data['main']['temp']).'°</div>';
			if(isset($data['main']['humidity']))
			echo '<div class="weather-term"><span class="abbr" title="Santykinis vandens garų kiekis ore">Santykinė drėgmė</span></div><div class="weather-term-desc">'.str_replace('.', ',', $data['main']['humidity']).' %</div>';
			if(isset($data['wind']['speed']) && isset($data['wind']['deg']))
				echo '<div class="weather-term">Vėjo greitis</div><div class="weather-term-desc">'.str_replace('.', ',', $data['wind']['speed']).' m/s'.(isset($data['wind']['deg']) ? ' <span title="Vėjo kryptis" class="abbr">'.windDirection($data['wind']['deg']).'</span>' : '').'</div>';
			if(isset($data['clouds']['all']))
				echo '<div class="weather-term">Debesuotumas</div><div class="weather-term-desc">'.$data['clouds']['all'].' %'.($data['clouds']['all'] == 0 ? ' (giedra)' : '').'</div>';
			if(isset($data['main']['pressure'])) {
				echo '<div class="weather-term">Atmosferos slėgis</div>';
				if($data['main']['pressure'] >= 1020) {
					echo '<div class="weather-term-desc" style="color: #357d32;font-weight: normal;"><span class="abbr" title="'.round($data['main']['pressure']*0.750061683).' mm Hg (1 hPa = 0.750061683 mm Hg)">'.$data['main']['pressure'].' hPa</span> (<span class="abbr" title="Ką gali rodyti: gražu, keičiasi, lija">gražus oras</span>)</div>';
				} elseif($data['main']['pressure'] >= 980) {
					echo '<div class="weather-term-desc" style="color: #0B0B61;font-weight: normal;"><span class="abbr" title="'.round($data['main']['pressure']*0.750061683).' mm Hg (1 hPa = 0.750061683 mm Hg)">'.$data['main']['pressure'].' hPa</span> (<span class="abbr" title="Ką gali rodyti: gražu, keičiasi, lija">keičiasi oras</span>)</div>';
				} else {
					echo '<div class="weather-term-desc" style="color: #660000;font-weight: normal;"><span class="abbr" title="'.round($data['main']['pressure']*0.750061683).' mm Hg (1 hPa = 0.750061683 mm Hg)">'.$data['main']['pressure'].' hPa</span> (<span class="abbr" title="Ką gali rodyti: gražu, keičiasi, lija">lija</span>)</div>';
				}//TODO: make graph
				//mm Hg -> 1 hectopascal (hektopaskalis) = 0.750061683 mm Hg (Gyvsidabrio stulpelio milimetras arba toras)
			}
			if(isset($data['sys']['sunrise']) && isset($data['sys']['sunset']))
				echo '<div class="weather-term"><span class="abbr" title="Saulė teka–leidžiasi">Teka–leidžiasi</span></div><div class="weather-term-desc" style="font-weight: normal;">'.date('H:i', $data['sys']['sunrise']).'–'.date('H:i', $data['sys']['sunset']).'</div>';
			echo '<div class="weather-term"><span class="abbr" title="Informacija atnaujinta prieš">Atnaujinta prieš</span></div><div class="weather-term-desc" style="font-weight: normal;">'.($seconds_diff == 0 ? 'Dabar' : round($seconds_diff/60).' min. '.round($seconds_diff%60).' s.').'</div>';
			echo '<div class="cl"></div>';//960-980 - red, 980-1020 - blue, 1020-1050 - green
		}
		
		$result = db_query("SELECT * FROM `1reminders` WHERE `kindergarten_id`=".DB_ID.' AND `isCompleted`=0'.(ADMIN ? ' AND (`forEmployeeId`='.DARB_ID.' OR `forEmployeeId`<>'.DARB_ID.' AND `isVisibleToAdmin`=1)' : " AND `forEmployeeId`=".DARB_ID)." ORDER BY `deadline` DESC");
		if(mysqli_num_rows($result)) {
			?>
			<table>
			<caption>Nebaigtų darbų priminimai:<caption>
			<tr>
				<th class="date-cell">Padaryti iki</th>
				<th>Darbas</th>
				<th>Sukūrė</th>
				<th>Daro</th>
				<th class="no-print">Veiksmai</th>
			</tr>
			<?php
			while($row = mysqli_fetch_assoc($result)) {
				echo "<tr>
				<td>".filterText($row['deadline'])."</td>
				<td>".nl2br(filterText($row['activity']))."</td>
				<td>".getAllEmployees($row['createdByEmployeeId']).' '.$row['created']."</td>
				<td>".getAllEmployees($row['forEmployeeId'])."</td>
				<td class=\"no-print\"><a href=\"/work_reminders?edit=".$row['ID']."\">Keisti</a> ".(!$row['isCompleted'] ? "<a href=\"/work_reminders?completed=".$row['ID']."\">Jau padarytas</a>" : '')."</td>
				</tr>";
			}
			echo '</table>';
		}



		if(!(BUHALT || SESELE || DIETISTAS)) {
			$today = date('N');
			if(ADMIN)
				$result = db_query("SELECT `".DB_bureliai."`.`pavadinimas`, `".DB_bureliai_schedule."`.*
					FROM `".DB_bureliai_schedule."` 
					LEFT JOIN `".DB_bureliai."` ON `".DB_bureliai_schedule."`.`burelio_id`=`".DB_bureliai."`.`ID`
					WHERE `diena`='".$today."'
					ORDER BY `diena`, `nuoVal`, `nuoMin`, `ikiVal`, `ikiMin`, `".DB_bureliai."`.`pavadinimas`");
			else
				$result = db_query("SELECT `".DB_bureliai."`.`pavadinimas`, `".DB_bureliai_schedule."`.*
					FROM `".DB_bureliai_schedule."` 
					LEFT JOIN `".DB_bureliai."` ON `".DB_bureliai_schedule."`.`burelio_id`=`".DB_bureliai."`.`ID`
					WHERE `diena`='".$today."'
					ORDER BY `nuoVal`, `nuoMin`, `ikiVal`, `ikiMin`, `".DB_bureliai."`.`pavadinimas`");
			//`grup_id`<>0 DESC, 
			if(mysqli_num_rows($result)) {
				$print_str = false;
				$str = ' 
				<table class="vertical-hover">
				<caption>Užsiėmimai šiandien ('.mb_lcfirst($dienos_kada[$today]).'):</caption>
				<tr>
					<th>Grupė arba vaikai</th>
					<th>Veikla</th>
					<th>Nuo</th>
					<th>Iki</th>
				</tr>';
				$kids_of_schedule = [];
				$res = db_query("SELECT `".DB_bureliai_schedule_kids."`.*, cr.*
					FROM `".DB_bureliai_schedule_kids."`
					JOIN `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<=CURDATE() GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from` AND cr.`parent_kid_id`=`".DB_bureliai_schedule_kids."`.`kid_id`
					WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0".(ADMIN ? '' : " AND cr.`grupes_id`=".GROUP_ID)." ORDER BY ".orderName('cr'));
				while($row = mysqli_fetch_assoc($res))
					$kids_of_schedule[$row['bureliai_schedule_id']][$row['kid_id']] = filterText(getName($row['vardas'], $row['pavarde']));

				while($row = mysqli_fetch_assoc($result)) {
					if(ADMIN || $row['grup_id'] == GROUP_ID || $row['grup_id'] == 0 && isset($kids_of_schedule[$row['ID']])) {
						$print_str = true;
						$str .= (isset($prev_group) && $prev_group != $row['grup_id'] ? '<tr><td colspan="10" style="background-color: rgb(228, 228, 228);"></td></tr>' : '').'
						<tr>
							<td>';
							if($row['grup_id'] != 0)
								$str .= filterText(getAllGroups($row['grup_id']));
							if(isset($kids_of_schedule[$row['ID']]))
								$str .= implode('<br>', $kids_of_schedule[$row['ID']]);

							$str .= '</td>
							<td>'.filterText($row['pavadinimas']).'</td>
							<td>'.nr_val_min($row['nuoVal'], $row['nuoMin']).'</td>
							<td>'.nr_val_min($row['ikiVal'], $row['ikiMin']).'</td>';
							
							$prev_group = $row['grup_id'];
							
						$str .= '</tr>';
					}
				}
				$str .= '</table>';
				if($print_str)
					echo $str;
			}
		}
		$today = date('Y-m-d');
		if(ADMIN)
			$result = db_query("SELECT * FROM `".DB_activities."` WHERE `kindergarten_id`=".DB_ID." AND `period`>='".$today."' ORDER BY `period` DESC");
		else
			$result = db_query("SELECT * FROM `".DB_activities."` WHERE `kindergarten_id`=".DB_ID." AND `period`>='".$today."' AND (`group_id`=0 OR `group_id`=".GROUP_ID.") ORDER BY `period` DESC");
		if(mysqli_num_rows($result) > 0) {
			echo '<table>
			<caption>Artėjantys (numatomi) renginiai</caption>
			<tr>
				<th class="date-cell">Data</th>
				<th>Vaikų grupė</th>
				<th>Veikla</th>
				<th>Kur</th>
				<th>Kita</th>
			</tr>';
			
			while($row = mysqli_fetch_assoc($result))
				echo "<tr><td>".($row['period'] == $today ? '<span class="suggested">'.filterText($row['period']).'</span>' : filterText($row['period']))."</td><td>".getAllGroups($row['group_id'])."</td><td>".nl2br(filterText($row['activity']))."</td><td>".nl2br(filterText($row['where']))."</td><td>".nl2br(filterText($row['other']))."</td></tr>";
			echo '</table>';
		}
		?>
		<br><div>Pastaba. Sistemoje „Mūsų darželis“ <!-- kiekviena --> įstaiga <!-- pasirenka --> renkasi funkcijas naudojimui, atitinkančias jų poreikius.
		<?php //Sistemoje „Mūsų darželis“ kiekviena įstaiga renkasi funkcijas pagal savo poreikius, kurias naudos //įstaigoje. ?></div>
		<?php if(TESTINIS && AUKLETOJA) { ?>
			<a href="#" onclick="startIntro();">Kur vykdoma vaikų lankomumo apskaita?</a>
			<?php
			/*
			Daro, kad vidiniam elementui suteikia class ar id'ą, o parentą paryškina
			*/
			?>
			<style>
			.introjs-fixParent {
    position: fixed !important;
    }
    .introjs-helperLayer {
    	background-color: rgba(255,255,255,.1);
    }
			</style>
			<script type="text/javascript">
			function startIntro(){
				//$("body").append('<div id="blackText"><style type="text/css">.introjs-showElement{color:#fff !important;} .introjs-helperLayer{background-color: rgba(255,255,255,.3); border: 1px solid rgba(255,255,255,.5);} /*.introjs-fixParent{position: static !important;}*/ </style></div>');
				var intro = introJs().oncomplete(function() {
					//$("#blackText").remove();
				}).onexit(function() {
					//$("#blackText").remove();
				});
			  intro.setOptions({
				steps: [
				  {
					element: document.querySelector("#lank1"),
					intro: "Žymimas lankomumas.",
					//highlightClass: 'blackText'
				  },
				  {
					element: document.querySelector("#lank2"),
					intro: "Pateisinamos dienos, kad neįtrauktų į apmokamas dienas. Pvz., pamaininio darbo, raštelio iš gydytojo.",
					//highlightClass: 'blackText'
				  }
				],
				nextLabel: 'Kitas',//Toliau
				prevLabel: 'Ankstesnis',//Anksčiau
				skipLabel: 'Uždaryti',
				doneLabel: 'Baigti'
			  });
	// introjs-showElement introjs-relativePosition
			  intro.start();
			}
			</script>
		<?php } ?>
		<!--[if lt IE 8]>
		<p class="browsehappy">Jūs naudojatės <strong>pasenusia</strong> naršykle. Prašome <a href="http://browsehappy.com/?locale=en">atnaujinkite savo naršyklę</a>, kad pilnavertiškai naudotumėtės „Mūsų Darželiu“. Tam padaryti greičiausiai turėtumėte kreiptis pas kompiuteristą, kuris <strong>atnaujintų</strong> šią interneto naršyklę arba geriausia, kad įdiegtų <strong>Firefox</strong> ar <strong>Google Chrome</strong> interneto naršyklę. Arba prašome susisiekti su mumis: info@musudarzelis.lt arba +370 6077 2346. Kartu rasime sprendimą.</p>
		<![endif]-->
		<script type="text/javascript" src="/libs/jquery-migrate-1.2.1.min.js"></script>
<script>
$(function(){
	var userLang = navigator.language || navigator.userLanguage;
	userLang = userLang.substr(0, 2);//for cases: en-us, lt-lt, en-gb
	if(userLang != 'lt') {
		if ( $.browser.webkit ) {
			$( "#webkit" ).show();
		}
		if ( $.browser.mozilla ) {
			$( "#firefox" ).show();
		}
	}
});
</script>
<div id="webkit" style="border: 1px solid #000; padding: 7px; margin: 10px 0;" hidden>
<!-- Jūsų interneto naršyklės kalba nėra lietuvių.  -->
<strong>Svarbu</strong>. Jūsų patogumui rekomenduojame interneto naršyklės kalbą pakeisti į <strong>lietuvių kalbą</strong> remiantis 8 žingsnių instrukcija (kad būtų lengviau naudotis):<!-- rekomenduojame ją pakeisti į lietuvių kalbą tokiu būdu --></strong><br>
1. Ekrano viršutiniame dešiniame kampe suraskite ir spustelkite nustatymų mygtuką. Iliustracija: <img src="/img/howto/webkit1.png" alt="Mygtukas „Nustatymai“" title="Fotografija ko ieškoti ekrane" style="border: 4px solid red;"> <!-- Interneto naršyklėje suraskite nustatymų ikoną ir ją spustelkite (kairiu pelės klavišu dešiniarankiams, kitų nenaudosime). Lengvesniam įsivaizdavimui toliau pateikėme iliustraciją, kurioje ši ikona apibraukta raudonu fonu: --><br><br>
2. Atsiradusiame meniu spustelėkite ant „<em>Settings</em>“. Iliustracija:<br><img src="/img/howto/webkit2.png" alt="Pasirinkimas „Settings“"><br><br>
3. Apačioje spustelėkite „<em>Show advanced settings</em>“. Iliustracija:<br><img src="/img/howto/webkit3.png" alt="Mygtukas „Show advanced settings“"><br><br>
4. Spustelėkite ant „<em>Language and input settings...</em>“. Iliustracija:<br><img src="/img/howto/webkit4.png" alt="Mygtukas „Language and input settings...“"><br><br>
5. Skyrelyje „<em>Languages</em>“ pasitikrinkite ar nėra lietuvių/lithuanian kalbos jei yra spustelėkite ant jos. Kitu atveju Spustelėkite „<em>Add</em>“. Iliustracija: <img src="/img/howto/webkit5.png" alt="Languages"><br><br>
6. Jei nebuvo lietuvių kalbos, tuomet šiame lange ją pasirinkite ir spustelėkite ant „<em>OK</em>“. Iliustracija:<br><img src="/img/howto/webkit6.png" alt="Lithuanian ir OK"><br><br>
7. Spustelėkite ant „<em>Display Google Chrome in this language</em>“, pažymėkite varnelę ties „<em>Use this language for spell checking</em>“ ir spustelkite „<em>Done</em>“. Iliustracija:<br> <img src="/img/howto/webkit7.png" alt=""><br><br>
8. Kad pamatyti lietuvių kalba: uždarykite interneto naršyklę ir ją iš naujo atidarykite.
</div>
<div id="firefox" style="border: 1px solid #000; padding: 7px; margin: 10px 0;" hidden>
<!-- <span class="notice">Nuorodos adresas atnaujintas 2016-04-02</span> -->
<strong>Svarbu</strong>. Jūsų patogumui rekomenduojame atsisiųsti ir įsidiegti interneto naršyklę Mozilla Firefox <strong>lietuvių kalba</strong> (kad būtų lengviau naudotis). <a href="https://www.mozilla.org/lt/firefox/new/?scene=2" target="_blank">Atsisiųsti galite šiame puslapyje spustelėję ant žalio mygtuko „Parsisiųskite“</a>.
<!-- Was not working on Windows 7 and English Firefox https://www.mozilla.org/lt/firefox/new/ -->
</div>

		<?php
		//https://www.youtube.com/watch?v=9lfC9kmohBI What parents say and what their children hear - Bright Side
		?>
		<h2>Svarbiausios naujienos</h2>
		<?php include 'index_news.php'; ?>
		</div>
