<?php
if(!defined('ADMIN') | !ADMIN) exit();
?>
<h1>Darbuotojai</h1>
<div id="content">
<?php
if(isset($_GET['issaugoti'])) {
	if (!mysqli_query($db_link, "INSERT INTO `".DB_employees."` SET `vardas`='".db_fix($_POST['vardas'])."',
		`pavarde`='".db_fix($_POST['pavarde'])."', `pareigybes`='".db_fix($_POST['pareigybes'])."',
		`nesiojamas_tel`='".db_fix($_POST['nesiojamas_tel'])."',
		`namu_tel`='".db_fix($_POST['namu_tel'])."', `pastabos`='".db_fix($_POST['pastabos'])."',
		`ataskaitininke`='".(isset($_POST['ataskaitininke']) ? (int)$_POST['ataskaitininke'] : 0)."',
        `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'")) {
		logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
	} else {
		msgBox('OK', 'Darbuotojo informacija išsaugota!');
		$emploee_id = (int)mysqli_insert_id($db_link);
	}
}
if(isset($_GET['atnaujinti'])) {
	$select = "UPDATE `".DB_employees."` SET `vardas`='".db_fix($_POST['vardas'])."',
		`pavarde`='".db_fix($_POST['pavarde'])."', `pareigybes`='".db_fix($_POST['pareigybes'])."',
        `nesiojamas_tel`='".db_fix($_POST['nesiojamas_tel'])."', `namu_tel`='".db_fix($_POST['namu_tel'])."',
		`pastabos`='".db_fix($_POST['pastabos'])."',
		`ataskaitininke`='".(isset($_POST['ataskaitininke']) ? (int)$_POST['ataskaitininke'] : 0)."',
		`updated`=CURRENT_TIMESTAMP, `updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."',
		`updatedCounter`=`updatedCounter`+1
		WHERE `ID`=".(int)$_GET['atnaujinti'];
	if (!mysqli_query($db_link, $select)) {
		logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
	} else {
		msgBox('OK', 'Darbuotojo informacija atnaujinta!');
		$emploee_id = (int)$_GET['atnaujinti'];
	}
}
if(isset($_GET['issaugoti']) || isset($_GET['atnaujinti']) || isset($_GET['assign'])) {
	if(isset($_GET['assign']))
		$emploee_id = (int)$_GET['assign'];
	if(!empty($_POST['pastas']) && !empty($_POST['person_type'])) {
		$_POST['pastas'] = filter_var($_POST['pastas'], FILTER_VALIDATE_EMAIL);
		if($_POST['pastas']) {
			$result = db_query("SELECT * FROM `".DB_users."` WHERE `email`='".db_fix($_POST['pastas'])."'");
			$row = mysqli_fetch_assoc($result);
			if(mysqli_num_rows($result) == 1) {
				$result = db_query("SELECT * FROM `".DB_users_allowed."` WHERE `user_id`='".(int)$row['user_id']."' AND `person_type`='".(int)$_POST['person_type']."' AND `person_id`='".$emploee_id."'");
				if(mysqli_num_rows($result) >= 1) {
					msgBox('WARN', 'Toks leidimas jau yra pridėtas. Be reikalo bandoma tokį patį pridėti dar kartą.');
				} else {
					db_query("INSERT INTO `".DB_users_allowed."` SET `user_id`='".(int)$row['user_id']."', `person_type`=".(int)$_POST['person_type'].", `person_subtype`=0, `person_id`='".$emploee_id."'");
					msgBox('OK', 'Sistemoje pridėtas dar vienas leidimas (jau esančiam prisijungimui).');
				}
			} else {
				if(register($_POST['vardas'], $_POST['pavarde'], $_POST['pastas'], false)) {
					if (!mysqli_query($db_link, "INSERT INTO `".DB_users_allowed."` SET `user_id`='".(int)mysqli_insert_id($db_link)."', `person_type`=".(int)$_POST['person_type'].", `person_subtype`=0, `person_id`='".$emploee_id."'")) {
						logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
					} //else msgBox('OK', 'Sėkmingai išsaugota!');
				}
			}
		} else msgBox('ERROR', 'Netinkamas el. paštas.');
	}
}

if(isset($_GET['delete'])) {
	db_query("UPDATE `".DB_employees."` SET `isDeleted`=1 WHERE `ID`=".(int)$_GET['delete']);
	/*if (!mysqli_query($db_link, "DELETE FROM `".DB_employees."` WHERE `ID`=".(int)$_GET['delete'])) {
		logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
	} else*/ msgBox('OK', 'Darbuotojo informacija ištrinta!');
	
}

if(isset($_GET['newPassword'])) {
	$password = randomPassword(8);
	$msg = "Sveiki,<br><br>Naują slaptažodį Jums sukūrė ".filterText($_SESSION['USER_DATA']['name'].' '.$_SESSION['USER_DATA']['surname'])." iš ".filterText($config['darzelio_pavadinimas'])." sistemai <a href=\"http://musudarzelis.lt/\">musudarzelis.lt</a>.<br><br>Prisijungimo el. pašto adresas yra {$_GET['newPassword']}<br>Naujai sudarytas slaptažodis yra {$password}<br><br>Smagaus naršymo.";

	$result = db_query("UPDATE `".DB_users."` SET `password`='".passwd_hash($password)."' WHERE `email`='".db_fix($_GET['newPassword'])."'");
	loga('Password changed: '.$_GET['newPassword'], 'login');
	$result = db_query("SELECT * FROM `".DB_users."` WHERE `email`='".db_fix($_GET['newPassword'])."'");
	$user = mysqli_fetch_assoc($result);
	if(sendemail($user['name'], $_GET['newPassword'], "MusuDarzelis", "info@musudarzelis.lt", 'Slaptažodžio keitimas musuDarzelis.lt', $msg, 'html'))
		header('Location: ?successful');
}
if(isset($_GET['successful'])) {
	msgBox('OK', 'Išsiųstas el. laiškas su nauju slaptažodžiu. Neužmirškite informuoti, kad el. pašte patikrintų šlamšto (angl. spam, junk) skyrių.');//katalogą
}
?>
<a href="?#employee-form" class="no-print fast-action fast-action-add">Naujo darbuotojo įvedimas</a> <?=ui_print()?>
<table id="employee-tbl">
<tr>
	<th>Eil.<br>nr.</th>
	<th>Vardas Pavardė</th>
	<th>Pagrindinės pareigos</th>
	<th>Mob. tel.<br>Namų tel.</th>
	<th>Pastabos</th>
	<th>Atsa-<br>kingas už<br>atas-<br>kaitas<!-- lankomumo žiniaraštį --></th>
	<th>Prieiga</th>
	<th class="no-print">Veiksmai</th>
</tr>
<?php
$groups = array();
$result = db_query("SELECT * FROM `".DB_employees_groups."` JOIN `".DB_groups."`  ON `".DB_employees_groups."`.`grup_id`=`".DB_groups."`.`ID`") ;
while ($row = mysqli_fetch_assoc($result)) {
	if(!isset($groups[$row['darb_id']]))
		$groups[$row['darb_id']] = array();
	$groups[$row['darb_id']][] = $row['pavadinimas'];
}
$darbuotojai = array();
$darbuotojai[0] = '';
$result = getValidEmployeesOrdered_db_query();
if(mysqli_num_rows($result) > 0) {
	$i = 0;
	while ($row = mysqli_fetch_assoc($result)) {
		$darbuotojai[$row['ID']] = filterText($row['vardas']." ".$row['pavarde']);
		echo "		<tr".(isset($_GET['edit']) && $_GET['edit'] == $row['ID'] || isset($_GET['assign']) && $_GET['assign'] == $row['ID'] ? ' class="opened-row"' : '').">
		<td>".++$i."</td>
		<td style=\"white-space: nowrap\">".filterText($row['vardas']." ".$row['pavarde'])."</td>
		<td>".(isset($pareigybes[$row['pareigybes']]) ? $pareigybes[$row['pareigybes']] : '')."</td>
        
		<td>".nr($row['nesiojamas_tel'])."<br>".nr($row['namu_tel'])."</td>
		<td>".filterText($row['pastabos'])."</td>
		<td>".($row['ataskaitininke'] ? "Taip" : "Ne")."</td>
		";
		$res = db_query("SELECT * FROM `".DB_users_allowed."` JOIN `".DB_users."` ON `".DB_users_allowed."`.`user_id`=`".DB_users."`.`user_id` 
			WHERE `person_type`>0 AND `person_id`='".(int)$row['ID']."'");
		if(mysqli_num_rows($res) > 0) {
			echo '<td>';
			while($r = mysqli_fetch_assoc($res)) {
				//if(KESV) {
					if($r['last_login_time'] != '0000-00-00 00:00:00')
						echo '<span class="notice">'.substr($r['last_login_time'], 0, -3).'</span> ';
					else
						echo '<span class="notice"><a href="?newPassword='.filterText($r['email']).'" title="Sudaryti ir išsiųsti naują slaptažodį į '.filterText($r['email']).'" onclick="return confirm(\'Rekomenduojame naudoti tik tada jei Jūs jau buvote informavę darbuotoją ir jis vis tiek nerado el. laiško su slaptažodžiu net ir šlamšto (angl. junk, spam) skyrelyje (kataloge, dėžutėje).\n\nAr tikrai norite sudaryti naują slaptažodį el. pašto adresui '.filterText($r['email']).' ir jį išsiųsti?\')">Naujas slaptažodis</a></span> ';
				//}
				echo '<strong>'.$person_type[$r['person_type']].'</strong>';//.' per '.filterText($r['email']).'<br>';
				if(isset($login_depends_on_assigned_groups[$r['person_type']])) {
					if(isset($groups[$row['ID']])) {
						if(count($groups[$row['ID']]) > 1)
							//http://stackoverflow.com/questions/6677035/jquery-scroll-to-element
							echo ': <a title="'.filterText(implode(', ', $groups[$row['ID']])).'" href="#" onclick="$(this).next().toggle(); $(\'html, body\').stop().animate( {\'scrollTop\': $(this).offset().top-40}, 900, \'swing\'); return false;" class="abbr">Daug grupių</a><em style="display: none;">'.implode(', ', $groups[$row['ID']]).'</em>';
						else
							echo ' <em>'.filterText(implode(', ', $groups[$row['ID']])).'</em>';
					} else
						echo ' <em><span class="warning">- (leidimo įgalinimui priskirkite grupes meniu punkte „Grupės“</span>)</span></em>';
				} else {
					echo ' <em>visose grupėse</em>';
				}
				echo '<br>';
			}
		} else {
			echo '<td class="warning"><a href="?assign='.$row['ID'].'#employee-form" title="Prisirinkti „Leidimų grupę“, įrašyti „El. pašto adresą“ ir išsaugoti">+ Naujas leidimas prisijungti</a>';
		}
		echo "</td>
		<td class=\"no-print\"><a href=\"?edit=".$row['ID']."#employee-form\">Keisti</a> <a href=\"?delete=".$row['ID']."\" onclick=\"return confirm('Ar tikrai norite trinti?')\">Trinti</a></td>
	</tr>";
	}
}
if(isset($_GET['edit']) || isset($_GET['assign'])) {
	$result = db_query("SELECT * FROM `".DB_employees."` WHERE `ID`=".(int)(isset($_GET['edit']) ? $_GET['edit'] : $_GET['assign']));
	$darb = mysqli_fetch_assoc($result);
}
?>
</table>

<script type="text/javascript" src="/workers/darbuotojai.js"></script>

<fieldset id="employee-form" class="no-print">
<legend><?=(!isset($_GET['assign']) ? (isset($_GET['edit']) ? "Darbuotojo duomenų redagavimas" : "Naujo darbuotojo įvedimas") : 'Priskyrimas leidimo darbuotojui  '.filterText($darb['vardas'].' '.$darb['pavarde']))?></legend>
<form action="?<?=(!isset($_GET['assign']) ? (!isset($_GET['edit']) ? 'issaugoti' : 'atnaujinti='.(int)$darb['ID']) : 'assign='.(int)$darb['ID'])?>" method="post" id="darb" onsubmit="return validateInput(this)" class="not-saved-reminder">
	<?php if(!isset($_GET['assign'])) { ?>
	<p><label>Vardas<span class="required" title="Privaloma įvesti">*</span> <input required="required" type="text" name="vardas" value="<?=(isset($_GET['edit']) ? filterText($darb['vardas']) : '')?>"></label></p>
	<p><label>Pavardė<span class="required" title="Privaloma įvesti">*</span> <input required="required" type="text" name="pavarde" value="<?=(isset($_GET['edit']) ? filterText($darb['pavarde']) : '')?>"></label></p>
	<div class="sel-wrapper"><label for="pareigybes">Pagrindinės pareigos<span class="required" title="Privaloma įvesti">*</span></label><div class="sel"><select id="pareigybes" name="pareigybes" required="required">
	<option value="">Nepasirinkta</option>
	<?php
	foreach( $pareigybes as $key => $value)
		echo "<option value=\"$key\"".(isset($_GET['edit']) && $darb['pareigybes'] == $key ? ' selected="selected"' : '').">$value</option>";
	?>
	</select></div></div>
	<p><label><span class="not-important">Nešiojamas telefonas</span> <input type="text" name="nesiojamas_tel" value="<?=(isset($_GET['edit']) ? nr_edit($darb['nesiojamas_tel']) : '')?>"></label></p>
	<p><label><span class="not-important">Namų telefonas</span> <input type="text" name="namu_tel" value="<?=(isset($_GET['edit']) ? nr_edit($darb['namu_tel']) : '')?>"></label></p>
	<p><label><span class="not-important">Pastabos:</span> <textarea name="pastabos"><?=(isset($_GET['edit']) ? filterText($darb['pastabos']) : '')?></textarea></label></p>
	<p><label><span class="not-important">Atsakingas už ataskaitas</span> <!-- lankomumo žiniaraštį --> <input type="checkbox" name="ataskaitininke" value="1"<?=(isset($_GET['edit']) && $darb['ataskaitininke']  ? ' checked="checked"' : '')?>"></label></p>
    <hr width="99%">
    <?php
    } elseif(isset($_GET['assign'])) {
    	?><input type="hidden" name="vardas" value="<?=filterText($darb['vardas'])?>"><input type="hidden" name="pavarde" value="<?=filterText($darb['pavarde'])?>"><?php
    }
	if(isset($_GET['edit'])) {
		$result = db_query("SELECT *
			FROM `".DB_users_allowed."` JOIN `".DB_users."` ON `".DB_users_allowed."`.`user_id`=`".DB_users."`.`user_id` 
			WHERE `person_type`>0 AND `person_id`='".(int)$_GET['edit']."'");
		$edit_logins = mysqli_num_rows($result);
		if($edit_logins > 0) {
			echo '<p><strong>Esanti prieiga prie sistemos</strong>:<br><table>
			<tr>
				<th>Leidimas</th>
				<th>El. paštas</th>
				<th>Veiksmai</th>';
				if(KESV && isset($_GET['edit']) && !isset($_GET['print'])) {
					echo '<th>Sukurta</th>
					<th>Paskutinis prisijungimas</th>';
				}
			echo '
			</tr>';
			while($row = mysqli_fetch_assoc($result)) {
				echo '<tr><td>'.$person_type[$row['person_type']].'</td><td>'.(KESV ? '<a href="/?Lpastas='.filterText($row['email']).'">' : '').filterText($row['email']).(KESV ? '</a>' : '').'</td><td><a href="/prisijungimai?edit='.(int)$row['user_id'].'#login-form">Keisti el. pašto adresą</a> <a href="/prisijungimai?del_user_id='.$row['user_id'].'&amp;del_person_type='.$row['person_type'].'&amp;del_person_id='.$row['person_id'].'">[-] Pašalinti leidimą</a></td>';
				
				if(KESV && isset($_GET['edit']) && !isset($_GET['print'])) {
					echo '<td>'.$row['created'].'</td>';
					echo '<td>'.$row['last_login_time'].'</td>';
				}
				echo '</tr>';
			}
			echo '</table></p>';
		}
	}
	?>
	<div class="sel-wrapper">
	<strong><!-- Duoti n Nauja -->Pridėti dar vieną NAUJĄ prieigą prie sistemos</strong> (jei dirbs sistemoje)<br>
	<!-- <span class="notice">Logopedo leidimų grupė skirta specialiajam pedagogui, tiflopedagogui, surdopedagogui ir kartais psichologui.</span> -->
	<label for="person-type" title="Ką gali daryti ir matyti prisijungęs žemiau įvestu el. paštu " class="abbr">Leidimų grupė<?=(isset($_GET['assign']) ? '<span class="required" title="Privaloma įvesti">*</span>' : '')?></label><!-- nurodytu = įvestu  (šioje sistemoje [tikriausiai savaime aišku]) -->
		<div class="sel"><select name="person_type" id="person-type"<?=(isset($_GET['assign']) ? ' required="required"' : '')?>>
		<option value="">Pasirinkite leidimų grupę</option>
		<?
		foreach($person_type_extended as $key => $val)
			if($key != 0)//Exclude parents
				echo "<option value=\"".$key."\">".$val."</option>";
		?>
		</select></div></label></div>
	
	<p><label><span class="abbr" title="Tas pats el. pašto adresas gali turėti daugiau nei vieną leidimą">El. pašto adresui<?=(isset($_GET['assign']) ? '<span class="required" title="Privaloma įvesti">*</span>' : '')?></span> <input type="email" name="pastas" style="width: 352px;"<?=(isset($_GET['assign']) ? ' required="required"' : '')?>></label></p>
	<?php
	if(KESV && isset($_GET['edit']) && !isset($_GET['print'])) {
		echo 'Sukurta: &nbsp;'.$darb['created'].' '.(isset($darbuotojai[$darb['createdByEmployeeId']]) ? $darbuotojai[$darb['createdByEmployeeId']] : $darb['createdByEmployeeId']).';<br>Pakeista: '.$darb['updated'].' '.(isset($darbuotojai[$darb['updatedByEmployeeId']]) ? $darbuotojai[$darb['updatedByEmployeeId']] : $darb['updatedByEmployeeId']);
	}
	?>
	<p><input type="submit" value="Išsaugoti" class="submit"></p>
	<br>
</form>
</fieldset>
</div>
