<?php if(!defined('DARBUOT') && !DARBUOT && (AUKLETOJA || ADMIN)) exit();
$_GET['limit'] = !isset($_GET['limit']) ? 12 : (int)$_GET['limit'];
?>
<h1>Vaikų lankomumo apskaitos žiniaraštis -&gt; Užbaigimas <!-- Lankomumo, pateisinimų, vaiko maitinimo duomenų užrakinimas (negalima keisti) --></h1>
<div id="content">
<p class="notice">Atrakinti gali tik vadovai.</p>
<?php
if(isset($_GET['lock'])) {
	list($date, $group_id) = explode('.', $_GET['lock']);
	$group_id = (int)$group_id;
	if($group_id != 0 && !hasGroup((int)$group_id)) die();
	$currently_locked = db_query("SELECT * FROM `1lock_kids_attendance_food` WHERE `kindergarten_id`=".DB_ID." AND `date`='".db_fix($date)."-01'".
			($group_id == 0 ? 
			//(!isset($_SESSION['ALL_GROUPS']) ? 
			' AND `group_id` IN ('.implode(',', array_keys(getAllowedGroups())).')'// : '')
			:
			' AND `group_id`='.(int)$group_id)
		);
	if($group_id == 0) {
		$groups = getAllowedGroups();
		while ($row = mysqli_fetch_assoc($currently_locked))
			unset($groups[$row['group_id']]);
	} else/*if($group_id != 0)*/ {
		$groups = [$group_id => ''];
		while ($row = mysqli_fetch_assoc($currently_locked))
			unset($groups[$row['group_id']]);
	}
	
	if(!empty($groups)) {
		foreach($groups as $group_id => $dumb)
			db_query("INSERT INTO `1lock_kids_attendance_food` SET
				`kindergarten_id`=".DB_ID.",
				`date`='".db_fix($date)."-01', `group_id`='".(int)$group_id."',
				`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
		msgBox('OK', 'Informacija užbaigta '.$date.' sėkmingai.');
	} else 
		msgBox('ERROR', 'Jau užrakinta.');
	
}
if(isset($_GET['unlock'])) {
	if(ADMIN) {
		list($date, $group_id) = explode('.', $_GET['unlock']);
		$group_id = (int)$group_id;
		if($group_id != 0 && !hasGroup((int)$group_id)) die();
		//Needs transaction of two queries
		$currently_locked = db_query("SELECT * FROM `1lock_kids_attendance_food` WHERE `kindergarten_id`=".DB_ID." AND `date`='".db_fix($date)."-01'".
				($group_id == 0 ? 
				//(!isset($_SESSION['ALL_GROUPS']) ? 
				' AND `group_id` IN ('.implode(',', array_keys(getAllowedGroups())).')'// : '')
				:
				' AND `group_id`='.(int)$group_id)
			);
		$groups = [];
		while ($row = mysqli_fetch_assoc($currently_locked))
			if(hasGroup($row['group_id']))
				$groups[$row['group_id']] = '';
	
		if(!empty($groups)) {
			db_query("DELETE FROM `1lock_kids_attendance_food` WHERE `kindergarten_id`=".DB_ID." AND `date`='".db_fix($date)."-01' AND `group_id` IN (".implode(',', array_keys($groups)).")");// `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'
		
			msgBox('OK', 'Informacija užbaigta '.mysqli_affected_rows($db_link).' grupėms sėkmingai.');
		} else 
			msgBox('ERROR', 'Jau užrakinta.');
	} else {
		echo 'Atrakinti gali tik vadovas.';
	}
}
//

function URL($new_parameters = [], $unset_parameters = []) {
	global $_GET;
	$__GET = $_GET;//Keep what is saved in URL
	unset($__GET['q'], $__GET['new'], $__GET['lock'], $__GET['unlock']);//1. Clean garbage
	$__GET = array_merge($__GET, $new_parameters);//2. Add new
	foreach($unset_parameters as $val)//3. Remove unused
		unset($__GET[$val]);
	return '?'.http_build_query($__GET);
}

$locks = [];
$result = db_query("SELECT * FROM `1lock_kids_attendance_food` WHERE `kindergarten_id`=".DB_ID.
	(!isset($_SESSION['ALL_GROUPS']) ? ' AND `group_id` IN ('.implode(',', array_keys(getAllowedGroups())).')' : ''));//Just an "optimization" that is probably not needed. 
while ($row = mysqli_fetch_assoc($result))
	$locks[substr($row['date'], 0, 7).$row['group_id']] = '';
?>
<form method="get">
	<div style="float: left; margin-right: 5px; line-height: 33px;">Nuo:</div>
	<div class="sel" style="float: left; margin-right: 5px; line-height: 33px;">
	<select name="dateFrom">
		<?php
		$result = db_query("SELECT YEAR(`data`) `year`, MONTH(`data`) `month` FROM `".DB_attendance."` GROUP BY YEAR(`data`) DESC, MONTH(`data`) DESC");
		while ($row = mysqli_fetch_assoc($result))
			echo '<option value="'.$row['year'].'-'.men($row['month']).'"'.((isset($_GET['dateFrom'])) && $row['year'].'-'.men($row['month']) == $_GET['dateFrom'] ? ' selected="selected" class="selected">'.$selectedMark : '>').$row['year'].'-'.men($row['month'])."</option>";
		?>
	</select></div>
	<div style="float: left; margin-right: 5px; line-height: 33px;">po</div>
	<input title="Po kiek mėnesių rodyti?" class="abbr" name="limit" type="number" min="1" max="100" style="float: left; margin-right: 5px; line-height: 33px; width:45px;" value="<?=(int)$_GET['limit']?>">
	<div style="float: left; margin-right: 5px; line-height: 33px;">mėn.</div>
	<input type="submit" value="Rodyti" class="filter">
</form>
<?php
if(!isset($_GET['dateFrom']))
	echo 'Rodoma 12-a naujausių mėnesių:';
?>
<table>
	<?php
	$dates = [];
	$years = [];
	$result = db_query("SELECT YEAR(`data`) `year`, MONTH(`data`) `month` FROM `".DB_attendance."`
	".(isset($_GET['dateFrom']) ? " WHERE `data`>='".db_fix($_GET['dateFrom'])."-01' GROUP BY YEAR(`data`) ASC, MONTH(`data`) ASC"/*from*/ : ' GROUP BY YEAR(`data`) DESC, MONTH(`data`) DESC'/*TOP*/).' LIMIT 0,'.(int)$_GET['limit']);
	while ($row = mysqli_fetch_assoc($result)) {
		$dates[] = $row['year'].'-'.men($row['month']);
		if(!isset($years[$row['year']]))
			$years[$row['year']] = 1;
		else
			++$years[$row['year']];
	}
	if(!isset($_GET['dateFrom'])) {
		$dates = array_reverse($dates);
		$years = array_reverse($years, true);
	}
	
	//Optional, so can be removed:
	/*$lock_possibility = [];
	$result = db_query("SELECT YEAR(`data`) `year`, MONTH(`data`) `month`, `kid_group_id` FROM `".DB_attendance."`
	".(isset($_GET['dateFrom']) ? " WHERE `data`>='".db_fix($_GET['dateFrom'])."-01' GROUP BY YEAR(`data`) ASC, MONTH(`data`) ASC, `kid_group_id`"/*from/ : ' GROUP BY YEAR(`data`) DESC, MONTH(`data`) DESC, `kid_group_id`'/*TOP/).' LIMIT 0,'.(int)$_GET['limit']);
	while ($row = mysqli_fetch_assoc($result))
		$lock_possibility[$row['year'].'-'.men($row['month']).$row['kid_group_id']] = '';*/
	
	echo '<tr><th colspan="2">Laikotarpiai ↦</th>';
	foreach($years as $year => $cnt)
		echo '<th colspan="'.$cnt.'" class="center">'.$year.'</th>';
	echo '</tr>';
	echo '<tr><th>Nr.</th><th>Grupės ↧</th>';
	foreach($dates as $date)
		echo '<th class="center">'.substr($date, 5, 2).'</th>';
	echo '</tr>';
	$locked_dates = [];
	$i = 0;
	foreach(getAllowedGroups() as $ID => $title) {
		echo '<tr><th>'.++$i.'</th><th>'.$title.'</th>';
		foreach($dates as $date) {
			$bg = '';//!isset($lock_possibility[$date.$ID]) ? ' style="background-color: grey;"' : '';
			$title = '';//!isset($lock_possibility[$date.$ID]) ? '; nėra pažymėto lankomumo' : '';
			if(isset($locks[substr($date, 0, 7).$ID])) {
				if(!isset($locked_dates[substr($date, 0, 7)]))
					$locked_dates[substr($date, 0, 7)] = 1;
				else
					++$locked_dates[substr($date, 0, 7)];
				if(ADMIN)
					echo '<td'.$bg.'><a title="Užrakinta'.$title.'. Spustelėjus - atrakinsite." href="'.URL(['unlock' => $date.'.'.$ID]).'" onclick="return confirm(\'Ar tikrai norite atrakinti?\')"><img src="/img/icon/lock-32-32.png"></a></td>';
				else
					echo '<td'.$bg.'><img title="Užrakinta. Atrakinti gali tik vadovas." src="/img/icon/lock-32-32.png"></td>';
			} else
				echo '<td'.$bg.'><a title="Atrakinta'.$title.'. Spustelėjus - užrakinsite." href="'.URL(['lock' => $date.'.'.$ID]).'"><img src="/img/icon/key-32-32.png"></a></td>';
			//&#x1f512; //http://www.fileformat.info/info/unicode/char/1f512/index.htm //http://stackoverflow.com/questions/1384380/is-there-a-unicode-glyph-that-looks-like-a-key-icon //Bootstrap
		}
			
		echo '</tr>';
	}
	echo '<tr><th colspan="2">Veiksmas visoms grupėms:</th>';
		foreach($dates as $date)
			if(isset($locked_dates[$date]) && $locked_dates[$date] == count(getAllowedGroups())) {
				if(ADMIN)
					echo '<td><img src="/img/icon/key-32-32.png" style="visibility: hidden"><br>
					<a title="Užrakinta. Spustelėjus - atrakinsite." href="'.URL(['unlock' => $date.'.0']).'" onclick="return confirm(\'Ar tikrai norite atrakinti?\')"><img src="/img/icon/lock-32-32.png"></a></td>';
				else
					echo '<td><img src="/img/icon/key-32-32.png" style="visibility: hidden"><br>
					<img title="Užrakinta. Atrakinti gali tik vadovas." src="/img/icon/lock-32-32.png"></td>';
			} elseif(!isset($locked_dates[$date]))
				echo '<td><a title="Atrakinta. Spustelėjus - užrakinsite." href="'.URL(['lock' => $date.'.0']).'"><img src="/img/icon/key-32-32.png"></a></td>';
			else {
				if(ADMIN)
					echo '<td>
				<a title="Atrakinta. Spustelėjus - užrakinsite." href="'.URL(['lock' => $date.'.0']).'"><img src="/img/icon/key-32-32.png"></a><br>
				<a title="Užrakinta. Spustelėjus - atrakinsite." href="'.URL(['unlock' => $date.'.0']).'" onclick="return confirm(\'Ar tikrai norite atrakinti?\')"><img src="/img/icon/lock-32-32.png"></a>
				</td>';
				else
					echo '<td>
				<a title="Atrakinta. Spustelėjus - užrakinsite." href="'.URL(['lock' => $date.'.0']).'"><img src="/img/icon/key-32-32.png"></a><br>
				<img title="Užrakinta. Atrakinti gali tik vadovas." src="/img/icon/lock-32-32.png">
				</td>';
			}
			//&#x1f512; //http://www.fileformat.info/info/unicode/char/1f512/index.htm //http://stackoverflow.com/questions/1384380/is-there-a-unicode-glyph-that-looks-like-a-key-icon //Bootstrap
			
		echo '</tr>';
?>
</table>
<?php
/*
$week_day = date('N', strtotime($row['date']));
echo "<td>".$week_day.' '.mb_lcfirst($dienos[$week_day])."</td>
Grafikos šaltinis
http://www.myiconfinder.com/icon/close-hide-lock-locked-password-privacy-private-protection-restriction-safe-secure-security/
http://www.myiconfinder.com/icon/authentication-key-hidden-hide-lock-password-security-private/7993

completed icon
task completed icon
*/
?>
<script src="/libs/tablehover/jquery.tablehover.pack.js"></script>
<script>$('table').tableHover({rowClass: 'hover', colClass: 'hover', /*clickClass: 'click',*/ headRows: true,  
                    footRows: true, headCols: true, footCols: true}); 
</script>
</div>
