function del(date,id) {
	$.post("/workers/tabeliai_remove.php", {id : id, date: date}, function(data) {
		if(data != 'ok')
			alert('Neleista ištrinti. Tik darbuotojai išsaugoją ar atnaujinę lankomumą gali ištrinti.');
		else
			location.reload();
	}).fail(function() {
		alert( "Nepavyko ištrinti lankomumo, galimos interneto ryšio problemos iki serverio. Išsisaugokite kompiuteryje ir bandykite vėliau." );
	});
}
$(function() {
	$(document).on('click', '.click-to-edit', function (event) {
		event.preventDefault();
		var text = $(this).text();
		$(this).data('text', text);
		$(this).text('');
		$('<p><input type="text" class="pastabos-tabeliai" value="'+text+'"> <a href="#" class="saveScnt" title="Išsaugoti į laukelį įvestą informaciją">Išsaugoti</a> <a href="#" class="remScnt" title="Pašalinti šį laukelį">Atšaukti</a></p>').appendTo($(this));
		$(this).removeClass('click-to-edit');
		$(this).find('input').focus();
	});
	
	$(document).on('click', '.remScnt', function (event) {
		event.preventDefault();
		//var text = $(this).prev().prev('input').val();
		//console.log(text);
		var td = $(this).parents('td');
		$(this).parent('p').remove();
		td.addClass('click-to-edit').text(td.data('text'));
	});
	
	$(document).on('click', '.saveScnt', function (event) {
		event.preventDefault();
		//console.log(year, month);
		var content = $(this).prev('input').val();
		var td = $(this).parents('td');
		if(content || td.data('text')) {
			//console.log('Not empty - save');
			var data = {
					id : td.data('id'),
					year : year,
					month : month,
					group_id : group_id};
			data[td.data('type')] = content;
			$.post( "/workers/tabeliai_pastabos.php", data, function(data) {
				if(!data.startsWith('ok')) {
					alert( "Neišsaugota. Užrakintas tabelis." );
				}
			//alert( "success" );
			console.log( "Data Loaded: " + data );
			}).fail(function() {
				alert( "Nepavyko išsaugoti, galimos interneto ryšio problemos iki serverio. Išsisaugokite kompiuteryje ir bandykite vėliau." );
			});
			 /* .done(function() {
				alert( "second success" );
			})*/
			//.always(function() {
			//	alert( "finished" );
			//});
			/*$.ajax({
				type: 'POST',
				url: "/workers/tabeliai_pastabos.php",
				data: {
					id : $(this).parents('td').data('id'),
					data : pastabos
				},
				success: success,
				dataType: dataType
			});*/
			td.text(content);
			td.addClass('click-to-edit');
		}
		//console.log($(this).prev('input').val());
		//console.log($(this).parents('td').data('id'));
	});
	
	$(document).on('click', '.click-to-edit-money', function (event) {
		event.preventDefault();
		$(this).find(".no-print").hide();
		var text = $(this).find(".no-print").text();
		//$(this).data('text', text);
		//$(this).text('');
		$('<p><input type="text" value="'+text+'" class="charge-tabeliai"> <a href="#" class="saveMon" title="Išsaugoti į laukelį įvestą informaciją">Išsaugoti</a> <a href="#" class="remMon" title="Pašalinti šį laukelį">Atšaukti</a></p>').appendTo($(this));
		$(this).removeClass('click-to-edit-money');
		$(this).find('input').focus();
	});
	
	$(document).on('click', '.remMon', function (event) {
		event.preventDefault();
		var td = $(this).parents('td');
		$(this).parent('p').remove();
		td.addClass('click-to-edit-money').find(".no-print").show();//text(td.data('text'));
	});
	//TODO: calculate total sum on the fly with JS, w/o the need of additional request to server
	//TODO: change on-print number
	$(document).on('click', '.saveMon', function (event) {
		event.preventDefault();
		var charge = $(this).prev('input').val();
		var td = $(this).parents('td');
		//if(charge || td.data('text')) {
			//console.log('Not empty - save');
			$.post( "/workers/tabeliai_charge.php", {
					id : td.data('id'),
					charge : charge,
					year : year,
					month : month,
					group_id : group_id}, function(data)
			{
				//alert( "success" );
				if(!data.startsWith('ok')) {
					alert( "Neišsaugota. Užrakintas tabelis." );
				}
				console.log( "Data Loaded: " + data );
			}).fail(function() {
				alert( "Nepavyko išsaugoti, galimos interneto ryšio problemos iki serverio. Išsisaugokite kompiuteryje ir bandykite vėliau." );
			});
			$(this).parent('p').remove();
			td.find(".no-print").text(charge).show();
			td.addClass('click-to-edit-money');
		//}
		//console.log($(this).prev('input').val());
		//console.log($(this).parents('td').data('id'));
	});
	$('#tabelis-tbl').tableHover({rowClass: 'hover', colClass: 'hover', /*clickClass: 'click',*/ headRows: true, footRows: true, headCols: true, footCols: true}); 
	
	/*$(".excel").click(function(){
		$("#tabelis-tbl").table2excel({
			// exclude CSS class
			exclude: ".noExl",
			name: "Tabelis"
		}); 
	});*/
					
	$(document).on('click', '.nZ', function(){
		var a = $(document).scrollTop();
		$('#nZ').show().html("<div class='exitDiv' onclick='$(\"#nZ\").hide();$(\"#blackScreen\").hide();'>X</div>"+
		$(this).attr('data-name')+", ["+($(this).parent().html()[0] == 'n' ? 'nŽ' : 'Ž')+"]<br /><br />"+
		"A) <a href='#' onclick='del(\""+$(this).attr('data-date')+"\",\""+$(this).attr('data-parent-kid-id')+"\")'>Ištrinti žymėtą lankomumą vaikui "+$(this).attr('data-date')+", nes vaiko oficialiai nebuvo registruoto darželyje</a><br /><br />ARBA<br /><br />"+
		"B) Sutvarkyti vaiko darželyje oficialias įregistravimo ir išreigstravimo datas meniu punkte „<a href=\"/vaikai?edit="+$(this).attr('data-kid-id')+"\" class=\"abbr\" title=\"Atidaryti vaiko duomenis\">Vaikai</a>“ (keičiant ar pridedant papildomus duomenis).<br /><small><span class='href' onclick='$(\"#information\").toggle();$(\"#nZ\").css(\"top\",(($(window).height()-$(\"#nZ\").height())/2-$(\"#header\").height()+$(document).scrollTop())+\"px\")'>Kaip rasti oficialiai išregistruotus vaikus iš darželio?</span><div id='information' hidden>Rasite archyve:<br /> 1) nueiti į meniu punktą „Vaikai“<br /> 2) spustelėti ant „Paieška (filtravimas)“<br /> 3) pažymėti varnelę ant „archyvas (išregistruotųjų)“<br /> 4) spustelėti „Ieškoti“.</div></small>").css("top",(($(window).height()-$('#nZ').height())/2-$("#header").height()+$(document).scrollTop())+"px").css("left",(($(window).width()-400)/2-$("#nav").width())+"px");
		$("#blackScreen").show();
	});
});
