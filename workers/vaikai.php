<?php if(!defined('DARBUOT') /*|| !DARBUOT not needed*/) exit();
$bylos_nr_pagal_registracijos_knyga_zurnala = PasvalioLiepaite || DB_PREFIX == 'pnvZil_';
?>
<h1>Vaikai</h1>
<div id="content">
<?php
define('IS_ALLOWED_TO_EDIT', true);//ADMIN || KaunoZiburelis || KaunoVarpelis || RODOMASIS || TESTINIS || SEMINARINE
$send_registration_email = ALLOW_PARENTS;//KaunoVaikystesLobiai || TESTINIS || LazdijuKregzdute || LazdijuVyturelis || TauragesRAzuoliukas || LazdijuRAzuoliukas || PasvalioLiepaite || DruskininkuBitute || KretingosRGruslaukesPagrMok;
$allow_archive = (!PasvalioLiepaite || (PasvalioLiepaite && ADMIN));
//if(IS_ALLOWED_TO_EDIT) {
if(isset($_GET['saved'])) {
	msgBox('OK', 'Vaiko informacija išsaugota!');
}
if(isset($_POST['save']) || isset($_POST['save-new-version'])) {
	if( empty($_POST['vardas']) || empty($_POST['pavarde']) ) {
		//logdie("<script type=\"text/javascript\">alert('Neužpildyti būtini laukai'); window.history.back(-1);</script>");
		die('<div class="red center">Neužpildyti būtini laukeliai.</div>');
	}

	/*$date = db_fix(substr($_POST['valid_from'], 0, 7)).'-01';
	$currently_locked = db_query("SELECT * FROM `1lock_kids_attendance_food` WHERE `kindergarten_id`=".DB_ID." AND `date`='".$date."'  AND `group_id`=".(int)$_POST['grupes_id']);
	if(!mysqli_num_rows($currently_locked)) {*/
	if(isset($_POST['save-new-version'])) {
		$result = db_query("SELECT * FROM `".DB_children."` WHERE `ID`=".(int)$_POST['ID']);
		if(!mysqli_num_rows($result))
			die(loga('ID:'.$_POST['ID']));
		$vaikas = mysqli_fetch_assoc($result);
	}
	/*
	http://stackoverflow.com/questions/2303415/mysql-two-columns-both-set-to-the-primary-key Simple and clean
	http://hengrui-li.blogspot.com/2009/11/solution-for-cant-update-table-in.html
	http://mark.koli.ch/mysql-trigger-error-1442-hy000-cant-update-table-tbl-in-stored-functiontrigger-because-it-is-al
	http://stackoverflow.com/questions/19041005/a-trigger-to-set-an-id-for-a-parent-row

	http://stackoverflow.com/questions/15300673/mysql-error-cant-update-table-in-stored-function-trigger-because-it-is-already

	http://mikehillyer.com/articles/managing-hierarchical-data-in-mysql/
	*/
	$sql = "INSERT INTO `".DB_children."` SET
		".(isset($_POST['save-new-version']) ? '`parent_kid_id`='.(int)$vaikas['parent_kid_id'].',' : '')."
		`valid_from`='".db_fix($_POST['valid_from'])."',
		`version_comment`='".db_fix($_POST['version_comment'])."',
		`vardas`='".db_fix($_POST['vardas'])."',
		`pavarde`='".db_fix($_POST['pavarde'])."',
		`mamos_vardas`='".db_fix($_POST['mamos_vardas'])."', `mamos_pavarde`='".db_fix($_POST['mamos_pavarde'])."', 
		`tevo_vardas`='".db_fix($_POST['tevo_vardas'])."', `tevo_pavarde`='".db_fix($_POST['tevo_pavarde'])."',
		`grupes_id`=".(int)$_POST['grupes_id']/*TODO: check allowance*/.", `gim_data`='".db_fix($_POST['gim_data'])."',
		`tevu_adresas`='".db_fix($_POST['tevu_adresas'])."',
		`mam_tel`='".db_fix($_POST['mam_tel'] == '370' ? '' : $_POST['mam_tel'])."',
		`tecio_tel`='".db_fix($_POST['tecio_tel'] == '370' ? '' : $_POST['tecio_tel'])."', 
		`glob_tel`='".db_fix($_POST['glob_tel'] == '370' ? '' : $_POST['glob_tel'])."',
		`namu_tel`='".db_fix($_POST['namu_tel'] == '370' ? '' : $_POST['namu_tel'])."',
		`papildomi_tel`='".db_fix($_POST['papildomi_tel'])."',
		`kita_info`='".db_fix($_POST['kita_info'])."',
		".($allow_archive ? "`archyvas`=".(isset($_POST['archyvas']) ? (int)$_POST['archyvas'] : 0)."," : "" )."
		`arDarzelyje`=".(isset($_POST['arDarzelyje']) ? (int)$_POST['arDarzelyje'] : 0).",
		`arPusryciaus`=".(isset($_POST['arPusryciaus']) ? (int)$_POST['arPusryciaus'] : 0).",
		`arPriespieciaus`=".(isset($_POST['arPriespieciaus']) ? (int)$_POST['arPriespieciaus'] : 0).",
		`arPietaus`=".(isset($_POST['arPietaus']) ? (int)$_POST['arPietaus'] : 0).",
		`arPavakariaus`=".(isset($_POST['arPavakariaus']) ? (int)$_POST['arPavakariaus'] : 0).",
		`arVakarieniaus`=".(isset($_POST['arVakarieniaus']) ? (int)$_POST['arVakarieniaus'] : 0).",
		`arNaktipieciaus`=".(isset($_POST['arNaktipieciaus']) ? (int)$_POST['arNaktipieciaus'] : 0).",
		`free_breakfast`=".(isset($_POST['free_breakfast']) ? (int)$_POST['free_breakfast'] : 0).",
		`free_priespieciai`=".(isset($_POST['free_priespieciai']) ? (int)$_POST['free_priespieciai'] : 0).",
		`free_lunch`=".(isset($_POST['free_lunch']) ? (int)$_POST['free_lunch'] : 0).",
		`free_afternoon_tea`=".(isset($_POST['free_afternoon_tea']) ? (int)$_POST['free_afternoon_tea'] : 0).",
		`free_dinner`=".(isset($_POST['free_dinner']) ? (int)$_POST['free_dinner'] : 0).",
		`free_naktipieciai`=".(isset($_POST['free_naktipieciai']) ? (int)$_POST['free_naktipieciai'] : 0).",
		`arNuolaida`=".(isset($_POST['arNuolaida']) ? (int)$_POST['arNuolaida'] : 0).",
		`dienosMokestis`=".(isset($_POST['dienosMokestis']) ? (int)$_POST['dienosMokestis'] : 0).",
		`monthly_discount_sum`=".(isset($_POST['monthly_discount_sum']) ? (float)str_replace(',', '.', $_POST['monthly_discount_sum']) : 0).",
		`percentage_discount_from_day_tax_per_month`=".(isset($_POST['percentage_discount_from_day_tax_per_month']) ? (float)str_replace(',', '.', $_POST['percentage_discount_from_day_tax_per_month']) : 0).",
		`feeForPlace`='".(!empty($_POST['feeForPlace']) ? (float)str_replace(',', '.', $_POST['feeForPlace']) : 0)."',
		`arRodytiElPasta`=".(isset($_POST['arRodytiElPasta']) ? (int)$_POST['arRodytiElPasta'] : 0).",
		`lankymo_pradzia`='".db_fix($_POST['lankymo_pradzia'])."',
		`lankymo_pabaiga`='".db_fix($_POST['lankymo_pabaiga'])."',
		`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."',
		`turetume_zinoti`='".db_fix($_POST['turetume_zinoti'])."',
		`asm_sas`='".db_fix($_POST['asm_sas'])."',
		".($bylos_nr_pagal_registracijos_knyga_zurnala ? "`bylos_nr_zurnale`='".db_fix($_POST['bylos_nr_zurnale'])."'," : "" )."
		`allergic`=".(isset($_POST['allergic']) ? (int)$_POST['allergic'] : 0).",
		`attend_sport`=".(isset($_POST['attend_sport']) ? (int)$_POST['attend_sport'] : 0);
	if(isset($_POST['save'])) {
		mysqli_autocommit($db_link, false);
		db_query($sql);
		$child_id = (int)mysqli_insert_id($db_link);
		db_query("UPDATE `".DB_children."` SET `parent_kid_id`=`ID` WHERE `parent_kid_id`=0");
		$result = mysqli_commit($db_link);
		mysqli_autocommit($db_link, true);
	} else {//save-new-version
		$result = db_query($sql);
		$child_id = (int)mysqli_insert_id($db_link);
		?>
			<script>window.location.replace("?edit=<?php echo $child_id; ?>&saved");</script>
			<noscript><meta http-equiv="refresh" content="0;URL='?edit=<?php echo $child_id; ?>&saved'"></noscript>
		<?php
		//By default does't work in firefox (because of security config)
		//Reason: http://stackoverflow.com/questions/503093/how-can-i-make-a-redirect-page-in-jquery-javascript
	}
	if (!$result) {
		logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
	} else {
		msgBox('OK', 'Vaiko informacija išsaugota!');
		if(!empty($_POST['mamos_pastas'])) {
			$_POST['mamos_pastas'] = filter_var($_POST['mamos_pastas'], FILTER_VALIDATE_EMAIL);
			if($_POST['mamos_pastas']) {
				$result = db_query("SELECT * FROM `".DB_users."` WHERE `email`='".db_fix($_POST['mamos_pastas'])."'");
				$row = mysqli_fetch_assoc($result);
				if(mysqli_num_rows($result) == 1) {
					db_query("INSERT INTO `".DB_users_allowed."` SET `user_id`='".(int)$row['user_id']."', `person_type`=0, `person_subtype`=1, `person_id`='".$child_id."'");
					msgBox('OK', 'Sistemoje jau egzistuojančiai mamai/tėčiui pridėtas dar vienas vaikas.');
				} else {
					if(register($_POST['mamos_vardas'], $_POST['mamos_pavarde'], $_POST['mamos_pastas'], false, $send_registration_email)) {
						db_query("INSERT INTO `".DB_users_allowed."` SET `user_id`='".(int)mysqli_insert_id($db_link)."', `person_type`=0, `person_subtype`=1, `person_id`='".$child_id."'");
						// msgBox('OK', 'Sėkmingai išsaugota!');
					}
				}
			} else msgBox('ERROR', 'Netinkamas mamos el. paštas.');
		}


		if(!empty($_POST['tevo_pastas'])) {
			$_POST['tevo_pastas'] = filter_var($_POST['tevo_pastas'], FILTER_VALIDATE_EMAIL);
			if($_POST['tevo_pastas']) {
				$result = db_query("SELECT * FROM `".DB_users."` WHERE `email`='".db_fix($_POST['tevo_pastas'])."'");
				$row = mysqli_fetch_assoc($result);
				if(mysqli_num_rows($result) == 1) {
					db_query("INSERT INTO `".DB_users_allowed."` SET `user_id`='".(int)$row['user_id']."', `person_type`=0, `person_subtype`=2, `person_id`='".$child_id."'");
					msgBox('OK', 'Sistemoje jau egzistuojančiai mamai/tėčiui pridėtas dar vienas vaikas.');
				} else {
					if(register($_POST['tevo_vardas'], $_POST['tevo_pavarde'], $_POST['tevo_pastas'], false, $send_registration_email)) {
						db_query("INSERT INTO `".DB_users_allowed."` SET `user_id`='".(int)mysqli_insert_id($db_link)."', `person_type`=0, `person_subtype`=2, `person_id`='".$child_id."'");
						// msgBox('OK', 'Sėkmingai išsaugota!');
					}
				}
			} else msgBox('ERROR', 'Netinkamas tėčio el. paštas.');
		}
	}
	/*} else {
		msgBox('ERROR', "Užrakintas šio mėnesio lankomumas. Kreipkitės į vadovą dėl atrakinimo.");
	}*/
}

if(isset($_POST['update'])) {
	if( empty($_POST['vardas']) || empty($_POST['pavarde']) ) {
		//logdie("<script type=\"text/javascript\">alert('Neužpildyti būtini laukai'); window.history.back(-1);</script>");
		die('<div class="red center">Neužpildyti būtini laukeliai.</div>');
	}

	$child_id = (int)$_POST['ID'];
	$old_child_row = mysqli_fetch_assoc(db_query("SELECT * FROM `".DB_children."` WHERE `ID`=".(int)$child_id));
	$parent_kid_id = (int)$old_child_row['parent_kid_id'];

	/*$result = db_query("SELECT * FROM `".DB_children."` WHERE `parent_kid_id`=".$parent_kid_id." AND `isDeleted`=0 ORDER BY `valid_from` DESC");// `valid_from`<='".CURRENT_DATE."' AND
	while ($row = mysqli_fetch_assoc($result))
		$kid[/*$row['valid_from']/] = $row;
	function getKidRow($date = CURRENT_DATE) {
		foreach($kid as /*$cdate =>/ $value) {
			if(/*$cdate/$value['valid_from'] <= $date) {
				return $value;
			}
		}
		return false;
	}

	//Užrakintus iki dienos, leisti tik pagal išimtį, bet yra niuansų, kadangi reikia sukurti toliau einantį tokį patį.

	//getKidRow($old_child_row['valid_from']);
	$next_day = date('Y-m-d', strtotime($old_child_row['valid_from'] . ' +1 day'));

	$date = db_fix(substr($old_child_row['valid_from'], 0, 7)).'-01';
	$currently_locked = db_query("SELECT * FROM `1lock_kids_attendance_food` WHERE `kindergarten_id`=".DB_ID." AND `date`='".$date."'  AND `group_id`=".(int)$_POST['grupes_id']);
	$currently_locked = mysqli_num_rows($currently_locked) ? true : false;
	if(!$currently_locked) {
		$date = db_fix(substr($_POST['valid_from'], 0, 7)).'-01';
		//Ar leidžiama pakeisti.
		$currently_locked = db_query("SELECT * FROM `1lock_kids_attendance_food` WHERE `kindergarten_id`=".DB_ID." AND `date`='".$date."'  AND `group_id`=".(int)$_POST['grupes_id']);
		$currently_locked = mysqli_num_rows($currently_locked) ? true : false;
	}*/






	$sql = "UPDATE `".DB_children."` SET";
	//if()
	$sql .= " `valid_from`='".db_fix($_POST['valid_from'])."',
			  `grupes_id`=".(int) $_POST['grupes_id']/*TODO: check allowance*/.",";
	$sql .= " `version_comment`='".db_fix($_POST['version_comment'])."',
		`vardas`='".db_fix($_POST['vardas'])."',
		`pavarde`='".db_fix($_POST['pavarde'])."',
		`mamos_vardas`='".db_fix($_POST['mamos_vardas'])."', `mamos_pavarde`='".db_fix($_POST['mamos_pavarde'])."', 
		`tevo_vardas`='".db_fix($_POST['tevo_vardas'])."', `tevo_pavarde`='".db_fix($_POST['tevo_pavarde'])."',
		`gim_data`='".db_fix($_POST['gim_data'])."',
		`tevu_adresas`='".db_fix($_POST['tevu_adresas'])."',
		`mam_tel`='".db_fix($_POST['mam_tel'] == '370' ? '' : $_POST['mam_tel'])."',
		`tecio_tel`='".db_fix($_POST['tecio_tel'] == '370' ? '' : $_POST['tecio_tel'])."', 
		`glob_tel`='".db_fix($_POST['glob_tel'] == '370' ? '' : $_POST['glob_tel'])."',
		`namu_tel`='".db_fix($_POST['namu_tel'] == '370' ? '' : $_POST['namu_tel'])."',
		`papildomi_tel`='".db_fix($_POST['papildomi_tel'])."',
		`kita_info`='".db_fix($_POST['kita_info'])."',";
	
	$sql .= ($allow_archive ? "`archyvas`=".(isset($_POST['archyvas']) ? (int)$_POST['archyvas'] : 0)."," : "" )."
		`arDarzelyje`=".(isset($_POST['arDarzelyje']) ? (int)$_POST['arDarzelyje'] : 0).",
		`arPusryciaus`=".(isset($_POST['arPusryciaus']) ? (int)$_POST['arPusryciaus'] : 0).",
		`arPriespieciaus`=".(isset($_POST['arPriespieciaus']) ? (int)$_POST['arPriespieciaus'] : 0).",
		`arPietaus`=".(isset($_POST['arPietaus']) ? (int)$_POST['arPietaus'] : 0).",
		`arPavakariaus`=".(isset($_POST['arPavakariaus']) ? (int)$_POST['arPavakariaus'] : 0).",
		`arVakarieniaus`=".(isset($_POST['arVakarieniaus']) ? (int)$_POST['arVakarieniaus'] : 0).",
		`arNaktipieciaus`=".(isset($_POST['arNaktipieciaus']) ? (int)$_POST['arNaktipieciaus'] : 0).",
		`free_breakfast`=".(isset($_POST['free_breakfast']) ? (int)$_POST['free_breakfast'] : 0).",
		`free_priespieciai`=".(isset($_POST['free_priespieciai']) ? (int)$_POST['free_priespieciai'] : 0).",
		`free_lunch`=".(isset($_POST['free_lunch']) ? (int)$_POST['free_lunch'] : 0).",
		`free_afternoon_tea`=".(isset($_POST['free_afternoon_tea']) ? (int)$_POST['free_afternoon_tea'] : 0).",
		`free_dinner`=".(isset($_POST['free_dinner']) ? (int)$_POST['free_dinner'] : 0).",
		`free_naktipieciai`=".(isset($_POST['free_naktipieciai']) ? (int)$_POST['free_naktipieciai'] : 0).",
		`arNuolaida`=".(isset($_POST['arNuolaida']) ? (int)$_POST['arNuolaida'] : 0).",
		`dienosMokestis`=".(isset($_POST['dienosMokestis']) ? (int)$_POST['dienosMokestis'] : 0).",
		`monthly_discount_sum`=".(isset($_POST['monthly_discount_sum']) ? (float)str_replace(',', '.', $_POST['monthly_discount_sum']) : 0).",
		`percentage_discount_from_day_tax_per_month`=".(isset($_POST['percentage_discount_from_day_tax_per_month']) ? (float)str_replace(',', '.', $_POST['percentage_discount_from_day_tax_per_month']) : 0).",
		`feeForPlace`='".(!empty($_POST['feeForPlace']) ? (float)str_replace(',', '.', $_POST['feeForPlace']) : 0)."',
		`allergic`=".(isset($_POST['allergic']) ? (int)$_POST['allergic'] : 0).",
		`attend_sport`=".(isset($_POST['attend_sport']) ? (int)$_POST['attend_sport'] : 0).",
		`asm_sas`='".db_fix($_POST['asm_sas'])."',";
	
	$sql .= "`arRodytiElPasta`=".(isset($_POST['arRodytiElPasta']) ? (int)$_POST['arRodytiElPasta'] : 0).",
		`lankymo_pradzia`='".db_fix($_POST['lankymo_pradzia'])."',
		`lankymo_pabaiga`='".db_fix($_POST['lankymo_pabaiga'])."',
		`turetume_zinoti`='".db_fix($_POST['turetume_zinoti'])."',
		".($bylos_nr_pagal_registracijos_knyga_zurnala ? "`bylos_nr_zurnale`='".db_fix($_POST['bylos_nr_zurnale'])."'," : "" )."
		`updated`=CURRENT_TIMESTAMP, `updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."', `updatedCounter`=`updatedCounter`+1
		WHERE `ID`=".(int)$child_id;
	db_query($sql);
	msgBox('OK', 'Vaiko informacija sėkmingai atnaujinta!');
	if(!empty($_POST['mamos_pastas'])) {
		$_POST['mamos_pastas'] = filter_var($_POST['mamos_pastas'], FILTER_VALIDATE_EMAIL);
		if($_POST['mamos_pastas']) {
			$result = db_query("SELECT * FROM `".DB_users."` WHERE `email`='".db_fix($_POST['mamos_pastas'])."'");
			$row = mysqli_fetch_assoc($result);
			if(mysqli_num_rows($result) == 1) {
				db_query("INSERT INTO `".DB_users_allowed."` SET `user_id`='".(int)$row['user_id']."', `person_type`=0, `person_subtype`=1, `person_id`='".$parent_kid_id."'");
				msgBox('OK', 'Sistemoje jau egzistuojančiai mamai/tėčiui pridėtas dar vienas vaikas.');
			} else {
				if(register($_POST['mamos_vardas'], $_POST['mamos_pavarde'], $_POST['mamos_pastas'], false, $send_registration_email)) {
					db_query("INSERT INTO `".DB_users_allowed."` SET `user_id`='".(int)mysqli_insert_id($db_link)."', `person_type`=0, `person_subtype`=1, `person_id`='".$parent_kid_id."'");
					// msgBox('OK', 'Sėkmingai išsaugota!');
				}
			}
		} else msgBox('ERROR', 'Netinkamas mamos el. paštas.');
	}

	if(!empty($_POST['tevo_pastas'])) {
		$_POST['tevo_pastas'] = filter_var($_POST['tevo_pastas'], FILTER_VALIDATE_EMAIL);
		if($_POST['tevo_pastas']) {
			$result = db_query("SELECT * FROM `".DB_users."` WHERE `email`='".db_fix($_POST['tevo_pastas'])."'");
			$row = mysqli_fetch_assoc($result);
			if(mysqli_num_rows($result) == 1) {
				db_query("INSERT INTO `".DB_users_allowed."` SET `user_id`='".(int)$row['user_id']."', `person_type`=0, `person_subtype`=2, `person_id`='".$parent_kid_id."'");
				msgBox('OK', 'Sistemoje jau egzistuojančiai mamai/tėčiui pridėtas dar vienas vaikas.');
			} else {
				if(register($_POST['tevo_vardas'], $_POST['tevo_pavarde'], $_POST['tevo_pastas'], false, $send_registration_email)) {
					db_query("INSERT INTO `".DB_users_allowed."` SET `user_id`='".(int)mysqli_insert_id($db_link)."', `person_type`=0, `person_subtype`=2, `person_id`='".$parent_kid_id."'");
					// msgBox('OK', 'Sėkmingai išsaugota!');
				}
			}
		} else msgBox('ERROR', 'Netinkamas tėčio el. paštas.');
	}

	if(!empty($_POST['mamos_pastas_disabled'])) {
		$_POST['mamos_pastas_disabled'] = filter_var($_POST['mamos_pastas_disabled'], FILTER_VALIDATE_EMAIL);
		if($_POST['mamos_pastas_disabled']) {
			$result = db_query("SELECT * FROM `".DB_users_allowed."` JOIN `".DB_users."` ON `".DB_users_allowed."`.`user_id`=`".DB_users."`.`user_id` WHERE `person_type`=0 AND `person_subtype`=1 AND `person_id`='".(int)$parent_kid_id."'");//All current kid accesses for mother
			if(mysqli_num_rows($result) > 0) {
				$old_row = mysqli_fetch_assoc($result);//Old data//Assumption: the same kid have only one mother record
				if($old_row['email'] != $_POST['mamos_pastas_disabled']) {
					$old_result_total = db_query("SELECT * FROM `".DB_users_allowed."` JOIN `".DB_users."` ON `".DB_users_allowed."`.`user_id`=`".DB_users."`.`user_id` WHERE `email`='".db_fix($old_row['email'])."'");
					$old_accesses_total = mysqli_num_rows($old_result_total);
			
					$new_result = db_query("SELECT * FROM `".DB_users."` WHERE `user_id`<>".$old_row['user_id']." AND `email`='".db_fix($_POST['mamos_pastas_disabled'])."'");//New email//Assumption: Exists such email
					$new_email_users = mysqli_num_rows($new_result);
					if($new_email_users == 1 && $old_accesses_total == 1) {
						db_query("DELETE FROM `".DB_users."` WHERE `user_id`=".(int)$old_row['user_id']);
						db_query("DELETE FROM `".DB_users_allowed."` WHERE `user_id`=".(int)$old_row['user_id']." AND `person_id`='".$parent_kid_id."'");
						$new_row = mysqli_fetch_assoc($new_result);
						db_query("INSERT INTO `".DB_users_allowed."` SET `user_id`='".(int)$new_row['user_id']."', `person_type`=0, `person_subtype`=1, `person_id`='".$parent_kid_id."'");
						msgBox('OK', 'Sistemoje jau egzistuojančiai mamai/tėčiui pridėtas dar vienas vaikas.');
					} else/*if($new_email_users != 1 && $old_accesses_total != 1)*/ {
						//Do update name and surname also?
						$update = empty($old_row['name']) && empty($old_row['surname']) ? ", `name`='".db_fix($_POST['mamos_vardas'])."', `surname`='".db_fix($_POST['mamos_pavarde'])."'" : '';
						db_query("UPDATE `".DB_users."` SET `email`='".db_fix($_POST['mamos_pastas_disabled'])."'$update WHERE `user_id`=".(int)$old_row['user_id']);
						send_login_data(DB_PREFIX, $old_row['name'], $_POST['mamos_pastas_disabled'], $send_registration_email);//Update a password and send an email of updated password
						if($new_email_users > 1) {
							loga("\$new_email_users=$new_email_users > 1;".$old_row['email'].'->'.$_POST['mamos_pastas_disabled'], 'warn', 1/*, 'context_details'*/);
						}
						if($old_accesses_total != 1) {
							loga("\$old_accesses_total=$old_accesses_total > 1;".$old_row['email'].'->'.$_POST['mamos_pastas_disabled'], 'warn', 2/*, 'context_details'*/);
						}
					}
				}
			}
		}
	}
	if(!empty($_POST['tevo_pastas_disabled'])) {
		$_POST['tevo_pastas_disabled'] = filter_var($_POST['tevo_pastas_disabled'], FILTER_VALIDATE_EMAIL);
		if($_POST['tevo_pastas_disabled']) {
			$result = db_query("SELECT * FROM `".DB_users_allowed."` JOIN `".DB_users."` ON `".DB_users_allowed."`.`user_id`=`".DB_users."`.`user_id` WHERE `person_type`=0 AND `person_subtype`=2 AND `person_id`='".(int)$parent_kid_id."'");//All current kid accesses for father
			if(mysqli_num_rows($result) > 0) {
				/*$father = mysqli_fetch_assoc($result);
				if($father['email'] != $_POST['tevo_pastas_disabled']) {
					//Do update name and surname also?
					db_query("UPDATE `".DB_users."` SET `email`='".db_fix($_POST['tevo_pastas_disabled'])."' WHERE `user_id`=".(int)$father['user_id']);
					send_login_data(DB_PREFIX, $father['name'], $_POST['tevo_pastas_disabled'], $send_registration_email);//Updates password and sends new one
				}*///5->26=>5x code size increase
				$old_row = mysqli_fetch_assoc($result);//Old data//Assumption: the same kid have only one father record
				if($old_row['email'] != $_POST['tevo_pastas_disabled']) {
					$old_result_total = db_query("SELECT * FROM `".DB_users_allowed."` JOIN `".DB_users."` ON `".DB_users_allowed."`.`user_id`=`".DB_users."`.`user_id` WHERE `email`='".db_fix($old_row['email'])."'");
					$old_accesses_total = mysqli_num_rows($old_result_total);
			
					$new_result = db_query("SELECT * FROM `".DB_users."` WHERE `user_id`<>".$old_row['user_id']." AND `email`='".db_fix($_POST['tevo_pastas_disabled'])."'");//New email//Assumption: Exists such email
					$new_email_users = mysqli_num_rows($new_result);
					if($new_email_users == 1 && $old_accesses_total == 1) {
						db_query("DELETE FROM `".DB_users."` WHERE `user_id`=".(int)$old_row['user_id']);
						db_query("DELETE FROM `".DB_users_allowed."` WHERE `person_id`='".$parent_kid_id."' AND `user_id`=".(int)$old_row['user_id']);
						$new_row = mysqli_fetch_assoc($result);
						db_query("INSERT INTO `".DB_users_allowed."` SET `user_id`='".(int)$new_row['user_id']."', `person_type`=0, `person_subtype`=2, `person_id`='".$parent_kid_id."'");
						msgBox('OK', 'Sistemoje jau egzistuojančiai mamai/tėčiui pridėtas dar vienas vaikas.');
					} else/*if($new_email_users != 1 && $old_accesses_total != 1)*/ {
						//Do update name and surname also?
						$update = empty($old_row['name']) && empty($old_row['surname']) ? ", `name`='".db_fix($_POST['tevo_vardas'])."', `surname`='".db_fix($_POST['tevo_pavarde'])."'" : '';
						db_query("UPDATE `".DB_users."` SET `email`='".db_fix($_POST['tevo_pastas_disabled'])."'$update WHERE `user_id`=".(int)$old_row['user_id']);
						send_login_data(DB_PREFIX, $old_row['name'], $_POST['tevo_pastas_disabled'], $send_registration_email);//Update a password and send an email of updated password
						if($new_email_users > 1) {
							loga("\$new_email_users=$new_email_users > 1;".$old_row['email'].'->'.$_POST['tevo_pastas_disabled'], 'warn', 1/*, 'context_details'*/);
						}
						if($old_accesses_total != 1) {
							loga("\$old_accesses_total=$old_accesses_total != 1;".$old_row['email'].'->'.$_POST['tevo_pastas_disabled'], 'warn', 2/*, 'context_details'*/);
						}
					}
				}
			}
		}
	}
}

if(isset($_GET['archive'])) {
	$result = db_query("SELECT * FROM `".DB_children."` WHERE `ID`=".(int)$_GET['edit']);
	$vaikas = mysqli_fetch_assoc($result);
	unset($vaikas['ID'], $vaikas['created'], $vaikas['updated'], $vaikas['updatedByUserId'], $vaikas['updatedByEmployeeId']);
	$vaikas['createdByUserId'] = USER_ID;
	$vaikas['createdByEmployeeId'] = DARB_ID;
	$vaikas['valid_from'] = $_GET['archive_from'];
	$vaikas['version_comment'] = 'Vaikas išregistruotas';//išbrauktas -> išėjo
	$vaikas['archyvas'] = 1;
	$ins = array();
	foreach($vaikas as $key => $val)
		$ins[] = "`$key`='".db_fix($val)."'";
	db_query("INSERT INTO `".DB_children."` SET ".implode(', ', $ins));
	//http://stackoverflow.com/questions/1632680/duplicate-a-record-in-mysql
	//Similar in attendance
	echo '<script>window.location.replace("?archived");</script>
		<noscript><meta http-equiv="refresh" content="0;URL=\'?archived\'"></noscript>';
}
if(isset($_GET['archived'])) {
	msgBox('OK', 'Vaikas išbrauktas iš sąrašo ir jo informacija archyvuota!');
}

if(isset($_GET['delete']) && KESV/*ADMIN*/) {
	if (!mysqli_query($db_link, "DELETE FROM `".DB_children."` WHERE `ID`=".(int)$_GET['delete'])) {
		logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
	} else msgBox('OK', 'Vaiko informacija ištrinta!');
}
if(isset($_GET['virtualDelete'])) {
	//Trinti vaiką neleisti kai yra pažymėtas lankomumas.
	$result = db_query("SELECT `vaiko_id` FROM `".DB_attendance."` WHERE `vaiko_id`=".(int)$_GET['virtualDelete'].' LIMIT 1');
	if(KESV || !mysqli_num_rows($result)) {
		if (db_query("UPDATE `".DB_children."` SET `isDeleted`=1, `deleted`=CURRENT_TIMESTAMP WHERE `ID`=".(int)$_GET['virtualDelete']))
			msgBox('OK', 'Vaiko informacija ištrinta!');
	} else {
		msgBox('ERROR', 'Vaikas nėra ištrintas, nes yra pažymėtas jo lankomumas.');// Gal norėjote vaiką išregistruoti? Pastaba: Vaikus perkelia tarp grupių vadovai.
	}
}
if(isset($_GET['undo'])) {
	if (db_query("UPDATE `".DB_children."` SET `isDeleted`=0, `deleted`=CURRENT_TIMESTAMP WHERE `ID`=".(int)$_GET['undo']))
		msgBox('OK', 'Vaiko informacija atstatyta.');
}


/*function title($string) {
	echo "onclick=\"if(value=='".filterText($string)."'){value=''}\" onblur=\"if(value==''){value='".filterText($string)."'}\"";
}*/

if(ADMIN) {
	$result = db_query("SELECT COUNT(*) `cnt` FROM `".DB_children."`");
	$row = mysqli_fetch_assoc($result);
	if($row['cnt'] == 0)
		echo '<p class="notice">Vaikus dažniausiai įveda kiekvienas pedagogas savo grupei. Ši pastraipa rodoma tik vadovams ir pradedant darbą su šia sistema.</p>';
}

if( isset($_GET['new']) || isset($_GET['edit']) )
	echo '<a href="?" class="no-print fast-action">Grįžti į vaikų sąrašą</a><br>';
if(isset($_GET['edit']) && ADMIN) {
	$result = db_query("SELECT * FROM `".DB_children."` WHERE `ID`=".(int)$_GET['edit']);
	$vaikas = mysqli_fetch_assoc($result);
	echo '<a href="?ieskoti&grupes_id='.$vaikas['grupes_id'].'" class="no-print fast-action">Grįžti į vaikų grupės '.(getAllGroups()[$vaikas['grupes_id']]).' sąrašą</a><br>';
}

if( !isset($_GET['new']) ) {
	if(!(BUHALT || LOGOPEDAS))
		echo '<a href="?new" class="no-print fast-action fast-action-add">Naujo vaiko duomenų įvedimas</a> ';//#child-form
	echo (isset($_GET['trash']) ? '<a href="?" class="no-print fast-action fast-action-delete a-opened">✖ Uždaryti šiukšlinę</a>' : '<a href="?trash" class="no-print fast-action fast-action-delete">Šiukšlinė</a>');
	ui_print();
}


?>
<div style="margin-top: 10px;" class="no-print">
	<a href="" onclick="$('#whats-new').toggle(); $(this).toggleClass('a-opened'); return false;">Supažindinantis video su vaikų duomenų įsigaliojimu<!-- Susipažinti su vaikų duomenų galiojimo pradžia --></a>
	<div style="display: none;" class="notice" id="whats-new">
		<a href="#" onclick="$('#demo-video').toggle(); return false;">Praktinis supažindinamasis video</a><br>
		Vaiko duomenys turi galiojimo pradžią. Duomenys pradedami naudoti kai vaikas pirmą kartą aplanko darželį ir nuo tos dienos vaikui žymimas lankomumas. Taip pat naudojama, kai turi būti pakeisti vaiko duomenys nuo tam tikros datos. Pavyzdžiui, nuo tam tikros datos turės pasikeisti vaiko: maitinimas (tik nuo tos dienos turės būti skaičiuojamas kitoks mokestis už maitinimą žiniaraščiuose), grupė, grupės rūšis, mokestis (nuolaida) ir kt. Duomenys nustoja galioti kai vaikas išregistruojamas iš darželio ir dėl to jo lankomumas nebegali būti žymimas. Vaikas išregistruojamas 3-ių etapų seka: 1)&nbsp;Spustelėti ant „Keisti“; 2) Spustelėti ant „Reikalingas pakeitimas, pradėsiantis galioti nuo naujos datos“, tada įrašyti datą nuo kada vaikas išregistruojamas, spustelėti ant „Patvirtinti datą“; 3) Įrašyti į laukelį pavadinimu „Duomenų įvedimo priežastis“ pavyzdžiui „vaiko išregistravimas“, pažymėti varnelę prie „Vaikas išregistruotas (neberodomas)“ ir spustelėti ant „Išsaugoti (naujus duomenis)“.
		<!-- Nuo šiol vaiko duomenys turi galiojimo pradžią. Duomenys pradedami naudoti kai vaikas pirmą kartą aplanko darželį ir nuo tos dienos vaikui žymimas lankomumas. Kartais naudojama, kai duomenys turi būti pakeisti nuo tam tikros datos. Pavyzdžiui, kai pateikiamas naujas dokumentas dėl kurio nuo tam tikros datos turės pasikeisti: maitinimas (tik nuo tos dienos turės būti skaičiuojamas kitoks mokestis už maitinimą žiniaraščiuose), grupė, grupės tipas, mokestis (nuolaida) ir/arba kt. Duomenys nustoja galioti kai vaikas išregistruojamas iš darželio ir jo lankomumas neturi būti žymimas. -->
		<!--
		Nuo šiol vaiko duomenys turi galiojimo pradžią. Galiojimo pradžia pirmiausia naudojama kai vaikas pirmą kartą aplanko darželį ir nuo tos dienos vaikas turi būti lankomumo žymėjime, kad jam būtų galima žymėti lankomumą. Laikui bėgant kartais naudojama kai reikalingas pakeitimas kuris įsigalioja (turi pradėti galioti) tik nuo tam tikros datos. Pavyzdžiui, kai pateikiamas naujas dokumentas dėl kurio nuo tam tikros datos turės pasikeisti: maitinimas (tik nuo tos dienos turės būti skaičiuojamas kitoks mokestis už maitinimą žiniaraščiuose), grupė, grupės tipas, mokestis (nuolaida) ir/arba kt. Galiausiai naudojama kai vaikas išregistruojamas iš darželio ir jo lankomumas neturi būti žymimas. -->
	</div>

	<div style="display: none;" class="notice" id="demo-video">
		<br>
		<p style="text-align: center">
			<strong>Video apačioje yra įrankių juostelė. Joje pirmu mygtuku galite susistabdyti video rodymą arba tęsti rodymą toliau. Jį rekomenduojame naudoti, kad galėtumėte ramiai perskaityti tekstą. Antruoju valdikliu galite pakeisti rodomą video vietą.</strong><br>
			<!-- TODO: check with a lot of browsers type="application/x-shockwave-flash" data="/Vaikai3.swf" http://stackoverflow.com/questions/15186337/validation-error-object-using-swf-flash -->
			<OBJECT CLASSID="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" WIDTH="640" HEIGHT="496" CODEBASE="http://active.macromedia.com/flash5/cabs/swflash.cab#version=7,0,0,0">
			<PARAM NAME=movie VALUE="/Vaikai3.swf">
			<PARAM NAME=play VALUE=true>
			<PARAM NAME=loop VALUE=false>
			<PARAM NAME=wmode VALUE=transparent>
			<PARAM NAME=quality VALUE=low>
			<EMBED SRC="/Vaikai3.swf" WIDTH=640 HEIGHT=496 quality=low loop=false wmode=transparent TYPE="application/x-shockwave-flash" PLUGINSPAGE="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash">
			</EMBED>
			</OBJECT>
		</p>
		<script src='/Vaikai3.js'></script>
	</div>
</div>
<?php

if( !(isset($_GET['edit']) || isset($_GET['new'])) ) { ?>
	<div id="table-columns-toggle" class="no-print"><a href="?ieskoti=<?php if(ADMIN) { ?>&grupes_id=<?php echo (isset($_GET['grupes_id']) ? (int)$_GET['grupes_id'] : 0); } ?>&valid_from=<?=date('Y-m-d')?>&archyvas=1&vardas=&pavarde=&mamos_vardas=&mamos_pavarde=&tevo_vardas=&tevo_pavarde=&gim_data=&tevu_adresas="<?=(isset($_GET['archyvas']) && $_GET['archyvas'] ? ' class="a-opened"' : '')?>>Archyvas</a> | <a href="#" id="search_btn" onclick="$(this).toggleClass('a-opened');"<?=(isset($_GET['ieskoti']) ? ' class="a-opened"' : '')?>>Paieška (filtravimas)</a> | <a href="#" id="targetcol_btn" onclick="$(this).toggleClass('a-opened');">Įjungimas/atjungimas stulpelių rodymo<?php /* Nurodyti/Rinktis (pasirinkimas) kokius stulpelius rodyti*/ ?></a></div>

	<?php
	$parents_logins = [];
	$result = db_query("SELECT * FROM `".DB_users_allowed."` JOIN `".DB_users."` ON `".DB_users_allowed."`.`user_id`=`".DB_users."`.`user_id` AND `".DB_users_allowed."`.`person_type`=0");
	while ($row = mysqli_fetch_assoc($result))
		$parents_logins[$row['person_id']][$row['person_subtype']] = $row;
	//person_subtype - 1 - mama, 2 - tėtis

	if(!empty($_GET['valid_from']))
		$_GET['valid_from'] = db_fix($_GET['valid_from']);
	else
		$_GET['valid_from'] = date('Y-m-d');
	?>
	<form method="get" style="padding-bottom:10px; <?php if(!isset($_GET['ieskoti'])) echo ' display: none'; ?>" onsubmit="return validateSearch(this)" id="search_form" class="no-print">
		<input type="hidden" name="ieskoti">
		<?php if(ADMIN) { ?>
		<div class="sel" style="float: left; margin-right: 5px;">
		<select name="grupes_id">
			<option value="0">Visos grupės</option>
			<?php
			foreach(getAllowedGroups() as $ID => $title)
				echo '<option value="'.$ID.'"'.(isset($_GET['ieskoti']) && isset($_GET['grupes_id']) && $ID == $_GET['grupes_id'] ? ' selected="selected"' : '').">".$title."</option>";
			?>
		</select>
		</div>
		<?php } ?>
		<span class="abbr" title="nurodytą dieną galiojusi vaiko informacija">Duomenų galiojimo data</span> <input class="datepicker" type="text" name="valid_from" value="<?=(!empty($_GET['valid_from']) ? filterText($_GET['valid_from']) : '')?>" placeholder="Galioja nuo" title="Vaiko informacija galiojanti nuo">
		<label><span class="abbr" title="Vaiko oficialiai nėra darželyje (sistemoje jis archyve) laikotarpiu nurodytu laukelyje „Duomenų galiojimo data“. Įprastai rodomi vaikai oficialiai esantys darželyje.">archyvas (išregistruotųjų)<!-- Tik išregistruoti vaikai --></span> <input type="checkbox" name="archyvas" value="1" <?=(!empty($_GET['archyvas']) && $_GET['archyvas'] ? 'checked="checked"' : '')?>></label>
		<input type="text" name="vardas" value="<?=(!empty($_GET['vardas']) ? filterText($_GET['vardas']) : '')?>" placeholder="Vardas" style="width: 70px;">
		<input type="text" name="pavarde" value="<?=(!empty($_GET['pavarde']) ? filterText($_GET['pavarde']) : '')?>" placeholder="Pavardė" style="width: 98px;">
		<input type="text" name="mamos_vardas" value="<?=(!empty($_GET['mamos_vardas']) ? filterText($_GET['mamos_vardas']) : '')?>" placeholder="Mamos vardas" style="width: 100px;">
		<input type="text" name="mamos_pavarde" value="<?=(!empty($_GET['mamos_pavarde']) ? filterText($_GET['mamos_pavarde']) : '')?>" placeholder="Mamos pavardė" style="width: 110px;">
		<input type="text" name="tevo_vardas" value="<?=(!empty($_GET['tevo_vardas']) ? filterText($_GET['tevo_vardas']) : '')?>" placeholder="Tėvo vardas" style="width: 90px;">
		<input type="text" name="tevo_pavarde" value="<?=(!empty($_GET['tevo_pavarde']) ? filterText($_GET['tevo_pavarde']) : '')?>" placeholder="Tėvo pavardė" style="width: 100px;">
		<input title="Gimimo data" class="datepicker" type="text" name="gim_data" value="<?=(!empty($_GET['gim_data']) ? filterText($_GET['gim_data']) : '')?>" placeholder="2000-12-31">
		<input type="text" name="tevu_adresas" value="<?=(!empty($_GET['tevu_adresas']) ? filterText($_GET['tevu_adresas']) : '')?>" placeholder="Namų adresas" style="width: 150px;">
		<?php if(false/*TESTINIS*/) { ?>
		<input type="text" name="mam_tel" value="<?=(!empty($_GET['mam_tel']) && $_GET['mam_tel'] != 0 ? filterText($_GET['mam_tel']) : '')?>" placeholder="Mamos tel." style="width: 100px;"> (37065544333)
		<input type="text" name="tecio_tel" value="<?=(!empty($_GET['tecio_tel']) && $_GET['tecio_tel'] != 0 ? filterText($_GET['tecio_tel']) : '')?>" placeholder="Tėvo tel." style="width: 100px;">
		<input type="text" name="glob_tel" value="<?=(!empty($_GET['glob_tel']) && $_GET['glob_tel'] != 0 ? filterText($_GET['glob_tel']) : '')?>" placeholder="Globėjų tel." style="width: 100px;">
		<input type="text" name="namu_tel" value="<?=(!empty($_GET['namu_tel']) && $_GET['namu_tel'] != 0 ? filterText($_GET['namu_tel']) : '')?>" placeholder="Namų tel." style="width: 100px;">
		<input type="text" name="kita_info" value="<?=(!empty($_GET['kita_info']) ? filterText($_GET['kita_info']) : '')?>" placeholder="Kita info" style="width: 100px;">
		<?php } ?>
		<input type="submit" value="Ieškoti" class="filter">
	</form>

	<?php
	$ieskoti = '';
	if(isset($_GET['ieskoti'])) {
		if(!empty($_GET['vardas'])) $ieskoti .= " AND cr.`vardas` LIKE '%".db_fix($_GET['vardas'])."%'";
		if(!empty($_GET['pavarde'])) $ieskoti .= " AND cr.`pavarde` LIKE '%".db_fix($_GET['pavarde'])."%'";
		if(!empty($_GET['mamos_vardas'])) $ieskoti .= " AND cr.`mamos_vardas` LIKE '%".db_fix($_GET['mamos_vardas'])."%'";
		if(!empty($_GET['mamos_pavarde'])) $ieskoti .= " AND cr.`mamos_pavarde` LIKE '%".db_fix($_GET['mamos_pavarde'])."%'";
		if(!empty($_GET['tevo_vardas'])) $ieskoti .= " AND cr.`tevo_vardas` LIKE '%".db_fix($_GET['tevo_vardas'])."%'";
		if(!empty($_GET['tevo_pavarde'])) $ieskoti .= " AND cr.`tevo_pavarde` LIKE '%".db_fix($_GET['tevo_pavarde'])."%'";
		if(!empty($_GET['grupes_id']) && $_GET['grupes_id'] != 0 && ADMIN) $ieskoti .= " AND cr.`grupes_id`=".(int)$_GET['grupes_id'];
		if(!empty($_GET['gim_data'])) $ieskoti .= " AND cr.`gim_data` LIKE '%".db_fix($_GET['gim_data'])."%'";
		if(!empty($_GET['tevu_adresas'])) $ieskoti .= " AND cr.`tevu_adresas` LIKE '%".db_fix($_GET['tevu_adresas'])."%'";
		if(!empty($_GET['mam_tel'])) $ieskoti .= " AND cr.`mam_tel` LIKE '%".(int)$_GET['mam_tel']."%'";
		if(!empty($_GET['tecio_tel'])) $ieskoti .= " AND cr.`tecio_tel` LIKE '%".(int)$_GET['tecio_tel']."%'";
		if(!empty($_GET['glob_tel'])) $ieskoti .= " AND cr.`glob_tel` LIKE '%".(int)$_GET['glob_tel']."%'";
		if(!empty($_GET['namu_tel'])) $ieskoti .= " AND cr.`namu_tel` LIKE '%".(int)$_GET['namu_tel']."%'";
		if(!empty($_GET['kita_info'])) $ieskoti .= " AND cr.`kita_info` LIKE '%".db_fix($_GET['kita_info'])."%'";
	
		if(isset($_GET['archyvas'])) $ieskoti .= " AND cr.`archyvas`=".(int)$_GET['archyvas'];//Or just 1
		else $ieskoti .= ' AND cr.`archyvas`=0';
	} else {
		$ieskoti = ' AND cr.`archyvas`=0';
	}
//}

	$grupes = getAllGroups();
	$grupes[0] = 'Neįvesta';

	if(ADMIN)
		$sql_group_by_kid_id = "SELECT `parent_kid_id`, COUNT(`parent_kid_id`) AS `cnt` FROM `".DB_children."` GROUP BY `parent_kid_id`";
	else
		$sql_group_by_kid_id = "SELECT `parent_kid_id`, COUNT(`parent_kid_id`) AS `cnt` FROM `".DB_children."` WHERE `grupes_id`=".GROUP_ID." GROUP BY `parent_kid_id`";
	$res = db_query($sql_group_by_kid_id);
	$versions_counter = array();
	while ($row = mysqli_fetch_assoc($res))
		$versions_counter[$row['parent_kid_id']] = ($row['cnt'] == 1 ? '' : $row['cnt']);

	/*
	SELECT `".DB_attendance."`.*, cr.*, COUNT(*) AS `kiek`, SUM(`yra`) AS `buvo`
			FROM `".DB_attendance."` 
			JOIN `".DB_children."` cr ON (cr.`parent_kid_id`=`".DB_attendance."`.vaiko_id AND cr.`valid_from`<=`".DB_attendance."`.`data`)
			LEFT JOIN `".DB_children."` mx ON cr.`parent_kid_id`=mx.`parent_kid_id` AND cr.`valid_from`<mx.`valid_from` AND cr.`valid_from`<=`".DB_attendance."`.`data` AND mx.`valid_from`<=`".DB_attendance."`.`data`
			WHERE cr.`isDeleted`= 0 AND YEAR(`".DB_attendance."`.`data`)=".$year." AND MONTH(`".DB_attendance."`.`data`)=".$month." AND cr.`grupes_id`=".(int)$_GET['grupes_id']." AND mx.`parent_kid_id` IS NULL
			GROUP BY cr.`parent_kid_id` -- `vardas`, `pavarde`
			ORDER BY cr.`vardas`, cr.`pavarde`
	*/
	if(ADMIN)
		//$sql = "SELECT *, TIMESTAMPDIFF(YEAR, `gim_data`, CURDATE()) AS age FROM `".DB_children."` WHERE `parent_kid_id`=0 AND `isDeleted`=0$ieskoti ORDER BY `vardas` ASC, `pavarde` ASC";
		/*OPTIMIZED(? berods) $sql = "SELECT cr.*, TIMESTAMPDIFF(YEAR, cr.`gim_data`, CURDATE()) AS age
		FROM `".DB_children."` cr
		LEFT JOIN `".DB_children."` fi ON (cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`<fi.`valid_from` AND cr.`valid_from`<='".db_fix($_GET['valid_from'])."' AND fi.`valid_from`<='".db_fix($_GET['valid_from'])."')
		WHERE 1=1 AND fi.`valid_from` IS NULL AND cr.`isDeleted`=0 $ieskoti
		ORDER BY ".orderName('cr');*/
		$sql = "SELECT cr.*, TIMESTAMPDIFF(YEAR, cr.`gim_data`, CURDATE()) AS age FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<='".db_fix($_GET['valid_from'])."' GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from` AND cr.`isDeleted`=".(isset($_GET['trash']) ? 1 : 0)."$ieskoti ORDER BY ".orderName('cr');
	else
		//$sql = "SELECT `".DB_children."`.*, TIMESTAMPDIFF(YEAR, `gim_data`, CURDATE()) AS age FROM `".DB_children."` WHERE `parent_kid_id`=0 AND `grupes_id`=".GROUP_ID." AND `isDeleted`=0$ieskoti ORDER BY `vardas` ASC, `pavarde` ASC";//WHERE `archyvas`=0 AND `grupes_id`=".GROUP_ID." ir nebuvo $ieskoti
		$sql = "SELECT cr.*, TIMESTAMPDIFF(YEAR, cr.`gim_data`, CURDATE()) AS age FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<='".db_fix($_GET['valid_from'])."' GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from` AND cr.`isDeleted`=".(isset($_GET['trash']) ? 1 : 0)." AND cr.`grupes_id`=".GROUP_ID."$ieskoti ORDER BY ".orderName('cr');
	/*
		$sql = "SELECT *, TIMESTAMPDIFF(YEAR, `gim_data`, CURDATE()) AS age FROM `".DB_children."` WHERE `isDeleted`=0$ieskoti ORDER BY `vardas` ASC, `pavarde` ASC";
	else
		$sql = "SELECT `".DB_children."`.*, TIMESTAMPDIFF(YEAR, `gim_data`, CURDATE()) AS age
	FROM `".DB_children."` WHERE `grupes_id`=".GROUP_ID." AND `isDeleted`=0$ieskoti
	ORDER BY `vardas` ASC, `pavarde` ASC";//WHERE `archyvas`=0 AND `grupes_id`=".GROUP_ID." ir nebuvo $ieskoti
	*/

	if(ADMIN)
		$sql_all = "SELECT cr.* FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from` AND cr.`isDeleted`=0 ORDER BY ".orderName('cr');
	else
		$sql_all = "SELECT cr.* FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from` AND cr.`isDeleted`=0 AND cr.`grupes_id`=".GROUP_ID." ORDER BY ".orderName('cr');
	$res = db_query($sql_all);
	$all_kids_count = mysqli_num_rows($res);
	/*while ($row = mysqli_fetch_assoc($res)) {
		if($row['valid_from'] > date('Y-m-d')) {
		}
	}*/


	$result = db_query($sql);
	$kids_count = mysqli_num_rows($result);
	if($kids_count > 0) {
		?>
	<div id="targetcol" class="target" style="display: none">Rodyti/nerodyti stulpelius:</div>

	<?php
	if($kids_count != $all_kids_count)
		echo '<p class="no-print">Rodoma vaikų: '.$kids_count.'; Iš viso vaikų: '.$all_kids_count.'; Vadinasi nerodoma: '.($all_kids_count-$kids_count).'. <span class="abbr" title="Vaikai: jau archyvuoti ir nebelanko darželio arba jie ateityje pradės eiti į darželį">Kodėl taip būna?</span> <span class="abbr" title="Punkte „Vaikų paieška (filtravimas)“ galite: a) nustatyti metais vėlesnę datą nuo kurios vaiko informacija galios ir/arba b) pažymėti varnelę ant „archyvas/išregistruotų“.">Kaip matyti daugiau?</span></p>';
	?>
	<table id="vaikai-tbl"<?=(isset($_GET['edit']) ? ' class="no-print"' : '')?>>
	<tr>
		<th title="Eilutės numeris"><span class="abbr">#</span></th>
		<th>Vaikas</th>
		<th>Tėvai (globėjai)</th>
		<th>Grupė</th>
		<th class="date-cell">Gimimo data</th><!--  style="width: 76px;" -->
		<th>Namų adresas</th>
		<th style="width: 150px;">Telefonai: mamos, tėčio, globėjo, namų</th>
		<th class="date-cell">Lankymo pradžia ir pabaiga</th><!-- style="width: 76px;" -->
		<th>Turėtume žinoti</th>
		<th>Kita informacija</th>
		<th title="Duomenų galiojančių nuo skirtingų datų kiekis rodomas jei yra daugiau nei 1" class="no-print"><span class="abbr">K</span></th>
		<th title="Ar archyve?"><span class="abbr">A</span><span style="font-size: 0px;">rchy-<br>vuotas</span></th>
		<th style="width: 80px;" class="no-print">Veiksmai</th>
	</tr>
	<?php
		$ar_istaigoje_yra_priespieciai = getConfig('ar_istaigoje_yra_priespieciai?', $_GET['valid_from']);
		$i = 0;
		while ($row = mysqli_fetch_assoc($result)) {
			$tel = array();
			if(!empty($row['mam_tel']))
				$tel[] = nr($row['mam_tel']).' (mamos)';
			if(!empty($row['tecio_tel']))
				$tel[] = nr($row['tecio_tel']).' (tėčio)';
			if(!empty($row['glob_tel']))
				$tel[] = nr($row['glob_tel']).' (globėjų)';
			if(!empty($row['namu_tel']))
				$tel[] = nr($row['namu_tel']).' (namų)';
			if(!empty($row['papildomi_tel']))
				$tel[] = $row['papildomi_tel'];
			$tel = implode("<br>", $tel);
	
			echo "		<tr".(isset($_GET['edit']) && $_GET['edit'] == $row['ID'] ? ' class="opened-row"' : '').">
			<td>".++$i."</td>
			<td><a href=\"?edit=".$row['ID']."\">".filterText(getName($row['vardas'], $row['pavarde']))."</a>";//#child-form
		
			if(!KaunoVaikystesLobiai) {
				if(KaunoRBaibokyne) {
					$nuo_txt = array(
						0 => '',
						1 => '<span class="infoBox" title="10 % nuolaida">10</span>',//'10 % nuolaida',
						2 => '<span class="infoBox" title="20 % nuolaida">20</span>',//'20 % nuolaida',
						3 => '<span class="infoBox" title="50 % nuolaida">50</span>'//'50 % nuolaida'
					);
				} else {
					$nuo_txt = array(
						0 => '',
						1 => '<span class="infoBox" title="50 % nuolaida">50</span>',// '50 % nuolaida',
						2 => '<span class="infoBox" title="100 % nuolaida">100</span>',//'100 % nuolaida'
						3 => '<span class="infoBox" title="70 % nuolaida">70</span>',//'70 % nuolaida'
						4 => '<span class="infoBox" title="80 % nuolaida">80</span>',//'80 % nuolaida'
						5 => '<span class="infoBox" title="90 % nuolaida">90</span>',//'90 % nuolaida'
						6 => '<span class="infoBox" title="20 % nuolaida">20</span>'//'20 % nuolaida'
					);
				}
				$kid_groups_types_abbr = [
					0 => 'Lopšelyje">L',
					1 => 'Darželyje">D',
					2 => 'Priešmokyklinis">P',
					3 => 'Pradinė klasė">K'
				];
				$style = ' style="background-color: #357d32; color: #fff;"';
							
				echo "<br>
				<span class=\"infoBox\" title=\"".($kid_groups_types_abbr[$row['arDarzelyje']])."</span> ";
				
				echo "<span class=\"infoBox\"".($row['free_breakfast'] ? $style : '')." title=\"".($row['arPusryciaus'] ? ($row['free_breakfast'] ? 'Nemokamai pusryčiaus">Pus' : 'Pusryčiaus">Pus') : 'Nepusryčiaus">___' )."</span> ";
				if($ar_istaigoje_yra_priespieciai)
					echo "<span class=\"infoBox\"".($row['free_priespieciai'] ? $style : '')." title=\"".($row['arPriespieciaus'] ? ($row['free_priespieciai'] ? 'Nemokamai priešpiečiai">Pri' : 'Priešpiečiaus">Pri') : 'Nepriešpiečiaus">___' )."</span> ";
					
				echo "<span class=\"infoBox\"".($row['free_lunch'] ? $style : '')." title=\"".($row['arPietaus'] ? ($row['free_lunch'] ? 'Nemokamai pietaus">Pie' : 'Pietaus">Pie') : 'Nepietaus">___' )."</span>
				<span class=\"infoBox\"".($row['free_afternoon_tea'] ? $style : '')." title=\"".($row['arPavakariaus'] ? ($row['free_lunch'] ? 'Nemokamai gaus pavakarius">Pavak' : 'Gaus pavakarius">Pavak') : 'Neužsakyti pavakariai">_____' )."</span>
				<span class=\"infoBox\"".($row['free_dinner'] ? $style : '')." title=\"".($row['arVakarieniaus'] ? ($row['free_lunch'] ? 'Nemokamai vakarieniaus">Vak' : 'Vakarieniaus">Vak') : 'Nevakarieniaus">___' )."</span> ";
				if(getConfig('ar_istaigoje_yra_naktipieciai?', $_GET['valid_from']))
					echo "<span class=\"infoBox\"".($row['free_naktipieciai'] ? $style : '')." title=\"".($row['arNaktipieciaus'] ? ($row['free_naktipieciai'] ? 'Nemokamai naktipiečiai">Nak' : 'Naktipiečiaus">Nak') : 'Nenaktipiečiaus">___' )."</span> ";
				echo $nuo_txt[$row['arNuolaida']];
			}
		
			echo "</td>
			<td>".(isset($parents_logins[$row['parent_kid_id']][1]) ? '<span class="abbr email'.($parents_logins[$row['parent_kid_id']][1]['login_counter'] ? '-accessed" title="Mama paskutinį kartą buvo prisijungus '.$parents_logins[$row['parent_kid_id']][1]['last_login_time'] : '" title="Jau įvestas mamos el. pašto adresas').'">@'.filterText(getName($row['mamos_vardas'], $row['mamos_pavarde'])).'</span>' : filterText(getName($row['mamos_vardas'], $row['mamos_pavarde'])))."<br>
		    ".(isset($parents_logins[$row['parent_kid_id']][2]) ? '<span class="abbr email'.($parents_logins[$row['parent_kid_id']][2]['login_counter'] ? '-accessed" title="Tėtis paskutinį kartą buvo prisijungęs '.$parents_logins[$row['parent_kid_id']][2]['last_login_time'] : ' title="Jau įvestas tėčio el. pašto adresas').' "">@'.filterText(getName($row['tevo_vardas'], $row['tevo_pavarde'])).'</span>' : filterText(getName($row['tevo_vardas'], $row['tevo_pavarde'])))."</td>
			<td>".$grupes[$row['grupes_id']]."</td>
			<td>".filterText($row['gim_data'])."<br>Amžius: ".$row['age']."</td>
			<td>".filterText($row['tevu_adresas'])."</td>
			<td>".$tel."</td>
			<td>".date_empty($row['lankymo_pradzia'])."<br>".date_empty($row['lankymo_pabaiga'])."</td>
			<td>".filterText($row['turetume_zinoti'])."</td>
			<td>".filterText($row['kita_info'])."</td>
			<td class=\"no-print\">".$versions_counter[$row['parent_kid_id']]."</td>
		    <td title=\"Ar archyve?\"><span class=\"abbr\">".($row['archyvas'] ? '+' : '-')."</span></td>
			<td class=\"no-print\">";
			if(IS_ALLOWED_TO_EDIT)
				echo '<a href="?edit='.$row['ID'].'">'.(LOGOPEDAS ? 'Peržiūrėti' : 'Keisti')."</a> ";//#child-form
			//if(ADMIN)
			//	echo "<a href=\"?delete=".$row['ID']."\" onclick=\"return confirm('Ar tikrai norite ištrinti? Geriausia, kad pakeistumėte vardą ir pavardę prieš ištrindami, kad geriau įsitikintumėte ar tikrai šitą norite ištrinti, nes ištrynimas turėtų reikšti, kad išsitrins viskas kas su tuo vaiku susiją, pvz., turėtų išsitrinti ir jo lankomumas. Ir tik tada jei visiškai tikri ištrinkite. Be vardo ir pavardės keitimo galite dar ir suarchyvuoti.')\">Trinti</a> ";
			//else
			if(!LOGOPEDAS) {
				if(!BUHALT) {
					if(isset($_GET['trash']))
						echo "<a href=\"?undo=".$row['ID']."\">Atstatyti</a> ";
					else
						echo "<a href=\"?virtualDelete=".$row['ID']."\" onclick=\"return confirm('Ar tikrai norite ištrinti? Vaikas išregistruojams iš darželio kitu būdu. Plačiau žiūrėkite puslapio viršuje esančią informaciją spustelėjus „Supažindinantis video su vaikų duomenų įsigaliojimu.“')\">Trinti</a> ";
				}
				echo '<a href="/dokumentai?vaiko_id='.$row['parent_kid_id'].'">Dokumentai</a>';
			}
			echo "</td></tr>";
		}
		echo '</table>';
	}
}


// EDIT -----------------------------
if(IS_ALLOWED_TO_EDIT && (isset($_GET['edit']) || isset($_GET['new']))) {
	if(isset($_GET['edit'])) {
		$result = db_query("SELECT * FROM `".DB_children."` WHERE `ID`=".(int)$_GET['edit']);
		$vaikas = mysqli_fetch_assoc($result);
		//Security:
		if(!ADMIN && !hasGroup($vaikas['grupes_id'])) {// != GROUP_ID
			unset($_GET['edit'], $vaikas);
		}
	}
	?>
	<fieldset id="child-form"<?=(isset($_GET['edit']) ? '' : ' class="no-print"')?>>
	<legend class="no-print"><?=(isset($_GET['edit']) ? filterText($l->getName($vaikas['vardas'].' '.$vaikas['pavarde'], 'kil'))." duomenų ".(isset($_GET['new-version']) ? 'naujas ' : '')."keitimas" : "Naujo vaiko duomenų įvedimas")?></legend>
	<form action="" method="get" class="no-print" id="form-new-version"><!-- #child-form -->
		<!-- <p class="notice">
			Duomenų (apie vaiką) laikotarpis - duomenys (apie vaiką) galiojantys nuo tam tikros datos.<br>
			<span class="abbr" title="Norint: išlaikyti duomenų asmeniškumą/privatumą kai keičiama vaiko grupė ir išlaikyti skirtingą mokėtinos sumos apskaičiavimą skirtingais laikortarpiais">Kam naudojami duomenų laikotarpiai?</span>
			<?php if(isset($_GET['edit'])) { ?>
			<span class="abbr" title="Įprastai naudojama tik tada kai keičiasi vaiko grupė, maisto davimas, grupės tipas, socialinė padėtis (ar mokestis).">Kada naudoti naują laikotarpį?</span>
			<?php } ?>
		</p> -->
	<?php
		if(isset($_GET['edit'])) {
			$result = db_query("SELECT * FROM `".DB_children."` WHERE `parent_kid_id`=".(int)$vaikas['parent_kid_id'].' AND `isDeleted`=0 ORDER BY `valid_from`');
			if(mysqli_num_rows($result)) {
				$i = -1;
				$versions = array();
				$CUR_DATE = date('Y-m-d');
				$not_inserted = true;
				while($row = mysqli_fetch_assoc($result)) {
					if(isset($_GET['new-version']) && $not_inserted && $row['valid_from'] > $_GET['valid_from_new']) {
						//echo 'aaa';
						$NEW_version = [ 'ID' => 0, 'valid_from' => $_GET['valid_from_new'] ];
						$versions[] = $NEW_version;
						$not_inserted = false;
					}
					$versions[] = $row;
				}
				if(isset($_GET['new-version']) && $not_inserted) {
					$NEW_version = [ 'ID' => 0, 'valid_from' => $_GET['valid_from_new'] ];
					$versions[] = $NEW_version;
					$not_inserted = false;
				}
				foreach($versions as $row) {
					if($row['valid_from'] <= $CUR_DATE)
						++$i;
				}
								/*if(isset($_GET['new-version']))
					echo '<div style="margin-top: 5px;">Kuriamas naujas, duomenų apie vaiką, laikotarpis pagrindu laikotarpio galiojančio nuo:';
				else
					echo '<div style="margin-top: 5px;"><!-- Rodomi duomenys <span class="notice">(apie vaiką)</span> galiojantys nuo: -->';*/

				//Duomenys galiojantys nuo
				//<span class="notice">(apie vaiką)</span> 
				//Galiojimo datos pastaba
				//Veiksmai
				//Nauji duomenys dėl naujo dokumento keičiančio: grupę, maitinimą, grupės tipą ar mokestį
				//Albinas: Duomenų apie naujai pateikiamus dokumentus įvedimas
				//Eiti į duomenų keitimą, Eiti į duomenų pakeitimo įvedimą, Kopijuoti, Eiti į dokumentą, Kopijuoti dar neišsaugant, Eiti į neišsaugotą kopiją, Eiti į neišsaugotą kopiją, Sudaryti kopiją
				?>
				<style>
				.versions-on-top { position:fixed; z-index: 10; top: 0px; margin-top: 0px !important;}
				</style>
				<label>Lentelę (esančią žemiau), būnant šiame puslapyje, visada rodyti ekrano viršuje <input type="checkbox" id="versions-on-top-switch"></label>
				<table class="no-hover" id="versions">
					<!-- <caption><?=filterText($l->getName($vaikas['vardas'].' '.$vaikas['pavarde'], 'kil'))?> duomenys</caption> --><?php
					if(count($versions) > 1)
						echo '<tr><th colspan="3" style="text-align:center">Vaiko duomenys galiojantys skirtingais laikotarpiais<!-- Pasirinkti vaiko duomenis --></th></tr>';
					?>
					<tr>
						<th>Duomenys galioja nuo <span title="Nuo mažiausio link didžiausio">↓<!-- ▼ --></span></th>
						<th>Duomenų įvedimo priežastis</th>
						<th>Duomenys apie naujai pateiktus dokumentus<!-- <br>Duomenų apie naujai pateikiamus dokumentus įvedimas --></th>
					</tr>
				<?php
				//echo '<br><div class="sel" style="margin-left: 10px;"><select name="edit" onchange="this.form.submit()">';
			
				$valid_id = (isset($versions[$i]['ID']) ? $versions[$i]['ID'] : -1);
				/*
				ir archyvuoti

Papildyti duomenis galiojančius nuo

Paruošti duomenis

Paruošti naujiems papildymams galiojančius nuo
Įvesti pakeitimus galiojančius nuo
Įvesti pakeitimus naujam laikotarpiui
Sukurti naują laikotarpį

Neišsaugotas

:
Albinas: naujai pateikiamo dokumento galiojimo datos įvedimas
Vaiko versija - skirtingas dokumentas
Dokumentas galioja nuo
				*/
				//foreach($versions as $row) {
				function valid_to($next_valid_from_date) {
					if(!empty($next_valid_from_date))
						return '–<span title="Sistema apskaičiavo duomenų galiojimo pabaigą" class="abbr notice" style="font-size: 13px;">'.date('Y-m-d', strtotime($next_valid_from_date. ' - 1 days')).'</span>';
					//return '–Neterminuotai';
				}
				for($i = 0; $i < count($versions); ++$i) {
					$row = $versions[$i];
				//
					$is_valid = $valid_id == $row['ID'] ? ' <span class="abbr" title="Šiandien ('.date('Y-m-d').') šie duomenys galioja" style="font-size: 12px;">Šiand. galioja</span> <!--  dabar galioja, dabar galiojanti, GALIOJANTYS, aktualūs -->' : '';//<span class="abbr" title="Šiandien ('.date('Y-m-d').') galioja (šie duomenys)">Š. G.</span>
					//$valid_from = $row['valid_from'] == '0000-00-00' ? 'pradžios' : $row['valid_from'];
					$valid_from = $row['valid_from'];
					$valid_to = valid_to((isset($versions[$i+1]['valid_from']) ? $versions[$i+1]['valid_from'] : 0));
					if($row['ID'] == 0) {
						$is_valid = $valid_id == $row['ID'] ? ' Šiandien šie duomenys dar negalioja, nes neišsaugoti.<!--  dabar galioja, dabar galiojanti, GALIOJANTYS, aktualūs -->' : '';
						echo '<tr class="opened-row" id="valid-form-'.$valid_from.'"><td>➤ '.$valid_from.$valid_to.' <strong>Dar neišsaugota!</strong> '.$is_valid.'</td><td></td><td></td></tr>';//Dar neišsaugoti duomenys
					} elseif($_GET['edit'] == $row['ID'] && !isset($_GET['new-version'])) {
						//echo '<option value="'.$row['ID'].'" selected="selected">'.$valid_from.' '.filterText(mb_truncate($row['version_comment'], 100)).'</option>';
						//Įvesti būsimus pakeitimus</span> pradėsiančius galioti
						//Spausti čia norint keisti grupę, maitinimą, grupės tipą ar mokestį
					
						//pradedančio
						//Reikalingas pakeitimas, pradėsiantis galioti nuo naujos datos
						//Kopijuoti naujai galiojimo datai
						//➔
						echo '<tr'.(isset($_GET['new-version']) ? ''/*no way because of outer if*/ : ' class="opened-row"').' id="valid-form-'.$valid_from.'"><td><span style="font-size: 18px; display: none;">➜✎✍</span>➤ <strong class="abbr" title="Šie '.(count($versions) == 1 ? 'vieninteliai ' : '').'duomenys atidaryti peržiūrai ir keitimui" style="/*color: #357d32*/">'.$valid_from.'</strong>'.$valid_to.$is_valid.'</td><td>'.filterText(mb_truncate($row['version_comment'], 100)).'</td><td>';
						//button type="button" class="submit"
						//a href="#"
						if(!LOGOPEDAS)
							echo '<button type="button" class="submit" id="new-version-btn" onclick="$(\'#new-version-form\').toggle(); $(\'#valid-from-new\').focus(); /*$(this).hide();*/ return false;" style="cursor: pointer; width: 385px;/*377*/ margin-bottom: 0 !important;"><!-- Ruoštis keitimui <strong>galiosiančiam nuo naujos datos</strong> --><!-- Reikalingas pakeitimas, <strong>galiosiantis nuo naujos datos</strong> -->Reikia pakeitimo, <strong>galiosiančio nuo naujos datos</strong></button>
							<!-- <br><button type="button" id="new-version-btn" onclick="$(\'#new-version-form\').toggle(); $(\'#valid-from-new\').focus(); /*$(this).hide();*/ return false;">Reikia pakeitimo, pradėsiančio galioti nuo naujos datos</button>
							<br><button type="button" id="new-version-btn" onclick="$(\'#new-version-form\').toggle(); $(\'#valid-from-new\').focus(); /*$(this).hide();*/ return false;">Naujai pateikiamo dokumento galiojimo datos įvedimas</button>
							<br><button type="button" id="new-version-btn" onclick="$(\'#new-version-form\').toggle(); $(\'#valid-from-new\').focus(); /*$(this).hide();*/ return false;">Sudaryti naują dokumentą kopijuojant šį</button> -->
							<div style="display: none; /*height: 150px;*/" id="new-version-form"><span class="abbr" title="Keitimas dėl naujai pateiktų dokumentų: grupės (ir kodo tabelyje jei jis keičiasi kartu), vaikas išregistruojamas arba/ir keičiantis duomenims iš skyrelio „Mokėtinos sumos ir maitinimo apskaičiavimui“.">Naujų duomenų<!-- Duomenų pakeitimų --></span> galiojimo pradžia <input class="datepicker" type="text" name="valid_from_new" id="valid-from-new"> <input type="hidden" name="edit" value="'.$vaikas['ID'].'"><br><input type="submit" name="new-version" class="submit abbr" style="margin:0; width: 100%; cursor: pointer;" value="(2) Patvirtinti datą" title="Kopijuoti duomenis galiojančius nuo '.$valid_from.'" id="new-version"><!--  (paskutinį kartą išsaugotus) --></div>';
						echo '</td></tr>';// value="'.($row['valid_from'] == $CUR_DATE ? '' : $CUR_DATE).'"
						//Pereiti į pakeitimo įvedimą
						//Pereiti į naujo pakeitimo įvedimą, kurį norite padaryti
						//Toliau, pakeitimų įvedimas
						//Nauja korekcija  (pagal šiuos duomenis) galiojanti nuo
						//Nuo šių duomenų, įvesti pakeitimą, galiojanį nuo
						//Bus įvedamas pakeitimas
					} else {
						if(ADMIN || hasGroup($row['grupes_id']))
							//echo '<option value="'.$row['ID'].'">'.$valid_from.' '.filterText(mb_truncate($row['version_comment'], 100)).'</option>';
							echo '<tr id="valid-form-'.$valid_from.'"><td><a href="?edit='.$row['ID'].'" data-scrollTo="">'.$valid_from.$valid_to.$is_valid.'</a></td><td>'.filterText(mb_truncate($row['version_comment'], 100)).'</td><td></td></tr>';//#child-form
						else
							//echo '<option value="'.$row['ID'].'" disabled="disabled">'.$valid_from.' '.filterText(mb_truncate($row['version_comment'], 100)).'</option>';
							echo '<tr id="valid-form-'.$valid_from.'"><td>'.$valid_from.$valid_to.$is_valid.' (ne Jūsų vaikų grupė)</td><td>'.filterText(mb_truncate($row['version_comment'], 100)).'</td><td></td></tr>';//šios vaiko grupės neturite
						//echo date_empty($row['updated']);
					}
				
				}
				//if(!isset($_GET['new-version']))
				//	echo '<tr><td colspan="3"><button type="button" id="archive-btn" onclick="$(\'#archive-form\').toggle(); /*$(this).hide();*/ return false;">Greitas vaiko išregistravimas</button><div style="display: none;" id="archive-form">Nuo:  <input class="datepicker" type="text" name="archive_from" value="'.($row['valid_from'] == $CUR_DATE ? '' : $CUR_DATE).'"> <input type="submit" name="archive" class="submit" style="margin:0" value="Išregistruoti" title="Išregistruoti iš darželio t. y. išbraukti iš sąrašo ir archyvuoti"></div></td></tr>';
					//Išbraukti iš sąrašo ir archyvuoti nuo | Išbraukti ir archyvuoti (išregistruoti iš darželio)
				echo '</table>';
			} /*else {
				echo '<div style="margin-top: 5px;"><input type="hidden" name="edit" value="'.$vaikas['ID'].'">Nėra anksčiau galiojusių duomenų apie vaiką negu '.($vaikas['valid_from'] == '0000-00-00' ? 'pradžios' : $vaikas['valid_from']).'.';//vaiko duomenų (versijų).<br>Kitais žodžiais: Tik viena vaiko informacijos versija.
			}*/
	
			/*
			$result = db_query("SELECT * FROM `".DB_children."` WHERE `parent_kid_id`=".(int)$vaikas['parent_kid_id'].' ORDER BY `valid_from`');
			if(mysqli_num_rows($result) > 1) {
				if(isset($_GET['new-version']))
					echo '<div style="margin-top: 5px;">Kuriamas naujas, duomenų apie vaiką, laikotarpis pagrindu laikotarpio galiojančio nuo:';
				else
					echo '<div style="margin-top: 5px;">Rodomi duomenys <span class="notice">(apie vaiką)</span> galiojantys nuo:';
				echo '<br><div class="sel" style="margin-left: 10px;"><select name="edit" onchange="this.form.submit()">';
				$i = 1;
				while($row = mysqli_fetch_assoc($result)) {
					if($_GET['edit'] == $row['ID'])
						echo '<option value="'.$row['ID'].'" selected="selected">'.($row['valid_from'] == '0000-00-00' ? 'pradžios' : $row['valid_from']).' '.filterText(mb_truncate($row['version_comment'], 100)).'</option>';
					else {
						if(ADMIN || hasGroup($row['grupes_id']))
							echo '<option value="'.$row['ID'].'">'.($row['valid_from'] == '0000-00-00' ? 'pradžios' : $row['valid_from']).' '.filterText(mb_truncate($row['version_comment'], 100)).'</option>';
						else
							echo '<option value="'.$row['ID'].'" disabled="disabled">'.($row['valid_from'] == '0000-00-00' ? 'pradžios' : $row['valid_from']).' '.filterText(mb_truncate($row['version_comment'], 100)).'</option>';
						//echo date_empty($row['updated']);
					}
					++$i;
				}
				echo '</select></div>';
			} else {
				echo '<div style="margin-top: 5px;"><input type="hidden" name="edit" value="'.$vaikas['ID'].'">Nėra anksčiau galiojusių duomenų apie vaiką negu '.($vaikas['valid_from'] == '0000-00-00' ? 'pradžios' : $vaikas['valid_from']).'.';//vaiko duomenų (versijų).<br>Kitais žodžiais: Tik viena vaiko informacijos versija.
			}*/
		}//Būsima pirma vaiko informacijos versija.
		/*if( !isset($_GET['new-version']) && isset($_GET['edit']) ) {
		?>
			<input type="submit" style="margin:0" name="new-version" value="Naujas laikotarpis pagal šiuos duomenis" class="submit">
			<button class="submit" style="margin:0" id="laikotarpis-btn" onclick="$('#laikotarpis').show(100); $('#laikotarpis-btn').hide(100);">Keisti laikotarpio pradžią ir pastabą</button>
		</div>
		<?php }*/ ?>
	</form>
	<br>
	<?php
	if(isset($_GET['edit']) && KESV)
		echo '<a href="http://dev.musudarzelis.lt/private/statistics/kid_data_in_tables.php?kid_id='.$vaikas['parent_kid_id'].'&prefix='.DB_PREFIX.'" class="no-print">Vaiko info</a>';

	function new_kid_version($style = '', $text = '€') {
		global $_GET;
		if(!isset($_GET['new-version']) && !isset($_GET['new'])) {
			//return '<button type="button" onclick="$(\'#new-version-form\').show(); $(\'#valid-from-new\').focus(); $(\'#versions\').get(0).scrollIntoView(); return false;" title="Keisti nuo naujos datos dėl naujai pristatytų dokumentų" style="'.$style.'" class="no-print" style="border: none;">'.$text.'</button>';
			//return '<div title="Pagal šį nustatymą skaičiuojamas apmokėjimas tėvams" style="'.$style.'" class="no-print">'.$text.'</div>';
			return '';
		}
		return '';
	}
	function payment_dependency() {
		//return '<span title="Pagal šį nustatymą skaičiuojamas apmokėjimas tėvams" style="/*font-weight: bold;*/color:#357d32">€</span>';
		return '';
	}
	if(isset($_GET['new-version'])) { ?>
	<script>
	$(function() {
		$('#version-comment').focus();
	});
	</script>
	<p class="notice"><a href="#" onclick="$('#explanation').toggle(); return false;">Paaiškinimas pirmą kartą naudojantis</a><span id="explanation" style="display: none;"><br><!-- Žemiau yra nukopijuoti duomenys (dar neišsaugoti) iš paskutinį kartą išsaugotų duomenų galiojančių nuo . Juos nukopijavus pakeista tik galiojimo data į 
	<br> -->Žemiau yra duomenys (dar neišsaugoti), kurie yra nukopijuoti iš duomenų galiojančių nuo <?=filterText($vaikas['valid_from'])?>, keiskite nukopijuotus duomenis kurie galios nuo <?=filterText($_GET['valid_from_new'])?>.

	<br>Galite įrašykite duomenų įvedimo priežastį. Jei vaiką išregistruojate pažymėkite varnelę ties „Vaikas išregistruotas“. Baigę keisti duomenis išsaugokite spustelėję mygtuką „Išsaugoti (naujus duomenis)“ esantį puslapio apačioje.</span></p>
	<?php } ?>
	<?php if(isset($_GET['new'])) { ?><p class="notice">Pastaba. Privaloma įvesti duomenis tik pažymėtus raudona žvaigždute t. y. <span class="required">*</span></p><?php } ?>
	<!-- gaalioja (galia) - galima ne taip suprasti -->
	<form action="<?php if(isset($_GET['new'])) echo '?'; if(isset($_GET['new-version'])) echo '?edit='.(int)$_GET['edit']; ?>#" method="post" onsubmit="return validateInput(this)" class="vaikas not-saved-reminder<?=(!isset($_GET['edit']) && !RODOMASIS ? ' new-input-form' : '')?>" id="kid-form">
		<div id="laikotarpis"<?=(!isset($_GET['new-version']) && isset($_GET['edit']) ? ' style="/*display: none;*/"' : '')?>>
		<!-- Duomenys galioja nuo /Duomenys galiojantys nuo   (Duomenų apie naujai pateiktus dokumentus įvedimas) -->
	
		<?php
		$valid_from_in_form = (isset($_GET['new-version']) ? filterText($_GET['valid_from_new']) : (isset($_GET['edit']) ? filterText($vaikas['valid_from']) : ''/*date('Y-m-d')*/));
		?>
		<!-- Įvedate vaiką, kurio dar nebuvo sistemoje, todėl šiuo atveju... | Šiuo atveju (įrašymo vaiko, kurio sistemoje dar nebuvo)... -->
		<p><label><!-- Šie duomenys sistemoje galios nuo --><span class="abbr" title="Visų žemiau esančių duomenų apie vaiką sistemoje galiojimo pradžia">Duomenys galioja nuo</span><span class="required">*</span><?=(!isset($_GET['new-version']) && !isset($_GET['edit']) ? ' <span class="abbr" title="Įrašant vaiką, kurio sistemoje dar nebuvo, įveskite vaiko registracijos datą darželyje pagal sutartį (įprastai rugsėjo mėn. 1 d.). Beje nuo šios datos vaikas bus meniu punkte „Lankomumas“, todėl jam bus galima žymėti lankomumą ir tai bus galima daryti iki jo išregistravimo sistemoje.">| Ką įrašyti?</span><!-- <span class="abbr" title="Sistema pasiūlė šiandienos datą, jei ji netinka pakeiskite ją. Nuo šios datos šiam vaikui bus galima žymėti lankomumą, meniu punkte „Lankomumas“.">| Įvestas siūlymas -&gt;</span> -->' : '')/*iki kitų šio vaiko duomenų sistemoje galiojimo pradžios*/?><!-- <span class="abbr" title="duomenų apie vaiką">Žemiau esančių duomenų</span><!- Vaiko informacijos versija galiojanti -> <span class="abbr" title="laikotarpio">galiojimo</span> pradžia --> <input required="required" class="datepicker" type="text" name="valid_from" value="<?=$valid_from_in_form?>" style="font-weight: bold; /*color: #357d32*/"></label></p>
		<!-- &#013; -->
		<p><label><span class="abbr" title="Trumpai kas ir dėl ko keitėsi, kad būtų lengviau matyti ir suprasti ateityje.">Duomenų įvedimo priežastis</span><!-- galiojimo pradžios pastaba Šito laikotarpio pastaba --> <input maxlength="255" type="text" id="version-comment" name="version_comment" value="<?=(isset($_GET['new-version']) ? '' : (isset($_GET['edit']) ? filterText($vaikas['version_comment']) : 'Darželio lankymo pradžia'))/*Pirmas vaiko atėjimas į darželį | Pirmoji vaiko lankomumo diena*/?>"></label></p>
		</div>
		<?php if(isset($_GET['edit']) && $allow_archive) { ?>
		<!-- Rodyti/keisti archyvavimo būseną | laikotarpio pradžios TO galiojimo datos -->
		<!-- Žymima tada kai vaikas išeina iš darželio.  -->
		<!-- Archyvavimas galios nuo tos datos, kuriai bus pažymėta varnelė. -->
		<!-- Pažymėjus, vaikas išbraukiamas (išregistruojamas) iš darželio nuo datos įvestos laukelyje „Duomenys galioja nuo“. Nuo tada vaiko duomenys yra archyvuojami ir neberodomi. Vaikų sąraše galima pamatyti tik tada, kai paieškoje pažymima, kad ieškoma archyvuotų vaikų. -->
		<p><label><span class="abbr" title="Pažymėjus, vaikas išbraukiamas (išregistruojamas) iš darželio nuo datos įvestos laukelyje „Duomenys galioja nuo“. Nuo tada vaiko duomenys yra archyve. Vaikų sąraše juos galima pamatyti atsidarius vaikų paiešką ir pažymėjus varnelę ties „Archyvas“.">Vaikas išregistruotas (neberodomas)<!-- ✖ --></span> <input type="checkbox" name="archyvas" value="1" <?=(isset($_GET['edit']) && $vaikas['archyvas'] ? 'checked="checked"' : '')?> id="archive-input"></label></p>
		<?php } ?>
		<br>
		<div id="when-archived-not-needed">
		<p><label>Vardas<span class="required">*</span> <input required="required" id="vardas" type="text" name="vardas" value="<?=(isset($_GET['edit']) ? filterText($vaikas['vardas']) : '')?>"></label></p>
		<p><label>Pavardė<span class="required">*</span> <input required="required" type="text" name="pavarde" value="<?=(isset($_GET['edit']) ? filterText($vaikas['pavarde']) : '')?>"></label></p>
		<p><label>Gimimo data <input type="text" name="gim_data" value="<?=(isset($_GET['edit']) ? filterText($vaikas['gim_data']) : '')?>"> (pvz., 2005-05-05)</label></p>
		<p><label>Mamos vardas <input type="text" name="mamos_vardas" value="<?=(isset($_GET['edit']) ? filterText($vaikas['mamos_vardas']) : '')?>"></label></p>
		<p><label>Mamos pavardė <input type="text" name="mamos_pavarde" value="<?=(isset($_GET['edit']) ? filterText($vaikas['mamos_pavarde']) : '')?>"></label></p>
		<?php
		$show = true;
		$email_use_for = '<span class="abbr" title="Laiškams, priminimui (artėjant dokumentų galiojimo pabaigai), prisijungimui (tik tada kai yra įjungti prisijungimai tėvams).">naudojama</span>';
		if(isset($_GET['edit'])) {
			$result = db_query("SELECT * FROM `".DB_users_allowed."` JOIN `".DB_users."` ON `".DB_users_allowed."`.`user_id`=`".DB_users."`.`user_id` WHERE `person_type`=0 AND `person_subtype`=1 AND `person_id`='".(int)$vaikas['parent_kid_id']."'");
			if(mysqli_num_rows($result) > 0) {
				$show = false;
				$mother = mysqli_fetch_assoc($result);
				echo '<p><label>Mamos el. paštas ('.$email_use_for.') <span class="abbr" title="Jeigu įvedate prašome atidžiai pasitikrinkite ar kiekvienas simbolis teisingas">!!!</span> <a href="#child-form" onclick="$(\'#mother_email\').prop(\'disabled\', false); return false;" title="Leisti keisti el. pašto adresą ir išsiųsti naujus prisijungimo duomenis išsaugojus" class="abbr no-print">Keisti</a> '.(KESV ? '<a href="/?Lpastas='.filterText($mother['email']).'" class="no-print">Log in</a><!-- <form action="http://dev.musudarzelis.lt/" method="post" target="_blank">
						<input type="hidden" name="pastas" value="'.$mother['email'].'">
						<input type="hidden" name="slaptazodis" value="">
						<input type="submit" name="prisijungimas" value="Log in">
					</form> -->' : '').' <input id="mother_email" type="text" name="mamos_pastas_disabled" value="'.$mother['email'].'" disabled="disabled"></label></p>';//Prisijungimas prie vaiko mamos el. paštu
			}
		}
		if($show)
			echo '<p><label>Mamos el. paštas ('.$email_use_for.') <span class="abbr" title="Jeigu įvedate prašome atidžiai pasitikrinkite ar kiekvienas simbolis teisingas">!!!</span> <input type="text" name="mamos_pastas"></label></p>';//Prisijungimas prie vaiko mamos el. paštu ⚠
		?>
		<p><label>Tėčio vardas <input type="text" name="tevo_vardas" value="<?=(isset($_GET['edit']) ? filterText($vaikas['tevo_vardas']) : '')?>"></label></p>
		<p><label>Tėčio pavardė <input type="text" name="tevo_pavarde" value="<?=(isset($_GET['edit']) ? filterText($vaikas['tevo_pavarde']) : '')?>"></label></p>
		<?php
		$show = true;
		if(isset($_GET['edit'])) {
			$result = db_query("SELECT * FROM `".DB_users_allowed."` JOIN `".DB_users."` ON `".DB_users_allowed."`.`user_id`=`".DB_users."`.`user_id` WHERE `person_type`=0 AND `person_subtype`=2 AND `person_id`='".(int)$vaikas['parent_kid_id']."'");
			if(mysqli_num_rows($result) > 0) {
				$show = false;
				$father = mysqli_fetch_assoc($result);
				echo '<p><label>Tėčio el. paštas ('.$email_use_for.') <span class="abbr" title="Jeigu įvedate prašome atidžiai pasitikrinkite ar kiekvienas simbolis teisingas">!!!</span> <a href="#child-form" onclick="$(\'#father_email\').prop(\'disabled\', false); return false;" title="Leisti keisti el. pašto adresą ir išsiųsti naujus prisijungimo duomenis išsaugojus" class="abbr no-print">Keisti</a> '.(KESV ? '<a href="http://dev.musudarzelis.lt/?Lpastas='.filterText($father['email']).'" class="no-print">Log in</a><!-- <form action="http://dev.musudarzelis.lt/" method="post" target="_blank">
						<input type="hidden" name="pastas" value="'.$father['email'].'">
						<input type="hidden" name="slaptazodis" value="">
						<input type="submit" name="prisijungimas" value="Log in">
					</form> -->' : '').' <input id="father_email" type="text" name="tevo_pastas_disabled" value="'.$father['email'].'" disabled="disabled"></label></p>';//Prisijungimas prie vaiko tėčio el. paštu
			}
		}
		if($show)
			echo '<p><label>Tėčio el. paštas ('.$email_use_for.') <span class="abbr" title="Jeigu įvedate prašome atidžiai pasitikrinkite ar kiekvienas simbolis teisingas">!!!</span> <input type="text" name="tevo_pastas"></label></p>';//Prisijungimas prie vaiko tėčio el. paštu
		?>
		<div class="sel-wrapper"><label for="group-id">Grupė<span class="required">*</span></label> <div class="sel"><select id="group-id" name="grupes_id" required>
			<option value="" selected hidden disabled>Pasirinkite grupę</option>
			<?php
			/*
			select required
			http://stackoverflow.com/questions/6048710/can-i-apply-the-required-attribute-to-select-fields-in-html5
			http://stackoverflow.com/questions/17479573/html5-required-attribute-on-non-supported-browsers
			*/
			foreach(getAllowedGroups() as $ID => $title)
				echo '<option value="'.$ID.'"'.(isset($_GET['edit']) && $ID == $vaikas['grupes_id'] || !isset($_GET['edit']) && !ADMIN && GROUP_ID ==  $ID ? ' selected="selected"' : '').">".$title."</option>";
			?>
			</select></div> <?=''/*new_kid_version('float: right; margin-right: 7px', 'Keisti nuo naujos datos')*/?> </div>	
		<p><label>Namų adresas <input type="text" name="tevu_adresas" value="<?=(isset($_GET['edit']) ? filterText($vaikas['tevu_adresas']) : '')?>"></label></p>

		<fieldset style="width: 480px; margin: 10px 0px 0px 20px;">
		<legend>Telefonai:</legend>
		<p><label>Mamos <input type="text" name="mam_tel" placeholder="37065544333" value="<?=(isset($_GET['edit']) ? (nr_edit($vaikas['mam_tel'])=='' ? '370' : nr_edit($vaikas['mam_tel'])) : '370')?>"> (pvz., 37065544333)</label></p>
		<p><label>Tėčio <input type="text" name="tecio_tel" value="<?=(isset($_GET['edit']) ? (nr_edit($vaikas['tecio_tel'])=='' ? '370' : nr_edit($vaikas['tecio_tel'])) : '370')?>"></label></p>
		<p><label>Globėjo <input type="text" name="glob_tel" value="<?=(isset($_GET['edit']) ? (nr_edit($vaikas['glob_tel'])=='' ? '370' : nr_edit($vaikas['glob_tel'])) : '370')?>"></label></p>
		<p><label>Namų <input type="text" name="namu_tel" value="<?=(isset($_GET['edit']) ? (nr_edit($vaikas['namu_tel'])=='' ? '370' : nr_edit($vaikas['namu_tel'])) : '370')?>"></label></p>
		<p><label>Papildomi: <textarea placeholder="37065544333 (brolio)" name="papildomi_tel" style="height: 50px;"><?=(isset($_GET['edit']) ? filterText($vaikas['papildomi_tel']) : '')?></textarea></label></p><!-- onblur="if (this.value == '') this.value = '37065544333 (brolio)';" onfocus="if (this.value == '37065544333 (brolio)') this.value = '';"  -->
		</fieldset>

		<p><label><span title="Kartais turėtų sutapti su laukeliu „Duomenys galioja nuo“<?=(!isset($versions) || count($versions) == 1 ? '. Kadangi šiuo atveju tik vieni duomenys tai turėtų sutapti.' : '')?> Pastaba. Sistema kituose meniu punktuose nenaudoja šio laukelio." class="abbr not-important">Sutarta <!-- Susitariama --> darželio lankymo pradžia</span> <!-- Pradėta/Pradės lankyti --> <input class="datepicker" type="text" name="lankymo_pradzia" placeholder="<?php echo date("Y-m-d"); ?>" value="<?=(isset($_GET['edit']) ? date_empty($vaikas['lankymo_pradzia']) : '')?>"></label></p>
		<p><label><span title="Kartais turėtų sutapti su laukeliu „Duomenys galioja nuo“. Pastaba. Sistema kituose meniu punktuose nenaudoja šio laukelio." class="abbr not-important">Sutarta darželio lankymo pabaiga</span> <input class="datepicker" type="text" name="lankymo_pabaiga" placeholder="<?php echo date("Y-m-d"); ?>" value="<?=(isset($_GET['edit']) ? date_empty($vaikas['lankymo_pabaiga']) : '')?>"></label></p>
	
		<?php if(!KaunoVaikystesLobiai) { ?>
		<p><label><span class="abbr" title="Ar rodyti šio vaiko tėvų el. paštų adresus kitiems tos pačios grupės tėvams?">Ar tėvai nori rodyti kitiems el. pašto adresą?</span> <input type="checkbox" name="arRodytiElPasta" value="1" <?=(isset($_GET['edit']) && $vaikas['arRodytiElPasta'] ? 'checked="checked"' : '')?>></label></p>
		<?php } ?>
	
	
		<fieldset style="width: 610px; margin: 10px 0px 0px 20px;">
		<legend class="print">Mokėtinos sumos ir maitinimo apskaičiavimui:</legend>
		<p class="height-for-reduced-size"><label>Alergiškas tam tikram maistui <input type="checkbox" name="allergic" value="1" <?=(isset($_GET['edit']) && $vaikas['allergic'] ? 'checked="checked"' : '')?>></label></p>
		<?php
		function free_food($name) {
			global $vaikas;
			echo '<div class="free-food-option"><label class="free-food-option">Nemokamai  <input class="free-food-option" type="checkbox" name="'.$name.'" value="1"'.(isset($_GET['edit']) && $vaikas[$name] ? ' checked="checked"' : '').'></label></div>';//<span class="abbr" title="Išimties tvarka"></span>
		}
		if(!KaunoVaikystesLobiai) { ?>
		<?php if(getConfig('ar_istaigoje_yra_pusryciai?', $valid_from_in_form)) { ?>
		<div class="height-for-reduced-size"><?=free_food('free_breakfast')?><label>Pusryčiai <?=payment_dependency()?> <input style="margin-right: 100px;" type="checkbox" name="arPusryciaus" value="1" <?=(isset($_GET['edit']) && $vaikas['arPusryciaus'] ? 'checked="checked"' : '')?>></label><?=new_kid_version('float: right')?></div>
		<?php } ?>
		<?php if(getConfig('ar_istaigoje_yra_priespieciai?', $valid_from_in_form)) { ?>
		<div class="height-for-reduced-size"><?=free_food('free_priespieciai')?><label>Priešpiečiai <?=payment_dependency()?> <input style="margin-right: 100px;" type="checkbox" name="arPriespieciaus" value="1" <?=(isset($_GET['edit']) && $vaikas['arPriespieciaus'] ? 'checked="checked"' : '')?>></label><?=new_kid_version('float: right')?></div>
		<?php } ?>
		<?php if(getConfig('ar_istaigoje_yra_pietus?', $valid_from_in_form)) { ?>
		<div class="height-for-reduced-size"><?=free_food('free_lunch')?><label>Pietūs <?=payment_dependency()?> <input style="margin-right: 100px;" type="checkbox" name="arPietaus" value="1" <?=(isset($_GET['edit']) && $vaikas['arPietaus'] ? 'checked="checked"' : '')?>></label><?=new_kid_version('float: right')?></div>
		<?php } ?>
		<?php if(getConfig('ar_istaigoje_yra_pavakariai?', $valid_from_in_form)) { ?>
		<div class="height-for-reduced-size"><?=free_food('free_afternoon_tea')?><label>Pavakariai <?=payment_dependency()?> <input style="margin-right: 100px;" type="checkbox" name="arPavakariaus" value="1" <?=(isset($_GET['edit']) && $vaikas['arPavakariaus'] ? 'checked="checked"' : '')?>></label><?=new_kid_version('float: right')?></div>
		<?php } ?>
		<?php if(getConfig('ar_istaigoje_yra_vakariene?', $valid_from_in_form)) { ?>
		<div><?=free_food('free_dinner')?><label>Vakarienė <?=payment_dependency()?> <input style="margin-right: 100px;" type="checkbox" name="arVakarieniaus" value="1" <?=(isset($_GET['edit']) && $vaikas['arVakarieniaus'] ? 'checked="checked"' : '')?>></label><?=new_kid_version('float: right')?></div>
		<?php } ?>
		<?php if(getConfig('ar_istaigoje_yra_naktipieciai?', $valid_from_in_form)) { ?>
		<div><?=free_food('free_naktipieciai')?><label>Naktipiečiai <?=payment_dependency()?> <input style="margin-right: 100px;" type="checkbox" name="arNaktipieciaus" value="1" <?=(isset($_GET['edit']) && $vaikas['arNaktipieciaus'] ? 'checked="checked"' : '')?>></label><?=new_kid_version('float: right')?></div>
		<?php } ?>
		<div class="sel-wrapper"><label for="arNuolaida">Mokestis<span class="required">*</span> <?=payment_dependency()?></label> <div class="sel"><select id="arNuolaida" name="arNuolaida" required>
			<option value="" selected hidden disabled>Pasirinkite mokestį</option>
			<?php
			foreach($nuolaidos_txt as $ID => $title)
				echo '<option value="'.$ID.'"'.(isset($_GET['edit']) && $ID == $vaikas['arNuolaida'] ? ' selected="selected"' : '').">".$title."</option>";
			?>
			</select></div> <?=new_kid_version('float: right; margin-right: 15px')?> </div>
	
		<?php /*<div>Grupės tipas<span class="required">*</span><!-- Mokymo įstaigoje -->
			<?=new_kid_version()?>
			<div style="margin-left: 30px;">
				<label><input type="radio" name="arDarzelyje" value="0" <?=(isset($_GET['edit']) && $vaikas['arDarzelyje'] == 0 ? 'checked="checked"' : '')?>> Lopšelio<!-- Ikimokyklinis: Lopšelyje--></label><br>
				<label><input type="radio" name="arDarzelyje" value="1" <?=(isset($_GET['edit']) && $vaikas['arDarzelyje'] == 1 ? 'checked="checked"' : '')?>> Ikimokyklinis darželio<!-- yje --></label><br>
				<label><input type="radio" name="arDarzelyje" value="2" <?=(isset($_GET['edit']) && $vaikas['arDarzelyje'] == 2 ? 'checked="checked"' : '')?>> Priešmokyklinė<!-- is --></label>
			</div>
		</div> */ ?>
	
		<div class="sel-wrapper"><label for="arDarzelyje">Grupės rūšis<span class="required">*</span> <?=payment_dependency()?></label> <div class="sel"><select id="arDarzelyje" name="arDarzelyje" required>
			<option value="" selected hidden disabled>Pasirinkite grupės rūšį</option>
			<?php
			foreach($kid_age_group_kieno as $ID => $title)
				echo '<option value="'.$ID.'"'.(isset($_GET['edit']) && $ID == $vaikas['arDarzelyje'] ? ' selected="selected"' : '').">".$title."</option>";
			?>
			</select></div> <?=new_kid_version('float: right; margin-right: 15px')?> </div>
	
		<?php
		$allowed_db_prefixes = ['knsRudno_', 't_', 'kretEgl_', 'kretrRasa_', 'pnvZvaigz_', 'psvLiep_', 'mazGiliuk_', 'knsrEglut_', 'trkrGandr_', 'kaiSpind_', 'klpNyk_', 'kaiAzuol_', 'akmGintar_', 'kretZil_', 'kretDarb_', 'kretKurm_', 'sauTrys_', 'kretRud_', 'kretZib_', 'kretJokub_', 'klpSau_', 'kaiZas_', 'knsrZil_', 'sauEgl_', 'molSau_'];
		if(/*!KaunoRBaibokyne*/ in_array(DB_PREFIX, $allowed_db_prefixes)) { ?>
		<div><label><span class="abbr" title="Abonentinis mokestis, kuriam nuolaida netaikoma">Mokestis už vietą darželyje</span> <input type="text" name="feeForPlace" value="<?=(isset($_GET['edit']) ? filterText(str_replace('.', ',', $vaikas['feeForPlace'])) : '')?>"></label></div>
		<?php } ?>
	
		<?php if(TauragesRAzuoliukas || TESTINIS) { ?>
		<p><label><span class="abbr" title="Vaikas apmokestinamas už sportą">Sportuoja</span> <input type="checkbox" name="attend_sport" value="1" <?=(isset($_GET['edit']) && $vaikas['attend_sport'] ? 'checked="checked"' : '')?>></label></p>
		<?php } ?>
	
		<?php /*<div>Mokestis<span class="required">*</span><?=new_kid_version()?><!-- Apmokėjimas Nuolaida -->
			<div style="margin-left: 30px;"><?php
			foreach($nuolaidos_txt as $ID => $val)
				echo '<label><input type="radio" name="arNuolaida" value="'.$ID.'" '.(isset($_GET['edit']) && $vaikas['arNuolaida'] == $ID ? 'checked="checked"' : '').'> '.$val.'</label><br>';
			?></div>
		</div>
		*/ ?>
		<?php } ?>
		<?php if(KaunoRBaibokyne /*per dieną*/ || KaunoVaikystesLobiai/*per mėnesį*/) { ?>
		<?php /*<div>Dienos mokestis<span class="required">*</span><?=new_kid_version()?>
			<div style="margin-left: 30px;">
				<label><input type="radio" name="dienosMokestis" value="0" <?=(isset($_GET['edit']) && $vaikas['dienosMokestis'] == 0 ? 'checked="checked"' : '')?>> Pilnos dienos kaina</label><br>
				<label><input type="radio" name="dienosMokestis" value="1" <?=(isset($_GET['edit']) && $vaikas['dienosMokestis'] == 1 ? 'checked="checked"' : '')?>> Pusės dienos kaina</label>
		</div>
		*/ ?>
		<div class="sel-wrapper"><label for="dienosMokestis">Dienos mokestis<span class="required">*</span></label> <div class="sel"><select id="dienosMokestis" name="dienosMokestis" required>
			<option value="" selected hidden disabled>Pasirinkite dienos mokestį</option>
			<?php
			$day_cost_labels = array(
				0 => 'Pilnos dienos kaina',
				1 => 'Pusės dienos kaina'
			);
			foreach($day_cost_labels as $ID => $title)
				echo '<option value="'.$ID.'"'.(isset($_GET['edit']) && $ID == $vaikas['dienosMokestis'] ? ' selected="selected"' : '').">".$title."</option>";
			?>
			</select></div> <?=new_kid_version('float: right; margin-right: 15px')?> </div>
		<?php } ?>
		<?php if(KaunoVaikystesLobiai/*per mėnesį*/) { ?>
		<div><label><?=getConfig('pavadinimas_laukeliui_menesines_nuolaidos_suma')?><input type="text" name="monthly_discount_sum"  value="<?=(isset($_GET['edit']) ? filterText(str_replace('.', ',', $vaikas['monthly_discount_sum'])) : '')?>"></label></div>
		<div><label><?=getConfig('pavadinimas_laukeliui_procentine_nuolaida_nuo_ugdymo_ir_prieziuros_kainos_per_men.')?><input type="text" name="percentage_discount_from_day_tax_per_month"  value="<?=(isset($_GET['edit']) ? filterText(str_replace('.', ',', $vaikas['percentage_discount_from_day_tax_per_month'])) : '')?>"></label></div>
		<?php } ?>
		</fieldset>
	
		<p><label>Ką turėtume žinoti apie Jūsų vaiką: <textarea name="turetume_zinoti" placeholder="pomėgiai, žaidimai, maistas, ligos ir kt." style="height: 50px;"><?=(isset($_GET['edit']) ? filterText($vaikas['turetume_zinoti']) : '')?></textarea></label></p>
		<p><label>Kita informacija: <textarea name="kita_info" style="height: 50px;"><?=(isset($_GET['edit']) ? filterText($vaikas['kita_info']) : '')?></textarea></label></p>
		<p><label><span class="abbr" title="Asmens sąskaita tabeliams, kitur vadinamas mokėtojo kodas">Vaiko kodas tabelyje</span> (iš buhalterijos) <input type="text" name="asm_sas" value="<?=(isset($_GET['edit']) ? filterText($vaikas['asm_sas']) : '')?>"></label></p>
		<?php if($bylos_nr_pagal_registracijos_knyga_zurnala) { ?>
		<p><label>Bylos nr. pagal registracijos knygą/žurnalą <input type="text" name="bylos_nr_zurnale" value="<?=(isset($_GET['edit']) ? filterText($vaikas['bylos_nr_zurnale']) : '')?>"></label></p>
		<?php } ?>
		</div>
		<?php
		if(!isset($_GET['edit'])) { ?>
			<p><input type="submit" name="save" value="Išsaugoti (naujus duomenis)" class="submit" onclick="var ret = true, vardas = $('input[name=vardas]').val(), pavarde = $('input[name=pavarde]').val(); $.ajax({url: '/workers/vaikai_edit.php?vardas='+vardas+'&pavarde='+pavarde, async : false, success: function(data){if(data != 'OK'){ret = confirm('Vaikas tokiu vardu ir pavarde jau įtrauktas į sarašą. Ar tikrai norite įraukti šį vaiką?');}}}); return ret;"><!-- naują ir kol kas vienintelį laikotarpį --></p>
		<?php } else/*EDIT*/ { ?>
			<?php if(KESV && !isset($_GET['print'])) {
				$res = db_query("SELECT * FROM `".DB_users_allowed."` JOIN `".DB_users."` ON `".DB_users_allowed."`.`user_id`=`".DB_users."`.`user_id` WHERE `person_type`>0 AND `person_id`='".(int)$vaikas['createdByEmployeeId']."'");
				$f = '';
				while($r = mysqli_fetch_assoc($res))
					$f .= $person_type[$r['person_type']].' '.$r['email'].' ';
				$res = db_query("SELECT * FROM `".DB_users_allowed."` JOIN `".DB_users."` ON `".DB_users_allowed."`.`user_id`=`".DB_users."`.`user_id` WHERE `person_type`>0 AND `person_id`='".(int)$vaikas['updatedByEmployeeId']."'");
				$s = '';
				while($r = mysqli_fetch_assoc($res))
					$s .= $person_type[$r['person_type']].' '.$r['email'].' ';
				echo 'Sukurta: &nbsp;'.$vaikas['created'].' '.getAllEmployees($vaikas['createdByEmployeeId']).' '.$f.';<br>Pakeista: '.$vaikas['updated'].' '.getAllEmployees($vaikas['updatedByEmployeeId']).' '.$s.($vaikas['isDeleted'] ? '<br>Ištrintas '.$vaikas['deleted'] : 'OK');
				echo '<br><a href="?delete='.(int)$_GET['edit'].'" onclick="return confirm(\'Ar tikrai norite visiškai ištrinti?\')">Ištrinti</a>';
			}
			?>
			<!-- <p><input type="hidden" name="ID" value="<?=(int)$vaikas['ID']?>"><input type="submit" name="update" value="Atnaujinti versiją / Išsaugoti atnaujintą versiją" class="submit"></p> -->
			<p><input type="hidden" name="ID" value="<?=(int)$vaikas['ID']?>">
			<?php
			if(!LOGOPEDAS) {
				if(!isset($_GET['new-version'])) { ?>
				<input type="submit" name="update" value="Išsaugoti" class="submit" id="update"><!--  esamame laikotarpyje, keičiant esamus duomenis --> <!--  | Atnaujinti versiją / Išsaugoti atnaujintą versiją -->
				<?php } else { ?>
				<input type="submit" name="save-new-version" value="Išsaugoti (naujus duomenis)" class="submit"><!-- , sukuriant naujo laikotarpio duomenis. Turėtų būti šie duomenys su nauja laikotarpio pradžia. --> <!--  naujame laikotarpyje -->
				<?php
				}
			}
			?>
			</p>
			<!-- <p class="notice">
			Antram mygtukui: Išsaugoti naują/papildomą versiją (versiją pradedančią galioti nuo kitos datos)<br>
			Išsaugoti į esamą versiją.<br>
			Išsaugoti sukuriant naują versiją.<br>
			Versija panašiausi žodžiai: laida, versija, duomenų fiksavimas datoje.
			<br>
			Išsaugoti pakeičiant esamą versiją<br>
			Išsaugoti naujoje versijoje<br>
			Išsaugoti keičiant esamą (info)<br>
			Išsaugoti naują modifikaciją</p> -->
		<?php
		}
		?>
	</form>
	</fieldset>
	<?php
}
?>
</div>
<script type="text/javascript" src="/libs/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="/libs/jquery.remember.js"></script>
<script type="text/javascript" src="/libs/columnManager/jquery.columnmanager.js"></script>
<script type="text/javascript" src="/workers/vaikai.js?v2"></script>
