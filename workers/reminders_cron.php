<?php
//if($_SERVER['REMOTE_ADDR'] !== '212.117.14.79') die();
header('Content-Type: text/text; charset=utf-8');
include '../main.php';
//echo '<pre>';

$text = "Laba diena,

Primename, kad dokumentas „%DOCUMENT%“ %KID% galioja iki %EXPIRY_DATE%. Iki šios dienos būtina atnešti naują dokumentą į %TITLE%.";
/*
Laba diena,

Primename, kad turite pateikti darželiui naują dokumentą „%DOCUMENT%“ iki %EXPIRY_DATE%.
*/

include '../libs/linksniai.php';
$l = new Linksniai;

foreach($db_prefixes as $prefix) {
	$result = db_query("SHOW TABLES LIKE '".$prefix."reminder'");
	if(mysqli_num_rows($result) > 0) {
		$result = db_query("SELECT yt.* FROM `".$prefix."config` yt JOIN (SELECT `title`, MAX(`valid_from`) `valid_from` FROM `".$prefix."config` WHERE `valid_from`<='".CURRENT_DATE."' AND `title`='darzelio_pavadinimas' GROUP BY `title`) ss ON yt.`title`=ss.`title` AND yt.`valid_from`=ss.`valid_from` AND yt.`title`='darzelio_pavadinimas'");//greatest-n-per-group
		$row = mysqli_fetch_assoc($result);
		$title = $row['value'];
		
		$workers = [];
		$result = db_query("SELECT * FROM `".$prefix."users`");
		while ($row = mysqli_fetch_assoc($result))
			$workers[$row['user_id']] = $row;

		$result = db_query("SELECT * FROM `".$prefix."reminder` WHERE CURDATE()=`expiry_date` - INTERVAL `remind_before` DAY");
		while ($reminder = mysqli_fetch_assoc($result)) {
			$res = db_query("SELECT * FROM `".$prefix."users_allowed` JOIN `".$prefix."users` ON `".$prefix."users_allowed`.`user_id`=`".$prefix."users`.`user_id` WHERE `person_type`=0 AND `person_id`=".(int)$reminder['kid_id']);
			$resKid = db_query("SELECT * FROM `".$prefix."vaikai` WHERE `ID`=".(int)$reminder['kid_id']);
			$kid = mysqli_fetch_assoc($resKid);
			$emails = array();
			$msg = str_replace('%TITLE%', $title, str_replace('%KID%', $l->getName(filterText($kid['vardas'].' '.$kid['pavarde']), 'kil'), str_replace('%DOCUMENT%', $reminder_document_types[$reminder['document_type']], str_replace('%EXPIRY_DATE%', $reminder['expiry_date'], $text))));
			while($kid_parent = mysqli_fetch_assoc($res)) {
				$emails[] = $kid_parent['email'];
				$workerUserId = $reminder['updatedByUserId'] != 0 ? $reminder['updatedByUserId'] : $reminder['createdByUserId'];
				if ( !sendemail($kid_parent['name'].' '.$kid_parent['surname'], $kid_parent['email'], 'MusuDarzelis', 'info@musudarzelis.lt', 'Priminimas. '.$reminder_document_types[$reminder['document_type']], $msg, 'plain', '', '', '', $workers[$workerUserId]['email'], filterText($workers[$workerUserId]['name'].' '.$workers[$workerUserId]['surname'])) )
					logdie("Nepavyko išsiųsti el. pašo sendemail(${kid_parent['name']}.' '.${kid_parent['surname']}, ${kid_parent['email']}, 'MusuDarzelis', 'info@musudarzelis.lt', 'Priminimas. '.".$reminder_document_types[$reminder['document_type']].", $msg)");
					//sendemail($toname, $toemail, $fromname, $fromemail, $subject, $message, $type = "plain", $cc = "", $bcc = "", $path = '')
			}
			
			if(!mysqli_query($db_link, "INSERT INTO `".$prefix."reminder_sent` SET 
				`reminder_id`=".$reminder['reminder_id'].", `document_type`=".(int)$reminder['document_type'].",
				`group_id`=".(int)$reminder['group_id'].", `kid_id`='".(int)$reminder['kid_id']."', 
				`sent_date`=CURRENT_TIMESTAMP(), `sent_emails`='".db_fix(implode(', ', $emails))."',
				`sent_text`='".db_fix($msg)."',
				`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'"))
				logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
		}
	}
}
?>OK
