<?php
if(!defined('DARBUOT')) exit();
$result = db_query("SHOW TABLES LIKE '".DB_health_observations."'");
if(mysqli_num_rows($result) > 0) {
?>
<h1>Sveikatos pastabos</h1>
<div id="content">
<?php
if(isset($_POST['add']) && !SOCIALINIS_PEDAGOGAS) {
	db_query("INSERT INTO `".DB_health_observations."` SET 
	`employee_id`='".(int)DARB_ID."', `child_id`='".(int)$_POST['child_id']."',
	`date`='".db_fix($_POST['date'])."', `height`='".(double)str_replace(',', '.', $_POST['height'])."', 
	`weight`='".(double)str_replace(',', '.', $_POST['weight'])."', `baldu_dydis`='".db_fix($_POST['baldu_dydis'])."', 
	`sutrikimai`='".db_fix($_POST['sutrikimai'])."', `rekomendacijos`='".db_fix($_POST['rekomendacijos'])."', 
	`note`='".db_fix($_POST['note'])."',
	`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
	msgBox('OK', 'Sėkmingai išsaugota.');
}
if (isset($_POST['update']) && !SOCIALINIS_PEDAGOGAS) {
	db_query("UPDATE `".DB_health_observations."` SET `employee_id`='".(int)DARB_ID."', `child_id`='".(int)$_POST['child_id']."',
	`date`='".db_fix($_POST['date'])."', `height`='".(double)str_replace(',', '.', $_POST['height'])."', 
	`weight`='".(double)str_replace(',', '.', $_POST['weight'])."', `baldu_dydis`='".db_fix($_POST['baldu_dydis'])."', 
	`sutrikimai`='".db_fix($_POST['sutrikimai'])."', `rekomendacijos`='".db_fix($_POST['rekomendacijos'])."', 
	`note`='".db_fix($_POST['note'])."',
	`updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."',
	`updated`=CURRENT_TIMESTAMP, 
	`updatedCounter`=`updatedCounter`+1
	WHERE `health_observation_id`=".(int)$_POST['ID']);
	msgBox('OK', "Informacija išsaugota!");
}

if(isset($_GET['archiveIt']) && !SOCIALINIS_PEDAGOGAS) {
	$id = (int)$_GET['archiveIt'];
	$result = db_query("SELECT `createdByEmployeeId` FROM `".DB_health_observations."` WHERE `health_observation_id`='".$id."'");
	$row = mysqli_fetch_assoc($result);
	if($row['createdByEmployeeId'] == DARB_ID || ADMIN) {//Can be more optimized
		db_query("UPDATE `".DB_health_observations."` SET `isArchived`=1 WHERE `health_observation_id`='".$id."'");
		msgBox('OK', 'Informacija archyvuota.');
	} else {
		//log it.
	}
}
if(isset($_GET['unArchiveIt']) && !SOCIALINIS_PEDAGOGAS) {
	$id = (int)$_GET['unArchiveIt'];
	$result = db_query("SELECT `createdByEmployeeId` FROM `".DB_health_observations."` WHERE `health_observation_id`='".$id."'");
	$row = mysqli_fetch_assoc($result);
	if($row['createdByEmployeeId'] == DARB_ID || ADMIN) {//Can be more optimized
		db_query("UPDATE `".DB_health_observations."` SET `isArchived`=0 WHERE `health_observation_id`='".$id."'");
		msgBox('OK', 'Informacija atstatyta.');
	} else {
		//log it.
	}
}

if(isset($_GET['delete']) && !SOCIALINIS_PEDAGOGAS) {
	$result = db_query("SELECT `createdByEmployeeId` FROM `".DB_health_observations."` WHERE `health_observation_id`='".(int)$_GET['delete']."'");
	$row = mysqli_fetch_assoc($result);
	if($row['createdByEmployeeId'] == DARB_ID || ADMIN) {//Can be more optimized
		db_query("DELETE FROM `".DB_health_observations."` WHERE `health_observation_id`=".(int)$_GET['delete']);
		msgBox('OK', 'Informacija ištrinta!');
	} else {
		//log it.
	}
}

if(!SOCIALINIS_PEDAGOGAS)
	echo '<a href="?#health-observation-form" class="no-print fast-action fast-action-add">Naujos sveikatos pastabos įvedimas</a>';
	echo (isset($_GET['archive']) && $_GET['archive'] == 1 ? ' <a href="?archive=0" class="no-print fast-action fast-action-archive">✖ Uždaryti archyvą</a>' : ' <a href="?archive=1" class="no-print fast-action fast-action-archive">Archyvas</a>');
echo ui_print()
?>

<table>
<tr>
	<th>Eil.<br>Nr.</th>
	<th>Vaikas</th>
	<th>Pažymos išdavimo data</th>
	<th>Ūgis (cm)</th>
	<th>Svoris (kg)</th>
	<th>Baldų dydis</th>
	<th>Sutrikimai</th>
	<th>Gydytojo rekomendacijos</th>
	<th>Pastabos</th>
	<?php if(!SOCIALINIS_PEDAGOGAS) { ?>
	<th class="no-print">Veiksmai</th>
	<?php } ?>
</tr>
<?php
if(ADMIN)// || SESELE
	$result = db_query("SELECT cr.*, `".DB_health_observations."`.*
	FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<=CURDATE() GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
	JOIN `".DB_health_observations."` ON cr.`parent_kid_id`=`".DB_health_observations."`.`child_id`
	WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 AND `isArchived`=".(isset($_GET['archive']) ? (int)$_GET['archive'] : 0 )."
	-- GROUP BY cr.parent_kid_id
	ORDER BY ".orderName('cr'));//cr.`vardas` ASC, cr.`pavarde` ASC
	/*SELECT * 
	FROM `".DB_health_observations."` JOIN `".DB_children."` ON ".DB_health_observations.".child_id=".DB_children.".ID
	WHERE  `".DB_children."`.`isDeleted`=0 AND `".DB_children."`.`archyvas`=0*/
else
	$result = db_query("SELECT cr.*, `".DB_health_observations."`.*
	FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<=CURDATE() GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
	JOIN `".DB_health_observations."` ON cr.`parent_kid_id`=`".DB_health_observations."`.`child_id`
	WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 AND cr.`grupes_id`=".GROUP_ID." AND `isArchived`=".(isset($_GET['archive']) ? (int)$_GET['archive'] : 0 )."
	-- GROUP BY cr.parent_kid_id
	ORDER BY ".orderName('cr'));//cr.`vardas` ASC, cr.`pavarde` ASC
	/*SELECT * FROM `".DB_health_observations."` 
	JOIN `".DB_children."` ON ".DB_health_observations.".child_id=".DB_children.".ID 
	WHERE `".DB_children."`.`grupes_id`=".GROUP_ID." AND `".DB_children."`.`isDeleted`=0 AND `".DB_children."`.`archyvas`=0*/
	//JOIN ".DB_employees_groups." ON ".DB_children.".grupes_id=".DB_employees_groups.".grup_id 
	//WHERE ".DB_employees_groups.".darb_id=".(int)DARB_ID
$i = 0;
while($row = mysqli_fetch_assoc($result)) {
	echo '<tr><td>'.++$i.'</td>
		<td>'.filterText(getName($row['vardas'], $row['pavarde'])).'</td>
		<td>'.filterText(date_empty($row['date'])).'</td>
		<td>'.filterText($row['height']).'</td>
		<td>'.filterText($row['weight']).'</td>
		<td>'.filterText($row['baldu_dydis']).'</td>
		<td>'.filterText($row['sutrikimai']).'</td>
		<td>'.filterText($row['rekomendacijos']).'</td>
		<td>'.filterText($row['note']).'</td>';
		if(!SOCIALINIS_PEDAGOGAS) {
			echo '<td class="no-print"><a href="?edit='.$row['health_observation_id'].'#health-observation-form">Keisti</a>';
			
			if($row['createdByEmployeeId'] == DARB_ID || ADMIN) {
				//echo " ".(isset($_GET['trash']) ? "<a href=\"?undelete=".$row['health_observation_id']."\">Atstatyti</a>" : "<a href=\"".$URL."delete=".$row['health_observation_id']."\" onclick=\"return confirm('Ar tikrai norite perkelti į šiukšlinę dokumentą?')\">Trinti</a>")." ";
				echo ' '.(isset($_GET['archive']) && $_GET['archive'] == 1 ? "<a href=\"?unarchive=".$row['health_observation_id']."\">Išarchyvuoti</a>" : "<a href=\"?archiveIt=".$row['health_observation_id']."\" onclick=\"return confirm('Ar tikrai norite archyvuoti?')\">Archyvuoti</a>");
				
				echo ' <a href="?delete='.$row['health_observation_id'].'" onclick="return confirm(\'Ar tikrai norite ištrinti?\')">Trinti</a></td>';
			}
		}
		echo '
		</tr>';
}
?>
</table>
<?php
if(isset($_GET['edit'])) {
	$result = db_query("SELECT * FROM `".DB_health_observations."` WHERE `health_observation_id`=".(int)$_GET['edit']);
	//Security:
	//if(!ADMIN && !SESELE && $vaikas['grupes_id'] != GROUP_ID) {
	//	unset($_GET['edit'], $vaikas);
	//}
	if(mysqli_num_rows($result))
		$vaikas = mysqli_fetch_assoc($result);
	else
		unset($_GET['edit']);
}
if(!SOCIALINIS_PEDAGOGAS) {
?>
<fieldset style="margin-top: 40px;" method="post" class="no-print">
	<legend><?=(isset($_GET['edit']) ? 'Redaguoti sveikatos pastabą' : 'Nauja sveikatos pastaba:')?></legend>
	<form method="post" id="health-observation-form" class="not-saved-reminder">
		<div><label>Vaikas: <div class="sel"><select name="child_id">
			<?php
			//DRY: ataskaita_pagal_vaika.php
			/*if(ADMIN)// || SESELE
				$result = mysqli_query($db_link, "SELECT * FROM ".DB_children." WHERE `isDeleted`=0 AND `archyvas`=0");
			else
				$result = mysqli_query($db_link, "SELECT * FROM ".DB_children." WHERE `grupes_id`=".GROUP_ID." AND `isDeleted`=0 AND `archyvas`=0");*/
			$result = db_query($get_kids_sql);
				//JOIN ".DB_employees_groups." ON ".DB_children.".grupes_id=".DB_employees_groups.".grup_id WHERE ".DB_employees_groups.".darb_id=".(int)DARB_ID
			while($row = mysqli_fetch_assoc($result))
				echo "<option value=\"".$row['parent_kid_id']."\"".(isset($_POST['child_id']) && $_POST['child_id'] == $row['parent_kid_id'] ? ' selected="selected"' : '').">".filterText(getName($row['vardas'], $row['pavarde']))."</option>";
			?>
		</select></div></label></div>
		<p><label>Pažymos išdavimo data: <input class="datepicker" type="text" name="date" placeholder="<?=date("Y-m-d")?>" value="<?=(isset($_GET['edit']) ? filterText($vaikas['date']) : '')?>"></label></p>
		<p><label>Ūgis (cm): <input type="text" name="height" style="width: 50px;" value="<?=(isset($_GET['edit']) ? filterText($vaikas['height']) : '')?>"></label></p>
		<p><label>Svoris (kg): <input type="text" name="weight" style="width: 50px;" value="<?=(isset($_GET['edit']) ? filterText($vaikas['weight']) : '')?>"></label></p>
		<p><label>Baldų dydis<!-- ženklinimas/markiracija -->: <input type="text" name="baldu_dydis" style="width: 50px;" value="<?=(isset($_GET['edit']) ? filterText($vaikas['baldu_dydis']) : '')?>"></label></p>
		<p><label>Sutrikimai:<br><textarea name="sutrikimai" style="width: 400px; height: 80px;"><?=(isset($_GET['edit']) ? filterText($vaikas['sutrikimai']) : '')?></textarea><label></p>
		<p><label>Gydytojo rekomendacijos:<br><textarea name="rekomendacijos" style="width: 400px; height: 80px;"><?=(isset($_GET['edit']) ? filterText($vaikas['rekomendacijos']) : '')?></textarea><label></p>
		<p><label>Pastabos:<br><textarea name="note" style="width: 400px; height: 80px;"><?=(isset($_GET['edit']) ? filterText($vaikas['note']) : '')?></textarea><label></p>
		<?=(isset($_GET['edit']) ? '<input type="hidden" name="ID" value="'.$vaikas['health_observation_id'].'">' : '')?>
		<p><input type="submit" name="<?=(isset($_GET['edit']) ? 'update' : 'add')?>" value="Išsaugoti" class="submit"></p>
	</form>
</fieldset>
</div>
<?php
}
} else {
	echo 'Jūsų darželiui šis modulis neįjungtas.';
}
