<?php if(!defined('DARBUOT') && !DARBUOT) exit(); /*TODO: tik auklėtojoms? */ ?>
<h1>Virtuali biblioteka <span class="abbr no-print" title="tėvai"><!-- Auklėtojos ir -->(matomumas)</span></h1>
<div id="content">
<?php
/*
CREATE TABLE IF NOT EXISTS `3libraryObject` (
  `kindergarten_id` smallint(5) unsigned NOT NULL,
  `ID` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
  `publication_year` int(4) NOT NULL,
  `ISBN` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
  `description` text COLLATE utf8_lithuanian_ci NOT NULL,
  `place` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
  `libraryObjectType` tinyint(1) unsigned NOT NULL,
  `createdByUserId` int(7) unsigned NOT NULL,
  `createdByEmployeeId` int(7) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedByUserId` int(7) unsigned NOT NULL DEFAULT '0',
  `updatedByEmployeeId` int(7) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `3libraryObject_debt` (
  `kindergarten_id` smallint(5) unsigned NOT NULL,
  `ID` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `libraryObjectId` int(7) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
  `contacts` text COLLATE utf8_lithuanian_ci NOT NULL,
  `debtFrom` date,
  `debtTo` date,
  `returnDate` date,
  `createdByUserId` int(7) unsigned NOT NULL,
  `createdByEmployeeId` int(7) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedByUserId` int(7) unsigned NOT NULL DEFAULT '0',
  `updatedByEmployeeId` int(7) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1;

tėvams, pedagogams, administracijai tai galėtų būti meniu punkte „Duomenų bankas“ (skyriuje „Kita“ arba „Darželio informacija“) prisijungus prie sistemos „Mūsų darželis“. Pavadinimą norėtųsi aiškesnį įrašyti.

gali kas keisti (tik tas kas įvedė informaciją ir administratorius), trinti informaciją (kaip suprantu trinti galėtų tik administratorius, nes kitaip vėlgi gali ištrinti auklėtojos pasiskolinimą, nors nepasiskolino)? TAIP

Daikto pasiskolinimas (susiejimas su daiktu):
1. Auklėtoja arba administratorius įrašo knygą paėmusio asmens vardą, pavardę. Ar žmogaus kontaktų nereikia? PUIKI MINTIS, GALIME ĮTRAUKTI IR KONTAKTUS

2. Auklėtoja arba administratorius įrašo skaitymo terminas nuo     iki. TAIP
3. TIK administratorius įrašo atidavimo žymą (kitaip tariant atidavimo datą), kad niekas kitas negalėtų pažymėti, kad grąžino, nors negrąžino. TAIP 
4. TIK administratoriui matyti statistiką kiek kas pasinaudojo skaitymo/priemonių pasiskolinimo paslaugomis; Patikslinkime. Pvz., įgyvendiname sąrašu: Pasiskolinusio vardas pavardė, kontaktai, daiktą: pavadinimas, leidimo metai, daikto rūšis.

Perkeliu tai ko reikia iš ano laiško, kad nereikėtų skaityti kiekvieną kartą visų laiškų:
Daiktas:
1. Pavadinimas;
2. Leidimo metai;
3. ISBN kodas (jei toks yra);
4. Trumpa anotacija/santrauka;
5. Kur saugoma;
6. Leidinio rūšis: Knyga, Žurnalas, Metodinis leidinys, Metodinė priemonė (padalomosios kortelės su metų laikais (pvz) arba pirštukinių lėlių teatras)

Visas sąrašas leidinių ir priemonių turi būti matomas tėvams, pedagogams, administracijai.
*/
if(isset($_POST['save'])) {
	db_query("INSERT INTO `3libraryObject` SET 
		`kindergarten_id`=".DB_ID.",
		`title`='".db_fix($_POST['title'])."',
		`publication_year`='".db_fix($_POST['publication_year'])."',
		`ISBN`='".db_fix($_POST['ISBN'])."',
		`description`='".db_fix($_POST['description'])."',
		`place`='".db_fix($_POST['place'])."',
		`onlyForWorkers`='".(isset($_POST['onlyForWorkers']) ? 1 : 0)."',
		`libraryObjectType`='".(int)$_POST['libraryObjectType']."',
		`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
	msgBox('OK', 'Įrašas išsaugotas.');
}
if(isset($_POST['edit'])) {
	db_query("UPDATE `3libraryObject` SET 
		`title`='".db_fix($_POST['title'])."',
		`publication_year`='".db_fix($_POST['publication_year'])."',
		`ISBN`='".db_fix($_POST['ISBN'])."',
		`description`='".db_fix($_POST['description'])."',
		`place`='".db_fix($_POST['place'])."',
		`onlyForWorkers`='".(isset($_POST['onlyForWorkers']) ? 1 : 0)."',
		`libraryObjectType`='".(int)$_POST['libraryObjectType']."',
		`updated`=CURRENT_TIMESTAMP, `updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."',
		`updatedCounter`=`updatedCounter`+1
		WHERE `kindergarten_id`=".DB_ID." AND `ID`=".(int)$_POST['edit']);
	msgBox('OK', 'Įrašas atnaujintas.');
}
if(isset($_GET['delete']) && ADMIN) {
	db_query("DELETE FROM `3libraryObject` WHERE `kindergarten_id`=".DB_ID." AND `ID`=".(int)$_GET['delete']);
	msgBox('OK', 'Įrašas ištrintas.');
}


if(isset($_POST['saveDebt'])) {
	db_query("INSERT INTO `3libraryObject_debt` SET 
		`kindergarten_id`=".DB_ID.",
		`libraryObjectId`='".(int)$_POST['libraryObjectId']."',
		`name`='".db_fix($_POST['name'])."',
		`surname`='".db_fix($_POST['surname'])."',
		`contacts`='".db_fix($_POST['contacts'])."',
		`debtFrom`='".db_fix($_POST['debtFrom'])."',
		`debtTo`='".db_fix($_POST['debtTo'])."',
		`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
	msgBox('OK', 'Įrašas išsaugotas.');
}
if(isset($_POST['editDebt'])) {
	db_query("UPDATE `3libraryObject_debt` SET 
		`name`='".db_fix($_POST['name'])."',
		`surname`='".db_fix($_POST['surname'])."',
		`contacts`='".db_fix($_POST['contacts'])."',
		`debtFrom`='".db_fix($_POST['debtFrom'])."',
		`debtTo`='".db_fix($_POST['debtTo'])."',
		".(ADMIN && !empty($_POST['returnDate']) ? "`returnDate`='".db_fix($_POST['returnDate'])."'," : '')."
		`updated`=CURRENT_TIMESTAMP, `updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."',
		`updatedCounter`=`updatedCounter`+1
		WHERE `kindergarten_id`=".DB_ID." AND `ID`=".(int)$_POST['editDebt']);
	msgBox('OK', 'Įrašas atnaujintas.');
}
if(isset($_GET['deleteDebt']) && ADMIN) {
	db_query("DELETE FROM `3libraryObject_debt` WHERE `kindergarten_id`=".DB_ID." AND `ID`=".(int)$_GET['deleteDebt']);
	msgBox('OK', 'Įrašas ištrintas.');
}
?>
<a href="?new#form" class="no-print fast-action fast-action-add">Pridėti priemonę</a> &nbsp; <?php
	if(isset($_GET['debts']))
		echo '<a href="?#form" class="no-print fast-action">Uždaryti statistiką</a>';
	else
		echo '<a href="?debts#form" class="no-print fast-action">Statistika</a>';
	echo ui_print();


$all_debt = [];
$result = db_query("SELECT * FROM `3libraryObject_debt` WHERE `kindergarten_id`=".DB_ID."");
while($row = mysqli_fetch_assoc($result))
	$all_debt[$row['libraryObjectId']] = $row;
	
$debt = [];
$result = db_query("SELECT * FROM `3libraryObject_debt` WHERE `kindergarten_id`=".DB_ID." AND `returnDate` IS NULL");
while($row = mysqli_fetch_assoc($result))
	$debt[$row['libraryObjectId']] = $row;

if(!isset($_GET['debts'])) {
	$result = db_query("SELECT * FROM `3libraryObject` WHERE `kindergarten_id`=".DB_ID." ORDER BY `onlyForWorkers`, `libraryObjectType`, `title`");
	if(mysqli_num_rows($result) > 0) {
		?>
		<h2>Turtas</h2>
		<table>
		<tr>
			<th>Pavadinimas</th>
			<th class="date-cell">Leidimo<br>metai</th>
			<th>ISBN</th>
			<th>Trumpas aprašas</th>
			<th>Kur saugoma</th>
			<th>Rūšis</th>
			<th>Tik darbuotojams</th>
			<th class="no-print">Veiksmai</th>
		</tr>
		<?php
		while($row = mysqli_fetch_assoc($result)) {
			echo "<tr><td>".filterText($row['title'])."</td><td>".filterText($row['publication_year'])."</td><td>".filterText($row['ISBN'])."</td><td>".nl2br(filterText($row['description']))."</td><td>".filterText($row['place'])."</td><td>".$library_objects_types[$row['libraryObjectType']]."</td><td>".($row['onlyForWorkers'] ? 'Tik' : '')."</td><td class=\"no-print\">";
			if(ADMIN || $row['createdByEmployeeId'] == DARB_ID)
				echo "<a href=\"?edit=".$row['ID']."#form\">Keisti</a>";
			if(ADMIN)
				echo " <a href=\"?delete=".$row['ID']."\" onclick=\"return confirm('Ar tikrai norite ištrinti šį įrašą?')\">Trinti</a>";
		
			if(isset($debt[$row['ID']])) {
				if(ADMIN)
					echo ' <a href="?editDebt='.$debt[$row['ID']]['ID'].'#form">Paskolinta iki '.$debt[$row['ID']]['debtTo'].'</a>';
				else
					echo ' Paskolinta iki '.$debt[$row['ID']]['debtTo'];
			} else
				echo " <a href=\"?newDebt=".$row['ID']."#form\">Pasiimti</a>";
			
			echo " <a href=\"?debts=".$row['ID']."\">Statistika</a>";
			echo "</td></tr>";
		}
		echo '</table>';
	}
} else {
	//$result = db_query("SELECT * FROM `3libraryObject` JOIN `3libraryObject_debt` ON `3libraryObject`.`ID`=`libraryObjectId`
	//WHERE `kindergarten_id`=".DB_ID.(!empty($_GET['debts']) ? " AND `3libraryObject`.`ID`=".(int)$_GET['debts'] : ''));
	$result = db_query("SELECT * FROM `3libraryObject` WHERE `kindergarten_id`=".DB_ID.(!empty($_GET['debts']) ? " AND `ID`=".(int)$_GET['debts'] : '').' ORDER BY `onlyForWorkers`, `libraryObjectType`, `title`');
	$result_debt = db_query("SELECT * FROM `3libraryObject_debt` WHERE `kindergarten_id`=".DB_ID.(!empty($_GET['debts']) ? " AND `libraryObjectId`=".(int)$_GET['debts'] : '').' ORDER BY `returnDate` DESC');
	$debts = [];
	while($row = mysqli_fetch_assoc($result_debt)) {
		$debts[$row['libraryObjectId']][] = $row;
	}
	if(mysqli_num_rows($result) > 0) {
		?>
		<h2>Turto skolinimosi statistika</h2>
		<table>
		<tr>
			<th>Pavadinimas</th>
			<th class="date-cell">Leidimo<br>metai</th>
			<th>ISBN</th>
			<th>Trumpas aprašas</th>
			<th>Kur saugoma</th>
			<th>Rūšis</th>
			<th>Tik darbuotojams</th>
		</tr>
		<?php
		$id = 0;
		while($row = mysqli_fetch_assoc($result)) {
			echo "<tr><td>".filterText($row['title'])."</td><td>".filterText($row['publication_year'])."</td><td>".filterText($row['ISBN'])."</td><td>".nl2br(filterText($row['description']))."</td><td>".filterText($row['place'])."</td><td>".$library_objects_types[$row['libraryObjectType']]."</td><td>".($row['onlyForWorkers'] ? 'Tik' : '')."</td></tr>";
			if(isset($debts[$row['ID']])) {
				echo '<tr><td colspan="100"><table>
					<tr>
					<th>Vardas Pavardė</th>
					<th>Kontaktai</th>
					<th>Pasiimta nuo–iki</th>
					<th>Grąžinta</th>
					<th class="no-print">Veiksmai</th>
					</tr>';
				foreach($debts[$row['ID']] as $row) {
					echo "<tr><td>".filterText($row['name'].' '.$row['surname'])."</td>
					<td>".filterText($row['contacts'])."</td>
					<td>".filterText($row['debtFrom'].'–'.$row['debtTo'])."</td>
					<td>".filterText($row['returnDate'])."</td>
					<td class=\"no-print\">";
					if(ADMIN) {
						echo ' <a href="?editDebt='.$row['ID'].'#form">Keisti</a>';
						echo ' <a href="?deleteDebt='.$row['ID'].'" onclick="return confirm(\'Ar tikrai norite ištrinti šį įrašą?\')">Ištrinti</a>';
					}
					echo "</td></tr>";
				}
				echo '</table></td></tr>';
			}
		}
		echo '</table>';
	}
}


if (isset($_GET['edit']) || isset($_GET['new'])) {
	if (isset($_GET['edit'])) {
		$result = db_query("SELECT * FROM `3libraryObject` WHERE `kindergarten_id`=".DB_ID." AND `ID`=".(int)$_GET['edit']);
		$row = mysqli_fetch_assoc($result);
	}
	?>
	<fieldset id="form">
	<legend><?=(isset($_GET['edit']) ? 'Priemonės informacijos keitimas ' : 'Priemonės informacijos pridėjimas')?></legend>
	<form method="post" class="not-saved-reminder no-print">
		<p><label>Pavadinimas<span class="required">*</span> <input type="text" style="width: 450px;" name="title" value="<?=(isset($row) ? filterText($row['title']) : '')?>"></label></p>
		<p><label>Leidimo metai<span class="required">*</span> <input type="text" name="publication_year" value="<?=(isset($row) ? filterText($row['publication_year']) : '')?>" maxlength="4" style="width: 50px;"></label></p>
		<p><label>ISBN <input type="text" name="ISBN" value="<?=(isset($row) ? filterText($row['ISBN']) : '')?>"></label></p>
		<p><label>Komantaras, trumpa anotacija/santrauka <textarea name="description"><?=(isset($row) ? filterText($row['description']) : '')?></textarea></label></p>
		<p><label>Kur saugoma<span class="required">*</span> <input type="text" style="width: 450px;" name="place" value="<?=(isset($row) ? filterText($row['place']) : '')?>"></label></p>
		<p><label>Rodyti tik darbuotojams <input type="checkbox" name="onlyForWorkers" value="1" <?=(isset($row) && $row['onlyForWorkers'] ? ' checked="checked"' : '')?>"></label></p>
		<div><label for="libraryObjectType">Leidinio rūšis<span class="required">*</span></label> <div class="sel"><select id="libraryObjectType" name="libraryObjectType" required>
			<option value="" selected hidden disabled>Pasirinkite</option>
			<?php
			foreach($library_objects_types as $ID => $title)
				echo '<option value="'.$ID.'"'.(isset($row) && $ID == $row['libraryObjectType'] ? ' selected="selected"' : '').">".$title."</option>";
			?>
			</select></div></div>
		<p><input type="hidden" <?=(!isset($_GET['edit']) ? 'name="save"' : 'name="edit" value="'.(int)$row['ID'].'"')?>><input type="submit" value="Išsaugoti" class="submit"></p>
	</form>
	</fieldset>
<?php
}

if (isset($_GET['editDebt']) || isset($_GET['newDebt'])) {
	if (isset($_GET['editDebt'])) {
		$result = db_query("SELECT * FROM `3libraryObject_debt` WHERE `kindergarten_id`=".DB_ID." AND `ID`=".(int)$_GET['editDebt']);
		$edit = mysqli_fetch_assoc($result);
	}
	$result = db_query("SELECT * FROM `3libraryObject` WHERE `kindergarten_id`=".DB_ID." AND `ID`=".(isset($_GET['editDebt']) ? $edit['libraryObjectId'] : (int)$_GET['newDebt']));
	$row = mysqli_fetch_assoc($result);
	
	?>
	<fieldset id="form">
	<legend><?=(isset($_GET['editDebt']) ? 'Priemonės skolinimosi keitimas ' : 'Priemonės skolinimosi pridėjimas')?></legend>
	<form method="post" class="not-saved-reminder no-print">
		<p><label>Vardas<span class="required">*</span> <input type="text" name="name" value="<?=(isset($edit) ? filterText($edit['name']) : '')?>"></label></p>
		<p><label>Pavardė<span class="required">*</span> <input type="text" name="surname" value="<?=(isset($edit) ? filterText($edit['surname']) : '')?>"></label></p>
		<p><label>Kontaktai <input type="text" name="contacts" value="<?=(isset($edit) ? filterText($edit['contacts']) : '')?>"></label></p>
		Datos:
		<p><label>Pasiima nuo <span class="required">*</span> <input class="datepicker" type="text" name="debtFrom" value="<?=(isset($edit) ? filterText($edit['debtFrom']) : '')?>"></label></p>
		<p><label>Grąžins iki <span class="required">*</span> <input class="datepicker" type="text" name="debtTo" value="<?=(isset($edit) ? filterText($edit['debtTo']) : '')?>"></label></p>
		<?php if(ADMIN && isset($_GET['editDebt'])) { ?>
		<p><label>Grąžino (data) <span class="required">*</span> <input class="datepicker" type="text" name="returnDate" value="<?=(isset($edit) ? filterText($edit['returnDate']) : '')?>"></label></p>
		<?php } ?>
		<p><input type="hidden" <?=(!isset($_GET['editDebt']) ? 'name="saveDebt"' : 'name="editDebt" value="'.(int)$edit['ID'].'"')?>><?=(isset($_GET['newDebt']) ? '<input type="hidden" name="libraryObjectId" value="'.(int)$row['ID'].'">' : '')?><input type="submit" value="Išsaugoti" class="submit"></p>
	</form>
	</fieldset>
<?php
}
?>
</div>
