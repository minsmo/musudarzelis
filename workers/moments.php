<?php if(!defined('DARBUOT') /*|| !DARBUOT not needed*/) exit();
//TODO: ideal: write text and upload in the same time
//TODO: all photos
?>
<h1>Vaiko veikla (galima pridėti nuotraukas, darbelius...)</h1>
<div id="content">
<?php
$width = 150;
$height = 150;
if(isset($_POST['save'])) {
	db_query("INSERT INTO `".DB_moment_activity."` SET 
		`kindergarten_id`=".DB_ID.",
		`date`='".db_fix($_POST['date'])."',
		`title`='".db_fix($_POST['title'])."',
		`description`='".db_fix($_POST['description'])."',
		`visibility`='".(int)$_POST['visibility']."',
		`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
	$moment_activity_id = mysqli_insert_id($db_link);
  	
	db_query("INSERT INTO `".DB_moment_activity_links."` SET 
		`moment_activity_id`='".$moment_activity_id."',
		`link_type`='group_id',
		`value`='".(int)$_POST['group_id']."',
		`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
	
  	if($_POST['visibility'] == moments_visibility::parents) {
		if(isset($_POST['kid_id'])) foreach($_POST['kid_id'] as $kid_id => $dumb)
			db_query("INSERT INTO `".DB_moment_activity_links."` SET 
				`moment_activity_id`='".$moment_activity_id."',
				`link_type`='kid_id',
				`value`='".(int)$kid_id."',
				`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
	}
	header("Location: ?saved&show=".$moment_activity_id);
}
if(isset($_GET['saved']))
	msgBox('OK', 'Veikla išsaugota.');

if(isset($_POST['update'])) {
	$moment_activity_id = (int)$_POST['update'];
	db_query("UPDATE `".DB_moment_activity."` SET 
		`date`='".db_fix($_POST['date'])."',
		`title`='".db_fix($_POST['title'])."',
		`description`='".db_fix($_POST['description'])."',
		`visibility`='".(int)$_POST['visibility']."',
		`updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."'
		WHERE `kindergarten_id`=".DB_ID." AND `ID`=".$moment_activity_id);
	$result = db_query("SELECT * FROM `".DB_moment_activity."` WHERE `kindergarten_id`=".DB_ID." AND `ID`=".$moment_activity_id);
	//echo mysqli_affected_rows($db_link);
	if(mysqli_num_rows($result) == 1) {
		$result = db_query("SELECT * FROM `".DB_moment_activity_links."`
			WHERE `moment_activity_id`='".$moment_activity_id."' AND (`link_type`='group_id' OR `link_type`='kid_id')");
		$groups = [];
		$kids = [];
		while ($row = mysqli_fetch_assoc($result))
			if($row['link_type'] == 'group_id')
				$groups[$row['value']] = '';
			else//kid_id
				$kids[$row['value']] = '';
			
  		if(!isset($groups[$_POST['group_id']])) {
  			//db_query("DELETE FROM `".DB_moment_activity_links."` WHERE `moment_activity_id`='".$moment_activity_id."' AND `link_type`='group_id'");
			db_query("UPDATE `".DB_moment_activity_links."` SET 
				`value`='".(int)$_POST['group_id']."',
				`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'
				WHERE `moment_activity_id`='".$moment_activity_id."' AND `link_type`='group_id'");
			
		}
		if($_POST['visibility'] == moments_visibility::parents) {
			$removed = [];
			foreach($kids as $id => $dumb)
				if(!isset($_POST['kid_id'][$id]))
					$removed[] = $id;
			if(!empty($removed))
				db_query("DELETE FROM `".DB_moment_activity_links."` WHERE 
					`moment_activity_id`='".$moment_activity_id."' AND
					`link_type`='kid_id' AND
					`value` IN (".implode(',', $removed).")");
			if(isset($_POST['kid_id'])) foreach($_POST['kid_id'] as $kid_id => $dumb) if(!isset($kids[$kid_id])) {
				db_query("INSERT INTO `".DB_moment_activity_links."` SET 
					`moment_activity_id`='".$moment_activity_id."',
					`link_type`='kid_id',
					`value`='".(int)$kid_id."',
					`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
					}
		} 
	}
	msgBox('OK', 'Veikla atnaujinta.');
}


//
if(isset($_POST['newFoto'])) {
	function upcount_name_callback($matches) {
		$index = isset($matches[1]) ? ((int)$matches[1]) + 1 : 1;
		$ext = isset($matches[2]) ? $matches[2] : '';
		return '_('.$index.')'.$ext;
	}
	function upcount_name($name) {
		return preg_replace_callback(
			'/(?:(?:_\(([\d]+)\))?(\.[^.]+))?$/',
			'upcount_name_callback',
			$name,
			1
		);
	}
	
	if(is_uploaded_file($_FILES['file']['tmp_name'])) {
		//if(verify_image($_FILES['file']['tmp_name'])) {
			$photo = $_FILES['file'];
			$photo_ext = strrchr($photo['name'], ".");
			$file_type = ['.jpg', '.jpeg', '.png', '.gif'];
			//if($photo['size'] >= 1024*1024*5) {
			//	echo "<div class=\"red center\">Klaida: failas gali užimti iki 2–5 MB</div>";
			//} else
			if(!in_array(strtolower($photo_ext), $file_type)) {
				echo "<div class=\"red center\">Neleidžiama. Failo tipas turi būti iš šių failų tipų: ".implode(', ', $file_type)."</div>";
			} else {
				$photo_db_name = date('Y-m')."/".str_replace($photo_ext, '.jpg', $photo['name']);
				$photo_new_name = UPLOAD_DIR.'foto/'.$photo_db_name;
				$do_save = true;
				if(!is_dir(UPLOAD_DIR."foto"/*gal prideti "/" gale? */))
					if(!mkdir(UPLOAD_DIR."foto", 0777)) {
						msgBox('ERROR', 'Nepavyko sukurti katalogo');
						$do_save = false;
					}
				if(!is_dir(UPLOAD_DIR.'foto/'.date('Y-m')/*gal prideti "/" gale? */))
					if(!mkdir(UPLOAD_DIR.'foto/'.date('Y-m'), 0777)) {
						msgBox('ERROR', 'Nepavyko sukurti katalogo');
						$do_save = false;
					}
				while(is_file($photo_new_name))
					$photo_new_name = upcount_name($photo_new_name);
				
				
				include './libs/SimpleImage.php';
				try {
					$img = new abeautifulsite\SimpleImage($photo['tmp_name']);
					$convStart = microtime(true);
					$img->auto_orient()->best_fit(1080, 1080)->save($photo_new_name, 82);
					$toTime = round(microtime(true)-$convStart, 2);
					
					$convStart = microtime(true);
					$thumb = str_replace('.jpg', '_'.$width.'x'.$height.'.jpg', $photo_new_name);
					$img->auto_orient()->best_fit($width, $height)->save($thumb, 82);
					$thumbTime = round(microtime(true)-$convStart, 2);
				} catch(Exception $e) {
					loga('Error: ' . $e->getMessage());
				}
				if($do_save) {
					//group_id, kid_id, achievement, achievement_cat_id, keyword, event
					db_query("INSERT INTO `".DB_moment_photos."` SET 
						-- `kindergarten_id`=".DB_ID.",
						`moment_activity_id`='".(int)$_POST['newFoto']."',
						`url`='".db_fix(date('Y-m').'/'.basename($photo_db_name))."',
						`orgSize`='".$photo['size']/(1024*1024)."',
						`toSize`='".filesize($photo_new_name)/(1024*1024)."',
						`toImgSize`='1080x1080',
						`toTime`='".$toTime."',
						`thumbTime`='".$thumbTime."',
						`title`='".db_fix($_POST['title'])."',
						`visibility`='".(int)$_POST['visibility']."',
						`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
					$moment_photo_id = mysqli_insert_id($db_link);
				  	
					db_query("INSERT INTO `".DB_moment_photos_links."` SET 
						`moment_photo_id`='".$moment_photo_id."',
						`link_type`='group_id',
						`value`='".(int)$_POST['group_id']."',
						`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
					if(isset($_POST['event']))
						db_query("INSERT INTO `".DB_moment_photos_links."` SET 
							`moment_photo_id`='".$moment_photo_id."',
							`link_type`='event', `value`='',
							`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
					if(isset($_POST['achievement']))
						db_query("INSERT INTO `".DB_moment_photos_links."` SET 
							`moment_photo_id`='".$moment_photo_id."',
							`link_type`='achievement', `value`='',
							`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
		
					if(isset($_POST['achievement_cat_id'])) foreach($_POST['achievement_cat_id'] as $achievement_cat_id => $dumb)
						db_query("INSERT INTO `".DB_moment_photos_links."` SET 
							`moment_photo_id`='".$moment_photo_id."',
							`link_type`='achievement_cat_id',
							`value`='".(int)$achievement_cat_id."',
							`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
				  	if(isset($_POST['kid_id'])) foreach($_POST['kid_id'] as $kid_id => $dumb)
						db_query("INSERT INTO `".DB_moment_photos_links."` SET 
							`moment_photo_id`='".$moment_photo_id."',
							`link_type`='kid_id',
							`value`='".(int)$kid_id."',
							`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
					msgBox('OK', 'Fotografija išsaugota.');
				}
			}
		//} else echo "<div class=\"red center\">Failas nėra paveiksliukas.</div>";
	} else msgBox('ERROR', 'Nėra failo.');
}
if(isset($_POST['updateFoto'])) {
	$result = db_query("SELECT * FROM `".DB_moment_activity."` WHERE `kindergarten_id`=".DB_ID." AND `ID`=".(int)$_GET['show']);
	if(mysqli_num_rows($result) == 1) {
		$moment_photo_id = (int)$_POST['updateFoto'];
		db_query("UPDATE `".DB_moment_photos."` SET
		`title`='".db_fix($_POST['title'])."', `visibility`='".(int)$_POST['visibility']."',
		`updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."'
		WHERE `moment_activity_id`='".(int)$_GET['show']."' AND `ID`=".$moment_photo_id);
		$result = db_query("SELECT * FROM `".DB_moment_photos."` WHERE `ID`=".$moment_photo_id);
		//echo mysqli_affected_rows($db_link);
		if(mysqli_num_rows($result) == 1) {//['kindergarten_id'] == DB_ID
			$result = db_query("SELECT * FROM `".DB_moment_photos_links."` WHERE `moment_photo_id`='".$moment_photo_id."'");
			$groups = [];
			$kids = [];
			$achievements = [];
			while ($row = mysqli_fetch_assoc($result))
				if($row['link_type'] == 'group_id')
					$groups[$row['value']] = '';
				elseif($row['link_type'] == 'kid_id')
					$kids[$row['value']] = '';
				elseif($row['link_type'] == 'achievement_cat_id')
					$achievements[$row['value']] = '';
				elseif($row['link_type'] == 'event')
					$event = '';
				elseif($row['link_type'] == 'achievement')
					$achievement = '';

	  		if(!isset($groups[$_POST['group_id']]))
				db_query("UPDATE `".DB_moment_photos_links."` SET 
					`value`='".(int)$_POST['group_id']."', `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'
					WHERE `moment_photo_id`='".$moment_photo_id."' AND `link_type`='group_id'");
			if(!isset($event) && isset($_POST['event']))
				db_query("INSERT INTO `".DB_moment_photos_links."` SET `moment_photo_id`='".$moment_photo_id."',
					`link_type`='event', `value`='', `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
			if(isset($event) && !isset($_POST['event']))
				db_query("DELETE FROM `".DB_moment_photos_links."` WHERE `link_type`='event' AND `moment_photo_id`='".$moment_photo_id."'");
			if(!isset($achievement) && isset($_POST['achievement']))
				db_query("INSERT INTO `".DB_moment_photos_links."` SET `moment_photo_id`='".$moment_photo_id."',
					`link_type`='achievement', `value`='', `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
			if(isset($achievement) && !isset($_POST['achievement']))
				db_query("DELETE FROM `".DB_moment_photos_links."` WHERE `link_type`='achievement' AND `moment_photo_id`='".$moment_photo_id."'");
		
			$remove = [];
			if(isset($kids)) foreach($kids as $id => $dumb)
				if(!isset($_POST['kid_id'][$id]))
					$remove[] = $id;
			if(!empty($remove))
				db_query("DELETE FROM `".DB_moment_photos_links."` WHERE 
					`moment_photo_id`='".$moment_photo_id."' AND
					`link_type`='kid_id' AND
					`value` IN (".implode(',', $remove).")");
			if(isset($_POST['kid_id'])) foreach($_POST['kid_id'] as $kid_id => $dumb) if(!isset($kids[$kid_id])) {
				db_query("INSERT INTO `".DB_moment_photos_links."` SET 
					`moment_photo_id`='".$moment_photo_id."',
					`link_type`='kid_id',
					`value`='".(int)$kid_id."',
					`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
			}
			$remove = [];
			if(isset($achievements)) foreach($achievements as $id => $dumb)
				if(!isset($_POST['achievement_cat_id'][$id]))
					$remove[] = $id;
			if(!empty($remove))
				db_query("DELETE FROM `".DB_moment_photos_links."` WHERE 
					`moment_photo_id`='".$moment_photo_id."' AND
					`link_type`='achievement_cat_id' AND
					`value` IN (".implode(',', $remove).")");
			if(isset($_POST['achievement_cat_id'])) foreach($_POST['achievement_cat_id'] as $achievement_cat_id => $dumb) if(!isset($kids[$achievement_cat_id])) {
				db_query("INSERT INTO `".DB_moment_photos_links."` SET 
					`moment_photo_id`='".$moment_photo_id."',
					`link_type`='achievement_cat_id',
					`value`='".(int)$achievement_cat_id."',
					`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
			}
		}
		msgBox('OK', 'Foto informacija atnaujinta.');
	}
}


if(isset($_GET['delete'])) {
	$ok = true;
	if(!ADMIN)
		$ok = mysqli_num_rows(db_query("SELECT * FROM `".DB_moment_activity."` WHERE `kindergarten_id`=".DB_ID." AND `createdByEmployeeId`='".DARB_ID."' AND `ID`=".(int)$_GET['delete'].""));
	if($ok) {
		db_query("UPDATE `".DB_moment_activity."` SET `isDeleted`=1 WHERE `kindergarten_id`=".DB_ID." AND `ID`=".(int)$_GET['delete']);
		msgBox('OK', 'Veikla išjungta.');
	}
}
if(isset($_GET['deleteImg'])) {
	$ok = true;
	if(!ADMIN)
		$ok = mysqli_num_rows(db_query("SELECT * FROM `".DB_moment_photos."` JOIN `".DB_moment_activity."` ON 
				`".DB_moment_activity."`.`ID`=`".DB_moment_photos."`.`moment_activity_id` AND
				`".DB_moment_photos."`.`ID`=".(int)$_GET['deleteImg']." AND `kindergarten_id`=".DB_ID." AND `".DB_moment_activity."`.`isDeleted`=0 AND `".DB_moment_photos."`.`isDeleted`=0 AND `".DB_moment_photos."`.`createdByEmployeeId`='".DARB_ID."'"));
	if($ok) {
		db_query("UPDATE `".DB_moment_photos."` SET `isDeleted`=1 WHERE `ID`=".(int)$_GET['deleteImg']);
		msgBox('OK', 'Fotografija išjungta.');
	}
}
?>
<a href="?new" class="no-print fast-action fast-action-add">Nauja veikla</a><?=ui_print()?>
<form action="" method="get">
	<!-- Paieška:
	Vaikas: -->
	<div class="sel" style="/*float: left;*/ margin-right: 5px;"><select name="kid_id">
	<option value=""<?=(empty($_GET['kid_id']) ? ' selected="selected" class="selected">'.$selectedMark : '>')?>Visi vaikai</option>
	<?php
	foreach(getValidSelectedKidName() as $id => $val)
		echo "<option value=\"".$id."\"".(isset($_GET['kid_id']) && $_GET['kid_id'] == $id ? ' selected="selected" class="selected">'.$selectedMark : '>').$val."</option>";
	?>
	</select></div>
	<div class="sel" style="/*float: left;*/ margin-right: 5px;"><select name="achievement_cat_id">
	<option value=""<?=(empty($_GET['kid_id']) ? ' selected="selected" class="selected">'.$selectedMark : '>')?>Visos pasiekimų sritys</option>
	<?php
	foreach($achievements_areas as $id => $val)
		echo "<option value=\"".$id."\"".(isset($_GET['achievement_cat_id']) && $_GET['achievement_cat_id'] == $id ? ' selected="selected" class="selected">'.$selectedMark : '>').$val."</option>";
	?>
	</select></div>
	<label>Renginys <input type="checkbox" name="event" value="1"<?=(!empty($_GET['event']) ? 'checked="checked"' : '')?>></label>
	<label>Vaiko pasiekimas <input type="checkbox" name="achievement" value="1"<?=(!empty($_GET['achievement']) ? 'checked="checked"' : '')?>></label>

	<input type="submit" name="list" value="Ieškoti" class="filter">
</form>

<script type="text/javascript" src="/libs/jquery.checkboxes.min.js"></script>
<script src="/workers/moments.js"></script>
<?php






if(isset($_GET['new']) || isset($_GET['edit'])) {
	if(isset($_GET['edit'])) {
		if(ADMIN)
			$result = db_query("SELECT `".DB_moment_activity."`.*, `".DB_moment_activity_links."`.`link_type`, `".DB_moment_activity_links."`.`value`
			FROM `".DB_moment_activity."` LEFT JOIN `".DB_moment_activity_links."` ON `".DB_moment_activity."`.`ID`=`".DB_moment_activity_links."`.`moment_activity_id` AND `link_type`='group_id'
			WHERE `kindergarten_id`=".DB_ID." AND `".DB_moment_activity."`.`ID`=".(int)$_GET['edit']." AND `".DB_moment_activity."`.`isDeleted`=0
			ORDER BY `date` DESC");
		else
			$result = db_query("SELECT `".DB_moment_activity."`.*, `".DB_moment_activity_links."`.`link_type`, `".DB_moment_activity_links."`.`value`
			FROM `".DB_moment_activity."` LEFT JOIN `".DB_moment_activity_links."` ON `".DB_moment_activity."`.`ID`=`".DB_moment_activity_links."`.`moment_activity_id` AND `link_type`='group_id' AND `value`='".GROUP_ID."'
			WHERE `kindergarten_id`=".DB_ID." AND (`visibility`=0 OR `visibility`<>0 AND `value`=".GROUP_ID.") AND `".DB_moment_activity."`.`ID`=".(int)$_GET['edit']." AND `".DB_moment_activity."`.`isDeleted`=0
			ORDER BY `date` DESC");
		$edit = mysqli_fetch_assoc($result);
		
		$result = db_query("SELECT * FROM `".DB_moment_activity_links."`
			WHERE `moment_activity_id`='".(int)$_GET['edit']."' AND (`link_type`='group_id' OR `link_type`='kid_id')");
		$groups = [];
		$kids = [];
		while ($row = mysqli_fetch_assoc($result))
			if($row['link_type'] == 'group_id')
				$groups[$row['value']] = '';
			else//kid_id
				$kids[$row['value']] = '';
	}
	?>
	<fieldset id="document-form">
	<legend>Nauja veikla</legend>
	<form <?php /*echo (isset($_GET['edit']) ? '' : 'enctype="multipart/form-data" ');*/ ?>method="post" class="not-saved-reminder">
		<p>Data <input class="datepicker" type="text" name="date" value="<?=(isset($edit['date']) ? $edit['date'] : date('Y-m-d'))?>"></p>
		<div><label>Veiklą matys <div class="sel"><select name="visibility" id="visibility">
			<?php
			foreach($moments_visibility as $id => $val)
				echo "<option value=\"".(int)$id."\"".(isset($_GET['new']) && $id == 1 || isset($edit['visibility']) && $edit['visibility'] == $id ? ' selected="selected"' : '').">".filterText($val)."</option>";
			?>
			</select></div>
			</label></div>
		<div id="kids-group">Vaikų grupė <div class="sel"><select name="group_id">
			<?php 
			foreach(getAllowedGroups() as $ID => $title)
				echo "<option value=\"".$ID."\"".(isset($_GET['new']) && GROUP_ID == $ID || isset($_GET['edit']) && isset($groups[$ID]) ? ' selected="selected"' : '').">".$title."</option>";
			?>
			</select></div></div>
		<p><label><strong>Veiklos pavadinimas</strong> (geriausia viena trumpa eilute)<span class="required">*</span><br> <textarea name="title" required="required"><?=(isset($edit['title']) ? filterText($edit['title']) : '')?></textarea></label></p>
		<p><label>Aprašymas<br><textarea name="description"><?=(isset($edit['description']) ? filterText($edit['description']) : '')?></textarea></label></p>
		
		
		<table id="kids">
			<tr><th>Vaikas</th><th>Matys jo tėvai</th></tr>
		<?php
		$result = db_query($get_kids_sql);
		while($row = mysqli_fetch_assoc($result))
			echo "\t<tr><td>".filterText(getName($row['vardas'], $row['pavarde']))."</td>
						<td><input type=\"checkbox\" name=\"kid_id[".$row['parent_kid_id']."]\" value=\"1\"".(isset($_GET['edit']) && isset($kids[$row['parent_kid_id']]) ? ' checked="checked"' : '')."></td></tr>";
		?>
		</table>
		<?=(!isset($_GET['edit']) ? 'Išsaugojus veiklą prie jos galėsite pridėti nuotraukas' : '')?>
		<p><?=(isset($_GET['edit']) ? '<input type="hidden" name="update" value="'.(int)$_GET['edit'].'">' : '')?><input type="submit"<?=(isset($_GET['new']) ? ' name="save"' : '')?> value="Išsaugoti" class="submit"></p>
	</form>
	</fieldset>
	<?php
}




if(isset($_GET['newFoto'])) {
	/*if(isset($_GET['edit'])) {
		$result = db_query("SELECT `".DB_moment_photos."`.*, `".DB_moment_photos_links."`.`link_type`, `".DB_moment_photos_links."`.`value`
			FROM `".DB_moment_photos."` LEFT JOIN `".DB_moment_photos_links."` ON `".DB_moment_photos."`.`ID`=`".DB_moment_activity_links."`.`moment_activity_id` AND `link_type`='group_id' AND `value`='".GROUP_ID."'
			WHERE `kindergarten_id`=".DB_ID." AND (`visibility`=0 OR `visibility`<>0 AND `value`=".GROUP_ID.") AND `".DB_moment_activity."`.`ID`=".(int)$_GET['edit']."
			ORDER BY `date` DESC");
		$edit = mysqli_fetch_assoc($result);
		
		$result = db_query("SELECT * FROM `".DB_moment_activity_links."`
			WHERE `moment_activity_id`='".(int)$_GET['edit']."' AND (`link_type`='group_id' OR `link_type`='kid_id')");
		$groups = [];
		$kids = [];
		while ($row = mysqli_fetch_assoc($result))
			if($row['link_type'] == 'group_id')
				$groups[$row['value']] = '';
			else//kid_id
				$kids[$row['value']] = '';
	}*/
	
	
	//Restrictions
	/*$result = db_query("SELECT `".DB_moment_activity."`.*, `".DB_moment_activity_links."`.`link_type`, `".DB_moment_activity_links."`.`value`
			FROM `".DB_moment_activity."` LEFT JOIN `".DB_moment_activity_links."` ON `".DB_moment_activity."`.`ID`=`".DB_moment_activity_links."`.`moment_activity_id` AND `link_type`='group_id' AND `value`='".GROUP_ID."'
			WHERE `kindergarten_id`=".DB_ID." AND (`visibility`=0 OR `visibility`<>0 AND `value`=".GROUP_ID.") AND `".DB_moment_activity."`.`ID`=".(int)$_GET['newFoto']."
			ORDER BY `date` DESC");
	$edit = mysqli_fetch_assoc($result);
		
	$result = db_query("SELECT * FROM `".DB_moment_activity_links."`
			WHERE `moment_activity_id`='".(int)$_GET['edit']."' AND (`link_type`='group_id' OR `link_type`='kid_id')");
	$groups = [];
	$kids = [];
	while ($row = mysqli_fetch_assoc($result))
		if($row['link_type'] == 'group_id')
			$groups[$row['value']] = '';
		else//kid_id
			$kids[$row['value']] = '';*/
	if(ADMIN)
		$result = db_query("SELECT `".DB_moment_activity."`.*, `".DB_moment_activity_links."`.`link_type`, `".DB_moment_activity_links."`.`value`
		FROM `".DB_moment_activity."` LEFT JOIN `".DB_moment_activity_links."` ON `".DB_moment_activity."`.`ID`=`".DB_moment_activity_links."`.`moment_activity_id` AND `link_type`='group_id'
		WHERE `kindergarten_id`=".DB_ID." AND `".DB_moment_activity."`.`ID`=".(int)$_GET['newFoto']." AND `".DB_moment_activity."`.`isDeleted`=0
		ORDER BY `date` DESC");
	else
		$result = db_query("SELECT `".DB_moment_activity."`.*, `".DB_moment_activity_links."`.`link_type`, `".DB_moment_activity_links."`.`value`
		FROM `".DB_moment_activity."` LEFT JOIN `".DB_moment_activity_links."` ON `".DB_moment_activity."`.`ID`=`".DB_moment_activity_links."`.`moment_activity_id` AND `link_type`='group_id' AND `value`='".GROUP_ID."'
		WHERE `kindergarten_id`=".DB_ID." AND (`visibility`=0 OR `visibility`<>0 AND `value`=".GROUP_ID.") AND `".DB_moment_activity."`.`ID`=".(int)$_GET['newFoto']." AND `".DB_moment_activity."`.`isDeleted`=0
		ORDER BY `date` DESC");
	$show = mysqli_fetch_assoc($result);
	if(!empty($show['title'])) {
	?>
	<script type="text/javascript" src="/libs/jquery.checkboxes.min.js"></script>
	<script type="text/javascript">
	$(function($) {
		$('#achievements').checkboxes('range', true);
		$('#kids-list').checkboxes('range', true);
	});
	</script>
	<fieldset id="document-form">
	<legend>Nauja nuotrauka (veiklai „<?=$show['title']?>“)</legend>
	<form enctype="multipart/form-data" method="post" class="not-saved-reminder" onsubmit="return checkUpload()">
		<p class="notice"><?php
		$result = db_query("SELECT `0crm_kindergartens_stats`.`kids` FROM `0crm_kindergartens` JOIN `0crm_kindergartens_stats` ON `0crm_kindergartens_stats`.`code`=`0crm_kindergartens`.`code` AND `prefix`='".DB_PREFIX."' LIMIT 1");
		$cnt = mysqli_fetch_assoc($result)['kids'];
		$result = db_query("SELECT COUNT(*) as `cnt` FROM `".DB_moment_photos."` JOIN `".DB_moment_activity."` ON `".DB_moment_activity."`.`ID`=`".DB_moment_photos."`.`moment_activity_id` AND `kindergarten_id`=".DB_ID);
		$current_cnt = mysqli_fetch_assoc($result)['cnt'];
		echo 'Nemokamai talpinti galite '.($cnt*30).' nuotraukų. Dabar patalpinta '.$current_cnt.' nuotraukų.';
		
		
		?></p>
		<p><label>Paveiksliukas<span class="required">*</span>: <input required="required" type="file" accept="image/*" style="width: 300px;" name="file" id="file"></label></p><?php //http://www.w3schools.com/tags/att_input_accept.asp ?>
		<p><label>Pavadinimas<br><textarea name="title"><?=(isset($edit['title']) ? filterText($edit['title']) : '')?></textarea></label></p>
		<div><label>Matomumas<span class="required">*</span> <div class="sel"><select name="visibility" id="visibility">
			<?php
			foreach($moments_visibility as $id => $val)
				echo "<option value=\"".(int)$id."\"".(isset($_GET['newFoto']) && $id == 1 || isset($edit['visibility']) && $edit['visibility'] == $id ? ' selected="selected"' : '').">".filterText($val)."</option>";
			?>
			</select></div>
			</label></div>
		<div>Vaikų grupė <div class="sel"><select name="group_id">
		<?php 
		foreach(getAllowedGroups() as $ID => $title)
			echo "<option value=\"".$ID."\"".(isset($_GET['newFoto']) && GROUP_ID == $ID /*|| isset($_GET['editFoto']) && isset($groups[$ID])*/ ? ' selected="selected"' : '').">".$title."</option>";
		?>
		</select></div></div>
		<strong>Paiešką/filtravimą palengvinantys žymėjimai:</strong>
		<p>Renginys <input type="checkbox" name="event" value="1"<?=(isset($_GET['editFoto']) && isset($edit['event']) ? ' checked="checked"' : '')?>></p>
		<p>Vaiko pasiekimas <input type="checkbox" name="achievement" value="1"<?=(isset($_GET['editFoto']) && isset($edit['achievement']) ? ' checked="checked"' : '')?>></p>
		<?php
		//TODO: keyword
		echo '<div style="float: left;"><table id="achievements"><tr><th>Ikimokyklinio pasiekimų aprašo sritis</th><th>Yra</th></tr>';
		foreach($achievements_areas as $id => $val)
			echo "\t<tr><td>".filterText($val)."</td>
						<td><input type=\"checkbox\" name=\"achievement_cat_id[".$id."]\" value=\"1\"".(isset($_GET['editFoto']) && isset($edit['achievement_cat_id'][$id]) ? ' checked="checked"' : '')."></td></tr>";
		echo '</table></div>';

		echo '<div style="float: left; margin-left: 10px;"><table id="kids-list"><tr><th>Vaikas</th><th>Yra nuotraukoje</th></tr>';//Jei „Matomumas“ parinkote „Tėvai“, matys tik šio vaiko tėvai:
		$result = db_query($get_kids_sql);
		while($row = mysqli_fetch_assoc($result))
			echo "\t<tr><td>".filterText(getName($row['vardas'], $row['pavarde']))."</td>
						<td><input type=\"checkbox\" name=\"kid_id[".$row['parent_kid_id']."]\" value=\"1\"".(isset($_GET['editFoto']) && isset($kids[$row['parent_kid_id']]) ? ' checked="checked"' : '')."></td></tr>";
		echo '</table></div>';
		?>
		<p style="clear: left;"><?=(isset($_GET['editFoto']) ? '<input type="hidden" name="updateFoto" value="'.(int)$_GET['editFoto'].'">' : '<input type="hidden" name="newFoto" value="'.(int)$_GET['newFoto'].'">')?><input type="submit" value="Išsaugoti" class="submit" onclick="document.body.style.cursor='wait';"></p>
	<script>
	function checkUpload() {
		var file = document.getElementById('file').files[0];
		if(file && file.size < 15*1024*1024) { // 2 MB (this size is in bytes)
			//Submit form		
		} else {
			alert('Failas didesnis negu 15 MB, todėl jo įkelti negalima. Jei tai paveiksliukas išsaugokite jį jpg formatu arba sumažinkite jo raišką, kad jo užimamas dydis sumažėtų. Tada bandykite dar kartą jau su mažesniu failu.');
			//Prevent default and display error
			//evt.preventDefault();
			return false;
		}
	}
	</script>
	</form>
	</fieldset>
	<?php
	}
}
?>




<?php if(!( isset($_GET['new']) || isset($_GET['show']) || isset($_GET['newFoto']) || isset($_GET['list']) )) { ?>
	<table>
		<tr>
			<th>Data ir pavadinimas</th>
			<th>Sukūrė</th>
			<th>Matomumas</th>
			<th class="no-print">Veiksmai</th>
		</tr>
		<?php	
		//JOIN 
		//(`".DB_moment_activity."`.`visibility`=1 AND `".DB_moment_activity."`.`ID`=`".DB_moment_activity_links."`.`moment_activity_id` AND  OR
		// `".DB_moment_activity."`.`visibility`=2 AND `".DB_moment_activity."`.`ID`=`".DB_moment_activity_links."`.`moment_activity_id`)
		if(ADMIN)
			$result = db_query("SELECT `".DB_moment_activity."`.*, `".DB_moment_activity_links."`.`link_type`, `".DB_moment_activity_links."`.`value`
			FROM `".DB_moment_activity."` LEFT JOIN `".DB_moment_activity_links."` ON `".DB_moment_activity."`.`ID`=`".DB_moment_activity_links."`.`moment_activity_id` AND `".DB_moment_activity_links."`.`link_type`='group_id'
			WHERE `kindergarten_id`=".DB_ID." AND `".DB_moment_activity."`.`isDeleted`=0
			ORDER BY `date` DESC");
		else
			$result = db_query("SELECT `".DB_moment_activity."`.*, `".DB_moment_activity_links."`.`link_type`, `".DB_moment_activity_links."`.`value`
			FROM `".DB_moment_activity."` LEFT JOIN `".DB_moment_activity_links."` ON `".DB_moment_activity."`.`ID`=`".DB_moment_activity_links."`.`moment_activity_id` AND `link_type`='group_id' AND `value`='".GROUP_ID."'
			WHERE `kindergarten_id`=".DB_ID." AND (`visibility`=0 OR `visibility`<>0 AND `value`=".GROUP_ID.") AND `".DB_moment_activity."`.`isDeleted`=0
			ORDER BY `date` DESC");
		while ($row = mysqli_fetch_assoc($result)) {
			echo '<tr'.(isset($_GET['edit']) && $_GET['edit'] == $row['ID'] ? ' class="opened-row"' : '').'>
			<td>'.$row['date']." <strong><a href=\"?show=".$row['ID']."\">".filterText($row['title'])."</a></strong></td>
			<td>".getAllEmployees($row['createdByEmployeeId'])."</td>
			<td>".$moments_visibility[$row['visibility']].($row['link_type'] == 'group_id' ? ' ('.getAllowedGroups()[$row['value']].')' : '')."</td>
			<td class=\"no-print\"><a href=\"?show=".$row['ID']."\">Peržiūrėti</a> <a href=\"?newFoto=".$row['ID']."\">Pridėti foto</a> <a href=\"?edit=".$row['ID']."\">Keisti</a>".(ADMIN || $row['createdByEmployeeId'] == DARB_ID ? ' <a href="?delete='.$row['ID'].'" onclick="return confirm(\'Ar tikrai norite ištrinti veiklą?\');">Trinti</a>' : '')."</td></tr>\n";
		}
		?>
	</table>
<?php }





if(isset($_GET['show'])) {
	if(ADMIN)
		$result = db_query("SELECT `".DB_moment_activity."`.*, `".DB_moment_activity_links."`.`link_type`, `".DB_moment_activity_links."`.`value`
		FROM `".DB_moment_activity."` LEFT JOIN `".DB_moment_activity_links."` ON `".DB_moment_activity."`.`ID`=`".DB_moment_activity_links."`.`moment_activity_id` AND `link_type`='group_id'
		 WHERE `kindergarten_id`=".DB_ID." AND `".DB_moment_activity."`.`ID`=".(int)$_GET['show']." AND `".DB_moment_activity."`.`isDeleted`=0
		 ORDER BY `date` DESC");
	else
		$result = db_query("SELECT `".DB_moment_activity."`.*, `".DB_moment_activity_links."`.`link_type`, `".DB_moment_activity_links."`.`value`
		FROM `".DB_moment_activity."` LEFT JOIN `".DB_moment_activity_links."` ON `".DB_moment_activity."`.`ID`=`".DB_moment_activity_links."`.`moment_activity_id` AND `link_type`='group_id' AND `value`='".GROUP_ID."'
		 WHERE `kindergarten_id`=".DB_ID." AND (`visibility`=0 OR `visibility`<>0 AND `value`=".GROUP_ID.") AND `".DB_moment_activity."`.`ID`=".(int)$_GET['show']." AND `".DB_moment_activity."`.`isDeleted`=0
		 ORDER BY `date` DESC");
	$show = mysqli_fetch_assoc($result);
	if(!empty($show['title'])) {
		$kids = [];
		$all_kids_newest_valid_info = "SELECT cr.* FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `isDeleted`=0 AND `archyvas`=0 GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from` WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 ORDER BY ".orderName('cr');
		$result = db_query($all_kids_newest_valid_info);
		while($row = mysqli_fetch_assoc($result))
			$kids[$row['parent_kid_id']] = filterText(getName($row['vardas'], $row['pavarde']));
	
		$result = db_query("SELECT * FROM `".DB_moment_activity_links."`
			WHERE `moment_activity_id`='".(int)$_GET['show']."'");
		$activity_links = [];
		while ($row = mysqli_fetch_assoc($result))
			$activity_links[$row['link_type']][] = $row['value'];
		
		echo "<h2>".$show['date']." <strong><a href=\"?show=".$show['ID']."\">".filterText($show['title'])."</a></strong></h2>";
		echo nl2br(filterText($show['description']));
		
		echo '<div>Veiklą matys: '.$moments_visibility[$show['visibility']];
		echo '<br>Grupė: ';
		foreach($activity_links as $key => $val) {
			if($key == 'group_id') {
				foreach($val as $value)
					echo getAllGroups()[$value].' ';
			} /*elseif($key == 'kid_id') {
				foreach($val as $value)
					echo [$value];
			}*/
		}
		echo '</div>';
		if(isset($activity_links['kid_id']) && count($activity_links['kid_id'])) {
			$kids_marks = [];
			if(isset($activity_links['kid_id'])) {
				foreach($activity_links['kid_id'] as $val)
					$kids_marks[] = $kids[$val];
			}
			echo '<span>Priskirti vaikai ('.count($activity_links['kid_id']).'): '.implode(', ', $kids_marks).'.</span>';
		}
		?>
		<link rel="stylesheet" href="/libs/fancyBox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen">
		<script type="text/javascript" src="/libs/fancyBox/jquery.fancybox.pack.js?v=2.1.5"></script>
		<script type="text/javascript">
		$(function() {
			$(".fancybox").fancybox({
				openEffect	: 'none',
				closeEffect	: 'none',
				tpl: {
					error	 : '<p class="fancybox-error">Užklaustas turinys negali būti užkrautas.<br>Prašome bandyti dar kartą vėliau.</p>',
					closeBtn : '<a title="Uždaryti" class="fancybox-item fancybox-close" href="javascript:;"></a>',
					next	 : '<a title="Kitas" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
					prev	 : '<a title="Ankstesnis" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
				}

			});
		});
		</script>
		<?php
		if(ADMIN)
			$result = db_query("SELECT `".DB_moment_photos."`.* -- , `".DB_moment_photos_links."`.`link_type`, `".DB_moment_photos_links."`.`value`
			FROM `".DB_moment_photos."` JOIN `".DB_moment_activity."` ON `".DB_moment_activity."`.`ID`=`".DB_moment_photos."`.`moment_activity_id` AND `kindergarten_id`=".DB_ID." AND `".DB_moment_activity."`.`ID`=".(int)$_GET['show']." AND `".DB_moment_activity."`.`isDeleted`=0 AND `".DB_moment_photos."`.`isDeleted`=0
			LEFT JOIN `".DB_moment_photos_links."` ON `".DB_moment_photos."`.`ID`=`".DB_moment_photos_links."`.`moment_photo_id` AND `link_type`='group_id'
			ORDER BY `date` DESC");
		else
			$result = db_query("SELECT `".DB_moment_photos."`.* -- , `".DB_moment_photos_links."`.`link_type`, `".DB_moment_photos_links."`.`value`
			FROM `".DB_moment_photos."` JOIN `".DB_moment_activity."` ON 
			`".DB_moment_activity."`.`ID`=`".DB_moment_photos."`.`moment_activity_id` AND `kindergarten_id`=".DB_ID." AND `".DB_moment_activity."`.`ID`=".(int)$_GET['show']." AND `".DB_moment_activity."`.`isDeleted`=0 AND `".DB_moment_photos."`.`isDeleted`=0
			LEFT JOIN `".DB_moment_photos_links."` ON `".DB_moment_photos."`.`ID`=`".DB_moment_photos_links."`.`moment_photo_id` AND `link_type`='group_id' AND `value`='".GROUP_ID."'
			WHERE `".DB_moment_photos."`.`visibility`=0 OR `".DB_moment_photos."`.`visibility`<>0 AND `value`=".GROUP_ID."
			ORDER BY `date` DESC");//AND `".DB_moment_activity."`.`ID`=".(int)$_GET['show']."
		$rows = [];
		$ids = [];
		while ($row = mysqli_fetch_assoc($result)) {
			$rows[] = $row;
			$ids[] = $row['ID'];
			if(!empty($_GET['editFoto']) && $_GET['editFoto'] == $row['ID'])
				$edit = $row;
		}
		//print_r(implode(',', $ids))
		$photos_links = [];
		if(!empty($ids)) {
			$res = db_query("SELECT * FROM `".DB_moment_photos_links."` WHERE `moment_photo_id` IN (".implode(',', $ids).")");
			while ($row = mysqli_fetch_assoc($res))
				$photos_links[$row['moment_photo_id']][$row['link_type']][] = $row['value'];
		}
		if(isset($_GET['editFoto'])) {
		//TODO
			?>
			<script type="text/javascript" src="/libs/jquery.checkboxes.min.js"></script>
			<script type="text/javascript">
			$(function($) {
				$('#achievements').checkboxes('range', true);
				$('#kids-list').checkboxes('range', true);
			});
			</script>
			<fieldset id="document-form">
			<legend>Keitimas nuotraukos duomenų</legend>
			<form enctype="multipart/form-data" method="post" class="not-saved-reminder">
				<p><label>Pavadinimas<br><textarea name="title"><?=(isset($edit['title']) ? filterText($edit['title']) : '')?></textarea></label></p>
				<div><label>Matomumas<span class="required">*</span> <div class="sel"><select name="visibility" id="visibility">
					<?php
					foreach($moments_visibility as $id => $val)
						echo "<option value=\"".(int)$id."\"".(isset($_GET['newFoto']) && $id == 1 || isset($edit['visibility']) && $edit['visibility'] == $id ? ' selected="selected"' : '').">".filterText($val)."</option>";
					?>
					</select></div>
					</label></div>
				<div>Vaikų grupė <div class="sel"><select name="group_id">
				<?php 
				foreach(getAllowedGroups() as $ID => $title)
					echo '<option value="'.$ID.'"'.(isset($_GET['newFoto']) && GROUP_ID == $ID || isset($_GET['editFoto']) && isset($photos_links[$edit['ID']]['group_id']) && in_array($ID, $photos_links[$edit['ID']]['group_id']) ? ' selected="selected"' : '').">".$title."</option>";
				?>
				</select></div></div>
				<strong>Paiešką/filtravimą palengvinantys žymėjimai:</strong>
				<p>Renginys <input type="checkbox" name="event" value="1"<?=(isset($_GET['editFoto']) && isset($photos_links[$edit['ID']]['event']) ? ' checked="checked"' : '')?>></p>
				<p>Vaiko pasiekimas <input type="checkbox" name="achievement" value="1"<?=(isset($_GET['editFoto']) && isset($photos_links[$edit['ID']]['achievement']) ? ' checked="checked"' : '')?>></p>
				<?php
				//TODO: keyword
				echo '<div style="float: left;"><table id="achievements"><tr><th>Ikimokyklinio pasiekimų aprašo sritis</th><th>Yra</th></tr>';
				foreach($achievements_areas as $id => $val)
					echo "\t<tr><td>".filterText($val)."</td>
								<td><input type=\"checkbox\" name=\"achievement_cat_id[".$id."]\" value=\"1\"".(isset($_GET['editFoto']) && isset($photos_links[$edit['ID']]['achievement_cat_id']) && in_array($id, $photos_links[$edit['ID']]['achievement_cat_id']) ? ' checked="checked"' : '')."></td></tr>";
				echo '</table></div>';

				echo '<div style="float: left; margin-left: 10px;"><table id="kids-list"><tr><th>Vaikas</th><th>Yra nuotraukoje</th></tr>';//Jei „Matomumas“ parinkote „Tėvai“, matys tik šio vaiko tėvai:
				$result = db_query($get_kids_sql);
				while($row = mysqli_fetch_assoc($result))
					echo "\t<tr><td>".filterText(getName($row['vardas'], $row['pavarde']))."</td>
								<td><input type=\"checkbox\" name=\"kid_id[".$row['parent_kid_id']."]\" value=\"1\"".(isset($_GET['editFoto']) && isset($photos_links[$edit['ID']]['kid_id']) && in_array($row['parent_kid_id'], $photos_links[$edit['ID']]['kid_id']) ? ' checked="checked"' : '')."></td></tr>";
				echo '</table></div>';
				?>
				<p style="clear: left;"><?=(isset($_GET['editFoto']) ? '<input type="hidden" name="updateFoto" value="'.(int)$_GET['editFoto'].'">' : '<input type="hidden" name="newFoto" value="'.(int)$_GET['newFoto'].'">')?><input type="submit" value="Išsaugoti" class="submit" onclick="document.body.style.cursor='wait';"></p>
			</form>
			</fieldset>
			<?php
		}
		?>
		<div><a href="?newFoto=<?=$show['ID']?>" class="no-print fast-action fast-action-add">Pridėti foto</a></div>
		<ol class="foto">
			<?php
			foreach($rows as $row) {
				echo '<li><a class="fancybox" rel="gallery1" href="'.UPLOAD_DIR.'foto/'.$row['url'].'" title="'.$row['title'].'"><span class="img"><img alt="" src="'.UPLOAD_DIR.'foto/'.str_replace('.jpg', '_'.$width.'x'.$height.'.jpg', $row['url']).'"></span>'.$row['title'];
				echo '</a>';
				$marks = [];
				if(isset($photos_links[$row['ID']]['event']) || isset($photos_links[$row['ID']]['achievement']))
					echo '<br>Žymės (kategorijos): ';
				if(isset($photos_links[$row['ID']]['event']))
					$marks[] = 'Renginys';
				if(isset($photos_links[$row['ID']]['achievement']))
					$marks[] = 'Vaiko pasiekimas';
					//
				if(isset($photos_links[$row['ID']]['achievement_cat_id'])) {
					foreach($photos_links[$row['ID']]['achievement_cat_id'] as $val)
						$marks[] = $achievements_areas[$val];
				}
				if(isset($photos_links[$row['ID']]['kid_id'])) {
					foreach($photos_links[$row['ID']]['kid_id'] as $val)
						$marks[] = $kids[$val];
				}
				echo implode(', ', $marks);
				echo ''.(ADMIN || $row['createdByEmployeeId'] == DARB_ID ? ' <a href="?show='.(int)$_GET['show'].'&amp;editFoto='.$row['ID'].'">Keisti</a> <a href="?deleteImg='.$row['ID'].'" onclick="return confirm(\'Ar tikrai norite ištrinti nuotrauką?\');">Trinti</a>' : '').'</li>';
			}
			?>
		</ol>
		<?php
		if(count($rows) > 0)
			echo '<div class="cl"><a href="?newFoto='.$show['ID'].'" class="no-print fast-action fast-action-add">Pridėti foto</a></div>';
	}
}





//getKidName
if(isset($_GET['list'])) { ?>
	<link rel="stylesheet" href="/libs/fancyBox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen">
	<script type="text/javascript" src="/libs/fancyBox/jquery.fancybox.pack.js?v=2.1.5"></script>
	<script type="text/javascript">
	$(function() {
		$(".fancybox").fancybox({
			openEffect	: 'none',
			closeEffect	: 'none',
			tpl: {
				error	 : '<p class="fancybox-error">Užklaustas turinys negali būti užkrautas.<br>Prašome bandyti dar kartą vėliau.</p>',
				closeBtn : '<a title="Uždaryti" class="fancybox-item fancybox-close" href="javascript:;"></a>',
				next	 : '<a title="Kitas" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
				prev	 : '<a title="Ankstesnis" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
			}

		});
	});
	</script>
	<?php
	if(ADMIN)
		$result = db_query("SELECT `".DB_moment_photos."`.*
		FROM `".DB_moment_photos."` JOIN `".DB_moment_activity."` ON `".DB_moment_activity."`.`ID`=`".DB_moment_photos."`.`moment_activity_id` AND `".DB_moment_activity."`.`kindergarten_id`=".DB_ID." AND `".DB_moment_activity."`.`isDeleted`=0 AND `".DB_moment_photos."`.`isDeleted`=0
		".(!empty($_GET['kid_id']) ? " JOIN `".DB_moment_photos_links."` `photos_links_kids` ON `".DB_moment_photos."`.`ID`=`photos_links_kids`.`moment_photo_id` AND `photos_links_kids`.`link_type`='kid_id' AND `photos_links_kids`.`value`='".(int)$_GET['kid_id']."'" : '')."
		".(!empty($_GET['achievement_cat_id']) ? " JOIN `".DB_moment_photos_links."` `photos_links_achievement_cat_id` ON `".DB_moment_photos."`.`ID`=`photos_links_achievement_cat_id`.`moment_photo_id` AND `photos_links_achievement_cat_id`.`link_type`='achievement_cat_id' AND `photos_links_achievement_cat_id`.`value`='".(int)$_GET['achievement_cat_id']."'" : '')."
		".(!empty($_GET['event']) ? " JOIN `".DB_moment_photos_links."` `photos_links_event` ON `".DB_moment_photos."`.`ID`=`photos_links_event`.`moment_photo_id` AND `photos_links_event`.`link_type`='event'" : '')."
		".(!empty($_GET['achievement']) ? " JOIN `".DB_moment_photos_links."` `photos_links_achievement` ON `".DB_moment_photos."`.`ID`=`photos_links_achievement`.`moment_photo_id` AND `photos_links_achievement`.`link_type`='achievement'" : '')."
		LEFT JOIN `".DB_moment_photos_links."` `photos_links` ON `".DB_moment_photos."`.`ID`=`photos_links`.`moment_photo_id` AND `photos_links`.`link_type`='group_id'
		ORDER BY `date` DESC");
	else
		$result = db_query("SELECT `".DB_moment_photos."`.*
		FROM `".DB_moment_photos."` JOIN `".DB_moment_activity."` ON 
		`".DB_moment_activity."`.`ID`=`".DB_moment_photos."`.`moment_activity_id` AND `".DB_moment_activity."`.`kindergarten_id`=".DB_ID." AND `".DB_moment_activity."`.`isDeleted`=0 AND `".DB_moment_photos."`.`isDeleted`=0
		".(!empty($_GET['kid_id']) ? " JOIN `".DB_moment_photos_links."` `photos_links_kids` ON `".DB_moment_photos."`.`ID`=`photos_links_kids`.`moment_photo_id` AND `photos_links_kids`.`link_type`='kid_id' AND `photos_links_kids`.`value`='".(int)$_GET['kid_id']."'" : '')."
		".(!empty($_GET['achievement_cat_id']) ? " JOIN `".DB_moment_photos_links."` `photos_links_achievement_cat_id` ON `".DB_moment_photos."`.`ID`=`photos_links_achievement_cat_id`.`moment_photo_id` AND `photos_links_achievement_cat_id`.`link_type`='achievement_cat_id' AND `photos_links_achievement_cat_id`.`value`='".(int)$_GET['achievement_cat_id']."'" : '')."
		".(!empty($_GET['event']) ? " JOIN `".DB_moment_photos_links."` `photos_links_event` ON `".DB_moment_photos."`.`ID`=`photos_links_event`.`moment_photo_id` AND `photos_links_event`.`link_type`='event'" : '')."
		".(!empty($_GET['achievement']) ? " JOIN `".DB_moment_photos_links."` `photos_links_achievement` ON `".DB_moment_photos."`.`ID`=`photos_links_achievement`.`moment_photo_id` AND `photos_links_achievement`.`link_type`='achievement'" : '')."
		LEFT JOIN `".DB_moment_photos_links."` `photos_links` ON `".DB_moment_photos."`.`ID`=`photos_links`.`moment_photo_id` AND `photos_links`.`link_type`='group_id'
		WHERE `".DB_moment_photos."`.`visibility`=0 OR `".DB_moment_photos."`.`visibility`<>0 AND `photos_links`.`value`='".GROUP_ID."'
		ORDER BY `date` DESC");//AND `".DB_moment_activity."`.`ID`=".(int)$_GET['show']."

	$rows = [];
	$ids = [];
	while ($row = mysqli_fetch_assoc($result)) {
		$rows[] = $row;
		$ids[] = $row['ID'];
		//if(!empty($_GET['editFoto']) && $_GET['editFoto'] == $row['ID'])
		//	$edit = $row;
	}
	//print_r(implode(',', $ids))
	$photos_links = [];
	if(!empty($ids)) {
		$res = db_query("SELECT * FROM `".DB_moment_photos_links."` WHERE `moment_photo_id` IN (".implode(',', $ids).")");
		while ($row = mysqli_fetch_assoc($res))
			$photos_links[$row['moment_photo_id']][$row['link_type']][] = $row['value'];
	}
	?>
	<ol class="foto">
		<?php
		foreach($rows as $row) {
			echo '<li><a class="fancybox" rel="gallery1" href="'.UPLOAD_DIR.'foto/'.$row['url'].'" title="'.$row['title'].'"><span class="img"><img alt="" src="'.UPLOAD_DIR.'foto/'.str_replace('.jpg', '_'.$width.'x'.$height.'.jpg', $row['url']).'"></span>'.$row['title'];
			echo '</a>';
			$marks = [];
			if(isset($photos_links[$row['ID']]['event']) || isset($photos_links[$row['ID']]['achievement']))
				echo '<br>Žymės (kategorijos): ';
			if(isset($photos_links[$row['ID']]['event']))
				$marks[] = 'Renginys';
			if(isset($photos_links[$row['ID']]['achievement']))
				$marks[] = 'Vaiko pasiekimas';
				//
			if(isset($photos_links[$row['ID']]['achievement_cat_id'])) {
				foreach($photos_links[$row['ID']]['achievement_cat_id'] as $val)
					$marks[] = $achievements_areas[$val];
			}
			if(isset($photos_links[$row['ID']]['kid_id'])) {
				foreach($photos_links[$row['ID']]['kid_id'] as $id)
					$marks[] = getAllKidName($id);
			}
			echo implode(', ', $marks).
				(ADMIN || $row['createdByEmployeeId'] == DARB_ID ? ' <a href="?show='.(int)$row['moment_activity_id'].'&amp;editFoto='.$row['ID'].'">Keisti</a> <a href="?deleteImg='.$row['ID'].'" onclick="return confirm(\'Ar tikrai norite ištrinti nuotrauką?\');">Trinti</a>' : '').
				'</li>';
		}
		?>
	</ol>
	<?php
}
?>
</div>
