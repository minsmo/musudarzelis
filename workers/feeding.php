<?php if(!defined('DARBUOT')) exit(); ?>
<h1>Kiek reikia<!-- turėtų būti --> porcijų pagal lankomumą (tik lankytoms dienoms) ir vaikų maitinimo duomenis</h1>
<div id="content">
<?=(isset($_GET['print']) ? ui_print() : '')?>
<form method="get" style="padding-bottom:10px;" class="no-print">
	<div class="sel" style="float: left; margin-right: 7px;"><select name="data" required="required">
	<?php
	if(isset($_GET['data'])) {
		list($year, $month) = explode('-', $_GET['data']);
		$year = (int) $year;
		$month = (int) $month;
	}
	$result = db_query("SELECT YEAR(`data`) `metai`, MONTH(`data`) `menuo` FROM `".DB_attendance."` GROUP BY YEAR(`data`) DESC, MONTH(`data`) DESC");
	//Neisigilinau, bet kažkas įdomaus: http://stackoverflow.com/questions/15390303/how-to-group-by-desc-order
	//http://use-the-index-luke.com/3-minute-test/mysql?quizkey=d2a088303b955b839e29cb8711a85235
	/*
	SELECT text, date_column
	  FROM tbl
	 WHERE date_column >= STR_TO_DATE('2012-01-01', '%Y-%m-%d')
	   AND date_column <  STR_TO_DATE('2013-01-01', '%Y-%m-%d');
	*/
	while ($row = mysqli_fetch_assoc($result))
		echo "<option value=\"".$row['metai']."-".$row['menuo']."\"".(isset($_GET['data']) && $row['metai'] == $year && $row['menuo'] == $month ? ' selected="selected" class="selected">'.$selectedMark : '>').$row['metai']."-".men($row['menuo'])."</option>";
	?>
	</select></div>
	<div class="sel" style="float: left; margin-right: 7px;"><select name="grupes_id">
	<?php
	if(!AUKLETOJA) {
		echo "<option value=\"0\"".(isset($_GET['grupes_id']) && 0 == $_GET['grupes_id'] ? ' selected="selected" class="selected">'.$selectedMark : '>')."Visoms (suma)</option>";
		if(isset($_GET['grupes_id']) && 0 == $_GET['grupes_id'])
			$grupes_pav = 'Visoms (suma)';
			
		echo "<option value=\"-1\"".(isset($_GET['grupes_id']) && -1 == $_GET['grupes_id'] ? ' selected="selected" class="selected">'.$selectedMark : '>')."Visoms (sąrašu)</option>";
		if(isset($_GET['grupes_id']) && -1 == $_GET['grupes_id'])
			$grupes_pav = 'Visoms (sąrašu)';
	}
	foreach(getAllowedGroups() as $ID => $title) {
		echo "<option value=\"".$ID."\"".(isset($_GET['data']) && isset($_GET['grupes_id']) && $ID == $_GET['grupes_id'] || !isset($_GET['data']) && !(ADMIN /*|| BUHALT*/) && $ID == GROUP_ID ? ' selected="selected" class="selected">'.$selectedMark : '>').$title."</option>";
		if(isset($_GET['grupes_id']) && $ID == $_GET['grupes_id'])
			$grupes_pav = $title;
	}
	?>
	</select></div>
	<div class="sel" style="float: left; margin-right: 7px;"><select name="po_du" <?=(isset($_GET['grupes_id']) && $_GET['grupes_id'] == -1 ? ' disabled' : '')?>>
	<option value="1"<?=(!isset($_GET['po_du']) && isset($_GET['po_du']) && $_GET['po_du'] ? '  selected="selected">'.$selectedMark : '>')?>Glaustas (be tuščių eilučių)</option>
	<option value=""<?=(isset($_GET['po_du']) && !$_GET['po_du'] ? '  selected="selected">'.$selectedMark : '>')?>Visos eilutės</option>
	</select></div>
	<?php /*
	Eilutės tik su užsakymais
	<label><input type="checkbox" id="podu" name="po_du" <?=(isset($_GET['grupes_id']) && $_GET['grupes_id'] == -1 ? ' disabled' : '')?> style="width: 33px;"<?=(isset($_GET['po_du']) ? ' checked' : '')?>> Glaustas (nerodyti tuščios lentelės eilutės. Tuščia eilutė būna, kai nėra nė vieno užsakiusio maitinimo variantą)</label><br />
	*/ ?>
	<label><input type="checkbox" name="print" style="width: 33px;"<?=(isset($_GET['print']) ? ' checked' : '')?>> Spausdinimui</label><br />
	
	<?php
	/*
	
	*/
	?>
	<input type="submit" class="filter" value="Skaičiuoti">
</form>

<?php
if(isset($_GET['grupes_id']) && isset($_GET['data']) /*&& (hasGroup($_GET['grupes_id']) || $_GET['grupes_id'] ==)*/) {
	$data_max = $year.'-'.men($month).'-'.date('t', strtotime($year.'-'.men($month).'-01'));
	$data_min = $year.'-'.men($month).'-01';
	
	$filter_by_group = '';
	if($_GET['grupes_id'] != 0 && $_GET['grupes_id'] != -1)
		 $filter_by_group = "AND `grupes_id`=".(int)$_GET['grupes_id'];
	$darbo_dienos = db_query("SELECT DAY(`".DB_attendance."`.`data`) AS `diena`
		FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<='".db_fix($data_max)."' $filter_by_group GROUP BY `parent_kid_id` --  AND `archyvas`=0
		) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
			JOIN `".DB_attendance."` ON cr.`parent_kid_id`=`".DB_attendance."`.vaiko_id
		WHERE cr.`isDeleted`= 0 AND `".DB_attendance."`.`data` BETWEEN '".$data_min."' AND '".$data_max."'
		GROUP BY DAY(`".DB_attendance."`.`data`)");
	$dienu_arr = [];
	while ($row = mysqli_fetch_assoc($darbo_dienos))
		$dienu_arr[] = $row['diena'];
	
	$filter_by_group = '';
	if($_GET['grupes_id'] != 0 && $_GET['grupes_id'] != -1)
		 $filter_by_group = "AND cr.`grupes_id`=".(int)$_GET['grupes_id'];
	$r_vaikai = db_query("SELECT `".DB_attendance."`.*, cr.*, COUNT(*) AS `kiek`, SUM(`yra`) AS `buvo`
		FROM `".DB_attendance."` 
		JOIN `".DB_children."` cr ON (cr.`parent_kid_id`=`".DB_attendance."`.vaiko_id AND cr.`valid_from`<=`".DB_attendance."`.`data`)
		LEFT JOIN `".DB_children."` mx ON cr.`parent_kid_id`=mx.`parent_kid_id` AND cr.`valid_from`<mx.`valid_from` AND cr.`valid_from`<=`".DB_attendance."`.`data` AND mx.`valid_from`<=`".DB_attendance."`.`data`
		WHERE cr.`isDeleted`= 0 AND `".DB_attendance."`.`data` BETWEEN '".$data_min."' AND '".$data_max."' $filter_by_group AND mx.`parent_kid_id` IS NULL
		GROUP BY cr.`parent_kid_id` -- `vardas`, `pavarde`");//ORDER BY ".orderName('cr', $data_max)
		
	if($_GET['grupes_id'] == -1) {
		$all_days = array();
		$result = db_query("SELECT `data` FROM `".DB_attendance."` 
				WHERE `data` BETWEEN '".$data_min."' AND '".$data_max."' AND `yra`=1
				GROUP BY `data`");
		while ($row = mysqli_fetch_assoc($result))
			$all_days[$row['data']] = ['arPusryciaus' => 0, 'arPriespieciaus' => 0, 'arPietaus' => 0, 'arPavakariaus' => 0, 'arVakarieniaus' => 0, 'arNaktipieciaus' => 0, 'allergic' => 0, 0 => 0, 'free_breakfast' => 0, 'free_lunch' => 0/*, 1 => 0, 2 => 0, 3 => 0, 4 => 0, 'tikPusryciai' => 0, 'tikPietus' => 0, 'tikPavakariai' => 0, 'tikVakariene' => 0*/];
	}
	$kiek = array();
	$children = [];
	while ($row = mysqli_fetch_assoc($r_vaikai)) {
		//Validation/Checking
		$exists_in_group = false;
		$pre_date = $year.'-'.men($month).'-';
		foreach($dienu_arr as $diena) {
			$kid = getKid($row['parent_kid_id'], $pre_date.dien($diena));
			if( ($kid['grupes_id'] == (int)$_GET['grupes_id'] || $_GET['grupes_id'] <= 0) && !$kid['isDeleted'] )//Pagal idėją turėtų būti && !$kid['archyvas']
				$exists_in_group = true;
		}
		if(!$exists_in_group) continue;
		$children[] = (int)$row['parent_kid_id'];
	}
	
	$kid_attendance = [];
	if(!empty($children)) {
		$vaiko_lankomumas = db_query("SELECT * FROM `".DB_attendance."` 
			WHERE `vaiko_id` IN (".implode(',', $children).") AND `data` BETWEEN '".$data_min."' AND '".$data_max."' AND `yra`=1
			ORDER BY `data`");
		while ($lankomumas = mysqli_fetch_assoc($vaiko_lankomumas)) 
			$kid_attendance[$lankomumas['vaiko_id']][] = $lankomumas;
	}
	$feeding_list = ['arPusryciaus', 'arPriespieciaus', 'arPietaus', 'arPavakariaus', 'arVakarieniaus', 'arNaktipieciaus', 'allergic', 'free_breakfast', 'free_lunch'];
	foreach($children as $child) {
		//$var = $kid_attendance[$child];//Works as cache speed up when placed above foreach
		if(isset($kid_attendance[$child])) foreach($kid_attendance[$child] as $lankomumas) {
			//Prepare cost calculation
			$data_dabar = $lankomumas['data'];
			$kid = getKid($lankomumas['vaiko_id'], $data_dabar);
			if($_GET['grupes_id'] == -1) {
				$lankomumas = array_merge($kid, $lankomumas);
				//Calculation
				//if($lankomumas['yra']) {//Būtoms ir nebūtoms nepateisintoms // || !$lankomumas['yra'] && !$lankomumas['justified_d']
					if(!isset($kiek[$kid['grupes_id']]))
						$kiek[$kid['grupes_id']] = $all_days;
					/*$kiek[$kid['grupes_id']][$data_dabar]['arPusryciaus'] += $lankomumas['arPusryciaus'];
					$kiek[$kid['grupes_id']][$data_dabar]['arPriespieciaus'] += $lankomumas['arPriespieciaus'];
					$kiek[$kid['grupes_id']][$data_dabar]['arPietaus'] += $lankomumas['arPietaus'];
					$kiek[$kid['grupes_id']][$data_dabar]['arPavakariaus'] += $lankomumas['arPavakariaus'];
					$kiek[$kid['grupes_id']][$data_dabar]['arVakarieniaus'] += $lankomumas['arVakarieniaus'];
					$kiek[$kid['grupes_id']][$data_dabar]['arNaktipieciaus'] += $lankomumas['arNaktipieciaus'];
					$kiek[$kid['grupes_id']][$data_dabar]['allergic'] += $lankomumas['allergic'];
					$kiek[$kid['grupes_id']][$data_dabar]['free_breakfast'] += $lankomumas['free_breakfast'];
					$kiek[$kid['grupes_id']][$data_dabar]['free_lunch'] += $lankomumas['free_lunch'];*/
					foreach($feeding_list as $feeding)
						$kiek[$kid['grupes_id']][$data_dabar][$feeding] += $lankomumas[$feeding];
					
					if($lankomumas['arPusryciaus']+$lankomumas['arPriespieciaus']+$lankomumas['arPietaus']+$lankomumas['arPavakariaus']+$lankomumas['arVakarieniaus']+$lankomumas['arNaktipieciaus'] == 0)
						$kiek[$kid['grupes_id']][$data_dabar][0]++;
				
					/*if($lankomumas['arPusryciaus']+$lankomumas['arPietaus']+$lankomumas['arPavakariaus']+$lankomumas['arVakarieniaus'] == 1) {
						if($lankomumas['arPusryciaus'])
							$kiek[$data_dabar][$lankomumas['arDarzelyje']]['tikPusryciai']++;
						if($lankomumas['arPietaus'])
							$kiek[$data_dabar][$lankomumas['arDarzelyje']]['tikPietus']++;
						if($lankomumas['arPavakariaus'])
							$kiek[$data_dabar][$lankomumas['arDarzelyje']]['tikPavakariai']++;
						if($lankomumas['arVakarieniaus'])
							$kiek[$data_dabar][$lankomumas['arDarzelyje']]['tikVakariene']++;
					}*/
				//}
			} else {//##################################################
				if($kid != false && ($kid['grupes_id'] == (int)$_GET['grupes_id'] || $_GET['grupes_id'] == 0)) {
					$lankomumas = array_merge($kid, $lankomumas);
					//Calculation
					//if($lankomumas['yra']) {//Būtoms ir nebūtoms nepateisintoms // || !$lankomumas['yra'] && !$lankomumas['justified_d']
						if(!isset($kiek[$data_dabar]))
							$kiek[$data_dabar] = array();
						if(!isset($kiek[$data_dabar][$lankomumas['arDarzelyje']]))
							$kiek[$data_dabar][$lankomumas['arDarzelyje']] = ['arPusryciaus' => 0, 'arPriespieciaus' => 0, 'arPietaus' => 0, 'arPavakariaus' => 0, 'arVakarieniaus' => 0, 'arNaktipieciaus' => 0, 'allergic' => 0, 0 => 0, 1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0, 'tikPusryciai' => 0, 'tikPietus' => 0, 'tikPavakariai' => 0, 'tikVakariene' => 0, 'free_breakfast' => 0, 'free_lunch' => 0, 'PusryciaiIrPietus' => 0, 'PusryciaiIrPavakariai' => 0, 'PusryciaiIrVakariene' => 0, 'PietusIrPavakariai' => 0, 'PietusIrVakariene' => 0, 'PavakariaiIrVakariene' => 0];
							
						/*$kiek[$data_dabar][$lankomumas['arDarzelyje']]['arPusryciaus'] += $lankomumas['arPusryciaus'];
						$kiek[$data_dabar][$lankomumas['arDarzelyje']]['arPietaus'] += $lankomumas['arPietaus'];
						$kiek[$data_dabar][$lankomumas['arDarzelyje']]['arPavakariaus'] += $lankomumas['arPavakariaus'];
						$kiek[$data_dabar][$lankomumas['arDarzelyje']]['arVakarieniaus'] += $lankomumas['arVakarieniaus'];
						$kiek[$data_dabar][$lankomumas['arDarzelyje']]['allergic'] += $lankomumas['allergic'];
						$kiek[$data_dabar][$lankomumas['arDarzelyje']]['free_breakfast'] += $lankomumas['free_breakfast'];
						$kiek[$data_dabar][$lankomumas['arDarzelyje']]['free_lunch'] += $lankomumas['free_lunch'];*/
						foreach($feeding_list as $feeding)
							$kiek[$data_dabar][$lankomumas['arDarzelyje']][$feeding] += $lankomumas[$feeding];
						
						$kiek[$data_dabar][$lankomumas['arDarzelyje']][$lankomumas['arPusryciaus']+$lankomumas['arPriespieciaus']+$lankomumas['arPietaus']+$lankomumas['arPavakariaus']+$lankomumas['arVakarieniaus']+$lankomumas['arNaktipieciaus']]++;
						if($lankomumas['arPusryciaus']+$lankomumas['arPietaus']+$lankomumas['arPavakariaus']+$lankomumas['arVakarieniaus'] == 1) {
							if($lankomumas['arPusryciaus'])
								$kiek[$data_dabar][$lankomumas['arDarzelyje']]['tikPusryciai']++;
							if($lankomumas['arPietaus'])
								$kiek[$data_dabar][$lankomumas['arDarzelyje']]['tikPietus']++;
							if($lankomumas['arPavakariaus'])
								$kiek[$data_dabar][$lankomumas['arDarzelyje']]['tikPavakariai']++;
							if($lankomumas['arVakarieniaus'])
								$kiek[$data_dabar][$lankomumas['arDarzelyje']]['tikVakariene']++;
						}
						
						
						if($lankomumas['arPusryciaus']+$lankomumas['arPietaus']+$lankomumas['arPavakariaus']+$lankomumas['arVakarieniaus'] == 2) {
							if($lankomumas['arPusryciaus']+$lankomumas['arPietaus']-$lankomumas['arPavakariaus']-$lankomumas['arVakarieniaus'] == 2)
								$kiek[$data_dabar][$lankomumas['arDarzelyje']]['PusryciaiIrPietus']++;
							
							if($lankomumas['arPusryciaus']+$lankomumas['arPavakariaus']-$lankomumas['arPietaus']-$lankomumas['arVakarieniaus'] == 2)
								$kiek[$data_dabar][$lankomumas['arDarzelyje']]['PusryciaiIrPavakariai']++;
							
							if($lankomumas['arPusryciaus']-$lankomumas['arVakarieniaus']-$lankomumas['arPietaus']-$lankomumas['arPavakariaus'] == 2)
								$kiek[$data_dabar][$lankomumas['arDarzelyje']]['PusryciaiIrVakariene']++;
							
							if($lankomumas['arPietaus']+$lankomumas['arPavakariaus']-$lankomumas['arPusryciaus']-$lankomumas['arVakarieniaus'] == 2)
								$kiek[$data_dabar][$lankomumas['arDarzelyje']]['PietusIrPavakariai']++;
							
							if($lankomumas['arPietaus']+$lankomumas['arVakarieniaus']-$lankomumas['arPusryciaus']-$lankomumas['arPavakariaus'] == 2)
								$kiek[$data_dabar][$lankomumas['arDarzelyje']]['PietusIrVakariene']++;
							
							if($lankomumas['arPavakariaus']+$lankomumas['arVakarieniaus']-$lankomumas['arPusryciaus']-$lankomumas['arPietaus'] == 2)
								$kiek[$data_dabar][$lankomumas['arDarzelyje']]['PavakariaiIrVakariene']++;
						}
					//}
				}
			}
		}
	}
	//print_r($kiek);
	ksort($kiek);
	if($_GET['grupes_id'] == -1)
		foreach($kiek as &$dates)
			ksort($dates);
		unset($dates);
	?>
	<script src="/libs/tablehover/jquery.tablehover.pack.js"></script>
	<?=(isset($_GET['print']) ? '' : '<script src="/libs/jquery.floatThead.min.js"></script>')?>
	<?php
	if (isset($_GET['download'])) {
		ob_clean();
		word_header();
		echo '<div class=Section2>';
	}
	?>
	<table id="food-tbl">
	<thead>
	
	<tr><th colspan="<?php
	if($_GET['grupes_id'] == -1) {
		$working_days = 0;
		foreach($kiek as $dates)
			$working_days = max(count($dates), $working_days);
	} else
		$working_days = count($kiek);
	$total_cols = $working_days+2;
	echo $total_cols;
	?>" style="text-align: center;"><?php echo filterText(getConfig('darzelio_pavadinimas', $data_max))."</th></tr>";
	if($_GET['grupes_id'] != -1)
		echo "<tr><th colspan=\"".($total_cols)."\" style=\"text-align: center;\">Vaikų grupė: ".$grupes_pav."</th></tr>";
	echo "<tr><th>".$year.'-'.men($month)." (".$working_days." darbo d.):</th>";
	if($_GET['grupes_id'] == -1) {
		foreach($kiek as &$dates)
			ksort($dates);
		unset($dates);
		
		foreach($all_days as $data => $tmp) {
			echo "<th>".substr($data, -2)."</th>";
		}
	} else {
		foreach($kiek as $data => $tmp) {
			echo "<th>".substr($data, -2)."</th>";
		}
	}
	echo "<th>Iš viso</th>
	</tr>
	</thead>";
	
	if($_GET['grupes_id'] == -1) {
		$food_time = ['arPusryciaus' => 'Pusryčių', 'arPriespieciaus' => 'Priešpiečių', 'arPietaus' => 'Pietų', 'arPavakariaus' => 'Pavakarių', 'arVakarieniaus' => 'Vakarienių', 'arNaktipieciaus' => 'Naktipiečių', 'allergic' => 'Alergiškų', 0 => 'Be maitinimo', 'free_breakfast' => '<span class="abbr" title="Tik pažymėjus varnelę nemokamų pusryčių meniu punkte „Vaikai“">Nemokami pusryčiai*</span>', 'free_lunch' => '<span class="abbr" title="Tik pažymėjus varnelę nemokamų pietų meniu punkte „Vaikai“">Nemokami pietūs*</span>'];
		//nemokamas maitinimas
		$AllowedGroups = getAllowedGroups();
		foreach($kiek as $group_id => $kiek_tmp) {
			echo "<tr><td colspan=\"$total_cols\" style=\"text-align: center\"><strong>".$AllowedGroups[$group_id]."</strong></td></tr>";
			foreach($food_time as $food_type => $food_type_title) {
				echo "<tr><td>".$food_type_title."</td>";
				foreach($kiek_tmp as $data => $tmp) {
					echo "<td>".(isset($kiek[$group_id][$data][$food_type]) ? ($kiek[$group_id][$data][$food_type] == 0 ? '' : $kiek[$group_id][$data][$food_type]) : '')."</td>";
				}
				$sum = 0;
				foreach($kiek_tmp as $data => $tmp) {
					$sum += isset($kiek[$group_id][$data][$food_type]) ? $kiek[$group_id][$data][$food_type] : 0;
				}
				echo "<td>".($sum == 0 ? '' : $sum)."</td>";
				echo "</tr>";
			}
		}
	} else {//##################################################
		$food_time = ['arPusryciaus' => 'Pusryčių', 'arPriespieciaus' => 'Priešpiečių', 'arPietaus' => 'Pietų', 'arPavakariaus' => 'Pavakarių', 'arVakarieniaus' => 'Vakarienių', 'arNaktipieciaus' => 'Naktipiečių', 'allergic' => 'Alergiškų', 0 => 'Be maitinimo', 1 => '1 maitinimo k.', 2 => '2 maitinimo k.', 3 => '3 maitinimo k.', 4 => '4 maitinimo k.', 5 => '5 maitinimo k.', 6 => '6 maitinimo k.',
		'tikPusryciai' => 'Tik pusryčiai', 'tikPietus' => 'Tik pietūs', 'tikPavakariai' => 'Tik pavakariai', 'tikVakariene' => 'Tik vakarienė', 'free_breakfast' => '<span class="abbr" title="Tik pažymėjus varnelę nemokamų pusryčių meniu punkte „Vaikai“">Nemokami pusryčiai*</span>', 'free_lunch' => '<span class="abbr" title="Tik pažymėjus varnelę nemokamų pietų meniu punkte „Vaikai“">Nemokami pietūs*</span>', 'PusryciaiIrPietus' => 'Pusryčiai ir pietūs', 'PusryciaiIrPavakariai' => 'Pusryčiai ir pavakariai', 'PusryciaiIrVakariene' => 'Pusryčiai ir vakarienė', 'PietusIrPavakariai' => 'Pietūs ir pavakariai', 'PietusIrVakariene' => 'Pietūs ir vakarienė', 'PavakariaiIrVakariene' => 'Pavakariai ir vakarienė'];
		//nemokamas maitinimas
	
		foreach($vaik_darzelio_tipas_kas as $group_type => $group_type_name) {
			if($group_type != 4) {//Exception for mixed
				echo "<tr><td colspan=\"$total_cols\" style=\"text-align: center\"><strong>".$group_type_name."</strong></td></tr>";
				foreach($food_time as $food_type => $food_type_title) {
					$echo = "<tr><td>".$food_type_title."</td>";
					foreach($kiek as $data => $tmp) {
						$echo .= "<td>".(isset($kiek[$data][$group_type][$food_type]) ? ($kiek[$data][$group_type][$food_type] == 0 ? '' : $kiek[$data][$group_type][$food_type]) : '')."</td>";
					}
					$sum = 0;
					foreach($kiek as $data => $tmp) {
						$sum += isset($kiek[$data][$group_type][$food_type]) ? $kiek[$data][$group_type][$food_type] : 0;
					}
					$echo .= "<td>".($sum == 0 ? '' : $sum)."</td>";
					$echo .= "</tr>";
					
					if($sum > 0 || empty($_GET['po_du']))
						echo $echo;
				}
			}
		}
		echo "<tr><td colspan=\"$total_cols\" style=\"text-align: center\"><strong>Iš viso</strong></td></tr>";
		foreach($food_time as $food_type => $food_type_title) {
			$echo = "<tr><td>".$food_type_title."</td>";
			$totalSum = 0;
			foreach($kiek as $data => $tmp) {
				$sum = 0;
				foreach($vaik_darzelio_tipas_kas as $group_type => $group_type_name) {
					if($group_type != 4) {//Exception for mixed
						$sum += isset($kiek[$data][$group_type][$food_type]) ? $kiek[$data][$group_type][$food_type] : 0;
					}
				}
				$echo .= "<td>".($sum == 0 ? '' : $sum)."</td>";
				$totalSum += $sum;
			}
			$echo .= "<td>".$totalSum."</td>";
			$echo .= "</tr>";
			if($totalSum > 0 || empty($_GET['po_du']))
				echo $echo;
		}
	}
	?></table>
	<?php
	if (isset($_GET['download'])) {
		download_word('Maitinimas '.year($year)."-".men($month).' '.str_replace([',,', '"'], ['„', '“'], $grupes_pav));
	} else {
		echo '<a href="?'.http_build_query(array_merge($_GET, ['download' => ''])).'" class="no-print">Atsisiųsti Word</a>';
	}
	?>
	<script>
	<?=(isset($_GET['print']) ? '' : "$('#food-tbl').floatThead({position: 'fixed'});")?>
	
	$('#food-tbl').tableHover({rowClass: 'hover', colClass: 'hover', /*clickClass: 'click',*/ headRows: true,  
                    footRows: true, headCols: true, footCols: true}); 
	$("select[name=grupes_id]").change(
		function(){
			if($(this).val() == -1)
				$('#podu').attr('disabled','disabled');
			else
				$('#podu').removeAttr('disabled');
		}
	);
	</script>
	<?php
}
?>
</div>
