<div id="achievements-dialog">
<div id="dialog-control">
	<span id="dialog-title"></span>
	<div style="float: right;">
		<span id="toggle" title="Sumažinti/Išdidinti langelį">_(sumažinti)</span>
		<span id="close" title="Uždaryti">×</span>
	</div>
</div>
<div class="dialog" id="dialog-1" data-title="1. KASDIENIO GYVENIMO ĮGŪDŽIAI">
  <p><strong>Kasdienio gyvenimo įgūdžiai</strong> – tai vaiko sveikatai saugoti ir stiprinti būtini įpročiai ir elgesys.</p>
<strong>Vaiko asmeniniai mitybos ir valgymo</strong> įgūdžiai formuojasi vaikui mokantis pačiam valgyti, pasirenkant
tai, ką jis norėtų valgyti iš jam siūlomų patiekalų, produktų, mokantis gražiai elgtis prie stalo
(pasakyti „skanaus“, „ačiū“, paimti ir taisyklingai laikyti stalo įrankius, stengtis valgyti tvarkingai, gerti iš
puoduko, naudotis servetėle ir pan.).
<p><strong>Kūno švaros ir aplinkos tvarkos</strong> įpročius vaikas ugdosi mokydamasis savarankiškai apsirengti ir
nusirengti, susitvarkyti savo žaislus ir žaidimo, veiklos vietą, pratindamasis laikytis asmens higienos.</p>
<p><strong>Vaiko saugus elgesys</strong> formuojasi suaugusiesiems mokant saugiai elgtis su vaistais, įvairiais daiktais
ir priemonėmis, saugiai pereiti gatvę, padedant atpažinti artimiausioje aplinkoje pasitaikančias pavojingas
ir grėsmingas saugumui situacijas.</p>
<p><strong>Taisyklinga laikysena</strong> nebūna įgimta, ji formuojasi vaikui augant ir vystantis, nuo pačių pirmųjų
gyvenimo metų. Vaiko raumenys yra silpni ir greitai pavargsta, ypač ilgiau būnant vienoje padėtyje (pvz.,
ilgai sėdint prie kompiuterio), todėl svarbu mokyti vaiką taisyklingai sėdėti, stovėti tiesiai, neįsitempus.
Kasdienio gyvenimo įgūdžių srityje vaikui
ugdantis tobulėja:
<ul>
  <li>vaiko asmeniniai valgymo
ir mitybos įgūdžiai,</li>
  <li>kūno švaros ir aplinkos tvarkos
palaikymo įgūdžiai,</li>
  <li>saugaus elgesio įgūdžiai,</li>
  <li>taisyklinga kūno laikysena.</li>
</ul>
<p></p>

<table>
    <tr>
<th></th>
<th>Žingsniai</th>
<th>Kasdienio gyvenimo įgūdžių srities pasiekimai</th>
</tr>
	<tr>
        <td class="darker" rowspan="3"><div class='rotate'>0–3 metai</div></td>
        <td class="step-center">1-asis žingsnis</td>
		<td><ul><li>Bando savarankiškai valgyti ir gerti iš puodelio.</li></ul></td>
    </tr>
		<tr><td class="step-center">2-asis žingsnis</td><td><ul><li>Valgo ir geria padedamas arba savarankiškai.
<li>Kartais parodo mimika, ženklais arba pasako, kada nori tuštintis ir šlapintis.
Suaugusiojo rengiamas vaikas „jam padeda“. Suaugusiojo padedamas plaunasi,
šluostosi rankas, išpučia nosį. Paprašytas padeda žaislą į nurodytą vietą.</li></ul></td></tr>
		<tr><td class="step-center">3-iasis žingsnis</td><td><ul><li>Savarankiškai valgo ir geria. Pradeda naudoti stalo įrankius. Pasako, ko nori ir ko
nenori valgyti.
<li>Pats eina į tualetą, suaugusiojo padedamas susitvarko. Suaugusiojo padedamas
nusirengia ir apsirengia, bando praustis, nusišluostyti veidą, rankas. Padeda į vietą
vieną kitą daiktą.</li></ul></td></tr>
	<tr>
        <td class='darker' rowspan="3"><div class='rotate'>4–6 metai</div></td>
        <td class="step-center">4-asis žingsnis</td>
		<td><ul><li>Valgo gana tvarkingai. Primenamas po valgio skalauja burną. Pasako, kodėl
reikia plauti vaisius, uogas, daržoves. Padeda suaugusiajam serviruoti ir po
valgio sutvarkyti stalą.</li>
<li>Dažniausiai savarankiškai naudojasi tualetu ir susitvarko juo pasinaudojęs. Šiek
tiek padedamas apsirengia ir nusirengia, apsiauna ir nusiauna batus. Šiek tiek
padedamas plaunasi rankas, prausiasi, nusišluosto rankas ir veidą. Priminus
čiaudėdamas ar kosėdamas prisidengia burną ir nosį. Gali sutvarkyti dalį žaislų,
su kuriais žaidė.</li>
<li>Pasako, kad negalima imti degtukų, vaistų, aštrių ir kitų pavojingų daiktų.</li></ul> </td>
    </tr>
		<tr><td class="step-center">5-asis žingsnis</td><td><ul><li>Valgo tvarkingai, dažniausiai taisyklingai naudojasi stalo įrankiais. Domisi, koks
maistas sveikas ir visavertis. Serviruoja ir tvarko stalą, vadovaujamas suaugusiojo.</li>
<li>Savarankiškai apsirengia ir nusirengia, apsiauna ir nusiauna batus. Priminus
plaunasi rankas, prausiasi, nusišluosto rankas ir veidą. Priminus tvarkosi žaislus
ir veiklos vietą.</li>
<li>Žaisdamas, ką nors veikdamas stengiasi saugoti save ir kitus.</li>
<li>Priminus stengiasi sėdėti, stovėti, vaikščioti taisyklingai.</li></ul> </td></tr>
		<tr><td class="step-center">6-asis žingsnis</td><td><ul><li>Valgo tvarkingai. Pasako, jog maistas reikalingas, kad augtume, būtume sveiki.
Įvardija vieną kitą maisto produktą, kurį valgyti sveika, vieną kitą – kurio vartojimą
reikėtų riboti. Savarankiškai serviruoja ir tvarko stalą.</li>
<li>Savarankiškai apsirengia ir nusirengia, apsiauna ir nusiauna batus. Suaugusiųjų
padedamas pasirenka drabužius ir avalynę pagal orus. Priminus ar savarankiškai
plaunasi rankas, prausiasi, nusišluosto rankas ir veidą. Dažniausiai savarankiškai
tvarkosi žaislus ir veiklos vietą.</li>
<li>Savarankiškai ar priminus laikosi sutartų saugaus elgesio taisyklių. Stebint
suaugusiajam saugiai naudojasi veiklai skirtais aštriais įrankiais. Žino, kaip saugiai
elgtis gatvėje, kur kreiptis iškilus pavojui, pasiklydus.</li>
<li>Priminus stengiasi vaikščioti, stovėti, sėdėti taisyklingai.</li></ul></td>
</tr>
<tr class="darker">
<td colspan="2">Vertybinė nuostata</td>
<td colspan="1">Noriai ugdosi sveikam kasdieniam gyvenimui reikalingus įgūdžius.</td>
</tr>
<tr class="darker">
<td colspan="2">Esminis gebėjimas</td>
<td colspan="1">Tvarkingai valgo, savarankiškai atlieka savitvarkos veiksmus: apsirengia ir
nusirengia, naudojasi tualetu, prausiasi, šukuojasi. Saugo savo sveikatą ir saugiai
elgiasi aplinkoje.</td>
</tr>
<tr>
        <td class='darker' rowspan="1"><div class="rotate"></div></td>
        <td class="step-center">7-asis žingsnis</td>
		<td><ul><li>Tvarkingai valgo, jaučia, kada alkanas, kada pasisotino, geria pakankamai vandens.
Stengiasi valgyti įvairų maistą. Įvardija kelis maisto produktus, kuriuos valgyti
sveika, ir kelis, kuriuos reikėtų riboti. Savarankiškai serviruoja ir tvarko stalą.</li>
<li>Dažniausiai savarankiškai ar priminus plaunasi rankas, prausiasi, šukuojasi.
Savarankiškai tvarkingai apsirengia ir nusirengia, apsiauna ir nusiauna. Suaugusiųjų
padedamas arba savarankiškai pasirenka drabužius ir avalynę pagal orus.
Savarankiškai tvarkosi žaislus ir veiklos vietą.</li>
<li>Savarankiškai laikosi sutartų saugaus elgesio taisyklių. Žino, kaip saugiai elgtis
gatvėje, kelyje, prie vandens telkinių, ant ledo, gaisro metu, su elektros prietaisais,
vaistais. Žino, kaip saugiai elgtis su nepažįstamais žmonėmis ir gyvūnais.</li>
<li>Kad būtų sveikas, stengiasi vaikščioti, stovėti, sėdėti taisyklingai.</li></ul></td>
    </tr>
</table>

</div>


<div class="dialog" id="dialog-2" data-title="2. FIZINIS AKTYVUMAS">
   <p><strong>Fizinis aktyvumas</strong>, judėjimas yra vienas iš svarbiausių prigimtinių vaiko poreikių, todėl būtina
skatinti tiek spontanišką, tiek pedagogo tikslingai inicijuojamą vaiko fizinį aktyvumą.
<p><strong>Fizinis aktyvumas užtikrina stambiosios motorikos įgūdžių</strong>, tokių kaip ėjimas, bėgimas, šokinėjimas,
pusiausvyros išlaikymas judant, laipiojimas aukštyn ir žemyn, važinėjimas triratuku, dviratuku, <strong>ugdymąsi</strong>.
<p><strong>Fizinis aktyvumas skatina fizinių vaiko savybių</strong>, tokių kaip lankstumas, vikrumas, ištvermė, greitumas,
judesių koordinacija, pusiausvyra, <strong>ugdymąsi. Aktyvi vaikų veikla skatina smulkiosios motorikos
įgūdžių</strong>, tokių kaip pirštų, delno, riešo, koordinuotų akių ir rankos judesių, gebėjimo naudoti piešimo,
rašymo priemones, kirpti žirklėmis, <strong>ugdymąsi</strong>.
<p>Fizinio aktyvumo srityje vaikui ugdantis tobulėja:
<ul>
<li>stambiosios motorikos įgūdžiai ir fizinės vaiko savybės,</li>
<li>smulkiosios motorikos įgūdžiai, rankų-akių koordinacija.</li>
</ul>

<table>
    <tr>
<th></th>
<th>Žingsniai</th>
<th>Fizinio aktyvumo srities pasiekimai</th>
</tr>
	<tr>
        <td class="darker" rowspan="3"><div class='rotate'>0–3 metai</div></td>
        <td class="step-center">1-asis žingsnis</td>
		<td><ul><li>Sėdi, šliaužia, ropoja pirmyn ir atgal, įkalnėn ir nuokalnėn, laiptais aukštyn,
ko nors įsitvėręs atsistoja, stovi laikydamasis ar savarankiškai, žingsniuoja laikydamasis,
vedamas arba savarankiškai, išlaiko pusiausvyrą.</li>
<li>Tikslingai siekia daikto, rankos judėjimą seka akimis, pačiumpa ir laiko daiktą
saujoje, paglosto žaislą jo nečiupdamas, kiša jį į burną, purto, mojuoja, stuksena,
gniaužo, dviem pirštais – nykščiu ir smiliumi – suima smulkų daiktą, perima
daiktus iš vienos rankos į kitą.</li></ul></td>
    </tr>
		<tr><td class="step-center">2-asis žingsnis</td><td><ul><li>Savarankiškai atsistoja, stovi, atsitupia, pasilenkia, eina į priekį, šoną ir atgal,
eina stumdamas ar tempdamas daiktą, bėga tiesiomis kojomis, atsisėdęs ant
riedančio žaislo stumiasi kojomis, pralenda per kliūtis keturpėsčia, padedamas
lipa laiptais aukštyn pristatomuoju žingsniu, spiria kamuolį išlaikydamas
pusiausvyrą.
<li>Pasuka riešą, apverčia plaštaką delnu žemyn, pasuka delnu aukštyn, mosteli
plaštaka, paima daiktą iš viršaus apimdamas jį pirštais, išmeta daiktus iš rankos
atleisdamas pirštus, ploja rankomis. Ridena, meta, gaudo kamuolį.</li></ul></td></tr>
		<tr><td class="step-center">3-iasis žingsnis</td><td><ul><li>Pastovi ant vienos kojos (3–4 sekundes). Tikslingai skirtingu ritmu eina ten, kur
nori, vaikščiodamas apeina arba peržengia kliūtis, eina plačia (25–30 cm) linija.
Bėga keisdamas kryptį, greitį. Lipa ir nulipa laiptais pakaitiniu žingsniu laikydamasis
suaugusiojo rankos ar turėklų. Atsispirdamas abiem kojomis pašoka nuo
žemės, nušoka nuo laiptelio, peršoka liniją, spiria kamuolį.</li>
<li>Geriau derina akies-rankos, abiejų rankų, rankų ir kojų judesius, tiksliau konstruoja,
veria ant virvutės sagas, ridena, mėto, gaudo kamuolį, įkerpa popieriaus kraštą.</li>
</ul></td></tr>
	<tr>
        <td class="darker" rowspan="3"><div class='rotate'>4–6 metai</div></td>
        <td class="step-center">4-asis žingsnis</td>
		<td><ul><li>Stovėdamas pasistiebia, atsistoja ant kulnų, stovėdamas ir sėdėdamas atlieka įvairius
judesius kojomis bei rankomis. Eina ant pirštų galų, eina siaura (5 cm) linija,
gimnastikos suoleliu, lipa laiptais aukštyn ir žemyn nesilaikydamas už turėklų, šokinėja
abiem ir ant vienos kojos, nušoka nuo paaukštinimo. Mina ir vairuoja triratuką.</li>
<li>Pieštuką laiko tarp nykščio ir kitų pirštų, tiksliau atlieka judesius plaštaka ir pirštais
(ima, atgnybia, suspaudžia dviem pirštais, kočioja tarp delnų) bei ranka (mojuoja,
plasnoja). Ištiestomis rankomis pagauna didelį kamuolį. Judesius tiksliau atlieka
kaire arba dešine ranka, koja.</li></ul> </td>
    </tr>
		<tr><td class="step-center">5-asis žingsnis</td><td><ul><li>Eina pristatydamas pėdą prie pėdos, pakaitiniu ir pristatomuoju žingsniu, aukš-
tai keldamas kelius, atlikdamas judesius rankomis, judėdamas vingiais. Greitas,
vikrus, bėgioja vingiais, greitėdamas ir lėtėdamas, išsisukinėdamas, bėga ant
pirštų galų. Šokinėja abiem kojomis vietoje ir judėdamas pirmyn, ant vienos
kojos, šokinėja per virvutę, peršoka žemas kliūtis, pašoka siekdamas daikto.
Laipioja lauko įrengimais. Spiria kamuolį iš įvairių padėčių, į taikinį.
<li>Pieštuką ir žirkles laiko beveik taisyklingai. Tiksliai atlieka sudėtingesnius judesius
pirštais ir ranka (veria ant virvelės smulkius daiktus, sega sagas). Meta kamuolį iš
įvairių padėčių, į taikinį, tiksliau gaudo, mušinėja. Įsisupa ir supasi sūpynėmis.</li></ul></td></tr>
		<tr><td class="step-center">6-asis žingsnis</td><td><ul><li>Eina ratu, poroje, prasilenkdamas, gyvatėle, atbulomis, šonu. Ištvermingas, bėga
ilgesnius atstumus. Bėga pristatomu ar pakaitiniu žingsniu, aukštai keldamas
kelius, bėga suoleliu, įkalnėn, nuokalnėn. Šokinėja ant vienos kojos judėdamas
pirmyn, šoka į tolį, į aukštį. Važiuoja dviračiu.</li>
<li>Rankos ir pirštų judesius atlieka vikriau, greičiau, tiksliau, kruopščiau. Tiksliau
valdo pieštuką bei žirkles ką nors piešdamas, kirpdamas. Su kamuoliu atlieka
sporto žaidimų elementus, žaidžia komandomis, derindami veiksmus.</li></ul></td>
</tr>
<tr class="darker">
<td colspan="2">Vertybinė nuostata</td>
<td colspan="1">Noriai, džiaugsmingai juda, mėgsta judrią veiklą ir žaidimus. </td>
</tr>
<tr class="darker">
<td colspan="2">Esminis gebėjimas</td>
<td colspan="1">Eina, bėga, šliaužia, ropoja, lipa, šokinėja koordinuotai, išlaikydamas
pusiausvyrą, spontaniškai ir tikslingai atlieka veiksmus, kuriems būtina akių-
rankos koordinacija bei išlavėjusi smulkioji motorika.</td>
</tr>
<tr>
           <td class='darker' rowspan="1"><div class="rotate"></div></td>
        <td class="step-center">7-asis žingsnis</td>
		<td><ul><li>Eina keisdamas greitį, staigiai sustoja, greitai pajuda iš vietos. Išlaiko saugų atstumą
eidamas, bėgdamas šalia draugo, būryje. Bėga derindamas du ar daugiau judesių
(bėga ir varosi, spiria kamuolį, bėga ir mojuoja rankomis, kaspinais). Šokinėja derindami
du skirtingus rankų ir (ar) kojų judesius (šoka per virvutę, žaidžia „Klases“).</li>
<li>Pieštuką ir žirkles laiko taisyklingai, kerpa gana tiksliai, sulenkia popieriaus lapą
per pusę, į keturias dalis. Tiksliai kopijuoja formas, raides. Meta ir kartais pataiko
kamuolį į krepšį, vartus, taikinį.</li></ul></td>
    </tr>
</table>

</div>


<div class="dialog" id="dialog-3" data-title="3. EMOCIJŲ SUVOKIMAS IR RAIŠKA">
   <p>Įvairias emocijas – džiaugsmą, pyktį, liūdesį, pavydą, gėdą, kaltę, meilę – vaikai jaučia nuo kūdikystės.
<p><strong>Emocinėms būsenoms būdingas sužadinimas, </strong> kurį vaikai jaučia fiziškai – kaip energijos antplūdį
arba energiją slopinantį sunkumą, dažną ar ramų širdies plakimą ir kt. Šioms būsenoms būdinga<strong> išorinė
raiška</strong>.t. y. išoriniai emocijų ženklai (šypsena, suraukti antakiai, sugniaužti kumščiai ir kt.), ir veiksmai,
poelgiai, t. y. kitam pasakyti žodžiai, santūrūs ar agresyvūs veiksmai ir kt.
<p>Emocinėms būsenoms taip pat būdinga <strong>sąmoningai</strong> jas <strong>išgyventi</strong> – bandyti susivokti, ką ir dėl ko
jauti, kokios emocijos ar jausmai užvaldė, gebėti tai išreikšti žodžiais, pasakyti kitam.
<p>Emocijų suvokimo ir raiškos srityje vaikui ugdantis tobulėja:
<ul><li>savo jausmų raiška, suvokimas ir pavadinimas, 
<li>kitų žmonių jausmų atpažinimas ir tinkamas reagavimas į juos,
<li>savo bei kitų nuotaikų ir jausmų apmąstymas.</ul>

<table>
    <tr>
<th></th>
<th>Žingsniai</th>
<th>Emocijų suvokimo ir raiškos srities pasiekimai</th>
</tr>
	<tr>
        <td class="darker" rowspan="3"><div class='rotate'>0–3 metai</div></td>
        <td class="step-center">1-asis žingsnis</td>
		<td><ul><li>Mimika, kūno judesiais ir garsais išreiškia džiaugsmą, liūdesį, baimę, pyktį. Patiria
išsiskyrimo su tėvais nerimą ir džiaugsmą jiems sugrįžus. Išreiškia nerimą,
pamatęs nepažįstamą žmogų. </li>
<li>Atspindi kitų vaikų emocijų raišką (kartu juokiasi, jei juokiasi kitas, nusimena, jei
kitas verkia).</ul></td>
    </tr>
		<tr><td class="step-center">2-asis žingsnis</td><td><ul><li>Džiaugsmą, liūdesį, baimę, pyktį reiškia skirtingu intensyvumu (nuo silpno
nepatenkinto niurzgėjimo iki garsaus rėkimo). Emocijos pastovesnės, tačiau dar
būdinga greita nuotaikų kaita. 
<li>Atpažįsta kito vaiko ar suaugusiojo džiaugsmo, liūdesio, pykčio emocijų išraiškas. </ul></td></tr>
		<tr><td class="step-center">3-iasis žingsnis</td><td><ul><li>Pradeda atpažinti, ką jaučia, turi savus emocijų raiškos būdus. Pradeda vartoti
emocijų raiškos žodelius ir emocijų pavadinimus. </li>
<li>Pastebi kitų žmonių emocijų išraišką, atpažįsta aiškiausiai reiškiamas emocijas
ir į jas skirtingai reaguoja (pasitraukia šalin, jei kitas piktas; glosto, jei kitas nuliūdęs).
</ul></td></tr>
	<tr>
        <td class="darker" rowspan="3"><div class='rotate'>4–6 metai</div></td>
        <td class="step-center">4-asis žingsnis</td>
		<td><ul><li>Pradeda suprasti, kad skirtingose situacijose (per gimimo dieną, susipykus su
draugu) jaučia skirtingas emocijas, kad jas išreiškia mimika, balsu, veiksmais, poza.
Pavadina pagrindines emocijas. 
<li>Atpažįsta kitų emocijas pagal veido išraišką, elgesį, veiksmus. Geriau supranta
kitų emocijas ir jausmus, dažnai tinkamai į juos reaguoja (pvz., stengiasi
paguosti, padėti).
<li>Pradeda suprasti, kad jo ir kitų emocijos gali skirtis (jam linksma, o kitam tuo pat
metu liūdna). </ul> </td>
    </tr>
		<tr><td class="step-center">5-asis žingsnis</td><td><ul><li>Atpažįsta bei pavadina savo jausmus ir įvardija situacijas, kuriose jie kilo. 
<li>Vis geriau supranta ne tik kitų jausmus, bet ir situacijas, kuriose jie kyla (pakvie-
čia žaisti nuliūdusį vaiką, kurio į žaidimą nepriėmė kiti).
<li>Pradeda kalbėtis apie jausmus su kitais – pasako ar paklausia, kodėl pyksta,
kodėl verkia. </ul></td></tr>
		<tr><td class="step-center">6-asis žingsnis</td><td><ul><li>Apibūdina savo jausmus, pakomentuoja juos sukėlusias situacijas bei priežastis. 
<li>Beveik neklysdamas iš veido mimikos, balso, kūno pozos nustato, kaip jaučiasi kitas,
pastebi nuskriaustą, nusiminusį ir dažniausiai geranoriškai stengiasi jam padėti.
<li>Pradeda kalbėtis apie tai, kas gali padėti pasijusti geriau, jei esi nusiminęs, piktas. </ul></td>
</tr>
<tr class="darker">
<td colspan="2">Vertybinė nuostata</td>
<td colspan="1">Domisi savo ir kitų emocijomis bei jausmais.</td>
</tr>
<tr class="darker">
<td colspan="2">Esminis gebėjimas</td>
<td colspan="1">Atpažįsta bei įvardina savo ir kitų emocijas ar jausmus, jų priežastis, įprastose
situacijose emocijas ir jausmus išreiškia tinkamais, kitiems priimtinais būdais,
žodžiais ir elgesiu atliepia kito jausmus (užjaučia, padeda).</td>
</tr>
<tr>
        <td class='darker' rowspan="1"><div class="rotate"></div></td>
        <td class="step-center">7-asis žingsnis</td>
		<td><ul><li>Domisi savo ir kitų emocijomis, jausmais bei jų raiška.
<li>Atpažįsta ir įvardija ne tik savo jausmus, bet ir nuotaikas bei jų priežastis. 
<li>Atpažįsta kitų emocijas ar jausmus, bando į juos atsiliepti (paguosti, užjausti),
keisti savo elgesį (susilaikyti, neskaudinti, atsižvelgti į kito norus). </ul></td>
    </tr>
</table>

</div>

<div class="dialog" id="dialog-4" data-title="4. SAVIREGULIACIJA IR SAVIKONTROLĖ">
   <p>Vaikystėje savikontrolei aktualūs du procesai: asmeninė vidinė kontrolė ir išorinė kontrolė.
<p><strong>Asmeninės vidinės kontrolės jausmas –</strong> tai jausmas, kad <strong> mes patys kontroliuojame savo gyvenimo
aplinkybes. Išorinės kontrolės jausmas –</strong> manymas, kad <strong> mūsų gyvenimą valdo</strong> sėkmingai ar
nesėkmingai susiklosčiusių aplinkybių visuma ar <strong>kitų veiksmai.</strong> Ikimokyklinio amžiaus vaikai iš pradžių
natūraliai priklauso nuo suaugusiųjų, t. y. išorinės kontrolės, tačiau ugdydamiesi jie vis labiau suvokia, kad
patys gali kontroliuoti daugelį savo gyvenimo situacijų.
<p>Savireguliacijos ir savikontrolės srityje vaikui ugdantis tobulėja:
<ul><li>gebėjimas nusiraminti, atsipalaiduoti, 
<li>jausmų raiška tinkamais būdais, jausmų raiškos kontrolė,
<li>gebėjimas laikytis susitarimų, taisyklių.</ul>

<table>
    <tr>
<th></th>
<th>Žingsniai</th>
<th>Savireguliacijos ir savikontrolės srities pasiekimai</th>
</tr>
	<tr>
        <td class="darker" rowspan="3"><div class='rotate'>0–3 metai</div></td>
        <td class="step-center">1-asis žingsnis</td>
		<td><ul><li>Gerai jaučiasi įprastoje aplinkoje. Nusiramina kalbinamas, nešiojamas, supamas.</li>
<li>Tapatinasi su suaugusiojo, prie kurio yra prisirišęs, emocijomis. </ul></td>
    </tr>
		<tr><td class="step-center">2-asis žingsnis</td><td><ul><li>Išsigandęs, užsigavęs, išalkęs nusiramina suaugusiojo kalbinamas, glaudžiamas,
maitinamas. Pats ieško nusiraminimo: apsikabina minkštą žaislą arba čiulpia
čiulptuką, šaukia suaugusįjį, ropščiasi ant kelių.
<li>Pradeda valdyti savo emocijų raišką ir veiksmus, reaguodamas į juo besirūpinančio
suaugusiojo veido išraišką, balso intonaciją, žodžius. </ul></td></tr>
		<tr><td class="step-center">3-iasis žingsnis</td><td><ul><li>Yra ramus ir rodo pasitenkinimą kasdiene tvarka bei ritualais. Jeigu kas nepatinka,
nueina šalin, atsisako bendros veiklos. </li>
<li>Geriau valdo savo emocijų raišką ir veiksmus, reaguodamas į juo besirūpinančio
suaugusiojo veido išraišką, balso intonaciją, žodžius. Žaisdamas kalba su savimi,
nes kalba padeda sutelkti dėmesį, kontroliuoti savo elgesį. Išbando įvairius
konfliktų sprendimo ar savo interesų gynimo būdus (rėkia, neduoda žaislo, pasako
suaugusiajam ir kt.).
<li>Bando laikytis suaugusiojo prašymų ir susitarimų.
</ul></td></tr>
	<tr>
        <td class="darker" rowspan="3"><div class='rotate'>4–6 metai</div></td>
        <td class="step-center">4-asis žingsnis</td>
		<td><ul><li>Nusiramina kalbėdamas apie tai, kas jį įskaudino, ir girdėdamas suaugusiojo
komentarus. 
<li>Pradeda valdyti savo emocijų raiškos intensyvumą priklausomai nuo situacijos
(pvz., ramioje aplinkoje džiaugsmą reiškia santūriau). Paklaustas ramioje situacijoje
pasako galimas savo ar kito asmens netinkamo elgesio pasekmes.
<li>Nuolat primenant ir sekdamas suaugusiojo bei kitų vaikų pavyzdžiu laikosi grupėje
numatytos tvarkos, susitarimų ir taisyklių. Žaisdamas stengiasi laikytis žaidimo
taisyklių. </ul> </td>
    </tr>
		<tr><td class="step-center">5-asis žingsnis</td><td><ul><li>Nusiramina, atsipalaiduoja, klausydamasis ramios muzikos, pabuvęs vienas,
kalbėdamasis su kitais
<li>Vis dažniau jausmus išreiškia mimika ir žodžiais, o ne veiksmais. Ramioje situacijoje
sugalvoja kelis konflikto sprendimo būdus, numato jų taikymo pasekmes.
<li>Retkarčiais primenamas laikosi grupėje numatytos tvarkos, susitarimų ir taisyklių.
Pats primena kitiems tinkamo elgesio taisykles ir bando jų laikytis be suaugusiųjų
priežiūros. </ul></td></tr>
		<tr><td class="step-center">6-asis žingsnis</td><td><ul><li>Pats taiko įvairesnius nusiraminimo, atsipalaidavimo būdus (pastovi prie akvariumo
su žuvytėmis, klauso pasakos naudodamasis ausinėmis ir kt.).
<li>Bando susilaikyti nuo netinkamo elgesio jį provokuojančiose situacijose, ieško
taikių išeičių, kad neskaudintų kitų. Stengiasi suvaldyti savo pyktį, įniršį.
<li>Supranta susitarimų, taisyklių prasmę bei naudingumą ir dažniausiai savarankiš-
kai jų laikosi. Lengvai priima dienos ritmo pasikeitimus.</ul></td>
</tr>
<tr class="darker">
<td colspan="2">Vertybinė nuostata</td>
<td colspan="1">Nusiteikęs valdyti emocijų raišką ir elgesį.</td>
</tr>
<tr class="darker">
<td colspan="2">Esminis gebėjimas</td>
<td colspan="1">Laikosi susitarimų, elgiasi mandagiai, taikiai, bendraudamas su kitais bando
kontroliuoti savo žodžius ir veiksmus (suvaldo pyktį, neskaudina kito),
įsiaudrinęs geba nusiraminti. </td>
</tr>
<tr>
        <td class='darker' rowspan="1"><div class="rotate"></div></td>
        <td class="step-center">7-asis žingsnis</td>
		<td><ul><li>Susijaudinęs, išsigandęs, sutrikęs bando nusiraminti ir ieškoti pagalbos.
<li>Taiko kelis skirtingus kitiems priimtinus emocijų ir jausmų reiškimo būdus.
Numato, kaip jaustųsi pats ar kitas asmuo įvairiose situacijose.
<li>Supranta, kad skirtingose vietose (darželyje, mokykloje ir kt.) yra kitokia tvarka,
kitos taisyklės, linkęs jas suprasti ir jų laikytis.</ul></td>
    </tr>
</table>

</div>

<div class="dialog" id="dialog-5" data-title="5. SAVIVOKA IR SAVIGARBA">
   <p>Vaiko savivoka ir savigarba yra svarbiausias jo asmenybinės raidos komponentas.
<p>Jį sudaro keli pagrindiniai aspektai: savivoka, savivaizdis, savigarba. <strong>Savivoka –</strong> tai savo buvimo
(aš esu, buvau, būsiu), atskirumo nuo kitų, unikalumo, asmeninio, lytinio, tautinio tapatumo jausmas. 
<p><strong>Savivaizdis –</strong> tai savo kūno, fizinės išvaizdos, jausmų, minčių, norų, ketinimų, savybių, gebėjimų, veiklos,
ryšių su šeimos, grupės, bendruomenės nariais supratimas ir gebėjimas apie juos kalbėti. <strong>Savigarba –</strong> tai
santykis su savimi, pasitikėjimas savimi ir savo gebėjimais, didžiavimasis savimi ir tikėjimas, kad kiti tave
vertina palankiai.
<p>Ugdantis savivoką ir savigarbą stiprėja:
<ul><li>vaiko asmeninio tapatumo jausmas,
<li>bendrumo su šeima, grupe jausmas, tautinio tapatumo jausmas,
<li>pozityvus savęs vertinimas.</ul>

<table>
    <tr>
<th></th>
<th>Žingsniai</th>
<th>Savivokos ir savigarbos srities pasiekimai</th>
</tr>
	<tr>
        <td class="darker" rowspan="3"><div class='rotate'>0–3 metai</div></td>
        <td class="step-center">1-asis žingsnis</td>
		<td><ul><li>Reaguoja į pasakytą savo vardą. Pradeda jausti savo kūno buvimą – apžiūrinė-
ja, tyrinėja savo rankas, kojas, stebi jų judėjimą. Apžiūrinėja, liečia kitą vaiką,
tyrinėja jo kūną.</li>
<li>Jaučiasi svarbus kitiems – šypsosi, krykštauja, kai aplinkiniai maloniai bendrauja
su juo.</ul></td>
    </tr>
		<tr><td class="step-center">2-asis žingsnis</td><td><ul><li>Turi savo kūno išorės vaizdinius – atpažįsta save neseniai darytose nuotraukose,
savo atvaizdą veidrodyje, pavadina kelias kūno dalis. Supranta, ką ir kaip gali
padaryti pats, išreiškia savo norus, pyksta, kai suaugusysis neleidžia to daryti.
<li>Džiaugiasi didėjančiomis savo galimybėmis judėti, atlikti veiksmus, kalbėti, tikisi
juo besirūpinančio suaugusiojo pritarimo, palaikymo, pagyrimo.</ul></td></tr>
		<tr><td class="step-center">3-iasis žingsnis</td><td><ul><li>Kalba pirmuoju asmeniu: „aš noriu“, „mano“. Savo „aš“ sieja su savo veikla ir
daiktų turėjimu – pasako, ką daro, ką turi. Pasako, kas jis yra – berniukas ar mergaitė,
atskiria berniukus nuo mergaičių, pavadina 5–6 kūno dalis. </li>
<li>Didžiuojasi tuo, ką turi ir ką gali daryti, tikisi, kad juo besirūpinantys suaugusieji
ir kiti vaikai jį mėgsta ir priima. 
</ul></td></tr>
	<tr>
        <td class="darker" rowspan="3"><div class='rotate'>4–6 metai</div></td>
        <td class="step-center">4-asis žingsnis</td>
		<td><ul><li>Supranta, kad turi nuo kitų atskirą savo norų, ketinimų, jausmų pasaulį. Pasako,
kaip jaučiasi, ko nori jis pats ir kaip jaučiasi, ko nori kitas asmuo. Supranta, kad
suaugęs žmogus negalėjo matyti to, ką jis matė, ką darė arba kas atsitiko, jeigu
nebuvo kartu (tėvams pasakoja, ką veikė darželyje ir kt.). 
<li>Mano, kad yra geras, todėl kiti jį mėgsta, palankiai vertina.
</ul> </td>
    </tr>
		<tr><td class="step-center">5-asis žingsnis</td><td><ul><li>Supranta, kad jis buvo, yra ir visada bus tas pats asmuo: atpažįsta save kūdikystės
nuotraukose, apibūdina savo išvaizdą, teisingai pasako, kad suaugęs bus
vyras (moteris), tėvelis (mamytė). 
<li>Jaučiasi esąs šeimos, vaikų grupės narys, kalba apie šeimą, draugus.
<li>Savęs vertinimas nepastovus, priklauso nuo tuo metu išsakyto suaugusiojo
vertinimo, siekia kitų dėmesio, palankių vertinimų.</ul></td></tr>
		<tr><td class="step-center">6-asis žingsnis</td><td><ul><li>Vis geriau suvokia savo norus, jausmus, savybes, gebėjimus, šeimą, bendruomenę,
Tėvynę. Ima suvokti save, kaip galintį daryti įtaką kitam (pralinksminti,
padėti ir kt.) ir atsakingai pasirinkti (ką veikti, kaip elgtis, aktyviai dalyvauti
priimant su jo gyvenimu ir veikla susijusius sprendimus ar kt.). Juokiasi iš savo
klaidų ar mažų nelaimių, jeigu jos nesukėlė rimtų pasekmių. 
<li>Save apibūdina, nusakydamas fizines ir elgesio savybes, priklausymą šeimai,
grupei, gali pasakyti savo tautybę. 
<li>Save ir savo gebėjimus vertina teigiamai. Stebi ir atpažįsta kitų palankumo ir
nepalankumo jam ženklus (pasakytus žodžius, kvietimą žaisti kartu ir kt.). </ul></td>
</tr>
<tr class="darker">
<td colspan="2">Vertybinė nuostata</td>
<td colspan="1">Save vertina teigiamai.</td>
</tr>
<tr class="darker">
<td colspan="2">Esminis gebėjimas</td>
<td colspan="1">Supranta savo asmens tapatumą („aš esu, buvau, būsiu“), pasako, kad yra
berniukas / mergaitė, priskiria save savo šeimai, grupei, bendruomenei, pasitiki
savimi ir savo gebėjimais, palankiai kalba apie save, tikisi, kad kitiems jis patinka,
supranta ir gina savo teises būti ir žaisti kartu su kitais.</td>
</tr>
<tr>
        <td class='darker' rowspan="1"><div class="rotate"></div></td>
        <td class="step-center">7-asis žingsnis</td>
		<td><ul><li>Pasitiki savimi ir savo gebėjimais.
<li>Supranta savo augimą, pasako, kaip atrodo, kuo domisi, ką veikia.
<li>Jaučiasi esąs savo šeimos, grupės narys, priskiria save giminei (močiutės,
seneliai, tetos, pusbroliai, pusseserės), gimtinei.
<li>Ima suprasti, ką jis pats gali padaryti, pakeisti, o kas nuo jo norų ir pastangų
nepriklauso.
<li>Mokosi saugoti savo privatumą, siekia kitų palankumo, yra tolerantiškas
kitokiam (kalbančiam kita kalba, kitokios išvaizdos vaikui ir kt.).</ul></td>
    </tr>
</table>

</div>

<div class="dialog" id="dialog-6" data-title="6. SANTYKIAI SU SUAUGUSIAISIAIS">
Vaiko santykiai su suaugusiaisiais <strong>apima šiltus artimus emocinius ryšius</strong>.
<p>Vaikas <strong>prisiriša prie jam artimų suaugusiųjų</strong>, su jais jaučiasi saugus, juos gerbia, myli, tikisi pagalbos
sudėtingose situacijose. Vėliau vaikas <strong>kuria partnerystės santykius su suaugusiuoju</strong>. Vaikas mokosi
suprasti suaugusįjį, tuo pačiu mokosi ir atsiskirti nuo jo, turėti savą pasaulį.
<p>Santykių su suaugusiaisiais srityje vaikui ugdantis tobulėja:
<ul><li>gebėjimas atsiskirti nuo tėvų ir pasitikėti pedagogais, abipusė pagarba,
<li>gebėjimas mokytis palaikyti partneriškus santykius su pedagogais,
<li>žinojimas, kaip saugiai elgtis su nepažįstamais suaugusiaisiais.</ul>

<table>
    <tr>
<th></th>
<th>Žingsniai</th>
<th>Santykių su suaugusiaisiais srities pasiekimai</th>
</tr>
	<tr>
        <td class="darker" rowspan="3"><div class='rotate'>0–3 metai</div></td>
        <td class="step-center">1-asis žingsnis</td>
		<td><ul><li>Verkia atsiskirdamas nuo tėvų, tačiau padedamas pedagogo pamažu nurimsta
ir įsitraukia į veiklą.</li>
<li>Atpažįsta juo besirūpinantį suaugusįjį, džiaugiasi jį pamatęs, atsako jam kalbinamas,
žaidinamas, siekia būti greta.</ul></td>
    </tr>
		<tr><td class="step-center">2-asis žingsnis</td><td><ul><li>Sunkiai atsiskiria nuo mamos, tėčio ar globėjo. 
<li>Akivaizdžiai parodo prieraišumą prie juo besirūpinančio suaugusiojo. Mėgsta
žaisti kartu su juo, stebi ir mėgdžioja jo žodžius, veiksmus. Prieš ką nors darydamas
pažiūri į suaugusiojo veidą, laukdamas pritarimo ar nepritarimo ženklų,
atpažįsta suaugusiojo emocijas, jausmus. Dažniausiai vykdo jam suprantamus
suaugusiojo prašymus, kreipiasi į jį pagalbos. 
<li>Bijo nepažįstamų žmonių, nežinomos aplinkos, neįprastų žaislų.</ul></td></tr>
		<tr><td class="step-center">3-iasis žingsnis</td><td><ul><li>Lengviau nei antraisiais metais atsiskiria nuo tėvų.</li>
<li>Drąsiai veikia, rizikuoja, išbando ką nors nauja, kai šalia yra juo besirūpinantis suaugusysis.
Mėgdžioja, tačiau žaidime savaip pertvarko suaugusiųjų veiksmus, žodžius,
intonacijas. Nori veikti savarankiškai ir tikisi suaugusiojo palaikymo, pagyrimo.
Ne visada priima suaugusiojo pagalbą, kartais užsispiria.
<li>Ramiai stebi nepažįstamus žmones, kai auklėtojas yra šalia jo arba matomas
netoliese.
</ul></td></tr>
	<tr>
        <td class="darker" rowspan="3"><div class='rotate'>4–6 metai</div></td>
        <td class="step-center">4-asis žingsnis</td>
		<td><ul><li>Lengvai atsiskiria nuo tėvų ar globėjų. Grupėje jaučiasi saugus, rodo pasitikėjimą
grupės auklėtojais, supranta jų jausmus, bendradarbiauja su jais: guodžiasi, kalbasi,
klausia, tariasi. Paklaustas suaugusiajam pasako savo nuomonę. Dažniausiai
stengiasi laikytis suaugusiųjų nustatytos tvarkos, priima jų pagalbą, pasiūlymus
bei vykdo individualiai pasakytus prašymus. Mėgsta ką nors daryti kartu su suaugusiuoju.
<li>Kalbasi, ką nors veikia su nepažįstamais žmonėmis, kai auklėtojas yra šalia jo arba
matomas netoliese.
</ul> </td>
    </tr>
		<tr><td class="step-center">5-asis žingsnis</td><td><ul><li>Rodo, prašo, siūlo, aiškina, nurodinėja, įtraukdamas suaugusįjį į savo žaidimus,
bendrą veiklą, pokalbius apie savijautą ir elgesį. Priima su veikla susijusius suaugusiojo
pasiūlymus. Tikrina suaugusiojo išsakytas leistino elgesio ribas – atsiklausia,
derasi, pasako, kaip pasielgė kitas, ir laukia komentarų. Dažniausiai laikosi
sutartų taisyklių, suaugusiojo prašymų, pasiūlymų, tačiau stipriai supykęs,
išsigandęs, susijaudinęs gali priešintis suaugusiajam. 
<li>Drąsiai bendrauja su mažiau pažįstamais ar nepažįstamais žmonėmis grupėje,
salėje ar įstaigos kieme.
</ul></td></tr>
		<tr><td class="step-center">6-asis žingsnis</td><td><ul><li>Nusiteikęs geranoriškai, pagarbiai, mandagiai bendrauti su suaugusiaisiais. Tariasi,
diskutuoja su jais dėl dienotvarkės ir elgesio taisyklių, teikia pasiūlymus, stengiasi
laikytis susitarimų, nors kartais su suaugusiuoju bendrauja priešiškai. Kasdienėse
situacijose bando tinkamu būdu išsakyti priešingą nei suaugusiojo nuomonę. 
<li>Paprašytas paaiškina, kodėl negalima bendrauti su nepažįstamais žmonėmis, kai
šalia nėra juo besirūpinančio suaugusiojo. Žino, į ką galima kreiptis pagalbos
pasimetus, nutikus nelaimei. </ul></td>
</tr>
<tr class="darker">
<td colspan="2">Vertybinė nuostata</td>
<td colspan="1">Nusiteikęs geranoriškai bendrauti ir bendradarbiauti su suaugusiaisiais.</td>
</tr>
<tr class="darker">
<td colspan="2">Esminis gebėjimas</td>
<td colspan="1">Pasitiki pedagogais, juos gerbia, ramiai jaučiasi su jais kasdienėje ir neįprastoje
aplinkoje, iš jų mokosi, drąsiai reiškia jiems savo nuomonę, tariasi, derasi; žino,
kaip reikia elgtis su nepažįstamais suaugusiaisiais.</td>
</tr>
<tr>
        <td class='darker' rowspan="1"><div class="rotate"></div></td>
        <td class="step-center">7-asis žingsnis</td>
		<td><ul><li>Pasitiki savimi ir savo gebėjimais.
<li>Geranoriškai ir pagarbiai bendrauja ir bendradarbiauja su suaugusiaisiais. 
<li>Pasitiki pedagogais, artimaisiais, juos gerbia, ramiai jaučiasi su jais neįprastoje
aplinkoje, iš jų mokosi.
<li>Patys pasiūlo suaugusiesiems įdomią bendrą veiklą, išsako savo nuomonę,
siekia susitarimų, prašo pagalbos. 
<li>Domisi suaugusiojo jausmais ir savijauta, užjaučia, pagaili, siūlo savo pagalbą. 
<li>Žino, kaip reikia elgtis su nepažįstamais suaugusiaisiais.</ul></td>
    </tr>
</table>

</div>

<div class="dialog" id="dialog-7" data-title="7. SANTYKIAI SU BENDRAAMŽIAIS">
Ikimokyklinio amžiaus vaikai mokosi užmegzti artimus ryšius su bendraamžiais.
<p><strong>Vaikai simpatizuoja vienas kitam.</strong>Simpatizavimas yra vienpusis ryšys, kai vienas vaikas domisi
kitu, yra jam palankus. Vaikai<strong>mokosi užmegzti ir palaikyti draugystę su vienu ar keliais vaikais</strong>. Jie
mokosi suprasti draugą ir jam atsiskleisti, atrasti bendrų interesų, žaisti bendrus žaidimus bei sėkmingai
spręsti kilusius nesutarimus. Vaikai <strong>mokosi palaikyti geranoriškus santykius</strong> su visais grupės vaikais.

<p>Santykių su bendraamžiais srityje vaikui ugdantis tobulėja:
<ul><li>gebėjimas užmegzti geranoriškus santykius su kitais vaikais,
<li>gebėjimas mokytis bendrauti ir bendradarbiauti, spręsti tarpusavio nesutarimus, 
<li>gebėjimas užmegzti ir palaikyti artimesnius asmeninius santykius su vienu ar keliais vaikais.</ul>

<table>
    <tr>
<th></th>
<th>Žingsniai</th>
<th>Santykių su bendraamžiais srities pasiekimai</th>
</tr>
	<tr>
        <td class="darker" rowspan="3"><div class='rotate'>0–3 metai</div></td>
        <td class="step-center">1-asis žingsnis</td>
		<td><ul><li>Patinka žiūrėti į kitus kūdikius, būti šalia kitų vaikų, juos liesti, mėgdžioti jų
veido išraišką, veiksmus.</ul></td>
    </tr>
		<tr><td class="step-center">2-asis žingsnis</td><td><ul><li>Mėgsta žaisti greta kitų vaikų, stebėti jų veiklą. Jiems šypsosi, mėgdžioja jų
judesius, veiksmus, ką nors pasako. Gali duoti žaislą kitam, jį imti iš kito, tačiau
supykęs gali atimti žaislą iš kito, jam suduoti.</ul></td></tr>
		<tr><td class="step-center">3-iasis žingsnis</td><td><ul><li>Ieško bendraamžių draugijos. Žaidžia greta, trumpai pažaidžia su kitu vaiku,
trumpam įsitraukia į kito vaiko žaidimą. 
<li>Bendrauja mimika, judesiais, veiksmais, dažniau kalbasi su kitu vaiku, pakaitomis
atlieka veiksmus su tuo pačiu žaislu. Audringai reiškia teises į savo daiktus,
žaislus, nori kito vaiko jam patinkančio žaislo.
<li>Gali simpatizuoti kuriam nors vaikui.
</ul></td></tr>
	<tr>
        <td class="darker" rowspan="3"><div class='rotate'>4–6 metai</div></td>
        <td class="step-center">4-asis žingsnis</td>
		<td><ul><li>Kartu su bendraamžiais žaidžia bendrus žaidimus (kviečia žaisti, priima, prašosi
priimamas į žaidimą). 
<li>Žaisdamas mėgdžioja kitus vaikus, supranta jų norus, stengiasi suprasti kita kalba
kalbančio vaiko sumanymus. Tariasi dėl vaidmenų, siužeto, žaislų. Padedamas
suaugusiojo, palaukia savo eilės, dalijasi žaislais, priima kompromisinį pasiūlymą.
<li>Gali turėti vieną ar kelis nenuolatinius žaidimų partnerius. Su jais lengvai susipyksta
ir susitaiko.
</ul> </td>
    </tr>
		<tr><td class="step-center">5-asis žingsnis</td><td><ul><li>Sėkmingai įsitraukia į vaikų grupę ir nuolat kartu žaidžia. 
<li>Geranoriškai veikia kartu su kitais, siūlydamas savo sumanymą ar priimdamas kitų
sumanymą, fantazuodamas. Tikslingai atsineša žaislą iš namų bendram žaidimui
su žaidimo draugu. Paprašius kitam vaikui, duoda pažaisti savo žaislu arba žaidžia
juo paeiliui. Noriai žaidžia su vaikais iš kitos kultūrinės ar socialinės aplinkos, natūraliai
priima vaikų skirtumus. Gali padėti kitam vaikui. Pats randa nesutarimo,
konflikto sprendimo būdą arba prašo suaugusiojo pagalbos. 
<li>Gali turėti draugą arba kelis kurį laiką nesikeičiančius žaidimų partnerius.
</ul></td></tr>
		<tr><td class="step-center">6-asis žingsnis</td><td><ul><li>Rodo iniciatyvą bendrauti ir bendradarbiauti su kitais vaikais, palaikyti su jais
gerus santykius, domisi skirtumais tarp vaikų ir juos toleruoja. 
<li>Taikiai diskutuoja, tariasi, derasi su kitais vaikais dėl žaidimų sumanymų ir
veiklos. Dalijasi žaislais ir kovoja už kitų teisę žaisti paeiliui. Siekdamas rasti
kompromisą, įsitraukia į derybų procesą. Supranta, kad grupė vaikų, norėdama
veikti sutartinai, turi susitarti dėl visiems priimtino elgesio. Supranta, koks
elgesys yra geras ar blogas ir kodėl. Suvokia savo veiksmų pasekmes sau ir
kitiems. 
<li>Turi draugą arba kelis nuolatinius žaidimų partnerius. Palaiko ilgalaikę draugystę
mažiausiai su vienu vaiku.</ul></td>
</tr>
<tr class="darker">
<td colspan="2">Vertybinė nuostata</td>
<td colspan="1">Nusiteikęs geranoriškai bendrauti ir bendradarbiauti su bendraamžiais.</td>
</tr>
<tr class="darker">
<td colspan="2">Esminis gebėjimas</td>
<td colspan="1">Supranta, kas yra gerai, kas blogai, draugauja bent su vienu vaiku, palankiai
bendrauja su visais (supranta kitų norus, dalijasi žaislais, tariasi, užjaučia,
padeda), suaugusiojo padedamas supranta savo žodžių ir veiksmų pasekmes sau
ir kitiems.</td>
</tr>
<tr>
        <td class='darker' rowspan="1"><div class="rotate"></div></td>
        <td class="step-center">7-asis žingsnis</td>
		<td><ul>
<li>Nusiteikęs susipažinti, susidraugauti, geranoriškai bendrauti ir bendradarbiauti
su bendraamžiais.
<li>Supranta, kas yra gerai, kas blogai, stengiasi elgtis pagal savą gero elgesio
supratimą. 
<li>Draugauja bent su vienu vaiku, palankiai, mandagiai, tolerantiškai bendrauja ir
bendradarbiauja su visais (dalijasi žaislais, tariasi, supranta kitų norus, derina
veiksmus). 
<li>Supranta savo žodžių ir veiksmų pasekmes sau ir kitiems.
<li>Pastebi ir priima kitų draugiškumo, palankumo ženklus, gerbia kitus vaikus,
išklauso jų nuomonę, iš jų mokosi.</ul></td>
    </tr>
</table>

</div>

<div class="dialog" id="dialog-8" data-title="8. SAKYTINĖ KALBA">
<strong>Sakytinė kalba –</strong> tai pagrindinė <strong>žmonių bendravimo priemonė</strong>.
<p>Ja išsakomi norai, reikalavimai, prašymai, emocijos, jausmai ir kt. Kalbėdamiesi gauname ir perduodame
informaciją, samprotaujame, darome išvadas, žodžiai padeda įsiminti ir saugoti žinias apie pasaulį.
<strong>Vaiko sakytinė kalba</strong> – tai vaiko <strong>klausymas ir kalbėjimas</strong>. Klausydamas ir kalbėdamas vaikas pratinasi
išgirsti ir suprasti, ką sako kitas, mokosi kalbėti pats.
<p>Sakytinės kalbos srityje vaikui ugdantis tobulėja:
<ul><li>aplinkinių kalbėjimo, skaitymo klausymasis, 
<li>kalbėjimo atpažinimas ir supratimas,
<li>natūralus vaiko kalbėjimas su suaugusiaisiais ir vaikais apie savo patirtį ir išgyvenimus,
<li>vaiko kalbėjimas su suaugusiaisiais ir vaikais apie supančią aplinką, jos objektus, įvykius,
<li>kalbėjimas laikantis perprastų kalbos taisyklių,
<li>tautosakos ir grožinės literatūros kūrinėlių deklamavimas, sekimas, pasakojimas.</ul>

<table>
    <tr>
<th></th>
<th>Žingsniai</th>
<th>Sakytinės kalbos srities pasiekimai</th>
</tr>
	<tr>
        <td class="darker" rowspan="3"><div class='rotate'>0–3 metai</div></td>
        <td class="step-center">1-asis žingsnis</td>
		<td><strong>Klausymas</strong><ul><li>Įdėmiai klausosi suaugusiojo. Skiria griežtą ir malonų kalbinančio suaugusiojo toną.
		<li>Supranta elementarius kalbinančiojo klausimus ir prašymus. Atpažįsta artimiausios
aplinkos garsus. Džiaugiasi įvairiais garsais ir ritmais. Supranta savo ir
artimųjų vardus, artimiausios aplinkos daiktų ir reiškinių pavadinimus. Supranta
veiksmų pavadinimus, geba veiksmais atsakyti į klausimus.</ul><p><strong>Kalbėjimas</strong>
		<ul><li>Komunikavimui vartoja įvairius garsus ir judesius: daug čiauška, kartoja,
mėgdžioja jam tariamus garsus ir skiemenis.
<li>Vartoja kelis trumpus žodelius objektams, veiksmams įvardyti, norams išsakyti,
palydi juos judesiu.</ul></td>
    </tr>
		<tr><td class="step-center">2-asis žingsnis</td><td><strong>Klausymas</strong><ul><li>Klausosi ir kalba, mimika, gestais reaguoja į suaugusiųjų ir vaikų kalbėjimą. 
		<li>Supranta vaikų ir suaugusiųjų kalbą apie artimiausios aplinkos objektus, reiš-
kinius, santykius, nesudėtingus trumpus tekstukus: žaidinimus, eilėraštukus,
pasakas, pasakojimus, su dienotvarke susijusius paaiškinimus, prašymus, paprastus
klausimus. Supranta ir greitai mokosi paprastų naujų žodžių. Išklauso ir
supranta du vienas po kito išsakomus prašymus, kvietimus.</ul>
<p><strong>Kalbėjimas</strong>
<ul><li>Noriai dalyvauja pokalbiuose. Mėgdžiojimu, žodelių pakartojimais, veiksmais,
mimika ir pantomimika dalyvauja paprastuose žodiniuose žaidimuose.</li>
<li>Dviejų trijų žodžių sakiniais kalba apie tai, ką mato ir girdi, kas atsitiko, ko nori.</li>
<li>Suaugusiojo padedamas kartoja girdėtus trumpus kūrinėlius.</li>
</ul></td></tr>
		<tr><td class="step-center">3-iasis žingsnis</td><td><strong>Klausymas</strong><ul><li>Klausosi skaitomų ir pasakojamų kūrinėlių, naujų žodžių. 
		<li>Išklauso, supranta ir reaguoja į kelis vienas paskui kitą sekančius prašymus,
siūlymus, patarimus.</ul><p><strong>Kalbėjimas</strong><ul><li>3–4 žodžių sakiniais kalba ir klausinėja apie save, savo norus, poreikius, išgyvenimus.
Pradeda mėgdžioti suaugusiųjų kalbėseną. Sako „ačiū“, „prašau“.
<li>Kalba ir klausinėja apie tai, ką matė ir girdėjo, apie aplinkos objektus, jų savybes,
įvykius, net jei jų dabar ir nemato. Domisi laidomis, animaciniais filmais vaikams,
kalba apie juos. Vienu ar keliais žodžiais atsako į elementarius klausimus.
<li>Kalba kelių žodžių sakiniais, žodžius derina pagal giminę, skaičių, linksnį.
<li>Kartu su suaugusiuoju deklamuoja eilėraštukus, užbaigia žinomas pasakas,
eilėraščius.</ul></td></tr>
	<tr>
        <td class="darker" rowspan="3"><div class='rotate'>4–6 metai</div></td>
        <td class="step-center">4-asis žingsnis</td>
		<td><strong>Klausymas</strong><ul><li>Klausosi aplinkinių pokalbių, sekamų, pasakojamų, skaitomų, deklamuojamų kū-
rinių literatūrine kalba, tarmiškai. 
		<li>Pradeda išklausyti, suprasti ir reaguoti į tai, ką jam sako, aiškina suaugusysis ar
vaikas. Stengiasi suprasti kita kalba kalbančių vaikų norus, pasiūlymus.
</ul><p><strong>Kalbėjimas</strong><ul><li>Kalba pats sau, kalba kitam, klausinėja, užkalbina, prašo, pašaukia, kartais laikydamasis
elementarių kalbinio etiketo normų. Kalba, pasakoja apie tai, ką jaučia ir
jautė, veikia ir veikė. Žaidžia garsais ir žodžiais, kuria naujus žodžius. 
<li>Kalba, pasakoja apie tai, ką mato ir matė, girdi ir girdėjo, ką sužinojo, suprato,
vartodamas elementarius terminus, girdėtus naujus žodžius.
<li>Kalbėdamas vartoja paprastos konstrukcijos gramatiškai taisyklingus sakinius.
Taisyklingai taria daugumą gimtosios kalbos žodžių garsų. Padedant atpažįsta
žodyje kelis atskirus garsus.
<li>Deklamuoja trumpus eilėraščius, atkartoja trumpas pasakas ar apsakymus, pridė-
damas savo žodžių, pasakojimą palydėdamas gestais ir mimika.</ul></td>
    </tr>
		<tr><td class="step-center">5-asis žingsnis</td><td><strong>Klausymas</strong><ul><li>Klausosi įvairaus turinio tekstų (grožinių, publicistinių, enciklopedinių, informacinių)
apie aplinką, įvairius įvykius, reiškinius, klausosi gyvai, įrašų.
		<li>Supranta sudėtingesnio turinio tekstus. Supranta, kad į jį kreipiamasi ar kalbama
ne gimtąja kalba. 
</ul><p><strong>Kalbėjimas</strong><ul><li>Natūraliai kitiems kalba apie tai, ką žino, veikia, ko nori, tikisi, nesupratus paaiškina,
pakartoja. Kalbėdamas žiūri į akis. 
<li>Kalba, pasakoja apie tai, kas buvo nutikę, įvykę, tai siedamas su žmonėmis, tautos
gyvenimu, gamtos reiškiniais. Vartoja įvairią techniką, transporto priemones
bei prietaisus įvardijančius žodžius. Pasakoja, kalbasi apie matytus animacinius
filmus, televizijos laidas, žaistus kompiuterinius žaidimus. Bando susikalbėti su
kitakalbiu vaiku, pakartodamas jo kalbos vieną kitą žodį.
<li>Laisvai kalba sudėtiniais sakiniais, žodžius į sakinius jungia laikydamasis perprastų
kalbos taisyklių. Vartoja daugumą kalbos dalių (daiktavardžius, veiksmažodžius,
būdvardžius, prieveiksmius, prielinksnius ir kt.). Išgirsta pirmą ir paskutinį garsą jo
paties, tėvų, draugų vardažodžiuose, trumpuose žodžiuose.
<li>Seka girdėtas ir savo sukurtas pasakas, kuria įvairias istorijas, eilėraštukus, inscenizuoja.
Deklamuoja skaitomų pasakų eiliuotus intarpus.</ul></td></tr>
		<tr><td class="step-center">6-asis žingsnis</td><td><strong>Klausymas</strong><ul><li>Klausosi draugų ir savo kalbos įrašų, įvairių stilių tekstų, mįslių, erzinimų, pajuokavimų
bendrine kalba ir tarme.	
		<li>Supranta knygelės, pasakojimo, pokalbio turinį, įvykių eigą. Supranta pajuokavimus,
dviprasmybes, frazeologizmus, perkeltinę žodžių prasmę. Supranta artimiausioje
aplinkoje vartojamus kitos kalbos žodžius.
</ul><p><strong>Kalbėjimas</strong><ul><li>Kalba natūraliai, atsižvelgdamas į bendravimo situaciją, išsakydamas savo patirtį,
norus, svajones, svarstymus, kalba apie problemų sprendimą, vartoja mandagumo
bei vaizdingus žodžius (sinonimus, antonimus ir kt.), technologinius terminus
(mikrofonas, pelė, klaviatūra ir kt.). Bando susikalbėti su kitakalbiu vaiku,
suaugusiuoju. Garsiai svarsto savo planuojamos veiklos eigą, praneša apie tai
draugui, grupelei draugų, visai grupei. Klausinėja apie tai, kas išgirsta, matyta,
sugalvota, pajausta.
<li>Pasakoja, kalba apie aplinką, gamtos reiškinius, techniką, įvardydamas įvairias detales,
savybes, būsenas, vartodamas naujai išgirstus sudėtingesnės sandaros žodžius.
<li>Kalba taisyklingais sudėtingais sakiniais, vartoja pagrindines kalbos dalis. Išgirsta
žodžius, kurie panašiai skamba, bet turi skirtingą reikšmę. Išgirsta pirmą, paskutinį
ir žodžio viduryje esančius garsus. Skiria gimtosios kalbos žodžius nuo
išgirstų kitos kalbos žodžių.
<li>Komentuoja meno kūrinius, atpasakoja pasakas, padavimus, apsakymus, matytus
ir girdėtus per įvairias skaitmenines laikmenas (TV, DVD, CD). Kuria ir
pasakoja įvairius tekstus, mįsles, humoristines istorijas, deklamuoja savo sukurtus
kūrinėlius, žaidžia prasmingais ir beprasmiais žodžiais, bando juokauti,
kalba „ateivių“ kalbomis, „užsienio“ kalbomis. Keičia balso stiprumą, kalbėjimo
tempą, intonacijas ir kt. </ul></td>
</tr>
<tr class="darker">
<td colspan="2">Vertybinė nuostata</td>
<td colspan="1">Nusiteikęs išklausyti kitą ir išreikšti save bei savo patirtį kalba.</td>
</tr>
<tr class="darker">
<td colspan="2">Esminis gebėjimas</td>
<td colspan="1">Klausosi ir supranta kitų kalbėjimą, kalba su suaugusiaisiais ir vaikais, natūraliai,
laisvai išreikšdamas savo išgyvenimus, patirtį, mintis, intuityviai junta kalbos grožį.</td>
</tr>
<tr>
    <td class='darker' rowspan="1"><div class="rotate"></div></td>
    <td class="step-center">7-asis žingsnis</td>
	<td><strong>Klausymas</strong><ul><li>Nepertraukdamas klausosi draugų ir suaugusiųjų kalbos, pasakojimų, samprotavimų,
komentarų, instrukcijų bendraujant, planuojant veiklą, veikiant.
Klausosi TV ir radijo laidų vaikams.
	<li>Klausosi ir supranta perkeltines kalbėjimo prasmes: dviprasmybes, absurdus,
humorą, fantazijas, palyginimus, žodžių daugiareikšmiškumą.
	<li>Išklauso ir supranta 3–4 dalių verbalinę instrukciją.
	<li>Supranta, kad kūrinys turi pradžią, pabaigą, vidurį, kad jame veikia skirtingi
veikėjai, kad yra tam tikra veiksmo vieta.
	<li>Suvokia pokalbio, pasakojimo, skaitomo kūrinio eigą.
</ul><p><strong>Kalbėjimas</strong><ul><li>Kalba, pasakoja apie tai, kas patirta (nutikimus, susitikimus, įspūdžius, veiklą)
įvykių eilės seka. Kalbasi apie tai, ką norėtų patirti, išgirsti, pamatyti, veikti.
Paaiškina konflikto, nesutarimo esmę, svarsto, tariasi. Nusako žaidimo, veiklos
taisykles.
<li>Pasakoja kelių įvykių istorijas, pasakojimą palydi pantomima.
<li>Kalbasi apie spaudinių iliustracijas, fotoalbumų nuotraukas, skelbimus, simbolius
gatvėse, parduotuvėse ir kt., apibūdina, aiškina.
<li>Pagal taisykles kalbasi telefonu.
<li>Taisyklingai vartoja įvairias konstatuojamųjų ir klausiamųjų sakinių formas
(kada, kur, kaip ir kt.). Kuria naujus žodžius pagal perprastas žodžių darybos
taisykles. Išgirsta visus garsus žodyje ir pasako juos eilės tvarka.
<li>Girdėtų kūrinių kalbinės raiškos elementus vartoja pokalbiuose, svarstymuose,
erzinimuose ir kt. Atpasakodamas kūrinį, vartoja įprastus kūrinio pradžiai ar
pabaigai posakius. Deklamuoja modernius eilėraščius.
<li>Žaidžia kalba – rimais, ritmais, kuria naujus žodžius.</ul></td>
    </tr>
</table>

</div>


<div class="dialog" id="dialog-9" data-title="9. RAŠYTINĖ KALBA">
<strong>Rašytinė kalba – tai vaiko rašymas ir skaitymas.</strong>
<p>Ji perprantama natūralioje aplinkoje, savarankiškoje paties vaiko veikloje, atsiradus ar sužadinus
poreikį kažką parašyti ir perskaityti. 
<strong> Rašymas prasideda pirmomis vaiko braukomomis linijomis</strong>, taškeliais, kraigalionėmis, atsitiktinai parašytais raidžių elementais,
 nukopijuotomis raidėmis. <strong>Skaitymas</strong> ikimokyklinio amžiaus vaikui <strong>– tai simbolių, brėžinių prasmės suvokimas,</strong>
 pirmųjų trumpų žodžių ir raidžių atpažinimas ir siejimas su garsais.
 <p>Skaitymo mokymosi sėkmę lemia vaiko įgudimas klausyti ir kalbėti bei jo gyvenimiška patirtis.
Kad vaikas mokytųsi skaityti, jis nuolat skatinamas papasakoti apie savo keverzones, piešinėlius, knygų
iliustracijas, imituoti skaitymą, pačiam skaitinėti ir pan.
<p>Rašytinės kalbos srityje vaikui ugdantis tobulėja:
<ul><li>domėjimasis skaitymu, raidėmis, žodžiais bei įvairiais simboliais ir jų reikšmėmis,
<li>domėjimasis rašymu, raidžių bei žodžių rašinėjimas, įvairių simbolių braižymas ar piešimas,
<li>trumpų žodelių skaitymas.
</ul>

<table>
    <tr>
<th></th>
<th>Žingsniai</th>
<th>Rašytinės kalbos srities pasiekimai </th>
</tr>
	<tr>
        <td class="darker" rowspan="3"><div class='rotate'>0–3 metai</div></td>
        <td class="step-center">1-asis žingsnis</td>
		<td><strong>Skaitymas</strong><ul><li>Vartinėja, žiūrinėja paveikslėlių knygeles, žiūrinėja paveikslėlius. Palankiai reaguoja
į knygelių skaitymą (vartymą) kartu su suaugusiaisiais.</ul>
		<p><strong>Rašymas</strong>
		<ul><li>Stebi rašančiuosius, domisi įvairiomis rašymo priemonėmis, brauko jomis įvairias
linijas.</ul></td>
    </tr>
	
		<tr><td class="step-center">2-asis žingsnis</td><td><strong>Skaitymas</strong><ul><li>Varto knygeles, žiūrinėja paveikslėlius, piršteliu juos rodo. Pradeda atpažinti jo
mėgstamas knygeles, nori, kad jas jam paskaitytų. Kartais knygelę laiko taisyklingai.
Reaguoja į skaitomą tekstą. Kreipia dėmesį į aplinkoje esančias raides,
žodžius, simbolius.</ul>
		<p><strong>Rašymas</strong>
		<ul><li>Įvairiomis rašymo priemonėmis spontaniškai brauko popieriaus lape.</ul></td></tr>

		<tr><td class="step-center">3-iasis žingsnis</td><td><strong>Skaitymas</strong><ul><li>Varto knygeles, dėmesį skirdamas ne tik paveikslėliams, bet ir tekstui, prašydamas
paskaityti. Geba sieti paveikslėlius su juose vaizduojamais konkrečiais
daiktais, juos pavadina. Pradeda pažinti aplinkoje esančius simbolius.</ul>
		<p><strong>Rašymas</strong>
		<ul><li>Įvairiomis rašymo priemonėmis kraiglioja vertikalias ir horizontalias linijas.</ul></td></tr>
	<tr>
        <td class="darker" rowspan="3"><div class='rotate'>4–6 metai</div></td>
        <td class="step-center">4-asis žingsnis</td>
		<td><strong>Skaitymas</strong><ul><li>Domisi skaitymu. Vaizduoja, kad skaito knygą, kuri jam buvo skaityta. Skaito
knygelių paveikslėlius, įvardija įvairių objektų ir veikėjų bruožus, veiksmus.
Atkreipia dėmesį į raides, simbolius (grafinius vaizdus) aplinkoje, pradeda jais
manipuliuoti įvairioje veikloje.</ul>
		<p><strong>Rašymas</strong>
		<ul><li>Domisi ne tik įvairiomis rašymo priemonėmis, bet ir galimybe rašyti (planšetiniu
kompiuteriu ir kt.) Keverzonėse ir piešiniuose pasirodo realių raidžių elementai ir
raidės. Raidėmis ir simboliais (grafiniais vaizdais) pradeda manipuliuoti įvairioje
veikloje.</ul></td>
    </tr>
		<tr><td class="step-center">5-asis žingsnis</td><td><strong>Skaitymas</strong><ul><li>Domisi abėcėlės raidėmis. Pastebi žodžius, prasidedančius ta pačia raide.
Supranta, kad kai kurios raidės turi savo pavadinimą ir specifinę grafinę raišką.
Supranta aplinkoje matomų kai kurių spausdintų žodžių prasmę.
<li>Sugalvoja pavadinimus paveikslėliams, knygelėms. Įvardija specifinius skaitomo
teksto veikėjų bruožus.</ul>
		<p><strong>Rašymas</strong>
		<ul><li>Domisi ir supranta skirtingų spaudinių funkcijas (kalendorius, valgiaraštis, reklama,
bukletas ir pan.).
<li>Kopijuoja raides, paprastus žodžius. „Iliustruoja“ pasakas, pasakojimus, istorijas,
filmukus, iliustracijose parašydamas nukopijuotas raides, žodžius. Kuria ir
gamina rankų darbo knygeles su elementariais nukopijuotais sakiniais, žodžiais,
raidėmis. Braižo ir aiškina planus, schemas, grafikus. Bando rašyti raides, pradėdamas
savo vardo raidėmis. (Planšetiniame kompiuteryje rašo savo vardą,
trumpus žodelius.)</ul></td></tr>

		<tr><td class="step-center">6-asis žingsnis</td><td><strong>Skaitymas</strong><ul><li>Domisi knygomis, įvairiais rašytiniais tekstais, supranta nesudėtingą jų siužetą,
klausinėja. Pradeda suprasti ryšį tarp knygos teksto, iliustracijų ir asmeninės
patirties. Žino keliolika abėcėlės raidžių. Supranta, kad garsas siejamas su raide,
o raidės sudaro žodį. Pradeda skirti žodžius sudarančius garsus, skiemenis. Pažįsta
parašytą žodį, kaip atskirų raidžių junginį.
<li>Gali perskaityti užrašus, kuriuos mato gatvėse, pavyzdžiui, parduotuvių, kirpyklų,
kavinių pavadinimus. </ul>
		<p><strong>Rašymas</strong>
		<ul><li>Spausdintomis raidėmis rašo savo vardą, kopijuoja aplinkoje matomus žodžius.
Piešiniuose užrašo atskirų objektų pavadinimus. Įvairiais simboliais bando perteikti
informaciją. Planšetiniu kompiuteriu rašo raides, žodžius. Supranta rašymo tikslus.</ul></td>
</tr>
<tr class="darker">
<td colspan="2">Vertybinė nuostata</td>
<td colspan="1">Domisi rašytiniais ženklais, simboliais, skaitomu tekstu.</td>
</tr>
<tr class="darker">
<td colspan="2">Esminis gebėjimas</td>
<td colspan="1">Atpažįsta ir rašinėja raides, žodžius bei kitokius simbolius, pradeda skaitinėti.</td>
</tr>
<tr>
    <td class='darker' rowspan="1"><div class="rotate"></div></td>
    <td class="step-center">7-asis žingsnis</td>
	<td><strong>Skaitymas</strong><ul><li>Gerbia ir tausoja knygas, kitus spaudinius (kalendorius, žurnalus ir kt.). Supranta
knygos ar kito informacijos šaltinio dalių pavadinimus, jų funkcijas (viršelis,
titulinis lapas, autorius, pavadinimas ir kt.). Atpažįsta ir pavadina keletą įvairiu
šriftu parašytų raidžių (didžiųjų ir mažųjų, spausdintų ir rašytinių). Atpažįsta ir
pavadina kelis skiriamuosius ženklus (klaustuką, šauktuką, tašką).
<li>Imituodamas skaitymą, pagal knygos iliustracijas kuria pasakojimą, panašų į girdėtą
skaitant. Imituodamas skaitymą, kuria savo pasakojimą, susijusį su patirtimi,
fantazuoja. Teisingai įvardija puslapį, sakinį, žodį, raidę. Bando perskaityti trumpus,
jam reikšmingus žodžius (savo vardą, artimųjų vardus, adresą ir kt.).</ul>
		<p><strong>Rašymas</strong>
		<ul><li>Piešiniu, rašinėjimais, raidėmis atvaizduoja savo patirtį, išgyvenimus, norus.
Piešiniuose, po darbeliais, įvairiuose laiškeliuose, pranešimuose ar kvietimuose
rašo atskiras raides, savo vardą, elementarius žodelius. Rašo nepaisydamas
žodžių darybos ir jų dėstymo eilutėje ar puslapyje taisyklių.</ul></td>
    </tr>
</table>

</div>

<div class="dialog" id="dialog-10" data-title="10. APLINKOS PAŽINIMAS">
Aplinkos pažinimas glaudžiai siejasi su tyrinėjimo ir mokėjimo mokytis gebėjimais.
<p><strong>Noras sužinoti, išmokti, suprasti, kas vyksta aplinkui, yra aplinkos pažinimo pamatas.</strong>Nereikėtų
galvoti, kad aplinkos pažinimas – tai pavadinimų, faktų ir kitos informacijos įsiminimas. Tokios žinios
bus bevertės, jeigu vaikas neišmoks mąstyti, pritaikyti to, ką sužinojo apie aplinką, pasirinkti. 
<p>Gilus aplinkos pažinimas susideda iš:
<ul><li><strong>domėjimosi</strong> socialine, kultūrine, gamtine <strong>aplinka, noro ją tyrinėti ir pažinti</strong>;
<li><strong>pagarbos gamtai ir gyvybei</strong>, žmonių sukurtai aplinkai, <strong>tolerancijos įvairių kultūrų, lyties,
socialinių ir amžiaus grupių žmonėms;</strong>
<li><strong>gebėjimo mąstyti, samprotauti</strong>, remiantis informacija, spręsti problemas, nusakyti pasaulį
kalba, vaizdais, simboliais ir kitomis priemonėmis;
<li><strong>žinių apie aplinką</strong>, jos raidą, aplinkos pažinimo būdų, savo šeimos, savęs, kaip bendruomenės
nario, suvokimo, savo teisių bei pareigų <strong>žinojimo</strong>;
<li><strong>aplinkos pažinimo būdų žinojimo</strong> bei supratimo, gebėjimo pritaikyti žinias.
</ul>
<p>Aplinkos pažinimo srityje vaikui ugdantis tobulėja:
<ul><li>socialinės aplinkos pažinimas,
	<li>gamtinės aplinkos pažinimas,
	<li>pagarba gyvybei ir aplinkai.</ul>

<table>
    <tr>
<th></th>
<th>Žingsniai</th>
<th>Aplinkos pažinimo srities pasiekimai</th>
</tr>
	<tr>
        <td class="darker" rowspan="3"><div class='rotate'>0–3 metai</div></td>
        <td class="step-center">1-asis žingsnis</td>
		<td><ul><li>Domisi aplinka, sutelkia dėmesį į arti esančius veidus, daiktus. 
		<li>Reaguoja į skirtingus vaizdus, paviršius, kvapus, garsus, skonius. 
		<li>Atpažįsta artimus žmones, žaislus, daiktus.
		</ul></td>
    </tr>
	
		<tr><td class="step-center">2-asis žingsnis</td><td><ul><li>Stebi ir atpažįsta artimiausią savo aplinką, orientuojasi joje.
		<li>Pažįsta ir pavadina kai kuriuos gyvūnus, žmones, daiktus, jų atvaizdus. 
		<li>Žino kai kurių daiktų paskirtį ir jais naudojasi (šukos, šaukštas, nosinaitė).
		</ul></td></tr>

		<tr><td class="step-center">3-iasis žingsnis</td><td><ul><li>Atpažįsta ir pavadina vis daugiau artimiausioje aplinkoje esančių augalų (sodo,
daržo, lauko), gyvūnų, daiktų, domisi jais.
		<li>Skiria atskirus gamtos reiškinius.
		<li>Orientuojasi savo grupės, darželio, namų aplinkoje.
		<li>Pasako savo ir savo šeimos narių vardus.
		<li>Dalyvauja prižiūrint augalus ar gyvūnus.
		</ul>
		</td></tr>
	<tr>
        <td class="darker" rowspan="3"><div class='rotate'>4–6 metai</div></td>
        <td class="step-center">4-asis žingsnis</td>
		<td><ul><li>Pažįsta gyvenamosios vietovės objektus (namai, automobiliai, keliai, parduotuvės
ir pan.). 
		<li>Pasako miesto, gatvės, kurioje gyvena, pavadinimus, savo vardą ir pavardę. 
		<li>Pastebi pasikeitimus savo aplinkoje.
		<li>Pastebi ir nusako aiškiausiai pastebimus gyvūnų ir augalų požymius. 
		<li>Atpažįsta gamtoje ar paveiksluose dažniausiai sutinkamus gyvūnus, medžius, gėles,
daržoves, grybus, pasako jų pavadinimus. 
		<li>Pasako metų laikų pavadinimus ir būdingus jiems požymius, skiria daugiau gamtos
reiškinių (rūkas, pūga, šlapdriba).
		</ul>
		</td>
    </tr>
		<tr><td class="step-center">5-asis žingsnis</td><td><ul><li>Pasakoja apie savo šeimą, jos buitį, tradicijas.
		<li>Moka papasakoti apie savo gimtąjį miestą ar gyvenvietę. Pasako savo gatvės
pavadinimą. Įvardija kelis žinomus gimtojo miesto objektus.
		<li>Gali savarankiškai nueiti į darželio salę, biblioteką, valgyklą ar, pvz., dailės studiją. 
		<li>Atranda buities prietaisų, skaitmeninių technologijų panaudojimo galimybes,
noriai mokosi jais naudotis.
		<li>Atpažįsta ir įvardija ne tik naminius, bet ir kai kuriuos laukinius gyvūnus.
Samprotauja apie naminių ir laukinių gyvūnų gyvenimo skirtumus.
		<li>Skiria daržoves, vaisius, uogas, nusako, kaip juos naudoti maistui. 
		<li>Domisi dangaus kūnais, gamtos reiškiniais, kurių negali pamatyti (pvz., ugnikalnių
išsiveržimas, žemės drebėjimas, smėlio audra).
		</ul>
		</td></tr>

		<tr><td class="step-center">6-asis žingsnis</td><td><ul><li>Pasako tėvų profesijas, įvardija savo giminaičius, žino savo namų adresą.
		<li>Pasako savo šalies ir sostinės pavadinimą.
		<li>Skiria ir pavadina suaugusiųjų profesijas, darbus ir buitį palengvinančią techniką
(prietaisai, transportas, įrenginiai). Samprotauja apie tai, kad gamindami daiktus
žmonės įdeda daug darbo, kokių savybių žmogui reikia darbe, kokios yra profesijos.
		<li>Domisi, kokie daiktai buvo naudojami seniau, kaip jie pasikeitė. 
		<li>Papasakoja apie tradicines šventes.
		<li>Pastebi aiškiai matomus skirtumus ir panašumus tarp gyvūnų ir tarp augalų.
Samprotauja apie tai, kur gyvena, kuo minta naminiai ir laukiniai gyvūnai. 
		<li>Moka prižiūrėti kambarinius augalus, daržoves, stebi jų augimą.
		<li>Papasakoja apie naminių gyvūnų naudą žmonėms ir augalų naudojimą maistui.
		<li>Pradeda suprasti Žemės, Saulės, Mėnulio ir kitų dangaus kūnų ryšius.
		<li>Pradeda jausti prieraišumą prie artimiausios gamtinės aplinkos, dalyvauja ją
prižiūrint ir puošiant, suvokia savo vietą joje, pažįsta ir įvardija gyvenamosios
vietovės objektus (upę, kalvą, mišką ir pan.), gyvūnus ir augalus.
		<li>Rodo pagarbą gyvajai ir negyvajai aplinkai ir besiformuojančią atsakomybę
už jos išsaugojimą. 
		<li>Mokosi rūšiuoti atliekas.
		</ul>
		</td></tr>

<tr class="darker">
<td colspan="2">Vertybinė nuostata</td>
<td colspan="1">Nori pažinti bei suprasti save ir aplinkinį pasaulį, džiaugiasi sužinojęs ką nors
nauja. </td>
</tr>
<tr class="darker">
<td colspan="2">Esminis gebėjimas</td>
<td colspan="1">Įvardija ir bando paaiškinti socialinius bei gamtos reiškinius, apibūdinti save,
savo gyvenamąją vietą, šeimą, kaimynus, gyvosios ir negyvosios gamtos
objektus, domisi technika ir noriai mokosi ja naudotis.</td>
</tr>
<tr>
    <td class='darker' rowspan="1"><div class="rotate"></div></td>
    <td class="step-center">7-asis žingsnis</td>
	<td><ul><li>Nusako miško, pievos, vandens telkinio augmenijos ir gyvūnijos būdingiausius
bruožus (miške vyrauja medžiai ir krūmai, pievose – žolės; vandens telkinių
augalai gali augti tik panirę ar pusiau panirę į vandenį ir pan.). 
		<li>Suaugusiųjų padedamas pastebi požymius, kurie rodo augalų bei gyvūnų prisitaikymą
gyventi sausumoje ar vandenyje.
		<li>Nurodo kelis gyvūnus, kurie minta tik augalais, ir kelis, kurie minta kitais gyvū-
nais ir vadinami plėšrūnais.
		<li>Atpažįsta po kelis miško ir vandens augalus bei gyvūnus, valgomuosius grybus.
		<li>Paaiškina, kad nežinomų augalų uogų ir grybų negalima ragauti, nes jie gali būti
nuodingi.
		<li>Paaiškina, kaip reikia prižiūrėti kambarinius ar daržo augalus, naminius gyvūnus. 
		<li>Nusako, iš ko ir kaip gaminama duona, pieno produktai.
		<li>Noriai tyrinėja medžiagų savybes, daiktų sandarą ir apie tai samprotauja, aiškinasi
kodėl; pateikia (ne)tirpstančių medžiagų, vandenyje (ne)skęstančių daiktų
pavyzdžių. 
		<li>Domisi gamtos reiškiniais ir jų aiškinimu (rasa, vaivorykštė, vėjas). Nusako, kaip
skiriasi orai kitose pasaulio šalyse.
		<li>Paaiškina, kad Saulė apšviečia ir šildo Žemę; samprotauja, kuo planetos skiriasi
nuo žvaigždžių.
		<li>Tvarkingai ir saugiai elgiasi gamtoje, išvykose, noriai dalyvauja tvarkant ir
puošiant aplinką.
		<li>Paaiškina, kaip reikia taupyti elektrą, šilumą ir vandenį, kaip rūšiuoti šiukšles,
esant galimybėms jas rūšiuoja.
		<li>Samprotauja apie profesijos pasirinkimą, kokių savybių reikia žmonėms, dirbantiems
įvairius darbus.
		<li>Domisi žmonių gyvenimo būdu, jo kaita mūsų ir kitose šalyse.
		<li>Domisi savo giminės istorija, savo gimtosios vietovės ir Lietuvos žinomais
žmonėmis, kultūros ir gamtos paminklais, gali apie kelis trumpai papasakoti.
		<li>Domisi aktualiais visuomeninio gyvenimo įvykiais.
		<li>Bando prisiimti atsakomybę už bendrą darbą, stengiasi prisidėti, dalyvauti
ikimokyklinės įstaigos ir vietos bendruomenės talkose, šventėse ir kituose
renginiuose.
		</ul>
		</td>
    </tr>
</table>

</div>


<div class="dialog" id="dialog-11" data-title="11. SKAIČIAVIMAS IR MATAVIMAS">
Pažinimo procesas neatsiejamas nuo sąvokų, jų ryšių, sąvokų struktūrų suvokimo, mat mūsų smegenys
nuolat ieško, kaip palyginti, susisteminti, apibendrinti mus pasiekiančią informaciją. <strong>Skaičiavimas</strong> –
tai komponentas, kuriame kalbama apie pirmąją vaiko pažintį su skaičiaus sąvoka.
<p>Čia svarbūs keli aspektai: skaičiaus vartojimas kiekiui nusakyti (kiek?); skaičiaus vartojimas numeruoti
(kelintas?); simboliai, vartojami skaičiams pažymėti. 
<p>Vaiko pažintinė raida neatsiejama nuo vis besiplečiančio jo suvokimo apie daiktų panašumus ir skirtumus.
Komponente <strong>matavimas</strong> išskiriami tokie aspektai: vaiko pažintis su paprasčiausiomis geometrinėmis
figūromis; daikto dydžio, dydžių santykio suvokimas; vis gerėjantis vaiko orientavimasis laike ir
erdvėje. 
<p>Skaičiavimo srityje vaikui ugdantis tobulėja:
<ul><li>gebėjimas vartoti skaičius ir matematinius simbolius daiktų kiekiui žymėti, daiktų grupėms
palyginti pagal kiekį,
<li>gebėjimas suprasti daikto vietą eilėje, pastebėti dėsningumus, sudaryti įvairias sekas.
</ul>
<p>Matavimo srityje vaikui ugdantis tobulėja:
<ul><li>gebėjimas tapatinti, grupuoti, klasifikuoti daiktus pagal formą, dydį, spalvą, 
	<li>gebėjimas matuoti; 
	<li>daikto vietos ir padėties erdvėje suvokimas; 
	<li>laiko tėkmės suvokimas.</ul>

<table>
    <tr>
<th></th>
<th>Žingsniai</th>
<th>Skaičiavimo ir matavimo srities pasiekimai </th>
</tr>
	<tr>
        <td class="darker" rowspan="3"><div class='rotate'>0–3 metai</div></td>
        <td class="step-center">1-asis žingsnis</td>
		<td><strong>Skaičiavimas</strong><ul><li>Gestais, mimika parodo, jog suvokia, ką reiškia yra (nėra), dar, taip (ne). 
		</ul>
		<strong>Matavimas</strong><ul><li>Susidomi, kai parodomas kitokios formos, dydžio, spalvos daiktas: siekia jį paimti,
tyrinėja. Atkreipia dėmesį į judančius, artėjančius daiktus, daug kartų kartoja
matytus veiksmus su daiktais.</ul></td>
    </tr>
	
		<tr><td class="step-center">2-asis žingsnis</td><td><strong>Skaičiavimas</strong><ul><li>Supranta, ką reiškia vienas, dar vienas, du, daug (parodo pirštukais, kiek turi
metukų). 
		</ul>
		<strong>Matavimas</strong><ul><li>Žaisdamas stengiasi rasti reikiamos formos, dydžio ar spalvos daiktą. Nuotraukoje,
piešinyje atpažįsta anksčiau matytą daiktą. Supranta vis daugiau žodžių,
kuriais nusakoma daikto forma, dydis, spalva, judėjimas erdvėje: paimti didelį,
nueiti iki, pažiūrėti į viršų ir pan.). </ul></td></tr>

		<tr><td class="step-center">3-iasis žingsnis</td><td><strong>Skaičiavimas</strong><ul><li>Skiria žodžius: mažai (vienas, du) ir daug. Paprašytas duoda kitiems po vieną
žaislą, daiktą. 
		<li>Geba išrikiuoti daiktus į vieną eilę. 
		</ul>
		<strong>Matavimas</strong><ul><li>Tapatina daiktus pagal formą, dydį. Suranda tokios pat spalvos (raudonos,
mėlynos, geltonos, žalios) daiktus.
<li>Supranta, kad bokštas, kurio viršuje bus didesnės, o apačioje mažesnės kaladėlės,
nebus tvirtas. Supranta ir pradeda vartoti daiktams lyginti skirtus žodžius: didelis
– mažas, ilgas – trumpas, sunkus – lengvas, storas – plonas, toks pat – ne toks,
kitoks, vienodi – skirtingi ir pan.</ul></td></tr>
	<tr>
        <td class="darker" rowspan="3"><div class='rotate'>4–6 metai</div></td>
        <td class="step-center">4-asis žingsnis</td>
		<td><strong>Skaičiavimas</strong><ul><li>Pradeda skaičiuoti daiktus, palygina dvi daiktų grupes pagal daiktų kiekį grupė-
je. Padalina daiktus į grupes po lygiai (po du, po tris). Supranta, kad prie daiktų
pridedant po vieną jų skaičius grupėje didėja, o paimant po vieną – mažėja. 
		<li>Pradeda vartoti kelintinius skaitvardžius (pirmas, antras...).
		</ul>
		<strong>Matavimas</strong><ul><li>Atpažįsta ir atrenka apskritos (skritulio), keturkampės (keturkampio), kvadratinės
(kvadrato) formos daiktus, vienodo dydžio ar spalvos daiktus. Statydamas,
konstruodamas, komponuodamas, grupuodamas pradeda atsižvelgti į daikto
formą, dydį, spalvą. 
<li>Labai skirtingus ir vienodus daiktus palygina pagal ilgį, storį, aukštį, masę ir pan.
Dydžių skirtumams apibūdinti pradeda vartoti žodžius: didesnis – mažesnis,
ilgesnis – trumpesnis, storesnis – plonesnis, aukštesnis – žemesnis ir pan.
Už save didesnius daiktus vadina dideliais, o mažesnius – mažais.
<li>Pradeda skirti dešinę ir kairę savo kūno pusę, kūno priekį, nugarą. Nurodydamas
kryptį (savo kūno atžvilgiu) naudoja žodžius: pirmyn – atgal, kairėn – dešinėn,
aukštyn – žemyn.
<li>Paros dalis sieja su savo gyvenimo ritmu. Žino metų laikus ir būdingus jiems
požymius.</ul></td>
    </tr>
	
		<tr><td class="step-center">5-asis žingsnis</td><td><strong>Skaičiavimas</strong><ul><li>Supranta, kad daiktų skaičius nepriklauso nuo daiktų formos, dydžio ir kitų
savybių bei jų padėties erdvėje. Skaičiuoja bent iki 5. Dėliodamas kelis daiktus,
sugeba atsakyti į klausimus: Kiek iš viso? Kiek daugiau? Kiek mažiau? 
		<li>Pastebi, kaip sudaryta daiktų (elementų) seka, geba pratęsti ją 1–2 daiktais
(elementais). Pratęsdamas pasikartojančių daiktų ar elementų seką, nebū-
tinai laikosi tos pačios jos sudarymo logikos (pvz., iš pradžių gali imti daiktus
ar elementus pagal vieną požymį, vėliau – pagal kitą). Skiria kelintinius
skaitvardžius.
		</ul>
		<strong>Matavimas</strong><ul><li>Skiria trikampę, stačiakampę formas. Randa mažai besiskiriančius daiktus.
Kalbėdamas apie spalvą, vartoja žodžius „vienos spalvos“, „dvispalvis“. Grupuoja,
komponuoja daiktus, atsižvelgdamas į jų spalvą, formą arba dydį.
<li>Palygindamas daiktų dydžius, naudojasi sąlyginiu matu (trečiu daiktu). Kalbė-
damas apie atstumą, daiktų ilgį, plotį, aukštį, storį, masę, vartoja žodžius: ilgesnis
– trumpesnis, siauresnis – platesnis, aukštesnis – žemesnis, lengvesnis – sunkesnis.
Supranta, ką reiškia sudėlioti nuo mažiausio iki didžiausio, ir atvirkščiai.
<li>Supranta, kad knygos skaitomos iš kairės į dešinę ir iš viršaus į apačią. Juda nurodyta
kryptimi. Skiria ir žodžiais išreiškia erdvinius daikto santykius su savimi:
priešais mane, už manęs, šalia manęs, mano kairėje ir pan.
<li>Pradeda suvokti praeitį, dabartį, ateitį. Skiria sąvokas šiandien, vakar, rytoj.</ul></td></tr>

		<tr><td class="step-center">6-asis žingsnis</td><td><strong>Skaičiavimas</strong><ul><li>Susieja daiktų (realių ar pavaizduotų) kiekį su atitinkamu daiktų skaičių žyminčiu simboliu. Skaičiuoja bent iki 10. Palygina mažai daiktų turinčias grupes pagal
kiekį. Supranta ir vartoja žodžius: daugiau (mažiau) vienu, dviem, po lygiai,
pusiau, į 2 dalis, į 4 dalis. 
		<li>Atpažįsta, atkuria, pratęsia, sukuria skirtingų garsų, dydžių, formų, spalvų sekas
su 2–3 pasikartojančiais elementais.
		</ul>
		<strong>Matavimas</strong><ul><li>Skiria plokštumos ir erdvės figūras: skritulį ir rutulį, kvadratą ir kubą. Klasifikuoja
daiktus pagal dydį, formą arba spalvą.
<li>Matuodamas atstumą, ilgį, tūrį, masę, naudojasi vienu ar keliais sąlyginiais matais
(savo pėda, sprindžiu, trečiu daiktu). Atranda, kad į skirtingos formos daiktus
galima sutalpinti tą patį skystų ar birių medžiagų (vandens, smėlio ir kt.)
kiekį. Lygindamas dydžius, vartoja jų skirtumo didumą pabrėžiančius žodžius
(šiek tiek didesnis, truputį mažesnis, didžiausias, mažiausias ir kt.).
<li>Apibūdina daiktų vietą ir padėtį kitų daiktų ar vienas kito atžvilgiu, sakydamas:
į kairę, į dešinę, aukščiau, žemiau, virš, po, šalia, greta, viduryje, tarp, priešais,
prie, prieš, paskui, šalia vienas kito, už, tarp, viduje, išorėje ir kt.
<li>Žino, jog gamtoje esama įvairios trukmės pasikartojančių ciklų (kartojasi savaitės
dienos, metų laikai ir pan.).</ul></td></tr>

<tr class="darker">
<td colspan="2">Vertybinė nuostata</td>
<td colspan="1">Nusiteikęs pažinti pasaulį skaičiuodamas ir matuodamas.</td>
</tr>
<tr class="darker">
<td colspan="2">Esminis gebėjimas</td>
<td colspan="1">Geba skaičiuoti daiktus, palyginti daiktų grupes pagal kiekį, naudoti skaitmenis,
apibūdinti daikto vietą eilėje, sudaryti sekas. 
<p>Geba grupuoti daiktus pagal spalvą, formą, dydį. Jaučia dydžių skirtumus,
daikto vietą ir padėtį erdvėje. Supranta ir vartoja žodžius, kuriais apibūdinamas
atstumas, ilgis, masė, tūris, laikas. Pradeda suvokti laiko tėkmę ir trukmę.</td>
</tr>
<tr>
    <td class='darker' rowspan="1"><div class="rotate"></div></td>
    <td class="step-center">7-asis žingsnis</td>
	<td><strong>Skaičiavimas</strong><ul><li>Pradeda suprasti ryšius tarp skaičių (pvz., kad šeši susideda iš dviejų trejetų arba
iš trijų dvejetų porų). Pradeda suprasti ir vartoti žodžius: sudėti, pridėti, atimti,
kiek bus, kiek liks ir pan.
		<li>Išdėlioja daiktus į eilę pagal tą patį požymį (pvz., pagal spalvos intensyvumą).
		</ul>
		<strong>Matavimas</strong><ul><li>Skiria ir pavadina plokštumos figūras (apskritimą, skritulį, kvadratą, stačiakampį,
trikampį) ir erdvės figūras (kubą, rutulį); aplinkoje randa daiktus, savo forma
primenančius šias figūras. Dėlioja paveikslėlius iš didesnio kiekio elementų, stato
iš smulkesnių detalių. Sugrupuoja daiktus pagal nurodytą požymį (dydį, formą).
Pradeda suprasti, kad tas pats daiktas gali priklausyti kitai grupei pagal kitą
požymį.
<li>Pradeda suprasti, kada vartojami priešingos reikšmės žodžiai: mažas ir didelis, lengvas ir sunkus, šilta ir
šalta ir kt. (pvz., sako: „Aš esu didelis, o kačiukas mažas, bet aš esu mažas, o namas didelis“). 
<li>Apibūdina daiktų vietą ir padėtį kitų daiktų ar vienas kito atžvilgiu, sakydamas:
į kairę, į dešinę, aukščiau, žemiau, virš, po, šalia, greta, viduryje, tarp, priešais,
prie, prieš, paskui, šalia vienas kito, už, tarp, viduje, išorėje ir kt.
<li>Moka parodyti, kur yra greta(tačiau tik iš šonų, ne viršuje) esantys, bet nematomi objektai (pvz., laukujos durys, sodas). Žaisdamas
naudojasi vis didesne erdve.</ul></td>
    </tr>
</table>

</div>


<div class="dialog" id="dialog-12" data-title="12. MENINĖ RAIŠKA">
<strong>Vaiko meninė raiška</strong> – tai vaiko įspūdžių, išgyvenimų, supratimo, emocijų, jausmų <strong>patirties reiškimas
ir vaizdavimas meninės raiškos būdais ir priemonėmis</strong>: veiksmu, judesiu, žodžiu, balso intonacija,
spalva, linija, forma ir kt. Ji apima muziką, šokį, vaidybą ir vizualinę raišką.
<p><strong>Muzika</strong> – tai vaiko raiška garsu, melodija, ritmu dainuojant, grojant, ritmuojant, klausantis muzikos,
kuriant, improvizuojant.
<p><strong>Šokis</strong> – tai vaiko raiška judesiu šokant liaudies ratelius, šokamuosius žaidimus ir improvizuotai
kuriant trumpas judesių, nesusijusių su šokio žanru, sekas.
<p><strong>Vaidyba</strong> – tai veikėjo vaizdavimas improvizuotais savo paties ar lėlės (žaislo) judesiais, veiksmais,
žodžiais, balso intonacijomis, veido mimika. Kiekvienas vaikas vaidindamas-vaizduodamas reiškia savo
požiūrį ir supratimą.
<p><strong>Vizualinė raiška</strong> – tai vaiko raiška vaizdų kalba tapant, liejant akvarele, piešiant tradicinėmis ir
skaitmeninėmis priemonėmis, lipdant, konstruojant, aplikuojant, spauduojant, lankstant, karpant ir kt.
<p>Meninės raiškos srityje vaikui ugdantis tobulėja:
<ul><li>emocijų, patirties, minčių, įspūdžių raiška meninėmis priemonėmis ir būdais,
	<li>meninės raiškos priemonių tyrinėjimas ir eksperimentavimas,
	<li>meninė kūryba ir improvizacija. </ul>

<table>
    <tr>
<th></th>
<th>Žingsniai</th>
<th>Meninės raiškos srities pasiekimai </th>
</tr>
	<tr>
        <td class="darker" rowspan="3"><div class='rotate'>0–3 metai</div></td>
        <td class="step-center">1-asis žingsnis</td>
		<td><strong>Muzika, šokis</strong><ul><li>Reaguoja į muzikos garsus, melodijas, balso intonacijas, judesius, išraiškingą
mimiką, suklusdamas, sutelkdamas žvilgsnį, nutildamas, nustodamas
arba pradėdamas judėti, krykštaudamas, žaisdamas balso intonacijomis,
garsais. 
		</ul>
		<strong>Žaidinimai, vaidyba</strong><ul><li>Kalbinamas, žaidinamas reiškia emocijas, norus įvairiomis balso intonacijomis,
veido mimika, lingavimu, plojimu, mojuodamas žaislu, daiktu. </ul>
		<strong>Vizualinė raiška</strong><ul><li>Storu pieštuku, teptuku, kreidele baksnoja, brauko, tepinėja palikdamas pėdsakus
(taškus, įvairių krypčių linijų brėžius, dėmes). Domisi ir džiaugiasi
dailės priemonėmis, jas liečia, apžiūrinėja, varto.</ul></td>
    </tr>
	
		<tr><td class="step-center">2-asis žingsnis</td><td><strong>Muzika, šokis</strong><ul><li>Įdėmiai klausosi muzikos ir aplinkos garsų, spontaniškai žaidžia balso intonacijomis,
rankų ir kojų judesiais, mėgdžioja žaidinimų judesius, suaugusiųjų balso
intonacijas, muzikos garsus, dviejų–trijų garsų melodijas ar daineles. Skambant
muzikai ritmiškai ploja, trepsi, tūpčioja, barškina, stuksena kokiu nors
daiktu.
		</ul>
		<strong>Žaidinimai, vaidyba</strong><ul><li>Žaidžiant sūpavimo, kykavimo, jodinimo, kutenimo, pirštukų žaidimus bei žaidinimus
mėgdžioja įvairias išgirstas intonacijas, parodytus veiksmus. Įvairius jausmus,
norus rodo judesiais ir veiksmais (pamojuoja, apkabina).</ul>
		<strong>Vizualinė raiška</strong><ul><li>Spontaniškai keverzoja rankų judesių piešinius, juos apžiūrinėja. Džiaugiasi (šūk-
čioja, krykštauja, mojuoja rankomis) dailės priemonės (tirštų dažų, minkštos
tešlos) paliekamu pėdsaku ir patiriamais jutimais, siekia pakartoti ir pratęsti
įdomią patirtį. 
<li>Tyrinėdamas dailės medžiagas ir priemones intuityviai atranda skirtingus veikimo
jomis būdus (brauko pirštais, varvina dažus, maigo tešlą).</ul></td></tr>

		<tr><td class="step-center">3-iasis žingsnis</td><td><strong>Muzika</strong><ul><li>Emocingai atliepia klausomus kūrinius (vaikiškas dainas, instrumentinius kūrinius)
– šypsosi, džiaugiasi, ploja, trepsi, sėdėdamas sūpuoja kojas ar pan. 
<li>Vienas ir kartu su kitais dainuoja 2–4 garsų daineles, palydėdamas jas judesiais.
<li>Drauge su pedagogu žaidžia muzikinius žaidimus, jų tekstą imituoja rankų, kūno
judesiais (žingsniuoja, bėga, apsisuka).
<li>Apžiūrinėja, tyrinėja ritminius muzikos instrumentus ir jais ritmiškai groja kartu
su pedagogu.
<li>Kuria, dainuoja vieno aukščio tonu savitus žodžius, ritmuoja vaikiškais instrumentais
ir daiktais. 
		</ul>
		<strong>Šokiai</strong><ul><li>Mėgdžioja žaidinimų, gyvūnų, augalų judesius.
<li>Šoka spontaniškai kurdamas dviejų–trijų natūralių judesių (eina, pritupia,
pasisuka ir kt.) seką.
		</ul>
		<strong>Žaidinimai, vaidyba</strong><ul><li>Žaisdamas su daiktu ar žaislu atlieka matytus veiksmus, judesius. Įvairiai
intonuodamas kalba apie tai, ką daro. Mėgdžioja šeimos narių kalbą, veiksmus.
Muzikiniuose rateliuose judesiais, veiksmais vaizduoja siužeto elementus,
reiškia savaime kilusias emocijas.</ul>
		<strong>Vizualinė raiška</strong><ul><li>Spontaniškai reiškia emocijas, įspūdžius dailės priemonėmis ir medžiagomis.
Piešia įvairias linijas, jas jungia į formas vis labiau koordinuodamas rankų judesius.
Bando ką nors pavaizduoti (mamą, mašiną). Savo abstrakcijose įžvelgia
daiktus ar įvykius.
<li>Eksperimentuoja dailės medžiagomis ir priemonėmis, tyrinėja įvairius veikimo jomis
būdus. Piešdamas, spauduodamas, tapydamas, lipdydamas, konstruodamas
labiau mėgaujasi procesu, o ne rezultatu.</ul></td></tr>
	<tr>
        <td class="darker" rowspan="3"><div class='rotate'>4–6 metai</div></td>
        <td class="step-center">4-asis žingsnis</td>
		<td><strong>Muzika</strong><ul><li>Klausydamasis ir tyrinėdamas gamtos garsus, trumpus vokalinius ir instrumentinius
kūrinius, judesiais emocingai atliepia jų nuotaiką, tempą bei keliais
žodžiais juos apibūdina. 
<li>Kartu su kitais dainuoja trumpas, aiškaus ritmo, siauro diapazono, laipsniškos
melodinės slinkties autorines ir liaudies dainas. Dainavimą palydi ritmiškais
judesiais. Tyrinėja savo balso galimybes (dainuoja garsiai, tyliai, aukštai, žemai,
greičiau, lėčiau). 
<li>Žaidžia įvairių tautų muzikinius žaidimus, atlikdamas kelis nesudėtingus judesius:
eina, bėga rateliu, trepsi, ploja, mojuoja, sukasi po vieną ir už parankių.
Tyrinėja garso išgavimo būdus kūno, gamtos, įvairiais muzikos instrumentais,
jais ritmiškai pritaria suaugusiojo grojimui. 
<li>Improvizuoja skanduodamas, plodamas, trepsėdamas, stuksendamas, spontaniškai
kuria ritminius, melodinius motyvus savo vardui, žodžiams. 
		</ul>
		<strong>Šokiai</strong><ul><li>Žaidžia vaizduojamuosius (darbo proceso, augalų vegetacijos, gyvūnų) šokamuosius
žaidimus, šoka trijų–keturių natūralių judesių (bėga, sukasi, ritasi ir kt.)
šokius. 
<li>Šoka spontaniškai kurdamas trijų–keturių natūralių judesių seką.
		</ul>
		<strong>Žaidinimai, vaidyba</strong><ul><li>Žaisdamas atkuria matytų situacijų fragmentus, panaudoja tikrus daiktus,
reikmenis, drabužius. Kuria dialogą tarp veikėjų, išraiškingai intonuoja.
Žaisdamas atsipalaiduoja. Muzikiniuose rateliuose kuria ar savaip perteikia
kelis veikėją vaizduojančius judesius, veiksmus, spontaniškai reiškia
emocijas.</ul>
		<strong>Vizualinė raiška</strong><ul><li>Patirtį išreiškia įvairiomis linijomis, jų deriniais, dėmėmis, geometrinėmis ir
laisvomis formomis, spalvomis, išgaudamas šiek tiek atpažįstamus vaizdus,
objektus, juos įvardija. Kuria spontaniškai, kartais pagal išankstinį sumanymą,
kuris darbo eigoje dažnai kinta, „pasimeta“. Kūrybos procesą palydi pasakojimu,
komentavimu, gestikuliavimu, mimika. 
<li>Eksperimentuoja dailės medžiagomis ir priemonėmis, atrasdamas spalvų, linijų,
formų, faktūrų įvairovę, turi mėgstamas spalvas. Kuria koliažus, spauduoja
įvairiomis priemonėmis, konstruoja, lipdo nesudėtingas formas. </ul></td>
    </tr>
	
		<tr><td class="step-center">5-asis žingsnis</td><td><strong>Muzika</strong><ul><li>Balsu, judesiais, pasirinktu muzikos instrumentu spontaniškai improvizuoja, pritaria
klausomam vokalinės, instrumentinės muzikos įrašui ar gyvai skambančios
muzikos kūriniui. Savais žodžiais išsako kilusius įspūdžius. Atpažįsta kai kurių
instrumentų (smuiko, būgno, dūdelės, varpelio) tembrus, girdėtus kūrinius. 
<li>Dainuoja vienbalses, dialoginio pobūdžio dainas, jaučia ritmą. Dainuodamas
išbando balso skambesį, išmėgina jį įvairioje aplinkoje (grupėje, kieme ir kt.).
Stengiasi tiksliau intonuoti, taisyklingiau artikuliuoti (aiškiai tarti balsius, priebalsius,
dvibalsius), taisyklingiau stovėti, kvėpuoti.
<li>Ritminiais, melodiniais, gamtos, savo gamybos vaikiškais muzikos instrumentais
pritaria dainoms, šokiams, tyrinėja jų skambėjimo tembrus.
<li>Improvizuodamas balsu, vaikišku muzikos instrumentu kuria ritmus, melodijas ketureiliams,
mįslėms, patarlėms. 
<li>Šoka sukamuosius (kai sukamasi poroje) ratelius, paprastų žingsnių (paprastasis,
aukštas paprastasis, stangrus, pritupiamasis) autorinius ir penkių–šešių natūralių
judesių (bėga, sukasi, pašoka ir kt.) šokius. 
<li>Šoka improvizuotai kurdamas penkių šešių natūralių judesių seką, reaguodamas
į muziką, išreikšdamas aplinkos vaizdus (gamtos reiškinius, gyvūnus).
		</ul>
		<strong>Vaidyba</strong><ul><li>Vaidindamas stalo, lėlių teatre, vaizduoja realistinį ir fantastinį siužetą, išplėtoja
vyksmą dialogu, monologu, keisdamas balso intonacijas. Išreiškia savo norus,
jausmus, mintis, baimes. Susikuria ištisą žaidimo aplinką, panaudodamas daiktus,
drabužius, reikmenis. Muzikiniuose žaidimuose ir rateliuose kuria ar savaip
perteikia 3–4 veiksmų seką, vaizduojančią augimą, darbus, veikėjų judėjimą,
stengiasi perteikti veikėjo nuotaiką. 
		</ul>
		<strong>Vizualinė raiška</strong><ul><li>Savo emocijas, patirtį, įspūdžius išreiškia kitiems atpažįstamais vaizdais. Išryš-
kina vaizduojamų objektų bruožus, reikšmingas detales. Objektus vaizduoja ne
tokius, kokius mato, o tokius, ką apie juos žino. Kuria pagal išankstinį sumanymą,
kuris procese gali kisti.
<li>Eksperimentuoja tapybos, grafikos, mišriomis dailės priemonėmis ir medžiagomis,
kuria sudėtingesnius koliažus, trimates formas iš įvairių medžiagų, asambliažus,
fotografuoja, piešia skaitmeninėmis priemonėmis (piešimo programomis
telefone, kompiuteryje). </ul></td></tr>

		<tr><td class="step-center">6-asis žingsnis</td><td><strong>Muzika</strong><ul><li>Klausosi įvairaus stiliaus, žanrų muzikos kūrinių ir spalvomis ar piešiniu spontaniškai
perteikia kilusius įspūdžius. Tyrinėja girdimos muzikos, triukšmo, tylos
panašumus ir skirtumus. Įvardija kūrinio nuotaiką, tempą, dinamiką, skiria kai
kuriuos instrumentus. 
<li>Dainuoja sudėtingesnio ritmo, melodijos, platesnio diapazono vienbalses
dainas, jas gana tiksliai intonuoja. Dainuoja trumpas daineles kanonu, įsiklausydamas
į savo ir draugų dainavimą. 
<li>Melodiniais vaikiškais muzikos instrumentais groja 2–3 garsų melodijas. Pritaria
suaugusiojo grojimui, atlikdami muzikines pjeses solo ir orkestre, seka dirigento
judesius, stengiasi kartu pradėti ir baigti kūrinėlį.
<li>Improvizuodamas balsu, muzikos instrumentu kuria melodiją trumpam tekstui,
paveikslui. Žaidžia muzikinius dialogus, kuria judesius kontrastingo pobūdžio
muzikai. 
		</ul>
		<strong>Šokis</strong><ul><li>Šoka sudėtingesnius ratelius (tiltelių, grandinėlės), paprastųjų ir bėgamųjų
(paprastasis bėgamasis, aukštas bėgamasis, liaunas, smulkus bėgamasis) žingsnių
autorinius ir natūralių judesių šokius. 
<li>Šoka improvizuotai kurdamas septynių–aštuonių natūralių judesių seką,
perteikdamas trumpą siužetą ar pasirinktą nuotaiką, išreikšdamas erdvės (aukš-
tai – žemai) ir laiko (greitai – lėtai) elementus.
		</ul>
		<strong>Vaidyba</strong><ul><li>Kurdamas lėlių, dramos vaidinimus pagal girdėtą pasaką ar pasiūlytą situaciją, improvizuoja
trumpas žodines veikėjų frazes, fizinius veiksmus, atskleidžia jų norus,
emocines būsenas. Tikslingai naudoja daiktus, teatro reikmenis, drabužius, aplinką.
Žaisdamas muzikinius žaidimus ir ratelius perteikia veikėjo mintis, emocijas. 
		</ul>
		<strong>Vizualinė raiška</strong><ul><li>Detalesniais, įtaigiais dailės darbeliais pasakoja realias ir fantastines istorijas,
įvykius. Vaizdus papildo grafiniais ženklais (raidėmis, skaičiais, žodžiais ir kt.).
Kuria pagal išankstinį sumanymą, nuosekliai bando jį įgyvendinti. Kartu su kitais
kuria bendrus dailės darbus. 
<li>Skirtingiems sumanymams įgyvendinti dažniausiai tikslingai pasirenka dailės
priemones ir technikas. Eksperimentuoja sudėtingesnėmis dailės technikomis,
skaitmeninio piešimo ir kitomis kompiuterinėmis technologijomis.</ul></td></tr>

<tr class="darker">
<td colspan="2">Vertybinė nuostata</td>
<td colspan="1">Jaučia meninės raiškos džiaugsmą, rodo norą aktyviai dalyvauti meninėje
veikloje.</td>
</tr>
<tr class="darker">
<td colspan="2">Esminis gebėjimas</td>
<td colspan="1">Spontaniškai ir savitai reiškia įspūdžius, išgyvenimus, mintis, patirtas emocijas
muzikuodamas, šokdamas, vaidindamas, vizualinėje kūryboje. </td>
</tr>
<tr>
    <td class='darker' rowspan="1"><div class="rotate"></div></td>
    <td class="step-center">7-asis žingsnis</td>
	<td><strong>Muzika</strong><ul><li>Klausosi kompozitorių ir liaudiškos muzikos kūrinių. Įvardija kai kuriuos klausytų
kūrinių autorius ir atpažįsta liaudiškos muzikos kūrinius. Tyrinėja melodijos
judėjimo kryptį, dermę, atlikimo rūšis.
<li>Išraiškingai, skambiu, natūraliu balsu dainuoja dainas a cappella ar su instrumentiniu
pritarimu. Dainuoja oktavos ribose solo, ansamblyje ir su visa grupe.
Tyrinėja balso skambėjimo ypatumus dainuodami su žodžiais ir be jų, žaisdami
įvairius žaidimus.
<li>Noriai, džiugiai groja ir improvizuoja daiktais iš gamtos (akmenukais, lazdelė-
mis, kriauklytėmis, kankorėžiais, riešutais ir kt.), savos gamybos vaikiškais muzikos
instrumentais. Groja ritmiškai, aiškiai, taisyklingai sėdi, laiko instrumentą
(būgnelį, trikampį, molio švilpynę ir kt.) ir pritaria dainoms, šokiams, rateliams,
muzikiniams žaidimams. 
<li>Kuria ritmus ir melodijas dainoms, improvizuoja muzikos instrumentais pagal
savo sugalvotą ar suaugusiojo pasiūlytą temą.
		</ul>
		<strong>Šokis</strong><ul><li>Žaisdamas šokamuosius žaidimus, šokdamas ratelius, natūralių judesių autorinius
šokius savitai reiškia nuotaikas. 
<li>Kuria natūralių judesių trumpą šokį, reaguodamas į muziką, perteikdamas trumpą
siužetą ar pasirinktą nuotaiką, išreikšdamas šokio elementus (erdvę, laiką ir
energiją).
		</ul>
		<strong>Vaidyba</strong><ul><li>Kurdamas lėlių, dramos vaidinimus pagal girdėtą pasaką, pasiūlytą meninę ar
ugdomąją situaciją, bendradarbiauja su kitu vaidinimo veikėju. Improvizuoja
dialogą, nuoseklią veiksmų seką, laisvai, su pasitikėjimu atskleidžia veikėjų
norus, emocines būsenas. Naudoja charakteringas balso intonacijas, judesius,
aprangos detales, prireikus – menamus ar tikrus reikmenis, dekoracijas.
Žaidžia muzikinius žaidimus ir ratelius, vaizduodamas veikėjų bendravimo
sceneles. 
		</ul>
		<strong>Vizualinė raiška</strong><ul><li>Drąsiai įgyvendina savo kūrybinius sumanymus. Numato galimą sumanymo
realizavimo seką bei rezultatą. Įvairiai varijuodamas spalvų, linijų bei formų
junginiais ir deriniais, išryškindamas detales pasakoja apie patirtus
išgyvenimus, matytus objektus, įsivaizduojamus įvykius, istorijas.
Kurdamas bendrus darbus bando derinti savo sumanymus ir veiksmus
su kitais. 
<li>Kūrybiškai panaudoja tradicines ir netradicines medžiagas, priemones,
technikas sumanymui įgyvendinti. Bando kurti naudodamasis skaitmeninio
piešimo ar kitomis kompiuterinėmis programomis, skaitmeninėmis
priemonėmis.
</ul></td>
    </tr>
</table>

</div>

<div class="dialog" id="dialog-13" data-title="13. ESTETINIS SUVOKIMAS">
<strong>Vaiko estetinis suvokimas – tai gebėjimas suvokti, pajausti ir reflektuoti estetinius potyrius</strong>,
kurie kyla matant ir jaučiant aplinkos (gamtos ir aplinkinio pasaulio, žmonių santykių, veiklos), meno
kūrinių, savo ir kitų kūrybos grožį.
<p>Estetinio suvokimo srityje vaikui ugdantis tobulėja:
<ul><li>nusiteikimas grožio, meninės kūrybos potyriams bei džiaugsmui; aplinkos, žmonių santykių,
meno, savo ir kitų kūrybos grožio pajauta;
<li>jautrumas grožiui, meno raiškos priemonėms (spalvai, linijai, formai, judesiui, muzikos
garsams ir kt.);
<li>jutiminių ir emocinių grožio išgyvenimų prisiminimas, apmąstymas ir dalijimasis su kitais. 
</ul>

<table>
    <tr>
<th></th>
<th>Žingsniai</th>
<th>Estetinio suvokimo srities pasiekimai</th>
</tr>
	<tr>
        <td class="darker" rowspan="3"><div class='rotate'>0–3 metai</div></td>
        <td class="step-center">1-asis žingsnis</td>
		<td><ul><li>Susidomi, trumpam sutelkia dėmesį bei rodo pasitenkinimą (krykštauja, siekia
paliesti rankomis), kai yra emocingai kalbinamas suaugusiojo, kai mato ryškius,
gražių formų daiktus, spalvingus paveikslėlius, žaislus, girdi ritmiškus muzikos ir
kitus garsus, mato šokio judesius.
		</ul></td>
    </tr>
	
		<tr><td class="step-center">2-asis žingsnis</td><td><ul><li>Skirtingai reaguoja girdėdamas besikeičiančių intonacijų suaugusiojo kalbinimą,
muzikos garsus, matydamas gražius gamtos bei aplinkos daiktus ar vaizdus,
spalvingas knygelių iliustracijas, šokančius ir vaidinančius vaikus ar suaugusiuosius.
Intuityviai mėgdžioja tai, kas jam patinka.
		</ul></td></tr>

		<tr><td class="step-center">3-iasis žingsnis</td><td><ul><li>Atpažįsta (suklūsta, rodo) kai kuriuos jau girdėtus muzikos kūrinius, matytus
šokius, ratelius, vaidinimo veikėjus, dailės kūrinius.
		<li>Emocingai reaguoja girdėdamas darnų garsų, intonacijų, žodžių sąskambį,
žiūrinėdamas savo ir kitų piešinėlius, spalvingas knygelių iliustracijas, žaislus,
džiaugdamasis savo puošnia apranga.
		<li>Paklaustas pasako, ar patiko muzikos kūrinėlis, dainelė, šokis, vaidinimas, dailės
darbelis.
		</ul>
		</td></tr>
	<tr>
        <td class="darker" rowspan="3"><div class='rotate'>4–6 metai</div></td>
        <td class="step-center">4-asis žingsnis</td>
		<td><ul><li>Džiaugiasi menine veikla, nori dainuoti, šokti, vaidinti, pasipuošti, gražiai atrodyti.
		<li>Skirtingai reaguoja (ramiai ar emocingai) klausydamas ir stebėdamas skirtingo
pobūdžio, kontrastingus meno kūrinius, aplinką. 
		<li>Keliais žodžiais ar sakiniais pasako savo įspūdžius apie klausytą muziką, dainelę,
eilėraštį, pasaką, matytą šokį, vaidinimą, dailės kūrinį, knygelių iliustracijas,
gamtos ir aplinkos daiktus ir reiškinius, pastebi ir apibūdina kai kurias jų detales.
Reaguoja į kitų nuomonę. 
		</ul>
		</td>
    </tr>
		<tr><td class="step-center">5-asis žingsnis</td><td><ul><li>Mėgaujasi muzikavimu, šokiu, vaidyba, dailės veikla. Rodo pasitenkinimą bendra
veikla ir kūryba, gėrisi savo ir kitų menine veikla, geru elgesiu, darbais. Gro-
žisi gamtos spalvomis, formomis, garsais. 
		<li>Pastebi kai kuriuos meninės kūrybos proceso ypatumus (siužetą, veikėjų bruo-
žus, nuotaiką, spalvas, veiksmus). Pasako, kaip jautėsi ir ką patyrė dainuodamas,
šokdamas, vaidindamas, piešdamas. 
		<li>Dalijasi įspūdžiais po koncertų, spektaklių, parodų, renginių lankymo. Pasako
savo nuomonę apie muzikos kūrinėlį, dainelę, šokį, vaidinimą, dailės darbelį,
aplinką, drabužį, tautodailės ornamentais papuoštus daiktus.
		</ul>
		</td></tr>

		<tr><td class="step-center">6-asis žingsnis</td><td><ul><li>Stengiasi kuo gražiau šokti, vaidinti, deklamuoti, dainuoti, groti, piešti, konstruoti.
Gėrisi ir grožisi savo menine kūryba. 
		<li>Pastebi papuoštą aplinką, meno kūrinius ir pasako, kas jam gražu. Palankiai
vertina savo ir kitų kūrybinę veiklą, pasako vieną kitą argumentą, kodėl gražu. 
		<li>Pasakoja įspūdžius apie muzikos, vaidinimo, šokio siužetą, matytus dailės, tautodailės
kūrinius, vaizduojamus įvykius, veikėjus, nuotaiką, kilusius vaizdinius. Plačiau papasakoja, ką sukūrė, kaip pats bei kiti dainavo, grojo, šoko, vaidino, piešė.
		</ul>
		</td></tr>

<tr class="darker">
<td colspan="2">Vertybinė nuostata</td>
<td colspan="1">Domisi, gėrisi, grožisi aplinka, meno kūriniais, menine veikla.</td>
</tr>
<tr class="darker">
<td colspan="2">Esminis gebėjimas</td>
<td colspan="1">Pastebi ir žavisi aplinkos grožiu, meno kūriniais, džiaugiasi savo ir kitų kūryba, jaučia,
suvokia ir apibūdina kai kuriuos muzikos, šokio, vaidybos, vizualaus meno estetikos
ypatumus, reiškia savo estetinius potyrius, dalijasi išgyvenimais, įspūdžiais.</td>
</tr>
<tr>
    <td class='darker' rowspan="1"><div class="rotate"></div></td>
    <td class="step-center">7-asis žingsnis</td>
	<td><ul><li>Nori matyti ir kurti grožį aplinkoje, kūrybinėje ir kasdienėje veikloje.
		<li>Atpažįsta ir pasakoja apie muzikoje, šokyje, vaidinime, vizualiajame mene
vaizduojamus gamtos, aplinkos, žmonių gyvenimo įvykius, objektus. Išsako
samprotavimus apie muzikos, vaidinimo, šokio, dailės, tautodailės kūrinių
siužetą, vaizduojamų įvykių kaitą, veikėjams būdingus bruožus, kilusius vaizdinius,
potyrius.
		<li>Pastebi, kas gražu, ir stengiasi tai panaudoti savo meninėje raiškoje.
		<li>Pasakoja, aiškina, ką pats sukūrė, kaip kūrė. Pasako, kurių kūrinių malonu klausyti
ir žiūrėti, o kurių nesinori klausyti, nemalonu žiūrėti. Reiškia savo nuomonę,
kodėl gražu. Domisi kitų kūrybinėmis idėjomis, sumanymais, geranoriškai juos
komentuoja.
		</ul>
		</td>
    </tr>
</table>

</div>

<div class="dialog" id="dialog-14" data-title="14. INICIATYVUMAS IR ATKAKLUMAS">
<strong>Iniciatyvumas</strong>  apibūdinamas kaip smalsumas, <strong>domėjimasis nauja informacija bei veikla</strong>,entuziastingas
naujos informacijos, naujų veiklos ar raiškos būdų ieškojimas, noras išmokti.
<p><strong>Atkaklumas</strong> yra <strong>gebėjimas</strong> gana <strong>ilgą laiką</strong> amžiaus galimybių ribose <strong> nepalikti atliekamos veiklos.</strong>
<p>Tai yra ilgalaikio dėmesingumo išlaikymas, gebėjimas išsaugoti svarbią informaciją bei idėjas atmintyje,
kurias ateityje vaikas gali panaudoti įvairiose veiklose ir situacijose, nepasimesti nesėkmės situacijoje,
didelėmis pastangomis pasiekti sėkmės. Atkaklumo pagrindas yra vaiko noras daryti įtaką aplinkiniam
pasauliui, noras pasiekti tikslų.</p>
<p>Iniciatyvumo ir atkaklumo srityje vaikui ugdantis tobulėja:
<ul><li>gebėjimas pačiam susirasti veiklą ir ją turiningai plėtoti,</li>
<li>gebėjimas įsitraukti į suaugusiojo pasiūlytą ugdymąsi skatinančią veiklą, susikoncentruoti ir
išradingai ją plėtoti,</li>
<li>gebėjimas susidoroti su kliūtimis siekiant sumanymų realizavimo.</li>
</ul>

<table>
    <tr>
<th></th>
<th>Žingsniai</th>
<th>Iniciatyvumo ir atkaklumo srities pasiekimai </th>
</tr>
	<tr>
        <td class="darker" rowspan="3"><div class='rotate'>0–3 metai</div></td>
        <td class="step-center">1-asis žingsnis</td>
		<td><ul><li>Pats juda (šliaužia, ropoja, eina) sudominusių žaislų, daiktų link. Trumpam sutelkia
žvilgsnį, seka judantį daiktą akimis, klausosi, atlieka tikslingus judesius,
veiksmus su daiktais.</li>
<li>Šypsodamasis, žvelgdamas į akis, čiauškėdamas, duodamas žaislą kitam paskatina
su juo žaisti; išreikšdamas norus parodo „taip“ arba „ne“.</li>
		</ul></td>
    </tr>
	
		<tr><td class="step-center">2-asis žingsnis</td><td><ul><li>Pats pasirenka daiktus, su jais žaidžia, daug kartų atkakliai bando atlikti naują
veiksmą, kartoja tai, kas pavyko. Judesį, veiksmą ar garsą gali pakartoti tuoj
pat, po kelių valandų, dienų, todėl savarankiškai modeliuoja kelis judesius ar
veiksmus į vieną seką. Trumpam atitraukus dėmesį vėl sugrįžta prie ankstesnės
veiklos.</li>
<li>Pats noriai mokosi iš tų, su kuriais jaučiasi saugus.</li>
<li>Veiksmais ir atskirais žodžiais reiškia norus, veda suaugusįjį prie dominančių
daiktų. Protestuoja, reiškia nepasitenkinimą, negalėdamas įveikti kliūties.</li>
		</ul></td></tr>

		<tr><td class="step-center">3-iasis žingsnis</td><td><ul><li>Nuolat energingai žaidžia, ką nors veikia, laisvai juda erdvėje, pats keičia veiklą,
pasirenka vieną iš kelių daiktų, sugalvoja būdus, kaip pasiekti neprieinamą norimą
daiktą.</li>
		<li>Mėgsta išbandyti suaugusiojo pasiūlytus naujus žaislus, žaidimus, neįprastą
veiklą.</li>
		<li>Ekspresyviai reiškia savo norus, sako „ne“.</li>
		</ul>
		</td></tr>
	<tr>
        <td class="darker" rowspan="3"><div class='rotate'>4–6 metai</div></td>
        <td class="step-center">4-asis žingsnis</td>
		<td><ul><li>Dažniausiai pats pasirenka ir kurį laiką kryptingai plėtoja veiklą vienas ir su
draugais.</li>
		<li>Kviečiant, sudominant įsitraukia į suaugusiojo pasiūlytą veiklą jam, vaikų grupelei
ar visai vaikų grupei.</li>
		<li>Susidūręs su kliūtimi arba nesėkme, bando ką nors daryti kitaip arba laukia
suaugusiojo pagalbos. Siekia savarankiškumo, bet vis dar laukia suaugusiųjų
paskatinimo, padrąsinimo. </li>
		</ul>
		</td>
    </tr>
		<tr><td class="step-center">5-asis žingsnis</td><td><ul><li>Pats pasirenka ir ilgesnį laiką kryptingai plėtoja veiklą vienas ir su draugais. 
		<li>Lengviau pereina nuo paties pasirinktos prie suaugusiojo jam, vaikų grupelei ar
visai vaikų grupei pasiūlytos veiklos. Suaugusiojo pasiūlytą veiklą atlieka susitelkęs,
išradingai, savaip, savarankiškai.</li>
		<li>Ilgesnį laiką pats bando įveikti kliūtis savo veikloje, nepavykus kreipiasi pagalbos
į suaugusįjį.</li>
		</ul>
		</td></tr>

		<tr><td class="step-center">6-asis žingsnis</td><td><ul><li>Turiningai plėtoja paties pasirinktą veiklą, ją tęsia po dienos miego, kitą dieną,
kelias dienas.</li>
		<li>Susidomėjęs ilgesniam laikui įsitraukia į suaugusiojo jam, vaikų grupelei ar visai
vaikų grupei pasiūlytą veiklą, siūlo vaikams ir suaugusiajam įsitraukti į jo paties
sugalvotą veiklą.</li>
		<li>Savarankiškai bando įveikti kliūtis savo veikloje, nepasisekus bando įtraukti
bendraamžius ir tik po to kreipiasi į suaugusįjį.</li>
		</ul>
		</td></tr>

<tr class="darker">
<td colspan="2">Vertybinė nuostata</td>
<td colspan="1">Didžiuojasi savimi ir didėjančiais savo gebėjimais.</td>
</tr>
<tr class="darker">
<td colspan="2">Esminis gebėjimas</td>
<td colspan="1">Savo iniciatyva pagal pomėgius pasirenka veiklą, ilgam įsitraukia ir ją plėtoja,
geba pratęsti veiklą po tam tikro laiko tarpo, kreipiasi į suaugusįjį pagalbos,
kai pats nepajėgia susidoroti su kilusiais sunkumais.</td>
</tr>
<tr>
    <td class='darker' rowspan="1"><div class="rotate"></div></td>
    <td class="step-center">7-asis žingsnis</td>
	<td><ul><li>Nusiteikęs pradėti, siūlyti naują veiklą, ją užbaigti, užmegzti naują draugystę,
naują kontaktą su suaugusiaisiais.</li>
		<li>Savo iniciatyva pagal savo pomėgius, interesus pasirenka veiklą, ilgam įsitraukia,
ją plėtoja.</li>
		<li>Nuo pradžios iki pabaigos gali atlikti ir nepatrauklią veiklą.</li>
		<li>Palankiai priima iššūkius, bando pats įveikti kliūtis.</li>
		<li>Kreipiasi į suaugusįjį pagalbos, kai pats nepajėgia susidoroti su kilusiais sunkumais.</li>
		</ul>
		</td>
    </tr>
</table>

</div>


<div class="dialog" id="dialog-15" data-title="15. TYRINĖJIMAS">
<strong>Tyrinėjimas –</strong>  tai procesas, kai atidžiai žiūrint, klausant, uodžiant, liečiant, klausinėjant, <strong>ieškant informacijos</strong> įvairiuose šaltiniuose sužinoma apie gyvosios ir negyvosios gamtos objektus bei reiškinius,
žmogaus sukurtus daiktus, žmonių gyvenimą.
<p><strong>Tyrinėjimas apima ir bandymus</strong> (eksperimentus), kai keičiame daiktus ar medžiagas, aplinkos sąlygas, pvz., kai išardome ir sudedame kitaip, kai tirpiname, šildome, šaldome medžiagas, kai sodiname
augalus ir stebime, kaip jų augimą veikia šviesa, laistymas ir pan.
<p><strong>Tyrinėjimas</strong> apibūdinamas kaip:
<ul>
<li><strong>smalsumas,</strong> domėjimasis, noras pažinti, išsiaiškinti kas ir kodėl vyksta;</li>
<li><strong>gebėjimai kelti problemas</strong>, klausimus ir tyrinėjimo tikslus, numatyti (spėti) laukiamus tyrinė-
jimo rezultatus;</li>
<li><strong>gebėjimas numatyti</strong> kaip tyrinės ir pasirinkti priemones;</li>
<li><strong>gebėjimas apmąstyti</strong> tyrinėjimo rezultatus, juos aptarti ir padaryti išvadas;</li>
<li>tyrinėjimo žingsnių, <strong>saugaus tyrinėjimo taisyklių žinojimas</strong> ir supratimas.</li>
</ul>
<p>Tyrinėjimo srityje vaikui ugdantis tobulėja:
<ul><li>domėjimasis supančia aplinka,</li>
	<li>tyrinėjimas pasinaudojant įvairiais pojūčiais,</li>
	<li>atrastų, sužinotų dalykų aptarimas.</li>
</ul>
<table>
    <tr>
<th></th>
<th>Žingsniai</th>
<th>Tyrinėjimo srities pasiekimai</th>
</tr>
	<tr>
        <td class="darker" rowspan="3"><div class='rotate'>0–3 metai</div></td>
        <td class="step-center">1-asis žingsnis</td>
		<td><ul><li>Stengiasi pamatyti, išgirsti, paliesti, paimti, paragauti žaislus ir kitus daiktus.</li>
<li>Reaguoja į tai, kas vyksta aplinkui, bando dalyvauti (mimika, judesiai, garsai).</li>
		</ul></td>
    </tr>
	
		<tr><td class="step-center">2-asis žingsnis</td><td><ul><li>Stengiasi išbandyti žaislus ar daiktus, stebi, kas vyksta aplinkui, rodo kitiems,
ką pavyko padaryti.</li>
<li>Mėgsta žaisti slėpynių. Patikusį veiksmą prašo pakartoti daug kartų.</li>
		</ul></td></tr>

		<tr><td class="step-center">3-iasis žingsnis</td><td><ul><li>Atsargiai elgiasi su nepažįstamais daiktais ir medžiagomis, tačiau rodo susidomėjimą,
bando aiškintis, kas tai yra, kaip ir kodėl tai veikia, vyksta.</li>
		</ul>
		</td></tr>
	<tr>
        <td class="darker" rowspan="3"><div class='rotate'>4–6 metai</div></td>
        <td class="step-center">4-asis žingsnis</td>
		<td><ul><li>Pats pasirenka žaidimui ar kitai veiklai reikalingus daiktus ir medžiagas, paaiškina,
kodėl pasirinko.</li>
		<li>Žaisdamas tyrinėja, išbando daiktus bei medžiagas (pvz., plaukia ar skęsta, rieda ar
sukasi ratu, tinka daiktai vienas prie kito, ar ne ir pan.).</li>
		</ul>
		</td>
    </tr>
		<tr><td class="step-center">5-asis žingsnis</td><td><ul><li>Geba suvokti ryšį tarp to, kaip daiktas padarytas ir jo paskirties (pvz., ratai yra
apvalūs, nes mašinos paskirtis yra judėti).</li>
		<li>Domisi medžiagomis, iš kurių padaryti daiktai, ir jų savybėmis. Suvokia medžiagos,
iš kurios padarytas daiktas, pasirinkimo tikslingumą (pvz., kodėl mašinos
korpusas iš metalo, o padangos iš gumos).</li>
		<li>Paaiškina, kad su nežinomais daiktais ir medžiagomis reikia elgtis atsargiai,
stengiasi taip daryti.</li>
		<li>Išskiria akivaizdžius daiktų, medžiagų, gyvūnų, augalų bruožus, savybes, kalbė-
dami apie tai kartais susieja skirtingus pastebėjimus.</li>
		</ul>
		</td></tr>

		<tr><td class="step-center">6-asis žingsnis</td><td><ul><li>Domisi aplinka, mėgsta stebėti, kaip auga augalai, kaip elgiasi gyvūnai, noriai
atlieka paprastus bandymus, tyrinėja, iš kokių medžiagų padaryti daiktai, kur jie
naudojami.</li>
		<li>Samprotauja apie tai, ką atrado, sužinojo, kelia tolesnius klausimus, siūlo idėjas,
ką dar galima būtų tyrinėti. </li>
		<li>Domisi, kaip seniau gyveno žmonės, kaip žmonės gyvena kitose šalyse.</li>
		<li>Aktyviai tyrinėdami aplinką demonstruoja vis didėjančią kūno kontrolę, tinkamai
pasitelkia visus pojūčius, savo galimybėms išplėsti pasitelkia įrankius ir kitas
priemones (pvz., lupą, mikroskopą).</li>
		<li>Su suaugusiaisiais ar kitais vaikais aptaria nesudėtingų stebėjimų, bandymų ar
konstravimo planus, numato rezultatą, mokosi pavaizduoti juos nesudėtingose
lentelėse, diagramose, išradingai, kūrybiškai pristato savo tyrinėjimus ir kitus
darbus.</li>
		<li>Stebėdamas fotografijas aiškinasi, kuo yra panašūs su savo artimaisiais, kuo
skiriasi nuo jų.</li>
		<li>Lygina daiktus, medžiagas, gyvūnus ir augalus, atsižvelgdamas į savybes, juos
tikslingai grupuoja ir klasifikuoja.</li>
		</ul>
		</td></tr>

<tr class="darker">
<td colspan="2">Vertybinė nuostata</td>
<td colspan="1">Smalsus, domisi viskuo, kas vyksta aplinkui, noriai stebi, bando, samprotauja. </td>
</tr>
<tr class="darker">
<td colspan="2">Esminis gebėjimas</td>
<td colspan="1">Aktyviai tyrinėja save, socialinę, kultūrinę ir gamtinę aplinką, įvaldo tyrinėjimo
būdus (stebėjimą, bandymą, klausinėjimą), mąsto ir samprotauja apie tai, ką
pastebėjo, atrado, pajuto, patyrė.</td>
</tr>
<tr>
    <td class='darker' rowspan="1"><div class="rotate"></div></td>
    <td class="step-center">7-asis žingsnis</td>
	<td><ul><li>Domisi gamtinės aplinkos tyrinėjimais (stebėjimais, bandymais). Siūlo idėjas,
ką norėtų tyrinėti, išbandyti, pasirenka tai, kas labiausiai domina.</li> 
		<li>Suaugusiojo padedamas numato, ką reikės atlikti, pasiūlo, kokias priemones
ir kaip galima panaudoti tyrinėjant, stebi ar bando, aptaria ir daro išvadas.
Tai, ką sužinojo tyrinėdamas, susieja su tuo, ką jau žinojo, išsako savo abejones
ir ką dar norėtų sužinoti.</li>
		<li>Suaugusiojo padedamas atlieka paprastus artimiausios socialinės aplinkos,
žmonių gyvenimo, darbo, kūrybos tyrinėjimus (stebi, klausia, nupiešia, nufotografuoja,
ieško informacijos įvairiuose šaltiniuose).</li>
		<li>Tyrinėjimams naudoja skaitmenines technologijas (kompiuterį, mobilųjį telefoną,
fotoaparatą ir kt.).</li>
		<li>Tyrinėja susidomėjęs, atidžiai, stengiasi viską pastebėti. Samprotauja apie
pastebėtas aplinkos objektų savybes, požymius, žmonių gyvenimo būdo bruo-
žus, palygina, kritiškai vertina.</li>
		<li>Atskiria dalis nuo visumos, sujungia dalis į visumą, sudaro sekas, grupuoja,
klasifikuoja. Projektuoja, modeliuoja.</li>
		<li>Tyrinėjimo rezultatus pavaizduoja piešiniu, nuotraukomis, nesudėtinga schema,
pristato kitiems. Baigęs tyrinėjimą, aptaria, kaip pavyko viską atlikti, ką galima
buvo daryti kitaip.</li>
		<li>Stebėdamas ar vykdydamas bandymą stengiasi viską atlikti tvarkingai, laikytis
sutartų taisyklių. Paaiškina, kad gamtą ar socialinę aplinką tyrinėti reikia atsargiai,
nežalojant, neniokojant, nurodo, ko reikia saugotis gamtoje (neragauti
nepažįstamų vaisių, uogų, neimti į rankas vabzdžių ir t. t.), buityje, gatvėje.</li>
		</ul>
		</td>
    </tr>
</table>

</div>

<div class="dialog" id="dialog-16" data-title="16. PROBLEMŲ SPRENDIMAS">
<strong>Problemų sprendimas – tai gebėjimas</strong> suprasti, įvertinti, interpretuoti ir <strong>pritaikyti žinias</strong> iššūkių,
sudėtingų užduočių ar <strong>sunkumų sprendimui</strong>.
<p>Išskiriami tokie problemų sprendimo žingsniai:
<ul><li>problemos nustatymas ir įvardijimas (aš negaliu padaryti to ir to, nežinau, kaip padaryti tai ir tai),</li>
<li>įvairių sprendimų ar išeičių paieška,</li>
<li>vieno kurio nors sprendimo pasirinkimas ir išbandymas,</li>
<li>įvertinimas, kas iš to išėjo, kokios pasekmės.</li>
</ul>
<p>Problemų sprendimo srityje vaikui ugdantis tobulėja:
<ul><li>problemų atpažinimas, įžvelgimas,</li>
	<li>sprendimų, išeičių paieška ir tinkamiausio sprendimo pasirinkimas bei įgyvendinimas, pasekmių,
panaudojus sprendimą, stebėjimas ir apmąstymas,</li>
	<li>mokymasis įveikti nesėkmes.</li>
</ul>
<table>
    <tr>
<th></th>
<th>Žingsniai</th>
<th>Problemų sprendimo srities pasiekimai </th>
</tr>
	<tr>
        <td class="darker" rowspan="3"><div class='rotate'>0–3 metai</div></td>
        <td class="step-center">1-asis žingsnis</td>
		<td><ul><li>Pakartoja nepasisekusį veiksmą, jį keičia, kad pasiektų laukiamą rezultatą.
Mimika, gestais ir žodžiais parodo, kad susidūrė su kliūtimi, tikėdamasis suaugusiojo
ar vyresnio vaiko pagalbos.</li>
		</ul></td>
    </tr>
	
		<tr><td class="step-center">2-asis žingsnis</td><td><ul><li>Susidūręs su sudėtinga veikla, kliūtimi, išbando jau žinomus veikimo būdus.
Stebi, kaip panašioje situacijoje elgiasi kiti ir išbando jų naudojamus būdus.</li>
<li>Nepavykus įveikti kliūties, meta veiklą arba laukia pagalbos.</li>
		</ul></td></tr>

		<tr><td class="step-center">3-iasis žingsnis</td><td><ul><li>Drąsiai imasi sudėtingos veiklos, atkakliai, keisdamas veikimo būdus bando ją
atlikti pats, stebi savo veiksmų pasekmes.</li>
<li>Nepavykus įveikti sudėtingos veiklos ar kliūties, prašo pagalbos arba meta
veiklą.</li>
		</ul>
		</td></tr>
	<tr>
        <td class="darker" rowspan="3"><div class='rotate'>4–6 metai</div></td>
        <td class="step-center">4-asis žingsnis</td>
		<td><ul><li>Supranta, kad susidūrė su sudėtinga veikla, kliūtimi, problema.</li>
		<li>Nori ją įveikti, išbando paties taikytus, stebėtus ar naujai sugalvotus veikimo būdus.</li>
		<li>Stebi savo veiksmų pasekmes, supranta, kada pavyko įveikti sunkumus. Nepasisekus
prašo suaugusiojo pagalbos.</li>
		</ul>
		</td>
    </tr>
		<tr><td class="step-center">5-asis žingsnis</td><td><ul><li>Retsykiais pats ieško sunkumų, kliūčių, aktyviai bando įveikti sutiktus sunkumus.
		<li>Ieško tinkamų sprendimų, tariasi su kitais, mokosi iš nepavykusių veiksmų,
poelgių.</li>
		<li>Nepasisekus samprotauja, ką galima daryti toliau, kitaip arba prašo suaugusiojo
pagalbos.</li>
		</ul>
		</td></tr>

		<tr><td class="step-center">6-asis žingsnis</td><td><ul><li>Atpažįsta, su kokiu sunkumu ar problema susidūrė.</li>
		<li>Ieško tinkamų sprendimų, pradeda numatyti priimtų sprendimų pasekmes,
tariasi su kitais ir atsižvelgia į jų nuomonę, siūlo ir priima pagalbą, mokosi iš savo
ir kitų klaidų.</li>
		<li>Nepasisekus bando kelis kartus, ieškodamas vis kitos išeities arba prašo kito
vaiko ar suaugusiojo pagalbos.</li>
		</ul>
		</td></tr>

<tr class="darker">
<td colspan="2">Vertybinė nuostata</td>
<td colspan="1">Nusiteikęs ieškoti išeičių kasdieniams iššūkiams bei sunkumams įveikti.  </td>
</tr>
<tr class="darker">
<td colspan="2">Esminis gebėjimas</td>
<td colspan="1">Atpažįsta ką nors veikiant kilusius iššūkius bei sunkumus, dažniausiai supranta,
kodėl jie kilo, suvokia savo ir kitų ketinimus, ieško tinkamų sprendimų ką nors
išbandydamas, tyrinėdamas, aiškindamasis, bendradarbiaudamas, pradeda
numatyti priimtų sprendimų pasekmes.</td>
</tr>
<tr>
    <td class='darker' rowspan="1"><div class="rotate"></div></td>
    <td class="step-center">7-asis žingsnis</td>
	<td><ul><li>Sudėtingą veiklą, kliūtis, sunkumus ar problemas priima natūraliai, nusiteikęs
juos įveikti.</li>
		<li>Ieškodamas, kaip susidoroti su sudėtinga veikla, kliūtimi ar problema, samprotauja
apie sprendimus, jų pasekmes, pasirenka tinkamiausią sprendimą iš kelių
galimų. Tariasi su kitais, drauge ieško išeities, siūlo ir priima pagalbą.</li>
		<li>Supranta ir pasako, ar problemą gali įveikti pats kartu su kitais vaikais, ar būtina
suaugusiojo pagalba.</li>
		</ul>
		</td>
    </tr>
</table>

</div>

<div class="dialog" id="dialog-17" data-title="17. KŪRYBIŠKUMAS">
<strong>Kūrybiškumas</strong> suprantamas kaip <strong>asmenybės savybė</strong>, susijusi su gebėjimu <strong>atrasti tai, kas nauja,
originalu, netikėta</strong>.
<p>Kūrybiškumui ugdyti būtina saugi, vaiko saviraišką skatinanti aplinka.</p>
<p>Kūrybiškumo srityje vaikui ugdantis tobulėja:
<ul><li>domėjimasis naujais, nežinomais, sudėtingais dalykais;</li>
<li>gebėjimas įžvelgti problemas, klausinėti, diskutuoti, įsivaizduoti, fantazuoti;</li>
<li>gebėjimas ieškoti atsakymų, netikėtų idėjų, kurti variantus, savaip pertvarkyti, pritaikyti;</li>
<li>drąsa veikti, daryti savaip.</li>
</ul></p>

<table>
    <tr>
<th></th>
<th>Žingsniai</th>
<th>Kūrybiškumo srities pasiekimai</th>
</tr>
	<tr>
        <td class="darker" rowspan="3"><div class='rotate'>0–3 metai</div></td>
        <td class="step-center">1-asis žingsnis</td>
		<td><ul><li>Pastebi ir smalsiai, gyvai reaguoja į naujus daiktus, žmones, aplinkos pasikeitimus.</li></ul></td>
    </tr>
	
		<tr><td class="step-center">2-asis žingsnis</td><td><ul><li>Domisi naujais daiktais, vaizdais, garsais, judesiais.</li>
<li>Atranda naujus veiksmus (tapyti ant veidrodžio, ridenti, nardinti į vandenį ir kt.)
ir taiko juos daiktų tyrinėjimui.</li>
		</ul></td></tr>

		<tr><td class="step-center">3-iasis žingsnis</td><td><ul><li>Atranda vis naujus dalykus artimiausioje įprastoje aplinkoje.</li>
<li>Įsivaizduoja gyvūnus, augalus, daiktus, apie kuriuos jam pasakojama, skaitoma. Žaisdamas atlieka įsivaizduojamus simbolinius veiksmus.</li>
		</ul>
		</td></tr>
	<tr>
        <td class="darker" rowspan="3"><div class='rotate'>4–6 metai</div></td>
        <td class="step-center">4-asis žingsnis</td>
		<td><ul><li>Įžvelgia naujas įprastų daiktų bei reiškinių savybes.</li>
		<li>Pasitelkia vaizduotę ką nors veikdamas: žaisdamas, pasakodamas, judėdamas.</li>
		<li>Sugalvoja įdomių idėjų, skirtingų veikimo būdų.</li>
		</ul>
		</td>
    </tr>
		<tr><td class="step-center">5-asis žingsnis</td><td><ul><li>Klausinėja, aiškindamasis jam naujus, nežinomus dalykus.</li>
		<li>Savitai suvokia ir vaizduoja pasaulį.</li>
		<li>Išradingai, neįprastai naudoja įvairias medžiagas, priemones. Lengvai sugalvoja,
keičia, pertvarko savitas idėjas, siūlo kelis variantus.</li>
		<li>Džiaugiasi savitu veikimo procesu ir rezultatu.</li>
		</ul>
		</td></tr>

		<tr><td class="step-center">6-asis žingsnis</td><td><ul><li>Nori atlikti ir suprasti vis daugiau naujų, nežinomų dalykų.</li>
		<li>Kelia probleminius klausimus, diskutuoja, svarsto, įsivaizduoja, fantazuoja.</li>
		<li>Ieško atsakymų, naujų idėjų, netikėtų sprendimų, neįprastų medžiagų, priemonių,
atlikimo variantų, lengvai, greitai keičia, pertvarko, pritaiko, siekia savito
rezultato.</li>
		<li>Drąsiai, savitai eksperimentuoja, nebijo suklysti, daryti kitaip.</li>
		</ul>
		</td></tr>

<tr class="darker">
<td colspan="2">Vertybinė nuostata</td>
<td colspan="1">Jaučia kūrybinės laisvės, spontaniškos improvizacijos bei kūrybos džiaugsmą.</td>
</tr>
<tr class="darker">
<td colspan="2">Esminis gebėjimas</td>
<td colspan="1">Savitai reiškia savo įspūdžius įvairioje veikloje, ieško nežinomos informacijos,
siūlo naujas, netikėtas idėjas ir jas savitai įgyvendina.</td>
</tr>
<tr>
    <td class='darker' rowspan="1"><div class="rotate"></div></td>
    <td class="step-center">7-asis žingsnis</td>
	<td><ul><li>Greitai pastebi ir renkasi tai, kas nauja, sudėtinga.</li>
		<li>Įžvelgia dar neišbandytas veiklos, saviraiškos galimybes, kelia probleminius
klausimus, diskutuoja, svarsto.</li>
		<li>Pasineria į kūrybos procesą, įsivaizduoja, fantazuoja, remiasi vidine nuojauta,
turimas patirtis jungdamas į naujas idėjas, simbolius, naujus atlikimo būdus.</li>
		<li>Pats ir kartu su kitais ieško atsakymų, netikėtų idėjų, savitų sprendimų, neįprastų
medžiagų, atlikimo variantų, lengvai, greitai keičia, pertvarko, pritaiko, siekia
savito rezultato.</li>
		<li>Nebijo daryti kitaip, būti kitoks, drąsiai, savitai eksperimentuoja.</li>
		</ul>
		</td>
    </tr>
</table>

</div>

<div class="dialog" id="dialog-18" data-title="18. MOKĖJIMAS MOKYTIS">
<strong>Mokėjimas mokytis</strong> ikimokykliniame amžiuje suprantamas kaip noras ko nors išmokti ir atkaklus
to tikslo siekimas.
<p>Tai <strong>gebėjimas išsikelti</strong> mokymosi ar kitos veiklos <strong>tikslus</strong>, planuoti, kaip jų bus siekiama, pasirinkti
tinkamus veikimo būdus, apmąstyti, kaip sekėsi ir kokie veiklos ar mokymosi rezultatai, kokie tolesni
tikslai.
<p>Mokėjimo mokytis srityje vaikui ugdantis tobulėja:
<ul><li>numatymas, ko nori išmokti,</li>
<li>aktyvus mokymasis,</li>
<li>gebėjimas apmąstyti, ko išmoko.</li>
</ul>

<table>
    <tr>
<th></th>
<th>Žingsniai</th>
<th>Mokėjimo mokytis srities pasiekimai</th>
</tr>
	<tr>
        <td class="darker" rowspan="3"><div class='rotate'>0–3 metai</div></td>
        <td class="step-center">1-asis žingsnis</td>
		<td><ul><li>Reaguoja į kalbinimą, mimiką, žaislus, daiktus. Stebi ir susitapatina, mėgdžioja,
siekia išgauti tą patį rezultatą. Pradeda tyrinėti žaislus ir daiktus visais pojūčiais.</li>
		</ul></td>
    </tr>
	
		<tr><td class="step-center">2-asis žingsnis</td><td><ul><li>Nori naujų įspūdžių, todėl aktyviai domisi aplinkos daiktais, juda, norėdamas
paimti, pasiekti, išbandyti žaislus ar daiktus. Stebi ir mėgdžioja, klausia.</li>
<li>Patraukia, pastumia, paridena, įdeda daiktus ir stebi, kas vyksta, bando pakartoti
pavykusį veiksmą. Stebi, mėgdžioja, klausia.</li>
		</ul></td></tr>

		<tr><td class="step-center">3-iasis žingsnis</td><td><ul><li>Veikia spontaniškai ir tikėdamasis tam tikro rezultato</li>
<li>Klausia, kaip kas nors vyksta, kaip veikia, atidžiai stebi, bando. Modeliuoja veiksmus ir siužetinio žaidimo epizodus.</li>
<li>Džiaugiasi tuo, ko išmoko.</li>
		</ul>
		</td></tr>
	<tr>
        <td class="darker" rowspan="3"><div class='rotate'>4–6 metai</div></td>
        <td class="step-center">4-asis žingsnis</td>
		<td><ul><li>Pasako, parodo, ką nori išmokti.</li>
		<li>Mėgsta kūrybiškai žaisti, veikti, siūlo žaidimų ir veiklos idėjas, imasi iniciatyvos
joms įgyvendinti, pastebi ir komentuoja padarinius.</li>
		<li>Pasako, ką veikė ir ką išmoko.</li>
		</ul>
		</td>
    </tr>
		<tr><td class="step-center">5-asis žingsnis</td><td><ul><li>Norėdamas ką nors išmokti, pasako, ko nežino ar dėl ko abejoja.</li>
		<li>Drąsiai spėja, bando, klysta ir taiso klaidas, klauso, ką sako kiti, pasitikslina.</li>
		<li>Aptaria padarytus darbus, planuoja, ką darys toliau, spėlioja, kas atsitiks, jeigu...</li>
		</ul>
		</td></tr>

		<tr><td class="step-center">6-asis žingsnis</td><td><ul><li>Kalba apie tai, ką norėtų išmokti, ką darys, kad išmoktų, numato, ką veiks toliau,
kai išmoks. Laiko save tikru mokiniu, atradėju.</li>
		<li>Drąsiai ieško atsakymų į klausimus, rodo iniciatyvą iškeldamas ir spręsdamas
problemas. Išsiaiškina, kokios informacijos reikia, randa reikiamą informaciją
įvairiuose šaltiniuose, pvz., enciklopedijose, žinynuose. Siūlo ir jungia idėjas bei
strategijas joms įgyvendinti.</li>
		<li>Pasako, ką jau išmoko, ko dar mokosi, paaiškina, kaip mokėsi, kaip mokysis toliau.</li>
		</ul>
		</td></tr>

<tr class="darker">
<td colspan="2">Vertybinė nuostata</td>
<td colspan="1">Noriai mokosi, džiaugiasi tuo, ką išmoko. </td>
</tr>
<tr class="darker">
<td colspan="2">Esminis gebėjimas</td>
<td colspan="1">Mokosi žaisdamas, stebėdamas kitus vaikus ir suaugusiuosius, klausinėdamas,
ieškodamas informacijos, išbandydamas, spręsdamas problemas, kurdamas,
įvaldo kai kuriuos mokymosi būdus, pradeda suprasti mokymosi procesą. </td>
</tr>
<tr>
    <td class='darker' rowspan="1"><div class="rotate"></div></td>
    <td class="step-center">7-asis žingsnis</td>
	<td><ul><li>Samprotauja apie mokymąsi mokykloje, išsako savo požiūrį į mokymąsi, jo prasmę.</li>
		<li>Kelia nesudėtingus mokymosi tikslus ir numato, kaip jų sieks, samprotauja apie
tai, kaip pavyko.</li>
		<li>Kalba apie tai, kad daug sužinoti ir daug ko išmokti apie žmones, daiktus, gamtą
galima klausinėjant, stebint aplinką, iš spaudos ir knygų, enciklopedijų, interneto,
TV ir kt.</li>
		<li>Savarankiškai susiranda nesudėtingą informaciją, klausinėja draugus, suaugusiuosius,
kai reikia, prašo suaugusiųjų pagalbos.</li>
		<li>Siūlo idėjas, ko ir kaip galima būtų mokytis kartu, imasi iniciatyvos joms įgyvendinti.
Mokosi iš kitų.</li>
		</ul>
		</td>
    </tr>
</table>

</div>
</div>
