<?php if(!defined('ADMIN')) exit(); 
abstract class ProgressAndAchievementsFieldType {
	const Note = 1;
	const Achievement = 0;
}
?>
<h1>Vaiko pažanga ir pasiekimai</h1>
<div id="content">
<?=ui_print()?>
<?php
if(isset($_POST['data']) && !preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $_POST['data'])) logdie("Blogas datos formatas: Jis turi būti „".date('Y-m-d')."“, o ne „".filterText($_POST['data'])."“. ".back());

if(isset($_POST['save']) && isset($_GET['kid_id']) && isset($_POST['data'])) {
	if(isset($_FILES['file']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
		//if(verify_image($_FILES['file']['tmp_name'])) {
			$photo_pic = $_FILES['file'];
			$photo_ext = strrchr($photo_pic['name'], ".");

			if($photo_pic['size'] >= 1024*1024*5) {
				echo "<div class=\"red center\">Klaida: failas gali užimti iki 5 MB</div>";
			} elseif(!in_array(strtolower($photo_ext), $file_type)) {
				echo "<div class=\"red center\">Klaida: Failo tipas privalo būti iš šių failų tipų: ".implode(', ', $file_type)."</div>";
			} else {
				$photo_new_name = UPLOAD_DIR.'progress_and_achievements/'.(int)$_GET['kid_id']."/".$photo_pic['name'];
				if(is_file($photo_new_name)) {//file_exists()
					if(sha1_file($photo_new_name) == sha1_file($photo_pic['tmp_name'])) {
						msgBox('ERROR', 'Failas neišsaugotas. Prie šio vaiko toks dokumentas jau pridėtas, antrą kartą kelti tokį patį nėra prasmės. Jei reikia tik pakeiskite papildomus laukelius prie jau esamo dokumento.');
					} else {
						msgBox('ERROR', 'Failas neišsaugotas. Prie šio vaiko dokumentas su tokiu pačiu pavadinimu jau yra (sistema netikrina ar toks pats turinys). 1) Jau pridėtas į sistemą toks pats dokumentas, tada dar kartą kelti jo nebereikia. 2) Pridedate kitą dokumentą į sistemą, kurio failo pavadinimas sutampa, tuomet pridedamo pakeiskite failo pavadinimą kompiuteryje ir bandykite dar kartą.');
					}
				} else {
					$do_save = true;
					if(!is_dir(UPLOAD_DIR.'progress_and_achievements'))
						if(!mkdir(UPLOAD_DIR.'progress_and_achievements', 0777)) {
							msgBox('ERROR', 'Nepavyko sukurti katalogo'.UPLOAD_DIR.'progress_and_achievements');
							$do_save = false;
						}
					if(!is_dir(UPLOAD_DIR.'progress_and_achievements/'.(int)$_GET['kid_id']))
						if(!mkdir(UPLOAD_DIR.'progress_and_achievements/'.(int)$_GET['kid_id'], 0777)) {
							msgBox('ERROR', 'Nepavyko sukurti katalogo');
							$do_save = false;
						}
					if(move_uploaded_file($photo_pic['tmp_name'], $photo_new_name))
						msgBox('OK', 'Failas išsaugotas.');
					else {
						msgBox('ERROR', 'Failas neišsaugotas.');
						$do_save = false;
					}
					//chmod($photo_new_name, 0644);
					if($do_save) {
						if(db_query("INSERT INTO `". DB_achievements ."` SET 
		`data`='".db_fix($_POST['data'])."', `value`='".db_fix($_POST['note'])."', 
		`achievement`='".db_fix($_POST['achievement'])."', `vaiko_id`=".(int)$_GET['kid_id'].", 
		`attachment`='".db_fix($photo_pic['name'])."',
		`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'"))
							echo "<div class=\"green center\">Dokumento informacija išsaugota duomenų bazėje.</div>";
					} else {
						db_query("INSERT INTO `". DB_achievements ."` SET 
		`data`='".db_fix($_POST['data'])."', `value`='".db_fix($_POST['note'])."', 
		`achievement`='".db_fix($_POST['achievement'])."', `vaiko_id`=".(int)$_GET['kid_id'].", 
		`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
					}
				}
			}
		//} else echo "<div class=\"red center\">Failas nėra paveiksliukas.</div>";
	} else {
		db_query("INSERT INTO `". DB_achievements ."` SET 
		`data`='".db_fix($_POST['data'])."', `value`='".db_fix($_POST['note'])."', 
		`achievement`='".db_fix($_POST['achievement'])."', `vaiko_id`=".(int)$_GET['kid_id'].", 
		`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
	}
	$achievement_id = (int)mysqli_insert_id($db_link);
	if(!empty($_POST['rights']))
		foreach($_POST['rights'] as $person_type)
			db_query("INSERT INTO `".DB_achievements_access."` SET `achievement_id`='".(int)$achievement_id."', `allowed_for_person_type`='".db_fix($person_type)."'");
			
	msgbox('OK', "Įrašas išsaugotas!");
	/*$result = db_query("SELECT * FROM `". DB_achievements ."` WHERE `data`='".db_fix($_POST['data'])."' AND `vaiko_id`=".(int)$_GET['kid_id']." AND `type`=1");//Notes
	if(mysqli_num_rows($result)) {
		msgBox('ERROR', "Ši diena jau užregistruota sistemoje. Keiskite esamą įrašą.");
	} else {
		// Notes
		if(!mysqli_query($db_link, "INSERT INTO `". DB_achievements ."` SET `data`='".db_fix($_POST['data'])."', `value`='".db_fix($_POST['note'])."', `vaiko_id`=".(int)$_GET['kid_id'].", `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."', `type`=".ProgressAndAchievementsFieldType::Note))
			logdie('Neteisinga užklausa: '. mysqli_error($db_link));

		// Achievements
		$i = 1;
		if(isset($_POST['pavadinimas'][$i]))
			for($k=0; $k < count($_POST['pavadinimas'][$i]); $k++)
				//`achievementType`='".(int)$_POST['tipas'][$i][$k]."',
				mysqli_query($db_link, "INSERT INTO `". DB_achievements ."` SET  
				`value`='".db_fix($_POST['pavadinimas'][$i][$k])."', `data`='".db_fix($_POST['data'])."', 
				`vaiko_id`=".(int)$_GET['kid_id'].", `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."', `type`=".ProgressAndAchievementsFieldType::Achievement) or logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
		msgbox('OK', "Išsaugota sėkmingai!");
	}*/
}

if(isset($_POST['update'])) {
	$result = db_query("SELECT * FROM `".DB_achievements."` WHERE `ID`=".(int)$_POST['ID']);
	$row = mysqli_fetch_assoc($result);
	if($row) {
		if($row['createdByUserId'] == 0 || $row['createdByUserId'] != 0 && (ADMIN || USER_ID == $row['createdByUserId'])) {
			db_query("UPDATE `". DB_achievements ."` SET 
				`value`='".db_fix($_POST['note'])."', `achievement`='".db_fix($_POST['achievement'])."', `data`='".db_fix($_POST['data'])."', 
				`updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."', `updated`=CURRENT_TIMESTAMP(), `updatedCounter`=`updatedCounter`+1
				WHERE `ID`=".(int)$_POST['ID']);
	
			$result = db_query("SELECT * FROM `". DB_achievements_access ."` WHERE `achievement_id`='".(int)$_POST['ID']."'");
			$existing_rigths = array();
			while($row = mysqli_fetch_assoc($result))
				$existing_rigths[$row['allowed_for_person_type']] = '';

			if(!empty($_POST['rights'])) {
				db_query("DELETE FROM `".DB_achievements_access."` WHERE `achievement_id`='".(int)$_POST['ID']."' AND `allowed_for_person_type` NOT IN (".implode(',', $_POST['rights']).")");
	
				foreach($_POST['rights'] as $person_type)
					if(!isset($existing_rigths[$person_type]))
						db_query("INSERT INTO `".DB_achievements_access."` SET `achievement_id`='".(int)$_POST['ID']."', `allowed_for_person_type`='".db_fix($person_type)."'");
			} else {
				db_query("DELETE FROM `".DB_achievements_access."` WHERE `achievement_id`='".(int)$_POST['ID']."'");
			}
	
			msgbox('OK', "Įrašas atnaujintas!");
			/*// Notes
			if(!mysqli_query($db_link, "UPDATE `". DB_achievements ."` SET `value`='".db_fix($_POST['note'])."', `data`='".db_fix($_POST['data'])."' WHERE `vaiko_id`=".(int)$_GET['kid_id']." AND `data`='".db_fix($_POST['old_date'])."' AND `type`=1"))
				logdie('Neteisinga užklausa: '. mysqli_error($db_link));
			// Achievements
			$i = 1;
			db_query("DELETE FROM `".DB_achievements."` WHERE `data`='".db_fix($_POST['old_date'])."' AND `vaiko_id`=".(int)$_GET['kid_id']." AND `type`=0");
			if(isset($_POST['pavadinimas'][$i]))
				for($k=0; $k < count($_POST['pavadinimas'][$i]); $k++)
					//`achievementType`='".(int)$_POST['tipas'][$i][$k]."', 
					db_query("INSERT INTO `". DB_achievements ."` SET 
					`value`='".db_fix($_POST['pavadinimas'][$i][$k])."', `data`='".db_fix($_POST['data'])."', 
					`vaiko_id`=".(int)$_GET['kid_id'].", `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."', `type`=0");*/
		} else
			msgbox('ERROR', "Įrašui keisti neturite teisių. Ištrinti gali tik autorius ir darželio vadovas (-ai).");
	} else
		msgbox('ERROR', "Tokio įrašo nėra");
}
if(isset($_GET['delete'])) {
	$result = db_query("SELECT * FROM `".DB_achievements."` WHERE `ID`=".(int)$_GET['delete']);
	$row = mysqli_fetch_assoc($result);
	if($row) {
		if($row['createdByUserId'] == 0 || $row['createdByUserId'] != 0 && (ADMIN || USER_ID == $row['createdByUserId'])) {
			if(!empty($row['attachment']))
	      		unlink(UPLOAD_DIR.'progress_and_achievements/'.(int)$row['vaiko_id']."/".$row['attachment']);
			db_query("DELETE FROM `".DB_achievements."` WHERE `ID`=".(int)$_GET['delete']);
			db_query("DELETE FROM `".DB_achievements_access."` WHERE `achievement_id`=".(int)$_GET['delete']);
			msgbox('OK', "Įrašas ištrintas!");
		} else
			msgbox('ERROR', "Įrašui ištrinti neturite teisių. Ištrinti gali tik autorius ir darželio vadovas (-ai).");
	} else
		msgbox('ERROR', "Tokio įrašo nėra");
}



if(ADMIN) { ?>
	<form method="get" style="padding-bottom:10px;" class="no-print">
		<div class="sel" style="float: left; margin-right: 5px;"><select name="grupes_id">
		<?php 
		echo "<option value=\"0\"".(isset($_GET['grupes_id']) && 0 == $_GET['grupes_id'] ? ' selected="selected" class="selected">'.$selectedMark : '>')."Visos grupės</option>";
		foreach(getAllGroups() as $ID => $title)
			echo "<option value=\"".$ID."\"".(isset($_GET['grupes_id']) && $ID == $_GET['grupes_id'] ? ' selected="selected" class="selected">'.$selectedMark : '>').$title."</option>";
		?>
		</select></div>
		<input type="submit" value="Filtruoti" class="filter">
		<input type="submit" name="latest" value="Rodyti 30 naujausių pasiekimų" class="filter">
	</form>
<?php } else { ?>
<form method="get" style="padding-bottom:10px;" class="no-print">
	<input type="submit" name="latest" value="Rodyti 30 naujausių pasiekimų" class="filter">
</form>
<?php } ?>
<form method="get">
	<script>
	$(function() {
		$('#kid_id').change(function() {
			$('#view-btn').click();
		});
		//W3C recommendation Fx bug https://bugzilla.mozilla.org/show_bug.cgi?id=126379 In very rare cases this is not a bug.
		$('#kid_id').keyup(function() {
			$('#view-btn').click();
		});
	});
	</script>
	<input type="hidden" name="grupes_id" value="<?=(isset($_GET['grupes_id']) ? (int)$_GET['grupes_id'] : 0)?>">
	<div class="sel<?=(isset($_GET['latest']) ? ' no-print' : '')?>" style="float: left; margin-right: 5px;"><select id="kid_id" name="kid_id"<?php if(!(isset($_GET['new']) || isset($_GET['edit']) || isset($_GET['latest']))) echo ' autofocus'; ?>>
		<?php
		$kids = array();
		//DRY: sveikatos_pastabos.php
		if(ADMIN)
			$result = db_query("SELECT cr.*, TIMESTAMPDIFF(YEAR, cr.`gim_data`, CURDATE()) AS age FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<=CURDATE() GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from` WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0".(isset($_GET['grupes_id']) && $_GET['grupes_id'] != 0 ? " AND cr.`grupes_id`=".(int)$_GET['grupes_id'] : '')." ORDER BY ".orderName('cr'));//cr.`vardas` ASC, cr.`pavarde` ASC
			//"SELECT * FROM `".DB_children."` WHERE".(isset($_GET['grupes_id']) && $_GET['grupes_id'] != 0 ? " grupes_id=".(int)$_GET['grupes_id']." AND" : '')." `isDeleted`=0 AND `archyvas`=0 AND `parent_kid_id`=0 ORDER BY vardas, pavarde"
		else
			$result = db_query($get_kids_sql);
			//"SELECT * FROM `".DB_children."` WHERE `grupes_id`=".GROUP_ID." AND `isDeleted`=0 AND `archyvas`=0 AND `parent_kid_id`=0 ORDER BY vardas, pavarde"
			//JOIN ".DB_employees_groups." ON ".DB_children.".grupes_id=".DB_employees_groups.".grup_id WHERE ".DB_employees_groups.".darb_id=".(int)DARB_ID."
		while($row = mysqli_fetch_assoc($result)) {
			$kids[$row['parent_kid_id']] = filterText(getName($row['vardas'], $row['pavarde']));
			echo "<option value=\"".$row['parent_kid_id']."\"".(isset($_GET['kid_id']) && $_GET['kid_id'] == $row['parent_kid_id'] ? ' selected="selected" class="selected">'.$selectedMark : '>').$kids[$row['parent_kid_id']]."</option>";//filterText($row['vardas'])." ".filterText($row['pavarde'])
		}
		?>
	</select></div>
	<input class="no-print filter" type="submit" id="view-btn" value="Peržiūrėti">
	<?php
	if(!ADMIN)
		echo '<input type="submit" name="new" value="Naujas įrašas">';
	?>
</form>
<div class="cl"></div>
<?php



if(isset($_GET['new']) || isset($_GET['edit'])) {
	if(isset($_GET['edit'])) {
		$result = db_query('SELECT * FROM `'.DB_achievements.'` WHERE `ID`='.(int)$_GET['edit']);
		$res = mysqli_fetch_assoc($result);
	}
	//$i = 1;
	?>
	<!-- <script type="text/javascript" src="/workers/children_progress_and_achievements.js?v2"></script> -->
	<script type="text/javascript" src="/libs/jquery.checkboxes.min.js"></script>
	<script type="text/javascript">
	$(function($) {
		$('#report-form').checkboxes('range', true);
	});
	</script>
	<fieldset id="report-form">
	<legend><?=(isset($_GET['edit']) ? "Įrašo redagavimas" : "Naujas įrašas")?></legend>
		<form action="<?=(isset($_GET['kid_id']) ? '?kid_id='.(int)$_GET['kid_id'] : '')?>" method="post" enctype="multipart/form-data" class="not-saved-reminder" onsubmit="return checkUpload()">
			<p><label>Data: <input class="datepicker" required="required" type="text" name="data" value="<?=(isset($_GET['edit']) ? filterText($res['data']) : date('Y-m-d'))?>"></label></p>
			<p><label>Pastabos: <textarea name="note"><?=(isset($_GET['edit']) ? filterText($res['value']) : '')?></textarea></label></p>
			<p><label>Pasiekimai: <textarea name="achievement"><?=(isset($_GET['edit']) ? filterText($res['achievement']) : '')?></textarea>
			<?php /*
			<a href="#" class="addAchievement" data-id="<?=$i?>">+ Pridėti</a>
			<div class="achievements-list"><?php
			if(!isset($_GET['new'])) {
				$paz = db_query('SELECT * FROM `'.DB_achievements.'` WHERE `data`="'.db_fix($_GET['data']).'" AND `vaiko_id`='.(int)$_GET['kid_id'].' AND `type`=0');
					$pav = array();
					//$tip = array();
					while($row = mysqli_fetch_assoc($paz)) {
						<select name="tipas['.$i.'][]">
											<option class="green" value="0"'.($row['achievementType'] == 0 ? ' selected="selected"' : '').'>Išmoko</option>
											<option class="yellow" value="1"'.($row['achievementType'] == 1 ? ' selected="selected"' : '').'>Įpusėjo</option>
											<option class="red" value="2"'.($row['achievementType'] == 2 ? ' selected="selected"' : '').'>Neišmoko</option>
										</select>
						echo '<p><textarea name="pavadinimas['.$i.'][]">'.filterText($row['value']).'</textarea> <a href="#" class="remScnt">- Trinti</a></p>';
						$pav[] = filterText($row['value']);
						//$tip[] = $row['achievementType'];
						//$child_achievements[$row['tipas']]
					}
			}
			?>
			</div>
			*/ ?>
			</label></p>
			<?php if(isset($_GET['new'])) { ?>
				<p><label>Dokumentas Jūsų kompiuteryje: <input type="file" style="width: 300px;" name="file" id="file"></label></p>
			<?php } elseif(!empty($res['attachment'])) { ?>
				<p><a href="<?php echo UPLOAD_DIR.'progress_and_achievements/'.$res['vaiko_id']."/".$res['attachment']; ?>">Failas <?=$res['attachment']?></a></p>
			<?php } //Else - file was not uploaded ?>
			<div>
			Leidimai matyti šio vaiko grupės darbuotojams <span class="notice">(pereinamuoju laikotarpiu neatsižvelgiama t. y. gali peržiūrėti kitas darbuotojas, nors ir nepažymėta)</span>:<br>
			<?php
			$existing_rigths = array();
			if(isset($_GET['edit'])) {
				$result = db_query("SELECT * FROM `". DB_achievements_access ."` WHERE `achievement_id`='".(int)$_GET['edit']."'");
				while($row = mysqli_fetch_assoc($result))
					$existing_rigths[$row['allowed_for_person_type']] = '';
			}
			foreach($person_type_who_dgs as $id => $value)
				if($id != 0 /*tėvai*/ && $id != 3 /*vadovai*/ && $id != 2 /*buhalt*/ /*Kaip dėl dietistų?*/)
					echo '<div><label><input type="checkbox" name="rights[]" value="'.$id.'"'.(isset($existing_rigths[$id]) ? ' checked="checked"' : '').'> '.$value.'</label></div>';//| matys infomaciją tik (priskirti tai grupei darbuotojai)
			?>
			</div>
			<div>
			Leidimas matyti šio vaiko tėvams informaciją:<br>
			<?php
			echo '<div><label><input type="checkbox" name="rights[]" value="0"'.(isset($existing_rigths[0]) ? ' checked="checked"' : '').'> Tėvai</label></div>';
			?>
			</div>
			<?php
			if(KESV && isset($_GET['edit']) && !isset($_GET['print'])) {
				echo 'Sukurta: &nbsp;'.$res['created'].' '.getAllEmployees($res['createdByEmployeeId']).';<br>Pakeista: '.$res['updated'].' '.getAllEmployees($res['updatedByEmployeeId']);
			}
			?>
			<p><?=(isset($_GET['edit']) ? '<input type="hidden" name="ID" value="'.(int)$res['ID'].'">' : '')?><input type="submit" name="<?=(isset($_GET['edit']) ? 'update' : 'save')?>" value="<?=(isset($_GET['edit']) ? 'Atnaujinti' : 'Išsaugoti naują įrašą')?>" class="submit"></p>
			 <script>
			function checkUpload() {
				var file = document.getElementById('file').files[0];
				//Jeigu gailas yra ir mažesnis arba jeigu filo nėra praleidžiam
				if(file.size < 2097152 || typeof file === 'undefined') { // 2 MB (this size is in bytes)
					//Submit form
				} else {
					//console.log(file);
					alert('Failas didesnis negu 2 MB, todėl jo įkelti negalima. Jei tai paveiksliukas išsaugokite jį jpg formatu arba sumažinkite jo raišką, kad jo užimamas dydis sumažėtų. Tada bandykite dar kartą jau su mažesniu failu.');
					//Prevent default and display error
					//evt.preventDefault();
					return false;
				}
			}
			</script>
			
			
		</form>
	</fieldset>
	<?php
}


if(isset($_GET['kid_id'])) {

	//TODO: make check if worker is allowed to see it; darb_id=".(int)DARB_ID
	$result = db_query("SELECT * FROM `".DB_achievements."` WHERE `vaiko_id`=".(int)$_GET['kid_id']." ORDER BY `data` DESC");
	//TODO: check access with INNER JOIN
	if(mysqli_num_rows($result) > 0) {
		?>
		<table>
		<tr>
			<th class="date-cell">Data</th>
			<th>Pastabos</th>
			<th>Pasiekimai</th>
			<th>Prisegtukas</th>
			<th>Sukūrė (ir pildo)</th>
			<th class="no-print">Tik matys (-o)</th>
			<th class="no-print">Veiksmas</th>
		</tr>
		<?php
		while ($row = mysqli_fetch_assoc($result)) {
			//$result3 = db_query("SELECT * FROM `".DB_achievements."` WHERE `vaiko_id`=".(int)$row['kid_id']." AND `type`=1 AND `data`='".$row['data']."'");
			//$row3['value'] = '';
			//if(mysqli_num_rows($result3) > 0)
			//	$row3 = mysqli_fetch_assoc($result3);
			echo "\t<tr".(isset($_GET['edit']) && $_GET['edit'] == $row['ID'] ? ' class="opened-row"' : '').">
			<td>".$row['data']."</td>
			<td>".nl2br(filterText($row['value']))."</td>
			<td>".nl2br(filterText($row['achievement']))."</td>
			<td>".(!empty($row['attachment']) ? '<a href="'.UPLOAD_DIR.'progress_and_achievements/'.$row['vaiko_id']."/".$row['attachment'].'">'.$row['attachment'].'</a>' : '')."</td>
			<td>".getAllEmployees($row['createdByEmployeeId'])."</td>
			<td class=\"no-print\">";
			$res = db_query("SELECT * FROM `".DB_achievements_access."` WHERE `achievement_id`=".$row['ID']);
			$access = [];
			while ($r = mysqli_fetch_assoc($res))
				if($r['allowed_for_person_type'] == 0)
					$access[] = '<strong>'.mb_lcfirst($person_type_who_dgs[$r['allowed_for_person_type']]).'</strong>';
				else
					$access[] = mb_lcfirst($person_type_who_dgs[$r['allowed_for_person_type']]);
			echo 'Vadovai';
			if(count($access))
				echo ', '.implode(', ', $access);
			echo "</td>
			<td class=\"no-print\">";
			/*echo "<td>";
			$result2 = db_query("SELECT * FROM `".DB_achievements."` WHERE `vaiko_id`=".(int)$row['kid_id']." AND `type`=0 AND `data`='".$row['data']."'");
			while ($row2 = mysqli_fetch_assoc($result2))
				echo filterText($row2['value']).";<br>";//$child_achievements[$row2['achievementType']]. ": ".
			echo "</td>";*/
			if($row['createdByUserId'] == 0 || $row['createdByUserId'] != 0 && (ADMIN || USER_ID == $row['createdByUserId']))
				echo "<a href='?kid_id=".(int)$row['vaiko_id']."&amp;edit=".(int)$row['ID']."'>Keisti</a> <a href='?kid_id=".(int)$row['vaiko_id']."&amp;delete=".(int)$row['ID']."' onclick=\"return confirm('Ar tikrai norite ištrinti?')\">Trinti</a>";
			echo "</td></tr>";
		}
		echo "</table>";
	} else
		echo "Nėra pastabų ir/arba pasiekimų.";
}


if(isset($_GET['latest'])) {
	$kids_list = '0';
	foreach($kids as $kid_id => $t)
		$kids_list .= ','.$kid_id;
	//$kids_list = substr($kids_list, 1);
	//TODO: make check if worker is allowed to see it; darb_id=".(int)DARB_ID
	$result = db_query("SELECT * FROM `".DB_achievements."` WHERE `vaiko_id` IN ($kids_list) ORDER BY `data` DESC LIMIT 30");
	//TODO: check access with INNER JOIN
	if(mysqli_num_rows($result) > 0) {
		?>
		<table>
		<tr>
			<th class="date-cell">Data</th>
			<th>Vaikas</th>
			<th>Pastabos</th>
			<th>Pasiekimai</th>
			<th>Prisegtukas</th>
			<th>Sukūrė (ir pildo)</th>
			<th class="no-print">Tik matys (-o)</th>
			<th class="no-print">Veiksmas</th>
		</tr>
		<?php
		while ($row = mysqli_fetch_assoc($result)) {
			echo "\t<tr>
			<td>".$row['data']."</td>
			<td>".(isset($kids[$row['vaiko_id']]) ? $kids[$row['vaiko_id']] : '?')."</td>
			<td>".filterText($row['value'])."</td>
			<td>".filterText($row['achievement'])."</td>
			<td>".(!empty($row['attachment']) ? '<a href="'.UPLOAD_DIR.'progress_and_achievements/'.$row['vaiko_id']."/".$row['attachment'].'">'.$row['attachment'].'</a>' : '')."</td>
			<td>".getAllEmployees($row['createdByEmployeeId'])."</td>
			<td class=\"no-print\">";
			$res = db_query("SELECT * FROM `".DB_achievements_access."` WHERE `achievement_id`=".$row['ID']);
			$access = [];
			while ($r = mysqli_fetch_assoc($res))
				if($r['allowed_for_person_type'] == 0)
					$access[] = '<strong>'.mb_lcfirst($person_type_who_dgs[$r['allowed_for_person_type']]).'</strong>';
				else
					$access[] = mb_lcfirst($person_type_who_dgs[$r['allowed_for_person_type']]);
			echo 'Vadovai';
			if(count($access))
				echo ', '.implode(', ', $access);
			echo "</td>
			<td class=\"no-print\">";
			if($row['createdByUserId'] == 0 || $row['createdByUserId'] != 0 && (ADMIN || USER_ID == $row['createdByUserId']))
				echo "<a href='?kid_id=".(int)$row['vaiko_id']."&amp;edit=".(int)$row['ID']."'>Keisti</a> <a href='?kid_id=".(int)$row['vaiko_id']."&amp;delete=".(int)$row['ID']."' onclick=\"return confirm('Ar tikrai norite ištrinti?')\">Trinti</a>";
			echo "</td></tr>";
		}
		echo "</table>";
	} else
		echo "Nėra pastabų ir/arba pasiekimų.";
}
?>
</div>
