<?php if(!defined('ADMIN') || !ADMIN) exit(); ?>
<h1>Formų kūrimas vadovams</h1>
<div id="content">
<?php
if(isset($_POST['save'])) {
	db_query("INSERT INTO `".DB_docs_templates."` SET
		`kindergarten_id`=".DB_ID.",
		`title`='".db_fix($_POST['title'])."',
		`content`='".db_fix($_POST['content'])."',
		`isPublished`=".(isset($_POST['isPublished']) ? 1 : 0).",
        `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
	msgBox('OK', 'Forma išsaugota.');
}

if(isset($_POST['update'])) {
	db_query("UPDATE `".DB_docs_templates."` SET `title`='".db_fix($_POST['title'])."',
		`content`='".db_fix($_POST['content'])."',
		`isPublished`=".(isset($_POST['isPublished']) ? 1 : 0).",
	    `updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."',
		`updated`=CURRENT_TIMESTAMP()
		WHERE `kindergarten_id`=".DB_ID." AND `form_id`=".(int)$_POST['update']);
	msgBox('OK', 'Forma atnaujinta.');
}

if(isset($_GET['delete'])) {
	db_query("UPDATE `".DB_docs_templates."` SET `isDeleted`=1 WHERE `kindergarten_id`=".DB_ID." AND `form_id`=".(int)$_GET['delete']);
	msgBox('OK', 'Forma ištrinta!');
}

if(isset($_GET['publish_form_id'])) {
	db_query("UPDATE `".DB_docs_templates."` SET
			`isPublished`=".(isset($_GET['publish']) ? 1 : 0).",
		    `updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."',
			`updated`=CURRENT_TIMESTAMP()
			WHERE `kindergarten_id`=".DB_ID." AND `form_id`=".(int)$_GET['publish_form_id']);
	msgBox('OK', 'Forma sėkmingai '.(isset($_GET['publish']) ? 'paskelbta/įjungta pildymui' : 'išjungta').'.');
}
?>
<a href="?new#forms" class="no-print fast-action fast-action-add">Nauja forma</a>
<?php
$result = db_query("SELECT * FROM `".DB_docs_templates."` WHERE `kindergarten_id`=".DB_ID." AND isDeleted=0 ORDER BY `created` DESC");
if(mysqli_num_rows($result) > 0) {
	?>
<table id="forms-tbl"<?=(isset($_GET['preview']) || isset($_GET['previewTbl']) ? ' class="no-print"' : '')?>>
<tr>
	<th>Formos pavadinimas</th>
	<th>Leisti pildyti darbuotojams</th>
	<th>Sukurta <span title="Nuo naujausiai sukurto iki vėliausiai">↓</span></th>
	<th>Pakeista</th>
	<th>Veiksmai</th>
</tr>
	<?php
	$opened_form_id = 0;//opened-row
	if(isset($_GET['edit']))
		$opened_form_id = (int)$_GET['edit'];
	if(isset($_POST['save']))
		$opened_form_id = (int)$_POST['save'];
	if(isset($_POST['update']))
		$opened_form_id = (int)$_POST['update'];
	if(isset($_GET['preview']))
		$opened_form_id = (int)$_GET['preview'];
	while ($row = mysqli_fetch_assoc($result)) {
		?><tr<?=($opened_form_id == $row['form_id'] ? ' class="opened-row"' : '')?>>
		<td><?=filterText($row['title'])?></td>
		<td><?=($row['isPublished'] ? 'Leidžiama' : 'Neleidžiama')?> <a href="?publish_form_id=<?=$row['form_id']?><?=($row['isPublished'] ? '' : '&amp;publish')?>"><?=($row['isPublished'] ? 'Nebeleisti' : 'Leisti')?></a></td>
		<td><?=$row['created']?></td>
		<td><?=$row['updated']?></td>
		<td><a href="?preview=<?=filterText($row['form_id'])?>#preview">Peržiūra</a> <a href="?edit=<?=filterText($row['form_id'])?>#forms">Keisti</a> <a href="?delete=<?=filterText($row['form_id'])?>" onclick="return confirm('Ar tikrai norite ištrinti?')">Trinti</a></td>
		</tr>
		<?php
	}
}
?>
</table>

<?php
if( isset($_GET['new']) || isset($_GET['edit']) ) {
	if(isset($_GET['edit'])) {
		$result = db_query("SELECT * FROM `".DB_docs_templates."` WHERE `kindergarten_id`=".DB_ID." AND `form_id`=".(int)$_GET['edit']);
		$form = mysqli_fetch_assoc($result);
	}
	?>
	<fieldset id="forms">
	<legend><?=(isset($_GET['edit']) ? "Tvarkyti formą " : "Nauja forma")?></legend>
	<form action="#" method="post" class="not-saved-reminder" style="width: 950px;">
		<p><label>Formos pavadinimas<span class="required">*</span>: <textarea required="required" name="title"><?=(isset($_GET['edit']) ? filterText($form['title']) : '')?></textarea></label></p>
		<p><label>Leisti pildyti darbuotojams <input type="checkbox" name="isPublished" value="1"<?=(isset($_GET['edit']) && $form['isPublished'] ? ' checked="checked"' : '')?>></label></p>
		<p><label>
		<div class="title" style="display: inline-block;">Išankstinis turinys</div>: <span class="notice">Išplėsti/sumažinti redagavimo vaizdą galite naudodami žemiau esantį</span>  <div style="background-image: url(http://dev.musudarzelis.lt/libs/ckeditorCustom/plugins/icons.png?t=F7J9); background-position: 0 -672px; width: 16px; height: 16px; display: inline-block; vertical-align: center;"></div>
		 <textarea class="wymeditor ckeditor" name="content" style="width: 950px;/*845*/"><?=(isset($_GET['edit']) ? filterText($form['content']) : '')?></textarea></p>
		<p><input type="hidden" <?=(!isset($_GET['edit']) ? 'name="save"' : 'name="update" value="'.(int)$form['form_id']).'"'?>"><input type="submit" value="Išsaugoti" class="submit"></p>
	</form>
	</fieldset>
<?php
}
if( isset($_GET['preview']) ) {
	if(isset($_GET['preview'])) {
		$result = db_query("SELECT * FROM `".DB_docs_templates."` WHERE `kindergarten_id`=".DB_ID." AND `form_id`=".(int)$_GET['preview']);
		$form = mysqli_fetch_assoc($result);
	}
	?><div id="planning-view">
		<div class="no-print">Pildyta pagal formą: <?=$form['title']?></div>
		<p><span class="title">Grupė:</span> XXXX</p>
		<p><span class="title">Pavadinimas:</span> XXXXX</p>
		<p><span class="title">Data:</span> XXXX-XX-XX</p>
		<div><span class="title"></span> <?=/*nl2br*/(strip_tags($form['content'], '<br><p><div><strong><em><table><tr><th><td><span><s><ol><ul><li><h1><h2><h3><del><pre><cite><img><hr><blockquote><tt><code><kbd><samp><var><ins><q><tbody><thead><tfoot>'))?></div>
		<div class="no-print">
				<div style="clear: left; float: left; width: 70px;">Sukūrė:</div><div style="float: left;">XXX</div>
				<div style="clear: left; float: left; width: 70px;">Atnaujino:</div><div style="float: left;">XXX</div>
				<div style="clear: left;"></div>
				<?php if(KESV) { ?>
				Keitė kartų: XXX
				<?php } ?>
		</div>
	</div><?php
}
?>
</div>
