<?php if(!defined('DARBUOT') || !DARBUOT) exit();
$tmp_s = microtime(true);
$show_health_observations = false;
$result = db_query("SHOW TABLES LIKE '".DB_health_observations."'");
if(mysqli_num_rows($result) > 0) {
	$result = db_query("SELECT * FROM ".DB_health_observations." LIMIT 1");
	if(mysqli_num_rows($result) > 0)
		$show_health_observations = true;
}
echo '<!-- '.(microtime(true)-$tmp_s).' -->';//0.03//0.r
?>
<ul>
	<li<?php echo ($_GET['q'] == '' ? ' class="active"' : '')?> style="margin-left: 0px; /*background-image: url(/img/menu/home.png);  background-position: 4px 2px; padding-left: 30px;*/"><a href="/">Pradžia</a></li>
	<!-- <li<?php echo ($_GET['q'] == '' ? ' class="active"' : '')?> style="background-image: url(/img/menu-home.png);"><a href="/">Pradžia</a></li> -->
	<?php if(!(ADMIN || /*LOGOPEDAS ||*/ DIETISTAS)) { ?>
	<li class="separator" style="margin-left: 0px; background-image: url(/img/menu/groups.png);  background-position: 4px 5px; padding-left: 30px;">Grupės</li>
	
	<!-- <li style="background-image: url(/img/menu-groups.png); height: auto; background-position: left 2px;">
		<div style="background-image: url(/img/menu-opened1.png);  background-position: 165px; 9px; background-repeat: no-repeat;">Grupės</div>
		<ul> -->
		<?php
		if(!(ADMIN)) {//|| BUHALT || SESELE
			foreach(getAllowedGroups()/*$_SESSION['GROUPS']*/ as $id => $title) {
				if($_SESSION['GROUP_ID'] != $id)
					echo '<li style="background-image: url(/img/menu-current.png);  background-position: 10px 7px;"><a href="?change_group_id='.$id.'">'.$title.'</a></li>';
				else {
					?><li style="background-image: url(/img/menu-opened.png);  background-position: 10px 9px;"><?=$title?>
				
					<ul>
					<?php if(MENO_PEDAGOGAS) { /* Meninio ugdymo pedagogė*/ ?>
						<li<?php echo ($_GET['q'] == 'vaiko_pasiekimai_pagal_nauja_aprasa' ? ' class="active"' : '')?>><a href="/vaiko_pasiekimai_pagal_nauja_aprasa" class="abbr" title="Vaiko žingsninė pažanga ir pasiekimai">Pasiekimų žingsneliai</a></li><!-- Naujieji -->
						<li<?php echo ($_GET['q'] == 'vaiko_pazanga_ir_pasiekimai' ? ' class="active"' : '')?>><a href="/vaiko_pazanga_ir_pasiekimai" class="abbr" title="Vaiko pažanga ir pasiekimai">Vaiko pasiekimai</a></li>
						<li<?php echo ($_GET['q'] == 'forms_fill_out' ? ' class="active"' : '')?>><a href="/forms_fill_out">Tekstas <span style="font-size: 13px;" class="abbr" title="Pavyzdys">(priešmok.)</span></a></li><?php //Rašyti formose, Šablonus, Rašyti pagal formas //Vėliau Atestavimui ?>
						<?php if($showOldPlanning) {//!KaunoVarpelis) { ?>
						<li<?php echo ($_GET['q'] == 'planavimas' ? ' class="active"' : '')?>><a href="/planavimas">Planavimas</a></li>
						<?php } ?>
						<li<?php echo ($_GET['q'] == 'planavimo_formos_pildymas' ? ' class="active"' : '')?>><a href="/planavimo_formos_pildymas">Planavimas formose</a></li>
						<li<?php echo ($_GET['q'] == 'grupiu_bureliai' ? ' class="active"' : '')?> title="Vaikų ugdymo užsiėmimų tvarkaraštis"><a href="/grupiu_bureliai" class="abbr">Užsiėmimų tvark.<!-- Pap. grupių veikla --></a></li><!-- title="Papildomos grupių (neformaliojo ugdymo) veiklos tvarkaraštis" -->
						<li<?php echo ($_GET['q'] == 'activities' ? ' class="active"' : '')?>><a href="/activities">Numatomos veiklos</a></li>
						<li<?php echo (startsWith($_GET['q'], 'tabeliai') ? ' class="active"' : '')?>><a href="/tabeliai">Tabeliai</a></li>
					<?php } elseif(KUNO_KULTUROS_PEDAGOGAS) { /* Kūno kultūros pedagogė */ ?>
						<li<?php echo ($_GET['q'] == 'forms_fill_out' ? ' class="active"' : '')?>><a href="/forms_fill_out">Tekstas <span style="font-size: 13px;" class="abbr" title="Pavyzdys">(priešmok.)</span></a></li><?php //Rašyti formose, Šablonus, Rašyti pagal formas //Vėliau Atestavimui ?>
						<?php if($showOldPlanning) {//!KaunoVarpelis) { ?>
						<li<?php echo ($_GET['q'] == 'planavimas' ? ' class="active"' : '')?>><a href="/planavimas">Planavimas</a></li>
						<?php } ?>
						<li<?php echo ($_GET['q'] == 'planavimo_formos_pildymas' ? ' class="active"' : '')?>><a href="/planavimo_formos_pildymas">Planavimas formose</a></li>
						<li<?php echo ($_GET['q'] == 'grupiu_bureliai' ? ' class="active"' : '')?> title="Vaikų ugdymo užsiėmimų tvarkaraštis"><a href="/grupiu_bureliai" class="abbr">Užsiėmimų tvark.<!-- Pap. grupių veikla --></a></li><!-- title="Papildomos grupių (neformaliojo ugdymo) veiklos tvarkaraštis" -->
						<li<?php echo ($_GET['q'] == 'activities' ? ' class="active"' : '')?>><a href="/activities">Numatomos veiklos</a></li>
						<?php if(RokiskioVarpelisLD && $show_health_observations) { ?>
						<li<?php echo ($_GET['q'] == 'sveikatos_pastabos' ? ' class="active"' : '')?>><a href="/sveikatos_pastabos">Sveikatos pastabos</a></li>
						<?php } ?>
					<?php } elseif(KINEZITERAPEUTAS) { /* KINEZITERAPEUTAS */ ?>
						<li<?php echo ($_GET['q'] == 'forms_fill_out' ? ' class="active"' : '')?>><a href="/forms_fill_out">Tekstas <span style="font-size: 13px;" class="abbr" title="Pavyzdys">(priešmok.)</span></a></li><?php //Rašyti formose, Šablonus, Rašyti pagal formas //Vėliau Atestavimui ?>
						<li<?php echo ($_GET['q'] == 'planavimo_formos_pildymas' ? ' class="active"' : '')?>><a href="/planavimo_formos_pildymas">Planavimas formose</a></li>
						<li<?php echo ($_GET['q'] == 'vaiko_pazanga_ir_pasiekimai' ? ' class="active"' : '')?>><a href="/vaiko_pazanga_ir_pasiekimai" class="abbr" title="Vaiko pažanga ir pasiekimai">Vaiko pasiekimai</a></li>
						<?php if($show_health_observations) { ?>
						<li<?php echo ($_GET['q'] == 'sveikatos_pastabos' ? ' class="active"' : '')?>><a href="/sveikatos_pastabos">Sveikatos pastabos</a></li>
						<?php } ?>
						<li<?php echo ($_GET['q'] == 'grupiu_bureliai' ? ' class="active"' : '')?> title="Vaikų ugdymo užsiėmimų tvarkaraštis"><a href="/grupiu_bureliai" class="abbr">Užsiėmimų tvark.<!-- Pap. grupių veikla --></a></li><!-- title="Papildomos grupių (neformaliojo ugdymo) veiklos tvarkaraštis" -->
						<li<?php echo ($_GET['q'] == 'activities' ? ' class="active"' : '')?>><a href="/activities">Numatomos veiklos</a></li>
					<?php } elseif(MUZIKOS_PEDAGOGAS) { /* Muzikos pedagogė*/ ?>
						<li<?php echo ($_GET['q'] == 'vaiko_pasiekimai_pagal_nauja_aprasa' ? ' class="active"' : '')?>><a href="/vaiko_pasiekimai_pagal_nauja_aprasa" class="abbr" title="Vaiko žingsninė pažanga ir pasiekimai">Pasiekimų žingsneliai</a></li><!-- Naujieji -->
						<li<?php echo ($_GET['q'] == 'forms_fill_out' ? ' class="active"' : '')?>><a href="/forms_fill_out">Tekstas <span style="font-size: 13px;" class="abbr" title="Pavyzdys">(priešmok.)</span></a></li><?php //Rašyti formose, Šablonus, Rašyti pagal formas //Vėliau Atestavimui ?>
						<?php if($showOldPlanning) {//!KaunoVarpelis) { ?>
						<li<?php echo ($_GET['q'] == 'planavimas' ? ' class="active"' : '')?>><a href="/planavimas">Planavimas</a></li>
						<?php } ?>
						<li<?php echo ($_GET['q'] == 'planavimo_formos_pildymas' ? ' class="active"' : '')?>><a href="/planavimo_formos_pildymas">Planavimas formose</a></li>
						<li<?php echo ($_GET['q'] == 'grupiu_bureliai' ? ' class="active"' : '')?> title="Vaikų ugdymo užsiėmimų tvarkaraštis"><a href="/grupiu_bureliai" class="abbr">Užsiėmimų tvark.<!-- Pap. grupių veikla --></a></li><!-- title="Papildomos grupių (neformaliojo ugdymo) veiklos tvarkaraštis" -->
						<li<?php echo ($_GET['q'] == 'activities' ? ' class="active"' : '')?>><a href="/activities">Numatomos veiklos</a></li>
					<?php } elseif(BUHALT) { /* Buhalterė */ ?>
						<li<?php echo (startsWith($_GET['q'], 'tabeliai') ? ' class="active"' : '')?>><a href="/tabeliai">Tabeliai</a></li>
						<li<?php echo ($_GET['q'] == 'dokumentai' ? ' class="active"' : '')?>><a href="/dokumentai">Vaiko dokumentai</a></li>
						<li<?php echo ($_GET['q'] == 'priminimai' ? ' class="active"' : '')?>><a href="/priminimai">Priminimai</a></li>
						<li<?php echo ($_GET['q'] == 'vaikai' ? ' class="active"' : '')?>><a href="/vaikai">Vaikai</a></li>
					<?php } elseif(SESELE) { /* Medicinos seselė */ ?>
						<li<?php echo (startsWith($_GET['q'], 'tabeliai') ? ' class="active"' : '')?>><a href="/tabeliai">Tabeliai</a></li>
						<li<?php echo ($_GET['q'] == 'forms_fill_out' ? ' class="active"' : '')?>><a href="/forms_fill_out">Tekstas <span style="font-size: 13px;" class="abbr" title="Pavyzdys">(priešmok.)</span></a></li><?php //Rašyti formose, Šablonus, Rašyti pagal formas //Vėliau Atestavimui ?>
						<li<?php echo ($_GET['q'] == 'planavimo_formos_pildymas' ? ' class="active"' : '')?>><a href="/planavimo_formos_pildymas">Planavimas formose</a></li>
						<?php if($show_health_observations) { ?>
						<li<?php echo ($_GET['q'] == 'sveikatos_pastabos' ? ' class="active"' : '')?>><a href="/sveikatos_pastabos">Sveikatos pastabos</a></li>
						<?php } ?>
						<li<?php echo ($_GET['q'] == 'grupiu_bureliai' ? ' class="active"' : '')?> title="Vaikų ugdymo užsiėmimų tvarkaraštis"><a href="/grupiu_bureliai" class="abbr">Užsiėmimų tvark.<!-- Pap. grupių veikla --></a></li><!-- title="Papildomos grupių (neformaliojo ugdymo) veiklos tvarkaraštis" -->
						<li<?php echo ($_GET['q'] == 'vaikai' ? ' class="active"' : '')?>><a href="/vaikai">Vaikai</a></li>
					<?php } elseif(LOGOPEDAS) { /* Logopedas */ ?>
						<li<?php echo ($_GET['q'] == 'vaikai' ? ' class="active"' : '')?>><a href="/vaikai">Vaikai</a></li>
						<li<?php echo ($_GET['q'] == 'veikla' ? ' class="active"' : '')?>><a href="/veikla">Vaiko veikla</a></li>
						<li<?php echo ($_GET['q'] == 'vaiko_pasiekimai_pagal_nauja_aprasa' ? ' class="active"' : '')?>><a href="/vaiko_pasiekimai_pagal_nauja_aprasa" class="abbr" title="Vaiko žingsninė pažanga ir pasiekimai">Pasiekimų žingsneliai</a></li>
						<li<?php echo ($_GET['q'] == 'vaiko_pazanga_ir_pasiekimai' ? ' class="active"' : '')?>><a href="/vaiko_pazanga_ir_pasiekimai" class="abbr" title="Vaiko pažanga ir pasiekimai">Vaiko pasiekimai</a></li>
						<li<?php echo ($_GET['q'] == 'forms_fill_out' ? ' class="active"' : '')?>><a href="/forms_fill_out">Tekstas <span style="font-size: 13px;" class="abbr" title="Pavyzdys">(priešmok.)</span></a></li><?php //Rašyti formose, Šablonus, Rašyti pagal formas //Vėliau Atestavimui ?>
						<li<?php echo ($_GET['q'] == 'planavimo_formos_pildymas' ? ' class="active"' : '')?>><a href="/planavimo_formos_pildymas">Planavimas formose</a></li>
						<li<?php echo (startsWith($_GET['q'], 'tabeliai') ? ' class="active"' : '')?>><a href="/tabeliai">Tabeliai</a></li>
						<!-- <li<?php echo ($_GET['q'] == 'vaikai' ? ' class="active"' : '')?>><a href="/vaikai">Vaikai</a></li> -->
					<?php } elseif(SOCIALINIS_PEDAGOGAS) { ?>
						<li<?php echo ($_GET['q'] == 'veikla' ? ' class="active"' : '')?>><a href="/veikla">Vaiko veikla</a></li>
						<li<?php echo ($_GET['q'] == 'vaiko_pasiekimai_pagal_nauja_aprasa' ? ' class="active"' : '')?>><a href="/vaiko_pasiekimai_pagal_nauja_aprasa" class="abbr" title="Vaiko žingsninė pažanga ir pasiekimai">Pasiekimų žingsneliai</a></li><!-- Naujieji -->
						<li<?php echo (startsWith($_GET['q'], 'tabeliai') ? ' class="active"' : '')?>><a href="/tabeliai">Tabeliai</a></li>
						<li<?php echo ($_GET['q'] == 'vaiko_pazanga_ir_pasiekimai' ? ' class="active"' : '')?>><a href="/vaiko_pazanga_ir_pasiekimai" class="abbr" title="Vaiko pažanga ir pasiekimai">Vaiko pasiekimai</a></li>
						<li<?php echo ($_GET['q'] == 'planavimo_formos_pildymas' ? ' class="active"' : '')?>><a href="/planavimo_formos_pildymas">Planavimas <span style="font-size: 13px;">formose</span></a></li>
						<li<?php echo ($_GET['q'] == 'grupiu_bureliai' ? ' class="active"' : '')?> title="Vaikų ugdymo užsiėmimų tvarkaraštis"><a href="/grupiu_bureliai" class="abbr">Užsiėmimų tvark.<!-- Pap. grupių veikla --></a></li><!-- title="Papildomos grupių (neformaliojo ugdymo) veiklos tvarkaraštis" -->
						<?php if($show_health_observations) { ?>
						<li<?php echo ($_GET['q'] == 'sveikatos_pastabos' ? ' class="active"' : '')?>><a href="/sveikatos_pastabos">Sveikatos pastabos</a></li>
						<?php } ?>
					<?php } else { /* Auklėtoja */ ?>
						<li<?php echo ($_GET['q'] == 'vaikai' ? ' class="active"' : '')?>><a href="/vaikai">Vaikai</a></li>
						<li<?php echo ($_GET['q'] == 'lankomumas' ? ' class="active"' : '')?>><a href="/lankomumas" id="lank1">Lankomumas</a></li>
						<li<?php echo ($_GET['q'] == 'pateisinimai' ? ' class="active"' : '')?>><a href="/pateisinimai" id="lank2">Pateisinimai</a></li>
						<li<?php echo ($_GET['q'] == 'tabeliai' ? ' class="active"' : (startsWith($_GET['q'], 'tabeliai') ? ' class="active-parent"' : ''))?>><a href="/tabeliai">Tabeliai</a></li>
						<?php //echo (startsWith($_GET['q'], 'tabeliai') ? '<li'.(startsWith($_GET['q'], 'tabeliai/rakinimas') ? ' class="active"' : '').'><a href="/tabeliai/rakinimas">&nbsp; + Užbaigimas</a></li>' : '')?>
						<li<?php echo ($_GET['q'] == 'veikla' ? ' class="active"' : '')?>><a href="/veikla">Vaiko veikla</a></li>
						<?php if(DB_PREFIX == 'kedAvil_') { ?>
						<li<?php echo ($_GET['q'] == 'speech_achievements' ? ' class="active"' : '')?>><a href="/speech_achievements">Logo. pasiekimai</a></li>
						<?php } ?>
						<li<?php echo ($_GET['q'] == 'vaiko_pazanga_ir_pasiekimai' ? ' class="active"' : '')?>><a href="/vaiko_pazanga_ir_pasiekimai" class="abbr" title="Vaiko pažanga ir pasiekimai">Vaiko pasiekimai</a></li>
						<?php //if(TESTINIS) { ?>
						<li<?php echo ($_GET['q'] == 'vaiko_pasiekimai_pagal_nauja_aprasa' ? ' class="active"' : '')?>><a href="/vaiko_pasiekimai_pagal_nauja_aprasa" class="abbr" title="Vaiko žingsninė pažanga ir pasiekimai">Pasiekimų žingsneliai</a></li><!-- Naujieji -->
						<?php //} ?>
						<li<?php echo ($_GET['q'] == 'forms_fill_out' ? ' class="active"' : '')?>><a href="/forms_fill_out">Tekstas <span style="font-size: 13px;" class="abbr" title="Pavyzdys">(priešmok.)</span></a></li><?php //Rašyti formose, Šablonus, Rašyti pagal formas //Vėliau Atestavimui ?>
						
						<?php if($showOldPlanning) {//!(KaunoVaikystesLobiai || KaunoVarpelis)) { ?>
						<li<?php echo ($_GET['q'] == 'planavimas' ? ' class="active"' : '')?>><a href="/planavimas">Planavimas</a></li>
						<?php } ?>
						<li<?php echo ($_GET['q'] == 'planavimo_formos_pildymas' ? ' class="active"' : '')?>><a href="/planavimo_formos_pildymas">Planavimas <span style="font-size: 13px;">formose</span></a></li>
						

						<li<?php echo ($_GET['q'] == 'daily_rhythm' ? ' class="active"' : '')?>><a href="/daily_rhythm">Dienos ritmas</a></li>
						<li<?php echo ($_GET['q'] == 'bureliai' ? ' class="active"' : '')?>><a href="/bureliai">Užsiėmimai<!--  class="abbr" Papildoma veikla --></a></li><!-- title="Papildomos grupių veiklos sąrašas" -->
						<li<?php echo ($_GET['q'] == 'grupiu_bureliai' ? ' class="active"' : '')?> title="Vaikų ugdymo užsiėmimų tvarkaraštis"><a href="/grupiu_bureliai" class="abbr">Užsiėmimų tvark.<!-- Pap. grupių veikla --></a></li><!-- title="Papildomos grupių (neformaliojo ugdymo) veiklos tvarkaraštis" -->
						<li<?php echo ($_GET['q'] == 'activities' ? ' class="active"' : '')?>><a href="/activities">Numatomos veiklos</a></li>
						
						
						<?php if(TESTINIS && KESV) {//!(KaunoVarpelis || KaunoZiburelis || KaunoRudnosiukas || KaunoRBaibokyne)) { ?>
						<li<?php echo ($_GET['q'] == 'laiskai' ? ' class="active"' : '')?>><a href="/laiskai">Laiškai</a></li>
						<?php } ?>
						<?php
						$result = db_query("SELECT COUNT(*) AS cnt
FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<='".date('Y-m-d')."' GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
JOIN `".DB_reminder."` ON cr.`parent_kid_id`=`".DB_reminder."`.`kid_id`
WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 AND cr.`grupes_id`=".GROUP_ID." AND `".DB_reminder."`.`isDeleted`=0 AND `expiry_date`<'".date('Y-m-d')."'");
						$r = mysqli_fetch_assoc($result);
						$newReminder = '';
						if($r['cnt'] > 0)
							 $newReminder = ' (<strong class="abbr" title="Nebegalioja dokumentų">'.$r['cnt'].'</strong>)';
						?>
						<li<?php echo ($_GET['q'] == 'priminimai' ? ' class="active"' : '')?>><a href="/priminimai">Priminimai<?=$newReminder?></a></li>
						<li<?php echo ($_GET['q'] == 'dokumentai' ? ' class="active"' : '')?>><a href="/dokumentai">Vaiko dokumentai</a></li>
						<?php if($show_health_observations && (TESTINIS || RODOMASIS || NAUJAS || KaunoZiburelis || KaunoRudnosiukas /*|| SiauliuRSavivald*/ || SiauliuZiogelis || LazdijuRZibute || TauragesRAzuoliukas || KaunoVaikystesLobiai || KaunoVarpelis || RokiskioVarpelisLD || KretingosRSalantuRasaLD || TauragesKodelciusLD || DB_PREFIX == 'pnvZil_')) {//!(KaunoRBaibokyne || LazdijuVyturelis || LazdijuRAzuoliukas || SEMINARINE)) { ?>
						<li<?php echo ($_GET['q'] == 'sveikatos_pastabos' ? ' class="active"' : '')?>><a href="/sveikatos_pastabos">Sveikatos pastabos</a></li>
						<?php } ?>
						<li<?php echo ($_GET['q'] == 'notes' ? ' class="active"' : '')?>><a href="/notes">Grupės užrašinė</a></li>
					<?php } ?>
					</ul>
					</li>
				<?php
				}
			}
		}
		?>
		<!-- </ul>
	</li> -->
	<?php } ?>
	
	<?php if(ADMIN) { ?>
		<li class="separator" style="margin-left: 0px; background-image: url(/img/menu/group_edit.png);  background-position: 5px 5px; padding-left: 30px; margin-top: 2px;">Pagrindinė informacija</li>
		<li<?php echo (isset($_GET['q']) && $_GET['q'] == 'grupes' ? ' class="active"' : '')?> style="/*background-image: url(/img/menu-groups.png); background-position: left 2px;*/"><a href="/grupes">Grupės</a></li>
		<li<?php echo ($_GET['q'] == 'vaikai' ? ' class="active"' : '')?>><a href="/vaikai">Vaikai</a></li>
		<li<?php echo ($_GET['q'] == 'darbuotojai' ? ' class="active"' : '')?>><a href="/darbuotojai">Darbuotojai</a></li>
		<li<?php echo ($_GET['q'] == 'prisijungimai' ? ' class="active"' : '')?>><a href="/prisijungimai" class="abbr" title="Prieiga prie sistemos tėvams ir darbuotojams">Prieiga</a></li>
	
		<li class="separator" style="margin-left: 0px; background-image: url(/img/menu/groups_info.png);  background-position: 7px 5px; padding-left: 30px; margin-top: 2px;">Vaikų ir grupių info.</li>
		<li<?php echo ($_GET['q'] == 'tabeliai' ? ' class="active"' : (startsWith($_GET['q'], 'tabeliai') ? ' class="active-parent"' : ''))?>><a href="/tabeliai">Žiniaraštis (tabelis)</a></li>
		<?php //if(TESTINIS) { ?>
		<?php echo (startsWith($_GET['q'], 'tabeliai') ? '<li'.(startsWith($_GET['q'], 'tabeliai/rakinimas') ? ' class="active"' : '').'><a href="/tabeliai/rakinimas">&nbsp; + Užbaigimas</a></li>' : '')?>
		<?php //} ?>
		<li<?php echo ($_GET['q'] == 'veikla' ? ' class="active"' : '')?>><a href="/veikla">Vaiko veikla</a></li>
		<li<?php echo ($_GET['q'] == 'vaiko_pazanga_ir_pasiekimai' ? ' class="active"' : '')?>><a href="/vaiko_pazanga_ir_pasiekimai">Vaiko pasiekimai</a></li>
		<li<?php echo ($_GET['q'] == 'vaiko_pasiekimai_pagal_nauja_aprasa' ? ' class="active"' : '')?>><a href="/vaiko_pasiekimai_pagal_nauja_aprasa" class="abbr" title="Vaiko žingsninė pažanga ir pasiekimai">Pasiekimų žingsneliai</a></li><!-- Naujieji -->
		<li<?php echo ($_GET['q'] == 'forms_fill_out' ? ' class="active"' : '')?>><a href="/forms_fill_out">Tekstas <!-- Rašyti formose --> <span style="font-size: 13px;" class="abbr" title="Pavyzdys">(priešmok.)</span></a></li><?php //Šablonus, Rašyti pagal formas //Vėliau Atestavimui ?>
		<li<?php echo ($_GET['q'] == 'forms_make' ? ' class="active"' : '')?> title="Daryti/pritaikyti/tvirtinti formas planavimui ir priešmokykliniam vertinimui"><a href="/forms_make" class="abbr"><!-- ↳ -->Teksto<!-- Sudaryti --> formos<span style="font-size: 13px;" class="abbr" title="Pavyzdys"> (priešmok.)</span></a></li>
		<?php if($showOldPlanning) {//!(KaunoVaikystesLobiai || KaunoVarpelis)) { ?>
			<li<?php echo ($_GET['q'] == 'planavimas' ? ' class="active"' : '')?>><a href="/planavimas">Planavimas</a></li>
		<?php } ?>
		<li<?php echo ($_GET['q'] == 'planavimo_formos_pildymas' ? ' class="active"' : '')?>><a href="/planavimo_formos_pildymas">Planavimas formose</a></li>
		<li<?php echo ($_GET['q'] == 'planavimo_formos' ? ' class="active"' : '')?>><a href="/planavimo_formos"><!-- ↳ -->Planavimo formų kūrimas</a></li>
	
	
	
		<li<?php echo ($_GET['q'] == 'daily_rhythm' ? ' class="active"' : '')?>><a href="/daily_rhythm">Dienos ritmas</a></li>
		<li<?php echo ($_GET['q'] == 'bureliai' ? ' class="active"' : '')?>><a href="/bureliai">Užsiėmimai<!--  class="abbr" Papildoma veikla --></a></li><!-- title="Papildomos grupių veiklos sąrašas" -->
		<li<?php echo ($_GET['q'] == 'grupiu_bureliai' ? ' class="active"' : '')?>><a href="/grupiu_bureliai">Užsiėmimų tvarkaraštis<!--  class="abbr" Pap. veiklos --></a></li><!--  title="Papildomos grupių veiklos tvarkaraštis" -->
		<?php //if(TESTINIS || RODOMASIS || KaunoRBaibokyne) {//!(KaunoVaikystesLobiai || KaunoVarpelis || KaunoZiburelis || KaunoRudnosiukas || KaunoRBaibokyne)) { ?>
			<li<?php echo ($_GET['q'] == 'activities' ? ' class="active"' : '')?>><a href="/activities">Numatomos veiklos</a></li>
		<?php //} ?>
		<li<?php echo ($_GET['q'] == 'dokumentai' ? ' class="active"' : '')?>><a href="/dokumentai">Vaiko dokumentai</a></li>
		<?php if($show_health_observations) { ?>
		<li<?php echo ($_GET['q'] == 'sveikatos_pastabos' ? ' class="active"' : '')?>><a href="/sveikatos_pastabos">Sveikatos pastabos</a></li>
		<?php } ?>
		<?php if(!(KaunoRBaibokyne || KaunoZingsnelis) && $showPayment) { ?>
			<li<?php echo ($_GET['q'] == 'kvitai' ? ' class="active"' : '')?>><a href="/kvitai">Kvitai <span class="notice">(spausdinimui)</span></a></li>
		<?php } ?>
		<?php if(TESTINIS || /*RODOMASIS ||*/ NAUJAS || SiauliuRSavivald) {//!(KaunoVaikystesLobiai || KaunoVarpelis || KaunoZiburelis || KaunoRudnosiukas || KaunoRBaibokyne)) { ?>
			<li<?php echo ($_GET['q'] == 'mokejimai' ? ' class="active"' : '')?>><a href="/mokejimai">Mokėjimai</a></li>
		<?php } ?>
	<?php } ?>

	<li class="separator" style="margin-left: 0px; background-image: url(/img/menu/communicate.png); background-position: 3px 5px; padding-left: 30px; margin-top: 2px;">Bendravimas</li>
		<li<?php echo ($_GET['q'] == 'zinutes' ? ' class="active"' : '')?> style="/*background-image: url(/img/menu-messages.png);*/"><a href="/zinutes">Susirašinėjimas<?=$newMsg?></a></li>
		<?php //if(!(BUHALT/* && !DruskininkuBitute*/)) { ?>
		<li<?php echo ($_GET['q'] == 'laiskai_grupems' ? ' class="active"' : '')?> style="/*background-image: url(/img/menu-letters.png);*/"><a href="/laiskai_grupems">Laiškai grupės<!-- (-ių) --> tėvams</a></li>
		<?php //} ?>

	<li class="separator" style="margin-left: 0px; background-image: url(/img/menu/common2.png);  background-position: 3px 5px; padding-left: 30px; margin-top: 2px;">Darželio informacija</li>
	<?php //if(DIETISTAS || SESELE || AUKLETOJA || ADMIN) { ?>
		<li<?php echo ($_GET['q'] == 'menu' ? ' class="active"' : '')?>><a href="/menu">Valgiaraštis</a></li>
	<?php //}
	if((DIETISTAS || SESELE || ADMIN || AUKLETOJA || BUHALT) && !KaunoVaikystesLobiai) { ?>
		<li<?php echo ($_GET['q'] == 'maitinimas' ? ' class="active"' : '')?>><a href="/maitinimas">Maitinimas</a></li>
	<?php }
	if(/*(DIETISTAS || ADMIN || BUHALT) &&*/ !KaunoVaikystesLobiai) { ?>
		<li<?php echo ($_GET['q'] == 'worker_food' ? ' class="active"' : '')?>><a href="/worker_food">Darbuotojų maitinimas</a></li>
	<?php }
	if(DIETISTAS) { ?>
	<li<?php echo (startsWith($_GET['q'], 'tabeliai') ? ' class="active"' : '')?>><a href="/tabeliai">Žiniaraštis (tabelis)</a></li>
	<li<?php echo ($_GET['q'] == 'attendance_lock' ? ' class="active"' : '')?>><a href="/attendance_lock">Lankomumo rakinimas</a></li>
	<?php } ?>
	<li<?php echo ($_GET['q'] == 'work_reminders' ? ' class="active"' : '')?>><a href="/work_reminders">Darbų priminimai</a></li>
	<li<?php echo ($_GET['q'] == 'documents_common' ? ' class="active"' : '')?>><a href="/documents_common">Bendri dokumentai</a></li>
	<li<?php echo ($_GET['q'] == 'parents_menu' ? ' class="active"' : '')?>><a href="/parents_menu">Tėvų meniu</a></li>
	<?php if(ADMIN) { ?>
		<?php //if(TESTINIS || SilutesRZemaiciuNaumiescioMD || SiauliuZiogelis || DB_PREFIX == 'sauTrys_' || DB_PREFIX == 'psvLiep_' || DB_PREFIX == 'kedPur_' || DB_PREFIX == 'kretAzuol_' || DB_PREFIX == 'tauKodel_') {
		$result = db_query("SELECT * FROM `".DB_attendance_marks."` WHERE `kindergarten_id`=".DB_ID);
		if(mysqli_num_rows($result) > 0 || KESV) { ?>
		<li<?php echo ($_GET['q'] == 'tabelio_marks' ? ' class="active"' : '')?>><a href="/tabelio_marks">Žiniaraščio žymės</a></li>
		<?php } ?>
		<li<?php echo ($_GET['q'] == 'nustatymai' ? ' class="active"' : '')?>><a href="/nustatymai">Nustatymai</a></li>
		<li<?php echo ($_GET['q'] == 'archyvas' ? ' class="active"' : '')?>><a href="/archyvas">Archyvas</a></li>
	<?php } ?>
	<?php if(ADMIN && (TESTINIS || RODOMASIS)) { ?>
	<li<?php echo ($_GET['q'] == 'page' ? ' class="active"' : '')?>><a href="/page">Puslapiai</a></li>
	<?php }

	$logo = '<span class="abbr" title="Logopedo, specialiojo pedagogo, tiflopedagogo, surdopedagogo dienynas">Logo.</span> ';
	/*if((ADMIN || LOGOPEDAS) && (TESTINIS)) { ?>
	<li<?php echo ($_GET['q'] == 'speech_therapist_kids' ? ' class="active"' : '')?>><a href="/speech_therapist_kids"><?=$logo?> info (nenaud<!-- . -->ojama)</a></li>
	<?php
	}*/

	$show_speech_therapist = true; //TESTINIS || RODOMASIS || LazdijuVyturelis || KaunoZingsnelis || MoletuVyturelis || LazdijuRAzuoliukas/*2015-01-13*/ || KaunoVarpelis/*2015-02-02*/;
	if((LOGOPEDAS || (ADMIN /*&& TESTINIS*/)) && ($show_speech_therapist)) {
		?><li class="separator" style="margin-left: 0px; background-image: url(/img/menu/girl51.png);  background-position: 7px 5px; padding-left: 30px; margin-top: 2px;" title="Logopedo, specialiojo pedagogo, tiflopedagogo, surdopedagogo dienynai"><span class="abbr">Logopedo ir kt. dienynai</span></li><?php
	}
	if(ADMIN && ($show_speech_therapist)) { ?>
		<li<?php echo ($_GET['q'] == 'speech_diaries' ? ' class="active"' : '')?>><a href="/speech_diaries"><?=$logo?> dienynai</a></li>
	<?php }
	if((LOGOPEDAS || (ADMIN /*&& TESTINIS*/)) && ($show_speech_therapist)) {
		if(ADMIN)
			$result = db_query("SELECT * FROM `".DB_speech_diaries."`");
		else
			$result = db_query("SELECT * FROM `".DB_speech_diaries."` WHERE `employee_id`=".DARB_ID);
		$diaries_cnt = mysqli_num_rows($result);
		if($diaries_cnt > 1) {
			echo '<form method="post" style="display: inline;"><div class="sel"><select style="width: 204px;" name="change_speech_therapist_diary" onchange="this.form.submit()">';
			while($r = mysqli_fetch_assoc($result)) {
				//Securely
				if(isset($_POST['change_speech_therapist_diary']) && $_POST['change_speech_therapist_diary'] == $r['ID'])
					$_SESSION['SPEECH_THERAPIST_DIARY'] = $r['ID'];
				
				if(!isset($_SESSION['SPEECH_THERAPIST_DIARY'])) {
					$_SESSION['SPEECH_THERAPIST_DIARY'] = $r['ID'];
					$_SESSION['SPEECH_THERAPIST_position_id'] = $r['position_id'];
				}
				if(isset($_SESSION['SPEECH_THERAPIST_DIARY']) && $_SESSION['SPEECH_THERAPIST_DIARY'] == $r['ID']) {
					$logo = '<span class="abbr" title="Logopedo, specialiojo pedagogo, tiflopedagogo, surdopedagogo dienynas">'.mb_substr($pareigybes[$r['position_id']], 0, 4).'.</span> ';
					$_SESSION['SPEECH_THERAPIST_position_id'] = $r['position_id'];
				}
				echo '<option value="'.$r['ID'].'"'.(isset($_SESSION['SPEECH_THERAPIST_DIARY']) && $_SESSION['SPEECH_THERAPIST_DIARY'] == $r['ID']  ? ' selected="selected"' : '').">".$pareigybes[$r['position_id']].' '.getAllEmployees($r['employee_id'])."</option>";
			}
			echo '</select></div></form>';
		} else {
			$r = mysqli_fetch_assoc($result);
			$_SESSION['SPEECH_THERAPIST_DIARY'] = $r['ID'];
			$_SESSION['SPEECH_THERAPIST_position_id'] = $r['position_id'];
			if(isset($_SESSION['SPEECH_THERAPIST_DIARY']) && $_SESSION['SPEECH_THERAPIST_DIARY'] == $r['ID'])
				$logo = '<span class="abbr" title="Logopedo, specialiojo pedagogo, tiflopedagogo, surdopedagogo dienynas">'.mb_substr($pareigybes[$r['position_id']], 0, 4).'.</span> ';
		}
	
	
		if(isset($_SESSION['SPEECH_THERAPIST_DIARY'])) {
		?>
		<li<?php echo ($_GET['q'] == 'speech_kids' ? ' class="active"' : '')?>><a href="/speech_kids"><?=$logo?> vaikai</a></li>
		<li<?php echo ($_GET['q'] == 'speech_groups' ? ' class="active"' : '')?>><a href="/speech_groups"><?=$logo?> grupės</a></li>
		<?php if(!ADMIN) { ?>
		<li<?php echo ($_GET['q'] == 'speech_attendance' ? ' class="active"' : '')?>><a href="/speech_attendance"><?=$logo?> <span class="abbr" title="Vaikų grupių lankomumo žymėjimas">žym. lanko.</span></a></li>
		<?php } ?>
		<li<?php echo ($_GET['q'] == 'speech_attendance_report' ? ' class="active"' : '')?>><a href="/speech_attendance_report"><?=$logo?><span title="Vaikų grupių lankomumo ataskaita" class="abbr">lanko. atask.</span></a></li>
		<li<?php echo ($_GET['q'] == 'speech_topics' ? ' class="active"' : '')?>><a href="/speech_topics"><?=$logo?><span class="abbr" title="Individualus, grupinis ar pogrupinis darbas su ugdytiniais">temos</span></a></li>
		<li<?php echo ($_GET['q'] == 'speech_other_activities' ? ' class="active"' : '')?>><a href="/speech_other_activities"><?=$logo?> kita veikla</a></li>
		<li<?php echo ($_GET['q'] == 'remarks_and_suggestions' ? ' class="active"' : '')?>><a href="/remarks_and_suggestions"><?=$logo?> pastebėjimai</a></li>
		<li<?php echo ($_GET['q'] == 'speech_schedule' ? ' class="active"' : '')?>><a href="/speech_schedule"><?=$logo?> tvarkaraštis</a></li>
		<?php
		} else {
			if(LOGOPEDAS) {
				?><li title="Darželio vadovai nesukūrė dienyno logopedui, specialiajam pedagogui, tiflopedagogui, surdopedagogui, todėl negalite matyti tokiam dienynui skirtų meniu punktų"><span class="abbr">Vadovai nusukūrė dienyno</span></li><?php
			}
		}
	}
	/*
	Logopedo info
	Logopedo vaikai
	Logopedo grupės
	Logopedui lanko. atask.
	Logopedui temos
	Logopedo kita veikla
	Logopedo pastebėjimai
	Logopedo tvarkaraštis*/

	if(LOGOPEDAS) { ?>
	<li<?php echo ($_GET['q'] == 'grupiu_bureliai' ? ' class="active"' : '')?> title="Vaikų ugdymo užsiėmimų tvarkaraštis"><a href="/grupiu_bureliai" class="abbr">Užsiėmimų tvark.<!-- Pap. grupių veikla --></a></li><!-- title="Papildomos grupių (neformaliojo ugdymo) veiklos tvarkaraštis" -->
	<?php }



if(!LOGOPEDAS && TESTINIS) {
	$informal = '<span class="abbr" title="Neformaliojo švietimo">Nef.</span> ';
	if(ADMIN)
		$result = db_query("SELECT * FROM `".DB_informal_diaries."` WHERE `kindergarten_id`=".DB_ID."");
	else
		$result = db_query("SELECT * FROM `".DB_informal_diaries."` WHERE `kindergarten_id`=".DB_ID." AND `employee_id`=".DARB_ID);
	$diaries_cnt = mysqli_num_rows($result);
	
	if(ADMIN || !ADMIN && $diaries_cnt) {
		?><li class="separator" style="margin-left: 0px; background-image: url(/img/menu/girl51.png);  background-position: 7px 5px; padding-left: 30px; margin-top: 2px;" title="Neformaliojo švietimo dienynai"><span class="abbr">Neformalūs dienynai</span></li><?php
		if(ADMIN) { ?>
			<li<?php echo ($_GET['q'] == 'informal_diaries' ? ' class="active"' : '')?>><a href="/informal_diaries"><?=$informal?> dienynai</a></li>
		<?php }
		
		if($diaries_cnt > 1) {
			echo '<form method="post" style="display: inline;"><div class="sel"><select style="width: 204px;" name="change_informal_diary" onchange="this.form.submit()">';
			while($r = mysqli_fetch_assoc($result)) {
				//Securely
				if(isset($_POST['change_informal_diary']) && $_POST['change_informal_diary'] == $r['ID'])
					$_SESSION['INFORMAL_DIARY'] = $r['ID'];
				
				if(!isset($_SESSION['INFORMAL_DIARY']))
					$_SESSION['INFORMAL_DIARY'] = $r['ID'];
				if(isset($_SESSION['INFORMAL_DIARY']) && $_SESSION['INFORMAL_DIARY'] == $r['ID'])
					$logo = '<span class="abbr" title="Logopedo, specialiojo pedagogo, tiflopedagogo, surdopedagogo dienynas">'.mb_substr($pareigybes[$r['position_id']], 0, 4).'.</span> ';
				echo '<option value="'.$r['ID'].'"'.(isset($_SESSION['INFORMAL_DIARY']) && $_SESSION['INFORMAL_DIARY'] == $r['ID']  ? ' selected="selected"' : '').">".$pareigybes[$r['position_id']].' '.getAllEmployees($r['employee_id'])."</option>";
			}
			echo '</select></div></form>';
		} else {
			$r = mysqli_fetch_assoc($result);
			$_SESSION['INFORMAL_DIARY'] = $r['ID'];
		}
	
	
		if(isset($_SESSION['INFORMAL_DIARY'])) {
		?>
		<!-- <li<?php echo ($_GET['q'] == 'speech_kids' ? ' class="active"' : '')?>><a href="/speech_kids"><?=$informal?> vaikai</a></li> -->
		<li<?php echo ($_GET['q'] == 'informal_groups' ? ' class="active"' : '')?>><a href="/informal_groups"><?=$informal?> grupės</a></li>
		<?php if(!ADMIN) { ?>
		<li<?php echo ($_GET['q'] == 'informal_attendance' ? ' class="active"' : '')?>><a href="/informal_attendance"><?=$informal?> <span class="abbr" title="Vaikų grupių lankomumo žymėjimas">žym. lanko.</span></a></li>
		<?php } ?>
		<li<?php echo ($_GET['q'] == 'informal_attendance_report' ? ' class="active"' : '')?>><a href="/informal_attendance_report"><?=$informal?><span title="Vaikų grupių lankomumo ataskaita" class="abbr">lanko. atask.</span></a></li>
		<li<?php echo ($_GET['q'] == 'informal_activities_content' ? ' class="active"' : '')?>><a href="/informal_activities_content"><?=$informal?><span class="abbr" title="Veiklos turinys">veikla</span></a></li>
		<?php
		} else {
			if(!LOGOPEDAS && !AUKLETOJA) {
				?><li title="Darželio vadovai nesukūrė dienyno neformaliajam ugdymui, todėl nematote tokio dienyno meniu punktų"><span class="abbr">Vadovai nusukūrė dienyno</span></li><?php
			}
		}
	}
}
	?>

	<li class="separator" style="margin-left: 0px; background-image: url(/img/menu/other.png);  background-position: 5px 5px; padding-left: 30px; margin-top: 2px;">Kita</li>
	<?php if(DB_PREFIX == 'knsZelmen_') { ?>
	<li<?php echo ($_GET['q'] == 'virtuali_biblioteka' ? ' class="active"' : '')?>><a href="/virtuali_biblioteka">Virtuali biblioteka</a></li>
	<?php } ?>
	<?php if(ALLOW_PARENTS) { ?>
	<li<?php echo ($_GET['q'] == 'parents_logins_stats' ? ' class="active"' : '')?>><a href="/parents_logins_stats">Tėvų prisijungimai</a></li>
	<?php } ?>
	<!-- <li<?php echo ($_GET['q'] == 'wishes' ? ' class="active"' : '')?>><a href="/wishes">Norai</a></li> -->
	<li<?php echo ($_GET['q'] == 'goodies' ? ' class="active"' : '')?>><a href="/goodies">Naudinga informacija</a></li>
	<li<?php echo ($_GET['q'] == 'ikimokyklinis' ? ' class="active"' : '')?>><a href="/ikimokyklinis">Info ikimokyklinis.lt</a></li>
	<li<?php echo ($_GET['q'] == 'spausdinti' ? ' class="active"' : '')?>><a href="/spausdinti">Spausdinimo nustatymai</a></li>
	<li<?php echo ($_GET['q'] == 'kontaktai' ? ' class="active"' : '')?>><a href="/kontaktai">„Mūsų darželis“ kontaktai</a></li>
	<li<?php echo ($_GET['q'] == 'pakeisti_slaptazodi' ? ' class="active"' : '')?> style="background-image: url(/img/menu-password.png); background-position: 8px center;"><a href="pakeisti_slaptazodi">Slaptažodžio keitimas</a></li>
	<li style="background-image: url(/img/menu-logout.png); background-position: 9px center;"><a href="/?atsijungti">Atsijungti</a></li>
</ul>

<?php if(LazdijuVyturelis || TESTINIS) { ?>
	<button onclick="window.print()" style="margin-left: 10px;">Spausdinimas</button>
	<?php
	//Fx - https://support.mozilla.org/lt/questions/948326 //PrintUtils.printPreview(PrintPreviewListener);
	//IE - http://stackoverflow.com/questions/230205/how-can-print-preview-be-called-from-javascript
	
	//☾❨❪❴ http://htmlarrows.com/symbols/
}
?>
