<?php if(!defined('DARBUOT') && !DARBUOT) exit(); ?>
<h1>Vaikų grupių veiklos planavimas</h1>
<div id="content">
<?php
$KaunoZiburelis = TESTINIS;//KaunoZiburelis
if(isset($_GET['issaugoti'])) {
	if( empty($_POST['fromDate']) || empty($_POST['toDate']) ) {
		die("<script type=\"text/javascript\">alert('Neužpildyti būtini laukai'); window.history.back(-1);</script>");
		//die('<div class="red center">Neužpildyti būtini laukeliai.</div>');
	}
	if($KaunoZiburelis)
		$_POST['veiklaVisaiVaikuGrupei'] = serialize(isset($_POST['veiklaVisaiVaikuGrupei']) ? $_POST['veiklaVisaiVaikuGrupei'] : '');
	
	//(int)$_POST['groupId']
	if (!mysqli_query($db_link, "INSERT INTO `".DB_planning."` SET `employeeId`='".DARB_ID."',
		`userId`='".USER_ID."',	`groupId`='".GROUP_ID."',
		`topic`='".db_fix($_POST['topic'])."',
		`fromDate`='".db_fix($_POST['fromDate'])."', `toDate`='".db_fix($_POST['toDate'])."',
		`tikslas`='".db_fix($_POST['tikslas'])."', `uzdaviniai`='".db_fix($_POST['uzdaviniai'])."',
		`ugdomojiVeiklaVaikuGrupelems`='".db_fix($_POST['ugdomojiVeiklaVaikuGrupelems'])."',
		`saveikaSuSeima`='".db_fix($_POST['saveikaSuSeima'])."',
		`veiklaVisaiVaikuGrupei`='".db_fix($_POST['veiklaVisaiVaikuGrupei'])."',
		`savaitesApmastymaiRefleksija`='".db_fix($_POST['savaitesApmastymaiRefleksija'])."',
		`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'")) {
		logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
	} else {
		$plan_id = mysqli_insert_id($db_link);
		if(isset($_POST['individuali_veikla'])) foreach($_POST['individuali_veikla'] as $kid_id => $activity) {
			db_query("INSERT INTO `".DB_planning_individual."` SET `plan_id`='".(int)$plan_id."', `child_id`='".(int)$kid_id."',  
					`activity`='".db_fix($activity)."', `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
		}
		
		msgBox('OK', 'Planuojamos veiklos informacija išsaugota!');
	}
}
if(isset($_GET['atnaujinti'])) {
	if( empty($_POST['fromDate']) || empty($_POST['toDate']) ) {
		die("<script type=\"text/javascript\">alert('Neužpildyti būtini laukai'); window.history.back(-1);</script>");
		//die('<div class="red center">Neužpildyti būtini laukeliai.</div>');
	}
	if($KaunoZiburelis) {
		//print_r($_POST['veiklaVisaiVaikuGrupei']);
		$_POST['veiklaVisaiVaikuGrupei'] = serialize(isset($_POST['veiklaVisaiVaikuGrupei']) ? $_POST['veiklaVisaiVaikuGrupei'] : '');
	}
	//	`groupId`='".(int)$_POST['groupId']."',
	if (!mysqli_query($db_link, "UPDATE `".DB_planning."` SET `employeeId`='".DARB_ID."',
		`userId`='".USER_ID."',
		`fromDate`='".db_fix($_POST['fromDate'])."', `toDate`='".db_fix($_POST['toDate'])."',
		`topic`='".db_fix($_POST['topic'])."',
		`tikslas`='".db_fix($_POST['tikslas'])."', `uzdaviniai`='".db_fix($_POST['uzdaviniai'])."',
		`ugdomojiVeiklaVaikuGrupelems`='".db_fix($_POST['ugdomojiVeiklaVaikuGrupelems'])."',
		`saveikaSuSeima`='".db_fix($_POST['saveikaSuSeima'])."',
		`veiklaVisaiVaikuGrupei`='".db_fix($_POST['veiklaVisaiVaikuGrupei'])."',
		`savaitesApmastymaiRefleksija`='".db_fix($_POST['savaitesApmastymaiRefleksija'])."',
		`updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."',
		`updated`=CURRENT_TIMESTAMP() WHERE `planning_id`=".(int)$_GET['atnaujinti'])) {
		logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
	} else {
		$plan_id = (int)$_GET['atnaujinti'];
		db_query("DELETE FROM `".DB_planning_individual."` WHERE `plan_id`=".(int)$plan_id);
		if(isset($_POST['individuali_veikla']))
			foreach($_POST['individuali_veikla'] as $kid_id => $activity) {
				db_query("INSERT INTO `".DB_planning_individual."` SET `plan_id`='".(int)$plan_id."', `child_id`='".(int)$kid_id."',  
						`activity`='".db_fix($activity)."'");
			}
		msgBox('OK', 'Planuojamos veiklos informacija išsaugota!');
	}
}

if(isset($_GET['delete']) && !empty($_GET['delete'])) {
	//TODO: check if allowed to delete
	if (!mysqli_query($db_link, "DELETE FROM `".DB_planning."` WHERE `planning_id`=".(int)$_GET['delete'])) {
		logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
	} else {
		if (!mysqli_query($db_link, "DELETE FROM `".DB_planning_individual."` WHERE `plan_id`=".(int)$_GET['delete']))
			logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
		else
			msgBox('OK', 'Planavimo dokumentas sėkmingai ištrintas!');
	}
}


$grupes = getAllGroups();
$grupes[0] = 'Neįvesta';
	
$URL = '?';
if( isset($_GET['grupes_id']) ) {
	$URL .= 'grupes_id='.(int)$_GET['grupes_id'].'&amp;';
} else {
	$_GET['grupes_id'] = 0;
}

if(!ADMIN) {
	echo '<a href="'.('?' == $URL ? '' : $URL).'#planning_form" class="no-print fast-action fast-action-add">Įvesti naują planuojamą veiklą</a> ';
	ui_print();
}

if(ADMIN)
	$result = db_query("SELECT * FROM `".DB_planning."`
	".(isset($_GET['grupes_id']) && $_GET['grupes_id'] > 0 ? ' WHERE `groupId`='.(int)$_GET['grupes_id'] : '')."
	ORDER BY `toDate` DESC");
else
	$result = db_query("SELECT * FROM `".DB_planning."` WHERE `groupId`=".GROUP_ID." ORDER BY `toDate` DESC");
if(mysqli_num_rows($result) > 0) {
	if(ADMIN) {
		?>
		<form method="get" class="no-print">
			<input type="hidden" name="autofocus">
			<div class="sel" style="float: left;"><select name="grupes_id" onchange="this.form.submit()" <?=(isset($_GET['autofocus']) ? 'autofocus' : '')?>>
			<?php
			foreach($grupes as $ID => $title)
				if($ID == 0)
					echo "<option value=\"".$ID."\"".(isset($_GET['grupes_id']) && $ID == $_GET['grupes_id'] ? ' selected="selected"' : '').">Visos grupės</option>";
				else
					echo "<option value=\"".$ID."\"".(isset($_GET['grupes_id']) && $ID == $_GET['grupes_id'] ? ' selected="selected"' : '').">".$title."</option>";
			?>
			</select></div>
		</form>
		<?php
	}
	?>
<table class="vertical-hover<?php if(isset($_GET['view'])) echo ' no-print'; ?>">
<tr>
	<th style="width: 161px;">Nuo–iki</th>
	<th>Grupė</th>
	<th><?=($KaunoZiburelis ? 'Turinys' : 'Tema')?></th>
	<th>Tikslas</th>
	<th>Uždaviniai</th>
	<th>Veiksmai</th>
</tr>
<?php
	while ($row = mysqli_fetch_assoc($result)) {
		echo "		<tr>
		<td>".$row['fromDate']."–".$row['toDate']."</td>
		<td>".$grupes[$row['groupId']]."</td>
		<td>".filterText($row['topic'])."</td>
		<td>".filterText($row['tikslas'])."</td>
		<td>".filterText($row['uzdaviniai'])."</td>
		<td><a href=\"".$URL."view=".$row['planning_id']."#planning-view\">Peržiūrėti<!-- Plačiau --></a>
		<a href=\"".$URL."edit=".$row['planning_id']."#planning_form\">Keisti</a>
		<a href=\"?delete=".$row['planning_id']."\" onclick=\"return confirm('Ar tikrai norite ištrinti planavimo dokumentą?')\">Trinti</a> 
		</td>
	</tr>";	
	}
	echo '</table>';
} elseif(ADMIN) {
	echo 'Esate prisijungęs kaip vadovas, auklėtojos dar nėra įkėlusios planavimų, kuriuos galėtumėte peržiūrėti ar reikalui esant pakoreguoti.';
}


// VIEW --------------------------------
if(isset($_GET['view'])) {
	$result = db_query("SELECT * FROM `".DB_planning."` WHERE `planning_id`=".(int)$_GET['view']." LIMIT 1");
	//if(isset($_GET['view']) && $_GET['view'] == $row['planning_id'])
	$plan_view = mysqli_fetch_assoc($result);//$row;
	?>
	<div id="planning-view">
		<p><span class="title"><?=($KaunoZiburelis ? 'Turinys' : 'Tema')?>:</span> <?=filterText($plan_view['topic'])?></p>
		<p><span class="title">Laikotarpis:</span> <?=date_empty($plan_view['fromDate'])?>–<?=date_empty($plan_view['toDate'])?></p>
		<p><span class="title">Planuojama (ugdomoji) veikla <span class="details">(tikslas)</span>:</span><br>
			<?=filterText($plan_view['tikslas'])?></p>
		<p><span class="title">Planuojama (ugdomoji) veikla <span class="details">(uždaviniai)</span>:</span><br>
			<?=filterText($plan_view['uzdaviniai'])?></p>
		<div><span class="title">Individuali veikla <span class="details">(Ugdymo individualizavimas pagal vaiko poreikius, gebėjimus ir kitus ypatumus)</span>:</span><br>
			<?php
	$result = db_query("SELECT `".DB_children."`.*, `".DB_planning_individual."`.`activity` 
	FROM `".DB_planning_individual."` JOIN `".DB_children."` ON `".DB_children."`.`ID`=`".DB_planning_individual."`.`child_id`
	WHERE `".DB_planning_individual."`.`plan_id`=".(int)$plan_view['planning_id']." ORDER BY ".orderName());//vardas, pavarde
	while ($row = mysqli_fetch_assoc($result)) {
		echo "<p><strong>".filterText(getName($row['vardas'], $row['pavarde']))."</strong>: ".filterText($row['activity'])."</p>";
	}
			?></div>
		<p><span class="title">(Ugdomoji) veikla vaikų grupelėms <span class="details">(Ugdymo diferencijavimas pagal vaikų poreikius, gebėjimus ir kitus ypatumus)</span>:</span><br>
			<?=filterText($plan_view['ugdomojiVeiklaVaikuGrupelems'])?></p>
		<p><span class="title">Sąveika/bendradarbiavimas su šeima <span class="details">(Auklėtojų, tėvų (globėjų) iniciatyvos, siūlymai, bendradarbiavimo būdai)</span>:</span><br>
			<?=filterText($plan_view['saveikaSuSeima'])?></p>
		<p><span class="title">Veikla visai vaikų grupei <span class="details">(Turinys, metodai, pagrindinės priemonės, vieta)</span>:</span><br>
			<?php
			if($KaunoZiburelis) {
				?>
				<table>
				<thead>
					<tr>
						<th>Eil.<br>Nr.</th>
						<th><!-- Veiklos pavadinimas -->Ugdomoji veikla</th>
						<!-- <th>Vaiko pasiekimai, kurie vestų į galutinį rezultatą</th> -->
						<th>Priemonės</th>
						<th>Galutinis vaiko pasiekimų rezultatas pasiekus uždavinį</th>
					</tr>
				</thead>
				<tbody id="tbody">
				<?php
				$plan_view['veiklaVisaiVaikuGrupei'] = unserialize($plan_view['veiklaVisaiVaikuGrupei']);
				$i=0;
				if(!empty($plan_view['veiklaVisaiVaikuGrupei']))
					foreach($plan_view['veiklaVisaiVaikuGrupei'] as $veiklaVisaiVaikuGrupei) {
						echo '<tr><td>'.(++$i).'</td>
						<td>'.filterText($veiklaVisaiVaikuGrupei['VeiklosPavadinimas']).'</td>
						
						<td>'.filterText($veiklaVisaiVaikuGrupei['Priemones']).'</td>
						<td>'.filterText($veiklaVisaiVaikuGrupei['GalutinisRezultatas']).'</td>';
					}//<td>'.filterText($veiklaVisaiVaikuGrupei['VaikoPasiekimai']).'</td>
				?>
				</body>
				</table>
				<?php
			} else {
				echo filterText($plan_view['veiklaVisaiVaikuGrupei']);
			}
			?></p>
		<p><span class="title">Savaites apmąstymai, refleksija <span class="details">(Kaip sekėsi įgyvendinti numatytą tikslą ir uždavinius)</span>:</span><br>
			<?=filterText($plan_view['savaitesApmastymaiRefleksija'])?></p>
		
		<div class="no-print">
			<div style="float: left; width: 70px;">Sukūrė:</div><div style="float: left;"><?=$plan_view['created']?> <?=getAllEmployees($plan_view['createdByEmployeeId'])?></div>
			<div style="clear: left; float: left; width: 70px;">Atnaujino:</div><div style="float: left;"><?=$plan_view['updated']?> <?=getAllEmployees($plan_view['updatedByEmployeeId'])?></div>
			<div style="clear: left;"></div>
		</div>
	</div>
	<?php
}


// EDIT -----------------------------
if(!(!isset($_GET['edit']) && ADMIN)) {//Nerodom vadovui naujo planavimo
	if(isset($_GET['edit'])) {
		$result = db_query("SELECT * FROM `".DB_planning."` WHERE `planning_id`=".(int)$_GET['edit']);
		$plan = mysqli_fetch_assoc($result);
	}
	?>
	<fieldset style="margin-top: 40px;" id="planning_form"<?php if(isset($_GET['view'])) echo ' class="no-print"'; ?>>
	<legend><?=(isset($_GET['edit']) ? 'Redaguoti planuojamą veiklą' : 'Nauja planuojama veikla')?></legend>
	<form name="editForm" method="post" class="not-saved-reminder" action="<?=$URL.(!isset($_GET['edit']) ? 'issaugoti' : 'atnaujinti='.(int)$plan['planning_id'])?>"<?=(isset($_GET['edit']) && ($KaunoZiburelis || TESTINIS) ? ' onsubmit="isAbleToSubmit(); return false;"' : '')?>>
		<?=(isset($_GET['edit']) ? '<input type="hidden" name="updated" value="'.date_empty($plan['updated']).'" id="updated"><input type="hidden" name="plan_id" value="'.(int)$plan['planning_id'].'" id="plan_id">' : '')?>
		<!-- <p><label>Grupei<span class="required">*</span>: <div class="sel"><select name="groupId">
			<!-- <option value="0">Pasirinkite grupę</option> ->
			<?php
			/*
			foreach(getAllowedGroups() as $ID => $title)
				echo "<option value=\"".$ID."\"".(isset($_GET['edit']) && $ID == $plan['groupId'] || (!isset($_GET['edit']) && GROUP_ID == $ID) ? ' selected="selected"' : '').">".$title."</option>";
			*/
			?>
		</select></div></label></p> -->
		<p><label><?=($KaunoZiburelis ? 'Turinys' : 'Tema')?><span class="required">*</span>:<br><input type="text" name="topic" value="<?=(isset($_GET['edit']) ? filterText($plan['topic']) : '')?>" style="width: 500px" required="required"></label></p>
		<p><label>Nuo<span class="required">*</span>:<br><input class="datepicker" type="text" name="fromDate" value="<?=(isset($_GET['edit']) ? date_empty($plan['fromDate']) : '')?>" placeholder="<?php echo date('Y-m-d'); ?>" required="required"></label></p>
		<p><label>Iki<span class="required">*</span>:<br><input class="datepicker" type="text" name="toDate" value="<?=(isset($_GET['edit']) ? date_empty($plan['toDate']) : '')?>" placeholder="<?php echo date('Y-m-d'); ?>" required="required"></label></p>
		<p><label>Planuojama (ugdomoji) veikla <span class="details">(tikslas)</span><span class="required">*</span>:<br><textarea required="required" name="tikslas" style="height: 20px; /*width: 500px*/"><?=(isset($_GET['edit']) ? filterText($plan['tikslas']) : '')?></textarea></label></p>
		<p><label>Planuojama (ugdomoji) veikla <span class="details">(uždaviniai)</span>: <textarea name="uzdaviniai"><?=(isset($_GET['edit']) ? filterText($plan['uzdaviniai']) : '')?></textarea></label></p>
		<p>Individuali veikla <span class="details">(Ugdymo individualizavimas pagal vaiko poreikius, gebėjimus ir kitus ypatumus)</span><br>
		<div class="sel"><select id="kidId">
			<?php
			/*if(ADMIN)
				$result = mysqli_query($db_link, "SELECT * FROM `".DB_children."` WHERE `isDeleted`=0 AND `archyvas`=0 ORDER BY vardas, pavarde" );
			else
				$result = mysqli_query($db_link, "SELECT `".DB_children."`.*
					FROM `".DB_children."` 
					JOIN `".DB_employees_groups."` ON `".DB_children."`.`grupes_id`=`".DB_employees_groups."`.`grup_id`
					WHERE `".DB_employees_groups."`.darb_id=".(int)DARB_ID." AND `".DB_children."`.`isDeleted`=0 AND `".DB_children."`.`archyvas`=0
					ORDER BY vardas, pavarde");//TODO: filter by selected kids group
			*/
			$result = db_query($get_kids_sql);
			while ($row = mysqli_fetch_assoc($result)) {
				echo "<option value=\"".$row['parent_kid_id']."\">".filterText(getName($row['vardas'], $row['pavarde']))."</option>";
			}
			?>
			</select></div>
			<a href="#" id="addScnt">+ Pridėti įvedimo laukelį<!-- šiam vaikui individualios veiklos informaciją --></a></h2>
			<div id="p_scents">
			<?php
			if(isset($_GET['edit'])) {
				$result = db_query("SELECT `".DB_children."`.*, `".DB_planning_individual."`.`activity` 
				FROM `".DB_planning_individual."` JOIN `".DB_children."` ON `".DB_children."`.`ID`=`".DB_planning_individual."`.`child_id`
				WHERE `".DB_planning_individual."`.`plan_id`=".(int)$_GET['edit']." ORDER BY vardas, pavarde");
				while ($row = mysqli_fetch_assoc($result))
					echo "<p><label>".filterText(getName($row['vardas'], $row['pavarde']))." <input type=\"text\" class=\"individual_plan\" name=\"individuali_veikla[${row['ID']}]\" value=\"".filterText($row['activity'])."\"></label> <a href=\"#\" class=\"remScnt\" id=\"individual${row['ID']}\">- Trinti šį laukelį</a></p>";
			}
			?>
			</div>
			<script type="text/javascript" src="/workers/planning.js?3"></script>
		</p>
		<p><label>(Ugdomoji) veikla vaikų grupelėms <span class="details">(Ugdymo diferencijavimas pagal vaikų poreikius, gebėjimus ir kitus ypatumus)</span>: <textarea name="ugdomojiVeiklaVaikuGrupelems"><?=(isset($_GET['edit']) ? filterText($plan['ugdomojiVeiklaVaikuGrupelems']) : '')?></textarea></label></p>
		<p><label>Sąveika/bendradarbiavimas su šeima <span class="details">(Auklėtojų, tėvų (globėjų) iniciatyvos, siūlymai, bendradarbiavimo būdai)</span>: <textarea name="saveikaSuSeima"><?=(isset($_GET['edit']) ? filterText($plan['saveikaSuSeima']) : '')?></textarea></label></p>
		<p><label>Veikla visai vaikų grupei <span class="details">(Turinys, metodai, pagrindinės priemonės, vieta)</span>:
		<?php if($KaunoZiburelis) { ?>
		<table>
			<thead>
				<tr>
					<th>Eil.<br>Nr.</th>
					<th><!--Veiklos pavadinimas -->Ugdomoji veikla</th>
					<!-- <th>Vaiko pasiekimai, kurie vestų į galutinį rezultatą</th> -->
					<th>Priemonės</th>
					<th>Galutinis vaiko pasiekimų rezultatas pasiekus uždavinį</th>
					<th>Valdymas</th>
				</tr>
			</thead>
			<tbody id="tbody">
				<?php
				if(isset($_GET['edit'])) {
					$plan['veiklaVisaiVaikuGrupei'] = unserialize($plan['veiklaVisaiVaikuGrupei']);
					//print_r($plan['veiklaVisaiVaikuGrupei']);
					$i=0;
					if(!empty($plan['veiklaVisaiVaikuGrupei']))
						foreach($plan['veiklaVisaiVaikuGrupei'] as $veiklaVisaiVaikuGrupei) {
							echo '<tr><td>'.($i+1).'</td>
							<td><textarea name="veiklaVisaiVaikuGrupei['.$i.'][VeiklosPavadinimas]">'.filterText($veiklaVisaiVaikuGrupei['VeiklosPavadinimas']).'</textarea></td>
						
							<td><textarea name="veiklaVisaiVaikuGrupei['.$i.'][Priemones]">'.filterText($veiklaVisaiVaikuGrupei['Priemones']).'</textarea></td>
							<td><textarea name="veiklaVisaiVaikuGrupei['.$i.'][GalutinisRezultatas]">'.filterText($veiklaVisaiVaikuGrupei['GalutinisRezultatas']).'</textarea></td>
							<td><a href="#" class="remRow">Trinti šią eilutę</a></td></tr>';
							++$i;
						}//<td><textarea name="veiklaVisaiVaikuGrupei['.$i.'][VaikoPasiekimai]">'.filterText($veiklaVisaiVaikuGrupei['VaikoPasiekimai']).'</textarea></td>
				}
				?>
			</tbody>
		</table>
		<a href="#" id="addRow">Pridėti įvedimo eilutę</a>
		<?php } else { ?>
		<textarea name="veiklaVisaiVaikuGrupei" style="height: 150px;"><?=(isset($_GET['edit']) ? filterText($plan['veiklaVisaiVaikuGrupei']) : '')?></textarea>
		<?php }	?></label></p>
		<p><label>Savaites apmąstymai, refleksija <span class="details">(Kaip sekėsi įgyvendinti numatytą tikslą ir uždavinius)</span>: <textarea name="savaitesApmastymaiRefleksija" style="height: 100px;"><?=(isset($_GET['edit']) ? filterText($plan['savaitesApmastymaiRefleksija']) : '')?></textarea></label></p>
		<p><input type="submit" value="Išsaugoti" id="planning-submit" class="submit"></p>
	</form>
	</fieldset>
	<?php 
}
?>
</div>
