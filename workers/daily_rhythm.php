<?php if(!defined('DARBUOT') && !DARBUOT) exit(); /*TODO: tik auklėtojoms? */ ?>
<h1>Vaiko dienos ritmas <span class="abbr no-print" title="Auklėtojos, tėvai, kartais vadovai">(matomumas)</span></h1>
<div id="content">
<?php
if(isset($_POST['save'])) {
	if(!ADMIN)
		$_POST['group_id'] = GROUP_ID;
	db_query("INSERT INTO `".DB_daily_rhythm."` SET 
		 `kindergarten_id`=".DB_ID.",
		`group_id`='".(int)$_POST['group_id']."', `period`='".db_fix($_POST['period'])."',
		`activity`='".db_fix($_POST['activity'])."',
		`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
	msgBox('OK', 'Dienos ritmo įrašas išsaugotas.');
}
if(isset($_POST['edit'])) {
	//$_POST['group_id'] = GROUP_ID;
	//`group_id`='".(int)$_POST['group_id']."', 
	db_query("UPDATE `".DB_daily_rhythm."` SET 
		`period`='".db_fix($_POST['period'])."',
		`activity`='".db_fix($_POST['activity'])."',
		`updated`=CURRENT_TIMESTAMP, `updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."',
		`updatedCounter`=`updatedCounter`+1
		WHERE  `kindergarten_id`=".DB_ID." AND `ID`=".(int)$_POST['edit']);
	msgBox('OK', 'Dienos ritmo įrašas atnaujintas.');
}
if(isset($_GET['delete'])) {
	db_query("DELETE FROM `".DB_daily_rhythm."` WHERE `kindergarten_id`=".DB_ID." AND `ID`=".(int)$_GET['delete']);
	msgBox('OK', 'Dienos ritmo įrašas ištrintas.');
}
?>
<a href="?new#menu-form" class="no-print fast-action fast-action-add">Naujas įrašas vaiko dienos ritme</a>
<?=ui_print()?>
<?php
if(ADMIN) {
	?>
	<form method="get" class="no-print">
		<input type="hidden" name="autofocus">
		<div class="sel" style="float: left"><select name="group_id" onchange="this.form.submit()" <?=(isset($_GET['autofocus']) ? 'autofocus' : '')?>>
		<?php
		$first = 0;
		foreach(getAllGroups() as $ID => $title) {
			if(!$first)
				$first = $ID;
			echo "<option value=\"".$ID."\"".(isset($_GET['group_id']) && $ID == $_GET['group_id'] ? ' selected="selected" class="selected">'.$selectedMark : '>').$title."</option>";
		}
		?>
		</select></div>
		<input type="submit" class="filter" value="Rodyti">
	</form>
	<?php
}

//if(!ADMIN || ADMIN && isset($_GET['group_id'])) {
	?>
	<table>
	<tr><th>Laikas</th><th>Veikla</th><th class="no-print">Veiksmai</th></tr>
	<?php
	if(ADMIN)
		$result = db_query("SELECT * FROM `".DB_daily_rhythm."` WHERE `kindergarten_id`=".DB_ID." AND `group_id`=".(isset($_GET['group_id']) ? (int)$_GET['group_id'] : $first)." ORDER BY `period`");
	else
		$result = db_query("SELECT * FROM `".DB_daily_rhythm."` WHERE `kindergarten_id`=".DB_ID." AND `group_id`=".GROUP_ID." ORDER BY `period`");
	while($row = mysqli_fetch_assoc($result))
		echo "<tr".(isset($_GET['edit']) && $_GET['edit'] == $row['ID'] ? ' class="opened-row"' : '')."><td>".filterText($row['period'])."</td><td>".nl2br(filterText($row['activity']))."</td><td class=\"no-print\"><a href=\"?edit=".$row['ID']."\">Keisti</a> <a href=\"?delete=".$row['ID']."\" onclick=\"return confirm('Ar tikrai norite ištrinti dienos ritmo įrašą?')\">Trinti</a></td></tr>";
	echo '</table>';
//}

if (isset($_GET['edit']) || isset($_GET['new'])) {
	if (isset($_GET['edit'])) {
		$result = db_query("SELECT * FROM `".DB_daily_rhythm."` WHERE `kindergarten_id`=".DB_ID." AND `ID`=".(int)$_GET['edit']);
		$day_plan = mysqli_fetch_assoc($result);
	}
	?>
	<fieldset id="menu-form" class="no-print">
	<legend><?=(isset($_GET['edit']) ? 'Vaiko dienos ritmo įrašo keitimas' : 'Naujas vaiko dienos ritmo įrašas')?></legend>
	<form method="post" class="not-saved-reminder">
		<?php if(ADMIN) { ?>
		<div class="sel"><select name="group_id">
		<?php
		foreach(getAllGroups() as $ID => $title)
			echo '<option value="'.$ID.'"'.(isset($day_plan) && $ID == $day_plan['group_id'] ? ' selected="selected" class="selected">'.$selectedMark : '>').$title."</option>";
		?>
		</select></div>
		<?php } ?>
		
		<p><label>Laikotarpis <span class="notice abbr" title="Laikams mažesniems už 10 priekyje prirašykite 0, pvz., 07:00 val.">(rikiavimo tvarkingumui)</span> <input type="text" name="period" value="<?=(isset($day_plan) ? filterText($day_plan['period']) : '')?>" autofocus></label></p>
		<p><label>Veikla <textarea name="activity"><?=(isset($day_plan) ? filterText($day_plan['activity']) : '')?></textarea></label></p>
		<p><input type="hidden" <?=(!isset($_GET['edit']) ? 'name="save"' : 'name="edit" value="'.(int)$day_plan['ID'].'"')?>><input type="submit" value="Išsaugoti" class="submit"></p>
	</form>
	</fieldset>
<?php
}
?>
</div>
