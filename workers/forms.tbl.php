<?php
//----------------------------------------------------------------------------------
if( isset($_GET['newTbl']) || isset($_GET['editTbl']) ) {//NEW TABLE TEMPLATE
	?>
	<script>
	var field = '<div class="field">\
				<a href="#" class="remField">- Trinti šį stulpelį</a>\
				<p><label>Stulpelio pavadinimas<span class="required">*</span>: <input required="required" type="text" name="title[]"></label></p>\
				<p><label>Stulpelio laukelio rūšis: <div class="sel" style="float: left;"><select name="type[]">\
					<?php
					foreach( $widget_field_types as $key => $value )
						echo "<option value=\"$key\">$value</option>";
					?>
				</select></div></label></p>\
				<div style="clear: left">Kas gali pildyti nustatoma tuomet kai parenkama lentelė planavimo formoje.</div>\
				<!-- <div style="clear:left"><label>Stulpelį gali tik peržiūrėti:<br><div class="sel" style="float: left; margin-right: 5px; margin-left: 100px;"><select name="allowedToView[]"><option value="">Pasirinkite ir spauskite „+ Pridėti“</option><?php
				foreach($person_type as $key => $val)
					if($key != 2/*Accountant*/ && $key != 3/*Vadovas*/)// && $key != 6/*Logopedas*/
						echo "<option value=\"".$key."\">".$val."</option>";
				?></select></div></label> <button class="addViewPersonType" style="line-height: 30px;">+ Pridėti</button> -->\
				<input class="viewPersonTypeInput" type="hidden" name="viewPersonType[]" value=""></div>\
			</div>';a
	</script>
	<?php
	if(isset($_GET['editTbl'])) {
		$result = db_query("SELECT * FROM `".DB_forms_widget."` WHERE `widget_id`=".(int)$_GET['editTbl']." AND `kindergarten_id`=".DB_ID);
		$widget = mysqli_fetch_assoc($result);
	}
	?>
	<fieldset id="forms">
	<legend><?=(isset($_GET['editTbl']) ? "Keisti lentelės (skirtos formai) sudėtį" : "Nauja lentelė (skirta formai)")?></legend>
	<form action="#" method="post" id="forms-width" class="not-saved-reminder">
		<p><label>Lentelės pavadinimas<span class="required">*</span>: <textarea required="required" name="main_title"><?=(isset($_GET['editTbl']) ? filterText($widget['title']) : '')?></textarea></label></p>
		<h3>Lentelės stulpeliai:</h3>
		<a href="#" class="addField">+ Pridėti įvedimo laukelį</a>
		<div id="sortable">
		<?php
		$field_exists = false;
		if(isset($_GET['editTbl'])) {
			$result = db_query("SELECT * FROM `".DB_forms_widget_fields."` WHERE `widget_id`=".(int)$_GET['editTbl']." AND `kindergarten_id`=".DB_ID." ORDER BY `order` ASC");
			$i = 0;
			while($field = mysqli_fetch_assoc($result)) {
				$field_exists = true;
			?>
			<div class="field">
				<a href="#" class="remField">- Trinti šį stulpelį</a>
				<p><label>Stulpelio pavadinimas<span class="required">*</span>: <input required="required" type="text" name="title[<?=''//$i?>]" value="<?=(isset($_GET['editTbl']) ? filterText($field['title']) : '')?>"></label></p>
				<p><label>Stulpelio laukelio rūšis: <div class="sel" style="float: left;"><select name="type[<?=''//$i?>]">
					<?php
					foreach( $widget_field_types as $key => $value )
						echo "<option value=\"$key\"".(isset($_GET['editTbl']) && $field['type'] == $key ? ' selected="selected"' : '').">$value</option>";
					?>
				</select></label></div></p>
				<div style="clear: left">Kas gali pildyti nustatoma tuomet kai parenkama lentelė planavimo formoje.</div>
				<!-- <div style="clear: left"><label>Stulpelį gali tik peržiūrėti:<br><div class="sel" style="float: left; margin-right: 5px; margin-left: 100px;"><select name="allowedToView[]"><option value="">Pasirinkite ir spauskite „+ Pridėti“</option><?php
				foreach($person_type as $key => $val)
					if($key != 2/*Accountant*/ && $key != 3/*Vadovas*/)// && $key != 6/*Logopedas*/
						echo "<option value=\"".$key."\">".$val."</option>";
				?></select></div></label> <button class="addViewPersonType" style="line-height: 30px;">+ Pridėti</button>
				<?php
				$res = db_query("SELECT * FROM `".DB_forms_widget_fields_allowed."` WHERE `field_id`=".(int)$field['field_id'].' AND `action`=1 AND `kindergarten_id`='.DB_ID);
				$viewPersonType = array();
				while($allow = mysqli_fetch_assoc($res)) {
					$viewPersonType[] = $allow['allowed_for_person_type'];
					echo "<div style=\"clear: left\">".$person_type[$allow['allowed_for_person_type']]." <input class=\"viewPersonType\" type=\"hidden\" data-view-person-type=\"".$allow['allowed_for_person_type']."\"> <a href=\"#\" class=\"remViewPersonType\">- Trinti</a></div>";//name=\"viewPersonType[".$i."][]\" value
				}
				echo "<input class=\"viewPersonTypeInput\" type=\"hidden\" name=\"viewPersonType[]\" value=\"".implode(",", $viewPersonType)."\">";
				?></div> -->
			</div>
			<?php
				++$i;
			}
		}
		?>
		</div>
		<a href="#" class="addField"<?=(isset($_GET['editTbl']) && !$field_exists || !isset($_GET['editTbl']) ? ' style="display: none;"' : '')?>>+ Pridėti stulpelį</a>
		
		<?php
		$allow_change = true;
		if(isset($_GET['editTbl'])) {
			$widget_id = (int)$widget['widget_id'];
			//someone already answered.
			$forms_with_such_table = array();
			$result = db_query("SELECT `form_id` FROM `".DB_forms_fields."` WHERE `type`=".(int)$widget_id." GROUP BY `form_id`");
			while($row = mysqli_fetch_assoc($result))
				$forms_with_such_table[] = $row['form_id'];
			if(count($forms_with_such_table) > 0)
				$result = db_query("SELECT * FROM `".DB_forms_fields_answer."` WHERE `form_id` IN (".implode(',', $forms_with_such_table).")");
			if(count($forms_with_such_table) > 0 && mysqli_num_rows($result) > 0) {
				$allow_change = false;
			}
		}
		if($allow_change)
			echo '<p><input type="hidden" '.(!isset($_GET['editTbl']) ? 'name="saveTbl"' : 'name="updateTbl" value="'.(int)$widget['widget_id'].'"').'><input type="submit" value="Išsaugoti" class="submit"></p>';
		else
			msgBox('ERROR', 'Lentelės keisti nebegalima, kažkas jau užpildė<!-- atsakė --> dokumentą, kuriame yra ši lentelė. Jei norite, kad galėtų pradėti pildyti formą su kitokia lentele, sukurkite naują lentelę ir formą su ja, galiausiai formą paskelbkite.');
		?>
		
	</form>
	</fieldset>
<?php
}


if(isset($_GET['previewTbl'])) {
	$r = db_query("SELECT * FROM `".DB_forms_widget."` WHERE `widget_id`=".(int)$_GET['previewTbl']." AND `kindergarten_id`=".DB_ID);
	$widget = mysqli_fetch_assoc($r);
	?><fieldset id="form-preview form-fill">
	<legend>Lentelės „<strong><?=$widget['title']?></strong>“ išankstinė peržiūra</legend><?php
	$res = db_query("SELECT * FROM `".DB_forms_widget_fields_allowed."` WHERE `kindergarten_id`=".DB_ID." AND `widget_id`=".(int)$_GET['previewTbl']);//.' AND `action`=1'
	$ViewAllowedWidget = array();
	while($row = mysqli_fetch_assoc($res)) {
		$ViewAllowedWidget[$row['field_id']][$row['allowed_for_person_type']] = '';
		$ViewAllowedWidget[$row['field_id']][3] = '';//3 - manager
	}

	$result_w = db_query("SELECT * FROM `".DB_forms_widget_fields."` WHERE `kindergarten_id`=".DB_ID." AND `widget_id`=".(int)$_GET['previewTbl']." ORDER BY `order` ASC");
	echo '<table class="plan">
	<tr>';
	//$widget_field_types//0 number //1 text
	while($row = mysqli_fetch_assoc($result_w)) {
		//if(isset($ViewAllowedWidget[(int)$row['field_id']][$_SESSION['USER_DATA']['person_type']])) {
			/*$can_edit = ' (Gali peržiūrėti ir pildyti:';
			$res = db_query("SELECT * FROM `".DB_forms_widget_fields_allowed."` WHERE `field_id`=".(int)$row['field_id'].' AND `action`=0');
			while($allow = mysqli_fetch_assoc($res))
				$can_edit .= ' '.$person_type_who_dgs[$allow['allowed_for_person_type']].';';
			$can_edit .= ')';*/
			$can_only_view = ' (Gali tik peržiūrėti:';
			$res = db_query("SELECT * FROM `".DB_forms_widget_fields_allowed."` WHERE `kindergarten_id`=".DB_ID." AND `field_id`=".(int)$row['field_id'].' AND `action`=1');//
			while($allow = mysqli_fetch_assoc($res))
				$can_only_view .= ' '.$person_type_who_dgs[$allow['allowed_for_person_type']].';';
			$can_only_view .= ')';
			$can = $can_only_view;//$can = $can_edit.' '.$can_only_view;
			$can = '';
		
			echo '<th data-type="'.$row['type'].'" data-name="f['/*.$field['field_id']*/.']['.$row['field_id'].'][]" title="'.$can.'"><!-- <span class="abbr"> -->'.$row['title'].'<!-- </span> --></th>';
		//}
	}
	echo '<th title="Trynimas eilučių"><span class="abbr">T</span><input type="hidden" name="f['/*.$field['field_id']*/.'][dumb]"></th>
		</tr>
		</table>
		<a href="#" class="addRow">Pridėti įvedimo eilutę</a>';
	?></fieldset><?php
}
?>
