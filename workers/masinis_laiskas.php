<?php if(!defined('DARBUOT')) exit();
?>
<h1>Laiškai grupės tėvams arba visų grupių tėvams</h1>
<div id="content">
<?php
if (isset($_GET['issiusti'])) {
	if(empty($_POST['tekstas'])) {
		msgBox('ERROR', 'Neįvedėte laiško turinio. Jeigu norite išsiųsti el. laišką prašome įvesti laiško turinį.');
	} else {
		$attachment_names = [];
		$photo_new_names = [];
		$ok = true;
		if(isset($_FILES['attachment'])) {
			$ok = false;
			for($i = 0; $i < count($_FILES['attachment']['tmp_name']); ++$i) {
				if( isset($_FILES['attachment']['tmp_name'][$i]) && is_uploaded_file($_FILES['attachment']['tmp_name'][$i]) ) {
					$add_attachment = true;
					$attachment_name = date('Y-m-d_H.i').'_'.$_FILES['attachment']['name'][$i];
				}
				if(!empty($attachment_name)) {
					//if(verify_image($_FILES['attachment']['tmp_name'])) {
						$photo_ext = strrchr($_FILES['attachment']['name'][$i], ".");

						if($_FILES['attachment']['size'][$i] >= 5*1024*1024) {
							echo "<div class=\"red center\">Klaida: failas gali užimti iki 5 MB</div>";
						} elseif(!in_array(strtolower($photo_ext), $file_type)) {
							echo "<div class=\"red center\">Klaida: Failo tipas privalo būti iš šių failų tipų: ".implode(', ', $file_type)."</div>";
						} else {
							if(!is_dir(UPLOAD_DIR.'mass_mail'))
							    if(!mkdir(UPLOAD_DIR.'mass_mail', 0777))
							        echo "<div class=\"red center\">Nepavyko sukurti katalogo</div>";

							$photo_new_name = UPLOAD_DIR.'mass_mail/'.$attachment_name;
							$photo_new_names[] = $photo_new_name;
							if(move_uploaded_file($_FILES['attachment']['tmp_name'][$i], $photo_new_name)) {
								echo "<div class=\"green center\">Failas išsaugotas.</div>";
								$ok = true;
								$attachment_names[] = $attachment_name;
							} else
								echo "<div class=\"red center\">Failo išsaugoti nepavyko.</div>";
							//chmod($photo_new_name, 0644);
						}
					//} else echo "<div class=\"red center\">Failas nėra paveiksliukas.</div>";
				} else {
					$ok = true;
				}
			}
		}
		if($ok) {
			$fromemail = $_SESSION['USER_DATA']['email'];
			if(!mysqli_query($db_link, "INSERT INTO `".DB_emails_mass."` SET `kindergarten_id`=".DB_ID.", `group_id`=".(int)$_GET['issiusti'].", `tema`='".db_fix($_POST['tema'])."', 
			`tekstas`='".db_fix($_POST['tekstas'])."', 
			`from_email`='".db_fix($fromemail)."', 
			`attachments`='".(empty($attachment_names) ? '' : db_fix(serialize($attachment_names)))."', `isPublic`=".(isset($_POST['isPublic']) ? 1 : 0).", `data`=".time().", `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'")) {
				logdie('Neteisinga užklausa: '. mysqli_error($db_link));
			} else {
				//For testing purposes only:
				//if(sendemail("Tėvams", 'forkik@gmail.com'/*toemail Siunčiama kopija sau */, $_SESSION['USER_DATA']['name'].' '.$_SESSION['USER_DATA']['surname'], 'forkik@gmail.com'/*from email*/, $_POST['tema'], $_POST['tekstas'], "plain", "", implode(", ", array('kestuciui.vaskeviciui@gmail.com')), (!empty($attachment_name) ? $photo_new_name : '')))
				//	msgBox('OK', "El. laiškas išsiųstas forkik@gmail.com".(!empty($attachment_name) ?  : 'NO FILE'.isset($_FILES['attachment']['tmp_name'])));
				$result = db_query("SELECT `".DB_users."`.`email`
				FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<=CURDATE() GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
				JOIN `".DB_users_allowed."` ON `".DB_users_allowed."`.`person_id`=cr.`parent_kid_id`
				JOIN `".DB_users."` ON `".DB_users_allowed."`.`user_id`=`".DB_users."`.`user_id`
				WHERE `".DB_users_allowed."`.`person_type`=0 AND cr.`isDeleted`=0 AND cr.`archyvas`=0".
				($_GET['issiusti'] == 0 ? '' : " AND cr.`grupes_id`=".(int)$_GET['issiusti']).
				" GROUP BY `".DB_users."`.`user_id`");
				/*"SELECT `".DB_users."`.`email` 
				FROM `".DB_children."` 
				JOIN `".DB_users_allowed."` ON `".DB_users_allowed."`.`person_id`=`".DB_children."`.`ID`
				JOIN `".DB_users."` ON `".DB_users_allowed."`.`user_id`=`".DB_users."`.`user_id`
				WHERE `".DB_users_allowed."`.`person_type`=0 AND `".DB_children."`.`archyvas`=0 AND `".DB_children."`.`isDeleted`=0".
				($_GET['issiusti'] == 0 ? '' : " AND `grupes_id`=".(int)$_GET['issiusti']).
				" GROUP BY `".DB_users."`.`user_id`"*/
				//v@v.lt, t@t.lt, t@t.lt, a@a.lt, a@a.lt, a@a.lt, kestuciui.vaskeviciui@gmail.com, f.orkik@gmail.com, f.orkik@gmail.com, m@m.lt
				//t@t.lt, t@t.lt, a@a.lt, kestuciui.vaskeviciui@gmail.com
				//t@t.lt, a@a.lt, kestuciui.vaskeviciui@gmail.com
				$pastai = array();
				while($row = mysqli_fetch_assoc($result))
					$pastai[] = $row['email'];
			
				
				$toname = "Tėvams";
				$toemail = $_SESSION['USER_DATA']['email'];/*Siunčiama kopija sau */
				$fromname = $_SESSION['USER_DATA']['name'].' '.$_SESSION['USER_DATA']['surname'];
				
				$subject = $_POST['tema'];
				$message = $_POST['tekstas'];
				//$type = "plain", $cc = "", $bcc = "", $path = '')
				if ( sendemail($toname, $toemail, $fromname, $fromemail, $subject, $message, "plain", "", implode(", ", $pastai), $photo_new_names) )
					msgBox('OK', 'El. laiškas išsiųstas šiems adresatams: '. implode(", ", $pastai));
				else
					echo "<div class=\"red center\">El. laiško išsiųsti nepavyko!</div>";
			}
		}
	}
}


?>
<p>Grupinis el. laiškas siunčiamas tik grupės tėvams ir jei jų vaikai nėra išregistruoti iš darželio.</p>
<div style="margin-top: 10px;" class="no-print">
	<a href="" onclick="$('#tips').toggle(); return false;">Laiškų rašymo patarimai</a>
	<div style="display: none;" class="notice" id="tips">
		<h2>Pagrindiniai patarimai dėl el. laiškų ar informacinių laiškų turinio</h2>
		Būtina prisiminti, kad geras laiškas yra:
		<ul>
		<li>tiksliai ir teisingai perteikiantis informaciją;</li>
		<li>mandagus, draugiškas, nuoširdus;</li>
		<li>aiškus, išsamus;</li>
		<li>lakoniškas ir sklandus, išbaigtas.</li>
		</ul>
		Laiškai gali būti: informaciniai – informuojantys apie įvykius, naujienas ir pan., su teigiamomis žiniomis ar su neigiamomis, pasiteiravimo, įtikinamieji.
		<h2>Informacinio laiško struktūra</h2>
		<ol style="margin-left: 30px;">
		<li>Įžangoje nurodoma laiško rašymo priežastis ir tikslas. Jei rašomas laiškas pirmą kartą – prisistatoma. Jei rašoma kam nors nukreipus, tai turėtų būti nurodyta.</li>
		<li>Dėstyme nurodoma norima informacija.</li>
		<li>Baigiamoji dalis – atsisveikinimo formulė</li>
		</ol>
		<h2>Informaciniai su teigiamomis žiniomis</h2>
		<ol style="margin-left: 30px;">
		<li>Įžangoje pradedama nuo teigiamos žinios.</li>
		<li>Dėstyme gali būti įtraukiamas įvairių sąlygų aprašymas ir pan.</li>
		<li>Pabaigoje – mandagus atsisveikinimas</li>
		</ol>
		<h2>Informaciniai su neigiamomis žiniomis</h2>
		<ol style="margin-left: 30px;">
		<li>Įžangoje nurodoma laiško rašymo priežastis ir tikslas. </li>
		<li>Dėstymo dalyje paaiškinama ir išvardijami neigiami faktai. Pabrėžiamas noras padėti.</li>
		<li>Trečia dalis – priimto sprendimo paaiškinimas.</li>
		<li>Baigiamoji dalis – viltis dėl tolimesnio bendradarbiavimo ir atsisveikinimo frazės. </li>
		</ol>
		<?php
		/*
		Pasiteiravimo laiško struktūra
Įžangoje nurodoma laiško rašymo priežastis ir tikslas. Jei laiškas rašomas pirmą kartą – prisistatoma. Jei rašoma kam nors nukreipus, tai turėtų būti nurodyta.
Dėstyme nurodoma, ką norima sužinoti, pateikiami klausimai.
Baigiamoji dalis – atsisveikinimo formulė. 
		*/
		?>
	</div>
</div>
<table>
<tr>
    <th>Grupė</th>
    <th>Išsiųsti grupiniai laiškai</th>
    <th>Veiksmai</th>
</tr>
<?php
function access_by_group($group_id) {
	if($group_id == 0)
		return 'Rodomas visiems darbuotojams (viešas)';
	return 'Rodomas tik tiems darbuotojams, kuriems leista dirbti su šia grupe (viešesnis)';
}
$kids_groups = getAllowedGroups();
$kids_groups[0] = 'Visos grupės';
foreach($kids_groups as $group_id => $pavadinimas) {
    ?>
<tr>
    <td><?php echo filterText($pavadinimas); ?></td>
    <td>
        <ul>
        <?php
        $rez = db_query("SELECT * FROM `".DB_emails_mass."` WHERE `kindergarten_id`=".DB_ID." AND `group_id`=".$group_id);
        while ($row = mysqli_fetch_assoc($rez)) {
        	if($row['isPublic'] || !$row['isPublic'] && $row['createdByEmployeeId'] == DARB_ID)
            	echo "<li>".(!empty($row['attachments']) ? '<span class="abbr" title="Su prisegtuku">[P]</span> ' : '').($row['isPublic'] ? '<span class="abbr" title="'.access_by_group($row['group_id']).'">[V]</span> ' : '')."<a href=\"?view=".$row['ID']."#mass_mail\">".date("Y-m-d H:i", $row['data']).": <strong>".filterText($row['tema'])."</strong>".($row['createdByEmployeeId'] != DARB_ID ? '. Išsiuntė: '.getAllEmployees($row['createdByEmployeeId']) : '')."</a></li>";//date("Y-m-d H:i:m", $row['data'])//$row['created']
        }
        ?>
        </ul>
    <td><?php if((ADMIN | BUHALT) && $group_id == 0 || $group_id != 0) { ?><a href="?siuntimas=<?php echo $group_id; ?>#new_single_mail" class="no-print fast-action fast-action-add">Rašyti naują laišką</a><?php } ?></td>
</tr><?php
}
?>
</table>

<?php
if(isset($_GET['view'])) {
	$result = db_query("SELECT * FROM `".DB_emails_mass."` WHERE `kindergarten_id`=".DB_ID." AND `ID`=".(int)$_GET['view']);
	if(mysqli_num_rows($result)) {
		$laisk = mysqli_fetch_assoc($result);
		?>
		<fieldset style="margin-top: 40px;" id="mass_mail">
		<legend>Išsiųstas laiškas</legend>
			<p style="margin-bottom: 0px;"><?=date("Y-m-d H:i", $laisk['data'])?>: <strong><?=filterText($laisk['tema'])?></strong></p>
			<p style="padding: 10px; margin-top: 0px;"><?=nl2br(trim(filterText($laisk['tekstas'])))?></p>
			<?php
			if( !empty($laisk['attachments']) ) {
				$laisk['attachments'] = unserialize($laisk['attachments']);
				foreach($laisk['attachments'] as $attachment)
					echo 'Prisegtukas: <a href="'.UPLOAD_DIR.'mass_mail/'.filterText($attachment).'">'.filterText($attachment).'</a><br>';
			}
			?>
			<p>Matomumas: <?=($laisk['isPublic'] ? access_by_group($laisk['group_id']) : 'Asmeninis')?></p>
			<p>Išsiuntė: <?=getAllEmployees($laisk['createdByEmployeeId'])?></p>
		</fieldset>
		<?
	}
}
if(isset($_GET['siuntimas']) && isset($kids_groups[(int)$_GET['siuntimas']])) {
	?>
	<fieldset style="margin-top: 40px;">
	<legend>Naujas laiškas grupei „<?=$kids_groups[(int)$_GET['siuntimas']]?>“</legend>
	<form enctype="multipart/form-data" action="?issiusti=<?php echo (int)$_GET['siuntimas']; ?>" method="post" id="new_single_mail" onsubmit="return checkUpload()">
		<p><label>Laiško tema<span class="required">*</span>: <input type="text" name="tema" style="width: 400px;" autofocus required></label></p>
		<p><label>Prisegtukas: <input type="file" name="attachment[]" id="file" style="width: 400px;" multiple></label></p>
		<p><textarea name="tekstas" style="width: 600px; height: 300px;" required></textarea></p>
		<p><label><?=($_GET['siuntimas'] == 0 ? 'Rodyti visiems darbuotojams (viešas)' : 'Rodyti tik tiems darbuotojams, kuriems leista dirbti su šia grupe (viešesnis)')?>: <input type="checkbox" name="isPublic" value="1"></label></p>
		<p class="notice">Grupinių el. laiškų siuntimas įjungtas. <!-- Šis modulis įjungtas todėl jei siųsite bus realiai išsiųstas grupinis el. laiškas. --> Laiško kopiją gausite į savo el. paštą.</p>
		<p><input type="submit" value="Išsiųsti" class="submit"></p>
		
	</form>
	</fieldset>
	<script>
    function checkUpload() {
    	var totalSize = 0;
    	var files = document.getElementById('file').files;
    	for(var i = 0; i < files.length; ++i) {
			var file = files[i];
			totalSize += file.size;
			if(!file || file.size < 5*1024*1024) {
				//Submit form        
			} else {
				alert('Failas pavadinimu „'+file.name+'“ didesnis negu 5 MB (užima net '+(file.size/1024.0/1024.0).toFixed(2)+' MB). Jo įkelti neleidžiama dėl Jūsų pačių saugumo, nes tėvams gali užimti per daug laiko jį atidaryti, tėvai gali negauti dėl tėvų el. pašto dėžučių apribojimų.\n\nJei tai paveiksliukas išsaugokite jį jpg formatu arba sumažinkite jo raišką, kad jo užimamas dydis sumažėtų. Tada bandykite dar kartą jau su mažesniu failu.');
				//Prevent default and display error
				//evt.preventDefault();
				return false;
			}
		}
		if(totalSize > 15*1024*1024) {
			alert('Prisegami failai bendrai užima daugiau negu 15 MB (užima net '+(totalSize/1024.0/1024.0).toFixed(2)+' MB). Neleidžiama jų išsiųsti dėl Jūsų pačių saugumo, nes tėvai gali negauti jų dėl tėvų el. pašto dėžučių apribojimų, netgi su plačiai naudojamomis el. pašto dėžutėmis tokiomis kaip Gmail, Yahoo, Outlook, nes jos leidžia iki 10–25 MB (be to tėvams gali užimti per daug laiko juos atidaryti).\n\nJei tai paveiksliukai išsaugokite juos jpg formatu arba sumažinkite jų raišką, kad užimamas dydis sumažėtų. Tada bandykite dar kartą jau su mažesniais failais.');
				//Prevent default and display error
				//evt.preventDefault();
				return false;
		}
    }
    </script>
	<?php
}
?>
</div>
