<?php if(!defined('DARBUOT') && !DARBUOT) exit(); /*TODO: tik dietistei */ ?>
<h1>Valgiaraštis <span class="abbr no-print" title="Dietistas, tėvai, auklėtojos, kartais vadovai">(matomumas)</span></h1>
<div id="content">
<?php
/*
Galėtų būti specializuojamas rodymas (reik įvedimo) kiekio gramais: lopšeliui, darželiui ir priešmokykliniam.
Be to kiekiai: kalorijų, baltymų, angliavandenių, vitaminų ir pan.
*/

if(!AUKLETOJA) {
	if(isset($_POST['save'])) {
		db_query("INSERT INTO `".DB_food_menu."` SET 
			`kindergarten_id`=".DB_ID.",
			`date`='".db_fix($_POST['date'])."', `breakfast`='".db_fix($_POST['breakfast'])."',
			`brunch`='".db_fix($_POST['brunch'])."',
			`lunch`='".db_fix($_POST['lunch'])."', `afternoon_tea`='".db_fix($_POST['afternoon_tea'])."',
			`dinner`='".db_fix($_POST['dinner'])."',
			`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
		msgBox('OK', 'Valgiaraščio informacija išsaugota.');
	}
	if(isset($_POST['edit'])) {
		db_query("UPDATE `".DB_food_menu."` SET 
			`date`='".db_fix($_POST['date'])."', `breakfast`='".db_fix($_POST['breakfast'])."',
			`brunch`='".db_fix($_POST['brunch'])."',
			`lunch`='".db_fix($_POST['lunch'])."', `afternoon_tea`='".db_fix($_POST['afternoon_tea'])."',
			`dinner`='".db_fix($_POST['dinner'])."', 
			`updated`=CURRENT_TIMESTAMP, `updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."'
			WHERE `kindergarten_id`=".DB_ID." AND `ID`=".(int)$_POST['edit']);
		msgBox('OK', 'Valgiaraščio informacija atnaujinta.');
	}
	if(isset($_GET['delete'])) {
		db_query("DELETE FROM `".DB_food_menu."` WHERE `kindergarten_id`=".DB_ID." AND `ID`=".(int)$_GET['delete']);
		msgBox('OK', 'Valgiaraščio informacija ištrinta.');
	}
}
$URL = '?';
if(isset($_GET['date']))
	$URL = '?date='.filterText($_GET['date']);
//Moved upper to get date to auto check <select>
$result = db_query("SELECT * FROM `".DB_food_menu."` WHERE `kindergarten_id`=".DB_ID
	.(isset($_GET['date']) ? " AND `date`>='".db_fix($_GET['date'])."' ORDER BY `date` ASC" : ' ORDER BY `date` DESC')
	." LIMIT 0,".(isset($_GET['show15']) ? 15 : 5));
$breakfast = false;
$brunch = false;
$lunch = false;
$afternoon_tea = false;
$dinner = false;
$rows = array();
while($row = mysqli_fetch_assoc($result)) {
	if(!empty($row['breakfast'])) $breakfast = true;
	if(!empty($row['brunch'])) $brunch = true;
	if(!empty($row['lunch'])) $lunch = true;
	if(!empty($row['afternoon_tea'])) $afternoon_tea = true;
	if(!empty($row['dinner'])) $dinner = true;
	$rows[] = $row;
}
if(!isset($_GET['date']))
	$rows = array_reverse($rows);


if(!AUKLETOJA) {
	?><a href="<?=$URL?>&amp;new#menu-form" class="no-print fast-action fast-action-add">Įvesti naują dienos valgiaraštį</a><?php
} ?>
<?=ui_print()?>
<p class="notice">Prireikus valgiaraštis gali būti talpinamas meniu punkte „Bendri dokumentai“.</p><?php //Gali pagalvoti, kad ?>

<div class="no-print">Rodoma 5-ioms dienoms.</div>
<form method="get" class="no-print">
	<div style="float: left; margin-right: 5px; padding: 4px 0px;">Nuo </div><div class="sel" style="float: left; margin-right: 7px;"><select name="date">
	<?php
	$result = db_query("SELECT `date` FROM `".DB_food_menu."` WHERE `kindergarten_id`=".DB_ID." ORDER BY `date` DESC");
	while($row = mysqli_fetch_assoc($result)) {
		echo "<option value=\"".$row['date']."\"".(isset($_GET['date']) && $_GET['date'] == $row['date'] || $rows[0]['date'] == $row['date'] ? ' selected="selected" style="font-weight: bold"' : '').">".$row['date']." ".$dienos_kada[date('N', strtotime($row['date']))]."</option>";
	}
	?>
	</select></div>
	<input type="submit" name="show5" class="filter" value="Rodyti 5 dienoms">
	<input type="submit" name="show15" class="filter" value="Rodyti 15 dienų">
</form>
<?php
echo '<table style="clear: both;">';
echo '<tr>';
foreach($rows as $row) 
	echo '<th class="center">'.$dienos[date('N', strtotime($row['date']))].'</th>';
echo '</tr>';
echo '<tr>';
foreach($rows as $row) 
	echo '<th class="center">'.$row['date'].'</th>';
echo '</tr>';
if($breakfast) {
	echo '<tr>';
	foreach($rows as $row) 
		echo '<th class="center" style="text-transform: uppercase;">Pusryčiai</th>';
	echo '</tr><tr>';
	foreach($rows as $row) 
		echo '<td>'.nl2br(filterText($row['breakfast'])).'</td>';
	echo '</tr>';
}
if($brunch) {
	echo '<tr>';
	foreach($rows as $row) 
		echo '<th class="center" style="text-transform: uppercase;">Priešpiečiai</th>';
	echo '</tr><tr>';
	foreach($rows as $row) 
		echo '<td>'.nl2br(filterText($row['brunch'])).'</td>';
	echo '</tr>';
}
if($lunch) {
	echo '<tr>';
	foreach($rows as $row) 
		echo '<th class="center" style="text-transform: uppercase;">Pietūs</th>';
	echo '</tr><tr>';
	foreach($rows as $row) 
		echo '<td>'.nl2br(filterText($row['lunch'])).'</td>';
	echo '</tr>';
}
if($afternoon_tea) {
	echo '<tr>';
	foreach($rows as $row) 
		echo '<th class="center" style="text-transform: uppercase;">Pavakariai</th>';
	echo '</tr><tr>';
	foreach($rows as $row) 
		echo '<td>'.nl2br(filterText($row['afternoon_tea'])).'</td>';
	echo '</tr>';
}
if($dinner) {
	echo '<tr>';
	foreach($rows as $row) 
		echo '<th class="center" style="text-transform: uppercase;">Vakarienė</th>';
	echo '</tr><tr>';
	foreach($rows as $row) 
		echo '<td>'.nl2br(filterText($row['dinner'])).'</td>';
	echo '</tr>';
}
if(!AUKLETOJA) {
	echo '<tr class="no-print">';
	foreach($rows as $row) 
			echo '<td><a href="'.$URL.'&amp;edit='.$row['ID'].'">Keisti</a> <a href="'.$URL.'&amp;delete='.$row['ID'].'" onclick="return confirm(\'Ar tikrai norite ištrinti valgiaraščio dieną?\')">Trinti</a></td>';
	echo '</tr>';
}
echo '</table>';


if ((isset($_GET['edit']) || isset($_GET['new'])) && !AUKLETOJA) {
	if (isset($_GET['edit'])) {
		$result = db_query("SELECT * FROM `".DB_food_menu."` WHERE `kindergarten_id`=".DB_ID." AND `ID`=".(int)$_GET['edit']);
		$menu = mysqli_fetch_assoc($result);
	}
	?>
	<script>
	//$(function(){
	$( window ).load(function() {
		//Is this focus needed?
		//setTimeout(function() {
			$('#date').focus();
		//}, 50);
	});
	</script>
	<fieldset id="menu-form">
	<legend><?=(isset($_GET['edit']) ? 'Dienos valgiaraščio keitimas' : 'Naujos dienos valgiaraštis')?></legend>
	<form action="<?=$URL?>" method="post" class="not-saved-reminder">
		<p><label>Valgiaraščio data<span class="required">*</span> <input id="date" required="required" class="datepicker" type="text" name="date" value="<?=(isset($menu) ? filterText($menu['date']) : '')?>"></label></p>
		<p><label>Pusryčiai <textarea name="breakfast"><?=(isset($menu) ? filterText($menu['breakfast']) : '')?></textarea></label></p>
		<p><label>Priešpiečiai <textarea name="brunch"><?=(isset($menu) ? filterText($menu['brunch']) : '')?></textarea></label></p>
		<p><label>Pietūs <textarea name="lunch"><?=(isset($menu) ? filterText($menu['lunch']) : '')?></textarea></label></p>
		<p><label>Pavakariai <textarea name="afternoon_tea"><?=(isset($menu) ? filterText($menu['afternoon_tea']) : '')?></textarea></label></p>
		<p><label>Vakarienė <textarea name="dinner"><?=(isset($menu) ? filterText($menu['dinner']) : '')?></textarea></label></p>
		<p><input type="hidden" <?=(!isset($_GET['edit']) ? 'name="save"' : 'name="edit" value="'.(int)$menu['ID'].'"')?>><input type="submit" value="Išsaugoti" class="submit"></p>
	</form>
	</fieldset>
<?php
}
?>
</div>
