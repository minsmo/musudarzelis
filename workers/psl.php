<?php if(!defined('ADMIN')) exit();
?>
<h1>Puslapiai</h1>
<div id="content">
<?php
if(isset($_GET['issaugoti'])) {
	if (!mysqli_query($db_link, "INSERT INTO `psl` SET `pavadinimas`='".db_fix($_POST['pavadinimas'])."', `pavadinimas-url`='".db_fix($_POST['pavadinimas-url'])."', `meta-keywords`='".db_fix($_POST['meta-keywords'])."', `meta-description`='".db_fix($_POST['meta-description'])."', `turinys`='".db_fix($_POST['turinys'])."', `data`='".db_fix($_POST['data'])."'")) {
		logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
	} else echo '<div class="green center">Puslapis išsaugotas!</div>';
}
if(isset($_GET['atnaujinti'])) {
	if (!mysqli_query($db_link, "UPDATE `psl` SET `pavadinimas`='".db_fix($_POST['pavadinimas'])."', `pavadinimas-url`='".db_fix($_POST['pavadinimas-url'])."', `meta-keywords`='".db_fix($_POST['meta-keywords'])."', `meta-description`='".db_fix($_POST['meta-description'])."', `turinys`='".db_fix($_POST['turinys'])."', `data`='".db_fix($_POST['data'])."' WHERE `ID`=".(int)$_GET['atnaujinti'])) {
		logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
	} else echo '<div class="green center">Puslapis atnaujintas!</div>';
}
if(isset($_GET['edit'])) {
	$result = db_query("SELECT * FROM `psl` WHERE `ID`=".(int)$_GET['edit']);
	$psl = mysqli_fetch_assoc($result);
}
?>

<table>
<tr>
	<th>Pavadinimas</th>
	<th>Pavadinimas adresui</th>
	<th>Redagavimo data</th>
	<th>Veiksmai</th>
</tr>
<?php
$result = db_query("SELECT * FROM `psl`");
while ($row = mysqli_fetch_assoc($result)) {
    echo "\t<tr>
	<td>".$row['pavadinimas']."</td>
        <td>".$row['pavadinimas-url']."</td>
        <td>".$row['data']."</td>
	<td><a href=\"?edit=".$row['ID']."\">Redaguoti</a>
    </tr>";	
}
?>
</table>


<fieldset style="margin-top: 40px;">
<legend><?=(isset($_GET['edit']) ? "Puslapio redagavimas" : "Naujas puslapis")?></legend>
<form action="?<?=(!isset($_GET['edit']) ? 'issaugoti' : 'atnaujinti='.(int)$psl['ID'])?>" method="post" id="psl">
    <p><label>Pavadinimas: <input type="text" name="pavadinimas" value="<?=(isset($_GET['edit']) ? $psl['pavadinimas'] : '')?>"></label></p>
    <p><label>Pavadinimas adresui: <input type="text" name="pavadinimas-url" value="<?=(isset($_GET['edit']) ? $psl['pavadinimas-url'] : '')?>"></label></p>
    <p><label>Raktažodžiai: <input type="text" name="meta-keywords" value="<?=(isset($_GET['edit']) ? $psl['meta-keywords'] : '')?>"></label></p>
    <p><label>Meta aprašymas: <input type="text" name="meta-description" value="<?=(isset($_GET['edit']) ? $psl['meta-description'] : '')?>"></label></p>
    <p><label>Turinys:<br><textarea class="wymeditor ckeditor" name="turinys"><?=(isset($_GET['edit']) ? stripslashes($psl['turinys']) : '')?></textarea></label></p>
    <p><label>Keitimo data: <input type="text" name="data" value="<?=(isset($_GET['edit']) ? $psl['data'] : '')?>"></label></p>
    <p><label><input type="submit" class="wymupdate submit" value="Išsaugoti"></label></p>
</form>
</fieldset>
</div>
