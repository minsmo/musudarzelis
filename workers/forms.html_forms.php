<?php
if(isset($_GET['preview'])) {
	$r = db_query("SELECT * FROM `".DB_forms."` WHERE `form_id`=".(int)$_GET['preview']);
	$form = mysqli_fetch_assoc($r);
	$result = db_query("SELECT * FROM `".DB_forms_fields."` WHERE `form_id`=".(int)$_GET['preview']." ORDER BY `order` ASC");
	?>
	<fieldset id="form-fill">
	<legend id="form-preview">Formos „<strong><?=$form['title']?></strong>“ išankstinė peržiūra</legend>
		<p><label>Planavimo laikotarpis nuo<span class="required">*</span>:<br><input class="datepicker" type="text" name="fromDate" placeholder="<?php echo date('Y-m-d'); ?>" required="required"></label></p>
		<p><label>Planavimo laikotarpis iki<span class="required">*</span>:<br><input class="datepicker" type="text" name="toDate" placeholder="<?php echo date('Y-m-d'); ?>" required="required"></label></p>
	<?php
	while($field = mysqli_fetch_assoc($result)) {
		$can_edit = ' (Gali peržiūrėti ir pildyti:<span style="color: #000">';
		$res = db_query("SELECT * FROM `".DB_forms_fields_allowed."` WHERE `field_id`=".(int)$field['field_id'].' AND `action`=0');
		while($allow = mysqli_fetch_assoc($res))
			$can_edit .= ' '.$person_type_who_dgs[$allow['allowed_for_person_type']].';';
		$can_edit .= '</span>)';
		$can_only_view = ' (Gali tik peržiūrėti:<span style="color: #000">';
		$res = db_query("SELECT * FROM `".DB_forms_fields_allowed."` WHERE `field_id`=".(int)$field['field_id'].' AND `action`=1');//
		while($allow = mysqli_fetch_assoc($res))
			$can_only_view .= ' '.$person_type_who_dgs[$allow['allowed_for_person_type']].';';
		$can_only_view .= '</span>)';
		$can = '<span class="notice">'.$can_edit.' '.$can_only_view.'</span>';
	
		if($field['type'] == 0) {//One line
			echo '<p><label><span class="title">'.filterText($field['title']).(!empty($field['help_text']) ? ' <span class="details">('.filterText($field['help_text']).')</span>' : '' ).($field['isRequired'] ? '<span class="required">*</span>' : '').$can.':</span><br><input type="text" name="f['.(int)$field['field_id'].']" style="width: 500px"'.($field['isRequired'] ? ' required="required"' : '').'></label></p>';
		} elseif($field['type'] == 1) {//Paragraph text
			echo '<p><label><span class="title">'.filterText($field['title']).(!empty($field['help_text']) ? ' <span class="details">('.filterText($field['help_text']).')</span>' : '' ).($field['isRequired'] ? '<span class="required">*</span>' : '').$can.':</span><br><textarea name="f['.(int)$field['field_id'].']"'.($field['isRequired'] ? ' required="required"' : '').'></textarea></label></p>';
		} elseif($field['type'] == 2) {//Data
			echo '<p><label><span class="title">'.filterText($field['title']).(!empty($field['help_text']) ? ' <span class="details">('.filterText($field['help_text']).')</span>' : '' ).($field['isRequired'] ? '<span class="required">*</span>' : '').$can.':</span><br><input class="datepicker" type="text" name="f['.(int)$field['field_id'].']" value="" placeholder="'.date('Y-m-d').'"'.($field['isRequired'] ? ' required="required"' : '').'></label></p>';
		} elseif($field['type'] == 3) {//Individual planning
			echo '<p><span class="title">'.filterText($field['title']).(!empty($field['help_text']) ? ' <span class="details">('.filterText($field['help_text']).')</span>' : '' ).$can.'<!-- Individuali veikla <span class="details">(Ugdymo individualizavimas pagal vaiko poreikius, gebėjimus ir kitus ypatumus)</span> -->:</span><br>';
			echo '<div class="sel"><select id="kidId">';
		
			/*if(ADMIN)
				$res = mysqli_query($db_link, "SELECT * FROM `".DB_children."` WHERE `isDeleted`=0 AND `archyvas`=0 AND `parent_kid_id`=0 ORDER BY vardas, pavarde" );
			else
				$res = mysqli_query($db_link, "SELECT `".DB_children."`.*
					FROM `".DB_children."` 
					JOIN `".DB_employees_groups."` ON `".DB_children."`.`grupes_id`=`".DB_employees_groups."`.`grup_id`
					WHERE `".DB_employees_groups."`.darb_id=".(int)DARB_ID." AND `".DB_children."`.`isDeleted`=0 AND `".DB_children."`.`archyvas`=0 AND `".DB_children."`.`parent_kid_id`=0
					ORDER BY vardas, pavarde");//TODO: filter by selected kids group
			*/
			$res = db_query($get_kids_sql);
			while ($row = mysqli_fetch_assoc($res)) {
				echo "<option value=\"".$row['parent_kid_id']."\">".filterText(getName($row['vardas'], $row['pavarde']))."</option>";
			}
			echo '</select></div>
		    <a href="#" id="addScnt">+ Pridėti įvedimo laukelį<!-- šiam vaikui individualios veiklos informaciją --></a></h2>
			<div id="p_scents">';
/*if(isset($_GET['edit'])) {
	$res = mysqli_query($db_link, "SELECT `".DB_children."`.*, `".DB_planning_individual."`.`activity` 
	FROM `".DB_planning_individual."` JOIN `".DB_children."` ON `".DB_children."`.`ID`=`".DB_planning_individual."`.`child_id`
	WHERE `".DB_planning_individual."`.`plan_id`=".(int)$_GET['edit']." ORDER BY vardas, pavarde") or logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
	while ($row = mysqli_fetch_assoc($res)) {
		echo "<p><label>".filterText($row['vardas'])." ".filterText($row['pavarde'])." <input type=\"text\" class=\"individual_plan\" name=\"individuali_veikla[${row['ID']}]\" value=\"".filterText($row['activity'])."\"></label> <a href=\"#\" class=\"remScnt\" id=\"individual${row['ID']}\">- Trinti šią informaciją</a></p>";
	}
}*/
			echo '</div>
			<script type="text/javascript" src="/workers/planning.js?3"></script>';
			/*$res = mysqli_query($db_link, "SELECT `".DB_children."`.*, `".DB_planning_individual."`.`activity` 
			FROM `".DB_planning_individual."` JOIN `".DB_children."` ON `".DB_children."`.`ID`=`".DB_planning_individual."`.`child_id`
			WHERE `".DB_planning_individual."`.`plan_id`=".(int)$plan_view['planning_id']." ORDER BY vardas, pavarde") or logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
			while ($row = mysqli_fetch_assoc($res)) {
				echo "<p><strong>".filterText($row['vardas'])." ".filterText($row['pavarde'])."</strong>: ".filterText($row['activity'])."</p>";
			}*/
			echo '</p>';
		} elseif($field['type'] == 4 && KaunoZiburelis) {
		} elseif($field['type'] >= 10) {
			echo '<p><span class="title">'.filterText($field['title']).(!empty($field['help_text']) ? ' <span class="details">('.filterText($field['help_text']).')</span>' : '' ).$can.':</span></p>';//echo $can;
			//Show from widget
			$result_w = db_query("SELECT * FROM `".DB_forms_widget_fields."` WHERE `widget_id`=".(int)$field['type']." AND `kindergarten_id`=".DB_ID." ORDER BY `order` ASC");
			echo '<table class="plan">
			<tr>';
			//DB_forms_widget_fields_allowed
			//$widget_field_types//0 number //1 text
			while($row = mysqli_fetch_assoc($result_w)) {
				/*$can_edit = ' (Gali peržiūrėti ir pildyti:';
				$res = db_query("SELECT * FROM `".DB_forms_widget_fields_allowed."` WHERE `field_id`=".(int)$row['field_id'].' AND `action`=0');
				while($allow = mysqli_fetch_assoc($res))
					$can_edit .= ' '.$person_type_who_dgs[$allow['allowed_for_person_type']].';';
				$can_edit .= ')';*/
				$can_only_view = ''; //$can_only_view = ' (Gali tik peržiūrėti:';
				$res = db_query("SELECT * FROM `".DB_forms_widget_fields_allowed."`
					WHERE `field_id`=".(int)$row['field_id'].' AND `kindergarten_id`='.DB_ID.' AND `action`=1');//
				while($allow = mysqli_fetch_assoc($res))
					$can_only_view .= ' '.$person_type_who_dgs[$allow['allowed_for_person_type']].';';
				//$can_only_view .= ')';
				//$can = $can_only_view;//$can_edit.' '.$can_only_view;
				echo '<th data-type="'.$row['type'].'" data-name="f['.$field['field_id'].']['.$row['field_id'].'][]"'.(empty($can_only_view) ? '>'.$row['title'] : ' title="Gali tik peržiūrėti:'.$can_only_view.'"><span class="abbr">'.$row['title'].'</span>').'</th>';
			}
			echo '
				<th title="Trynimas eilučių"><span class="abbr">T</span></th>
			<tr>
			</table>
			<a href="#" class="addRow">Pridėti įvedimo eilutę</a>';
		}
	}
	?>
	Išsaugojimo mygtukas matomas planavimą pildantiems darbuotojams.
	</fieldset><div style="height: 200px;"></div>
	<?php
}



if( isset($_GET['new']) || isset($_GET['edit']) ) {//NEW FORM TEMPLATE//Kad būtų aiškiau, kad išsaugoti visai ne tas
	?>
	<script>
	var field = '<div class="field">\
				<a href="#" class="remField">- Trinti šį laukelį</a>\
				<p><label>Laukelio pavadinimas<span class="required">*</span>: <input required="required" type="text" name="title[]"></label></p>\
				<p><label>Laukelio pagalbinis tekstas skliausteliuose: <textarea name="help_text[]"></textarea></label></p>\
				<div><label>Laukelio rūšis <span class="abbr" title="Jei parinkta „Vaiko individualus planavimas“: 1) žemiau esantis punktas „Išsaugoti tuščią laukelį“ negalioja; 2) vienoje planavimo formoje gali būti tik vienas „Vaiko individualus planavimas“.">(išimtys<!--  kai parinkta „Vaiko individualus planavimas“ -->)</span>: <div class="sel"><select name="type[]">\
					<?php
					foreach( $field_types as $key => $value )
						echo "<option value=\"$key\">$value</option>";
					?>
				</select></div></label></div>\
				<div style="clear:left; display: none;" class="additional"><label>Išsaugoti tuščią laukelį: <div class="options"><div class="sel"><select name="isRequired[]"><option value="0">Leisti</option><option value="1">Neleisti</option></select></div></div></label></div>\
				<div style="clear:left"><label>Laukelį gali peržiūrėti ir pildyti:<br><div class="options"><div class="sel"><select name="allowedToEdit[]" class="addEditPersonType1">\
				<option value="" selected="selected" disabled selected hidden>Pasirinkite priskyrimui</option><?php
				foreach($person_type_who as $key => $val)
					if($key != 0/*Parents*/ && $key != 2/*Accountant*/ && $key != 3/*Vadovas*/)// && $key != 6/*Logopedas*/
						echo "<option value=\"".$key."\">".$val."</option>";
				?></select></div></label> <button class="addEditPersonType" style="display: none;">+ Pridėti</button>\
				<input class="editPersonTypeInput" type="hidden" name="editPersonType[]" value=""></div></div>\
				<div style="clear:left"><label>Laukelį gali tik peržiūrėti:<div class="options"><div class="sel"><select name="allowedToView[]" class="addViewPersonType1">\
				<option value="" selected="selected" disabled selected hidden>Pasirinkite priskyrimui</option><?php
				foreach($person_type_who as $key => $val)
					if($key != 2/*Accountant*/ && $key != 3/*Vadovas*/)// && $key != 6/*Logopedas*/
						echo "<option value=\"".$key."\">".$val."</option>";
				?></select></div></label> <button class="addViewPersonType" style="display: none;">+ Pridėti</button>\
				<input class="viewPersonTypeInput" type="hidden" name="viewPersonType[]" value=""></div></div>\
			</div>';
	</script>
	<?php
	if(isset($_GET['edit'])) {
		$result = db_query("SELECT * FROM `".DB_forms."` WHERE `form_id`=".(int)$_GET['edit']);
		$form = mysqli_fetch_assoc($result);
		
		//someone already answered.
		$result = db_query("SELECT * FROM `".DB_forms_fields_answer."` WHERE `form_id`=".(int)$_GET['edit']);
		if(mysqli_num_rows($result) > 0) {
			header("Location: ?editR=".(int)$_GET['edit']);
			//msgBox('ERROR', 'Formos keisti nebegalima, kažkas pagal ją jau užpildė<!-- atsakė --> dokumentą! Jei norite, kad darbuotojai galėtų pradėti pildyti pagal kitokią formą, sukurkite naują formą ir ją paskelbkite.');
		}
	}
	?>
	<fieldset id="forms">
	<legend><?=(isset($_GET['edit']) ? "Keisti planavimo formos sudėtį" : "Nauja planavimo forma")?></legend>
	<form action="#" method="post" id="forms-width" class="not-saved-reminder">
		<p><label>Planavimo formos pavadinimas<span class="required">*</span>: <textarea required="required" name="main_title"><?=(isset($_GET['edit']) ? filterText($form['title']) : '')?></textarea></label></p>
		<p><label>Paskelbta pildymui: <input type="checkbox" name="isPublished" value="1"<?=(isset($_GET['edit']) && $form['isPublished'] ? ' checked="checked"' : '')?>></label></p>
		<a href="#" class="additional-options">Rodyti smulkų nustatymą</a>
		<h3>Planavimo formos įvedimo laukeliai:</h3>
		<?php
		if(!isset($_GET['edit']))
			echo '<p class="notice">Pastaba: Kuriant naują planavimo formą nereikia pridėti dviejų planavimo laikotarpio laukelių (datos nuo ir iki). Jie jau būna įdėti kaip patys pirmieji formos laukeliai. Tai galite pamatyti išsaugoję formą ir atsidarę ją išankstinėje peržiūroje.</p>';
		?>
		<a href="#" class="addField">+ Pridėti įvedimo laukelį</a>
		<div id="sortable">
		<?php
		$field_exists = false;
		if(isset($_GET['edit'])) {
			$result = db_query("SELECT * FROM `".DB_forms_fields."` WHERE `form_id`=".(int)$_GET['edit']." ORDER BY `order` ASC");
			$i = 0;
			while($field = mysqli_fetch_assoc($result)) {
				$field_exists = true;
			?>
			<div class="field">
				<a href="#" class="remField">- Trinti šį laukelį</a>
				<p><label>Laukelio pavadinimas<span class="required">*</span>: <input required="required" type="text" name="title[<?=''//$i?>]" value="<?=(isset($_GET['edit']) ? filterText($field['title']) : '')?>"></label></p>
				<p><label>Laukelio pagalbinis tekstas skliausteliuose<!-- , automatiškai uždedami skliausteliai -->: <textarea name="help_text[<?=''//$i?>]"><?=(isset($_GET['edit']) ? filterText($field['help_text']) : '')?></textarea></label></p>
				<div><label>Įvedimo laukelio rūšis <!-- Pasirinkite įvedimo laukelį --> <span class="abbr" title="Jei parinkta „Vaiko individualus planavimas“, tai žemiau esantis punktas „Išsaugoti tuščią laukelį“ negalioja. Viena planavimo forma gali turėti tik vieną laukelį „Vaiko individualus planavimas“">(išimtys)</span>: <div class="sel"><select name="type[<?=''//$i?>]">
					<?php
					foreach( $field_types as $key => $value )
						echo "<option value=\"$key\"".(isset($_GET['edit']) && $field['type'] == $key ? ' selected="selected"' : '').">$value</option>";
					?>
				</select></div></label></div>
				<div style="clear: left; display: none;" class="additional"><label>Išsaugoti tuščią laukelį<!-- esant tuščiam laukeliui Ar būtina užpildyti laukelį, kad būtų galima išsaugoti -->:<div class="options"><div class="sel"><select name="isRequired[<?=''//$i?>]">
				<option value="0"<?=(isset($_GET['edit']) && !$field['isRequired'] ? ' selected="selected"' : '')?>>Leisti</option><!-- Gal reikia sukeisti vietomis? -->
				<option value="1"<?=(isset($_GET['edit']) && $field['isRequired'] ? ' selected="selected"' : '')?>>Neleisti</option>
				</select></div></div></label></div>
				<div style="clear: left"><label>Laukelį gali peržiūrėti ir pildyti:<div class="options"><div class="sel"><select name="allowedToEdit[]" class="addEditPersonType1">
				<option value="" selected="selected" disabled selected hidden>Pasirinkite priskyrimui</option><?php
				foreach($person_type_who as $key => $val)
					if($key != 0/*Parents*/ && $key != 2/*Accountant*/ && $key != 3/*Vadovas*/)//&& $key != 6/*Logopedas*/
						echo "<option value=\"".$key."\">".$val."</option>";
				?></select></div></label> <button class="addEditPersonType" style="display: none;">+ Pridėti</button>
				<?php
				$res = db_query("SELECT * FROM `".DB_forms_fields_allowed."` WHERE `field_id`=".(int)$field['field_id'].' AND `action`=0');
				$editPersonType = array();
				while($allow = mysqli_fetch_assoc($res)) {
					$editPersonType[] = $allow['allowed_for_person_type'];
					echo "<div style=\"clear: left\">".$person_type_who[$allow['allowed_for_person_type']]." <input class=\"editPersonType\" type=\"hidden\" data-edit-person-type=\"".$allow['allowed_for_person_type']."\"> <a href=\"#\" class=\"remEditPersonType\">- Trinti</a></div>";//name=\"editPersonType[".$i."][]\" value
				}
				echo "<input class=\"editPersonTypeInput\" type=\"hidden\" name=\"editPersonType[]\" value=\"".implode(",", $editPersonType)."\">";
				?>
				</div>
				</div>
				
				<div style="clear: left"><label>Laukelį gali tik peržiūrėti:<div class="options"><div class="sel"><select name="allowedToView[]" class="addViewPersonType1">
				<option value="" selected="selected" disabled selected hidden>Pasirinkite priskyrimui</option><?php
				foreach($person_type_who as $key => $val)
					if($key != 2/*Accountant*/ && $key != 3/*Vadovas*/)// && $key != 6/*Logopedas*/
						echo "<option value=\"".$key."\">".$val."</option>";
				?></select></div></label> <button class="addViewPersonType" style="display: none;">+ Pridėti</button>
				<?php
				$res = db_query("SELECT * FROM `".DB_forms_fields_allowed."` WHERE `field_id`=".(int)$field['field_id'].' AND `action`=1');
				$viewPersonType = array();
				while($allow = mysqli_fetch_assoc($res)) {
					$viewPersonType[] = $allow['allowed_for_person_type'];
					echo "<div style=\"clear: left\">".$person_type_who[$allow['allowed_for_person_type']]." <input class=\"viewPersonType\" type=\"hidden\" data-view-person-type=\"".$allow['allowed_for_person_type']."\"> <a href=\"#\" class=\"remViewPersonType\">- Trinti</a></div>";//name=\"viewPersonType[".$i."][]\" value
				}
				echo "<input class=\"viewPersonTypeInput\" type=\"hidden\" name=\"viewPersonType[]\" value=\"".implode(",", $viewPersonType)."\">";
				?>
				</div>
				</div>
			</div>
			<?php
				++$i;
			}
		}
		?>
		</div>
		<a href="#" class="addField"<?=(isset($_GET['edit']) && !$field_exists || !isset($_GET['edit']) ? ' style="display: none;"' : '')?>>+ Pridėti įvedimo laukelį</a>
		<p><input type="hidden" <?=(!isset($_GET['edit']) ? 'name="save"' : 'name="update" value="'.(int)$form['form_id'].'"')?>><input type="submit" value="Išsaugoti" class="submit"></p>
	</form>
	</fieldset>
<?php
}

if(  isset($_GET['editR']) ) {//Edit rights
	$result = db_query("SELECT * FROM `".DB_forms."` WHERE `form_id`=".(int)$_GET['editR']);
	$form = mysqli_fetch_assoc($result);
	?>
	<fieldset id="forms">
	<legend>Minimaliai patobulinti planavimo formą</legend>
	<form action="#" method="post" id="forms-width" class="not-saved-reminder">
		<?php
		$result = db_query("SELECT * FROM `".DB_forms_fields_answer."` WHERE `form_id`=".(int)$_GET['editR']);
		if(mysqli_num_rows($result) > 0) {
			echo '<div style="background-color: #bcd8bc; padding: 7px;">Pedagogai jau yra užpildę planavimą pagal šią formą. Įstaigoje keičiant planavimo formą iš vienos į kitą reikia sukurti naują planavimo formą sistemoje, o senąją išjungti.</div>';//Užpildę žmonės nesitiki, kad forma pasikeis, todėl formą rekomenduojama keisti tik minimaliai.
			//(savo klaidą)
		}
		?>
		<p><label>Planavimo formos pavadinimas<span class="required">*</span>: <textarea required="required" name="main_title"><?=filterText($form['title'])?></textarea></label></p>
		<p><label>Paskelbta pildymui: <input type="checkbox" name="isPublished" value="1"<?=($form['isPublished'] ? ' checked="checked"' : '')?>></label></p>
		<h3>Planavimo formos įvedimo laukeliai:</h3>
		<div><?php
		// id="sortable"
		$result = db_query("SELECT * FROM `".DB_forms_fields."` WHERE `form_id`=".(int)$_GET['editR']." ORDER BY `order` ASC");
		$i = 0;
		while($field = mysqli_fetch_assoc($result)) {
		?>
		<div class="field">
			<input type="hidden" name="field_id[]" value="<?=filterText($field['field_id'])?>">
			<p><label>Laukelio pavadinimas<span class="required">*</span>: <input required="required" type="text" name="title[<?=''//$i?>]" value="<?=filterText($field['title'])?>"></label></p>
			<p><label>Laukelio pagalbinis tekstas skliausteliuose<!-- , automatiškai uždedami skliausteliai -->: <textarea name="help_text[<?=''//$i?>]"><?=filterText($field['help_text'])?></textarea></label></p>
			<?php if(KESV) { ?>
			<div><label>KV Pasirinkite įvedimo laukelį <span class="abbr" title="Jei parinkta „Vaiko individualus planavimas“, tai žemiau esantis punktas „Išsaugoti tuščią laukelį“ negalioja. Vienoje planavimo formoje gali būti tik vienas „Vaiko individualus planavimas“">(išimtys<!-- kai parinkta „Vaiko individualus planavimas“ -->)</span>: <div class="sel"><select name="type[<?=''//$i?>]">
					<?php
					foreach( $field_types as $key => $value )
						echo "<option value=\"$key\"".($field['type'] == $key ? ' selected="selected"' : '').">$value</option>";
					?>
				</select></div></label></div>
			<?php } ?>
			<div style="clear: left"><label>Laukelį gali <strong>peržiūrėti ir pildyti</strong>:
			<div class="options"><div class="sel"><select name="allowedToEdit[]" class="addEditPersonType1"><option value="" disabled selected hidden>Pasirinkite priskyrimui</option><?php
			foreach($person_type_who as $key => $val)
				if($key != 0/*Parents*/ && $key != 2/*Accountant*/ && $key != 3/*Vadovas*/)//&& $key != 6/*Logopedas*/
					echo "<option value=\"".$key."\">".$val."</option>";
			?></select></div></label> <button class="addEditPersonType" style="display: none;">+ Pridėti</button>
			<?php
			$res = db_query("SELECT * FROM `".DB_forms_fields_allowed."` WHERE `field_id`=".(int)$field['field_id'].' AND `action`=0');
			$editPersonType = array();
			while($allow = mysqli_fetch_assoc($res)) {
				$editPersonType[] = $allow['allowed_for_person_type'];
				echo "<div style=\"clear: left\">".$person_type_who[$allow['allowed_for_person_type']]." <input class=\"editPersonType\" type=\"hidden\" data-edit-person-type=\"".$allow['allowed_for_person_type']."\"> <a href=\"#\" class=\"remEditPersonType\">- Trinti</a></div>";//name=\"editPersonType[".$i."][]\" value
			}
			echo "<input class=\"editPersonTypeInput\" type=\"hidden\" name=\"editPersonType[]\" value=\"".implode(",", $editPersonType)."\">";
			?>
			</div>
			</div>
			
			<div style="clear: left"><label>Laukelį gali <strong>tik peržiūrėti</strong>:<div class="options"><div class="sel"><select class="addViewPersonType1" name="allowedToView[]"><option value="" disabled selected hidden>Pasirinkite priskyrimui</option><?php
			foreach($person_type_who as $key => $val)
				if($key != 2/*Accountant*/ && $key != 3/*Vadovas*/)// && $key != 6/*Logopedas*/
					echo "<option value=\"".$key."\">".$val."</option>";
			?></select></div></label> <button class="addViewPersonType" style="display: none;">+ Pridėti</button>
			<?php
			$res = db_query("SELECT * FROM `".DB_forms_fields_allowed."` WHERE `field_id`=".(int)$field['field_id'].' AND `action`=1');
			$viewPersonType = array();
			while($allow = mysqli_fetch_assoc($res)) {
				$viewPersonType[] = $allow['allowed_for_person_type'];
				echo "<div style=\"clear: left\">".$person_type_who[$allow['allowed_for_person_type']]." <input class=\"viewPersonType\" type=\"hidden\" data-view-person-type=\"".$allow['allowed_for_person_type']."\"> <a href=\"#\" class=\"remViewPersonType\">- Trinti</a></div>";//name=\"viewPersonType[".$i."][]\" value
			}
			echo "<input class=\"viewPersonTypeInput\" type=\"hidden\" name=\"viewPersonType[]\" value=\"".implode(",", $viewPersonType)."\">";
			?>
			</div></div><hr style="clear: left; border-top: 1px solid grey;" height="0">
		</div>
		<?php
			++$i;
		}
		?>
		</div>
		<p><input type="hidden" name="updateR" value="<?=(int)$form['form_id']?>"><input type="submit" value="Išsaugoti" class="submit"></p>
	</form>
	</fieldset>
<?php
}


if(isset($_GET['select'])) {
	$r = db_query("SELECT * FROM `".DB_forms."` WHERE `form_id`=".(int)$_GET['select']);
	$form = mysqli_fetch_assoc($r);
	$result = db_query("SELECT * FROM `".DB_forms_fields."` WHERE `form_id`=".(int)$_GET['select']." ORDER BY `order` ASC");
	?>
	<fieldset id="form-fill">
	<legend id="form-preview">Formos „<strong><?=$form['title']?></strong>“ laukelio pasirinkimas, kurio reikšmė rodoma bendroje lentelėje/sąraše, ten ji vadinama „Papildoma info“</legend>
	<form action="#" method="post" class="not-saved-reminder">
	<div class="sel"><select name="field_id">
	<option value="0">Nerodyti laukelio bendroje lentelėje/sąraše</option>
	<?php
	while($field = mysqli_fetch_assoc($result)) {
		echo '<option value="'.$field['field_id'].'"'.($form['primary_field_id'] == $field['field_id']  ? ' selected="selected"' : '').'>'.filterText($field['title']).'</option>';
	}
	?>
	</select></div><br>
	<input type="hidden" name="updateSelect" value="<?=(int)$_GET['select']?>">
	<input type="submit" value="Išsaugoti" class="submit">
	</form>
	</fieldset><div style="height: 200px;"></div>
	<?php
}
