$(function() {
	$('.addAchievement').on('click', function(event) {
		event.preventDefault();
		var id = $(this).data("id");
		var scntDiv = $('.achievements-list');//(this).next
		$('<p><textarea name="pavadinimas['+id+'][]"></textarea> <a href="#" class="remScnt">- Trinti</a></p>').appendTo(scntDiv);
		//<select name="tipas['+id+'][]"><option class="green" value="0" selected="selected">Išmoko</option><option class="yellow" value="1">Įpusėjo</option><option class="red" value="2">Neišmoko</option></select>
		scntDiv.find('input:last').focus();
	});

	$(document).on('click', '.remScnt', function (event) {
		event.preventDefault();
		$(this).parents('p').remove();
	});
});
