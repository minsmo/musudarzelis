<!DOCTYPE html>
<html dir="ltr" lang="lt-LT">
<head>
	<meta charset="UTF-8">
	<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="X-UA-Compatible" content="chrome=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	-->
	<meta name="viewport" content="width=device-width">
	<title>test</title>
	<link rel="stylesheet" href="/css/jquery-ui-1.11.2.min.css">
	<link rel="stylesheet" href="/css/is-new.css">
	<script>var ie = false;</script>
	<!--[if lt IE 12]>
	<style type="text/css">
	select {width: 100%;}
	.sel {background-image:none;}
	</style>
	<![endif]-->
	<!--[if lt IE 8]>
	<style type="text/css">
	select { width: auto; }
	</style>
	<![endif]-->
	<!--[if lt IE 7]>
	<style type="text/css">
	#nav {position: absolute; top: 0;}
	#content-sidebar {left: 0;}
	#content-wrapper {margin-left: 229px;}
	</style>
	<script>ie = true;if (!window.console) console = {log: function() {}};</script>
	<![endif]-->
	<script type="text/javascript" src="/libs/jquery-1.11.1.min.js"></script><!-- instead of jquery-1.10.2.min.js -->
	<script type="text/javascript" src="/libs/jquery.ui.datepicker-lt.js"></script>
	<script type="text/javascript" src="/libs/jquery-ui-1.11.2.min.js"></script>
	
	
	
	
	
	
	
	<!-- <script src="/libs/x-editable/jquery-editable-poshytip.min.js"></script> 
	<script src="/libs/x-editable/jquery.editable.min.js"></script>
	<script src="/libs/x-editable/jquery.jeditable.mini.js"></script>-->
	<script src="/libs/x-editable/jquery.editinplace.js"></script>
	<script>
	//TODO: REVIEW https://plugins.jquery.com/editable/
	//http://stackoverflow.com/questions/9282387/jquery-inline-edit-on-double-click
	//http://www.appelsiini.net/projects/jeditable/
	//http://vitalets.github.io/x-editable/index.html
	/*$(document).ready(function() {
		//toggle `popup` / `inline` mode
		//$.fn.editable.defaults.mode = 'inline';     
		
		//make username editable
		$('#username').editable();
    });*/
    $(function() {

		/*$("#username").editable("http://www.appelsiini.net/projects/jeditable/php/save.php", { 
			indicator : '<img src="img/indicator.gif">',
			data   : "aa",
			type   : "input",
			submit : "Gerai",
			cancel : 'Atšaukti',
			style  : "inherit",
			submitdata : function() {
				return {id : 2};
			}
		});*/
		$("#username").editInPlace({
			callback: function(unused, enteredText) { return enteredText; },
			// url: './server.php',
			default_text : 'Pridėti tekstą',
			save_button : '<button class="inplace_save">Išsaugoti</button>',
        	cancel_button: '<button class="inplace_cancel">Atšaukti</button>',
			show_buttons: true
		});
	});
	</script>
</head>
<body>
	<a href="#" id="username" data-type="text" data-placement="right" data-title="Enter username"></a>
</body>
</html>
