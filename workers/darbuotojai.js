﻿function validateInput(thisForm) {
    var error="";
    var dataFilter = /^[A-Ž -]{3,60}$/i;
    var pasoNrFilter = /^[0-9]{11}$/;
    var telFilter = /^[0-9+]{7,11}$/;
    //var emailFilter   = /^[-A-Ž0-9~!!$%^&*_=+}{\'?@\.]+$/i;
    var emailFilter = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i; //http://fightingforalostcause.net/misc/2006/compare-email-regex.php

    if(thisForm.vardas.value.length === 0) {
        thisForm.vardas.style.background = 'Yellow';
        error += "Privalo būti darbuotojo vardas užpildytas.\n";
    }
    if(thisForm.pavarde.value.length === 0) {
        thisForm.pavarde.style.background = 'Yellow';
        error += "Privalo būti darbuotojo pavardė užpildyta.\n";
    }

    /*if (thisForm.vardas.value != '' && !dataFilter.test(thisForm.vardas.value)) {
        thisForm.vardas.style.background = 'Yellow';
        error += "Vardas privalo susidaryti tik iš raidžių ir turi būti mažiausiai 3 ir daugiausiai 60 simbolių.\n";
    }

    if (thisForm.pavarde.value != '' && !dataFilter.test(thisForm.pavarde.value)) {
        thisForm.pavarde.style.background = 'Yellow';
        error += "Pavardė privalo susidaryti tik iš raidžių ir turi būti mažiausiai 3 ir daugiausiai 60 simbolių.\n";
    }*/
    if (thisForm.nesiojamas_tel.value !=  '' && !telFilter.test(thisForm.nesiojamas_tel.value)) {
        thisForm.nesiojamas_tel.style.background = 'Yellow';
        error += "Nešiojamas tel. privalo būti susidarytas tik iš skaičių ir jis turi būti sudarytas iš mažiauiai 7 ir daugiausiai 11 simbolių.\n";
    }
    if (thisForm.namu_tel.value !=  '' && !telFilter.test(thisForm.namu_tel.value)) {
        thisForm.namu_tel.style.background = 'Yellow';
        error += "Namų tel. privalo būti susidarytas tik iš skaičių ir jis turi būti sudarytas iš mažiauiai 7 ir daugiausiai 11 simbolių.\n";
    }
    /*if (thisForm.pastas.value !=  '' && !emailFilter.test(thisForm.pastas.value)) {
        thisForm.pastas.style.background = 'Yellow';
        error += "Įvestasis el. pašto adresas yra neteisingas.\n";
    }
    if (thisForm.paso_nr.value !=  '' && !pasoNrFilter.test(thisForm.paso_nr.value)) {
        thisForm.paso_nr.style.background = 'Yellow';
        error += "Blogai įvestas paso nr.\n";
    }*/
 
    if (error != "") {
        alert("Blogai užpildyta:\n" + error);
        return false;
    }

    return true;
}