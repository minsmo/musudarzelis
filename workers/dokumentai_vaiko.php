<?php if(!defined('DARBUOT')) exit();
?>
<h1>Vaiko dokumentai</h1>
<div id="content">
<?php
if(isset($_POST['pridetiNauja'])) {
	if(empty($_POST['vaiko_id'])) {//!ADMIN && 
		msgBox('ERROR', 'Nenurodyta kuriam vaikui priklauso dokumentas. Čia keliami dokumentai tik nurodžius konkretų vaiką. Pirmiau būtina, kad vadovai arba pedagogai įvestų vaikus.');
	} else {
		if(is_uploaded_file($_FILES['file']['tmp_name'])) {
			//if(verify_image($_FILES['file']['tmp_name'])) {
				$photo_pic = $_FILES['file'];
				$photo_ext = strrchr($photo_pic['name'], ".");

				if($photo_pic['size'] >= 1024*1024*5) {
					echo "<div class=\"red center\">Klaida: failas gali užimti iki 5 MB</div>";
				} elseif(!in_array(strtolower($photo_ext), $file_type)) {
					echo "<div class=\"red center\">Klaida: Failo tipas privalo būti iš šių failų tipų: ".implode(', ', $file_type)."</div>";
				} else {
					$photo_new_name = UPLOAD_DIR.(int)$_POST['vaiko_id']."/".$photo_pic['name'];
					if(is_file($photo_new_name)) {//file_exists()
						if(sha1_file($photo_new_name) == sha1_file($photo_pic['tmp_name'])) {
							msgBox('ERROR', 'Failas neišsaugotas. Prie šio vaiko toks dokumentas jau pridėtas, antrą kartą kelti tokį patį nėra prasmės. Jei reikia tik pakeiskite papildomus laukelius prie dokumento.');
						} else {
							msgBox('ERROR', 'Failas neišsaugotas. Prie šio vaiko dokumentas su tokiu pačiu pavadinimu jau yra (sistema netikrina ar toks pats turinys). 1) Jau pridėtas į sistemą toks pats dokumentas, tada dar kartą kelti jo nebereikia. 2) Pridedate kitą dokumentą į sistemą, kurio failo pavadinimas sutampa, tuomet pridedamo pakeiskite failo pavadinimą kompiuteryje ir bandykite dar kartą.');
						}
					} else {
						$do_save = true;
						if(!is_dir(UPLOAD_DIR.(int)$_POST['vaiko_id']))
							if(!mkdir(UPLOAD_DIR.(int)$_POST['vaiko_id'], 0777)) {
								msgBox('ERROR', 'Nepavyko sukurti katalogo');
								$do_save = false;
							}
						if(move_uploaded_file($photo_pic['tmp_name'], $photo_new_name))
							msgBox('OK', 'Failas išsaugotas.');
						else {
							msgBox('ERROR', 'Failas neišsaugotas.');
							$do_save = false;
						}
						//chmod($photo_new_name, 0644);
						if($do_save)
							if(db_query("INSERT INTO `".DB_documents."` SET failo_vardas='".db_fix($photo_pic['name'])."',
								aprasymas='".db_fix($_POST['aprasymas'])."', spec_aprasymas='".db_fix($_POST['spec_aprasymas'])."', 
								tipas=".(int)$_POST['tipas'].", vaiko_id=".(int)$_POST['vaiko_id'].', createdByUserId='.USER_ID.', `createdByEmployeeId`='.DARB_ID))
								echo "<div class=\"green center\">Dokumento informacija išsaugota duomenų bazėje.</div>";
					}
				}
			//} else echo "<div class=\"red center\">Failas nėra paveiksliukas.</div>";
		} else msgBox('ERROR', 'Nėra failo.');
	}
}
if(!empty($_POST['update'])) {
	$result = db_query("SELECT * FROM `".DB_documents."` WHERE ID=".(int)$_POST['update']);
	$row = mysqli_fetch_assoc($result);
	$ok = true;
	if($row['vaiko_id'] != $_POST['vaiko_id']) {
		if(!is_dir(UPLOAD_DIR.(int)$_POST['vaiko_id']))
			if(!mkdir(UPLOAD_DIR.(int)$_POST['vaiko_id'], 0777))
				msgBox('ERROR', 'Nepavyko sukurti katalogo');
		if(rename(UPLOAD_DIR.$row['vaiko_id'].'/'.$row['failo_vardas'], UPLOAD_DIR.(int)$_POST['vaiko_id'].'/'.$row['failo_vardas']))
			msgBox('OK', "Failas sėkmingai perkeltas!");
		else {
			msgBox('ERROR', 'Failo nepavyko perkelti! Dokumento informacija neatnaujinta.');
			$ok = false;
		}
	} else
		$ok = false;
	if($ok) {
		db_query("UPDATE `".DB_documents."` SET aprasymas='".db_fix(htmlspecialchars($_POST['aprasymas'], ENT_QUOTES, 'UTF-8'))."', spec_aprasymas='".db_fix(htmlspecialchars($_POST['spec_aprasymas'], ENT_QUOTES, 'UTF-8'))."', tipas=".(int)$_POST['tipas'].", vaiko_id=".(int)$_POST['vaiko_id'].", `updated`=CURRENT_TIMESTAMP, `updatedByUserId`=".USER_ID.", `updatedByEmployeeId`=".DARB_ID.", `updatedCounter`=`updatedCounter`+1 WHERE ID=".(int)$_POST['update']);
		msgBox('OK', "Sėkmingai atnaujinta.");
	}
}
if(isset($_GET['del_id']) && ADMIN) {
	$row = mysqli_fetch_assoc(db_query("SELECT * FROM `".DB_documents."` WHERE ID=".(int)$_GET['del_id'])) or logdie(mysqli_error($db_link));
	unlink(UPLOAD_DIR.$row['vaiko_id']."/".$row['failo_vardas']);
	if(mysqli_query($db_link, "DELETE FROM `".DB_documents."` WHERE ID=".(int)$_GET['del_id']." LIMIT 1"))
		echo "<div class=\"green center\">Dokumentas ištrintas iš duomenų bazės</div>";
	else logdie("<div class=\"red center\">Klaida ištrinant dokumentą iš duomenų bazės: ".mysqli_error($db_link)."</div>");
}

$show_archived = " AND `".DB_documents."`.`isArchived`=0";
if(isset($_GET['archived'])) {
	$show_archived = " AND `".DB_documents."`.`isArchived`=".(int)$_GET['archived'];//1 OR 0
	if($_GET['archived'] == 2)
		$show_archived = "";
}

if(isset($_GET['archiveIt'])) {
	db_query("UPDATE `".DB_documents."` SET `isArchived`='".(int)$_GET['archiveIt']."' WHERE `ID`=".(int)$_GET['id']);
	msgBox('OK', "Sėkmingai pakeistas archyvavimo lygis.");
}

$URL = "";
if(isset($_GET['archived'])) {
	if(empty($URL))
		$URL = "archived=".(int)$_GET['archived'];
	else
		$URL .= "&amp;archived=".(int)$_GET['archived'];
}
if(isset($_GET['kid_id'])) {
	if(empty($URL))
		$URL = "kid_id=".(int)$_GET['kid_id'];
	else
		$URL .= "&amp;kid_id=".(int)$_GET['kid_id'];
}
$URL = "?".$URL;

?>
<a href="?#document-form" class="no-print fast-action fast-action-add">Naujas vaiko dokumentas</a>
<?php /*Variantas apačioje už brūkšnio rašyti, kad neblaškytų dėmesio if(ADMIN) { ?>
<p class="notice">Pastaba. Failų saugojimas yra papildomai apmokestinamas.</p>
<?php }*/ ?>
<h2>Vaiko dokumentai</h2>
<?php
if(KESV)
	$s = microtime(true);
	
/*$default_limit = 40;
$limit = isset($_GET['limit']) ? (int)$_GET['limit'] : $default_limit;
$return_limit = '';
if($limit != $default_limit) $return_limit = '&amp;limit='.$limit;
$page = isset($_GET['page']) ? (int)$_GET['page'] : 0;*/

if(ADMIN)
	$result = db_query("SELECT cr.* -- , COUNT(*) `cnt`
	FROM `".DB_children."` cr
	-- LEFT JOIN `".DB_children."` fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`<fi.`valid_from` AND cr.`valid_from`<=CURDATE() AND fi.`valid_from`<=CURDATE()
	JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<=CURDATE() GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
	JOIN `".DB_documents."` ON cr.`parent_kid_id`=`".DB_documents."`.vaiko_id
	WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 -- AND fi.`valid_from` IS NULL
	GROUP BY cr.parent_kid_id
	ORDER BY cr.`vardas` ASC, cr.`pavarde` ASC");
else
	$result = db_query("SELECT cr.*
	FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<=CURDATE() GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
	JOIN `".DB_documents."` ON cr.`parent_kid_id`=`".DB_documents."`.vaiko_id
	WHERE cr.`isDeleted`=0 AND cr.`grupes_id`=".GROUP_ID." AND cr.`archyvas`=0
	GROUP BY cr.parent_kid_id
	ORDER BY cr.`vardas` ASC, cr.`pavarde` ASC");
// LIMIT ".$limit." OFFSET ".$limit*$page	

/*<form method="get">
		Rodyti po <input name="limit" type="text" value="<?php echo $limit; ?>" style="width:30px; text-align: center;" onchange="this.form.submit();"><!-- TODO: think -->
</form>*/

if(KESV)
	echo round(microtime(true)-$s, 2);
	//"SELECT cr.*, TIMESTAMPDIFF(YEAR, cr.`gim_data`, CURDATE()) AS age FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<=CURDATE() GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from` AND cr.`isDeleted`=0  ORDER BY cr.`vardas` ASC, cr.`pavarde` ASC"
/*
JOIN `".DB_employees_groups."` ON `".DB_children."`.grupes_id=`".DB_employees_groups."`.`grup_id`
JOIN `".DB_groups."`  ON `".DB_groups."`.`ID`=`".DB_employees_groups."`.`grup_id`
WHERE `".DB_employees_groups."`.`darb_id`=".(int)DARB_ID."
*/
if(mysqli_num_rows($result) > 1) {
  ?>
  <form method="get">
  		<div style="float: left; margin-right: 5px; padding: 4px 0px;">Dabar rodoma:</div>
		<div class="sel" style="float: left; margin-right: 10px;">
		<select name="kid_id">
	  	<option value="">Visų vaikų dokumentai</option><?php
		while($row = mysqli_fetch_assoc($result))
			echo "<option value=\"".$row['parent_kid_id']."\"".(!empty($_GET['kid_id']) && $_GET['kid_id'] == $row['parent_kid_id'] ? ' selected="selected"' : '').">".filterText(getName($row['vardas'], $row['pavarde']))."</option>";// (".$row['cnt'].")
		?>
		</select></div>
		<div class="sel" style="float: left; margin-right: 10px;">
		<select name="archived">
		<option value="">Pasirinkite rodymo kiekį</option><?php
		echo "<option value=\"0\"".(!isset($_GET['archived']) || $_GET['archived'] == 0 ? ' selected="selected"' : '').">Tik dar nearchyvuoti</option>";//galimai svarbūs
		echo "<option value=\"1\"".(!empty($_GET['archived']) && $_GET['archived'] == 1 ? ' selected="selected"' : '').">Tik archyvuoti</option>";
		echo "<option value=\"2\"".(!empty($_GET['archived']) && $_GET['archived'] == 2 ? ' selected="selected"' : '').">Visi</option>";
		?>
		</select></div>
	  <input type="submit" value="Filtruoti" class="filter"><!-- Taikyti filtravimą -->
  </form>
  <?php
}
?>



<table class="vertical-hover">
<tr>
	<th>Dokumentas</th>
	<th>Vaikas</th>
	<th>Rūšis</th>
	<th>Aprašymas</th>
	<th>Aprašymas darbuotojams</th>
	<th>Įkeltas</th>
	<th>Veiksmai</th>
</tr>
<?php
//$dok_edit = array();
if(KESV)
	$s = microtime(true);
if(ADMIN)
	$result = db_query("SELECT cr.*, `".DB_documents."`.*, `".DB_documents."`.ID AS dID
	FROM `".DB_children."` cr 
	LEFT JOIN `".DB_children."` fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`<fi.`valid_from` AND cr.`valid_from`<=CURDATE() AND fi.`valid_from`<=CURDATE()
	-- JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<=CURDATE() GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
	JOIN `".DB_documents."` ON cr.`parent_kid_id`=`".DB_documents."`.vaiko_id
	WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 AND fi.`valid_from` IS NULL$show_archived
	".(!empty($_GET['kid_id']) ? ' AND `'.DB_documents.'`.vaiko_id='.(int)$_GET['kid_id'] : '')."
	ORDER BY `".DB_documents."`.`created` DESC");//'".date('Y-m-d')."'
	// AND fi.`valid_from`<=CURDATE() AND 
	//LEFT JOIN `".DB_children."` <...>
	/*SELECT  `".DB_children."`.*, `".DB_documents."`.*, `".DB_documents."`.ID AS dID 
	FROM `".DB_documents."`
	JOIN `".DB_children."` ON `".DB_children."`.ID=`".DB_documents."`.vaiko_id
	WHERE `".DB_children."`.`isDeleted`=0 AND `".DB_children."`.`archyvas`=0 -- AND `".DB_children."`.`parent_kid_id`=0
	".(!empty($_GET['kid_id']) ? ' AND `'.DB_documents.'`.vaiko_id='.(int)$_GET['kid_id'] : '')."
	ORDER BY `".DB_documents."`.`created` DESC*/
else
	$result = db_query("SELECT cr.*, `".DB_documents."`.*, `".DB_documents."`.ID AS dID
	FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<=CURDATE() GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
	JOIN `".DB_documents."` ON cr.`parent_kid_id`=`".DB_documents."`.vaiko_id
	WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 AND cr.`grupes_id`=".GROUP_ID."$show_archived
	".(!empty($_GET['kid_id']) ? ' AND `'.DB_documents.'`.vaiko_id='.(int)$_GET['kid_id'] : '')."
	ORDER BY `".DB_documents."`.`created` DESC");
if(KESV)
	echo round(microtime(true)-$s, 2);
	/*$result = mysqli_query($db_link, "SELECT  `".DB_children."`.*, `".DB_documents."`.*, `".DB_documents."`.ID AS dID 
	FROM `".DB_documents."`
	JOIN `".DB_children."` ON `".DB_children."`.ID=`".DB_documents."`.vaiko_id
	WHERE `".DB_children."`.grupes_id=".GROUP_ID." AND `".DB_children."`.`isDeleted`=0 AND `".DB_children."`.`archyvas`=0` AND `".DB_children."`.`parent_kid_id`=0".
		(!empty($_GET['kid_id']) ? ' AND `'.DB_documents.'`.vaiko_id='.(int)$_GET['kid_id'] : '')."
	ORDER BY `".DB_documents."`.`created` DESC") or logdie('WWW'.mysqli_error($db_link));*.
	/*
	JOIN `".DB_employees_groups."` ON `".DB_children."`.grupes_id=`".DB_employees_groups."`.`grup_id`
	JOIN `".DB_groups."`  ON `".DB_groups."`.`ID`=`".DB_employees_groups."`.`grup_id`
	WHERE `".DB_employees_groups."`.`darb_id`=".(int)DARB_ID. 
	*/
while($row = mysqli_fetch_assoc($result)) {
	//$file = "../musudarzelis.lt/".UPLOAD_DIR.$row['vaiko_id']."/".$row['failo_vardas'];
	$file = UPLOAD_DIR.$row['vaiko_id']."/".$row['failo_vardas'];
	echo "<tr".(isset($_GET['edit']) && $_GET['edit'] == $row['dID'] ? ' class="opened-row"' : '').">
		<td>".(is_file($file) ? "<a href=\"".filterText($file)."\">".filterText($row['failo_vardas'])."</a>" : 'Nėra failo: '.filterText($row['failo_vardas']) )."</td>
		<td>".filterText(getName($row['vardas'], $row['pavarde']))."</td>
		<td>".$dok_tipai[$row['tipas']]."</td>
		<td>".filterText($row['aprasymas'])."</td>
		<td>".filterText($row['spec_aprasymas'])."</td>
		<td>".date_empty($row['created'])."</td>
		<td><a href=\"".$URL."&amp;edit=".$row['dID']."#document-form\">Keisti</a> <a href=\"".$URL."&amp;archiveIt=".!$row['isArchived']."&amp;id=".$row['dID']."\">".($row['isArchived'] == 1 ? 'Atstatyti' : 'Archyvuoti')."</a> ".(ADMIN ? '<a href="?del_id='.$row['dID'].'" onclick="return confirm(\'Ar tikrai norite ištrinti?\');">Trinti</a>' : '')."</td>
	</tr>";
	if(isset($_GET['edit']) && $_GET['edit'] == $row['dID']) {
		$dok_edit = $row;
	}
}
?>
</table>
<?php
/*
$last = ceil($document_qty/$limit)-1;
if($last > 0) {
	$pagination = '';
	$prev = $page > 0	 ? $page-1 : 0;
	$next = $page < $last ? $page+1 : 0;
	$pagination .= '<a href="'.$_GET['q'].'?page=0'.$return_limit.'"><img src="img/page-first.png" alt="Pirmas"></a> ';
	$pagination .= '<a href="'.$_GET['q'].'?page='.$prev.$return_limit.'"><img src="img/page-prev.png" alt="Ankstesnis"></a> ';
	for($i = 0; $i <= $last; ++$i) {
		if($i==$page)
			$pagination .= '<a href="'.$_GET['q'].'?page='.$i.$return_limit.'"><strong>'.($i+1).'</strong></a> ';
		else
			$pagination .= '<a href="'.$_GET['q'].'?page='.$i.$return_limit.'">'.($i+1).'</a> ';
	}
	$pagination .= '<a href="'.$_GET['q'].'?page='.$next.$return_limit.'">Kitas</a> ';
	$pagination .= '<a href="'.$_GET['q'].'?page='.$last.$return_limit.'">Paskutinis</a>';

	echo $pagination;
}
*/
?>
<fieldset id="document-form">
<legend>Įkelti (naują) vaiko dokumentą</legend>
<form <?php echo (isset($_GET['edit']) ? '' : 'enctype="multipart/form-data" '); ?>method="post" action="<?php echo $URL ?>" class="not-saved-reminder" onsubmit="return checkUpload()">
	<?php if (!isset($_GET['edit'])) { ?>
	<p><label>Dokumentas Jūsų kompiuteryje<span class="required">*</span>: <input required="required" type="file" style="width: 300px;" name="file" id="file"></label></p>
	<?php } ?>
	<div><label>Vaikas<span class="required">*</span>: <div class="sel"><select name="vaiko_id">
		<?php
		$result = db_query($get_kids_sql);
			/*
			JOIN `".DB_employees_groups."` ON `".DB_children."`.grupes_id=`".DB_employees_groups."`.`grup_id`
			JOIN `".DB_groups."`  ON `".DB_groups."`.`ID`=`".DB_employees_groups."`.`grup_id`
			WHERE `".DB_employees_groups."`.`darb_id`=".(int)DARB_ID
			*/
		while ($row = mysqli_fetch_assoc($result))
			echo "<option value=\"".$row['parent_kid_id']."\"".(isset($_GET['edit']) && isset($dok_edit) && $dok_edit['vaiko_id'] == $row['parent_kid_id'] || (!empty($_GET['kid_id']) && $_GET['kid_id'] == $row['parent_kid_id'])  ? ' selected="selected"' : '').">".filterText(getName($row['vardas'], $row['pavarde']))."</option>";
		?>
		</select></div></label></div>
	<div>
		<label>Rūšis<span class="required">*</span>: 
		<div class="sel"><select name="tipas">
		<?php
		foreach($dok_tipai as $id => $val)
			echo "<option value=\"$id\"".(isset($_GET['edit']) && isset($dok_edit) && $dok_edit['tipas'] == $id ? ' selected="selected"' : '').">$val</option>";
		?>
		</select></div>
		</label>
	</div>
	<p><label>Trumpas aprašymas<!-- (raštelio iš gydytojo atveju) -->:<br><input type="text" style="width: 300px;" name="aprasymas" value="<?php if(isset($dok_edit)) echo filterText($dok_edit['aprasymas']); ?>"></label><!-- <span style="text-decoration: line-through">Pastaba: tai ką įvesite rodys ataskaitose kai susiesite dokumentą su nelankytomis dienomis, todėl reikėtų įvesti laikotarpį nuo kada iki kada.</span> --></p>
	 <p><label>Aprašymas matomas tik darbuotojams:<br><input type="text" style="width: 300px;" name="spec_aprasymas" value="<?php if(isset($dok_edit)) echo filterText($dok_edit['spec_aprasymas']); ?>"></label></p>
	<p><input type="hidden" <?=(!isset($_GET['edit']) ? 'name="pridetiNauja"' : 'name="update" value="'.(int)$dok_edit['vaiko_id'].'"')?>><input type="submit" value="Išsaugoti" class="submit"></p>
	<script>
	function checkUpload() {
		var file = document.getElementById('file').files[0];
		if(file && file.size < 2097152) { // 2 MB (this size is in bytes)
			//Submit form		
		} else {
			alert('Failas didesnis negu 2 MB, todėl jo įkelti negalima. Jei tai paveiksliukas išsaugokite jį jpg formatu arba sumažinkite jo raišką, kad jo užimamas dydis sumažėtų. Tada bandykite dar kartą jau su mažesniu failu.');
			//Prevent default and display error
			//evt.preventDefault();
			return false;
		}
	}
	</script>
</form>
</fieldset>
</div>
