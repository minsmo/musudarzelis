$(function() {
	$( "#sortable" ).sortable();
	//$( "#sortable" ).disableSelection();
	var formsDiv = $('#sortable');
	$('.addField').on('click', function(event) {
		event.preventDefault();
		$(field).appendTo(formsDiv);
		formsDiv.children('div:last').find('input:first').focus();
		$('.addField').show();
	});

	$(document).on('click', '.remField', function (event) {
		event.preventDefault();
		$(this).parent('div').remove();
		if(!formsDiv.children().length)
			formsDiv.next().hide();
	});
	
	
	//$('form').submit(function() {
	//});
	
	/*function changeName() {
		$('#sortable > div').each(function(index) {
			$(this).find("input[type='hidden'].editPersonType").attr("name", 'editPersonType['+index+'][]');
			$(this).find("input[type='hidden'].viewPersonType").attr("name", 'viewPersonType['+index+'][]');
		});
	}*/
	$(document).on('click', '.addEditPersonType', function (event) {
		event.preventDefault();
		var value = $(this).prev().find('select').val();
		var parent = $(this).parent();
		if(value && !parent.find('[data-edit-person-type="'+value+'"]').length){//input[value
			$('<div style="clear: left">'+$(this).prev().find('option:selected').text()+' <input class="editPersonType" type="hidden" data-edit-person-type="'+value+'">  <a href="#" class="remEditPersonType">- Trinti</a></div>').appendTo(parent);//name="editPersonType[][]" value="
			DONE("„"+$(this).prev().find('option:selected').text()+"“ buvo pridėtas į sarašą!")
		}
		//changeName();
		var editPersonType = [];
		parent.find('[data-edit-person-type]').each(function(index, element) {//input[value="'+value+'"]
			editPersonType.push($(element).data('edit-person-type'));
		});
		parent.find('.editPersonTypeInput').val(editPersonType.join(','));
	});
	$(document).on('click', '.remEditPersonType', function (event) {
		event.preventDefault();//return false;
		parent = $(this).parent('div').parent();
		$(this).parent().parent().find('option[value='+$(this).prev().attr('data-edit-person-type')+']').show();
		$(this).parent('div').remove();
		var editPersonType = [];
		parent.find('[data-edit-person-type]').each(function(index, element) {//input[value="'+value+'"]
			editPersonType.push($(element).data('edit-person-type'));
		});
		parent.find('.editPersonTypeInput').val(editPersonType.join(','));
	});
	//----------------------
	$(document).on('click', '.addViewPersonType', function (event) {
		event.preventDefault();
		var value = $(this).prev().find('select').val();
		var parent = $(this).parent();
		if(value && !parent.find('[data-view-person-type="'+value+'"]').length){//input[value
			$('<div style="clear: left">'+$(this).prev().find('option:selected').text()+' <input class="viewPersonType" type="hidden" data-view-person-type="'+value+'">  <a href="#" class="remViewPersonType">- Trinti</a></div>').appendTo(parent);//name="viewPersonType[][]" value
			DONE("„"+$(this).prev().find('option:selected').text()+"“ buvo pridėtas į sarašą!");
		}
		//changeName();
		var viewPersonType = [];
		parent.find('[data-view-person-type]').each(function(index, element) {//input[value="'+value+'"]
			viewPersonType.push($(element).data('view-person-type'));
		});
		parent.find('.viewPersonTypeInput').val(viewPersonType.join(','));
	});
	$(document).on('click', '.remViewPersonType', function (event) {
		event.preventDefault();//return false;
		parent = $(this).parent('div').parent();
		$(this).parent().parent().find('option[value='+$(this).prev().attr('data-view-person-type')+']').show();
		$(this).parent('div').remove();
		var viewPersonType = [];
		parent.find('[data-view-person-type]').each(function(index, element) {//input[value="'+value+'"]
			viewPersonType.push($(element).data('view-person-type'));
		});
		parent.find('.viewPersonTypeInput').val(viewPersonType.join(','))
	});
	
	$('.addRow').on('click', function(event) {
		event.preventDefault();
		var size = $(this).prev().find('th').length;
		var tr = $('<tr></tr>');
		var el = $(this).prev().find('th').first();
		for(var i = 0; i < size; ++i) {
			var type = el.data('type');
			if(type == 0)
				tr.append('<td><input name="'+el.data('name')+'" type="number" min="1"></td>');
			else if(type == 1)
				tr.append('<td><textarea name="'+el.data('name')+'"></textarea></td>');
			else
				tr.append('<td><a href="#" class="remRow abbr" style="color: red" title="Trinti šią eilutę" onclick="return confirm(\'Ar tikrai norite ištrinti eilutę?\')">X</a></td>');
			el = el.next();
		}
		$(this).prev().append(tr);
	});
	$(document).on('click', '.remRow', function (event) {
		event.preventDefault();//return false;
		$(this).parents('tr').remove();
	});
	$('.additional-options').on('click', function(event) {
		event.preventDefault();//return false;
		$('.additional').toggle();
		if ($(".additional").is(":hidden"))
			$(this).text('Rodyti smulkų nustatymą');
		else
			$(this).text('Slėpti smulkų nustatymą');
	});
	$(document).on("change", ".addEditPersonType1", function() {
		$(".addEditPersonType").click();
		if($(this).val())
			$(this).find('option[value='+$(this).val()+']').hide();
		$(this).val("");
	});
	$(document).on("change", ".addViewPersonType1", function() {
		$(".addViewPersonType").click();
		if($(this).val())
			$(this).find('option[value='+$(this).val()+']').hide();
		$(this).val("");
	});
	
	$(".options").each(function() {
		$(this).find(".editPersonType").each(function() {
			var id = $(this).attr("data-edit-person-type");
			$(this).parent().parent().find("option[value="+id+"]").hide();
		});
		$(this).find(".viewPersonType").each(function() {
			var id = $(this).attr("data-view-person-type");
			$(this).parent().parent().find("option[value="+id+"]").hide();
		});
	});
});//Pridėkite papildomą
