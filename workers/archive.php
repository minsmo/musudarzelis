<?php
if(!defined('ADMIN') || !ADMIN) exit();
?>
<h1>Archyvinio failo sudarymas (duomenų atsisiuntimui į savo kompiuterį)</h1>
<div id="content">
<?php
if(isset($_POST['order'])) {
	db_query("INSERT INTO `".DB_archive."` SET `kindergarten_id`=".DB_ID.",
			`attendance_report`='".(isset($_POST['attendance_report']) ? 1 : 0)."',
			`attendance_report_type`='".db_fix($_POST['attendance_report_type'])."',
			`planning`='".(isset($_POST['planning']) ? 1 : 0)."', 
			`kids`='".(isset($_POST['kids']) ? 1 : 0)."', 
			`kids_groups`='".(isset($_POST['kids_groups']) ? 1 : 0)."', 
			`kids_progress_and_achievements`='".(isset($_POST['kids_progress_and_achievements']) ? 1 : 0)."',
			`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
	msgBox('OK', 'Sėkmingai užsakyta.');
	$toemail = 'forkik@gmail.com';
	if ( !sendemail("Administratorius", $toemail, 'MusuDarzelis archyvas', 'info@musudarzelis.lt', 'Archyvo generavimo užsakymas', DB_PREFIX."\r\nhttp://dev.musudarzelis.lt/private/tools/archive.php", "plain", "", "ramunerup@gmail.com") )
		logdie("<div class=\"red center\">Jeigu ataskaitų archyvas nebus sugeneruotas per 1 darbo dieną, prašome kreiptis info@musudarzelis.lt pranešant apie ataskaitų archyvo laukimą.</div>");
}

/*$r7 = new HttpRequest($location, HttpRequest::METH_GET);
$r7->setOptions(array('cookies' => $cookies_vma));
$r7->send();//->getBody();
$cookies = $r7->getResponseCookies();
print_r($cookies);
echo $cookies[1]->cookies['Session'];*/
if(!is_dir(UPLOAD_DIR.'archive')) {
	echo system('mkdir -m 777 '.UPLOAD_DIR.'archive');
}
/*
$old = umask(0); 
mkdir($path,0777); 
umask($old); 
//http://stackoverflow.com/questions/7878784/php-mkdir-permissions
*/
//if(!mkdir(UPLOAD_DIR.'archive', 0777))
    //echo "<div class=\"red center\">Nepavyko sukurti katalogo</div>";
$margin = 'print&printArchive&';
if(isset($_GET['generate'])) {
	function remove_forbidden_chars_for_Win_filenames($filename) {
		$bad = array_merge(
        	array_map('chr', range(0,31)),
        	array("<", ">", ":", '"', "/", "\\", "|", "?", "*"));
		return str_replace($bad, '', $filename);
		//http://msdn.microsoft.com/en-us/library/aa365247%28VS.85%29.aspx
		//http://en.wikipedia.org/wiki/Filename#Reserved%5Fcharacters%5Fand%5Fwords
		//http://stackoverflow.com/questions/3380114/strip-bad-windows-filename-characters
	}
	$result = db_query("SELECT * FROM `".DB_archive."` WHERE `archive_id`=".(int)$_GET['generate']);
	if($Arow = mysqli_fetch_assoc($result)) {
		$json = [];
		$wkhtmltopdf = [];
		$result = db_query("SELECT * FROM `".DB_groups."`");
		while ($row = mysqli_fetch_assoc($result))
			$groups[$row['ID']] = $row['pavadinimas'];
		
		//Attendance
		if($Arow['attendance_report']) {
			//$attendance_report_type = 'full';

			$periods = array();
			$result = db_query("SELECT YEAR(`data`) AS `metai`, MONTH(`data`) AS `menuo` FROM `".DB_attendance."` GROUP BY YEAR(`data`) DESC, MONTH(`data`) DESC");
			while ($row = mysqli_fetch_assoc($result))
				$periods[] = $row['metai']."-".$row['menuo'];
		
			foreach($groups as $group_id => $name) {
				foreach($periods as $period) {
					$dest = UPLOAD_DIR.'archive/tabelis.pdf';
					$dest = lithuanianLettersToLatin(remove_forbidden_chars_for_Win_filenames('tabelis_'.$period.'_'.$name.'.pdf'));
		
					$wkhtmltopdf[] = 'wkhtmltopdf -O Landscape --no-background --print-media-type --cookie '.session_name().' '.$_COOKIE[session_name()].' \'http://dev.musudarzelis.lt/tabeliai?data='.$period.'&grupes_id='.$group_id.'&'.$Arow['attendance_report_type']."' '".$dest."' 2>&1\n";// 2>&1
					//https://github.com/shower/shower/wiki/Print

					$json[] = array(
						'url' => 'http://dev.musudarzelis.lt/tabeliai?'.$margin.'data='.$period.'&grupes_id='.$group_id.'&'.$Arow['attendance_report_type'],
						'orientation' => 'landscape',
						'output' => $dest,
					);
				}
			}
			/*
			28 reports (4 groups 7 months)
			real	0m15.655s
		user	0m4.748s
		sys	0m0.680s
			*/
		}
	
		//Planning
		if($Arow['planning']) {
			$result = db_query("SELECT * FROM `".DB_forms_fields_answer."` ORDER BY `toDate` DESC");
			while ($row = mysqli_fetch_assoc($result)) {
				$dest = 'planavimas_'.$row['fromDate'].'-'.$row['toDate'].'_'.lithuanianLettersToLatin(remove_forbidden_chars_for_Win_filenames($groups[$row['group_id']])).'.pdf';
				
				$wkhtmltopdf[] = 'wkhtmltopdf --no-background --print-media-type --cookie '.session_name().' '.$_COOKIE[session_name()].' \'http://dev.musudarzelis.lt/planavimo_formos_pildymas?view='.$row['answer_id'].'#planning_view\' \''.$dest."' 2>&1\n";// 2>&1
				$json[] = array(
					'url' => 'http://dev.musudarzelis.lt/planavimo_formos_pildymas?'.$margin.'view='.$row['answer_id'].'#planning_view',
					'orientation' => 'portrait',
					'output' => $dest,
				);
			}
			
		}
	
		//Kids
		if($Arow['kids']) {
			$result = db_query("SELECT * FROM `".DB_children."` WHERE `isDeleted`=0 ORDER BY vardas, pavarde");//GROUP BY `parent_kid_id` 
			$kids = array();
			while($row = mysqli_fetch_assoc($result)) {
				$kids[$row['ID']] = $row['vardas'].'_'.$row['pavarde'];
				$dest = lithuanianLettersToLatin(remove_forbidden_chars_for_Win_filenames('vaikas_'.$row['vardas'].'_'.$row['pavarde'].'_'.$row['parent_kid_id'].'_'.$row['valid_from'].'.pdf'));
				
				$wkhtmltopdf[] = 'wkhtmltopdf --no-background --print-media-type --cookie '.session_name().' '.$_COOKIE[session_name()].' \'http://dev.musudarzelis.lt/vaikai?edit='.$row['ID'].'\' \''.$dest."' 2>&1\n";// 2>&1
				$json[] = array(
					'url' => 'http://dev.musudarzelis.lt/vaikai?'.$margin.'edit='.$row['ID'],
					'orientation' => 'portrait',
					'output' => $dest,
				);
			}
		}

		//Kids groups
		if($Arow['kids_groups']) {
			foreach($groups as $group_id => $name) {
				$dest = lithuanianLettersToLatin(remove_forbidden_chars_for_Win_filenames('grupes_'.$name.'_vaikai.pdf'));
	
				$wkhtmltopdf[] = 'wkhtmltopdf -O Landscape --no-background --print-media-type --cookie '.session_name().' '.$_COOKIE[session_name()].'  \'http://dev.musudarzelis.lt/vaikai?ieskoti&grupes_id='.$group_id."' '".$dest."' 2>&1\n";// 2>&1
				//https://github.com/shower/shower/wiki/Print
				$json[] = array(
					'url' => 'http://dev.musudarzelis.lt/vaikai?'.$margin.'ieskoti&grupes_id='.$group_id,
					'orientation' => 'landscape',
					'output' => $dest,
				);
			}
		}
	
		//Kids progress and achievements
		if($Arow['kids_progress_and_achievements']) {
			$result = db_query("SELECT * FROM `".DB_children."` JOIN `".DB_achievements."` ON `parent_kid_id`=`vaiko_id`
			WHERE `isDeleted`=0 -- TODO: fix BUG
			GROUP BY `parent_kid_id` ORDER BY vardas, pavarde");
			$kids = array();
			while($row = mysqli_fetch_assoc($result)) {
				$kids[$row['ID']] = $row['vardas'].'_'.$row['pavarde'];
				$dest = lithuanianLettersToLatin(remove_forbidden_chars_for_Win_filenames('vaiko_pazanga_ir_pasiekimai_'.$row['ID'].'_'.$row['vardas'].'_'.$row['pavarde'].'.pdf'));
				
				$wkhtmltopdf[] = 'wkhtmltopdf --no-background --print-media-type --cookie '.session_name().' '.$_COOKIE[session_name()].' \'http://dev.musudarzelis.lt/vaiko_pazanga_ir_pasiekimai?kid_id='.$row['parent_kid_id'].'\' \''.$dest."' 2>&1\n";// 2>&1
				$json[] = array(
					'url' => 'http://dev.musudarzelis.lt/vaiko_pazanga_ir_pasiekimai?'.$margin.'kid_id='.$row['parent_kid_id'],
					'orientation' => 'portrait',
					'output' => $dest,
				);
			}
			/*
			27 kids
			real	0m13.947s
		user	0m3.508s
		sys	0m0.660s
		*/
		}
		
		echo $_COOKIE['MUSUDARZELIS'].'<br>';
		
		echo '<br>'.count($json).'<br><br>';

		echo '<br><div style="height: 200px !important;"><textarea>'.json_encode($json).'</textarea></div><br><br>';
		
	/*
	Total: 87 reports
	real	0m49.696s
	user	0m15.569s
	sys	0m2.208s
	*/
		
	
		//echo escapeshellcmd($ex).'<br>';
		//echo '<br>'.system($ex, $retval);
		//$v = "wkhtmltopdf 'http://dev.musudarzelis.lt/tabeliai?data=2010-11&grupes_id=8&full' ".UPLOAD_DIR.'archive/tabelis.pdf';
		//echo $v;
		//echo '<br>'.system($ex);//> /dev/null &
		//echo '<br>'.exec(UPLOAD_DIR.'archive/sh.sh > /dev/null &');//> /dev/null &
		//echo $retval;
		//echo '<br>'.system('pwd', $retval);
		//echo $retval;
	}
}
?>
<a href="#archive-form" class="no-print fast-action fast-action-add">Naujas ataskaitų archyvo užsakymas</a>

<h2>Atsisiųsti sugeneruotą ataskaitų archyvą:</h2>
<table>
<tr>
	<th>Nr.</th>
	<th>Generavimo data</th>
	<th>Ataskaitų kiekis</th>
	<th>Atsisiųsti</th>
	<th>Užsakė</th>
</tr>
<?php
/*
<tr>
	<td>1</td>
	<td>2014-07-15</td>
	<td>87</td>
	<td><a href="<?=(UPLOAD_DIR.'archive')?>/2014-07-15.zip">Atsisiųsti</a></td>
</tr>
*/

$result = db_query("SELECT * FROM `".DB_archive."` WHERE `kindergarten_id`=".DB_ID." ORDER BY `generated` DESC");
$i = 1;
while ($row = mysqli_fetch_assoc($result)) {
	echo '<tr>
		<td>'.$i++.'</td>';
		if(date_empty($row['generated'])) {
			echo '<td>'.$row['generated'].'</td>
			<td>'.$row['reports'].'</td>
			<td><a href="'.(UPLOAD_DIR.'archive').'/'.$row['url'].'.zip">Atsisiųsti</a></td>';
		} else {
			echo '<td></td>
			<td></td>
			<td>Laukia eilėje</td>';
		}
		echo '<td>'.getAllEmployees($row['createdByEmployeeId']).'</td>';
	echo '</tr>';
}
if($i == 1) echo '<td colspan="100">Nėra užsakytų ataskaitų archyvo generavimų.</td>';
?>
<!--
<tr>
	<td>2</td>
	<td>2014-06-04</td>
	<td>1193</td>
	<td><a href="2014-06-04.zip">Atsisiųsti</a></td>
</tr>
<tr>
	<td>3</td>
	<td>2014-06-05</td>
	<td>1041</td>
	<td><a href="2014-06-05.zip">Atsisiųsti</a></td>
</tr>
-->
</table>

<fieldset id="archive-form">
<legend>Naujas ataskaitų archyvo užsakymas, kad duomenis atsisiųsti į savo kompiuterį</legend>
<form method="post">
	<div><label style="float: left;">Tabelis: <input type="checkbox" name="attendance_report" value="1"></label>
		<div class="sel" style="float: left; margin-left: 5px;"><select name="attendance_report_type">
			<option value="full">Visi stulpeliai</option>
			<option value="svietimui">Pagal švietimą</option>
			<option value="summary">Sutrumpinta</option>
		</select></div>
	</div>
	<p style="clear: left"><label>Planavimas: <input type="checkbox" name="planning" value="1"></label></p>
	<p><label>Vaikai: <input type="checkbox" name="kids" value="1"></label></p>
	<p><label>Grupės vaikai: <input type="checkbox" name="kids_groups" value="1"></label></p>
	<p><label>Vaikų pažanga ir pasiekimai (be žingsnių): <input type="checkbox" name="kids_progress_and_achievements" value="1"></label></p>
	<p><input type="submit" name="order" value="Užsakyti (Padėti į eilę archyvo sukūrimą)" class="submit"> Pastaba: Archyvas sukuriamas per parą, bet gali užtrukti savaitę.</p>
</form>
</fieldset>
</script>
</div>
