<h1>Savo slaptažodžio keitimas</h1>
<div id="content">
<?php
//https://wiki.mozilla.org/The_autocomplete_attribute_and_web_documents_using_XHTML
if($change_password_state == 1) {
	msgBox('OK', 'Slaptažodis sėkmingai pakeistas!');
} elseif($change_password_state == -1) {
	msgBox('ERROR', 'Slaptažodžiai nesutampa!');
} elseif($change_password_state == -2) {
	msgBox('WARN', 'Pabandykite iš naujo. Apsauga nepraeita.');
} elseif($change_password_state == -3) {
	msgBox('WARN', 'Senas/dabartinis slaptažodis ne toks.');
}
?>
<form method="post">
	<p><label>Senas/dabartinis slaptažodis: <input type="password" name="old_pass" value="" autocomplete="off" required="required"></label></p>
	<p><label>Naujas slaptažodis: <input type="password" name="new_pass" value="" autocomplete="off" required="required"></label></p>
	<p><label>Pakartoti naują slaptažodį: <input type="password" name="new_pass2" autocomplete="off" required="required"></label><input type="hidden" name="token" value="<?=$_SESSION['token']?>"></p>
	<p><input type="submit" value="Pakeisti slaptažodį" class="submit"></p>
</form>
</div>
