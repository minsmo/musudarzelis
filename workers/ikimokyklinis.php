<?php if(!defined('DARBUOT') && !DARBUOT) exit(); /*TODO: tik auklėtojoms?
4 variantus
Daugiausiai susidomėjimo iš žmonių sulaukę straipsniai, kurie turėjo daugiau negu 50 mygtuko „Patinka“ paspaudimų.
Straipsniai sulaukę daugiau negu 50 mygtuko „Patinka“ paspaudimų.
Daugiausiai susidomėjimo iš žmonių sulaukę straipsniai.


Naujienos, straipsniai tėvams ir darželių darbuotojams:
http://www.ikimokyklinis.lt/index.php/straipsniai/tevams/10-fraziu-kurios-jusu-vaika-gali-paversti-kompleksuotu-suaugusiuoju/20607
http://www.ikimokyklinis.lt/index.php/naujienos/svietimo-ir-mokslo-naujienos/naujos-informacines-priemones-mokytojams-ir-tevams-ugdantiems-ikimokyklinio-amziaus-vaikus/21210
http://www.ikimokyklinis.lt/index.php/straipsniai/tevams/kaip-padeti-ikimokyklinio-amziaus-vaikui-suprasti-ir-issakyti-savo-emocijas/20579
http://www.ikimokyklinis.lt/index.php/straipsniai/bendri-straipsniai/argumentai-nevartoti-alkoholio-auginant-vaikus/17902
http://www.ikimokyklinis.lt/index.php/straipsniai/bendri-straipsniai/nuo-ketveriu-kodelciuku-amzius/20666
http://www.ikimokyklinis.lt/index.php/straipsniai/specialistams/apie-kokybiska-pedagogo-saveika-su-vaiko-tevais/17553
Ar šie straipsniai/naujienos Jums vertingi?


Naujienos http://www.ikimokyklinis.lt/index.php/naujienos/svietimo-ir-mokslo-naujienos/nauji-reikalavimai-maitinimo-organizavimui-ugdymo-istaigose/18339
http://www.ikimokyklinis.lt/index.php/straipsniai/bendri-straipsniai/igoris-vasiliauskas-apie-tevu-lyderyste-isitraukima-ir-komunikacija-su-tevais-vaiku-darzelyje/20301

Mokomoji medžiaga
Mokomosios priemonės
http://www.ziburelis.lt/
http://www.ikimokyklinis.lt/index.php/straipsniai/dalinames-patirtimi/ikt-panaudojimas-priesmokykliniame-amziuje/11810

SaulyTUČIAI http://www.ikimokyklinis.lt/index.php/biblioteka/edukacines-priemones/animacine-ugdymo-priemone-vaiku-darzeliams-saulytuciai/13201

*/ ?>
<h1><!-- Ikimokyklinis.lt straipsniai, kuriais skaitytojai daugiausiai kartų pasidalino -->Skaitytojų geriausiai įvertinti straipsniai ir naujienos.</h1>
<div id="content">
<?php
if(isset($_GET['do'])) {
	list($type, $id) = explode('.', $_GET['do']);
	db_query("INSERT INTO `1ikimokyklinis_lt` SET 
			`type`='".(int)$type."', `content_id`='".(int)$id."', 
			`kindergarten_id`=".DB_ID.",
			`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
	msgBox('OK', 'Išsaugota.');
}
if(isset($_GET['unDo'])) {
	list($type, $id) = explode('.', $_GET['unDo']);
	db_query("DELETE FROM `1ikimokyklinis_lt` WHERE 
			`type`='".(int)$type."' AND `content_id`='".(int)$id."' AND 
			`kindergarten_id`=".DB_ID." AND `createdByUserId`='".USER_ID."' AND `createdByEmployeeId`='".DARB_ID."'");
	msgBox('OK', 'Pašalinta žymė.');
}
if(isset($_GET['click'])) {
	db_query("INSERT INTO `1ikimokyklinis_lt_click` SET `content_id`='".(int)$_GET['click']."', `kindergarten_id`=".DB_ID.", `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
	$result = db_query("SELECT * FROM `0news` WHERE `id`=".(int)$_GET['click']);
	header('Location: '.mysqli_fetch_assoc($result)['url']);
	die();
}


$marks = [];//
$result = db_query("SELECT * FROM `1ikimokyklinis_lt` WHERE `kindergarten_id`=".DB_ID." AND `createdByEmployeeId`=".DARB_ID);
while($row = mysqli_fetch_assoc($result)) {
	$marks[$row['content_id']][$row['type']] = $row['content'];
}
$result = db_query("SELECT * FROM `0news` WHERE `share_count`>9 ORDER BY `share_count` DESC");
if(mysqli_num_rows($result)) {
	$max = 0;
	?>
	<table>
	<tr>
		<th>Į-<br>ver-<br>tis %</th>
		<th>Jau<br>skai-<br>čiau</th>
		<th>Nuoroda (straipsnio arba naujienos)</th>
		<th style="min-width: 310px">Veiksmai</th>
	</tr>
	<?php
	while($row = mysqli_fetch_assoc($result)) {
		if($max == 0)
			$max = $row['share_count'];
		echo "<tr>
		<td>".round($row['share_count']/$max*100, 0)."</td>
		<td>".(isset($marks[$row['id']][0]) ? 'Skaič' : '')."</td>
		<td><a href=\"?click=".$row['id']."\" target=\"_blank\">".filterText(str_replace('http://www.ikimokyklinis.lt/index.php/', '', $row['url']))."</a></td>
		<td>".(isset($marks[$row['id']][0]) ? "<a href=\"?unDo=0.".$row['id']."\" class=\"highlight\">X skaityta</a> " : "<a href=\"?do=0.".$row['id']."\">Perskaičiau</a> | ")."
		".(isset($marks[$row['id']][1]) ? "<a href=\"?unDo=1.".$row['id']."\" class=\"highlight\">X patinka</a> " : "<a href=\"?do=1.".$row['id']."\">Patinka</a> | ")."
		".(isset($marks[$row['id']][2]) ? "<a href=\"?unDo=2.".$row['id']."\" class=\"highlight\">X naudinga</a> " : "<a href=\"?do=2.".$row['id']."\">Naudinga</a> | ")."
		".(isset($marks[$row['id']][3]) ? "<a href=\"?unDo=3.".$row['id']."\" class=\"highlight\">X praktiška</a> " : "<a href=\"?do=3.".$row['id']."\">Praktiška</a>")."
		</td>
		</tr>";
	}
	echo '</table>';
}
/*
if (isset($_GET['edit']) || isset($_GET['new'])) {
	if (isset($_GET['edit'])) {
		$result = db_query("SELECT * FROM `1reminders` WHERE `kindergarten_id`=".DB_ID.(!ADMIN ? " AND `forEmployeeId`=".DARB_ID : '')." AND `ID`=".(int)$_GET['edit']);
		$reminder = mysqli_fetch_assoc($result);
	}
	?>
	<fieldset id="menu-form">
	<legend><?=(isset($_GET['edit']) ? 'Darbo priminimo keitimas' : 'Naujas darbo priminimas')?></legend>
	<form method="post" class="not-saved-reminder no-print">
		<p><label>Komentaras <input type="checkbox" name="isVisibleToAdmin" value="1" <?=(isset($_GET['edit']) && $reminder['isVisibleToAdmin'] ? 'checked="checked"' : '')?>></label></p>
		<p><input type="hidden" <?=(!isset($_GET['edit']) ? 'name="save"' : 'name="edit" value="'.(int)$reminder['ID'].'"')?>><input type="submit" value="Išsaugoti" class="submit"></p>
	</form>
	</fieldset>
<?php
}*/
?>
</div>
