<?php if(!defined('DARBUOT') && !DARBUOT) exit(); /*TODO: tik auklėtojoms? */ ?>
<h1>Darbų priminimai</h1>
<div id="content">
<?php
if(isset($_POST['save'])) {
//forUserId` int(7) unsigned NOT NULL,
	if(!isset($_POST['priskyrimas']))
		$_POST['priskyrimas'][] = DARB_ID;
	foreach($_POST['priskyrimas'] as $forEmployeeId)
		db_query("INSERT INTO `1reminders` SET 
			`kindergarten_id`=".DB_ID.",
			`forEmployeeId`='".(int)$forEmployeeId."',
			`deadline`='".db_fix($_POST['deadline'])."',
			`activity`='".db_fix($_POST['activity'])."',
			`isVisibleToAdmin`='".(isset($_POST['isVisibleToAdmin']) ? 1 : 0)."',
			`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
	msgBox('OK', 'Darbo priminimas išsaugotas.');
}
if(isset($_POST['edit'])) {
	db_query("UPDATE `1reminders` SET 
		`deadline`='".db_fix($_POST['deadline'])."',
		`activity`='".db_fix($_POST['activity'])."',
		`isVisibleToAdmin`='".(isset($_POST['isVisibleToAdmin']) ? 1 : 0)."',
		`updated`=CURRENT_TIMESTAMP, `updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."',
		`updatedCounter`=`updatedCounter`+1
		WHERE `kindergarten_id`=".DB_ID.(!ADMIN ? " AND `forEmployeeId`=".DARB_ID : '')." AND `ID`=".(int)$_POST['edit']);
	msgBox('OK', 'Darbo priminimas atnaujintas.');
}
if(isset($_GET['delete'])) {
	db_query("DELETE FROM `1reminders` WHERE `kindergarten_id`=".DB_ID.(!ADMIN ? " AND `forEmployeeId`=".DARB_ID : '')." AND `ID`=".(int)$_GET['delete']);
	msgBox('OK', 'Darbo priminimas ištrintas.');
}
if(isset($_GET['completed'])) {
	db_query("UPDATE `1reminders` SET 
		`isCompleted`='1',
		`updated`=CURRENT_TIMESTAMP, `updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."',
		`updatedCounter`=`updatedCounter`+1
		WHERE `kindergarten_id`=".DB_ID.(!ADMIN ? " AND `forEmployeeId`=".DARB_ID : '')." AND `ID`=".(int)$_GET['completed']);
	msgBox('OK', 'Darbo priminimas atnaujintas.');
}

?>
<a href="?new#menu-form" class="no-print fast-action fast-action-add">Naujas priminimas</a> <?=''//ui_print()?>

<?php if(isset($_GET['done'])) { ?>
	| <a href="?" class="no-print fast-action">Nebaigti darbai</a>
<?php } else { ?>
	| <a href="?done" class="no-print fast-action">Baigti darbai</a>
<?php }


$result = db_query("SELECT * FROM `1reminders` WHERE `kindergarten_id`=".DB_ID.' AND `isCompleted`='.(int)isset($_GET['done']).(ADMIN ? ' AND (`forEmployeeId`='.DARB_ID.' OR `forEmployeeId`<>'.DARB_ID.' AND `isVisibleToAdmin`=1)' : " AND `forEmployeeId`=".DARB_ID)." ORDER BY `deadline` DESC");
if(mysqli_num_rows($result)) {
	?>
	<table>
	<caption><?=(isset($_GET['done']) ? 'Baigti darbai:' : 'Nebaigti darbai:')?><caption>
	<tr>
		<th class="date-cell">Padaryti iki</th>
		<th>Darbas</th>
		<th>Sukūrė</th>
		<th>Daro</th>
		<th class="no-print">Veiksmai</th>
	</tr>
	<?php
	while($row = mysqli_fetch_assoc($result)) {
		echo "<tr><td>".filterText($row['deadline'])."</td>
		<td>".nl2br(filterText($row['activity']))."</td>
		<td>".getAllEmployees($row['createdByEmployeeId']).' '.$row['created']."</td>
		<td>".getAllEmployees($row['forEmployeeId'])."</td>
		<td class=\"no-print\"><a href=\"?edit=".$row['ID']."\">Keisti</a> ".(!$row['isCompleted'] ? "<a href=\"?completed=".$row['ID']."\">Jau padarytas</a>" : '')." <a href=\"?delete=".$row['ID']."\" onclick=\"return confirm('Ar tikrai norite ištrinti darbo priminimą?')\">Trinti</a></td></tr>";
	}
	echo '</table>';
}

if (isset($_GET['edit']) || isset($_GET['new'])) {
	if (isset($_GET['edit'])) {
		$result = db_query("SELECT * FROM `1reminders` WHERE `kindergarten_id`=".DB_ID.(!ADMIN ? " AND `forEmployeeId`=".DARB_ID : '')." AND `ID`=".(int)$_GET['edit']);
		$reminder = mysqli_fetch_assoc($result);
	}
	?>
	<fieldset id="menu-form">
	<legend><?=(isset($_GET['edit']) ? 'Darbo priminimo keitimas' : 'Naujas darbo priminimas')?></legend>
	<form method="post" class="not-saved-reminder no-print">
		<p><label>Data iki kada padaryti <!-- <span class="notice abbr" title="Datas rašyti tokiu formatu: META-ME-DI (Pvz., 2015-07-13)">(tvarkingumui)</span> --> <input type="text"class="datepicker" name="deadline" value="<?=(isset($reminder) ? filterText($reminder['deadline']) : '')?>" <?=(!isset($reminder) ? ' autofocus' : '')?>></label></p>
		<p><label>Darbas <textarea name="activity"><?=(isset($reminder) ? filterText($reminder['activity']) : '')?></textarea></label></p>
		<p><label>Matomas vadovams <input type="checkbox" name="isVisibleToAdmin" value="1" <?=(isset($_GET['edit']) && $reminder['isVisibleToAdmin'] ? 'checked="checked"' : '')?>></label></p>
		<?php
		if(ADMIN && isset($_GET['new'])) {
			?>
			<div>
			<div class="sel" style="margin-right: 5px;"><select id="personId">
			<option value="" disabled selected hidden>Pasirinkite darbuotoją priskyrimui</option>
			<?php
			foreach(getValidEmployees() as $key => $value)
				echo "<option value=\"$key\">$value</option>";
			?>
			</select></div> <!-- <button id="addPerson">Priskirti darbuotoją</button> -->
			</div>
			<h3>Darbuotojų sąrašas:</h3>
			<div id="persons" style="clear: left">
			</div>
			<script>
			$(function() {
				var addPerson = $('#persons');
				$('#personId').change(function(event) {
					event.preventDefault();
					var id = $("#personId").val();
					if(id) {
						$("option[value="+id+"]").hide();
						if(!$("#person"+id).length) {
							$('<p><label>'+$("#personId option:selected").text()+' <input type="hidden" name="priskyrimas[]" value="'+id+'"></label> <a href="#" class="remPerson" id="person'+id+'">- Ištrinti susiejimą</a></p>').appendTo(addPerson);
							DONE("Darbuotojas „"+$("#personId option:selected").text()+"“ buvo pridėtas į sarašą!");
						} else
							alert("Šis darbuotojas jau pridėtas.");
					}
					$('#personId').val("");
				});

				$(document).on('click', '.remPerson', function (event) {
					event.preventDefault();
					$(this).parents('p').remove();
					$("option[value="+($(this).parents('p').find("input").val())+"]").show();
				})
	
				$("#personId > option").each(function() {
					if($("#person"+this.value).length)
						$("option[value="+this.value+"]").hide();
				});
			});
			</script>
			<?php
		}
		?>
		<p><input type="hidden" <?=(!isset($_GET['edit']) ? 'name="save"' : 'name="edit" value="'.(int)$reminder['ID'].'"')?>><input type="submit" value="Išsaugoti" class="submit"></p>
	</form>
	</fieldset>
<?php
}
?>
</div>
