<?php if(!defined('DARBUOT')) exit();
abstract class DocumentsCommonPermissions {//Access rights
    const Workers = 0;
    const All = 1;
}
$documents_common_access = [0 => 'Tik darbuotojams', 1 => 'Darbuotojams ir tėvams'];
/*
Įkeltas jau sukurtas dokumentas matomas [/ rašomas puslapis]:
    Vienam žmogui (tėvui, darbuotojams)
    Konkrečiai grupei (tėvui, darbuotojams)
    Konkrečiam grupės tipui (tėvams?)
    visam darželiui (tėvams, darbuotojams)
*/
/*CREATE TABLE IF NOT EXISTS `t_documents_common` (
  `ID` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
  -- `workers_description` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
  -- `tipas` tinyint(1) unsigned NOT NULL,
  -- `vaiko_id` int(7) NOT NULL,
  -- `isArchived` tinyint(1) unsigned NOT NULL,
  `access` tinyint(1) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdByUserId` int(7) unsigned NOT NULL,
  `createdByEmployeeId` int(7) unsigned NOT NULL,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedByUserId` int(7) unsigned NOT NULL,
  `updatedByEmployeeId` int(7) unsigned NOT NULL,
  `updatedCounter` int(4) unsigned NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;*/
?>
<h1>Bendri dokumentai</h1>
<div id="content">
<?php
if(isset($_GET['pridetiNauja'])) {
	if(is_uploaded_file($_FILES['file']['tmp_name'])) {
	    //if(verify_image($_FILES['file']['tmp_name'])) {
	        $photo_pic = $_FILES['file'];
	        $photo_ext = strrchr($photo_pic['name'], ".");

	        if($photo_pic['size'] >= 1024*1024*5) {
	            echo "<div class=\"red center\">Klaida: failas gali užimti iki 2–5 MB</div>";
	        } elseif(!in_array(strtolower($photo_ext), $file_type)) {
	            echo "<div class=\"red center\">Klaida: Failo tipas privalo būti iš šių failų tipų: ".implode(', ', $file_type)."</div>";
	        } else {
				$photo_new_name = UPLOAD_DIR."common/".$photo_pic['name'];
	            if(is_file($photo_new_name)) {//file_exists()
	            	if(sha1_file($photo_new_name) == sha1_file($photo_pic['tmp_name'])) {
	            		msgBox('ERROR', 'Failas neišsaugotas. Toks dokumentas jau pridėtas, antrą kartą kelti tokį patį nėra prasmės. Jei reikia tik pakeiskite papildomus laukelius prie dokumento.');
	            	} else {
	            		msgBox('ERROR', 'Failas neišsaugotas. Dokumentas su tokiu pačiu pavadinimu jau yra (sistema netikrina ar toks pats turinys). 1) Jau pridėtas į sistemą toks pats dokumentas, tada dar kartą kelti jo nebereikia. 2) Pridedate kitą dokumentą į sistemą, kurio failo pavadinimas sutampa, tuomet pridedamo pakeiskite failo pavadinimą kompiuteryje ir bandykite dar kartą.');
	            	}
	            } else {
					$do_save = true;
					if(!is_dir(UPLOAD_DIR."common"/*gal prideti "/" gale? */))
						if(!mkdir(UPLOAD_DIR."common", 0777)) {
							msgBox('ERROR', 'Nepavyko sukurti katalogo');
							$do_save = false;
						}
					if(move_uploaded_file($photo_pic['tmp_name'], $photo_new_name))
			            msgBox('OK', 'Failas išsaugotas.');
			        else {
			        	msgBox('ERROR', 'Failas neišsaugotas.');
						$do_save = false;
					}
					//chmod($photo_new_name, 0644);
					if($do_save)
						if(db_query("INSERT INTO `".DB_documents_common."` SET `kindergarten_id`='".DB_ID."',
							`file_name`='".db_fix($photo_pic['name'])."',
							`description`='".db_fix($_POST['description'])."', `access`='".(int)$_POST['access']."',
							`createdByUserId`=".USER_ID.", `createdByEmployeeId`=".DARB_ID))
							echo "<div class=\"green center\">Dokumento informacija išsaugota duomenų bazėje.</div>";
	            }
	        }
	    //} else echo "<div class=\"red center\">Failas nėra paveiksliukas.</div>";
	} else msgBox('ERROR', 'Nėra failo.');
}
if(!empty($_GET['update'])) {
    if(!mysqli_query($db_link, "UPDATE `".DB_documents_common."` SET `kindergarten_id`='".DB_ID."', `description`='".db_fix(filterText($_POST['description']))."', `access`='".(int)$_POST['access']."', `updated`=CURRENT_TIMESTAMP, `updatedByUserId`=".USER_ID.", `updatedByEmployeeId`=".DARB_ID.", `updatedCounter`=`updatedCounter`+1 WHERE ID=".(int)$_GET['update'])) {
		logdie('Neteisinga užklausa: '.mysqli_error($db_link));
	} else {
		msgBox('OK', "Sėkmingai atnaujinta.");
	}
}
if(isset($_GET['delete'])) {
    $row = mysqli_fetch_assoc(db_query("SELECT * FROM `".DB_documents_common."` WHERE `kindergarten_id`='".DB_ID."' AND `ID`=".(int)$_GET['delete'])) or logdie(mysqli_error($db_link));
    if(ADMIN || $row['createdByEmployeeId'] == DARB_ID) {
		unlink(UPLOAD_DIR."common/".$row['file_name']);
		if(mysqli_query($db_link, "DELETE FROM `".DB_documents_common."` WHERE `kindergarten_id`='".DB_ID."' AND `ID`=".(int)$_GET['delete']." LIMIT 1"))
		    echo "<div class=\"green center\">Dokumentas ištrintas iš duomenų bazės</div>";
		else logdie("<div class=\"red center\">Klaida ištrinant dokumentą iš duomenų bazės: ".mysqli_error($db_link)."</div>");
    }
}
if (isset($_GET['archiveIt'])) {
	db_query("UPDATE `".DB_documents_common."` SET `isArchived`=1 WHERE `kindergarten_id`='".DB_ID."' AND `ID`=".(int)$_GET['archiveIt']);
	msgBox('OK', "Sėkmingai suarchuotas.");
}
if (isset($_GET['unarchiveIt'])) {
	db_query("UPDATE `".DB_documents_common."` SET `isArchived`=0 WHERE `kindergarten_id`='".DB_ID."' AND `ID`=".(int)$_GET['unarchiveIt']);
	msgBox('OK', "Sėkmingai atstatytas.");
}
if (isset($_GET['star'])) {
	db_query("UPDATE `".DB_documents_common."` SET `isStar`=1 WHERE `kindergarten_id`='".DB_ID."' AND `ID`=".(int)$_GET['star']);
	msgBox('OK', "Sėkmingai pažymėtas.");
}
if (isset($_GET['unstar'])) {
	db_query("UPDATE `".DB_documents_common."` SET `isStar`=0 WHERE `kindergarten_id`='".DB_ID."' AND `ID`=".(int)$_GET['unstar']);
	msgBox('OK', "Sėkmingai atžymėtas.");
}
?>
<a href="?#document-form" class="no-print fast-action fast-action-add">Naujas bendras dokumentas</a>
<?php
echo (isset($_GET['archive']) ? ' <a href="?" class="no-print fast-action fast-action-archive">✖ Uždaryti archyvą</a>' : ' <a href="?archive" class="no-print fast-action fast-action-archive">Archyvas</a>');
?>
<h2>Bendri dokumentai</h2>

<table class="vertical-hover">
<tr>
	<th title="Svarba"><span class="abbr">S</span></th>
    <th>Dokumentas</th>
    <th>Aprašymas</th>
    <th>Matomumas</th>
    <th>Įkeltas</th>
    <th>Veiksmai</th>
</tr>
<?php
$result = db_query("SELECT * FROM `".DB_documents_common."` WHERE `kindergarten_id`='".DB_ID."' AND `isArchived`=".(isset($_GET['archive']) ? 1 : 0)." ORDER BY `created` DESC");
while($row = mysqli_fetch_assoc($result)) {
	//$file = "../musudarzelis.lt/".UPLOAD_DIR."common/".$row['file_name'];
	$file = UPLOAD_DIR."common/".$row['file_name'];
    echo "<tr".(isset($_GET['edit']) && $_GET['edit'] == $row['ID'] ? ' class="opened-row"' : '').">
    	<td>".($row['isStar'] ? '<a href="?unstar='.$row['ID'].'" style="color: #006600; text-decoration: none;">★</a>' : '<a href="?star='.$row['ID'].'" style="text-decoration: none;">☆</a>')."</td>
        <td>".(is_file($file) ? "<a href=\"".filterText($file)."\">".filterText($row['file_name'])."</a>" : 'Nėra failo: '.filterText($row['file_name']) )."</td>
        <td>".filterText($row['description'])."</td>
        <td>".$documents_common_access[$row['access']]."</td>
        <td>".date_empty($row['created'])."</td>
        <td><a href=\"?edit=".$row['ID']."#document-form\">Keisti</a> ".($row['isArchived'] ? '<a href="?archive&amp;unarchiveIt='.$row['ID'].'">Grąžinti</a>' : '<a href="?archiveIt='.$row['ID'].'" onclick="return confirm(\'Ar tikrai norite archyvuoti?\')">Archyvuoti</a>').(ADMIN || $row['createdByEmployeeId'] == DARB_ID ? ' <a href="?delete='.$row['ID'].'" onclick="return confirm(\'Ar tikrai norite ištrinti?\');">Trinti</a>' : '')."</td>
    </tr>";
    if(isset($_GET['edit']) && $_GET['edit'] == $row['ID']) {
        $dok_edit = $row;
    }
}
?>
</table>


<fieldset id="document-form">
<legend>Įkelti (naują) bendrą dokumentą</legend>
<form <?php echo (isset($_GET['edit']) ? '' : 'enctype="multipart/form-data" '); ?>method="post" action="?<?php echo (isset($_GET['edit']) ? 'update='.(int)$_GET['edit'] : 'pridetiNauja'); ?>" class="not-saved-reminder" onsubmit="return checkUpload()">
	<?php if (!isset($_GET['edit'])) { ?>
    <p><label>Dokumentas Jūsų kompiuteryje<span class="required">*</span>: <input required="required" type="file" style="width: 300px;" name="file" id="file"></label></p>
    <?php } ?>
    <p><label>Trumpas aprašymas:<br><input type="text" style="width: 530px;" name="description" value="<?php if(isset($dok_edit)) echo filterText($dok_edit['description']); ?>" maxlength="255"></label></p>
    <div>Matomumas<span class="required">*</span>: <div class="sel"><select name="access">
	<?php
		foreach($documents_common_access as $key => $val) {
			echo '<option value="'.$key.'"'.(isset($dok_edit) && $dok_edit['access'] == $key ? ' selected="selected"' : '').'>'.$val.'</option>';
		}
	?>
	</select></div></div>
    <p><input type="submit" value="Išsaugoti" class="submit"></p>
    <script>
    function checkUpload() {
		var file = document.getElementById('file').files[0];
		if(file && file.size < 2097152) { // 2 MB (this size is in bytes)
		    //Submit form        
		} else {
			alert('Failas didesnis negu 2 MB, todėl jo įkelti negalima. Jei tai paveiksliukas išsaugokite jį jpg formatu arba sumažinkite jo raišką, kad jo užimamas dydis sumažėtų. Tada bandykite dar kartą jau su mažesniu failu.');
		    //Prevent default and display error
		    //evt.preventDefault();
		    return false;
		}
    }
    </script>
</form>
</fieldset>
</div>
