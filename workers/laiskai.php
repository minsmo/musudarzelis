<?php if(!defined('DARBUOT')) exit();
?>
<h1>Laiškai (modulis nevystomas ir nenaudojamas)</h1>
<div id="content">
<?php
if (isset($_GET['issiusti'])){
	//TODO: check empty validity and report it
	if(!mysqli_query($db_link, "INSERT INTO `".DB_emails."` SET `kindergarten_id`=".DB_ID.", `vaiko_id`='".(int)$_GET['issiusti']."', `tema`='".db_fix($_POST['tema'])."', 
		`turinys`='".db_fix($_POST['turinys'])."', `data`='".time()."', `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'")) {
		logdie('Neteisinga užklausa: '. mysqli_error($db_link));
	} else {
		$result = mysqli_query($db_link, "SELECT `".DB_users."`.`email` 
		FROM `".DB_children."`
		JOIN `".DB_users_allowed."` ON `".DB_users_allowed."`.`person_id`=`".DB_children."`.`ID`
		JOIN `".DB_users."` ON `".DB_users_allowed."`.`user_id`=`".DB_users."`.`user_id`
		WHERE `ID`=".(int)$_GET['issiusti']) or logdie('2 neteisinga užklausa: '.mysqli_error($db_link));
        msgBox('OK', "Informacija išsaugota!");
		$row = mysqli_fetch_assoc($result);
		//REVIEW:
		if(sendemail("Tėvams", $row['email'], $_SESSION['USER_DATA']['name'].' '.$_SESSION['USER_DATA']['surname'], $_SESSION['USER_DATA']['email'], $_POST['tema'], $_POST['turinys']))
		    msgBox('OK', "El. laiškas išsiųstas į ".$row['email']);
        else
            echo '<div class="red center">El. laiško išsiųsti nepavyko!</div>';
	}
}
?>

<table>
<tr>
	<th>Vardas pavardė</th>
	<th>Išsiųsti laiškai</th>
	<th>Veiksmai</th>
</tr>
<?php
if(ADMIN)
    $result = db_query("SELECT * FROM `".DB_children."`");
else
    $result = db_query("SELECT `".DB_children."`.* FROM `".DB_children."` WHERE `grupes_id`=".GROUP_ID);
    // *,  , `".DB_employees_groups."` WHERE `grup_id` AND `darb_id`=".(int)DARB_ID

if(mysqli_num_rows($result) > 0) {
	while ($row = mysqli_fetch_assoc($result)) {
		echo "\t<tr>
		<td>".filterText($row['vardas'])." ".filterText($row['pavarde'])."</td>
		<td>";
        if(ADMIN)
	    	$rez = db_query("SELECT * FROM `".DB_emails."` WHERE `kindergarten_id`=".DB_ID." AND `vaiko_id`=".$row['ID']);
        else
            $rez = db_query("SELECT * FROM `".DB_emails."` WHERE `kindergarten_id`=".DB_ID." AND `vaiko_id`=".$row['ID']." AND `createdByEmployeeId`=".(int)DARB_ID);
		while ($eil = mysqli_fetch_assoc($rez)) {
			echo "<a href=\"?laisk&amp;laiskas=".$eil['ID']."#single_mail\">".date("Y-m-d H:i:m", $eil['data']).": ".filterText($eil['tema'])."</a><br>";
		}
		echo "</td>
		<td><a href=\"?laisk&amp;siuntimas=".$row['ID']."#new_single_mail\">Naujo laiško siuntimas</a></td>
	</tr>";
	}
}
?>
</table>


<?php
if(isset($_GET['laiskas'])) {
	$result = db_query("SELECT * FROM `".DB_emails."` WHERE `kindergarten_id`=".DB_ID." AND `ID`=".(int)$_GET['laiskas']);
	$laisk = mysqli_fetch_assoc($result);
	$result2 = db_query("SELECT * FROM `".DB_employees."` WHERE `ID`=".(int)$laisk['createdByEmployeeId']);
	$issiunte = mysqli_fetch_assoc($result2);
	?>
	<fieldset style="margin-top: 40px;" id="single_mail">
	<legend>Išsiųstas laiškas</legend>
		<p><?=filterText($issiunte['vardas'])?> <?=filterText($issiunte['pavarde'])?></p>
		<p>Išsiuntė: <?=filterText($issiunte['vardas'])?> <?=filterText($issiunte['pavarde'])?></p>
		<p>Tema: <?=filterText($laisk['tema'])?></p>
		<p><?=filterText($laisk['turinys'])?></p>
	</fieldset>
	<?
}
if(isset($_GET['siuntimas'])) {
	$result = db_query("SELECT * FROM `".DB_children."` WHERE `ID`=".(int)$_GET['siuntimas']);
	$vaik = mysqli_fetch_assoc($result);
	?>
	<fieldset style="margin-top: 40px;">
	<legend>Naujas laiškas</legend>
	<form action="?laisk&amp;issiusti=<?=((int)$_GET['siuntimas'])?>" method="post" id="new_single_mail">
		<p>Kam: <?=filterText($vaik['vardas'])?> <?=filterText($vaik['pavarde'])?></p>
		<p>Laiško tema: <input type="text" name="tema" style="width: 400px;"></p>
		<p><textarea name="turinys" style="width: 600px; height: 300px;"></textarea></p>
		<p><input type="submit" value="Išsiųsti"></p>
	</form>
	</fieldset>
	<?php
}
?>
</div>
