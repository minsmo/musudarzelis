<?php if(!defined('DARBUOT')) exit(); ?>
<h1>Rašymas <!-- pagal formas vaikų grupėms --></h1>
<div id="content">
<?php 
$max_opened_msg_delay = 15;//30 s.
//Pradėti nuo šablono
$grupes = getAllGroups();
$grupes[0] = 'Sau';
$grupes[-3] = 'Sau, vadovams';
	
$form_title = [0 => 'Be formos'];
$result = db_query("SELECT `form_id`, `title` FROM `".DB_docs_templates."` WHERE `kindergarten_id`=".DB_ID);
while ($row = mysqli_fetch_assoc($result))
	$form_title[$row['form_id']] = $row['title'];

$add_filter = '';
if(!ADMIN)
	$add_filter = "(`group_id`=".GROUP_ID." OR (`group_id`=0 OR `group_id`=-3) AND `createdByUserId`=".USER_ID.") AND ";

if(isset($_POST['save']) /*&& !ADMIN*/) {
	db_query("INSERT INTO `".DB_docs."` SET `form_id`='".(int)$_POST['save']."',
		`title`='".db_fix($_POST['title'])."', `content`='".db_fix($_POST['content'])."',
		`group_id`=".(isset(getAllowedGroups()[$_POST['group_id']]) || $_POST['group_id'] == 0 || $_POST['group_id'] == -3 ? $_POST['group_id'] : 0)/*GROUP_ID*/.",
		`allow_parents`=".(isset($_POST['allow_parents']) ? 1 : 0).",
		`kid_id`='".db_fix($_POST['kid_id'])."',
		`date`='".db_fix($_POST['date'])."',
		`kindergarten_id`=".DB_ID.",
		`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
	msgBox('OK', 'Informacija išsaugota sėkmingai.');
}
if(isset($_POST['edit'])) {
	$result = db_query("SELECT * FROM `".DB_docs."` WHERE `kindergarten_id`=".DB_ID." AND `answer_id`=".(int)$_POST['edit']);
	$answer = mysqli_fetch_assoc($result);
	if(isset($answer['createdByEmployeeId']) &&
		(ADMIN || (isset($answer['group_id']) && (isset(getAllowedGroups()[$answer['group_id']]) || ($answer['group_id'] == 0 || $answer['group_id'] == -3) && $answer['createdByEmployeeId'] == DARB_ID)))
	) {
		db_query("INSERT INTO `".DB_docs."` SET `form_id`='".(int)$answer['form_id']."',
		`title`='".db_fix($_POST['title'])."', `content`='".db_fix($_POST['content'])."',
		`group_id`=".(ADMIN || $answer['createdByEmployeeId'] == DARB_ID ? (isset(getAllowedGroups()[$_POST['group_id']]) || $_POST['group_id'] == 0 || $_POST['group_id'] == -3 ? ($_POST['group_id']) : 0) : $answer['group_id'])/*GROUP_ID*/.",
		`allow_parents`=".(isset($_POST['allow_parents']) ? 1 : 0).",
		`kid_id`='".db_fix(ADMIN || $answer['createdByEmployeeId'] == DARB_ID ? $_POST['kid_id'] : $answer['kid_id'])."',
		`date`='".db_fix($_POST['date'])."',
		`kindergarten_id`=".DB_ID.",
		`created`='".$answer['created']."', `createdByUserId`='".$answer['createdByUserId']."', `createdByEmployeeId`='".$answer['createdByEmployeeId']."', `updated`=CURRENT_TIMESTAMP(),
		`from_answer_id`='".(int)$_POST['edit']."'");
		$new_opened_answer_id = (int)mysqli_insert_id($db_link);
		db_query("UPDATE `".DB_docs."` SET
			-- `date`='".db_fix($_POST['date'])."', `content`='".db_fix($_POST['content'])."', `title`='".db_fix($_POST['title'])."',
			-- ".(ADMIN || $answer['createdByEmployeeId'] == DARB_ID ? "`group_id`=".(isset(getAllowedGroups()[$_POST['group_id']]) || $_POST['group_id'] == 0 || $_POST['group_id'] == -3 ? $_POST['group_id'] : 0)/*GROUP_ID*/."," : '')."
			-- `allow_parents`=".(isset($_POST['allow_parents']) ? 1 : 0).",
			-- ".(ADMIN || $answer['createdByEmployeeId'] == DARB_ID ? "`kid_id`=".(int)$_POST['kid_id'].',' : '')."
			`updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."', `updated`=CURRENT_TIMESTAMP(), `updatedCounter`=`updatedCounter`+1,
			`openedByUserId`=NULL, `openedByEmployeeId`=NULL, `openedDate`=NULL,
			`isOld`='1'
			WHERE `kindergarten_id`=".DB_ID." AND `answer_id`='".(int)$_POST['edit']."'");
		msgBox('OK', 'Informacija atnaujinta sėkmingai.');
	}
}

if(isset($_GET['delete'])) {
	$answer_id = (int)$_GET['delete'];
	$result = db_query("SELECT `createdByEmployeeId` FROM `".DB_docs."` WHERE `kindergarten_id`=".DB_ID." AND `answer_id`='".$answer_id."'");
	$row = mysqli_fetch_assoc($result);
	if($row['createdByEmployeeId'] == DARB_ID || ADMIN) {//Can be made more optimized
		db_query("UPDATE `".DB_docs."` SET `isDeleted`=1 WHERE `kindergarten_id`=".DB_ID." AND `answer_id`='".$answer_id."'");
		msgBox('OK', 'Sėkmingai ištrinta!');
	}
}
if(isset($_GET['undelete'])) {
	$answer_id = (int)$_GET['undelete'];
	$result = db_query("SELECT `createdByEmployeeId` FROM `".DB_docs."` WHERE `kindergarten_id`=".DB_ID." AND `answer_id`='".$answer_id."'");
	$row = mysqli_fetch_assoc($result);
	if($row['createdByEmployeeId'] == DARB_ID || ADMIN) {//Can be made more optimized
		db_query("UPDATE `".DB_docs."` SET `isDeleted`=0 WHERE `kindergarten_id`=".DB_ID." AND `answer_id`='".$answer_id."'");
		msgBox('OK', 'Sėkmingai atstatyta!');
	}
}

if(isset($_GET['archiveIt'])) {
	$answer_id = (int)$_GET['archiveIt'];
	$result = db_query("SELECT `createdByEmployeeId` FROM `".DB_docs."` WHERE `kindergarten_id`=".DB_ID." AND `answer_id`='".$answer_id."'");
	$row = mysqli_fetch_assoc($result);
	if($row['createdByEmployeeId'] == DARB_ID || ADMIN) {//Can be made more optimized
		db_query("UPDATE `".DB_docs."` SET `isArchived`=1 WHERE `kindergarten_id`=".DB_ID." AND (`answer_id`='".$answer_id."' OR `from_answer_id`='".$answer_id."')");
		msgBox('OK', 'Sėkmingai archyvuota.');
	}
}
if(isset($_GET['unarchive'])) {
	$answer_id = (int)$_GET['unarchive'];
	$result = db_query("SELECT `createdByEmployeeId` FROM `".DB_docs."` WHERE `kindergarten_id`=".DB_ID." AND `answer_id`='".$answer_id."'");
	$row = mysqli_fetch_assoc($result);
	if($row['createdByEmployeeId'] == DARB_ID || ADMIN) {//Can be made more optimized
		db_query("UPDATE `".DB_docs."` SET `isArchived`=0 WHERE `kindergarten_id`=".DB_ID." AND (`answer_id`='".$answer_id."' OR `from_answer_id`='".$answer_id."')");
		msgBox('OK', 'Sėkmingai atstatyta.');
	}
}

//URL to remember
$URL = '?';
if( isset($_GET['trash']) )
	$URL .= 'trash&amp;';
if( isset($_GET['old']) )
	$URL .= 'old&amp;';
if( isset($_GET['archive']) )
	$URL .= 'archive&amp;';
/*if( isset($_GET['group_id']) )
	$URL .= 'group_id='.(int)$_GET['group_id'].'&amp;';
else
	$_GET['group_id'] = 0;*/



$kids = [0 => ''];
//DRY: dokumentai_vaiko.php
$result = db_query($get_kids_sql);
while ($row = mysqli_fetch_assoc($result))
	$kids[$row['parent_kid_id']] = filterText(getName($row['vardas'], $row['pavarde']));

if(ADMIN)
	$result = db_query("SELECT *, UNIX_TIMESTAMP(`openedDate`) AS `openedDateTimestamp` FROM `".DB_docs."`
	WHERE `isDeleted`=".(isset($_GET['trash']) ? 1 : 0)." AND `isOld`".(isset($_GET['old']) ? '<>0' : '=0')." AND `isArchived`".(isset($_GET['archive']) ? '<>0' : '=0')." AND `kindergarten_id`=".DB_ID." AND (`group_id`<>0 OR `group_id`=0 AND `createdByUserId`='".USER_ID."')
	"/*.(isset($_GET['group_id']) && $_GET['group_id'] > 0 ? ' AND `group_id`='.(int)$_GET['group_id'] : '')*/."
	ORDER BY `date` DESC");
else
	$result = db_query("SELECT *, UNIX_TIMESTAMP(`openedDate`) AS `openedDateTimestamp` FROM `".DB_docs."`
		WHERE `isDeleted`=".(isset($_GET['trash']) ? 1 : 0)." AND `isOld`".(isset($_GET['old']) ? '<>0' : '=0')." AND `isArchived`".(isset($_GET['archive']) ? '<>0' : '=0')." AND `kindergarten_id`=".DB_ID." AND (`group_id`=".GROUP_ID." OR (`group_id`=0 OR `group_id`=-3) AND `createdByUserId`='".USER_ID."')
		ORDER BY `".DB_docs."`.`date` DESC");

if(ADMIN)
	$trashCnt = mysqli_fetch_assoc(db_query("SELECT COUNT(*) as `cnt` FROM `".DB_docs."` WHERE `isDeleted`=1 AND `isOld`=0 AND `isArchived`=0 AND `kindergarten_id`=".DB_ID." AND (`group_id`<>0 OR `group_id`=0 AND `createdByUserId`='".USER_ID."')"))['cnt'];
else
	$trashCnt = mysqli_fetch_assoc(db_query("SELECT COUNT(*) as `cnt` FROM `".DB_docs."` WHERE `isDeleted`=1 AND `isOld`=0 AND `isArchived`=0 AND `kindergarten_id`=".DB_ID." AND (`group_id`=".GROUP_ID." OR (`group_id`=0 OR `group_id`=-3) AND `createdByUserId`='".USER_ID."')"))['cnt'];


//if(!ADMIN) {
	?><a href="?new_fill#form-fill" class="no-print fast-action fast-action-add">Naujas dokumentas</a> <?php
//}


echo (isset($_GET['trash']) ? '<a href="'.str_replace('trash&amp;', '', $URL).'" class="no-print fast-action fast-action-delete">✖ Uždaryti šiukšlinę</a>' : '<a href="'.$URL.'trash" class="no-print fast-action fast-action-delete">Šiukšlinė'.($trashCnt > 0 ? ' ('.$trashCnt.')' : '').'</a>');//✖  Išmesti, perkelti į šiukšlinę, nerodyti, slėpti
echo (isset($_GET['old']) ? ' <a href="'.str_replace('old&amp;', '', $URL).'" class="no-print fast-action fast-action-archive">✖ Uždaryti kopijas</a>' : ' <a href="'.$URL.'old" class="no-print fast-action fast-action-archive">Senos kopijos</a>');
echo (isset($_GET['archive']) ? ' <a href="'.str_replace('archive&amp;', '', $URL).'" class="no-print fast-action fast-action-archive">✖ Uždaryti archyvą</a>' : ' <a href="'.$URL.'archive" class="no-print fast-action fast-action-archive">Archyvas</a>');
echo ui_print();



/*
if(ADMIN) {
	?>
	<form method="get" class="no-print">
		<input type="hidden" name="autofocus">
		<div class="sel" style="float: left"><select name="group_id" onchange="this.form.submit()" <?=(isset($_GET['autofocus']) ? 'autofocus' : '')?>>
		<?php
		foreach($grupes as $ID => $title)
			if($ID == 0)
				echo "<option value=\"".$ID."\"".(isset($_GET['group_id']) && $ID == $_GET['group_id'] ? ' selected="selected" class="selected">'.$selectedMark : '>')."Visos grupės</option>";
			else
				echo "<option value=\"".$ID."\"".(isset($_GET['group_id']) && $ID == $_GET['group_id'] ? ' selected="selected" class="selected">'.$selectedMark : '>').$title."</option>";
		?>
		</select></div>
	</form>
	<?php
}*/
	
if(mysqli_num_rows($result) > 0) {
	?>
	<table class="vertical-hover<?php if(isset($_GET['view'])) echo ' no-print'; ?>">
	<tr>
		<th class="date-cell">Data <span title="Nuo naujausios datos iki vėliausios">↓</span></th>
		<th>Pavadinimas</th>
		<th>Rodymas<br>(grupei ar sau)</th>
		<th>Vaikas</th>
		<th>Formos pavadinimas</th>
		<th>Sukūrė</th>
		<th>Atnaujino</th>
		<th class="no-print">Veiksmai</th>
	</tr>
	<?php
	$opened_answer_id = 0;//opened-row
	if(isset($_GET['view']))
		$opened_answer_id = (int)$_GET['view'];
	if(isset($_GET['edit']))
		$opened_answer_id = (int)$_GET['edit'];
	if(isset($_POST['edit']))
		$opened_answer_id = (int)$_POST['edit'];
	if(isset($new_opened_answer_id))
		$opened_answer_id = $new_opened_answer_id;
	while ($row = mysqli_fetch_assoc($result)) {
		echo "		<tr".($opened_answer_id == $row['answer_id'] ? ' class="opened-row"' : '').">
		<td>".$row['date']."</td>
		<td>".filterText($row['title'])."</td>
		<td>".(isset($grupes[$row['group_id']]) ? $grupes[$row['group_id']] : '').($row['allow_parents'] && ($row['group_id'] > 0 || $row['kid_id'] > 0) ? ($row['kid_id'] > 0 ? ' ir tik vaiko tėvams' : ' ir šios grupės tėvams') : '')."</td>
		<td>".(isset($kids[$row['kid_id']]) ? $kids[$row['kid_id']] : '')."</td>
		<td>".$form_title[$row['form_id']]."</td>
		<td>".substr($row['created'], 0, -3)." ".getAllEmployees($row['createdByEmployeeId'])."</td>
		<td>".substr(date_empty($row['updated']), 0, -3)." ".getAllEmployees($row['updatedByEmployeeId'])."</td>
		<td class=\"no-print\"><a href=\"".$URL."view=".$row['answer_id']."#planning-view\">Peržiūrėti</a> ";
		if(!isset($_GET['old'])) {
			if(ADMIN /*|| isset($EditLinkAllowed[$row['form_id']])*/
				|| (isset($row['group_id']) 
					&& (isset(getAllowedGroups()[$row['group_id']]) || ($row['group_id'] == 0 || $row['group_id'] == -3) && $row['createdByEmployeeId'] == DARB_ID))) {
				
				if($row['openedDateTimestamp'] < time()-$max_opened_msg_delay)
					echo " <a href=\"".$URL."edit=".$row['answer_id']."#form-fill\">Keisti</a> ";
				else
					echo ' [Dabar redaguoja '.getAllEmployees($row['openedByEmployeeId'])." ".$row['openedDate']."]";//BUG gaunasi kai redaguoja abu, jeigu atidaro keli žmonės tai kadangi viena atidarymo data tai konkuruoja tada ta data tai vieno tai kito: <a href=\"".$URL."edit=".$row['answer_id']."#form-fill\" onclick=\"return confirm('Ar tikrai norite keisti? Tai dažniausiai pavojinga, nes Jūs nematysite tiesiogiai ką keičia kitas žmogus. O išsaugant duomenis tas kas išsaugos duomenis paskutinis to duomenys užsirašys ant viršaus, o tarpinio išsaugojimo dalis duomenų bus panaikinti.')\">Keisti</a>
			}
		}
		if($row['createdByEmployeeId'] == DARB_ID || ADMIN) {
			echo " ".(isset($_GET['trash']) ? "<a href=\"".$URL."undelete=".$row['answer_id']."\">Atstatyti</a>" : "<a href=\"".$URL."delete=".$row['answer_id']."\" onclick=\"return confirm('Ar tikrai norite perkelti į šiukšlinę dokumentą?')\">Trinti</a>")." ";
			echo " ".(isset($_GET['archive']) ? "<a href=\"".$URL."unarchive=".$row['answer_id']."\">Išarchyvuoti</a>" : "<a href=\"".$URL."archiveIt=".$row['answer_id']."\" onclick=\"return confirm('Ar tikrai norite archyvuoti?')\">Archyvuoti</a>")." ";
		}
		echo "
		</td>
	</tr>";	
	}
	?>
	</table>
	<?php
}

if(isset($_GET['new_fill']) /*&& !ADMINoptional*/) { ?>
	<fieldset id="form-fill">
	<legend>Naujas dokumentas</legend>
	<form method="get">
		<?php
		$result = db_query("SELECT * FROM `".DB_docs_templates."` WHERE `isPublished`=1 AND `isDeleted`=0 AND `kindergarten_id`=".DB_ID);
		/*if(mysqli_num_rows($result) == 0) {
			echo "Vadovai dar nepaskelbė šablonų.";
		} else {*/
		?>
		<div><div style="line-height: 30px; float: left"><!-- Pildyti pagal -->Forma</div><div class="sel" style="float: left"><select name="form_id" required="required">
			<option value="" selected hidden disabled>Pasirinkite formą</option>
			<option value="0">Tuščia</option>
			<?php
			while ($row = mysqli_fetch_assoc($result))
				echo "<option value=\"".$row['form_id']."\">".$row['title']."</option>";
			?>
		</select></div> <input type="submit" value="Išsirinkau formą pildymui (Kopijuoti formos turinį)" class="filter" style="margin-left: 7px;"></div><!-- Išsirinkau -->
 		<div class="notice">Formas gali įdėti žmogus turintis vadovo prisijungimą.</div>
		<?php //} ?>
	</form>
	</fieldset>
<?php }

if(isset($_GET['form_id']) /*&& !ADMINoptional*/ || isset($_GET['edit'])) {
	$msg_in_form = '';
	
	if(isset($_GET['edit'])) {
		//Security: like hasGroup() but via $add_filter
		$result = db_query("SELECT *, UNIX_TIMESTAMP(`openedDate`) AS `openedDateTimestamp`
			FROM `".DB_docs."` WHERE $add_filter`answer_id`='".(int)$_GET['edit']."' AND `kindergarten_id`=".DB_ID." ORDER BY `date` DESC");
		if(mysqli_num_rows($result)) {
			$answer = mysqli_fetch_assoc($result);
			if($answer['openedDateTimestamp'] > time()-$max_opened_msg_delay && ($answer['openedByEmployeeId'] != DARB_ID || $answer['openedByUserId'] != USER_ID)) {
				$msg_in_form = '<div class="error" id="form-fill">Atidarėte kito žmogaus redaguojamą dokumentą. Dabar redaguoja '.getAllEmployees((int)$answer['openedByEmployeeId'])." ".$answer['openedDate'].'.</div>';
			} else {
				//Opened
				db_query("UPDATE `".DB_docs."` 
					SET `openedByUserId`=".USER_ID.", `openedByEmployeeId`=".DARB_ID.", `openedDate`=CURRENT_TIMESTAMP()
					WHERE $add_filter`kindergarten_id`=".DB_ID." AND `answer_id`=".(int)$answer['answer_id']);
			}
		}
	}

	if(!empty($msg_in_form)) {
		echo $msg_in_form;
	} else/*if(isset($_GET['form_id']) || isset($_GET['edit']))*/ {
		unset($answer);
		if(isset($_GET['form_id'])) {
			if($_GET['form_id'] == 0)
				$answer = ['title' => 'Be formos', 'date' => '', 'content' => ''];
			else {
				$result = db_query("SELECT * FROM `".DB_docs_templates."` WHERE `kindergarten_id`=".DB_ID." AND `form_id`=".(int)$_GET['form_id']);
				if(mysqli_num_rows($result)) {
					$answer = mysqli_fetch_assoc($result);
					//Do need to reset title?
					$answer['date'] = '';
				} else
					echo '<div id="form-fill">Tokios formos nėra.</div>';
			}
		} else {
			$result = db_query("SELECT * FROM `".DB_docs."` WHERE `kindergarten_id`=".DB_ID." AND `answer_id`=".(int)$_GET['edit']);
			$answer = mysqli_fetch_assoc($result);
		}
		if(isset($answer)) {
			?>
			<fieldset id="form-fill">
			<legend><?=(isset($_GET['form_id']) ? 'Pildyti ' : 'Redaguoti')?> pagal formą „<strong><?=$answer['title']?></strong>“</legend>
			<form action="<?=$URL?>" method="post" class="not-saved-reminder new-input-form">
				<?php //echo $msg_in_form; ?>
				<input type="hidden" name="<?=(isset($_GET['form_id']) ? 'save' : 'edit')?>" value="<?=(isset($_GET['form_id']) ? (int)$_GET['form_id'] : $answer['answer_id'])?>"<?=(isset($_GET['edit']) ? ' id="answer_id"' : '')?>>
				<p><label><span class="title">Pavadinimas<span class="required">*</span></span> <input type="text" name="title" value="<?=date_empty($answer['title'])?>" required="required" style="width: 98%; max-width: 1000px;"></label></p>
				<p><label><span class="title">Data<span class="required">*</span></span> <input class="datepicker" type="text" name="date" value="<?=date_empty($answer['date'])?>" required="required"></label></p>
				<div>Rodyti (vaikų grupė)<span class="required">*</span> <div class="sel"><select name="group_id"<?=(isset($_GET['edit']) && !(ADMIN || $answer['createdByEmployeeId'] == DARB_ID) ? ' disabled="disabled" style="color: grey;"' : '')?>required="required">
					<option value="" selected="selected" disabled selected hidden>Pasirinkite kas matys</option>
					<option value="0"<?=(isset($_GET['edit']) && 0 == $answer['group_id'] ? ' selected="selected"' : '')?>>Be grupės (asmeninis)</option>
					<option value="-3"<?=(isset($_GET['edit']) && -3 == $answer['group_id'] ? ' selected="selected"' : '')?>>Be grupės (sau ir vadovams)</option>
					<?php
					foreach(getAllowedGroups() as $ID => $title)
						echo '<option value="'.$ID.'"'.(isset($_GET['edit']) && $ID == $answer['group_id'] ? ' selected="selected"' : '').">".$title."</option>";
					?>
				</select></div></div>
								
				<div><label>Vaikas 
					<div class="sel">
					<select name="kid_id"<?=(isset($_GET['edit'])&& !(ADMIN || $answer['createdByEmployeeId'] == DARB_ID) ? ' disabled="disabled" style="color: grey;"' : '')?>>
						<option value="0"<?=(isset($_GET['edit']) && $answer['kid_id'] == 0 ? ' selected="selected"' : '')?>>Be vaiko</option>
						<?php
						//DRY: dokumentai_vaiko.php
						$result = db_query($get_kids_sql);//TODO: adapt to edit date
						while ($row = mysqli_fetch_assoc($result))
							echo "<option value=\"".$row['parent_kid_id']."\"".(isset($_GET['edit']) && $answer['kid_id'] == $row['parent_kid_id']  ? ' selected="selected"' : '').">".filterText(getName($row['vardas'], $row['pavarde']))."</option>";
						?>
					</select></div>
					</label>
				</div>
				<div><label>Rodyti tėvams <input type="checkbox" name="allow_parents" value="1"<?php if(isset($_GET['edit']) && $answer['allow_parents']) echo ' checked="checked"';?>><span class="notice">(a. nurodžius grupę ir nenurodžius vaiko rodoma nurodytos grupės tėvams arba b. nurodžius vaiką rodoma tik nurodyto vaiko tėvams)</span></label></div>
				<p><label><div class="title" style="display: inline-block;">Turinys</div>: <span class="notice">Išplėsti/sumažinti redagavimo vaizdą galite naudodami žemiau esantį</span>  <div style="background-image: url(http://dev.musudarzelis.lt/libs/ckeditorCustom/plugins/icons.png?t=F7J9); background-position: 0 -672px; width: 16px; height: 16px; display: inline-block; vertical-align: center;"></div>
				<textarea class="wymeditor ckeditor" name="content"><?=filterText($answer['content'])?></textarea></label></p>
				<p><input type="submit" class="submit" value="Išsaugoti"></p>
			</form></fieldset>
			<script type="text/javascript" src="/workers/docs_fill_out.js?0"></script>
			<?php
			echo '<script>$(function(){$("form").sisyphus({locationBased: true,onRestore:function(){alert(\'Šios naršyklės duomenys atstatyti iš praeito neišsaugojimo\');}});});</script>';
		} else echo 'qqq';
	}
}


if(isset($_GET['view'])) {
	$answer_result = db_query("SELECT * FROM `".DB_docs."` WHERE $add_filter`kindergarten_id`=".DB_ID." AND `answer_id`='".(int)$_GET['view']."'", 'aBloga užklausa: ');
	if(mysqli_num_rows($answer_result) > 0) {
		$answer = mysqli_fetch_assoc($answer_result);
		//print_r($answer);
		//LEFT JOIN kadangi jei bus daug kitų answer'ių, bet maniškiam field'ui nebus įvesta tai būtent dėl to
		$result = db_query("SELECT * FROM `".DB_docs."` WHERE `kindergarten_id`=".DB_ID." AND `answer_id`='".(int)$_GET['view']."'");
		if (isset($_GET['download'])) {
			ob_clean();
			word_header();
		}
		?><div id="planning-view">
		<?php if($answer['form_id'] != 0) { ?>
		<div class="no-print">Pildyta pagal formą: <?=$form_title[$answer['form_id']]?></div>
		<?php } ?>
		<?php if($answer['group_id'] != 0) { ?>
		<p><span class="title">Grupė:</span> <?=$grupes[$answer['group_id']]?></p>
		<?php } ?>
		<?php if(isset($kids[$answer['kid_id']])) { ?>
		<p><span class="title">Vaikas:</span> <?=$kids[$answer['kid_id']]?></p>
		<?php } ?>
		<p><span class="title">Pavadinimas:</span> <?=filterText($answer['title'])?></p>
		<p><span class="title">Data:</span> <?=date_empty($answer['date'])?></p>
		<div><span class="title"></span> <?=/*nl2br*/(strip_tags($answer['content'], '<br><p><div><strong><em><table><tr><th><td><span><s><ol><ul><li><h1><h2><h3><del><pre><cite><img><hr><blockquote><tt><code><kbd><samp><var><ins><q><tbody><thead><tfoot>'))?></div>
		<div class="no-print">
				<div style="clear: left; float: left; width: 70px;">Sukūrė:</div><div style="float: left;"><?=$answer['created']?> <?=getAllEmployees($answer['createdByEmployeeId'])?></div>
				<div style="clear: left; float: left; width: 70px;">Atnaujino:</div><div style="float: left;"><?=date_empty($answer['updated'])?> <?=getAllEmployees($answer['updatedByEmployeeId'])?></div>
				<div style="clear: left;"></div>
				<?php
				if($answer['from_answer_id'] /*&& $answer['from_answer_id'] != $answer['answer_id'] */) {
					echo '<a href="?old&amp;view='.$answer['from_answer_id'].'#planning-view">Senesnė kopija</a>';
				}
				
				if(KESV) { ?>
				Keitė kartų: <?=$answer['updatedCounter']?><br>
				<?php
				if($answer['from_answer_id'] != $answer['answer_id'])
					echo 'Kopijuota iš: '.$answer['from_answer_id'].'<br>';
				?>
				answer_id: <?=$answer['answer_id']?><br>
				<?php } ?>
			</div>
		</div>
		<?php
		//<a onclick="this.href='data:application/msword;charset=UTF-8,'+encodeURIComponent(document.documentElement.outerHTML)" href="#" download="page.doc">Atsisiųsti</a>
		if (isset($_GET['download'])) {
			echo '</body></html>';
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');//application/vnd.openxmlformats-officedocument.wordprocessingml.document http://stackoverflow.com/questions/4212861/what-is-a-correct-mime-type-for-docx-pptx-etc
			header('Content-Disposition: attachment; filename='.basename($answer['date'].' '.$answer['title']).'.doc');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: '.strlen(ob_get_contents()));
			ob_end_flush();
			exit;
		} else {
			echo '<a href="?'.http_build_query(array_merge($_GET, ['download' => ''])).'" class="no-print">Atsisiųsti Word</a>';
		}
	} else {
		echo '<p>Nėra tokio užpildyto planavimo.</p>';
	}
}
?>
</div>
</div>
