<?php if(!defined('DARBUOT')) exit(); ?>
<h1>Vaikų ugdymo veiklos (arba pamokų) tvarkaraštis (su specialistais)</h1>
<div id="content">
<?php
if (isset($_POST['save']) && isset($_POST['burelio_id']) && !SOCIALINIS_PEDAGOGAS) {
	db_query("INSERT INTO `".DB_bureliai_schedule."` SET
		`burelio_id`=".(int)$_POST['burelio_id'].", `grup_id`=".(int)$_POST['grupes_id'].", 
		`diena`=".(int)$_POST['diena'].", `nuoVal`=".(int)$_POST['nuoVal'].", `nuoMin`=".(int)$_POST['nuoMin'].", 
		`ikiVal`=".(int)$_POST['ikiVal'].", `ikiMin`=".(int)$_POST['ikiMin'].", 
		`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
	if($_POST['grupes_id'] == 0) {
		$bureliai_schedule_id = (int)mysqli_insert_id($db_link);
		if(isset($_POST['kids'])) foreach($_POST['kids'] as $kid_id) {
			$kid_id = (int)$kid_id;
			db_query("INSERT INTO `".DB_bureliai_schedule_kids."` SET `bureliai_schedule_id`=".(int)$bureliai_schedule_id.", `kid_id`=".(int)$kid_id);
		}
	}
	echo msgBox('OK', 'Sėkmingai išsaugota.');
} elseif(isset($_POST['save']) && !SOCIALINIS_PEDAGOGAS) {
	echo msgBox('ERROR', 'Neįvestas neformaliojo ugdymo veiklos pavadinimas.');
}

if (isset($_POST['update']) && !SOCIALINIS_PEDAGOGAS) {
	db_query("UPDATE `".DB_bureliai_schedule."` SET 
		`grup_id`=".(int)$_POST['grupes_id'].", `burelio_id`=".(int)$_POST['burelio_id'].", 
		`diena`=".(int)$_POST['diena'].", `nuoVal`=".(int)$_POST['nuoVal'].", `nuoMin`=".(int)$_POST['nuoMin'].", 
		`ikiVal`=".(int)$_POST['ikiVal'].", `ikiMin`=".(int)$_POST['ikiMin'].",
		`updated`=CURRENT_TIMESTAMP, `updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."', `updatedCounter`=`updatedCounter`+1
		WHERE `ID`=".(int)$_POST['update']);
	if($_POST['grupes_id'] == 0) {
		if(!empty($_POST['kids'])) {
			foreach($_POST['kids'] as &$kid_id)
				$kid_id = (int)$kid_id;
			unset($kid_id);//FIXED. Why? Warning. Reference of a $value and the last array element remain even after the foreach loop. It is recommended to destroy it by unset().
			db_query("DELETE FROM `".DB_bureliai_schedule_kids."` WHERE `bureliai_schedule_id`='".(int)$_POST['update']."' AND `kid_id` NOT IN (".implode(',', $_POST['kids']).")");
			foreach($_POST['kids'] as $kid_id) {
				$res = db_query("SELECT * FROM `".DB_bureliai_schedule_kids."` WHERE `bureliai_schedule_id`=".(int)$_POST['update']." AND `kid_id`=".(int)$kid_id);
				if(mysqli_num_rows($res) == 0)
					db_query("INSERT INTO `".DB_bureliai_schedule_kids."` SET `bureliai_schedule_id`=".(int)$_POST['update'].", `kid_id`=".(int)$kid_id);
			}
			
		}
	}
	msgBox('OK', 'Sėkmingai atnaujinta.');
}

if (isset($_GET['delete'])  && !SOCIALINIS_PEDAGOGAS) {
	db_query("DELETE FROM ".DB_bureliai_schedule." WHERE `ID`=".(int)$_GET['delete']);
	db_query("DELETE FROM `".DB_bureliai_schedule_kids."` WHERE `bureliai_schedule_id`=".(int)$_GET['delete']);
	msgBox('OK', 'Sėkmingai ištrinta');
}
if(!SOCIALINIS_PEDAGOGAS) { ?>
<a href="?#additional-activity" class="no-print fast-action fast-action-add">Įvesti naują veiklos įrašą tvarkaraštyje</a> <?=ui_print()?>
<?php } ?>

<table class="vertical-hover">
<tr>
	<th>Grupė arba vaikai</th>
	<th>Veikla</th>
	<th>Diena</th>
	<th>Nuo</th>
	<th>Iki</th>
	<?php if(!SOCIALINIS_PEDAGOGAS) { ?>
	<th class="no-print">Veiksmai</th>
	<?php } ?>
</tr>
<?php
$kids_of_schedule = [];
$result = db_query("SELECT `".DB_bureliai_schedule_kids."`.*, cr.*
	FROM `".DB_bureliai_schedule_kids."`
	JOIN `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<=CURDATE() GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from` AND cr.`parent_kid_id`=`".DB_bureliai_schedule_kids."`.`kid_id`
	WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 ORDER BY ".orderName('cr'));//".(ADMIN ? '' : " AND cr.`grupes_id`=".GROUP_ID)."
while($row = mysqli_fetch_assoc($result))
	$kids_of_schedule[$row['bureliai_schedule_id']][$row['kid_id']] = filterText(getName($row['vardas'], $row['pavarde']));

$result = db_query("SELECT `".DB_bureliai."`.`pavadinimas` AS `bpav`, `".DB_groups."`.`pavadinimas`, `".DB_bureliai_schedule."`.*
	FROM `".DB_bureliai_schedule."` 
	LEFT JOIN `".DB_groups."` ON `".DB_bureliai_schedule."`.`grup_id`=`".DB_groups."`.`ID`
	LEFT JOIN `".DB_bureliai."` ON `".DB_bureliai_schedule."`.`burelio_id`=`".DB_bureliai."`.`ID`
	ORDER BY `grup_id`<>0 DESC, `".DB_groups."`.`pavadinimas`, `diena`, `nuoVal`, `nuoMin`, `ikiVal`, `ikiMin`, `".DB_bureliai."`.`pavadinimas`");
while($row = mysqli_fetch_assoc($result)) {
	$hide_class = '';
	if($row['grup_id'] != 0 && $_SESSION['GROUP_ID'] != $row['grup_id']) {
		$hide_class = ' hide';
		$hide = true;
	}
	echo (isset($prev_group) && $prev_group != $row['pavadinimas'] ? '<tr><td colspan="10" style="background-color: rgb(228, 228, 228);"></td></tr>' : '');
	?>
	<tr class="<?=(isset($prev_day) && $prev_day != $row['diena'] ? 'line' : '').$hide_class?>">
		<td><?php
		if($row['grup_id'] != 0)
			echo filterText($row['pavadinimas']);
		elseif(isset($kids_of_schedule[$row['ID']]))
			echo implode('<br>', $kids_of_schedule[$row['ID']]);
		?></td>
		<td><?=filterText($row['bpav'])?></td>
		<td><?=$dienos_kada[$row['diena']]?></td>
		<td><?=nr_val_min($row['nuoVal'], $row['nuoMin'])?></td>
		<td><?=nr_val_min($row['ikiVal'], $row['ikiMin'])?></td>
		<?php if(!SOCIALINIS_PEDAGOGAS) { ?>
			<td class="no-print">
			<?php if($row['grup_id'] == 0 || $row['grup_id'] != 0 && hasGroup($row['grup_id'])) { ?>
				<a href="?edit=<?=$row['ID']?>#additional-activity">Keisti</a>
			<?php } ?>
			<?php if($row['createdByEmployeeId'] == DARB_ID || ADMIN) { ?>
				<a href="?delete=<?=$row['ID']?>" onclick="return confirm('Ar tikrai norite trinti?')">Trinti</a>
			<?php } ?>
			</td>
			<?php
		}
		$prev_day = $row['diena'];
		$prev_group = $row['pavadinimas'];
		?>
	</tr>
	<?php
}
?>
</table>
<?php
if(isset($hide))
	echo '<a href="#" onclick="$(\'.hide\').show(); return false;" class="no-print">Rodyti kitų grupių tvarkaraštį</a>';


if(!SOCIALINIS_PEDAGOGAS) {
	if (isset($_GET['edit'])) {
		$result = db_query("SELECT * FROM `".DB_bureliai_schedule."` WHERE `ID`=".(int)$_GET['edit']);
		$edit = mysqli_fetch_assoc($result);
	}
	?>
	<fieldset id="additional-activity" class="no-print">
	<legend><?=(isset($_GET['edit']) ? "Veiklos duomenų keitimas" : "Nauja veikla")?></legend>
	<form method="post" class="not-saved-reminder">
		<div>Veikla:
			<?php
			$result = db_query("SELECT * FROM `".DB_bureliai."` ORDER BY `pavadinimas`");
			if(mysqli_num_rows($result) > 0) {
				echo '<div class="sel"><select name="burelio_id" required><option value="" selected hidden disabled>Pasirinkite</option>';
				while ($row = mysqli_fetch_assoc($result)) {
					echo "\t\t<option value=\"".$row['ID']."\"".((isset($_GET['edit']) && $row['ID'] == $edit['burelio_id']) ? ' selected="true"' : '').">".filterText($row['pavadinimas'])."</option>\n";	
				}
				echo '</select></div>';
			} else {
				echo 'Pirmiau reikia įvesti <a href="/bureliai">ugdymo veiklos pavadinimą</a>';
			}
			?>
		</div>
		<div>Grupė ar vaikai: <div class="sel"><select name="grupes_id" id="groupOrKids"><option value="" selected hidden disabled>Pasirinkite</option>
		<?php
		echo "\t\t<option value=\"0\"".(isset($_GET['edit']) && 0 == $edit['grup_id'] ? ' selected="true"' : '').">Vaikai vietoje grupės</option>\n";
		$result = db_query("SELECT * FROM `".DB_groups."` ORDER BY `pavadinimas` DESC");
		if(mysqli_num_rows($result) > 0) {
			while ($row = mysqli_fetch_assoc($result)) {
				echo "\t\t<option value=\"".$row['ID']."\"".((isset($_GET['edit']) && $row['ID'] == $edit['grup_id']) ? ' selected="true"' : '').">".filterText($row['pavadinimas'])."</option>\n";	
			}
		}
		?>
		</select></div></div>
		<div id="kidSelect" style="padding-left: 50px;<?=(/*!isset($_GET['edit']) ||*/ isset($_GET['edit']) && 0 == $edit['grup_id'] ? '' : ' display: none')?>">
			<label>Vaikai pridėjimui: <div class="sel"><select id="kidId"><option value="" hidden selected disabled>Pasirinkite vaiką</option>
			<?php
			$result = db_query($get_kids_sql);
			while($row = mysqli_fetch_assoc($result))
				echo "<option value=\"".$row['parent_kid_id']."\">".filterText(getName($row['vardas'], $row['pavarde']))."</option>";
			?>
			</select></div>
			<a href="#" id="add_kid" hidden>+ Pridėti vaiką</a> 
			</label>
			<div id="kids_list">
			<?php
			if(isset($_GET['edit'])) {
				//TODO: can be issue when record with main kid ID was removed
				$res = db_query("SELECT `".DB_children."`.*
				FROM `".DB_bureliai_schedule_kids."` JOIN `".DB_children."` ON `".DB_children."`.`ID`=`".DB_bureliai_schedule_kids."`.`kid_id`
				WHERE `".DB_bureliai_schedule_kids."`.`bureliai_schedule_id`=".(int)$_GET['edit']." ORDER BY ".orderName());//vardas, pavarde
				while ($row = mysqli_fetch_assoc($res)) {
					echo "<p>".filterText(getName($row['vardas'], $row['pavarde']))." <input type=\"hidden\" name=\"kids[]\" value=\"${row['ID']}\" \"> <a href=\"#\" class=\"remKid\" data-kid-id=\"${row['ID']}\" id=\"kid${row['ID']}\">- Trinti</a></p>";
				}
			}
			?>
			</div>
		</div>
		<div style="margin-top: 7px;">
			<div style="float: left; margin-top: 4px;">Vyksta:</div>
			<div class="sel" style="float: left; margin-left: 5px"><select name="diena">
				<?php
				foreach($dienos_kada as $id => $val)
					echo "<option value=\"$id\"".(isset($_GET['edit']) && $id == $edit['diena'] ? ' selected="true"' : '').">$val</option>";
				?>
			</select></div>
			<div style="float: left; margin-left: 5px">
				Nuo <input type="text" name="nuoVal" style="width: 20px" value="<?php echo (isset($_GET['edit']) ? $edit['nuoVal'] : ''); ?>">:
				<input type="text" name="nuoMin" style="width: 20px" value="<?php echo (isset($_GET['edit']) ? $edit['nuoMin'] : ''); ?>">
				Iki <input type="text" name="ikiVal" style="width: 20px" value="<?php echo (isset($_GET['edit']) ? $edit['ikiVal'] : ''); ?>">:
				<input type="text" name="ikiMin" style="width: 20px" value="<?php echo (isset($_GET['edit']) ? $edit['ikiMin'] : ''); ?>"> 
			</div>
			<div class="cl"></div>
		</div>
		<p><input type="hidden" <?=(!isset($_GET['edit']) ? 'name="save"' : 'name="update" value="'.(int)$edit['ID'].'"')?>><input type="submit" value="Išsaugoti" class="submit"></p>
	</form>
	</fieldset>
	<script src="/workers/bureliai_schedule.js"></script>
	<?php
}
?>
</div>
