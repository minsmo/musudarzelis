$(function() {
	/*var EmployeeDiv = $('#employees');
	$('#addEmployee').on('click', function(event) {
		event.preventDefault();
		var id = $("#employeeId").val();
		if(!$("#employee"+id).length) {
			$('<p><label>'+$("#employeeId option:selected").text()+'<input type="hidden" name="priskyrimas[]" value="'+id+'"></label> <a href="#" class="remEmployee" id="employee'+id+'">- Trinti šį laukelį</a></p>').appendTo(EmployeeDiv);// data-id="'+id+'"
		} else
			alert("Šis darbuotojas jau pridėtas prie redaguojamos grupės.");
	});

	$(document).on('click', '.remEmployee', function (event) {
		event.preventDefault();//return false;
		$(this).parents('p').remove();
	});
	*/
	
	/*Code below is used only when button "assign employee" is in use
	$('select').change(function(event) {
		event.preventDefault();
		if($(this).val() != 0)
			$(this).parent().next().show();
		else
			$(this).parent().next().hide();
	});*/
	/*VERY OLD $(document).on('click', '.remEmployee', function (event) {
		event.preventDefault();//return false;
		$(this).prev().children().val(0);
		$(this).hide();
	});*/
	
	
	var addPerson = $('#persons');
	$('#personId').change(function(event) {
		event.preventDefault();
		var id = $("#personId").val();
		if(id) {
			$("option[value="+id+"]").hide();
			if(!$("#person"+id).length) {
				$('<p><label>'+$("#personId option:selected").text()+' <input type="hidden" name="priskyrimas[]" value="'+id+'"></label> <a href="#" class="remPerson" id="person'+id+'">- Ištrinti susiejimą</a></p>').appendTo(addPerson);
				DONE("Darbuotojas „"+$("#personId option:selected").text()+"“ buvo pridėtas į sarašą!");
			} else
				alert("Šis darbuotojas jau pridėtas.");
		}
		$('#personId').val("");
	});

	$(document).on('click', '.remPerson', function (event) {
		event.preventDefault();
		$(this).parents('p').remove();
		$("option[value="+($(this).parents('p').find("input").val())+"]").show();
	})
	
	$("#personId > option").each(function() {
		if($("#person"+this.value).length)
			$("option[value="+this.value+"]").hide();
	});
});
