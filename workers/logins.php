<?php if(!defined('ADMIN') || !ADMIN) exit(); ?>
<h1>Tėvų ar/ir darbuotojų prieigos prie sistemos</h1>
<div id="content">
<?php
//Logins
if(isset($_GET['add'])) {
	$_POST['email'] = filter_var($_POST['email'], FILTER_VALIDATE_EMAIL);
    if( empty($_POST['email']) ) {
        //logdie("<script type=\"text/javascript\">alert('Neužpildyti būtini laukai'); window.history.back(-1);</script>");
        die('<div class="red center">Neužpildyti būtini laukeliai.</div>');
    }
    $result = db_query("SELECT `email` FROM `".DB_users."` WHERE `email`='".db_fix($_POST['email'])."'");
	if(mysqli_num_rows($result) >= 1) {
		msgBox('ERROR', 'Toks prisijungimo el. paštas jau egzistuoja sistemoje.');
	} else {
		if($_POST['email']) {
			register(isset($_POST['name']) ? $_POST['name'] : '', isset($_POST['surname']) ? $_POST['surname'] : '', $_POST['email']);//TODO: REVIEW
		} else msgBox('ERROR', 'Netinkamas el. paštas.');
	}
}
if(isset($_GET['update'])) {
    if( empty($_POST['email']) ) {
        //logdie("<script type=\"text/javascript\">alert('Neužpildyti būtini laukai'); window.history.back(-1);</script>");
        die('<div class="red center">Neužpildyti būtini laukeliai.</div>');
    }
	$select = "UPDATE `".DB_users."` SET `email`='".db_fix($_POST['email'])."', `name`='".db_fix($_POST['name'])."', `surname`='".db_fix($_POST['surname'])."'";
	//if(!empty($_POST['slaptazodis'])) $select .= ", `slaptazodis`='".passwd_hash($_POST['slapyvardis'].$_POST['slaptazodis'])."'";
	$select .= " WHERE `user_id`=".(int)$_GET['update'];

	if (!mysqli_query($db_link, $select)) {
		logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
	} else msgBox('OK', 'Sėkmingai atnaujinta!');
}
if(isset($_GET['delete'])) {
	if (!mysqli_query($db_link, "DELETE FROM `".DB_users."` WHERE `user_id`=".(int)$_GET['delete'])) {
		logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
	} else {
		if (!mysqli_query($db_link, "DELETE FROM `".DB_users_allowed."` WHERE `user_id`=".(int)$_GET['delete'])) {
			logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
		} else
			msgBox('OK', 'Prisijungimas sėkmingai ištrintas!');
	}
}
//Rights
if(isset($_GET['add_right']) && isset($_POST['user_id'])) {
	if($_POST['person_type'] == PARENTS && (!isset($_POST['child_id']) || empty($_POST['child_id']))) {
		msgBox('ERROR', 'Nenurodytas vaikas, pirmiausia reikia įvesti vaikus, kad juos galima būtų priskirti.');
	} else {
		$result = db_query("SELECT * FROM `".DB_users_allowed."` WHERE `user_id`='".(int)$_POST['user_id']."' AND `person_type`='".(int)$_POST['person_type']."' AND `person_id`='".($_POST['person_type'] == PARENTS ? (int)$_POST['child_id'] : (int)$_POST['employee_id'])."'");
		if(mysqli_num_rows($result) >= 1) {
			msgBox('WARN', 'Toks leidimas jau egzistuoja. Be reikalo bandote tokį patį pridėti dar kartą.');
		} else {
			if (mysqli_query($db_link, "INSERT INTO `".DB_users_allowed."` SET `user_id`='".(int)$_POST['user_id']."', `person_type`='".(int)$_POST['person_type']."', `person_subtype`='".(int)$_POST['person_subtype']."', `person_id`='".($_POST['person_type'] == PARENTS ? (int)$_POST['child_id'] : (int)$_POST['employee_id'])."'"))
				msgBox('OK', 'Sėkmingai išsaugota!');
			else logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
		}
	}
}
if(isset($_GET['del_user_id']) && isset($_GET['del_person_type']) && isset($_GET['del_person_id'])) {
	db_query("DELETE FROM `".DB_users_allowed."` WHERE `user_id`=".(int)$_GET['del_user_id']." AND `person_type`=".(int)$_GET['del_person_type']." AND `person_id`=".(int)$_GET['del_person_id']);
	//addNotificationMsg('user_rights_removed');
	//header("Location: ?notification=user_rights_removed");
	msgBox('OK', 'Prisijungimui leidimas pašalintas!');
}

if(KESV) {
	if(isset($_GET['merge'])) {
		//rows 24 => Dublis cnt 29;
		//6
		//1 vardo ir pavardės nebuvo prisijungusiame
		//1 kretEgl_ voveriukugr@gmail.com auklėtoja
		//1 pnvrBit_ Medicinos seselė neprisijungė, nors kaip tėvas buvo prisijungusi

		//Abejais buvo prisijungusi kaiSpind_ linaparniauskiene@gmail.com
		//druskBit_ WTF?
		$passed = true;
		$to_user_id = 0;
		$rows = [];
		$result = db_query("SELECT * 
			FROM `".DB_users."` JOIN `".DB_users_allowed."` ON `".DB_users."`.`user_id`=`".DB_users_allowed."`.`user_id`
			WHERE `".DB_users."`.`email`='".db_fix($_GET['merge'])."'");
		$name = '';
		$surname = '';
		$use_login_counter = false;
		while($row = mysqli_fetch_assoc($result)) {
			$rows[] = $row;
			$name = $row['name'];
			$surname = $row['surname'];
			if($row['login_counter'] != 0) {
				$to_user_id = $row['user_id'];//Means: Do use login counter?
				$use_login_counter = true;
			}
			if($row['person_type'] != PARENTS) {
				$passed = false;
				die('TĖV.');
			}
		}
		foreach($rows as $row) {
			if($to_user_id == 0 && !$use_login_counter)
				$to_user_id = $row['user_id'];
		}

		//if(!$use_login_counter)
		if($to_user_id != 0) {
			foreach($rows as $row) {
				if($name != $row['name'])
					die('Nesutampa vardai.');//TODO: finish;
				if($surname != $row['surname'])
					die('Nesutampa pavardės.');//TODO: finish;
				if($to_user_id != $row['user_id'] && $row['login_counter'] != 0 && $use_login_counter)
					die('Two logins');
			}
			$remove_user_id = [];
			$safe99_to_remove = [];
			foreach($rows as $row) {
				if($to_user_id != $row['user_id']) {
					db_query("UPDATE `".DB_users_allowed."` SET `user_id`=".(int)$to_user_id." WHERE `user_id`=".(int)$row['user_id']." AND `person_type`=".(int)$row['person_type']." AND `person_subtype`=".(int)$row['person_subtype']." AND `person_id`=".(int)$row['person_id']);
					//
					$safe99_to_remove[$row['user_id']] = mysqli_affected_rows($db_link);
					echo ' OK'.$safe99_to_remove[$row['user_id']];//Debug
					//02:53:50
				}
			}
			foreach($safe99_to_remove as $user_id => $cnt)
				if($cnt)
					db_query("DELETE FROM `".DB_users."` WHERE `user_id`=".(int)$user_id);
			msgBox('OK', 'Sėkmingai sulieta.');//Can be avoided//TODO: finish
		} else {
			msgBox('OK', 'Nėra prisijungusio!');//Can be avoided//TODO: finish
		}
		//msgBox('OK', 'Sulieta sėkmingai!');
	}

	$result = db_query("SELECT *, COUNT(*) `cnt` FROM `".DB_users."` GROUP BY `email` HAVING COUNT(*)>1");
	while ($row = mysqli_fetch_assoc($result)) {
		echo '<a href="?merge='.filterText($row['email']).'">'.filterText($row['email']).'</a> ';
	}
}
/*
Prieiga suteikiama žmogui prie sistemoje esančios informacijos, t.y. tėvų ar/ir darbuotojų.
Žmonių (tėvų ar/ir darbuotojų) prieiga prie sistemos esančios informacijos.
Žmonių (tėvų ar/ir darbuotojų) prieiga
Žmonių (tėvų ar/ir darbuotojų) prieiga prie „MusuDarzelis“ sistemos.
*/

$depends_groups = [];
foreach($login_depends_on_assigned_groups as $id => $dumb) {
	$depends_groups[] = mb_lcfirst($person_type_who[$id]);
}
?>
<p>Priega žmogui priklauso nuo jo <strong>el. pašto</strong> ir jam <strong>suteiktų leidimų grupių</strong> prieiti prie vaiko(ų) ar darbuotojo darbinės aplinkos, o <strong>slaptažodis</strong> iš karto po prisijungimo išsaugojimo sukuriamas ir išsiunčiamas el. paštu.<br>
Jei šis žmogus yra <strong><?=_join('</strong>, <strong>', $depends_groups, 'ar')?></strong>, tai gali prieiti tik prie <strong>priskirtų vaikų grupių</strong> (meniu punkte „Grupės“).<br>
Jei šis žmogus yra <strong><?=/*mb_ucfirst*/(_join('</strong>, <strong>', $all_groups_for_person_type_who, 'ir'))?></strong>, tai gauna prieigą prie visų darželyje esančių grupių.

</p>
<p class="notice">Pastabos:<br>
Šis meniu punktas dažniausiai naudojamas tik tada, kai neužtenka galimybių meniu punktų „Darbuotojai“ ir „Vaikai“<!-- modulių teikiamų -->.<br>
<!-- Prieiga tėvams gali būti automatiškai sukuriama vaiko įvedimo į sistemą metu („Vaikai“ meniu punkte) kai įvedami laukeliai „Prisijungimas prie vaiko mamos el. paštu“ arba/ir „Prisijungimas prie vaiko tėčio el. paštu“.<br>
Siūlau: --> Prieiga tėvams įprastai kuriama vaiko įvedimo į sistemą metu (meniu punkte „Vaikai“) įvedus laukelius „Prisijungimas prie vaiko mamos el. paštu“ arba/ir „Prisijungimas prie vaiko tėčio el. paštu“.<br>
Prieiga darbuotojams įprastai kuriama meniu punkte „Darbuotojai“ įvedant papildomus duomenis skyrelyje „Duoti naują prieigą prie sistemos<!-- Nauja prieiga prie sistemos -->“.</p>
<a href="?#login-form" class="no-print fast-action fast-action-add">Naujo prisijungimo įvedimas</a>


<form method="get" class="no-print">
	<div class="sel" style="float: left; margin-right: 7px;"><select name="person_type">
	<?php 
	foreach($person_type_extended as $ID => $title)
		echo "<option value=\"".$ID."\"".(isset($_GET['person_type']) && $ID == $_GET['person_type'] ? ' selected="selected"' : '').">".$title."</option>";
	?>
	</select></div>
	<input type="submit" class="filter" value="Rodyti el. paštus turinčius tik šios rūšies leidimą">
</form>
<table id="logins-tbl">
<tr>
	<th>Vardas Pavardė</th>
	<th>Prisijungimo el. paštas</th>
	<th><span class="abbr" title="Paskutinio grįžusio atgal laiško data. Grįžo dėl: blogo el. pašto adreso, perpildytos el. pašto dėžutės ar kitos priežasties">Grįžo laiškas</span></th>
	<th>Leidimai (prisijungus el. paštu, jis nurodytas kairėje, gali dirbti <!-- (atlikti veiksmus) --> kaip)</th>
	<th>Veiksmai</th>
</tr>
<?php
$wrong_emails = [];
$result = db_query("SELECT * FROM `0wrong_emails`");
while($row = mysqli_fetch_assoc($result))
	$wrong_emails[$row['email']] = $row['last_date'];
	
$opened_edit_id = isset($_GET['edit']) ? (int)$_GET['edit'] : 0;//opened-row
$result = db_query("SELECT * FROM `".DB_users."` ORDER BY `name`, `surname`, `email`");
while($row = mysqli_fetch_assoc($result)) {
	$echo = '<tr'.($opened_edit_id == $row['user_id'] ? ' class="opened-row"' : '').'>
		<td>'.filterText($row['name']).' '.filterText($row['surname']).'</td>
		<td class="email'.($row['login_counter'] ? '-accessed' : '').'">'.filterText($row['email']).'</td>
		<td>'.(isset($wrong_emails[$row['email']]) ? $wrong_emails[$row['email']] : '').'</td>
		<td>';
	//$echo .= '<tr><td><strong>'.$row['name'].' '.$row['surname'].'</strong> su el. paštu <strong>'.$row['email'].'</strong> gali prisijungti kaip:<div style="padding-left:50px">';
	//LEFT JOIN `` `".DB_users_allowed."`.`user_id`=`darbuotojai`=`ID`  LEFT JOIN  
	$result_inner = db_query("SELECT `".DB_users_allowed."`.*, `".DB_children."`.*, `".DB_employees."`.`vardas` AS `Dvardas`, `".DB_employees."`.`pavarde` AS `Dpavarde`
	FROM `".DB_users_allowed."`
	LEFT JOIN `".DB_children."` ON `".DB_users_allowed."`.`person_id`=`".DB_children."`.ID
	LEFT JOIN `".DB_employees."` ON `".DB_users_allowed."`.`person_id`=`".DB_employees."`.ID AND `".DB_employees."`.`isDeleted`=0
	WHERE `".DB_users_allowed."`.`user_id`=".(int)$row['user_id']);
	$hasType = false;
	while($row_inner = mysqli_fetch_assoc($result_inner)) {
		if(isset($_GET['person_type']) && $row_inner['person_type'] == $_GET['person_type'])
			$hasType = true;
		if($row_inner['person_type'] == PARENTS)
			$echo .= '<strong class="abbr" title="Gali prisijungti kaip">'.$person_subtype[$row_inner['person_subtype']].' vaiko: '.filterText($row_inner['vardas']).' '.filterText($row_inner['pavarde']).'</strong> <a href="vaikai?edit='.$row_inner['person_id'].'#child-form">Peržiūrėti/Redaguoti vaiką</a>';
		else
			$echo .= '<strong class="abbr" title="Gali prisijungti kaip">'.$person_type_who[$row_inner['person_type']].': '.filterText($row_inner['Dvardas']).' '.filterText($row_inner['Dpavarde']).'</strong> <a href="/darbuotojai?edit='.$row_inner['person_id'].'#employee-form">Peržiūrėti/Redaguoti darbuotoją</a>';
		//	$echo .= '<a href="?edit_join='.$row_inner['pavarde'].'">'.$person_type[$row_inner['person_type']];
		$echo .= ' <a href="?del_user_id='.$row_inner['user_id'].'&amp;del_person_type='.$row_inner['person_type'].'&amp;del_person_id='.$row_inner['person_id'].'" onclick="return confirm(\'Ar tikrai norite pašalinti šį leidimą?\')">[-] Pašalinti leidimą</a> 
		<br>';
	}
	$echo .= '<a href="?edit='.$row['user_id'].'&amp;add_rights#add_rights">[+] Pridėti leidimą (teisę)</a></td>
		<td><a href="?edit='.$row['user_id'].'#login-form">Keisti</a> <a href="?delete='.$row['user_id'].'" onclick="return confirm(\'Ar tikrai norite ištrinti?\')"> Trinti</a></td>
		</tr>';//</div>
	if(!isset($_GET['person_type']) || $hasType)
		echo $echo;
}
?>
</table>



<?php
if(isset($_GET['edit'])) {
	$result = db_query("SELECT * FROM `".DB_users."` WHERE `user_id`=".(int)$_GET['edit']);
	$edit = mysqli_fetch_assoc($result);
}
?>
<fieldset method="post" id="login-form-fieldset">
	<legend><?=(!isset($_GET['edit']) ? 'Naujas prisijungimas:' : 'Prisijungimo redagavimas')?></legend>
	<form action="?<?=(!isset($edit) ? 'add' : 'update='.(int)$edit['user_id'])?>" method="post" id="login-form" class="not-saved-reminder">
		<p><label>El. paštas<span class="required abbr" title="Būtina užpildyti">*</span>: <input required="required" type="email" name="email" style="width: 300px;" value="<?=(isset($edit) ? filterText($edit['email']) : '')?>"></label></p>
		<p><label>Vardas: <input type="text" name="name" style="width: 200px;" value="<?=(isset($edit) ? filterText($edit['name']) : '')?>"></label></p>
		<p><label>Pavardė: <input type="text" name="surname" style="width: 200px;" value="<?=(isset($edit) ? filterText($edit['surname']) : '')?>"></label></p>
		<?=(isset($edit) ? '<p>Sukurta: '.$edit['created'].'</p>' : '')?>
		<p><input type="submit" value="Išsaugoti (tik aukščiau esančius duomenis)" class="submit"></p>
	</form>
	<hr>
	<?php
	if(isset($_GET['edit'])) {
		//TODO: think about DB_children
		echo '<strong>Leidimai prisijungti kaip (teisės):</strong><br>';
		$result = db_query("SELECT  `".DB_users_allowed."`.*, `".DB_children."`.*, `".DB_employees."`.`vardas` AS `Dvardas`, `".DB_employees."`.`pavarde` AS `Dpavarde` 
		FROM `".DB_users_allowed."` 
		LEFT JOIN `".DB_children."` ON `".DB_users_allowed."`.`person_id`=`".DB_children."`.ID  
		LEFT JOIN `".DB_employees."` ON `".DB_users_allowed."`.`person_id`=`".DB_employees."`.ID AND `".DB_employees."`.`isDeleted`=0
		WHERE `".DB_users_allowed."`.`user_id`=".(int)$_GET['edit']);
		while($row = mysqli_fetch_assoc($result)) {
			echo ' <a href="?del_user_id='.$row['user_id'].'&amp;del_person_type='.$row['person_type'].'&amp;del_person_id='.$row['person_id'].'" onclick="return confirm(\'Ar tikrai norite pašalinti šį leidimą?\')">[-] Pašalinti leidimą</a> ';
			if($row['person_type'] == PARENTS)
				echo '('.$person_subtype[$row['person_subtype']].') Vaikas: <a href="vaikai?edit='.$row['person_id'].'#child-form">Peržiūrėti/Redaguoti duomenis '.filterText($row['vardas']).' '.filterText($row['pavarde']).'</a>';
			else
				echo $person_type[$row['person_type']].': <a href="/darbuotojai?edit='.$row['person_id'].'#employee-form">Peržiūrėti/Redaguoti duomenis '.filterText($row['Dvardas']).' '.filterText($row['Dpavarde']).'</a>';
			//	echo '<a href="?edit_join='.$row_inner['pavarde'].'">'.$person_type[$row_inner['person_type']];
			
			echo '<br>';
		}
		if(!isset($_GET['add_rights']))
				echo '<div style="margin-bottom: 100px;"></div>';
	}
	
	
	if(isset($_GET['add_rights'])) {
	?>
	<fieldset style="margin-top: 40px;" method="post">
	<legend><strong>Leidimų (teisių) pridėjimas</strong></legend>
	<form action="?add_right" method="post" id="add_rights">
		<input type="hidden" name="user_id" value="<?=$edit['user_id']?>">
		<?php /*<!-- <p><label>Prisijungimas: 
			
			<?php echo filterText($edit['name']).' '.filterText($edit['surname']) ?>
			
			<select name="user_id">
			<!-- <option value="0">Pasirinkite prisijungimą</option> -->
			<?
			$result = mysqli_query($db_link, "SELECT * FROM `".DB_users."`") or logdie('Neteisinga užklausa: '.mysqli_error($db_link));
			while ($row = mysqli_fetch_assoc($result)) {
				echo "<option value=\"".$row['user_id']."\"".(isset($_GET['edit']) && $row['user_id'] == $edit['user_id'] ? ' selected="selected"' : '').">".$row['name']." ".$row['surname']."</option>";
			}?>
			</select>
			
		</label></p> --><?php*/ ?>
		<p><label>Prisijungs kaip: 
			<div class="sel"><select name="person_type" id="person_type">
			<!-- <option value="0">Pasirinkite prisijungimo formą</option> -->
			<?
			foreach($person_type_extended as $key => $val)
				echo "<option value=\"".$key."\">".$val."</option>";
			?>
			</select></div>
		</label></p>
		<script type="text/javascript">
		jQuery(function($) {
			$("#person_type").change(function() {
				if($(this).val() == 0) {
					$("#child").show();
					$("#person_subtype").show();
					$("#employee").hide();
				} else {
					$("#child").hide();
					$("#person_subtype").hide();
					$("#employee").show();
				}
			});
		});
		</script>


		<div id="child"><label>Vaikas: 
			<div class="sel"><select name="child_id" >
			<!--<option value="">Pasirinkite vaiką</option> -->
			<?
			$result = mysqli_query($db_link, $get_kids_sql) or logdie('Neteisinga užklausa: '.mysqli_error($db_link));//"SELECT * FROM `".DB_children."` WHERE `isDeleted`=0 ORDER BY vardas, pavarde"
			while ($row = mysqli_fetch_assoc($result)) {
				echo "<option value=\"".$row['parent_kid_id']."\">".filterText($row['vardas'])." ".filterText($row['pavarde'])."</option>";
			}
			?>
			</select></div>
		</label></div>
		<div id="person_subtype"><label><!-- Kas per žmogus vaikui -->Koks žmogus jungiamas prie vaiko:
			<div class="sel"><select name="person_subtype">
			<!-- <option value="0">Pasirinkite prisijungimo formą</option> -->
			<?
			foreach($person_subtype as $key => $val)
				echo "<option value=\"".$key."\">".$val."</option>";
			?>
			</select></div>
		</label></div>
		<div id="employee" style="display: none"><label>Darbuotojas: 
			<div class="sel"><select name="employee_id">
			<!--<option value="0">Pasirinkite darbuotoją</option> -->
			<?
			$result = getValidEmployeesOrdered_db_query();
			while ($row = mysqli_fetch_assoc($result)) {
				echo "<option value=\"".$row['ID']."\">".filterText($row['vardas'])." ".filterText($row['pavarde'])."</option>";
			}
			?>
			</select></div>
		</label></div>
		<p><input type="submit" value="Pridėti leidimą (teisę)" class="submit"> <?php /*if(!isset($_GET['edit'])) {  }*/ ?></p>
	</form>
	</fieldset>
	<?php
	}
	?>
</fieldset>
</div>
