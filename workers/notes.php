<?php if(!defined('DARBUOT') && !DARBUOT) exit(); /*TODO: tik auklėtojoms? */
$max_opened_msg_delay = 15;//30 s.
?>
<h1>Grupės bendra užrašinė <span class="abbr no-print" title="Auklėtojai">(matomumas)</span></h1>
<div id="content">
<?php
if(isset($_POST['save'])) {
	db_query("INSERT INTO `".DB_notes."` SET 
		`title`='".db_fix($_POST['title'])."',
		`content`='".db_fix($_POST['content'])."',
		`kindergarten_id`=".DB_ID.",
		`group_id`='".GROUP_ID."',
		`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
	msgBox('OK', 'Užrašinės įrašas išsaugotas.');
}
if(isset($_POST['edit'])) {
	db_query("UPDATE `".DB_notes."` SET 
		`title`='".db_fix($_POST['title'])."',
		`content`='".db_fix($_POST['content'])."',
		`updated`=CURRENT_TIMESTAMP, `updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."',
		`updatedCounter`=`updatedCounter`+1
		WHERE `ID`=".(int)$_POST['edit']." AND `kindergarten_id`=".DB_ID." AND `group_id`='".GROUP_ID."'");
	msgBox('OK', 'Užrašinės įrašas atnaujintas.');
}
if(isset($_GET['delete'])) {
	db_query("DELETE FROM `".DB_notes."` WHERE `ID`=".(int)$_GET['delete']." AND `kindergarten_id`=".DB_ID." AND `group_id`='".GROUP_ID."'");
	msgBox('OK', 'Užrašinės įrašas ištrintas.');
}

?>
<a href="?new#note" class="no-print fast-action fast-action-add">Nauja užrašinė</a> <?=ui_print()?>
<table>
<tr><th>Pavadinimas</th><th class="no-print">Veiksmai</th><th class="no-print">Sukūrė</th><th class="no-print">Atnaujino</th></tr>
<?php
$result = db_query("SELECT *, UNIX_TIMESTAMP(`openedDate`) AS `openedDateTimestamp` FROM `".DB_notes."` WHERE `kindergarten_id`=".DB_ID." AND `group_id`='".GROUP_ID."' ORDER BY `title` DESC");
$first = true;
while($row = mysqli_fetch_assoc($result)) {
	$first_line = $first && !isset($_GET['edit']) && !isset($_GET['ID']) || isset($_GET['ID']) && $_GET['ID'] == $row['ID'] ? filterText($row['title']) : "<a href=\"?ID=".$row['ID']."\">".filterText($row['title'])."</a>";
	echo "<tr".($first && !isset($_GET['edit']) && !isset($_GET['ID']) || isset($_GET['edit']) && $_GET['edit'] == $row['ID'] || isset($_GET['ID']) && $_GET['ID'] == $row['ID'] ? ' class="opened-row"' : '').">
	<td>".$first_line."</td>
	<td class=\"no-print\">";
	if($row['openedDateTimestamp'] < time()-$max_opened_msg_delay)//if($row['openedDateTimestamp'] < time()-$max_opened_msg_delay && ($row['openedByEmployeeId'] != DARB_ID || $row['openedByUserId'] != USER_ID))
		echo " <a href=\"?edit=".$row['ID']."#note\">Keisti</a> ";
	else
		echo ' [Dabar redaguoja '.getAllEmployees($row['openedByEmployeeId'])." ".$row['openedDate']."]";
				
	echo " <a href=\"?delete=".$row['ID']."\" onclick=\"return confirm('Ar tikrai norite ištrinti užrašinės įrašą?')\">Trinti</a></td>
	<td class=\"no-print\">".$row['created']." ".getAllEmployees($row['createdByEmployeeId'])."</td>
	<td class=\"no-print\">".date_empty($row['updated'])." ".getAllEmployees($row['updatedByEmployeeId'])."</td>
	</tr>";
	if($first) {
		if(!isset($_GET['edit']) && !isset($_GET['ID']) || isset($_GET['ID']) && $_GET['ID'] == $row['ID']) {
			echo "<tr><td colspan=\"4\" class=\"opened-row\">".nl2br(filterText($row['content']))."</td></tr>";
		}
		$first = false;
	} else {
		if(isset($_GET['ID']) && $_GET['ID'] == $row['ID']) {
			echo "<tr><td colspan=\"4\" class=\"opened-row\">".nl2br(filterText($row['content']))."</td></tr>";
		}
	}
}
echo '</table>';

if (isset($_GET['edit']) || isset($_GET['new'])) {
	if (isset($_GET['edit'])) {
		$result = db_query("SELECT * FROM `".DB_notes."` WHERE `ID`=".(int)$_GET['edit']." AND `kindergarten_id`=".DB_ID." AND `group_id`='".GROUP_ID."'");
		$note = mysqli_fetch_assoc($result);
		
		$add_filter = '`group_id`='.GROUP_ID.' AND `kindergarten_id`='.DB_ID.' AND ';
		db_query("UPDATE `".DB_notes."` SET `openedByUserId`='".USER_ID."', `openedByEmployeeId`='".DARB_ID."', `openedDate`=CURRENT_TIMESTAMP() 
		WHERE $add_filter`ID`=".(int)$_GET['edit'].' LIMIT 1');
	}
	?>
	<fieldset id="note">
	<legend><?=(isset($_GET['edit']) ? 'Užrašinės įrašo keitimas' : 'Naujas užrašinės įrašas')?></legend>
	<form method="post" class="not-saved-reminder no-print">
		<p><label>Pavadinimas <input type="text" name="title" value="<?=(isset($note) ? filterText($note['title']) : '')?>"> (pvz., <?=date('Y-m')?> einamieji užrašai)</label></p>
		<p><label>Turinys <textarea name="content"><?=(isset($note) ? filterText($note['content']) : '')?></textarea></label></p>
		<p><input type="hidden" <?=(!isset($_GET['edit']) ? 'name="save"' : 'name="edit" value="'.(int)$note['ID'].'"')?><?=(isset($_GET['edit']) ? ' id="note_id"' : '')?>><input type="submit" value="Išsaugoti" class="submit"></p>
	</form>
	</fieldset>
	<script>
	$(function() {
		var note_id = $('#note_id').val();
		if(note_id) {
			function note_editing() {
				$.get('/workers/notes_edit.php?ID='+note_id);
			}
			note_editing();
			setInterval(note_editing, 10000);//5000 - 20 concurrent people - 0.05 s.
			window.onbeforeunload = function() {
				$.ajax('/workers/notes_edit.php?remove&ID='+note_id, {async : false});
			};
		}
	});
	</script>
<?php
}
?>
</div>
