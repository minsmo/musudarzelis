		<p><strong>2016-10-04</strong> Įdėjome priešpiečius ir naktipiečius. <?php if(ADMIN) { ?>Vadovai gali pasirinkti nuo kada kokie maitinimo kartai yra įstaigoje meniu skyriuje „Nustatymai“.<?php } ?></p>
		<p><strong>2016-09-29</strong> Socialiniam pedagogui pridėjome meniu skyrių „Vaiko veikla“, kur jis gali įkelti realią veiko veiklą ir prisegti nuotraukas prie auklėtojų grupių.</p>
		<?php if(LOGOPEDAS || ADMIN) { ?><p><strong>2016-09-23</strong> Logopedui patobulinome meniu skyrių „<?=$logo?> kita veikla“.</p><? } ?>
		<p><strong>2016-09-20</strong> Logopedui pridėjome meniu skyrių „Vaikai“.</p>
		<p><strong>2016-09-02</strong> Logopedui pridėjome meniu skyrių „Vaiko veikla“, kur jis gali įkelti realią veiko veiklą ir prisegti nuotraukas prie auklėtojų grupių.</p>
		<p><strong>2016-08-23</strong> Meniu skyriuje „Vaiko veikla“ įdėta paieškos/filtavimo galimybė.</p>
		<?php if(ADMIN) { ?><p><strong>2016-08-22</strong> Meniu skyriuje „Planavimo formų kūrimas“ įdėta planų formų kopijavimo galimybė.</p><?php } ?>
		
		<?php if(isset($_GET['more'])) { ?>
		<hr>
		Daugiau senesnių naujienų:
		<p><strong>2016-06-15</strong> Meniu skyriuose „Planavimas formose“ <?php if(ADMIN) { ?>ir „Planavimo formų kūrimas“<?php } ?> lentelėms rodomi pavadinimai<?php if(ADMIN) { ?>, juos pašalinti galima meniu skyriuje  „Planavimo formų kūrimas“ vietoje esamo teksto įrašant tarpą ar kita<?php } ?>.</p>
		<p><strong>2016-05-23</strong> Meniu skyriuje „Laiškai grupės tėvams“ įdėjome „Laiškų rašymo patarimai“. <a href="https://www.e-tar.lt/portal/lt/legalAct/TAR.AF02472A1EBF/eXZqYrlvxR" target="_blank">Tik priminimas, kad nuo 2016-05-01 įsigaliojo nauja redakcija Lietuvos higienos norma HN 75:2010 „Įstaiga, vykdanti ikimokyklinio ir (ar) priešmokyklinio ugdymo programą. Bendrieji sveikatos saugos reikalavimai“</a></p>
		<p><strong>2016-04-22</strong> Meniu punkto „Pasiekimų žingsneliai“ skaičiuojami vidurkiai lentelėje. Meniu punkte „Planavimas formose“ peržiūroje duomenų skyreliai labiau atskiri greitesniam skyrelių suradimui. </p>
		<p><strong>2016-04-11</strong> Išplėsta ataskaita meniu punkte „Darbuotojų maitinimas“ -&gt; „Ataskaita „Darbuotojai““.</p>
		<p><strong>2016-03-29</strong> Įdėtas meniu punktas „Info ikimokyklinis.lt“.</p>
		<p class="notice"><strong>2016-03-17</strong> Įdėtas meniu punktas „Darbų priminimai“.</p>
		<p class="notice"><strong>2016-02-14</strong> Meniu punkte „Pasiekimų žingsneliai“ įdėtas 1) kopijavimas įvertinimų iš senos datos į naują; 2) pasiekimų aprašymo trumpalaikis paslėpimas (nerodymas).</p>
		<p class="notice"><strong>2015-01-12</strong> Meniu punkte „Vaikai“ įdėtas pasirinkimas „Pradinė klasė“.</p>
		<p class="notice"><strong>2015-12-30</strong> Meniu punkte „Planavimas formose“ vienu metu filtruoti galima pagal kelis požymius. Muzikos pedagogui įdėtas meniu punktas „Pasiekimų žingsneliai“.</p>
		<p class="notice"><strong>2015-12-28</strong> Meniu punkte „Naudinga informacija“ įdėjome įsakymą, LR Sveikatos apsaugos ministerijos, įsigaliojantį nuo 2016-01-01, kuris papildo Lietuvos higienos normą HN 75:2010 „Įstaiga, vykdanti ikimokyklinio ir (ar) priešmokyklinio ugdymo programą. Bendrieji sveikatos saugos reikalavimai“.</strong></p>
		<p><strong>2015-12-23</strong> Artėja stebuklų metas - Kalėdų laikotarpis. Mums svarbu, kad Jums būtų lengva ir gera, todėl sukūrėme <strong><a href="/wishes">norų skyrelį</a></strong>, kuriame <strong>kviečiame išsakyti savo norus, svajones dėl „Mūsų darželis“ tobulėjimo</strong>. Iš širdies sieksime Jums svarbius troškimus patenkinti ir išpildyti, todėl kuo anksčiau pasieks ši informacija, tuo didesni šansai, kad mes galėsime išpildyti Jūsų troškimus.</p>
		<p><strong>2015-12-22</strong> Meniu punkte „Vaiko veikla“ galite rašyti <strong>apie vykusią veiklą darželyje, įkelti vaikų nuotraukas</strong>. Socialiniam pedadogui įdėtas meniu punktas „Planavimas formose“.</p>
		<p><strong>2015-12-21</strong> Meniu punkte „Tabeliai“ galima įsidėti papildomus stulpelius lentelėje papildomo turinio įrašymui tokiu pat būdu kaip stulpeliui „Pastabos“. Tai gali padaryti vadovai meniu punkte „Nustatymai“.</p>
		<p class="notice"><strong>2015-12-16</strong> Meniu punkte „Naudinga informacija“ įdėjome leidinį „<a target="_blank" href="http://www.smlpc.lt/media/media/vaiko_saugos_vadovas_LT_sumazintas.pdf">Produktų vaikams saugos vadovas. Potencialiai pavojingi produktai</a>“. Logopedui įdėjome meniu punktą „Vaiko pasiekimai“.</p>
		<p><strong>2015-12-11</strong> Meniu punkte „Maitinimas“ praplėstos galimybės: rodymo glaustai ir papildomas skaičiavimas dviejų maitinimų variantų.</p>
		<p><strong>2015-12-07</strong> Logopedui įdėtas meniu punktas „Pasiekimų žingsneliai“.</p>
		<p><strong>2015-11-27</strong> Meniu punkte „Pasiekimų žingsneliai“ suvienodintas naudojimas ir įdėta apsaugų.</p>
		<p><strong>2015-11-26</strong> Vaikų grupės auklėtojams patobulintas lankomumo žymėjimas.</p>
		<p><strong>2015-11-10</strong> Medicinos darbuotojui įdėtas meniu punktas „Tabeliai“.</p>
		<p><strong>2015-11-09</strong> Patobulinimai. Sukurta socialinio pedagogo leidimų grupė. Meniu punkte „Planavimas formose“ įdėtas filtravimas pagal mėnesį.</p>
		<p><strong>2015-11-06</strong> Patobulinti meniu punktai „Grupės“, „Prieiga“, „Numatoma veikla“.</p>
		<p><strong>2015-10-30</strong> Meniu punkte „Pasiekimų žingsneliai“ renkantis voratiklį paryškintas rodymas ir spausdinimas. Ar spausdinant tilps į vieną A4 lapą priklauso nuo nustatymų: saityno naršyklės (Google Chrome, Mozilla Firefox) ir nuo saityno naršyklės lango dydžio (lango dydis priklauso nuo ekrano raiškos), nes grafikas prisitaiko prie lango dydžio. Pavyzdžiui, naudojantis Mozilla Firefox, kad tilptų į vieną A4 lapą gali tekti sumažinti lango dydį ar/ir sumažinti mastelį (žr. meniu punktą „<a href="/spausdinti">Spausdinimo nustatymai</a>“).</p>
		<p><strong>2015-10-27</strong> Patobulinimai. Įdėtas meniu punktas „Tėvų meniu“.</p>
		<p><strong>2015-10-22</strong> Patobulintas meniu punktas „Pasiekimų žingsneliai“: 1) spausdinant diagramas pašalintas nereikalingas tekstas; 2) įdėta vertinimų peržiūra lentelės pavidalu<?php //, kurioje įverčio dydis perteikiamas vizualiai - foniniu kontrastu ?>; 3) nuo šiol papildomai ištrinti pasiekimus gali vadovai, anksčiau galėjo tik pirmasis darbuotojas išsaugojęs įvertinimą.</p>
		<p><strong>2015-10-19</strong> Patobulinimai. Naujos galimybės meniu punkte „Pasiekimų žingsneliai“.</p>
		<p><strong>2015-10-14</strong> Logopedui pridėta kelios galimybės meniu punktuose: lankomumo ataskaita, temos ir kita veikla.</p>
		<p><strong>2015-10-13</strong> Patobulinimai. <?php if(ADMIN) { ?>Vadovams meniu punkte „Planavimo formų kūrimas“ įdėtas punktas „Rinktis rodmenį“, kuriame galima pasirinkti vieną laukelį iš planavimo formos.<?php } ?> Meniu punkte „Planavimo formos“ įdėtas stulpelis „Papildoma info“.</p>
		<p><strong>2015-10-12</strong> Menų pedagogui įdėtas meniu punktas „Pasiekimų žingsneliai“.</p>
		<p><strong>2015-10-06</strong> Logopedams įdėtas meniu punktas „Tabeliai“. Meninio ugdymo pedagogams įdėtas meniu punktas „Vaiko pasiekimai“. Patobulinta ataskaita „Maitinimas rodomas juodai“ (įtraukiant ir spausdinimą) meniu punkte „Žiniaraštis (tabeliai)“.</p>
		<p><strong>2015-10-05</strong> Su Mokytojo diena! Meniu punkte „Žiniaraštis (tabeliai)“ įdėtas „Maitinimas rodomas juodai“.</p>
		<p><strong>2015-09-30</strong> Patobulintas meniu punktas „Spausdinimo nustatymai“. Meniu punkte „Tekstas“ galima prisegti vaiką be to teksto sukūrėjui arba vadovui keisti prisegtą grupę ir vaiką.</p>
		<p><strong>2015-09-28</strong> Pataisymai. Medicinos seselei įjungtas meniu punktas „Vaikai“. Tabeliuose papildoma ataskaita apie maitinimą.</p>
		<p><strong>2015-09-21</strong> Vadovams meniu punkte „Planavimo formos“ atsidarius archyvą arba/ir šiukšlinę pridėtas filtravimas pagal grupes.</p>
		<p><strong>2015-09-15</strong> Įdėtas meniu punktas „Numatomos veiklos“, renginių (projektų) planavimui.</p>
		<p><strong>2015-09-14</strong> Logiškesnis meniu punkto „Pasiekimų žingsneliai“ pavadinimas.</p>
		<p><strong>2015-09-08</strong> Prisijungiant naudotojui turinčiam ne vieną leidimų grupę rodomas išskleistas leidimų grupių sąrašas. Auklėtojoms vieno mygtuko paspaudimu galima pažymėti visas mėnesio vieno vaiko praleistas dienas kaip pateisintas.</p>
		<p><strong>2015-09-08</strong> Patobulinimai. Pedagogams įdėtas meniu punktas „Tekstas“. Vadovams įdėtas meniu punktas „Teksto formos“.</p>
		<p><strong>2015-09-07</strong> Patobulinimai. Meniu punkte „Darbuotojai“ skaičiuojamas eil. nr.</p>
		<p><strong>2015-09-01</strong> Mokslo metų pradžios proga linkime kūrybingumo ir sveikatos! P. S. Vadovams meniu punkte „Grupės“ nuėjus į „Vaikų išregistravimas“ galima pasirinktus vaikus oficialiai išregistruoti iš darželio nuo pasirinktos datos.</p>
		<p><strong>2015-08-26</strong> Vadovams meniu punkte „Grupės“ nuėjus į „Vaikų perkėlimas“ galima pasirinktus vaikus perkelti į kitas grupes nuo pasirinktos datos (vaikų grupių pavadinimus).</p>
		<p><strong>2015-08-12</strong> Įvairūs patobulinimai.<?php //Daugiau rodoma būsenų kur esama. Žingsnelių pasiekimai vadovams. Dienos ritmo atnaujinimą sutaisiau. ?></p>
		<p><strong>2015-07-27</strong> Logiškesnis meniu eiliškumas ir suskaidytas į skyrius. Pakeistas dizainas išryškinančiu turinio išdėstymą. Paprastesnis žingsnelių pasiekimų vertinimas. Įdėtas meniu punktas „Naudinga informacija“. </p>
		<p><strong>2015-05-25</strong> Meniu punkte „Tabeliai“ sumas nuo šiol gali keisti tik buhalteris ir vadovas.</p>
		<p><strong>2015-05-11</strong> Meniu punkte „Planavimas formose“ įdėta planavimų archyvavimo galimybė. Meniu punkte „Maitinimas“ pridėta eilutė „Nemokami pusryčiai“.</p>
		<p><strong>2015-04-29</strong> Patobulinimai. Truputį pagerintas spausdinimas užsiėmimų tvarkaraščio ir planavimo formų kūrimas vadovams.</p>
		<p><strong>2015-04-21</strong> Pridėtas failo įkėlimas meniu punkte „Vaiko pažanga, pasiekimai“.</p>
		<p><strong>2015-04-14</strong> Vadovams pagerinta darbo su sistema pradžia ir meniu punktas „Planavimo formų kūrimas“.</p>
		<p><strong>2015-04-07</strong> Vaikų grupių pedagogai gali rašytis užrašus grupei meniu punkte „Grupės užrašinė“.</p>
		<p><strong>2015-03-31</strong> Papildomai įdėtas savaitės dienų rodymas meniu punktuose „Tabeliai“ ir „Lankomumas“.</p>
		<p><strong>2015-03-30</strong> Planavime pridėtas lentelės formos sudarymas (patys vadovai gali įtraukti standartinę „Plaukimo takelio“ formą).</p>
		<p><strong>2015-03-23</strong> Pridėtas meniu punktas „Darbuotojų maitinimas“, kurį pildo turintys leidimą dietisto arba vadovo, o peržiūri buhalterė.</p>
		<p><strong>2015-03-16</strong> Tabeliuose pridėtas maitinimo rodymas užvedus pelės žymeklį ant kiekvienos oficialiai lankytinos vaiko dienos. Meniu punkte „Prieiga“ pridėtas filtravimas pagal leidimą.</p>
		<p><strong>2015-03-11</strong> Patobulinimai. Padidintas kiekis leidžiamų įvesti banko sąskaitų. Meniu punktas „Užsiėmimų tvark.araštis“ įdėtas meno pedagogui, kūno kultūros pedagogui, muzikos pedagogui, kinezeterapeutui ir seselei.</p>
		<p><strong>2015-03-09</strong> Patobulinimai. Tabeliuose pridėtas socialinio remtinumo stulpelis. Patobulintas užsiėmimų tvarkaraštis.</p>
		<p><strong>2015-03-05</strong> Patobulinimai.</p>
		<p><strong>2015-02-23</strong> Pridėtas logopedo, specialiojo pedagogo, tiflopedagogo ir surdopedagogo dienynų pildymas.</p>
		<p><strong>2015-02-09</strong> Pridėtas meniu punktas „Bendri dokumentai“ skirtas dokumentų atsisiuntimui tik darbuotojams arba tėvams ir darbuotojams. Buhalterės gali siųsti el. laiškus grupių tėvams.</p>
		<p><strong>2015-02-02</strong> Failų archyvavimas meniu punkte „Dokumentai“. Patobulinimai meniu punktuose „Vaikai“, „Užsiėmimų tvarkaraštis“.</p>
		<p><strong>2015-01-26</strong> Patobulinimas. Užsiėmimus ir jų tvarkaraštį nuo šiol gali įvesti ir auklėtojai.</p>
		<p><strong>2015-01-19</strong> Patobulinimai. Nuo šiol planavimą (formoje) perkeltą į šiukšliadėžę gali atstatyti darbuotojai pradėję rašyti planavimą<!-- jį pradėję rašyti darbuotojai -->. Meniu punkte „Maitinimas“ papildoma sąrašinė ataskaita.</p>
		<p><strong>2015-01-12</strong> Patobulinimai. Meniu punktą „Maitinimas“ nuo šiol mato ir auklėtojos. Meniu punkte „Maitinimas“ pridėti kiekvienam mėnesiui reikalingi porcijų kiekiai. Meniu punkte „Valgiaraštis“ esančių duomenų peržiūra įdėta auklėtojai<!-- Meniu punktas „Valgiaraštis“ jau mato ir auklėtojos (tik informacijos peržiūrai) -->. Meniu punkte „Vaikai“ esančių duomenų peržiūra įdėta buhalterei. Meniu punkte „Žiniaraštis (tabeliai)“ įdėtas eksperimentinis mokėjimo sumos išsaugojimas, keičiantis tik tabelių informaciją.</p>
		<p><strong>2015-01-07</strong> Patobulinimai. Papildymas 2014-12-22 naujienos: Papildomos kainos eurais turėtų būti įvestos sistemoje taip, kad iki 2014-12-31 būtų išlaikytas skaičiavimas litais (kitaip tariant įvedus kainas eurais galutinės sumos už laikotarpį iki 2014-12-31 turi nepasikeisti). Darbuotojai, kurių darželiai turi kainas įvedamas meniu punkte „Vaikai“, papildomai turi įvesti kainas eurais su galiojimo pradžia 2015-01-01.</p>
		<p><strong>2015-01-05</strong> Patobulinimai. Logopedo dienyno vedimas prieinamas tiems, kurie pranešė arba praneš, kad nori jį išbandyti.</p>
		<p><strong>2015-01-01</strong> Patobulinimai, patikslinimai ir naujos galimybės. Vadovai gali užsakyti duomenų archyvą meniu punkte „Archyvas“. Meniu punkte „Vaiko pažanga, pasiekimai“ galima pamatyti 30 naujausių pasiekimų. Jeigu meniu punkte „Vaikų pasiekimai ir pažanga“ nepridėjote leidimų informacijos peržiūrai tėvams, tai tėvai nematys Jūsų parašytos informacijos. Meniu punkte „Maitinimas“ skaičiuojamos papildomos eilutės gaunantiems tik pusryšius, tik pietus, tik pavakarius ir tik vakarienę.</p>
		<p><strong>2014-12-22</strong> Priminimas. Vadovai turi įvesti kainas eurais, įsigaliojančias nuo 2015-01-01 meniu punkte „Nustatymai“, kadangi nuo 2015-01-01 kainos bus skaičiuojamos eurais. Patobulinta spausdinimo išvaizda.</p>
		<p><strong>2014-12-15</strong> Patobulintas ir pataisytas meniu punktas „Maitinimas“, kuriame nuo šiol papildomai skaičiuojama bendra porcijų suma.</p>
		<p><strong>2014-12-08</strong> Pridėtas prisijungimas Kinezeterapeutui, kuris mato šiuos pagrindinius meniu punktus: „Planavimas formose“; „Vaiko pasiekimai“; „Susirašinėjimas“. Meniu punkte „Užsiėmimų tvarkaraštis“, užsiėmimą galima priskirti parinktiems vaikams, ne tik grupei.<!-- Užsiėmimų tvarkaraštį galima individualizuoti jau ir vaikui, ne tik grupei. --></p>
		<p><strong>2014-12-02</strong> Meniu punkte „Vaikų pasiekimai ir pažanga“ pridėti leidimai informacijos peržiūrai. Pažymėti naudotojai ateityje galės peržiūrėti informaciją.</p>
		<p><strong>2014-12-01</strong> Patobulinimai.</p>
		<p><strong>2014-11-24</strong> Patobulinimai ir pataisymai. Meniu punkte „Vaikai“ galima padaryti dalį nemokamo maitinimo. Dietistas ir medicinos darbuotojas gali sudaryti valgiaraštį ir pamatyti maitinimo patiekimui reikalingų porcijų kiekį, paskutinį ir vadovai gali matyti. Auklėtojos gali įvesti vaiko dienos ritmą. Kai kur pakartoti mygtukai spausdinimui.</p>
		<p><strong>2014-11-10</strong> Patobulinimai ir pataisymai. Vaikų tėvų el. paštus nuo šiol gali pakeisti ir auklėtojos, ne tik vadovai.</p>
		<p><strong>2014-11-03</strong> Patobulinimai ir patikslinimai.</p>
		<p><strong>2014-10-20</strong> Smulkūs patobulinimai ir patikslinimai.</p>
		<p><strong>2014-10-06</strong> Pridėjome vadovams planavimo formų kūrimą ir pavyzdines formas. Jų pildymą pedagogams. Papildėme išplėstinį tabelį. Auklėtojos gali tvarkyti dokumentų priminimus.</p>
		<p><strong>2014-10-03</strong> Atlikti svarbūs pakeitimai meniu punkte „Vaikai“. Todėl pirmiausia atkreipkite dėmesį į sistemos pakeitimus esančius meniu punktuose „Vaikai“ ir „Lankomumas“. Su „Mūsų darželis“ komanda susisiekti galite nurodytais kontaktais meniu punkte „„Mūsų darželis“ kontaktai“.</p>
		<?php } else { ?>
		<a href="?more">Daugiau senesnių naujienų</a>
		<?php } ?>
