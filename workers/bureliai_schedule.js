$(function() {
	var kidsDiv = $('#kids_list');
	$('#add_kid').on('click', function(event) {
		event.preventDefault();
		var id = $("#kidId").val();
		if(!$("#kid"+id).length){
			$('<p>'+$("#kidId option:selected").text()+' <input type="hidden" name="kids[]" value="'+id+'"> <a href="#" class="remKid" data-kid-id="'+id+'" id="kid'+id+'">- Trinti</a></p>').appendTo(kidsDiv);
			DONE("Vaikas „"+$("#kidId option:selected").text()+"“ buvo pridėtas į sarašą!");
		}else
			alert("Šis vaikas jau pridėtas.");
	});
	$(document).on('change', '#kidId', function () {
		if($(this).val())
			$(this).find('option[value='+$(this).val()+']').hide();
		$('#add_kid').click();
		$(this).val('');
	});
	$(document).on('click', '.remKid', function (event) {
		event.preventDefault();//return false;
		$(this).parent().parent().parent().find('option[value='+$(this).attr('data-kid-id')+']').show();
		$(this).parents('p').remove();
	});
	$('#groupOrKids').change(function() {
		if($(this).val() == 0) {
			$('#kidSelect').show();
		} else {
			$('#kidSelect').hide();
		}
		//if($(this).val())
	});

	$('#kids_list > p').each(function() {
		$('#kidId').find('option[value='+$(this).find('a').attr('data-kid-id')+']').hide();
	});
});
