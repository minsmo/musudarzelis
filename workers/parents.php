<?php if(!defined('DARBUOT')) exit(); ?>
<h1>Tėvai matys/mato meniu punktus</h1>
<div id="content">
<?php
if(isset($_POST['save'])) {
//KaunoVaikystesLobiai || TESTINIS || LazdijuKregzdute || LazdijuVyturelis || TauragesRAzuoliukas || LazdijuRAzuoliukas || PasvalioLiepaite || DruskininkuBitute || KretingosRGruslaukesPagrMok
	foreach($_POST['ok'] as &$id) {
		$id = (int)$id;
	}
	unset($id);//Warning http://php.net/manual/en/control-structures.foreach.php
	$menu_items = [];
	$result = db_query("SELECT `menu_item_id` FROM `1parents_menu_access` WHERE `kindergarten_id`=".DB_ID);
	while($row = mysqli_fetch_assoc($result))
		$menu_items[$row['menu_item_id']] = '';
	$sql_insert = array();
	foreach($_POST['ok'] as $menu_item_id)
		if(!isset($menu_items[$menu_item_id]))
			$sql_insert[] = "(".DB_ID.", ".(int)$menu_item_id.", '"./*createdByUserId*/USER_ID."', '"./*createdByEmployeeId*/DARB_ID."')";
	if(!empty($sql_insert))
		db_query("INSERT INTO `1parents_menu_access` (`kindergarten_id`, `menu_item_id`, `createdByUserId`, `createdByEmployeeId`) VALUES ".implode(',', $sql_insert));
	db_query("DELETE FROM `1parents_menu_access` WHERE `kindergarten_id`=".DB_ID. (isset($_POST['ok']) ? " AND `menu_item_id` NOT IN (".implode(',', $_POST['ok']).")" : ''));
	msgbox('OK', "Sėkmingai išsaugoti meniu punktai rodymui tėvams.");
}
if(isset($_POST['plan'])) {
	db_query("UPDATE `0db` SET `plan_to_allow_parents`='".db_fix($_POST['plan'])."' WHERE `id`=".DB_ID);
	$toemail = 'forkik@gmail.com';
	sendemail('Administratoriui', $toemail, 'Robotas', 'info@musudarzelis.lt', 'Darželio planuojama data įjungti+išsiųsti tėvų prisijungimus', 'Darželis <strong>'.DB_PREFIX.'</strong> planuoja <strong>'.filterText($_POST['plan']).'</strong> įjungti+išsiųsti tėvų prisijungimus.', "html", "", "ramunerup@gmail.com");
	msgbox('OK', "Sėkmingai išsaugota plano data.");
}



if(ALLOW_PARENTS) {
	echo '<p>Tėvai jau gali prisijungti prie sistemos „Mūsų darželis“. Jei jų el. pašto adresas įvestas teisingai, bet jie neranda savo slaptažodžio net ir šlamšto skyrelyje, tuomet jie gali atsisiųsti naują slaptažodį per funkciją „Pamiršau slaptažodį“.</p>';
	if(ADMIN)
		echo '<p class="notice">Pastaba. Meniu punkto „<em>Planai formose (ugdymo)</em>“ rodymui tėvams nurodykite tėvus prie duomenų laukelių meniu skyriuje „<a href="/planavimo_formos">Planavimo formų kūrimas</a>“ per „Keisti sudėtį“; Pastabos: Bet kada galėsite pakeisti rodomus meniu punktus tėvams. Rekomenduojame neįjunkti tuščių meniu punktų, nes jei tėvai nusivils pirmą kartą atsidarę tuščią meniu punktą, kitą kartą jau nebesitikės rasti turinį net jei jis bus įdėtas, todėl geriau įjunkite tik tada kai jau bus turinys.</span></p>';
} else
	echo '<p><strong>NEĮJUNGTI prisijungimai tėvams</strong>. Tėvai negali prisijungti prie sistemos „Mūsų darželis“. TODĖL: Įvedant vaikus su el. pašto adresais meniu punkte „Vaikai“, tėvams nebus išsiųsti prisijungimo duomenys. Tad el. paštus veskite drąsiai :)<br>Tik kai būsite pripratę prie sistemos, jausitės pasiruošę ir nuspręsite prijungti tėvus, tuomet reikės:<br>
	1) žemiau nurodyti (nurodys vadovo teises turintis asmuo) meniu punktus rodomus tėvams. Jei įjungėte meniu punkto „<em>Planai formose (ugdymo)</em>“ rodymą tėvams, tuomet įjunkite (jei neįjungti) duomenų laukelius rodomus tėvams meniu punkte „<a href="/planavimo_formos">Planavimo formų kūrimas</a>“; <span class="notice">Pastabos: Bet kada galėsite pakeisti rodomus meniu punktus tėvams. Rekomenduojame neįjunkti tuščių meniu punktų, nes jei tėvai nusivils pirmą kartą atsidarę tuščią meniu punktą, kitą kartą jau nebesitikės rasti turinį net jei jis bus įdėtas, todėl geriau įjunkite tik tada kai jau bus turinys.</span><br>
	2) iš anksto informuoti komandą „Mūsų darželis“ (planuojamą datą vadovai gali nurodyti apačioje, o kita suderinti el. paštu), kad būtų laiku įjungti prisijungimai tėvams ir išsiųsti prisijungimai tėvams, kurių el. pašto adresai jau įvesti meniu punkte „Vaikai“.</p>';
?>
<span  class="present">Žalias fonas - rodymui</span>, <span  class="absent">raudonas fonas - nerodymui</span>.
<p>Automatiškai matys:</p>
<ul>
	<li class="present">Naudinga informacija</li>
	<li class="present">Susirašinėjimas</li>
	<li class="present">Slaptažodžio keitimas</li>
	<li class="present">Atsijungti</li>
</ul>
<script>
$(function() {
	function eachCheckbox() {
		$("input[type='checkbox']").each(function() {
			//var thisCheck = $(this);
			if ( this.checked ) {//thischeck.is(':checked')
				$(this).parent().removeClass("absent");
				$(this).parent().prev().removeClass("absent");
				$(this).parent().addClass("present");
				$(this).parent().prev().addClass("present");
			} else {
				$(this).parent().addClass("absent");
				$(this).parent().prev().addClass("absent");
				$(this).parent().removeClass("present");
				$(this).parent().prev().removeClass("present");
			}
		});
	}
	eachCheckbox();
	$(":checkbox").click(function() {
		setTimeout(eachCheckbox, 1);
	});
});
</script>
Pasirinkimas ką rodyti tėvams:
<?php if(ADMIN) { ?>
<form method="post">
	<table>
		<tr>
			<th>Meniu punktas</th>
			<th>Rodyti</th>
		</tr>
	<?php
	$result = db_query("SELECT * FROM `1parents_menu_access` WHERE `kindergarten_id`=".DB_ID);
	$used = [];
	while($row = mysqli_fetch_assoc($result))
		$used[$row['menu_item_id']] = '';
	foreach($parents_menu_items as $key => $pair)
		echo '<tr><td>'.$pair['title']."</td><td><input type=\"checkbox\" name=\"ok[]\" value=\"$key\"".(isset($used[$key]) ? ' checked="checked"' : '')."></td></tr>";
	?>
	</table>
	<input name="save" type="submit" class="submit" value="Išsaugoti">
</form>
<?php } else {
	$result = db_query("SELECT * FROM `1parents_menu_access` WHERE `kindergarten_id`=".DB_ID);
	$used = [];
	while($row = mysqli_fetch_assoc($result))
		$used[$row['menu_item_id']] = '';
	echo '<ul>';
	foreach($parents_menu_items as $key => $pair) {
		echo '<li'.(isset($used[$key]) ? ' class="present"' : ' class="absent"').'>'.$pair['title']."</li>";
	}
	echo '</ul>';
}

if(!ALLOW_PARENTS && ADMIN) {
?>
<hr>
<br>
<form method="post">
<?php
$result = db_query("SELECT `plan_to_allow_parents` FROM `0db` WHERE `id`=".DB_ID);
$row = mysqli_fetch_assoc($result);
?>
Planuojama data įjungti ir išsiųsti prisijungimus tėvams <input type="text" class="datepicker" name="plan" value="<?=filterText(isset($row['plan_to_allow_parents']) ? $row['plan_to_allow_parents'] : '')?>">
<input type="submit" value="Išsaugoti" class="submit" style="margin:0">
</form>
<br><br>
<?php } ?>
</div>
