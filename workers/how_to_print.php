<?php //if(!defined('DARBUOT')) exit(); ?>
<h1>Instrukcijos spausdinimui - kaip panaikinti teksto antraštes<!-- Spausdinamų antraščių teksto panaikinimo nustatymo langai --></h1>
<div id="content">
<div style="line-height: 35px; vertical-align: bottom;">
Saityno naršyklės kalba: <label><input type="radio" name="language" value="lt" required="required"> <strong>lietuvių</strong></label> <label><input type="radio" name="language" value="en"> <strong>anglų</strong></label>
</div>
<div style="height: 40px;">
	<div style="display: inline-block; padding: 7px 0px; margin-right: 47px; vertical-align: top;">Saityno naršyklė:</div>
	<label><input type="radio" name="browser" value="mozilla" style="margin-top: 5px; vertical-align: top;" required="required"><img src="/img/print/firefox.png" height="40"></label>
	<label style="margin-left: 20px;"><input type="radio" name="browser" value="webkit" style="margin-top: 5px; vertical-align: top;"><img src="/img/print/Google_Chrome.png" height="40"></label>
</div>
<style>
.button {
padding: 1px 10px;
background: #f0f0f0;
background: -moz-linear-gradient(top,#f0f0f0,#fcfcfc);
background: -webkit-gradient(linear,center top,center bottom,from(#f0f0f0),to(#fcfcfc));
-webkit-border-radius: 3px;
-moz-border-radius: 3px;
border-radius: 3px;
-webkit-background-clip: padding-box;
-moz-background-clip: padding;
background-clip: padding-box;
color: #303030;
border: 1px solid #e0e0e0;
border-bottom-width: 2px;
white-space: nowrap;
}
</style>

<button class="filter" id="show">Rodyti</button>

<div id="lt-webkit" class="tutorial">
	<img src="/img/print/webkit_0.png"><br><br>
	<img src="/img/print/webkit_1.png"><br><br>
	<img src="/img/print/webkit_2.png">
</div>
<div id="en-webkit" class="tutorial">
	Žr. lietuviškai
</div>
<div id="lt-mozilla" class="tutorial">
	<img src="/img/print/mozilla_1.png"><br><br>
	<img src="/img/print/mozilla_2.png"><br><br>
	<img src="/img/print/mozilla_3.png"><br><br>
	<img src="/img/print/mozilla_4.png"><br><br>
	<img src="/img/print/mozilla_5_print.png"><br><br>
	<img src="/img/print/mozilla_5_printFinal.png"><br><br>
	
	Retai, bet galite matyti tokį langą, kuriame nueitumėte į kortelę „Nuostatos“ ir parinktumėte tuščias puslapines antraštes ir poraštes:<br>
	<img src="/img/print/lt_mozilla_spausdinimo_nuostatos.png">
</div>
<div id="en-mozilla" class="tutorial">
	Spustelkite ant meniu mygtuko <img src="/img/print/2014-01-10-13-08-08-mozilla.png" alt="New Fx Menu" style="border: none;"> ir tada spustelkite ant
	<span class="button">Print</span><br><br>
	<img src="/img/print/2014-05-30-14-16-53-mozilla.png"><br><br>
	Spustelkite ant mygtuko <span class="button">Page Setup</span> ir tada spustelkite ant kortelės „Margins & Header/Footer“, tada skiltyje „Headers & Footers“ nustatymus pakeiskite į „--blank--“ (tuščia):<br><br>
	<img src="/img/print/Firefox-PageSetup4.jpg"><br><br>
	
	<img src="/img/print/mozilla_4.png"><br><br>
	<img src="/img/print/mozilla_5_print.png"><br><br>
	<img src="/img/print/mozilla_5_printFinal.png"><br><br>
</div>
<script type="text/javascript" src="/libs/jquery-migrate-1.2.1.min.js"></script>
<script>
$(function(){
	var userLang = navigator.language || navigator.userLanguage;
	userLang = userLang.substr(0, 2);//for cases: en-us, lt-lt, en-gb
	$( "[value="+userLang+"]" ).prop( "checked", true );
	
	if ( $.browser.webkit ) {
		$( "#output" ).text(userLang+'webkit');
		$( "[value=webkit]" ).prop( "checked", true );
	}
	if ( $.browser.mozilla ) {
		$( "#output" ).text(userLang+'mozilla');
		$( "[value=mozilla]" ).prop( "checked", true );
	}
	
	
	$( "#show" ).click(function() {
		//$( "#output" ).toggle();//'slow'
		var id = $( "[name=language]:checked" ).val()+'-'+$( "[name=browser]:checked" ).val();
		console.log(id);
		$('.tutorial').hide();
		$('#'+id).show();
	});
});
</script>
</div>
