$(function() {
	var scntDiv = $('#p_scents');
	//var added = {};
	$('#addScnt').on('click', function(event) {
		event.preventDefault();
		//arrKids[i]+" <input class='individual_plan' type='text' name='individuali_veikla["+arrKidsIds[i]+"]' onchange='saveInputValue("+i+",this.value)' value='"+ value +"'>
		var id = $("#kidId").val();
		//if(!(id in added) ) {
		if(!$("#individual"+id).length) {
			$('<p><label>'+$("#kidId option:selected").text()+' <input type="text" class="individual_plan" name="individuali_veikla['+id+']"></label> <a href="#" class="remScnt" data-kid-id="'+id+'" id="individual'+id+'">- Trinti šį laukelį</a></p>').appendTo(scntDiv);// data-id="'+id+'"
			scntDiv.find('input:last').focus();
			DONE("Vaikas „"+$("#kidId option:selected").text()+"“ buvo pridėtas į sarašą!");
			//added[id] = true;
		} else
			alert("Šiam vaikui laukelis jau pridėtas, kuris skirtas individualiai vieklai įrašyti.");
		//return false;
	});
	$(document).on('change', '#kidId', function () {
		if($(this).val())
			$(this).find('option[value='+$(this).val()+']').hide();
		$('#addScnt').click();
		$(this).val('');
	});
	$(document).on('click', '.remScnt', function (event) {//$('#remScnt').live('click', function() { 
		event.preventDefault();//return false;
		//delete added[$(this).data("id")];
		$(this).parent().parent().parent().find('option[value='+$(this).attr('data-kid-id')+']').show();
		$(this).parents('p').remove();
	});
	var tbody = $('#tbody');
	var uniq = $('#tbody tr').size();
	$('#addRow').on('click', function(event) {
		event.preventDefault();
		var size = document.getElementById("tbody").getElementsByTagName("tr").length+1;
		/*
		</th>
				<th>Vaiko pasiekimai, kurie vestų į galutinį rezultatą</th>
				<th>Priemonės</th>
				<th>Galutinis vaiko pasiekimų rezultatas pasiekus uždavinį</th>
		*/
		$('<tr><td>'+size+'</td><td><textarea name="veiklaVisaiVaikuGrupei['+uniq+'][VeiklosPavadinimas]"></textarea></td> <td><textarea name="veiklaVisaiVaikuGrupei['+uniq+'][Priemones]"></textarea></td><td><textarea name="veiklaVisaiVaikuGrupei['+uniq+'][GalutinisRezultatas]"></textarea></td><td><a href="#" class="remRow">Trinti šią eilutę</a></td></tr>').appendTo(tbody);
		//<td><textarea name="veiklaVisaiVaikuGrupei['+uniq+'][VaikoPasiekimai]"></textarea></td>
		++uniq;
	});
	$(document).on('click', '.remRow', function (event) {//$('#remScnt').live('click', function() { 
		event.preventDefault();//return false;
		$(this).parents('tr').remove();
		//$('#tbody tr td:first-child').text('dddd');
		$('#tbody tr').each(function(i, el){
			$(this).children('td').first().text(i+1);
		});
	});
	$('#p_scents > p').each(function() {
		$('#kidId').find('option[value='+$(this).find('a').attr('data-kid-id')+']').hide();
	});
	
	
	/*var plan_id, check_updated = true;
	$("#planning-submit").click(function(e) { //_form //-submit
		// Handler immediately detaches itself, so that
		// we don't have an infinite loop when we call
		// $(this).submit() ourselves below
		var this_ = $(this);
		plan_id = $('#plan_id').val();
		if(plan_id && check_updated) {
			check_updated = false;
			e.preventDefault();
			//var $form = $(this);
			//e.preventDefault();
			//$.ajaxSetup({async:false});
			$.get('/workers/planning_updated.php?plan_id='+plan_id, function(data) {
				console.log(data);
				var updated = $('#updated').val();
				console.log(updated);
				console.log(data == updated);
				if(data == updated) {
					//$form.off("submit").submit();
					//return true;
					//$form.submit();
					//this_.unbind('submit').submit();
					$("#planning-submit").submit();
				} else {
					$('#dataChange').show();
					//return false;
				}
			});
			//$.ajaxSetup({async:true});
			//return false;
			//return false;
			/*
			//$(this).unbind('submit');
			e.preventDefault();
			// Do the AJAX
			//http://stackoverflow.com/questions/6318073/how-to-send-post-with-jquery-onsubmit-but-still-send-form
			var this_ = this;//$(this);
			$.ajax({
				url: '/workers/planning_updated.php?plan_id='+plan_id,
				type: 'GET',
				timeout: 1000,
				success: function(data) { 
					console.log(data);
					var updated = $('#updated').val();
					console.log(updated);
					console.log(data == updated);
					if(data == updated) {
				        // make sure that you are no longer handling the submit event; clear handler
				        this_.off('submit');
				        // actually submit the form
		            	this_.submit();
		            }
		         }
			});
			/.done(function (data) {
				console.log(data);
				var updated = $('#updated').val();
				console.log(updated);
				console.log(data == updated);
				if(data == updated) {
					//$(this).unbind('submit').submit();
					//check = false;
					//this_.unbind('submit').submit();
					//$(this).submit();
					//this_.submit();
					//http://stackoverflow.com/questions/17885843/ajax-request-before-regular-form-is-being-submitted
					// make sure that you are no longer handling the submit event; clear handler
		            this_.off('submit');
		            // actually submit the form
		            this_.submit();
				} else {
					$('#dataChange').show();
				}
			}).fail(function (jqXHR, textStatus) {
				console.log(jqXHR);
				console.log(textStatus);
				$('#dataChange').show();//TODO: inform about availability
			});/

			// Prevent the browser from submitting it NOW,
			// because the AJAX is still running
			//return false;
			/
		}
	});*/
	/*var plan_id, check_updated = true, updated;
    $(document).on('submit', '#planning_form', function(e) {
		var this_ = $(this);
		plan_id = $('#plan_id').val();
		if(plan_id && check_updated) {
			//$(this).unbind('submit');
			e.preventDefault();
			$.ajax({
			  url: '/workers/planning_updated.php?plan_id='+plan_id,
			  //processData: false,
			  //contentType: false,
			  type: 'GET',
			  timeout: 1000//,
			  //success: function(data){
			  //}
			}).done(function (data) {
				console.log(data);
				updated = $('#updated').val();
				console.log(updated);
				console.log(data == updated);
				if(data == updated) {
					//$(this).unbind('submit').submit();
					check_updated = false;
					this_.unbind('submit').submit();
				} else {
					$('#dataChange').show();
				}
			}).fail(function (jqXHR, textStatus) {
				console.log(jqXHR);
				console.log(textStatus);
				$('#dataChange').show();//TODO: inform about availability
			});
			return false;//http://stackoverflow.com/questions/6318073/how-to-send-post-with-jquery-onsubmit-but-still-send-form
			//http://stackoverflow.com/questions/11172811/jquery-on-submit-doesnt-work
		} else {
			return true;
		}
	});*/
	/*var plan_id, updated;
	$('#planning_form').submit(function(e) { 
		// this code prevents form from actually being submitted
		e.preventDefault();
		e.returnValue = false;

		var $form = $(this);

		// this is the important part. you want to submit
		// the form but only after the ajax call is completed
		$.ajax({ 
			type: 'post',
			url: someUrl, 
			context: $form, // context will be "this" in your handlers
			success: function() { // your success handler
			},
			error: function() { // your error handler
			},
			complete: function() { 
				// make sure that you are no longer handling the submit event; clear handler
				this.off('submit');
				// actually submit the form
				this.submit();
			}
		});
	});*/
	
	/*
	Maybe, but not guaranteed:
	var is_dirty = false;
	$(":input").change(function() {//input, #form textarea
		is_dirty = true;
	});*/
});

function isAbleToSubmit() {
	var plan_id = $('#plan_id').val(), updated;
	if(plan_id) {
		$.ajax({
		  url: '/workers/planning_updated.php?plan_id='+plan_id,
		  //processData: false,
		  //contentType: false,
		  type: 'GET',
		  timeout: 1000//,
		  //success: function(data){
		  //}
		}).done(function (data) {
			console.log(data);
			updated = $('#updated').val();
			console.log(updated);
			console.log(data == updated);
			if(data == updated) {
				//$(this).unbind('submit').submit();
				//check_updated = false;
				//this_.unbind('submit').submit();
				//$('#planning_form').submit();
				document.editForm.submit();
			} else {
				$('.updated').text(data);
				$('#dataChange').show();
				$('#dataChangeBar').show();
			}
		}).fail(function (jqXHR, textStatus) {
			console.log(jqXHR);
			console.log(textStatus);
			$('#dataChange').show();//TODO: inform about availability
			$('#dataChangeBar').show();
		});
	}
	//http://stackoverflow.com/questions/19129463/form-submit-and-ajax-at-the-same-time-using-onsubmit
	//http://stackoverflow.com/questions/1263526/sending-an-ajax-request-before-form-submit/1263534#1263534
	//Idea: http://jquery.10927.n7.nabble.com/Waiting-on-ajax-request-before-form-submit-td94989.html
}

/*
var arrInputValues = new Array(0);
var arrKidsIds = new Array(0);
var arrKids = new Array(0);

function addInput() {
	var e = document.getElementById("kidId");
	var strKidId = e.options[e.selectedIndex].value;
	var strKidName = e.options[e.selectedIndex].text;
	arrInputValues.push("");
	arrKidsIds.push(strKidId);
	arrKids.push(strKidName);
	display();
}

function display() {
	var str = "";
	var size = arrInputValues.length;
	for (intI=0;intI<size;intI++) {
		str += createInput(intI, arrInputValues[intI]);
	}
	document.getElementById('parah').innerHTML=str;
}

function saveInputValue(intI,strValue) {
	arrInputValues[intI]=strValue;
}

function createInput(i,value) {
  	return arrKids[i]+" <input class='individual_plan' type='text' name='individuali_veikla["+arrKidsIds[i]+"]' onchange='saveInputValue("+i+",this.value)' value='"+ value +"'><br>\n";
}

function deleteInput() {
	if (arrInputValues.length > 0) {
		arrInputValues.pop();
		arrKids.pop();
	}
	display();
}
display();
*/
