<?php if(!defined('DARBUOT')) exit();
$max_opened_msg_delay = 15;//30 s.

abstract class PlanningFieldPermissions//Access rights
{
    const Edit = 0;
    const OnlyView = 1;
}
?>
<h1>Vaikų grupių veiklos planai</h1>
<div id="content">
<p class="notice">Jeigu kas nors redaguoja planavimą, po <?php echo $max_opened_msg_delay; ?> s. galite pasitikrinti (ar jis vis dar redaguojamas) dar kartą atsidarę šį meniu punktą.</p>
<?php
$grupes = getAllGroups();
$grupes[0] = 'Neįvesta';
	
$form_title = array();
$result = db_query("SELECT `form_id`, `title` FROM `".DB_forms."` WHERE `isDeleted`=0");
while ($row = mysqli_fetch_assoc($result))
	$form_title[$row['form_id']] = $row['title'];

$add_filter = '';
if(!ADMIN)
	$add_filter = '`group_id`='.GROUP_ID.' AND ';

if(TESTINIS) {//KaunoZiburelis
	$KaunoZiburelis = KaunoZiburelis;
	/*echo (KaunoZiburelis ? 'DEBUG: TAIP' : 'DEBUG: NE').'<br>';
	
	//uopz_undefine('KaunoZiburelis');//PECL uopz >= 1.0.0 http://php.net/manual/en/function.uopz-undefine.php
	//PECL runkit >= 0.7.0 http://stackoverflow.com/questions/6455877/can-you-undefine-or-change-a-constant-in-php
	define('KaunoZiburelis', true);
	echo (KaunoZiburelis ? 'DEBUG: TAIP' : 'DEBUG: NE');*/
}

if(isset($_POST['save']) && !ADMIN) {
	if (!mysqli_query($db_link, "INSERT INTO `".DB_forms_fields_answer."` SET `form_id`='".(int)$_POST['save']."',
		`group_id`=".GROUP_ID.", `fromDate`='".db_fix($_POST['fromDate'])."', `toDate`='".db_fix($_POST['toDate'])."',
        `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'")) {
		logdie('Bloga užklausa: ' . mysqli_error($db_link));
	} else {
		$answer_id = (int)mysqli_insert_id($db_link);
		
		$res = db_query("SELECT * FROM `".DB_forms_fields_allowed."` WHERE `form_id`=".(int)$_POST['save']." AND `action`=".PlanningFieldPermissions::Edit);
		$EditAllowed = array();
		while($row = mysqli_fetch_assoc($res))
			$EditAllowed[$row['field_id']][$row['allowed_for_person_type']] = '';
		$res = db_query("SELECT * FROM `".DB_forms_fields."` WHERE `form_id`=".(int)$_POST['save']);
		$fields = array();
		while($row = mysqli_fetch_assoc($res)) {
			$fields[$row['field_id']]['type'] = $row['type'];
			$fields[$row['field_id']]['isRequired'] = $row['isRequired'];
		}
		
		if(isset($_POST['f'])) foreach($_POST['f'] as $field_id => $answer) {
			if( ADMIN || isset($EditAllowed[$field_id][$_SESSION['USER_DATA']['person_type']]) ) {
				if($fields[$field_id]['type'] == 4 && $KaunoZiburelis) {
					$answer = serialize($answer);//Būdavo dar isset.
				}
				if($fields[$field_id]['type'] >= 10) {
					unset($answer['dumb']);
					/*
					if(!ADMIN) {
						foreach($answer as $key => $val) {
							$res = db_query("SELECT * FROM `".DB_forms_widget_fields_allowed."` WHERE `kindergarten_id`=".DB_ID." AND `field_id`=".(int)$key.' AND `action`=0 AND `allowed_for_person_type`='.$_SESSION['USER_DATA']['person_type']);
							if(mysqli_num_rows($res) == 0)
								unset($answer[$key]);
						}
					}*/
					$answer = json_encode($answer);			
				}
				db_query("INSERT INTO `".DB_forms_fields_answers."` SET 
					`answer_id`=".$answer_id.", `field_id`='".(int)$field_id."',
					`answer`='".db_fix(trim($answer))."', `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
		    }
		}

		if(isset($_POST['individuali_veikla'])) foreach($_POST['individuali_veikla'] as $kid_id => $activity) {
			db_query("INSERT INTO `".DB_forms_fields_answers_i."` SET `answer_id`='".(int)$answer_id."', `child_id`='".(int)$kid_id."',  
					`activity`='".db_fix(trim($activity))."', `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
		}
			
		msgBox('OK', 'Planavimo informacija išsaugota sėkmingai!');
	}
}
if(isset($_POST['edit'])) {
	$answer_id = (int)$_POST['edit'];
	$res = db_query("SELECT * FROM `".DB_forms_fields_answer."` WHERE `answer_id`='".$answer_id."'");
	if(mysqli_num_rows($res) > 0) {
		$forFormId = mysqli_fetch_assoc($res);
		$allowedToChangePeriod = ($forFormId['createdByEmployeeId'] == DARB_ID || ADMIN);
		if (!mysqli_query($db_link, "UPDATE `".DB_forms_fields_answer."` SET
			".($allowedToChangePeriod ? "`fromDate`='".db_fix($_POST['fromDate'])."', `toDate`='".db_fix($_POST['toDate'])."'," : '')."
		    `updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."', `updated`=CURRENT_TIMESTAMP(), `updatedCounter`=`updatedCounter`+1
		    WHERE `answer_id`='".$answer_id."'")) {
			logdie('Bloga užklausa: ' . mysqli_error($db_link));
		} else {
			/*if(ADMIN)
				db_query("DELETE FROM `".DB_forms_fields_answers."` WHERE `answer_id`=".(int)$answer_id);
			else
				db_query("DELETE `".DB_forms_fields_answers."` 
					FROM `".DB_forms_fields_answers."` JOIN `".DB_forms_fields_allowed."` ON `".DB_forms_fields_answers."`.`field_id`=`".DB_forms_fields_allowed."`.`field_id`
					WHERE `".DB_forms_fields_answers."`.`answer_id`=".(int)$answer_id." AND `".DB_forms_fields_allowed."`.`allowed_for_person_type`=".$_SESSION['USER_DATA']['person_type']." AND `".DB_forms_fields_allowed."`.`action`=".PlanningFieldPermissions::Edit);*/
			$plan_data = array();
			$result = db_query("SELECT * FROM `".DB_forms_fields_answers."` WHERE `answer_id`=".$answer_id);
			while($row = mysqli_fetch_assoc($result))
				$plan_data[$row['field_id']] = $row['answer'];
		
			$res = db_query("SELECT * FROM `".DB_forms_fields_allowed."` WHERE `form_id`=".$forFormId['form_id']." AND `action`=".PlanningFieldPermissions::Edit);
			$EditAllowed = array();
			while($row = mysqli_fetch_assoc($res))
				$EditAllowed[$row['field_id']][$row['allowed_for_person_type']] = '';
			$res = db_query("SELECT * FROM `".DB_forms_fields."` WHERE `form_id`=".$forFormId['form_id']);
			$fields = array();
			$individual_planning_allowed = false;
			while($row = mysqli_fetch_assoc($res)) {
				$fields[$row['field_id']]['type'] = $row['type'];
				$fields[$row['field_id']]['isRequired'] = $row['isRequired'];
				if(ADMIN || $row['type'] == 3 && isset($EditAllowed[$row['field_id']][$_SESSION['USER_DATA']['person_type']]))//Individual planning
					$individual_planning_allowed = true;
			}
		
			if(isset($_POST['f'])) foreach($_POST['f'] as $field_id => $answer) {//Should iterate through all just in case there is no
				if( ADMIN || isset($EditAllowed[$field_id][$_SESSION['USER_DATA']['person_type']]) ) {
					if($fields[$field_id]['type'] == 4 && $KaunoZiburelis) {
						$answer = serialize($answer);//Būdavo dar isset.
					}
					//echo $fields[$field_id]['type'];
					if($fields[$field_id]['type'] >= 10) {
						unset($answer['dumb']);
						/*print_r($answer);//Too much work. Edge case what to do if one user wants to delete some row, the other should confirm?
						$res = db_query("SELECT * FROM `".DB_forms_widget_fields_allowed."` WHERE `widget_id`=".(int)$fields[$field_id]['type'].' AND `action`='.PlanningFieldPermissions::Edit);
						$EditAllowedWidget = array();
						while($row = mysqli_fetch_assoc($res)) {
							$EditAllowedWidget[$row['field_id']][$row['allowed_for_person_type']] = '';
							$EditAllowedWidget[$row['field_id']][3] = '';//3 - manager
						}
						if(!empty($answer)) {
							foreach($answer as $tbl_col_id => $row) {
								$isEditAllowed = ADMIN || isset($EditAllowedWidget[(int)$tbl_col_id][$_SESSION['USER_DATA']['person_type']]);
								if(!$isEditAllowed) {
									unset($answer[$tbl_col_id]);
								}
							}
						}*/
						//if(isset($plan_data[$field_id]) && $plan_data[$field_id] != 'null') {
							//var_dump($plan_data[$field_id]);
							/*echo '---------------';
							print_r(json_decode($plan_data[$field_id], true));
							echo '---------------';
							print_r($answer);
							$answer = array_merge_recursive(json_decode($plan_data[$field_id], true), $answer);*/
						//}
						$answer = json_encode($answer);
					}

					if(!isset($plan_data[$field_id]))
						db_query("INSERT INTO `".DB_forms_fields_answers."` SET 
							`answer_id`=".$answer_id.", `field_id`='".(int)$field_id."',
							`answer`='".db_fix(trim($answer))."', `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
					else
						db_query("UPDATE `".DB_forms_fields_answers."` SET 
							`answer`='".db_fix(trim($answer))."', `updated`=CURRENT_TIMESTAMP(), `updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."'
							WHERE `answer_id`=".$answer_id." AND `field_id`='".(int)$field_id."'");
				}
			}
		
			if(isset($_POST['individuali_veikla'])) {
				if($individual_planning_allowed) {
					//Can be more optimized
					db_query("DELETE FROM `".DB_forms_fields_answers_i."` WHERE `answer_id`=".(int)$answer_id);
					foreach($_POST['individuali_veikla'] as $kid_id => $activity) {
						db_query("INSERT INTO `".DB_forms_fields_answers_i."` SET `answer_id`='".(int)$answer_id."', `child_id`='".(int)$kid_id."',  
								`activity`='".db_fix(trim($activity))."', `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
					}
				}
			}
		
			//Reset opened
			db_query("UPDATE `".DB_forms_fields_answer."` SET `openedByUserId`=NULL, `openedByEmployeeId`=NULL, `openedDate`=NULL
				WHERE $add_filter`answer_id`=".(int)$answer_id);
		
			msgBox('OK', 'Planavimo informacija atnaujinta sėkmingai!');
		}
	}
}

if(isset($_GET['delete'])) {
	$answer_id = (int)$_GET['delete'];
	$result = db_query("SELECT `createdByEmployeeId` FROM `".DB_forms_fields_answer."` WHERE `answer_id`='".$answer_id."'");
	$row = mysqli_fetch_assoc($result);
	if($row['createdByEmployeeId'] == DARB_ID || ADMIN) {//Can be made more optimized
		db_query("UPDATE `".DB_forms_fields_answer."` SET `isDeleted`=1 WHERE `answer_id`='".$answer_id."'");
		/*if (!mysqli_query($db_link, "DELETE FROM `".DB_forms_fields_answer."` WHERE `answer_id`='".$answer_id."'")) {
			logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
		} else {
			db_query("DELETE FROM `".DB_forms_fields_answers."` WHERE `answer_id`=".(int)$answer_id);
			db_query("DELETE FROM `".DB_forms_fields_answers_i."` WHERE `answer_id`=".(int)$answer_id);
			msgBox('OK', 'Sėkmingai ištrinta!');
		}*/
	} else {
		//log it.
	}
}
if(isset($_GET['force_delete'])) {//No button for it
	$answer_id = (int)$_GET['force_delete'];
	$result = db_query("SELECT `createdByEmployeeId` FROM `".DB_forms_fields_answer."` WHERE `answer_id`='".$answer_id."'");
	$row = mysqli_fetch_assoc($result);
	if($row['createdByEmployeeId'] == DARB_ID || ADMIN) {//Can be made more optimized
		if (!mysqli_query($db_link, "DELETE FROM `".DB_forms_fields_answer."` WHERE `answer_id`='".$answer_id."'")) {
			logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
		} else {
			db_query("DELETE FROM `".DB_forms_fields_answers."` WHERE `answer_id`=".(int)$answer_id);
			db_query("DELETE FROM `".DB_forms_fields_answers_i."` WHERE `answer_id`=".(int)$answer_id);
			msgBox('OK', 'Sėkmingai ištrinta!');
		}
	} else {
		//log it.
	}
}
if(isset($_GET['undelete'])) {
	$answer_id = (int)$_GET['undelete'];
	$result = db_query("SELECT `createdByEmployeeId` FROM `".DB_forms_fields_answer."` WHERE `answer_id`='".$answer_id."'");
	$row = mysqli_fetch_assoc($result);
	if($row['createdByEmployeeId'] == DARB_ID || ADMIN) {//Can be more optimized
		db_query("UPDATE `".DB_forms_fields_answer."` SET `isDeleted`=0 WHERE `answer_id`='".$answer_id."'");
	} else {
		//log it.
	}
}
if(isset($_GET['archiveIt'])) {
	$answer_id = (int)$_GET['archiveIt'];
	$result = db_query("SELECT `createdByEmployeeId` FROM `".DB_forms_fields_answer."` WHERE `answer_id`='".$answer_id."'");
	$row = mysqli_fetch_assoc($result);
	if($row['createdByEmployeeId'] == DARB_ID || ADMIN) {//Can be more optimized
		db_query("UPDATE `".DB_forms_fields_answer."` SET `isArchived`=1 WHERE `answer_id`='".$answer_id."'");
	} else {
		//log it.
	}
}
if(isset($_GET['unArchiveIt'])) {
	$answer_id = (int)$_GET['unArchiveIt'];
	$result = db_query("SELECT `createdByEmployeeId` FROM `".DB_forms_fields_answer."` WHERE `answer_id`='".$answer_id."'");
	$row = mysqli_fetch_assoc($result);
	if($row['createdByEmployeeId'] == DARB_ID || ADMIN) {//Can be more optimized
		db_query("UPDATE `".DB_forms_fields_answer."` SET `isArchived`=0 WHERE `answer_id`='".$answer_id."'");
	} else {
		//log it.
	}
}

//URL to remember
$URL = '?';
$URL_ = array();
if( isset($_GET['group_id']) ) {
	$URL .= 'group_id='.(int)$_GET['group_id'].'&amp;';
	$URL_['group_id'] = (int)$_GET['group_id'];
} else {
	$_GET['group_id'] = 0;
}
if( isset($_GET['archive']) ) {
	$URL .= 'archive='.(int)$_GET['archive'].'&amp;';
	$URL_['archive'] = (int)$_GET['archive'];
} else {
	$_GET['archive'] = 0;
}
if( isset($_GET['trash']) ) {
	$URL .= 'trash&amp;';
	$URL_['trash'] = '';
}
if( isset($_GET['filter_form_id']) ) {
	$URL .= 'filter_form_id='.(int)$_GET['filter_form_id'].'&amp;';
	$URL_['filter_form_id'] = (int)$_GET['filter_form_id'];
} else {
	$_GET['filter_form_id'] = 0;
}
if( isset($_GET['filter_date']) ) {
	$URL .= 'filter_date='.urlencode($_GET['filter_date']).'&amp;';
	$URL_['filter_date'] = urlencode($_GET['filter_date']);
} else {
	$_GET['filter_date'] = 0;
}
function URL($key = '', $val = '', $del = '') {
	global $URL_;
	$URL_tmp = $URL_;
	if(!empty($key))
		$URL_tmp[$key] = $val;
	if($del == 'del')
		unset($URL_tmp[$key]);
	$url = '?';
	foreach($URL_tmp as $key => $val)
		$url .= $key.'='.$val.'&amp;';
	return $url;
}

if(!ADMIN) {
	?><a href="?new_fill#form-fill" class="no-print fast-action fast-action-add">Įvesti naują planavimą</a> <?php
}

if(ADMIN)
	$resultTbl = db_query("SELECT `".DB_forms_fields_answer."`.*, `".DB_forms_fields_answers."`.`answer`, UNIX_TIMESTAMP(`openedDate`) AS `openedDateTimestamp`
	FROM `".DB_forms_fields_answer."`
	LEFT JOIN `".DB_forms."` ON `".DB_forms."`.`form_id`=`".DB_forms_fields_answer."`.`form_id`
	LEFT JOIN `".DB_forms_fields_answers."` ON (`".DB_forms_fields_answers."`.`field_id`=`primary_field_id` AND `".DB_forms_fields_answers."`.`answer_id`=`".DB_forms_fields_answer."`.`answer_id`)
	WHERE `".DB_forms_fields_answer."`.`isDeleted`=".(isset($_GET['trash']) ? 1 : 0)."
	AND `".DB_forms_fields_answer."`.`isArchived`=".(int)$_GET['archive']."
	".(isset($_GET['group_id']) && $_GET['group_id'] > 0 ? ' AND `'.DB_forms_fields_answer.'`.`group_id`='.(int)$_GET['group_id'] : '')."
	".(isset($_GET['filter_form_id']) && $_GET['filter_form_id'] > 0 ? ' AND `'.DB_forms_fields_answer.'`.`form_id`='.(int)$_GET['filter_form_id'] : '')."
	".(isset($_GET['filter_date']) && $_GET['filter_date'] != 0 ? " AND `".DB_forms_fields_answer."`.`fromDate`<='".date("Y-m-t", strtotime($_GET['filter_date']))."' AND `".DB_forms_fields_answer."`.`toDate`>='".date("Y-m-1", strtotime($_GET['filter_date']))."'" : '')."
	GROUP BY `".DB_forms_fields_answer."`.`answer_id`
	ORDER BY `".DB_forms_fields_answer."`.`toDate` DESC");
else
	//$resultTbl = db_query("SELECT *, UNIX_TIMESTAMP(`openedDate`) AS `openedDateTimestamp` 
	//FROM `".DB_forms_fields_answer."` WHERE `group_id`=".GROUP_ID." ORDER BY `toDate` DESC");
	$resultTbl = db_query("SELECT `".DB_forms_fields_answer."`.*, `".DB_forms_fields_answers."`.`answer`, UNIX_TIMESTAMP(`".DB_forms_fields_answer."`.`openedDate`) AS `openedDateTimestamp` 
		FROM `".DB_forms_fields_answer."` JOIN `".DB_forms_fields_allowed."` ON (`".DB_forms_fields_answer."`.`form_id`=`".DB_forms_fields_allowed."`.`form_id` AND `allowed_for_person_type`=".$_SESSION['USER_DATA']['person_type'].")
		LEFT JOIN `".DB_forms."` ON `".DB_forms."`.`form_id`=`".DB_forms_fields_answer."`.`form_id`
		LEFT JOIN `".DB_forms_fields_answers."` ON (`".DB_forms_fields_answers."`.`field_id`=`primary_field_id` AND `".DB_forms_fields_answers."`.`answer_id`=`".DB_forms_fields_answer."`.`answer_id`)
		WHERE `".DB_forms_fields_answer."`.`group_id`=".GROUP_ID." AND `".DB_forms_fields_answer."`.`isDeleted`=".(isset($_GET['trash']) ? 1 : 0)."
		".(isset($_GET['filter_form_id']) && $_GET['filter_form_id'] > 0 ? ' AND `'.DB_forms_fields_answer.'`.`form_id`='.(int)$_GET['filter_form_id'] : '')."
		".(isset($_GET['filter_date']) && $_GET['filter_date'] != 0 ? " AND `".DB_forms_fields_answer."`.`fromDate`<='".date("Y-m-t", strtotime($_GET['filter_date']))."' AND `".DB_forms_fields_answer."`.`toDate`>='".date("Y-m-1", strtotime($_GET['filter_date']))."'" : '')."
		AND `".DB_forms_fields_answer."`.`isArchived`=".(int)$_GET['archive']."
		GROUP BY `".DB_forms_fields_answer."`.`answer_id`
		ORDER BY `".DB_forms_fields_answer."`.`toDate` DESC");


if(ADMIN)
	$resultDates = db_query("SELECT DATE_FORMAT(`".DB_forms_fields_answer."`.`fromDate`, '%Y-%m') `fromDate`, DATE_FORMAT(`".DB_forms_fields_answer."`.`toDate`, '%Y-%m') `toDate`
	FROM `".DB_forms_fields_answer."`
	WHERE `".DB_forms_fields_answer."`.`isDeleted`=".(isset($_GET['trash']) ? 1 : 0)."
	AND `".DB_forms_fields_answer."`.`isArchived`=".(int)$_GET['archive']."
	".(isset($_GET['group_id']) && $_GET['group_id'] > 0 ? ' AND `'.DB_forms_fields_answer.'`.`group_id`='.(int)$_GET['group_id'] : '')."
	".(isset($_GET['filter_form_id']) && $_GET['filter_form_id'] > 0 ? ' AND `'.DB_forms_fields_answer.'`.`form_id`='.(int)$_GET['filter_form_id'] : '')."
	GROUP BY `".DB_forms_fields_answer."`.`fromDate`");
else
	$resultDates = db_query("SELECT DATE_FORMAT(`".DB_forms_fields_answer."`.`fromDate`, '%Y-%m') `fromDate`, DATE_FORMAT(`".DB_forms_fields_answer."`.`toDate`, '%Y-%m') `toDate`
		FROM `".DB_forms_fields_answer."` JOIN `".DB_forms_fields_allowed."` ON (`".DB_forms_fields_answer."`.`form_id`=`".DB_forms_fields_allowed."`.`form_id` AND `allowed_for_person_type`=".$_SESSION['USER_DATA']['person_type'].")
		WHERE `".DB_forms_fields_answer."`.`group_id`=".GROUP_ID." AND `".DB_forms_fields_answer."`.`isDeleted`=".(isset($_GET['trash']) ? 1 : 0)."
		".(isset($_GET['filter_form_id']) && $_GET['filter_form_id'] > 0 ? ' AND `'.DB_forms_fields_answer.'`.`form_id`='.(int)$_GET['filter_form_id'] : '')."
		AND `".DB_forms_fields_answer."`.`isArchived`=".(int)$_GET['archive']."
		GROUP BY `".DB_forms_fields_answer."`.`fromDate`");
	
if( mysqli_num_rows($resultTbl) > 0 || isset($_GET['archive']) || isset($_GET['trash']) ) {
	echo (isset($_GET['trash']) ? '<a href="'.URL('trash', 0, 'del').'" class="no-print fast-action fast-action-delete">✖ Uždaryti šiukšlinę</a>' : '<a href="'.$URL.'trash" class="no-print fast-action fast-action-delete">Šiukšlinė</a>');//✖  Išmesti, perkelti į šiukšlinę, nerodyti, slėpti
	echo (isset($_GET['archive']) && $_GET['archive'] == 1 ? ' <a href="'.URL('archive', 0).'" class="no-print fast-action fast-action-archive">✖ Uždaryti archyvą</a>' : ' <a href="'.URL('archive', 1).'" class="no-print fast-action fast-action-archive">Archyvas</a>');
	echo ui_print();
}

	if(ADMIN) {
		if(isset($URL_['group_id'])) {
			$unseted_group_id = $URL_['group_id'];
			unset($URL_['group_id']);
		}
		?>
		<form method="get" class="no-print" action="<?=URL()?>">
			<input type="hidden" name="autofocus">
			<div class="sel" style="float: left"><select name="group_id" onchange="this.form.submit()" <?=(isset($_GET['autofocus']) ? 'autofocus' : '')?>>
			<?php
			echo "<option value=\"0\"".(isset($_GET['group_id']) && 0 == $_GET['group_id'] ? ' selected="selected" class="selected">'.$selectedMark : '>')."Visos grupės</option>";
			foreach($grupes as $ID => $title)
				if($ID != 0)
					echo "<option value=\"".$ID."\"".(isset($_GET['group_id']) && $ID == $_GET['group_id'] ? ' selected="selected" class="selected">'.$selectedMark : '>').$title."</option>";
			?>
			</select></div>
			<?php
			foreach($URL_ as $ID => $val)
				echo '<input type="hidden" name="'.$ID.'" value="'.$val.'">';
			?>
		</form>
		<?php
		if(isset($unseted_group_id)) {
			$URL_['group_id'] = $unseted_group_id;
		}
	}
	if(isset($URL_['filter_form_id'])) {
		$unseted_filter_form_id = $URL_['filter_form_id'];
		unset($URL_['filter_form_id']);
	}
	?>
	<form method="get" class="no-print" action="<?=URL()?>">
		<div class="sel" style="float: left"><select name="filter_form_id" onchange="this.form.submit()" <?=(isset($_GET['autofocus']) ? 'autofocus' : '')?>>
		<?php
		echo "<option value=\"0\"".(isset($_GET['filter_form_id']) && 0 == $_GET['filter_form_id'] ? ' selected="selected" class="selected">'.$selectedMark : '>')."Visos formos</option>";
		foreach($form_title as $ID => $title)
			echo "<option value=\"".$ID."\"".(isset($_GET['filter_form_id']) && $ID == $_GET['filter_form_id'] ? ' selected="selected" class="selected">'.$selectedMark : '>').$title."</option>";
		?>
		</select></div>
		<?php
		foreach($URL_ as $ID => $val)
			echo '<input type="hidden" name="'.$ID.'" value="'.$val.'">';
		?>
	</form>
	<?php
	if(isset($unseted_filter_form_id)) {
		$URL_['filter_form_id'] = $unseted_filter_form_id;
	}
	
	$dates = [];
	while ($row = mysqli_fetch_assoc($resultDates)) {
		$dates[$row['fromDate']] = '';
		$dates[$row['toDate']] = '';
	}
	krsort($dates);
	?>
	<form method="get" class="no-print" action="<?=URL()?>">
		<div class="sel" style="float: left"><select name="filter_date" onchange="this.form.submit()" <?=(isset($_GET['autofocus']) ? 'autofocus' : '')?>>
		<?php
		echo "<option value=\"0\"".(isset($_GET['filter_date']) && 0 == $_GET['filter_date'] ? ' selected="selected" class="selected">'.$selectedMark : '>')."Visos datos</option>";
		foreach($dates as $ID => $dumb)
			echo "<option value=\"".$ID."\"".(isset($_GET['filter_date']) && $ID == $_GET['filter_date'] ? ' selected="selected" class="selected">'.$selectedMark : '>').$ID."</option>";
		?>
		</select></div>
		<?php
		unset($URL_['filter_date']);
		foreach($URL_ as $ID => $val)
			echo '<input type="hidden" name="'.$ID.'" value="'.$val.'">';
		?>
	</form>
	<div style="clear: left;"></div>
	<?php



if(mysqli_num_rows($resultTbl) > 0) {
	if(!ADMIN) {
		$res = db_query("SELECT * FROM `".DB_forms_fields_allowed."` WHERE `allowed_for_person_type`=".$_SESSION['USER_DATA']['person_type']." AND `action`=".PlanningFieldPermissions::Edit.' GROUP BY `form_id`');
		$EditLinkAllowed = array();
		while($row = mysqli_fetch_assoc($res))
			$EditLinkAllowed[$row['form_id']] = '';
	}
	?>
	<table class="vertical-hover<?php if(isset($_GET['view'])) echo ' no-print'; ?>">
	<tr>
		<th>Nuo–iki (Rikiuota pagal „iki“)</th>
		<?php
		if(!isset($_GET['group_id']) || $_GET['group_id'] == 0)
			echo "<th>Grupė</th>";
		if(!isset($_GET['filter_form_id']) || $_GET['filter_form_id'] == 0)
			echo "<th>Formos pavadinimas</th>";
		?>
		<th>Papildoma info</th>
		<th>Sukūrė<br>Atnaujino</th>
		<th class="no-print">Veiksmai</th>
	</tr>
	<?php
	$opened_answer_id = 0;//opened-row
	if(isset($_GET['view']))
		$opened_answer_id = (int)$_GET['view'];
	if(isset($_GET['edit']))
		$opened_answer_id = (int)$_GET['edit'];
	if(isset($_POST['edit']))
		$opened_answer_id = (int)$_POST['edit'];
	while ($row = mysqli_fetch_assoc($resultTbl)) {
		echo "		<tr".($opened_answer_id == $row['answer_id'] ? ' class="opened-row"' : '').">
		<td>".str_replace('-', '.', $row['fromDate'])."<span style=\"display: inline-block; float: right;\">–".str_replace('-', '.', $row['toDate'])."</span></td>";
		if(!isset($_GET['group_id']) || $_GET['group_id'] == 0)
			echo "<td>".(isset($grupes[$row['group_id']]) ? $grupes[$row['group_id']] : 'Vadovų ištrinta grupė')."</td>";
		if(!isset($_GET['filter_form_id']) || $_GET['filter_form_id'] == 0)
			echo "<td>".$form_title[$row['form_id']]."</td>";
		echo "<td>".filterText($row['answer'])."</td>
		<td><div>".substr($row['created'], 0, -3)." ".getAllEmployees($row['createdByEmployeeId'])."</div>".substr(date_empty($row['updated']), 0, -3)." ".getAllEmployees($row['updatedByEmployeeId'])."</td>
		<td class=\"no-print\"><a href=\"".$URL."view=".$row['answer_id']."#planning-view\">Peržiūrėti</a> ";
		if(ADMIN || isset($EditLinkAllowed[$row['form_id']])) {
			if($row['openedDateTimestamp'] < time()-$max_opened_msg_delay)
				echo " <a href=\"".$URL."edit=".$row['answer_id']."#form-fill\">Keisti</a> ";
			else
				echo ' [Dabar redaguoja '.getAllEmployees($row['openedByEmployeeId'])." ".$row['openedDate']."]";//BUG gaunasi kai redaguoja abu, jeigu atidaro keli žmonės tai kadangi viena atidarymo data tai konkuruoja tada ta data tai vieno tai kito: <a href=\"".$URL."edit=".$row['answer_id']."#form-fill\" onclick=\"return confirm('Ar tikrai norite keisti? Tai dažniausiai pavojinga, nes Jūs nematysite tiesiogiai ką keičia kitas žmogus. O išsaugant duomenis tas kas išsaugos duomenis paskutinis to duomenys užsirašys ant viršaus, o tarpinio išsaugojimo dalis duomenų bus panaikinti.')\">Keisti</a>
		}
		if($row['createdByEmployeeId'] == DARB_ID || ADMIN) {
			echo " ".(isset($_GET['trash']) ? "<a href=\"".$URL."undelete=".$row['answer_id']."\">Atstatyti</a>" : "<a href=\"".$URL."delete=".$row['answer_id']."\" onclick=\"return confirm('Ar tikrai norite perkelti į šiukšlinę planavimo dokumentą?')\">Trinti</a>")." ";
			echo ($row['isArchived'] ? "<a href=\"".$URL."unArchiveIt=".$row['answer_id']."\">Išarchyvuoti</a>" : "<a href=\"".$URL."archiveIt=".$row['answer_id']."\" onclick=\"return confirm('Ar tikrai norite archyvuoti planavimo dokumentą?')\">Archyvuoti</a>");
		}
		echo "
		</td>
	</tr>";	
	}
	?>
	</table>
	<?php
}

if(isset($_GET['new_fill']) && !ADMIN/*optional*/) { ?>
	<fieldset id="form-fill">
	<legend>Įvesti naują planavimą</legend>
	<form method="get" style="margin-bottom: 100px;">
		<?php
		$result = db_query("SELECT * FROM `".DB_forms."` WHERE `isPublished`=1 AND `isDeleted`=0");
		if(mysqli_num_rows($result) == 0) {
			echo "Vadovai dar nepaskelbė planavimo formų.";
		} else {
		//TODO: Show only those forms that have viewable or editable fields
		//Show in list clickable urls to avoid clicks...
		?>
		<p><div class="sel" style="float: left"><select name="form_id" required="required">
			<?php
			while ($row = mysqli_fetch_assoc($result))
				echo "<option value=\"".$row['form_id']."\">".$row['title']."</option>";
			?>
		</select></div> <input type="submit" value="Išsirinkau formą planavimo pildymui" class="filter" style="margin-left: 7px;"></p>
		<?php } ?>
	</form>
	</fieldset>
<?php }

if(isset($_GET['form_id']) && !ADMIN/*optional*/ || isset($_GET['edit'])) {
	$msg_in_form = '';
	if(!ADMIN) {//for individual planning
		$group_id = GROUP_ID;
		$kidsFrom = null;
	}
	if(isset($_GET['edit'])) {
		//Security: like hasGroup() but via $add_filter
		$result = db_query("SELECT *, UNIX_TIMESTAMP(`openedDate`) AS `openedDateTimestamp`, DATE(`created`) AS `kidsFrom`
			FROM `".DB_forms_fields_answer."` WHERE $add_filter`answer_id`='".(int)$_GET['edit']."' ORDER BY `toDate` DESC");
		if(mysqli_num_rows($result)) {
			$answer = mysqli_fetch_assoc($result);
			$res = db_query("SELECT * FROM `".DB_forms_fields_allowed."` WHERE `allowed_for_person_type`=".$_SESSION['USER_DATA']['person_type']." AND `action`=".PlanningFieldPermissions::Edit.' AND `form_id`='.$answer['form_id']);
			if(ADMIN || mysqli_num_rows($res))
				$allow_to_show_edit = true;
			
			$allowedToChangePeriod = ($answer['createdByEmployeeId'] == DARB_ID || ADMIN);
			$group_id = $answer['group_id'];//GROUP_ID
			$kidsFrom = $answer['kidsFrom'];

			
			if($answer['openedDateTimestamp'] > time()-$max_opened_msg_delay && ($answer['openedByEmployeeId'] != DARB_ID || $answer['openedByUserId'] != USER_ID)) {
				$msg_in_form = '<div class="error" id="form-fill">Atidarėte kito žmogaus redaguojamą dokumentą. Dabar redaguoja '.getAllEmployees((int)$answer['openedByEmployeeId'])." ".$answer['openedDate'].'.</div>';
			} else {
				//Opened
				db_query("UPDATE `".DB_forms_fields_answer."` 
					SET `openedByUserId`=".USER_ID.", `openedByEmployeeId`=".DARB_ID.", `openedDate`=CURRENT_TIMESTAMP()
					WHERE $add_filter`answer_id`=".(int)$answer['answer_id']);
			}
		}
	} else {
		$allowedToChangePeriod = true;
	}
	
	if(!empty($msg_in_form)) {
		echo $msg_in_form;
	} elseif(isset($_GET['form_id']) || isset($allow_to_show_edit)) {
		$form_id = isset($_GET['form_id']) ? (int)$_GET['form_id'] : $answer['form_id'];
		$r = db_query("SELECT * FROM `".DB_forms."` WHERE `form_id`=".$form_id);
	
		if(mysqli_num_rows($r)) {
			$form = mysqli_fetch_assoc($r);
			if(isset($_GET['form_id']))//new
				$result = db_query("SELECT * FROM `".DB_forms_fields."` WHERE `form_id`=".$form_id." ORDER BY `order` ASC");
			else//edit
				$result = db_query("SELECT *, `".DB_forms_fields."`.`field_id` 
				FROM `".DB_forms_fields."` LEFT JOIN `".DB_forms_fields_answers."` ON `".DB_forms_fields."`.`field_id`=`".DB_forms_fields_answers."`.`field_id` AND `".DB_forms_fields_answers."`.`answer_id`='".(int)$answer['answer_id']."'
				WHERE `form_id`=".$answer['form_id']." ORDER BY `order` ASC");// must be included `".DB_forms_fields."`.`field_id` in SELECT because otherwise it can be empty

			$res = db_query("SELECT * FROM `".DB_forms_fields_allowed."` WHERE `form_id`=".$form_id.' AND `action`='.PlanningFieldPermissions::Edit);
			$EditAllowed = array();
			while($row = mysqli_fetch_assoc($res)) {
				$EditAllowed[$row['field_id']][$row['allowed_for_person_type']] = '';
				$EditAllowed[$row['field_id']][3] = '';//3 - manager
			}
			$res = db_query("SELECT * FROM `".DB_forms_fields_allowed."` WHERE `form_id`=".$form_id);//.' AND `action`=1'
			$ViewAllowed = array();
			while($row = mysqli_fetch_assoc($res)) {
				$ViewAllowed[$row['field_id']][$row['allowed_for_person_type']] = '';
				//$ViewAllowed[$row['field_id']][3] = '';//3 - manager
			}
			?>
			<fieldset id="form-fill">
			<legend><?=(isset($_GET['form_id']) ? 'Pildyti ' : 'Redaguoti')?> pagal formą „<strong><?=$form['title']?></strong>“</legend>
			<form action="<?=$URL?>" method="post" class="not-saved-reminder" id="persist-data">
				<?php //echo $msg_in_form; ?>
				<input type="hidden" name="<?=(isset($_GET['form_id']) ? 'save' : 'edit')?>" value="<?=(isset($_GET['form_id']) ? $form_id : $answer['answer_id'])?>"<?=(isset($_GET['edit']) ? ' id="answer_id"' : '')?>>
				<p><label><span class="title">Planavimo laikotarpis nuo<span class="required">*</span>:</span><br><input class="datepicker" type="text" name="fromDate" value="<?=(isset($_GET['edit']) ? date_empty($answer['fromDate']) : '')?>" placeholder="<?=date('Y-m-d')?>" required="required"<?=($allowedToChangePeriod ? '' : ' disabled="disabled"')?>></label></p>
				<p><label><span class="title">Planavimo laikotarpis iki<span class="required">*</span>:</span><br><input class="datepicker" type="text" name="toDate" value="<?=(isset($_GET['edit']) ? date_empty($answer['toDate']) : '')?>" placeholder="<?=date('Y-m-d')?>" required="required"<?=($allowedToChangePeriod ? '' : ' disabled="disabled"')?>></label></p>
			<?php
			while($field = mysqli_fetch_assoc($result)) {
				//DRY FROM forms.php
				$can_edit = ' (Gali peržiūrėti ir pildyti:<span style="color: #000">';
				$res = db_query("SELECT * FROM `".DB_forms_fields_allowed."` WHERE `field_id`=".(int)$field['field_id'].' AND `action`=0');
				while($allow = mysqli_fetch_assoc($res))
					$can_edit .= ' '.$person_type_who_dgs[$allow['allowed_for_person_type']].';';
				$can_edit .= '</span>)';
				$can_only_view = ' (Tik peržiūri:<span style="color: #000">';
				$res = db_query("SELECT * FROM `".DB_forms_fields_allowed."` WHERE `field_id`=".(int)$field['field_id'].' AND `action`=1');//
				while($allow = mysqli_fetch_assoc($res))
					$can_only_view .= ' '.$person_type_who_dgs[$allow['allowed_for_person_type']].';';
				$can_only_view .= '</span>)';
				$can = '<span class="notice">'.$can_edit.' '.$can_only_view.'</span>';
				
				if(isset($ViewAllowed[(int)$field['field_id']][$_SESSION['USER_DATA']['person_type']]) || ADMIN) {
					$isEditAllowed = ADMIN || isset($EditAllowed[(int)$field['field_id']][$_SESSION['USER_DATA']['person_type']]);
					$disabled = ($isEditAllowed ? '' : ' disabled="disabled"');
					if($field['type'] == 0) {//One line
						echo '<p><label><span class="title">'.filterText($field['title']).(!empty($field['help_text']) ? ' <span class="details">('.filterText($field['help_text']).')</span>' : '' ).($field['isRequired'] ? '<span class="required">*</span>' : '').$can.':</span><br><input type="text" name="f['.(int)$field['field_id'].']" value="'.(isset($_GET['edit']) ? filterText($field['answer']) : '').'" style="width: 500px"'.($field['isRequired'] ? ' required="required"' : '').$disabled.'></label></p>';
					} elseif($field['type'] == 1) {//Paragraph text
						echo '<p><label><span class="title">'.filterText($field['title']).(!empty($field['help_text']) ? ' <span class="details">('.filterText($field['help_text']).')</span>' : '' ).($field['isRequired'] ? '<span class="required">*</span>' : '').$can.':</span><br><textarea name="f['.(int)$field['field_id'].']"'.($field['isRequired'] ? ' required="required"' : '').$disabled.'>'.(isset($_GET['edit']) ? filterText($field['answer']) : '').'</textarea></label></p>';
					} elseif($field['type'] == 2) {//Data
						echo '<p><label><span class="title">'.filterText($field['title']).(!empty($field['help_text']) ? ' <span class="details">('.filterText($field['help_text']).')</span>' : '' ).($field['isRequired'] ? '<span class="required">*</span>' : '').$can.':</span><br><input class="datepicker" type="text" name="f['.(int)$field['field_id'].']" value="'.(isset($_GET['edit']) ? date_empty($field['answer']) : '').'" placeholder="'.date('Y-m-d').'"'.($field['isRequired'] ? ' required="required"' : '').$disabled.'></label></p>';
					} elseif($field['type'] == 3) {//Individual planning
						echo '<p><span class="title">'.filterText($field['title']).(!empty($field['help_text']) ? ' <span class="details">('.filterText($field['help_text']).')</span>' : '' ).$can.'<!-- Individuali veikla <span class="details">(Ugdymo individualizavimas pagal vaiko poreikius, gebėjimus ir kitus ypatumus)</span> -->:</span><br>';
						if($isEditAllowed) {
							echo '<div class="sel"><select id="kidId"><option value="" selected hidden disabled>Pasirinkite vaiką</option>';
							/*if(ADMIN)
								$res = mysqli_query($db_link, "SELECT * FROM `".DB_children."` WHERE `isDeleted`=0 AND `archyvas`=0 AND `parent_kid_id`=0 AND `grupes_id`=".$answer['group_id']." ORDER BY vardas, pavarde");//Only in edit.
							else
								$res = mysqli_query($db_link, "SELECT `".DB_children."`.* FROM `".DB_children."` 
								WHERE `grupes_id`=".GROUP_ID." AND `isDeleted`=0 AND `archyvas`=0 AND `parent_kid_id`=0
								ORDER BY vardas, pavarde");*/
							$res = db_query(getKidsSql($group_id, $kidsFrom));//$get_kids_sql //Grupes ID, pradzia
								/*$res = mysqli_query($db_link, "SELECT `".DB_children."`.*
									FROM `".DB_children."` 
									JOIN `".DB_employees_groups."` ON `".DB_children."`.`grupes_id`=`".DB_employees_groups."`.`grup_id`
									WHERE `".DB_employees_groups."`.`darb_id`=".(int)DARB_ID." AND `".DB_children."`.`isDeleted`=0 AND `".DB_children."`.`archyvas`=0 AND `".DB_children."`.`parent_kid_id`=0
									ORDER BY vardas, pavarde");//without filtering by selected kids group*/
							while ($row = mysqli_fetch_assoc($res))
								echo "<option value=\"".$row['parent_kid_id']."\">".filterText(getName($row['vardas'], $row['pavarde']))."</option>";
				
							echo '</select></div>
								<a href="#" id="addScnt">+ Pridėti įvedimo laukelį<!-- šiam vaikui individualios veiklos informaciją --></a> <span class="abbr" title="Vaikai kurie buvo planavimo sukūrimo datą ir būtent tos grupės kuriai planavimas buvo pradėtas rašyti">Kodėl toks vaikų sąrašas?</span>';
						}
						echo '
						<div id="p_scents">';

						if(isset($_GET['edit'])) {
							$res = db_query("SELECT `".DB_children."`.*, `".DB_forms_fields_answers_i."`.`activity` 
							FROM `".DB_forms_fields_answers_i."` JOIN `".DB_children."` ON `".DB_children."`.`ID`=`".DB_forms_fields_answers_i."`.`child_id`
							WHERE `".DB_forms_fields_answers_i."`.`answer_id`=".(int)$_GET['edit']." ORDER BY ".orderName());//vardas, pavarde
							while ($row = mysqli_fetch_assoc($res)) {
								echo "<p><label>".filterText(getName($row['vardas'], $row['pavarde']))." <input type=\"text\" class=\"individual_plan\" name=\"individuali_veikla[${row['ID']}]\" value=\"".filterText($row['activity'])."\"".$disabled."></label> ".($isEditAllowed ? "<a href=\"#\" class=\"remScnt\" data-kid-id=\"${row['ID']}\" id=\"individual${row['ID']}\">- Trinti šią informaciją</a>" : '')."</p>";
							}
						}
						echo '</div>
						<script type="text/javascript" src="/workers/planning.js?3"></script>';
		
						/*$result = mysqli_query($db_link, "SELECT `".DB_children."`.*, `".DB_forms_fields_answers_i."`.`activity` 
						FROM `".DB_forms_fields_answers_i."` JOIN `".DB_children."` ON `".DB_children."`.`ID`=`".DB_forms_fields_answers_i."`.`child_id`
						WHERE `".DB_forms_fields_answers_i."`.`plan_id`=".(int)$plan_view['planning_id']." ORDER BY vardas, pavarde") or logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
						while ($row = mysqli_fetch_assoc($result))
							echo "<p><strong>".filterText($row['vardas'])." ".filterText($row['pavarde'])."</strong>: ".filterText($row['activity'])."</p>";
						*/
						echo '</p>';
					} elseif($field['type'] == 4 && $KaunoZiburelis) {
						/*
						echo '<p><label><span class="title">'.filterText($field['title']).(!empty($field['help_text']) ? ' <span class="details">('.filterText($field['help_text']).')</span>' : '' ).($field['isRequired'] ? '<span class="required">*</span>' : '').':</span><br><input class="datepicker" type="text" name="f['.(int)$field['field_id'].']" value="'.(isset($_GET['edit']) ? date_empty($field['answer']) : '').'" placeholder="'.date('Y-m-d').'"'.($field['isRequired'] ? ' required="required"' : '').$disabled.'></label></p>';
						*/
						?><table>
						<thead>
							<tr>
								<th>Eil.<br>Nr.</th>
								<th><!--Veiklos pavadinimas -->Ugdomoji veikla</th>
								<!-- <th>Vaiko pasiekimai, kurie vestų į galutinį rezultatą</th> -->
								<th>Priemonės</th>
								<th>Galutinis vaiko pasiekimų rezultatas pasiekus uždavinį</th>
								<th>Valdymas</th>
							</tr>
						</thead>
						<tbody id="tbody">
						<?php
						if(isset($_GET['edit'])) {
							$field['answer'] = unserialize($field['answer']);
							//print_r($plan['veiklaVisaiVaikuGrupei']);
							$i=0;
							if(!empty($field['answer']))
								foreach($field['answer'] as $veiklaVisaiVaikuGrupei) {
									echo '<tr><td>'.($i+1).'</td>
									<td><textarea name="f['.(int)$field['field_id'].'][VeiklosPavadinimas]">'.filterText($veiklaVisaiVaikuGrupei['VeiklosPavadinimas']).'</textarea></td>
				
									<td><textarea name="f['.(int)$field['field_id'].'][Priemones]">'.filterText($veiklaVisaiVaikuGrupei['Priemones']).'</textarea></td>
									<td><textarea name="f['.(int)$field['field_id'].'][GalutinisRezultatas]">'.filterText($veiklaVisaiVaikuGrupei['GalutinisRezultatas']).'</textarea></td>
									<td><a href="#" class="remRow">Trinti šią eilutę</a></td></tr>';
									++$i;
								}//<td><textarea name="veiklaVisaiVaikuGrupei['.$i.'][VaikoPasiekimai]">'.filterText($veiklaVisaiVaikuGrupei['VaikoPasiekimai']).'</textarea></td>
						}
						?>
						</tbody>
						</table>
						<a href="#" id="addRow">Pridėti įvedimo eilutę</a>
						<?php
					} elseif($field['type'] >= 10) {//---------------------------------------------
						echo '<p><span class="title">'.filterText($field['title']).(!empty($field['help_text']) ? ' <span class="details">('.filterText($field['help_text']).')</span>' : '' ).$can.':</span></p>';//echo $can;
						/*$res = db_query("SELECT * FROM `".DB_forms_widget_fields_allowed."` WHERE `widget_id`=".(int)$field['type'].' AND `action`='.PlanningFieldPermissions::Edit);
						$EditAllowedWidget = array();
						while($row = mysqli_fetch_assoc($res)) {
							$EditAllowedWidget[$row['field_id']][$row['allowed_for_person_type']] = '';
							$EditAllowedWidget[$row['field_id']][3] = '';//3 - manager
						}*/
						$res = db_query("SELECT * FROM `".DB_forms_widget_fields_allowed."` WHERE `kindergarten_id`=".DB_ID." AND `widget_id`=".(int)$field['type']);//.' AND `action`=1'
						$ViewAllowedWidget = array();
						while($row = mysqli_fetch_assoc($res)) {
							$ViewAllowedWidget[$row['field_id']][$row['allowed_for_person_type']] = '';
							//$ViewAllowedWidget[$row['field_id']][3] = '';//3 - manager
						}
						//print_r($ViewAllowedWidget);
						//Show from widget
						$result_w = db_query("SELECT * FROM `".DB_forms_widget_fields."` WHERE `kindergarten_id`=".DB_ID." AND `widget_id`=".(int)$field['type']." ORDER BY `order` ASC");
						echo '<table class="plan">
						<tr>';
						//DB_forms_widget_fields_allowed
						//$widget_field_types//0 number //1 text
						$field_subfields = array();
						while($row = mysqli_fetch_assoc($result_w)) {
							//if(isset($ViewAllowedWidget[(int)$row['field_id']][$_SESSION['USER_DATA']['person_type']]) || $isEditAllowed || ADMIN) {
								/*$can_edit = ' (Gali peržiūrėti ir pildyti:';
								$res = db_query("SELECT * FROM `".DB_forms_widget_fields_allowed."` WHERE `field_id`=".(int)$row['field_id'].' AND `action`=0');
								while($allow = mysqli_fetch_assoc($res))
									$can_edit .= ' '.$person_type_who_dgs[$allow['allowed_for_person_type']].';';
								$can_edit .= ')';*/
								$can_only_view = '';//$can_only_view = ' (Gali tik peržiūrėti:';
								$res = db_query("SELECT * FROM `".DB_forms_widget_fields_allowed."` WHERE `field_id`=".(int)$row['field_id'].' AND `action`=1');//
								while($allow = mysqli_fetch_assoc($res))
									$can_only_view .= ' '.$person_type_who_dgs[$allow['allowed_for_person_type']].';';
								//$can_only_view .= ')';
								//$can = $can_only_view;//$can = $can_edit.' '.$can_only_view;
							
								echo '<th data-type="'.$row['type'].'" data-name="f['.$field['field_id'].']['.$row['field_id'].'][]"'.(empty($can_only_view) ? '>'.$row['title'] : ' title="Gali tik peržiūrėti:'.$can_only_view.'"><span class="abbr">'.$row['title'].'</span>').'</th>';
								//echo '<th data-type="'.$row['type'].'" data-name="f['.$field['field_id'].']['.$row['field_id'].'][]"'.$can/*.(isset($ViewAllowedWidget[(int)$row['field_id']][$_SESSION['USER_DATA']['person_type']]) ? ' disabled="disabled"' : '')*/.'><span class="abbr">'.$row['title'].'</span></th>';
								$field_subfields[$row['field_id']] = $row['type'];
							//}
						}
						if($isEditAllowed)
							echo '<th title="Trynimas eilučių"><span class="abbr">T</span><input type="hidden" name="f['.$field['field_id'].'][dumb]"></th>';
						echo '</tr>';
						if(isset($_GET['edit'])) {
							$field['answer'] = json_decode($field['answer'], true);
							if(!empty($field['answer'])) {
								//print_r($field['answer']);
								$cnt = count(reset($field['answer']));//max cnt
								/*foreach($field['answer'] as $tbl_col_id => $row) {
									if(count($row) > $cnt)
										$cnt = count($row);
								}*/
								//$cnt = count(reset($field['answer']));
								for($i = 0; $i < $cnt; ++$i) {
									echo '<tr>';
									foreach($field['answer'] as $tbl_col_id => $row) {
										//if(isset($ViewAllowedWidget[(int)$tbl_col_id][$_SESSION['USER_DATA']['person_type']])) {
										//	echo '<td>'.filterText($row[$i]).'</td>';
										//} else {// || $isEditAllowed || ADMIN) {
											//$isEditAllowed = ADMIN || isset($EditAllowedWidget[(int)$tbl_col_id][$_SESSION['USER_DATA']['person_type']]);
											//$disabled = ($isEditAllowed ? '' : ' disabled="disabled"');
											$field_id   = 'f['.(int)$field['field_id'].']['.$tbl_col_id.']'.$i;
											$field_name = 'f['.(int)$field['field_id'].']['.$tbl_col_id.'][]';
											$field_value = $row[$i];//isset($row[$i]) ? $row[$i] : '';
											if($field_subfields[$tbl_col_id] == 0)
												echo '<td><input name="'.$field_name.'" id="'.$field_id.'" type="number" min="1" value="'./*(int)*/filterText($field_value).'"'.$disabled.'></td>';
											elseif($field_subfields[$tbl_col_id] == 1)
												echo '<td><textarea name="'.$field_name.'" id="'.$field_id.'"'.$disabled.'>'.filterText($field_value).'</textarea></td>';
										//}
									}
									if($isEditAllowed)
										echo '<td><a href="#" class="remRow abbr" style="color: red" title="Trinti šią eilutę" onclick="return confirm(\'Ar tikrai norite ištrinti eilutę?\')">X</a></td>';
									echo '</tr>';
								}
							}
						}
						echo '</table>';
						if($isEditAllowed)
							echo '<a href="#" class="addRow">Pridėti įvedimo eilutę</a>';
					}
				}
			}
			?>
				<p><input type="submit" value="Išsaugoti" class="submit"></p>
			</form></fieldset>
			<?php
			//if(isset($_GET['edit']))
				echo '<script type="text/javascript" src="/workers/forms_fill.js?6"></script><script>$(function(){
				//setTimeout(function(){
				$("form#persist-data").sisyphus({excludeFields: $("td input, td textarea"), locationBased: true,onRestore:function(){alert(\'Duomenys atstatyti iš praeito neišsaugojimo\');}});
				//}, 3000);
				});</script>';
				//echo '<script>for (var i = 0; i < localStorage.length; i++){console.log(localStorage.key(i) + " " +localStorage.getItem(localStorage.key(i)));}</script>';
		} else
			echo '<div id="form-fill">Tokios formos nėra.</div>';
	}
}


if(isset($_GET['view'])) {
	$answer_result = db_query("SELECT * FROM `".DB_forms_fields_answer."` WHERE $add_filter`answer_id`='".(int)$_GET['view']."'", 'aBloga užklausa: ');
	if(mysqli_num_rows($answer_result) > 0) {
		$answer = mysqli_fetch_assoc($answer_result);
		//print_r($answer);
		//LEFT JOIN kadangi jei bus daug kitų answer'ių, bet maniškiam field'ui nebus įvesta tai būtent dėl to
		$result = db_query("SELECT *, `".DB_forms_fields."`.`field_id`
			FROM `".DB_forms_fields."` LEFT JOIN `".DB_forms_fields_answers."` ON (`".DB_forms_fields."`.`field_id`=`".DB_forms_fields_answers."`.`field_id` AND `".DB_forms_fields_answers."`.`answer_id`='".(int)$_GET['view']."')
			WHERE `".DB_forms_fields."`.`form_id`=".$answer['form_id']."
			ORDER BY `".DB_forms_fields."`.`order` ASC");
		if (isset($_GET['download'])) {
			ob_clean();
			word_header();
		}
		?><div id="planning-view">
		<div class="no-print">Pildyta pagal formą: <?=$form_title[$answer['form_id']]?></div>
		<p><span class="title">Grupė:</span> <?=$grupes[$answer['group_id']]?></p>
		<p><span class="title">Laikotarpis:</span> <?=date_empty($answer['fromDate'])?>–<?=date_empty($answer['toDate'])?></p>
		<?php
		$res = db_query("SELECT * FROM `".DB_forms_fields_allowed."` WHERE `form_id`=".$answer['form_id'].' AND `action`='.PlanningFieldPermissions::Edit);
		$EditAllowed = array();
		while($row = mysqli_fetch_assoc($res)) {
			$EditAllowed[$row['field_id']][$row['allowed_for_person_type']] = '';
			$EditAllowed[$row['field_id']][3] = '';//3 - manager
		}
		$res = db_query("SELECT * FROM `".DB_forms_fields_allowed."` WHERE `form_id`=".$answer['form_id']);//.' AND `action`=1'
		$ViewAllowed = array();
		while($row = mysqli_fetch_assoc($res)) {
			$ViewAllowed[$row['field_id']][$row['allowed_for_person_type']] = '';
			//$ViewAllowed[$row['field_id']][3] = '';//3 - manager
		}
		while($field = mysqli_fetch_assoc($result)) {
			if(ADMIN || isset($ViewAllowed[(int)$field['field_id']][$_SESSION['USER_DATA']['person_type']])) {
				$isEditAllowed = ADMIN || isset($EditAllowed[(int)$field['field_id']][$_SESSION['USER_DATA']['person_type']]);
				$no_print = ($isEditAllowed ? '' : ' class="no-print"');
				if($field['type'] == 0) {//One line
					echo '<p'.$no_print.'><span class="title">'.filterText($field['title']).(!empty($field['help_text']) ? ' <span class="details">('.filterText($field['help_text']).')</span>' : '' ).($field['isRequired'] ? '<span class="required">*</span>' : '').':</span><br>'.filterText($field['answer']).'</p>';
				} elseif($field['type'] == 1) {//Paragraph text
					echo '<p'.$no_print.'><span class="title">'.filterText($field['title']).(!empty($field['help_text']) ? ' <span class="details">('.filterText($field['help_text']).')</span>' : '' ).($field['isRequired'] ? '<span class="required">*</span>' : '').':</span><br>'.nl2br(filterText($field['answer'])).'</p>';
				} elseif($field['type'] == 2) {//Data
					echo '<p'.$no_print.'><span class="title">'.filterText($field['title']).(!empty($field['help_text']) ? ' <span class="details">('.filterText($field['help_text']).')</span>' : '' ).($field['isRequired'] ? '<span class="required">*</span>' : '').':</span><br>'.filterText($field['answer']).'</p>';
				} elseif($field['type'] == 3) {//Individual planning
					echo '<p'.$no_print.'><span class="title">Individuali veikla <span class="details">(Ugdymo individualizavimas pagal vaiko poreikius, gebėjimus ir kitus ypatumus)</span>:</span><br>';
					$res = db_query("SELECT `".DB_children."`.*, `".DB_forms_fields_answers_i."`.`activity` 
						FROM `".DB_forms_fields_answers_i."` JOIN `".DB_children."` ON `".DB_children."`.`ID`=`".DB_forms_fields_answers_i."`.`child_id`
						WHERE `".DB_forms_fields_answers_i."`.`answer_id`=".(int)$answer['answer_id']." ORDER BY ".orderName());//vardas, pavarde
					while ($row = mysqli_fetch_assoc($res))
						echo "<strong>".filterText(getName($row['vardas'], $row['pavarde']))."</strong>: ".filterText($row['activity'])."<br>";
					echo '</p>';
				} elseif($field['type'] == 4 && $KaunoZiburelis) {
					//TODO
				} elseif($field['type'] >= 10) {//---------------------------------------------
					echo '<p'.$no_print.'><span class="title">'.filterText($field['title']).(!empty($field['help_text']) ? ' <span class="details">('.filterText($field['help_text']).')</span>' : '' ).'</p>';
					$res = db_query("SELECT * FROM `".DB_forms_widget_fields_allowed."` WHERE `kindergarten_id`=".DB_ID." AND `widget_id`=".(int)$field['type']);//.' AND `action`=1'
					/*$ViewAllowedWidget = array();
					while($row = mysqli_fetch_assoc($res)) {
						$ViewAllowedWidget[$row['field_id']][$row['allowed_for_person_type']] = '';
						$ViewAllowedWidget[$row['field_id']][3] = '';//3 - manager
					}*/
					$result_w = db_query("SELECT * FROM `".DB_forms_widget_fields."` WHERE `kindergarten_id`=".DB_ID." AND `widget_id`=".(int)$field['type']." ORDER BY `order` ASC");
					echo '<table class="plan">
					<tr>';
					while($row = mysqli_fetch_assoc($result_w)) {
						//if(ADMIN || isset($ViewAllowedWidget[(int)$row['field_id']][$_SESSION['USER_DATA']['person_type']]) || $isEditAllowed) {					
							echo '<th data-type="'.$row['type'].'" data-name="f['.$field['field_id'].']['.$row['field_id'].'][]">'.$row['title'].'</th>';
						//}
					}
					echo '</tr>';
					$field['answer'] = json_decode($field['answer'], true);
					if(!empty($field['answer'])) {
						$cnt = count(reset($field['answer']));
						for($i = 0; $i < $cnt; ++$i) {
							echo '<tr>';
							foreach($field['answer'] as $tbl_col_id => $row) {
								//if(ADMIN || isset($ViewAllowedWidget[(int)$tbl_col_id][$_SESSION['USER_DATA']['person_type']]) || $isEditAllowed) {
									echo '<td>'.nl2br(filterText($row[$i])).'</td>';
								//}
							}
							echo '</tr>';
						}
					}
					echo '</table>';
				}
			}
		}
		?><div class="no-print">
				<div style="float: left; width: 70px;">Sukūrė:</div><div style="float: left;"><?=$answer['created']?> <?=getAllEmployees($answer['createdByEmployeeId'])?></div>
				<div style="clear: left; float: left; width: 70px;">Atnaujino:</div><div style="float: left;"><?=date_empty($answer['updated'])?> <?=getAllEmployees($answer['updatedByEmployeeId'])?></div>
				<div style="clear: left;"></div>
				<?php if(KESV) { ?>
				Keitė kartų: <?=$answer['updatedCounter']?>
				<?php } ?>
			</div>
		</div><?php
		if (isset($_GET['download'])) {
			echo '</body></html>';
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');//application/vnd.openxmlformats-officedocument.wordprocessingml.document http://stackoverflow.com/questions/4212861/what-is-a-correct-mime-type-for-docx-pptx-etc
			header('Content-Disposition: attachment; filename='.basename(date_empty($answer['fromDate']).'–'.date_empty($answer['toDate']).' '.$grupes[$answer['group_id']].' '.$form_title[$answer['form_id']].'.doc'));
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: '.strlen(ob_get_contents()));
			ob_end_flush();
			exit;
		} else {
			ui_print();
			echo ' <a href="?'.http_build_query(array_merge($_GET, ['download' => ''])).'" class="no-print">Atsisiųsti Word</a>';
		}
	} else {
		echo '<p>Nėra tokio užpildyto planavimo.</p>';
	}
}
?>
</div>
