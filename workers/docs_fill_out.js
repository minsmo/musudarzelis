$(function() {
	var answer_id = $('#answer_id').val();
	if(answer_id) {
		function answer_editing() {
			$.get('/workers/docs_fill_out_edit.php?answer_id='+answer_id);
		}
		answer_editing();
		setInterval(answer_editing, 10000);//5000 - 20 concurrent people - 0.05 s.
		window.onbeforeunload = function() {
			$.ajax('/workers/docs_fill_out_edit.php?remove&answer_id='+answer_id, {async : false});
		};
	}
});
