<?php if(!defined('DARBUOT')) exit();
$allowed_only_to_order = !(DIETISTAS || ADMIN);
?>
<h1>Darbuotojų maitinimo žymėjimas</h1>
<div id="content">
<?php
if(isset($_POST['date']) && !preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $_POST['date'])) logdie(msgBox('ERROR', "Blogas datos formatas: Jis turi būti „".date('Y-m-d')."“. ".back()));


if(isset($_GET['delete']) && !$allowed_only_to_order) {
	db_query("DELETE FROM `".DB_worker_food_ate."` WHERE `date`='".db_fix($_GET['delete'])."'");
	msgBox('OK', "Darbuotojų maitinimų kiekio dienoje žymėjimas ištrintas!");
}

if(isset($_POST['save']) && isset($_GET['zymeti']) && !$allowed_only_to_order) {
	$workers_counter = 0;
	//BULK INSERT UPDATE http://stackoverflow.com/questions/6030071/mysql-table-insert-if-not-exist-otherwise-update
	//http://stackoverflow.com/questions/11664684/how-to-bulk-update-mysql-data-with-one-query
	if(isset($_POST['worker_id'])) {
		foreach($_POST['worker_id'] as &$worker_id)
			$worker_id = (int)$worker_id;
		unset($worker_id);//Warning http://php.net/manual/en/control-structures.foreach.php
		
		$workers = [];
		$result = db_query("SELECT `worker_id` FROM `".DB_worker_food_ate."` WHERE `date`='".db_fix($_GET['zymeti'])."' AND `worker_id` IN (".implode(',', $_POST['worker_id']).")");
		while($row = mysqli_fetch_assoc($result))
			$workers[$row['worker_id']] = '';
		$sql_insert = [];
		foreach($_POST['worker_id'] as $worker_id) {
			if(isset($workers[$worker_id])) {
				db_query("UPDATE `".DB_worker_food_ate."` SET `isApproved`=1, `updated`=CURRENT_TIMESTAMP, `updatedByUserId`=".USER_ID.", 
				`updatedByEmployeeId`=".DARB_ID.", `updatedCounter`=`updatedCounter`+1
				WHERE `date`='".db_fix($_GET['zymeti'])."' AND `worker_id`=".(int)$worker_id);
			} else {//INSERT ... ON DUPLICATE KEY UPDATE OR select WHERE date`='".db_fix($_GET['zymeti'])."' AND `vaiko_id` IN () THEN UPDATE/insert
				$sql_insert[] = "(1,'".db_fix($_GET['zymeti'])."',".(int)$worker_id.", '"./*createdByUserId*/USER_ID."', '"./*createdByEmployeeId*/DARB_ID."')";
			}
			$workers_counter++;
		}
		if(!empty($sql_insert))
			db_query("INSERT INTO `".DB_worker_food_ate."` (`isApproved`, `date`, `worker_id`, `createdByUserId`, `createdByEmployeeId`) VALUES ".implode(',', $sql_insert));
		db_query("DELETE FROM `".DB_worker_food_ate."` WHERE `isApproved`=1 AND `date`='".db_fix($_GET['zymeti'])."' AND `worker_id` NOT IN (".implode(',', $_POST['worker_id']).")");
		msgbox('OK', "". $workers_counter . " darbuotojų maitinimas sėkmingai išsaugotas!");
	}
}

if(isset($_GET['order']) && $allowed_only_to_order) {
	//BULK INSERT UPDATE http://stackoverflow.com/questions/6030071/mysql-table-insert-if-not-exist-otherwise-update
	//http://stackoverflow.com/questions/11664684/how-to-bulk-update-mysql-data-with-one-query
	$result = db_query("SELECT `worker_id` FROM `".DB_worker_food_ate."` WHERE `date`='".db_fix($_GET['order'])."' AND `worker_id`=".DARB_ID);
	if(mysqli_num_rows($result) == 0) {
		db_query("INSERT INTO `".DB_worker_food_ate."` (`isApproved`, `orderedDate`, `date`, `worker_id`, `createdByUserId`, `createdByEmployeeId`) VALUES (0,CURRENT_TIMESTAMP(),'".db_fix($_GET['order'])."',".(int)DARB_ID.", '"./*createdByUserId*/USER_ID."', '"./*createdByEmployeeId*/DARB_ID."')");
		msgbox('OK', "Sėkmingai užsakytas darbuotojų maitinimas.");
	}
}


if($allowed_only_to_order) {
	$dates = [];
	$result = db_query("SELECT * FROM `".DB_worker_food_ate."` WHERE `worker_id`=".DARB_ID." ORDER BY `date` DESC LIMIT 0,30");
	//`orderedDate` IS NULL
	while ($row = mysqli_fetch_assoc($result))
		$dates[$row['date']] = $row;
	?>
	<fieldset id="attendance-day-form">
	<legend>Užsakyti maitinimą</legend>
	<form method="get" onsubmit="return validateDate(this)">
		<?php if (isset($dates[date('Y-m-d')])) { ?>
		<p><input class="datepicker" type="text" name="order"> <input style="margin:0" type="submit" value="Užsakyti maitinimą" class="submit"></p>
		<?php } else { ?>
		<p>Automatiškai įrašėme šiandienos datą (jei netiktų - pasirinkite/įrašykite kitą datą)<br><input class="datepicker" type="text" name="order" value="<?=date('Y-m-d')?>"> <input style="margin:0" type="submit" value="Užsakyti maitinimą" class="submit"></p>
		<?php } ?>
	</form>
	</fieldset>
	<?php
	if(mysqli_num_rows($result) > 0) {
		?>
		Paskutinės 30 dienų, kuriomis valgėte ar užsakėte valgyti:
		<ul>
		<?php
		foreach($dates as $date => $row)
			echo '<li>'.$date.($row['isApproved'] ? ' patvirtinta' : ' nepatvirtinta').(!empty($row['orderedDate']) ? ' (Jūs užsakėte)' : '').'</li>';
		?>
		</ul>
		<?php
	}
}

if(!$allowed_only_to_order) { ?>
<a href="?<?=(isset($_GET['date']) ? 'date=' . filterText($_GET['date']).'&amp;' : '')?>new_date#attendance-day-form" id="new-day" class="no-print fast-action fast-action-add">Nauja maitinimo diena</a><?php
}


if( isset($_GET['new_date']) && !$allowed_only_to_order) {
	?>
	<fieldset id="attendance-day-form">
	<legend>Nauja darbuotojų maitinimo diena</legend>
	<form method="get" onsubmit="return validateDate(this)">
		<!-- <?=(isset($_GET['edit'])  ? 'Paredagavus darbo dieną jeigu joje buvo įvestas lankomumas jis automatiškai persikelia į pakeistą dieną.':'')?> -->
		<p><span class="abbr" title="Datos įprastas formatas <?=date('Y-m-d')?>"><!-- Data: --></span> <input class="datepicker" type="text" name="zymeti" value="<?=(/*isset($_GET['edit']) ? $data :*/ date('Y-m-d'))?>">  <input style="margin:0" type="submit" value="Pereiti į darbuotojų maitinimo žymėjimą" class="submit"></p>
	</form>
	</fieldset>
	<?php
} elseif( !$allowed_only_to_order || BUHALT ) {

	if(isset($_GET['zymeti']) && !BUHALT) {
		$mark_date = $_GET['zymeti'];
		?>
		<script type="text/javascript" src="/libs/jquery.checkboxes.min.js"></script>
		<script type="text/javascript">
		$(function($) {
			$('#grupes_lankomumas').checkboxes('range', true);
		});
		</script>
		<fieldset id="attendance-form">
		<legend>Darbuotojų maitinimo redagavimas: <?=filterText($mark_date)?></legend>
		<form method="post" action="?zymeti=<?=filterText($mark_date)?><?php /* remember filter parameters: */ if(isset($_GET['date'])) echo '&amp;date='.filterText($_GET['date']); ?>" class="not-saved-reminder">
			<a href="#attendance-form" class="attendance-checkboxes" data-toggle="checkboxes" data-action="check">Žymėti </a><a href="#attendance-form" class="attendance-checkboxes" data-toggle="checkboxes" data-action="check" style="font-weight: bold;">visi valgo</a>
			<?php //Buvo „Pažymėti visus vaikus, kad yra“. „Žymėti Yra visi“ atrodo, kad kažkuo pažymėti, Žymėjimas ?>
			<!--<a href="#attendance-form" data-toggle="checkboxes" data-action="uncheck">Pažymėti, kad nė vieno nėra</a> -->
			<table id="grupes_lankomumas">
			<tr>
				<th title="Imami iš meniu punkto „Darbuotojai“"><span class="abbr">Darbuotojas</span></th>
				<th>Gavo maitinimą</th>
				<th>Užsisakė</th>
			<?php
			$i = 0;
			//Kids: Valid, not archived
			$result = getValidEmployeesOrdered_db_query();
			//$sk = mysqli_num_rows($result);//TODO: Jeigu perdarysiu JS
			$worker_id = [];
			$workers = [];
			while($row = mysqli_fetch_assoc($result)) {
				$worker_id[] = $row['ID'];
				$workers[$row['ID']] = $row['vardas'].' '.$row['pavarde'];
			}
			$worker_food = [];
			if(!empty($worker_id)) {
				$sel = db_query("SELECT * FROM `".DB_worker_food_ate."` WHERE `worker_id` IN (".implode(',', $worker_id).") AND `date`='".db_fix($mark_date)."'");
				while($row = mysqli_fetch_assoc($sel))
					$worker_food[$row['worker_id']] = $row;
			}
			foreach($workers as $worker_id => $name) {
				echo "\t<tr><td>".filterText($name)."</td>
					<td><input type=\"checkbox\" name=\"worker_id[]\" value=\"".$worker_id."\"".(isset($worker_food[$worker_id]) ? ' checked="checked"' : '')."></td>
					<td>".(!empty($worker_food[$worker_id]['orderedDate']) ? 'Pats darbuotojas' : '')."</td>
					</tr>\n";
				++$i;// && !empty($worker_food[$worker_id])
			}
			if($i != 0)
				echo '<tr><td colspan="5" id="counter"></td></tr>';
			?>
			</table>
			<?php if($i != 0) { ?>
			<p><input type="submit" name="save" value="Patvirtinti maitinimą" class="submit"></p>
			<?php } ?>
		</form>
		</fieldset>
		<?php
	}



	$result = db_query("SELECT YEAR(`date`) AS `metai`, MONTH(`date`) AS `menuo` FROM `".DB_worker_food_ate."` GROUP BY YEAR(`date`) DESC, MONTH(`date`) DESC");
	if(mysqli_num_rows($result)) {
		?>
		<h2>Žymėtos maitinimo dienos</h2>
		<form method="get">
			<div class="sel" style="float: left; margin-right: 5px; line-height: 33px;">
			<select name="date">
				<?php
				if(isset($_POST['date'])) {
					list($year, $month) = explode('-', $_POST['date']);//, $day
					$year = (int) $year;
					$month = (int) $month;
				}
				if(isset($_GET['date'])) {
					list($year, $month) = explode('-', $_GET['date']);//, $day
					$year = (int) $year;
					$month = (int) $month;
				}
				while ($row = mysqli_fetch_assoc($result))
					echo "<option value=\"".$row['metai']."-".$row['menuo']."\"".((isset($_GET['date']) || isset($_POST['date'])) && $row['metai'] == $year && $row['menuo'] == $month ? ' selected="selected"' : '').">".$row['metai']."-".men($row['menuo'])."</option>";
				?>
			</select></div>
			<?php if(!BUHALT) { ?>
			<input type="submit" name="filter" value="Filtruoti" class="filter">
			<?php } ?>
			<input type="submit" name="report" value="Ataskaita „Dienos“" class="filter">
			<input type="submit" name="report_workers" value="Ataskaita „Darbuotojai“" class="filter">
		</form>
		<?php
	}
	
	if(!(isset($_GET['report']) || isset($_GET['report_workers'])) && !BUHALT) {
		if(isset($_GET['date'])) {
			list($year, $month) = explode('-', $_GET['date']);
			$result = db_query("SELECT * FROM `".DB_worker_food_ate."` WHERE YEAR(`date`)=".(int)$year." AND MONTH(`date`)=".(int)$month." GROUP BY `date` ORDER BY `date` DESC");
		} else
			$result = db_query("SELECT * FROM `".DB_worker_food_ate."` GROUP BY `date` ORDER BY `date` DESC LIMIT 0,30");

		if(mysqli_num_rows($result) > 0) {
			if(!isset($_GET['date']))
				echo '<em>Sąraše dabar rodoma 30 naujausių dienų.</em>';
			?>
			<table border="1">
			<tr>
				<th>Data</th>
				<th>Veiksmai</th>
			</tr>
			<?php
			while ($row = mysqli_fetch_assoc($result)) {
				echo "\t<tr>
					<td>".$row['date']."</td>
					<td><a href=\"?zymeti=".$row['date'].(isset($_GET['date']) ? "&amp;date=".filterText($_GET['date']) : '')."#attendance-form\">Žymėti maitinimą</a>
						<a href=\"?delete=".$row['date']."\" onclick=\"return confirm('Ar tikrai norite ištrinti?')\">Trinti</a>
					</td>
				</tr>";
			}
			?>
			</table>
			<?php
		}
	} elseif(isset($_GET['report']) && isset($_GET['date'])) {

		list($year, $month) = explode('-', $_GET['date']);
		$result = db_query("SELECT `date`, COUNT(*) `cnt` FROM `".DB_worker_food_ate."` WHERE `isApproved`=1 AND YEAR(`date`)=".(int)$year." AND MONTH(`date`)=".(int)$month." GROUP BY `date` ORDER BY `date`");
		
		if(mysqli_num_rows($result) > 0) {
			$dates = [];
			while ($row = mysqli_fetch_assoc($result))
				$dates[$row['date']] = $row['cnt'];
			?>
			<table border="1">
			<tr>
				<th style="text-align: center" colspan="<?=(count($dates)+1)?>"><?=$year?>-<?=$month?></th>
			</tr>
			<tr>
				<td>Darželio darbo diena</td>
				<?php
				foreach($dates as $date => $cnt)
					echo '<td>'.substr($date, -2, 2).'</td>';
				?>
			</tr>
			<tr>
				<td>Darbuotojų maitinimų kiekis (patvirtinta)</td>
				<?php
				foreach($dates as $cnt)
					echo '<td>'.$cnt.'</td>';
				?>
			</tr>
			</table>
			<?php
		}
	} elseif(isset($_GET['report_workers']) && isset($_GET['date'])) {
		list($year, $month) = explode('-', $_GET['date']);
		$result = db_query("SELECT `worker_id`, `date` FROM `".DB_worker_food_ate."` WHERE `isApproved`=1 AND YEAR(`date`)=".(int)$year." AND MONTH(`date`)=".(int)$month." ORDER BY `date`");//, COUNT(*) `cnt` GROUP BY `worker_id`
		if(mysqli_num_rows($result) > 0) {
			$cnt = [];
			$cntByDate = [];
			$cntByWorker = [];
			$total = 0;
			while ($row = mysqli_fetch_assoc($result)) {
				$cnt[$row['worker_id']][$row['date']] = '';// = $row['cnt']
                                if(!isset($cntByDate[$row['date']]))
                                    $cntByDate[$row['date']] = 0;
				++$cntByDate[$row['date']];
                                if(!isset($cntByWorker[$row['worker_id']]))
                                    $cntByWorker[$row['worker_id']] = 0;
				++$cntByWorker[$row['worker_id']];
				++$total;
			}
			?>
			<table border="1">
			<tr>
				<th style="text-align: center" colspan="<?=count($cntByDate)+4/*or 2*/?>"><?=$year?>-<?=$month?></th>
			</tr>
			<tr>
				<th rowspan="2">Darbuotojas</th>
				<th colspan="<?=count($cntByDate)+1?>">Maitinimų kiekis (patvirtinta)</th>
				<?php
				if(DB_PREFIX == 'knsAtzal_') {
					echo '<th rowspan="2">Iš viso<br>suma</th>';
					echo '<th rowspan="2">Pastabos</th>';
				}
				?>
			</tr>
			<tr>
				<?php
				foreach($cntByDate as $date => $dumb)
					echo '<th>'.substr($date, -2, 2).'</th>';
				?>
				<th><strong>Iš viso</strong></th>
			</tr>
			<?php
			foreach($cnt as $worker_id => $dumb) {
				echo '<tr><td>'.getAllEmployees($worker_id).'</td>';//<td>'.$cnt.'</td></tr>';
				foreach($cntByDate as $date => $dumb)
					echo '<td>'.(isset($cnt[$worker_id][$date]) ? 1 : '').'</td>';
				echo '<td>'.(isset($cntByWorker[$worker_id]) ? $cntByWorker[$worker_id] : '').'</td>';
				if(DB_PREFIX == 'knsAtzal_') {
					echo '<td>'.(isset($cntByWorker[$worker_id]) ? price($cntByWorker[$worker_id]*getConfig('darbuotojo_maitinimo_kaina_dienai')) : '').'</td>';
					echo '<td></td>';
				}
				
				echo '</tr>';
			}
			echo '<tr><td><strong>Iš viso maitinimų</strong></td>';
				foreach($cntByDate as $date => $cnt)
					echo '<td>'.$cntByDate[$date].'</td>';
				?>
				<td><?=$total?></td>
				<?php
				if(DB_PREFIX == 'knsAtzal_') {
					echo '<td></td>';
					echo '<td></td>';
				}
			echo '</tr>';
			if(DB_PREFIX == 'knsAtzal_') {
				echo '<tr><td><strong>Iš viso suma</strong></td>';
				$sum = 0;
				foreach($cntByDate as $date => $cnt) {
					$sum += $cntByDate[$date]*getConfig('darbuotojo_maitinimo_kaina_dienai');
					echo '<td>'.price($cntByDate[$date]*getConfig('darbuotojo_maitinimo_kaina_dienai')).'</td>';
				}
				echo '<td></td><td>'.price($sum).'</td><td></td></tr>';
			}
			?>
			</table>
			<?php
		}
	}
}
?>
</div>
<script type="text/javascript" src="/workers/worker_food_ate.js?2"></script>
