<?php if(!defined('DARBUOT') || !(ADMIN || LOGOPEDAS)) exit(); ?>
<h1><span class="no-print"><?php
$name = $l->getName(filterText($pareigybes[$_SESSION['SPEECH_THERAPIST_position_id']]), 'kil');
if($name == 'Specialusio Pedagogo')
	$name = 'Specialiojo pedagogo';
//TODO: modify for other things.
echo $name;
?> </span>pratybų, specialiųjų pamokų tvarkaraštis <small style="font-weight: normal;">(galimybė matyti tėvams)</small></h1>
<div id="content">
<?php
if (isset($_POST['save'])) {
	db_query("INSERT INTO `".DB_speech_schedule."` SET
		`diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']},
		`time`='".db_fix($_POST['time'])."', `day`=".(int)$_POST['day'].", 
		`speech_group_id`=".(int)$_POST['speech_group_id'].", 
		`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
	echo msgBox('OK', 'Sėkmingai išsaugota.');
}
if (isset($_POST['update'])) {
	db_query("UPDATE `".DB_speech_schedule."` SET 
		`diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']},
		`time`='".db_fix($_POST['time'])."', `day`=".(int)$_POST['day'].", 
		`speech_group_id`=".(int)$_POST['speech_group_id'].",
		`updated`=CURRENT_TIMESTAMP, `updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."', `updatedCounter`=`updatedCounter`+1
		WHERE `ID`=".(int)$_POST['update']);
	msgBox('OK', 'Sėkmingai atnaujinta.');
}

if (isset($_GET['delete'])) {
	db_query("DELETE FROM ".DB_speech_schedule." WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `ID`=".(int)$_GET['delete']);
	msgBox('OK', 'Sėkmingai ištrinta');
}
?>
<a href="?#additional-activity" class="no-print fast-action fast-action-add">Įvesti naują įrašą tvarkaraštyje</a> <?=ui_print()?>

<table class="vertical-hover">
<tr>
	<th>Laikas</th>
	<th>Diena</th>
	<th>Grupė</th>
	<th class="no-print">Grupės vaikai</th>
	<th class="no-print">Veiksmai</th>
</tr>
<?php
/*$kids_of_schedule = [];
$result = db_query("SELECT `".DB_bureliai_schedule_kids."`.*, cr.*
	FROM `".DB_bureliai_schedule_kids."`
	JOIN `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<=CURDATE() GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from` AND cr.`parent_kid_id`=`".DB_bureliai_schedule_kids."`.`kid_id`
	WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 ORDER BY ".orderName('cr'));

while($row = mysqli_fetch_assoc($result)) {
	$kids_of_schedule[$row['bureliai_schedule_id']][] = array(
		'kid_id' => $row['kid_id'],
		'name' => filterText(getName($row['vardas'], $row['pavarde'])),
	);
}*/
$result = db_query("SELECT `".DB_speech_groups."`.`title`, `".DB_speech_schedule."`.*
	FROM `".DB_speech_schedule."` 
	LEFT JOIN `".DB_speech_groups."` ON (`".DB_speech_schedule."`.`speech_group_id`=`".DB_speech_groups."`.`ID` AND `".DB_speech_groups."`.`diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']})
	WHERE `".DB_speech_schedule."`.`diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `".DB_speech_groups."`.`isArchived`=0
	ORDER BY `day`, `time`, `title`");
	//`".DB_speech_groups."`.`pavadinimas`
while($row = mysqli_fetch_assoc($result)) { 
?>
<tr>
	<td><?=filterText($row['time'])?></td>
	<td><?=$dienos_kada[$row['day']]?></td>
	<td><?=filterText($row['title'])?></td>
	<td class="no-print">
		<?php
		//TODO: finish (get valid kid data)
		$result_d = db_query("SELECT `".DB_children."`.* 
		FROM `".DB_speech_groups_kids."` JOIN `".DB_children."`  ON `".DB_speech_groups_kids."`.`kid_id`=`".DB_children."`.`parent_kid_id` 
		WHERE `group_id`=".(int)$row['speech_group_id'].' AND `diary_id`='.$_SESSION['SPEECH_THERAPIST_DIARY'].'
		GROUP BY `kid_id` -- fix
		ORDER BY `vardas`, `pavarde`');//TODO: in the future maybe someone need to add the same kid twice then will be a need for GROUP BY `kid_id`
		
		if(mysqli_num_rows($result_d) > 0) {
			$tmp = array();
			while ($d = mysqli_fetch_assoc($result_d))
				$tmp[] = filterText($d['vardas']).' '.filterText($d['pavarde']);
			echo implode(",<br>", $tmp);
		} else {
			//echo 'Dėmesio! Norint, kad auklėtojos ir muzikos pedagogės galėtų dirbti<!-- prisijungti ir prisijungusios dirbti --> su šia grupe privaloma priskirti norimus darbuotojus prie šios grupės. Tai padaryti pirmiausia spauskite ant nuorodos, esančios dešiniau, pavadinimu „Keisti darbuotojų priskyrimus“.';
		}
		?></td>
	<td class="no-print">
		<a href="?edit=<?=$row['ID']?>">Keisti</a>
		<a href="?delete=<?=$row['ID']?>" onclick="return confirm('Ar tikrai norite trinti?')">Trinti</a>
	</td>
</tr>
<?php
}

?>
</table>

<?php
if (isset($_GET['edit'])) {
	$result = db_query("SELECT * FROM `".DB_speech_schedule."` WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `ID`=".(int)$_GET['edit']);
	$edit = mysqli_fetch_assoc($result);
}
?>
<fieldset id="additional-activity" class="no-print">
<legend><?=(isset($_GET['edit']) ? "Tvarkaraščio keitimas" : "Naujas įrašas")?></legend>
<form method="post" class="not-saved-reminder">
	<div>Grupė <div class="sel"><select name="speech_group_id" required="required">
	<option value="" selected="selected" disabled selected hidden>Pasirinkite grupę</option>
	<?php
	$result = getDbQuerySpeechGroups();//Desc?
	if(mysqli_num_rows($result) > 0) {
		while ($row = mysqli_fetch_assoc($result)) {
			echo "\t\t<option value=\"".$row['ID']."\"".((isset($_GET['edit']) && $row['ID'] == $edit['speech_group_id']) ? ' selected="true"' : '').">".filterText($row['title'])."</option>\n";	
		}
	}
	?>
	</select></div></div>
	<div style="float: left; margin-top: 14px;">Diena</div>
	<div class="sel" style="margin-left: 5px; margin-top: 10px; margin-bottom: 10px"><select name="day" required="required">
			<?php
			foreach($dienos_kada as $id => $val)
				echo "<option value=\"$id\"".(isset($_GET['edit']) && $id == $edit['day'] ? ' selected="true"' : '').">$val</option>";
			?>
	</select></div>
	<div>Laikas <input type="text" name="time" maxlength="20" style="width: 100px" required="required" value="<?php echo (isset($_GET['edit']) ? $edit['time'] : ''); ?>"> <span class="notice">(laikas rikiuojamas pagal skaičius, todėl 8 valandą rekomenduojame rašyti 08)</span></div>
	<p><input type="hidden" <?=(!isset($_GET['edit']) ? 'name="save"' : 'name="update" value="'.(int)$edit['ID'].'"')?>><input type="submit" value="Išsaugoti" class="submit"></p>
</form>
</fieldset>
<script src="/workers/bureliai_schedule.js"></script>
</div>
