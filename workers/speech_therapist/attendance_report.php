<?php if(!defined('DARBUOT') ) exit(); ?>
<h1><?php
$name = $l->getName(filterText($pareigybes[$_SESSION['SPEECH_THERAPIST_position_id']]), 'kil');
if($name == 'Specialusio Pedagogo')
	$name = 'Specialiojo pedagogo';
//TODO: modify for other things.
echo $name;
?> vaikų grupių lankomumo ataskaita</h1>
<div id="content">
<form method="get" style="padding-bottom:10px;" class="no-print">
	<div class="sel" style="float: left; margin-right: 7px;"><select name="date" required="required">
	<?php
	if(isset($_GET['date'])) {
		list($year, $month) = explode('-', $_GET['date']);//, $day
		$year = (int) $year;
		$month = (int) $month;
	}
	$result = db_query("SELECT YEAR(`date`) `metai`, MONTH(`date`) `menuo` FROM `".DB_speech_attendance."` WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} GROUP BY YEAR(`date`) DESC, MONTH(`date`) DESC");
	//Neisigilinau, bet kažkas įdomaus: http://stackoverflow.com/questions/15390303/how-to-group-by-desc-order
	//http://use-the-index-luke.com/3-minute-test/mysql?quizkey=d2a088303b955b839e29cb8711a85235
	while ($row = mysqli_fetch_assoc($result))
		echo "<option value=\"".$row['metai']."-".$row['menuo']."\"".(isset($_GET['date']) && $row['metai'] == $year && $row['menuo'] == $month ? ' selected="selected">'.$selectedMark : '>').$row['metai']."-".men($row['menuo'])."</option>";
	?>
	</select></div>
	
	<div class="sel" style="float: left; margin-right: 7px;"><select name="group_id">
	<?php
	$result = getDbQuerySpeechGroups();
	$speech_groups = [];
	echo "<option value=\"0\"".(isset($_GET['group_id']) && 0 == $_GET['group_id'] ? ' selected="selected">'.$selectedMark : '>')."Visos</option>";
	while($row = mysqli_fetch_assoc($result)) {
		echo "<option value=\"".$row['ID']."\"".(isset($_GET['group_id']) && $row['ID'] == $_GET['group_id'] ? ' selected="selected">'.$selectedMark : '>').$row['title']."</option>";
		$speech_groups[$row['ID']] = $row['title'];
	}
	?>
	</select></div>
	<input type="submit" class="filter" value="Rodyti">
</form>

<?php
if(isset($_GET['date']) && isset($_GET['group_id'])) {
	echo ui_print();
	$date_max = $year.'-'.men($month).'-'.date('t', strtotime($year.'-'.men($month).'-01'));
	$date_min = $year.'-'.men($month).'-01';
	
	
	if($_GET['group_id'] == 0)
		$groups = $speech_groups;
	else
		$groups = [(int)$_GET['group_id'] => $speech_groups[(int)$_GET['group_id']]];
	
	foreach($groups as $group_id => $dump) {
		$working_days_result = db_query("SELECT DAY(`date`) `day` FROM `".DB_speech_attendance."` WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND YEAR(`date`)='".$year."' AND MONTH(`date`)='".(int)$month."' AND `kid_group_id`=".(int)$group_id." GROUP BY DAY(`date`)");
		$working_days_cnt = mysqli_num_rows($working_days_result);

		//Jeigu vaikas ištrintas ką padaryti, kad ...? //copied from tabeliai.php
		$attendance_result = db_query("SELECT `".DB_speech_attendance."`.*, cr.*
			FROM `".DB_speech_attendance."` 
			JOIN `".DB_children."` cr ON (cr.`parent_kid_id`=`".DB_speech_attendance."`.`kid_id` AND cr.`valid_from`<=`".DB_speech_attendance."`.`date`)
			LEFT JOIN `".DB_children."` mx ON cr.`parent_kid_id`=mx.`parent_kid_id` AND cr.`valid_from`<mx.`valid_from` AND cr.`valid_from`<=`".DB_speech_attendance."`.`date` AND mx.`valid_from`<=`".DB_speech_attendance."`.`date`
			WHERE cr.`isDeleted`=0 AND 
			`".DB_speech_attendance."`.`date` BETWEEN '".$date_min."' AND '".$date_max."'
			-- YEAR(`".DB_speech_attendance."`.`date`)=".$year." AND MONTH(`".DB_speech_attendance."`.`date`)=".(int)$month."
			 AND `".DB_speech_attendance."`.`kid_group_id`=".(int)$group_id." AND mx.`parent_kid_id` IS NULL AND `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']}
			GROUP BY cr.`parent_kid_id` -- `vardas`, `pavarde`
			ORDER BY ".orderName('cr', $date_max));//cr.`vardas`, cr.`pavarde`
	
	
		$all_attendance = db_query("SELECT *, DAY(`date`) `day` FROM `".DB_speech_attendance."` 
			WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `date` BETWEEN '".$date_min."' AND '".$date_max."' AND `kid_group_id`=".(int)$group_id);
		$attended = array();
		while ($row = mysqli_fetch_assoc($all_attendance)) {
			if(!isset($attended[$row['kid_id']]))
				$attended[$row['kid_id']] = array();
			$attended[$row['kid_id']][$row['day']] = $row['yra'];
		}
		?>
		<p><strong><?php
		if(mysqli_num_rows($attendance_result) == 1) {
			$row = mysqli_fetch_assoc($attendance_result);
			echo filterText(getName($row['vardas'], $row['pavarde'], $date_max)).'</strong> iš grupės/pogrupio <strong>';
			mysqli_data_seek($attendance_result, 0);
		}
		?>
		<?=$speech_groups[$group_id]?></strong></p>
		<table>
		<tr>
			<th rowspan="2">Eil. nr.</th>
			<th rowspan="2">Vardas ir pavardė</th>
			<th>Mėnuo</th>
			<th colspan="<?=$working_days_cnt?>"><?=($year.'-'.men($month))?></th>
		</tr>
		<tr>
			<th>Klasė, grupė \ Diena</th>
			<?php
			$all_days = array();
			while ($row = mysqli_fetch_assoc($working_days_result)) {
				echo '<th>'.$row['day'].'</th>';
				$all_days[] = $row['day'];
			}
			?>
		</tr>
		<?php
		$i = 0;
		while ($row = mysqli_fetch_assoc($attendance_result)) {
			echo '<tr>';
				echo "<td>".++$i."</td>";
				echo "<td>".filterText(getName($row['vardas'], $row['pavarde'], $date_max))."</td>";
				echo "<td>".getAllGroups($row['grupes_id'])."</td>";
				foreach($all_days as $day)
					echo "<td>".(isset($attended[$row['parent_kid_id']][$day]) ? ($attended[$row['parent_kid_id']][$day] == 1 ? ' ' : 'n') : '-')."</td>";
			echo '</tr>';
		}
		?>
		</table>
		<!-- PASTABOS: textarea -->
		<?php
	}
}
?>
</div>
