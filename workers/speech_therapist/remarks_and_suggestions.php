<?php if(!defined('DARBUOT') || !(ADMIN || LOGOPEDAS)) exit(); ?>
<h1 class="abbr" title="Pildo tik vadovai"><span class="no-print"><?php
$name = $l->getName(filterText($pareigybes[$_SESSION['SPEECH_THERAPIST_position_id']]), 'kil');
if($name == 'Specialusio Pedagogo')
	$name = 'Specialiojo pedagogo';
//TODO: modify for other things.
echo $name;
?></span> PASTABOS IR SIŪLYMAI DĖL DIENYNO TVARKYMO</h1>
<div id="content">
<?php
$pareigybes[0] = 0;

$darbuotojai = array();
$result = db_query("SELECT * FROM `".DB_employees."`");
$darbuotojai[0] = '';
$positions = array();
$positions[0] = 0;
while ($row = mysqli_fetch_assoc($result)) {
	$darbuotojai[$row['ID']] = filterText($row['vardas'].'  '.$row['pavarde']);//Position selection is not so good, because in time it does change
	$positions[$row['ID']] = $row['pareigybes'];
}
//TODO: save corporate position titles in DB_speech_remarks_and_suggestions
//Off topic http://radiuscambodia.files.wordpress.com/2012/04/typical-organizational-position-title-1.pdf
if(isset($_POST['save']) && !LOGOPEDAS) {
	db_query("INSERT INTO `".DB_speech_remarks_and_suggestions."` SET 
		`kindergarten_id`=".DB_ID.",
		`diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']},
		`date`='".db_fix($_POST['date'])."',
		`remarks_and_suggestions`='".db_fix($_POST['remarks_and_suggestions'])."',
		`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."', `createdByPosition_id`=".(int)$positions[DARB_ID]);
	msgBox('OK', 'Pastebėjimo įrašas išsaugotas.');
}
if(isset($_POST['edit']) && !LOGOPEDAS) {
	db_query("UPDATE `".DB_speech_remarks_and_suggestions."` SET 
		`diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']},
		`date`='".db_fix($_POST['date'])."',
		`remarks_and_suggestions`='".db_fix($_POST['remarks_and_suggestions'])."',
		`updated`=CURRENT_TIMESTAMP, `updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."',
		`updatedByPosition_id`=".(int)$positions[DARB_ID].",
		`updatedCounter`=`updatedCounter`+1
		WHERE `kindergarten_id`=".DB_ID." AND `ID`=".(int)$_POST['edit']);
	msgBox('OK', 'Pastebėjimo įrašas išsaugotas.');
}
if(isset($_GET['delete']) && !LOGOPEDAS) {
	db_query("DELETE FROM `".DB_speech_remarks_and_suggestions."` WHERE `kindergarten_id`=".DB_ID." AND `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `ID`=".(int)$_GET['delete']);
	msgBox('OK', 'Pastebėjimo įrašas išsaugotas.');
}
?>
<?php if(!LOGOPEDAS) { ?>
<a href="?new#menu-form" class="no-print fast-action fast-action-add">Naujas veiklos įrašas</a> <?php } ?> <?=ui_print()?>
<table class="activities">
<tr>
	<th class="date-cell">Data</th>
	<th>Pastabos ir pasiūlymai</th>
	<th>Sukūrė</th>
	<th>Pakeitė</th>
	<?php if(!LOGOPEDAS) { ?>
	<th class="no-print">Veiksmai</th>
	<?php } ?>
</tr>
<?php
$result = db_query("SELECT * FROM `".DB_speech_remarks_and_suggestions."` WHERE `kindergarten_id`=".DB_ID." AND `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} ORDER BY `date`");
while($row = mysqli_fetch_assoc($result)) {
	echo "<tr>
		<td>".filterText($row['date'])."</td>
		<td>".nl2br(filterText($row['remarks_and_suggestions']))."</td>
		<td><span class=\"no-print abbr notice\" title=\"Nespausdinama\">".filterText(substr(date_empty($row['created']), 0, 10)).'</span> '.filterText($darbuotojai[$row['createdByEmployeeId']].' '.$pareigybes[$row['createdByPosition_id']])."</td>
		<td><span class=\"no-print abbr notice\" title=\"Nespausdinama\">".filterText(substr(date_empty($row['updated']), 0, 10)).'</span> '.filterText($darbuotojai[$row['updatedByEmployeeId']].' '.$pareigybes[$row['updatedByPosition_id']])."</td>";
		if(!LOGOPEDAS) {
			echo "<td class=\"no-print\"><a href=\"?edit=".$row['ID']."\">Keisti</a> <a href=\"?delete=".$row['ID']."\" onclick=\"return confirm('Ar tikrai norite ištrinti pastebėjimo įrašą?')\">Trinti</a></td>";
		}
	echo "
	</tr>";
}
echo '</table>';

if ((isset($_GET['edit']) || isset($_GET['new'])) && !LOGOPEDAS) {
	if (isset($_GET['edit'])) {
		$result = db_query("SELECT * FROM `".DB_speech_remarks_and_suggestions."` WHERE `kindergarten_id`=".DB_ID." AND `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `ID`=".(int)$_GET['edit']);
		$edit = mysqli_fetch_assoc($result);
	}
	?>
	<fieldset id="menu-form">
	<legend><?=(isset($_GET['edit']) ? 'Pastebėjimo įrašo keitimas' : 'Naujas pastebėjimo įrašas')?></legend>
	<form method="post" class="not-saved-reminder no-print">
		<p><label>Data <input class="datepicker" type="text" name="date" value="<?=(isset($edit) ? filterText($edit['date']) : '')?>"></label></p>
		<p><label>Pastabos ir pasiūlymai <textarea name="remarks_and_suggestions"><?=(isset($edit) ? filterText($edit['remarks_and_suggestions']) : '')?></textarea></label></p>
		<p><input type="hidden" <?=(!isset($_GET['edit']) ? 'name="save"' : 'name="edit" value="'.(int)$edit['ID'].'"')?>><input type="submit" value="Išsaugoti" class="submit"></p>
	</form>
	</fieldset>
<?php
}
?>
</div>
