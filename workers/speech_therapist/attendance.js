$(function() {
	$('.addScnt').on('click', function(event) {
		event.preventDefault();
		var id = $(this).data("id");
		var scntDiv = $(this).next('.p_scents');
		//
		//$('<p><label>'+$("#kidId option:selected").text()+' <input type="text" class="individual_plan" name="individuali_veikla['+id+']"></label> <a href="#" class="remScnt" id="individual'+id+'">Trinti</a></p>').appendTo(scntDiv);// data-id="'+id+'"
		$('<p><select name="tipas['+id+'][]"><option class="green" value="0" selected="selected">Išmoko</option><option class="yellow" value="1">Įpusėjo</option><option class="red" value="2">Neišmoko</option></select><input type="text" name="pavadinimas['+id+'][]"> <a href="#" class="remScnt">Trinti</a></p>').appendTo(scntDiv);
	});

	$(document).on('click', '.remScnt', function (event) {
		event.preventDefault();
		$(this).parents('p').remove();
	});
	
	
	function eachCheckbox() {
		$("input[type='checkbox']").each(function() {
			var thisCheck = $(this).parent().parent();
			if ( this.checked )//thischeck.is(':checked')
				thisCheck.removeClass("absent").addClass("present");
			else
				thisCheck.addClass("absent").removeClass("present");
		});
	}
	eachCheckbox();
	$( "#attendance-checkboxes" ).click(function() {
		setTimeout(eachCheckbox, 1);
	});
	$(":checkbox").click(function() {
		setTimeout(eachCheckbox, 1);
	});
	
	$("#suggested-date").change(suggestedDate).map(suggestedDate);
	function suggestedDate() {
		if($(this).val() == $(this).data('date'))
			$(this).addClass('suggested');
		else
			$(this).removeClass('suggested');
	}
	
	function array_flip( trans ) {
		var key, tmp_ar = {};
		for ( key in trans )
			if ( trans.hasOwnProperty( key ) )
				tmp_ar[trans[key]] = key;
		return tmp_ar;
	}
	function getURLParameter(name) {
		return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null
	}
	
	function day_filter() {
		var filter = array_flip($(this).val().split(',')), first = true;
		//$("#filtered-group-id option:selected").data('ids').split(',');
		$('#filtered-group-id option').each(function() {
			if ($(this).val() in filter) {
				$(this).show();
			} else {
				$(this).hide();
			}
		});
		$('#filtered-group-id option').each(function() {
			if ($(this).val() in filter && first && getURLParameter('group_id') == $(this).val()) {// in filter
				$(this).attr('selected', 'selected');//.val($(this).val());
				first = false;
				return;
			}
		});
		$('#filtered-group-id option').each(function() {
			if ($(this).val() in filter && first) {
				$(this).attr('selected', 'selected');
				first = false;
				return;
			}
		});
	}
	$('#day').map(day_filter);
	$('#day').change(day_filter);
	
});
function validateDate(thisForm) {
	var error="";
	var dataFilter = /^\d{4}-\d{1,2}-\d{1,2}$/;
	//if (!dataFilter.test(thisForm.data.value)) {
	//	thisForm.data.style.background = 'Yellow';
	if (!dataFilter.test(thisForm.zymeti.value)) {
		thisForm.zymeti.style.background = 'Yellow';
		error += "Blogas datos formatas: jis turi būti „****-**-**“.\n";
	}

	if (error != "") {
		alert("Blogai užpildyta:\n" + error);
		return false;
	}

	return true;
}
