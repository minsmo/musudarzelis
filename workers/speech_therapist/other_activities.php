<?php if(!defined('DARBUOT') || !(LOGOPEDAS || ADMIN)) exit(); ?>
<h1><span class="no-print"><?php
$name = $l->getName(filterText($pareigybes[$_SESSION['SPEECH_THERAPIST_position_id']]), 'kil');
if($name == 'Specialusio Pedagogo')
	$name = 'Specialiojo pedagogo';
//TODO: modify for other things.
echo $name;
?></span> KITA VEIKLA</h1>
<div id="content">
<?php
if(isset($_POST['save'])) {
	db_query("INSERT INTO `".DB_speech_other_activities."` SET 
		`diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']},
		`date`='".db_fix($_POST['date'])."',
		`activity`='".db_fix($_POST['activity'])."',
		`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
	msgBox('OK', 'Veiklos įrašas išsaugotas.');
}
if(isset($_POST['edit'])) {
	//$_POST['group_id'] = GROUP_ID;
	//`group_id`='".(int)$_POST['group_id']."', 
	db_query("UPDATE `".DB_speech_other_activities."` SET 
		`diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']},
		`date`='".db_fix($_POST['date'])."',
		`activity`='".db_fix($_POST['activity'])."',
		`updated`=CURRENT_TIMESTAMP, `updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."',
		`updatedCounter`=`updatedCounter`+1
		WHERE `ID`=".(int)$_POST['edit']);
	msgBox('OK', 'Veiklos įrašas atnaujintas.');
}
if(isset($_GET['delete'])) {
	db_query("DELETE FROM `".DB_speech_other_activities."` WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `ID`=".(int)$_GET['delete']);
	msgBox('OK', 'Veiklos įrašas ištrintas.');
}
?>
<a href="?new#menu-form" class="no-print fast-action fast-action-add">Naujas veiklos įrašas</a> <?=ui_print()?>

<form method="get" style="padding-bottom:10px;" class="no-print">
	<div class="sel" style="margin-right: 7px;"><select name="date" required="required"><!--  title="Žymėto lankomumo laikotarpis" -->
	<?php
	if(isset($_GET['date'])) {
		list($year, $month) = explode('-', $_GET['date']);//, $day
		$year = (int) $year;
		$month = (int) $month;
	}
	$result = db_query("SELECT YEAR(`date`) `metai`, MONTH(`date`) `menuo` FROM `".DB_speech_other_activities."` WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} GROUP BY YEAR(`date`) DESC, MONTH(`date`) DESC");
	//Neisigilinau, bet kažkas įdomaus: http://stackoverflow.com/questions/15390303/how-to-group-by-desc-order
	//http://use-the-index-luke.com/3-minute-test/mysql?quizkey=d2a088303b955b839e29cb8711a85235
	while ($row = mysqli_fetch_assoc($result))
		echo "<option value=\"".$row['metai']."-".men($row['menuo'])."\"".(isset($_GET['date']) && $row['metai'] == $year && $row['menuo'] == $month ? ' selected="selected">'.$selectedMark : '>').year($row['metai'])."-".men($row['menuo'])."</option>";
	?>
	</select></div>
	<input type="submit" class="filter" value="Rodyti">
</form>

<?=(!isset($_GET['date']) ? " Rodoma 30 naujausių įrašų:" : '')?>
<table class="activities">
<tr>
	<th class="date-cell">Data</th>
	<th>Veiklos turinys</th>
	<th class="no-print">Sukurta</th>
	<th class="no-print">Pakeista</th>
	<th class="no-print">Veiksmai</th>
</tr>
<?php
$result = db_query("SELECT * FROM `".DB_speech_other_activities."` WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']}".(isset($_GET['date']) ? " AND `date`>='".db_fix($_GET['date'])."-01' AND `date`<='".date("Y-m-t", strtotime($_GET['date'].'-01'))."'" : '')." ORDER BY `date` DESC".(!isset($_GET['date']) ? " LIMIT 0,30" : ''));
while($row = mysqli_fetch_assoc($result)) {
	echo "<tr>
		<td>".filterText($row['date'])."</td>
		<td>".nl2br(filterText($row['activity']))."</td>
		<td class=\"no-print\">".filterText($row['created'])."</td>
		<td class=\"no-print\">".filterText(date_empty($row['updated']))."</td>
		<td class=\"no-print\"><a href=\"?edit=".$row['ID']."\">Keisti</a> <a href=\"?delete=".$row['ID']."\" onclick=\"return confirm('Ar tikrai norite ištrinti veiklos įrašą?')\">Trinti</a></td>
	</tr>";
}
echo '</table>';

if (isset($_GET['edit']) || isset($_GET['new'])) {
	if (isset($_GET['edit'])) {
		$result = db_query("SELECT * FROM `".DB_speech_other_activities."` WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `ID`=".(int)$_GET['edit']);
		$activity = mysqli_fetch_assoc($result);
	}
	?>
	<fieldset id="menu-form">
	<legend><?=(isset($_GET['edit']) ? 'Veiklos įrašo keitimas' : 'Naujas veiklos įrašas')?></legend>
	<form method="post" class="not-saved-reminder no-print">
		<p><label>Data <input class="datepicker" type="text" name="date" value="<?=(isset($activity) ? filterText($activity['date']) : '')?>"></label></p>
		<p><label>Veiklos turinys <textarea name="activity"><?=(isset($activity) ? filterText($activity['activity']) : '')?></textarea></label></p>
		<p><input type="hidden" <?=(!isset($_GET['edit']) ? 'name="save"' : 'name="edit" value="'.(int)$activity['ID'].'"')?>><input type="submit" value="Išsaugoti" class="submit"></p>
	</form>
	</fieldset>
<?php
}
?>
</div>
