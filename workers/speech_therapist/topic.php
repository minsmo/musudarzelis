<?php if(!defined('DARBUOT')) exit(); ?>
<h1>Darbo temų su ugdytiniais peržiūra <small style="font-weight: normal;">(temos rašomos meniu punkte „žym. lanko.“)</small></h1>
<div id="content">

<form method="get" style="padding-bottom:10px;" class="no-print">
<div class="sel" style="float: left; margin-right: 7px;"><select name="group_id">
	<?php
	$result = getDbQuerySpeechGroups();
	$speech_groups = [];
	while($row = mysqli_fetch_assoc($result)) {
		echo "<option value=\"".$row['ID']."\"".(isset($_GET['group_id']) && $row['ID'] == $_GET['group_id'] ? ' selected="selected">'.$selectedMark : '>')."".$row['title']."</option>";
		$speech_groups[$row['ID']] = $row['title'];
	}
	?>
	</select></div>
	<input type="submit" class="filter" name="summary" value="Rodyti">
</form>

<form method="get" style="padding-bottom:10px;" class="no-print">
	<div class="sel" style="margin-right: 7px;"><select name="date" required="required"><!--  title="Žymėto lankomumo laikotarpis" -->
	<?php
	if(isset($_GET['date'])) {
		list($year, $month) = explode('-', $_GET['date']);//, $day
		$year = (int) $year;
		$month = (int) $month;
	}
	$result = db_query("SELECT YEAR(`date`) `metai`, MONTH(`date`) `menuo` FROM `".DB_speech_attendance_topic."` GROUP BY YEAR(`date`) DESC, MONTH(`date`) DESC");
	//Neisigilinau, bet kažkas įdomaus: http://stackoverflow.com/questions/15390303/how-to-group-by-desc-order
	//http://use-the-index-luke.com/3-minute-test/mysql?quizkey=d2a088303b955b839e29cb8711a85235
	while ($row = mysqli_fetch_assoc($result))
		echo "<option value=\"".$row['metai']."-".men($row['menuo'])."\"".(isset($_GET['date']) && $row['metai'] == $year && $row['menuo'] == $month ? ' selected="selected">'.$selectedMark : '>').year($row['metai'])."-".men($row['menuo'])."</option>";
	?>
	</select></div>
	<input type="submit" class="filter" value="Rodyti">
</form>

<?php if(isset($_GET['group_id'])) { ?>
	<?=ui_print()?>
 
	<h2><?=$speech_groups[$_GET['group_id']]?></h2>
	<table>
	<tr>
		<th>Data</th>
		<th>Pratybų, specialiųjų pamokų tema (veiklos turinys)</th>
	</tr>
	<?php
	$result = db_query("SELECT * FROM `".DB_speech_attendance_topic."` WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `kid_group_id`=".(int)$_GET['group_id']." AND `topic`<>'' ORDER BY `date` DESC");
	while($row = mysqli_fetch_assoc($result)) {
		echo "<tr>
			<td>".$row['date']."</td>
			<td>".filterText($row['topic'])."</td>
		</tr>";
	}
	?>
	</table>
<?php } ?>

<?php if(isset($_GET['date'])) { ?>
	<?=ui_print()?>
 
	<table>
	<tr>
		<th>Data</th>
		<th>Pratybų, specialiųjų pamokų tema (veiklos turinys)</th>
	</tr>
	<?php
	$result = db_query("SELECT * FROM `".DB_speech_attendance_topic."` WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `topic`<>'' AND `date`>='".db_fix($_GET['date'])."-01' AND `date`<='".date("Y-m-t", strtotime($_GET['date'].'-01'))."' ORDER BY `kid_group_id`,`date` DESC");
	$group_id = '';
	while($row = mysqli_fetch_assoc($result)) {
		if($row['kid_group_id'] != $group_id) {
			echo "<tr><td colspan=\"2\">Vaikų grupė <strong>".$speech_groups[$row['kid_group_id']]."</strong></td></tr>";
			$group_id = $row['kid_group_id'];
		}
		echo "<tr>
			<td>".$row['date']."</td>
			<td>".filterText($row['topic'])."</td>
		</tr>";
	}
	?>
	</table>
<?php } ?>
</div>
