<?php if(!defined('DARBUOT') || !LOGOPEDAS) exit(); ?>
<h1><?php
$name = $l->getName(filterText($pareigybes[$_SESSION['SPEECH_THERAPIST_position_id']]), 'kil');
if($name == 'Specialusio Pedagogo')
	$name = 'Specialiojo pedagogo';
//TODO: modify for other things.
echo $name;
?> vaikų grupių lankomumo žymėjimas</h1>
<div id="content">
<?php
if(isset($_POST['date']) && !preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $_POST['date'])) logdie(msgBox('ERROR', "Blogas datos formatas: Jis turi būti „".date('Y-m-d')."“. ".back()));

if(isset($_GET['delete'])) {
	$mark_date = $_GET['delete'];
	//Kids: Valid, not archived
	/*$result = db_query("SELECT cr.`parent_kid_id` FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<='".db_fix($mark_date)."' GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
		JOIN `".DB_speech_groups_kids."` ON (`".DB_speech_groups_kids."`.`kid_id`=cr.parent_kid_id AND `".DB_speech_groups_kids."`.`group_id`=".(int)$_GET['group_id'].")
		WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0
		ORDER BY cr.`vardas`, cr.`pavarde`");
	$kids = array();
	while($row = mysqli_fetch_assoc($result))
		$kids[] = $row['parent_kid_id'];
	if(!empty($kids))
		db_query("DELETE FROM `".DB_speech_attendance."` WHERE `date`='".db_fix($mark_date)."' AND `kid_group_id`=".(int)$_GET['group_id']." AND `kid_id` IN (".implode(',', $kids).")");*/

	db_query("DELETE FROM `".DB_speech_attendance."` WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `date`='".db_fix($mark_date)."' AND `kid_group_id`=".(int)$_GET['group_id']);
	db_query("DELETE FROM `".DB_speech_attendance_topic."` WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `date`='".db_fix($mark_date)."' AND `kid_group_id`=".(int)$_GET['group_id']);
	msgBox('OK', "Lankomumas ištrintas!");
}

if(isset($_GET['deleteSingle'])) {
	db_query("DELETE FROM `".DB_speech_attendance."` WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `date`='".db_fix($_GET['deleteSingle'])."' AND `kid_group_id`=".(int)$_GET['group_id']." AND `kid_id`=".(int)$_GET['kid_id']);
	msgBox('OK', "Vaiko lankomumas ištrintas!");
}


if(isset($_POST['save']) && isset($_GET['zymeti'])) {
	$vaikas_i = 0;
	if(isset($_POST['kid_id'])) {
		foreach($_POST['kid_id'] as $i => $child_id) {
			$result = db_query("SELECT `date` FROM `".DB_speech_attendance."` WHERE `date`='".db_fix($_GET['zymeti'])."' AND `kid_group_id`=".(int)$_GET['group_id']." AND `kid_id`=".(int)$child_id);
			$attended = (isset($_POST['yra'][$i]) ? (int)$_POST['yra'][$i] : 0);
			if(mysqli_num_rows($result)) {
				db_query("UPDATE `".DB_speech_attendance."` SET
				`yra`=".$attended.",
				`updated`=CURRENT_TIMESTAMP, `updatedByUserId`=".USER_ID.", `updatedByEmployeeId`=".DARB_ID.",
				`updatedCounter`=`updatedCounter`+1
				WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `date`='".db_fix($_GET['zymeti'])."' AND `kid_group_id`=".(int)$_GET['group_id']." AND `kid_id`=".(int)$child_id."
				AND `yra`<>".$attended);
			} else {
				db_query("INSERT INTO `".DB_speech_attendance."` SET 
				`diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']},
				`kid_group_id`=".(int)$_GET['group_id'].", 
				`yra`=".$attended.", `date`='".db_fix($_GET['zymeti'])."',
				`kid_id`=".(int)$child_id.", `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
			}
			$vaikas_i++;
		}
		msgbox('OK', "Grupės (". $vaikas_i . " vaikų) lankomumas sėkmingai išsaugotas!");
	}
	
	
	$result = db_query("SELECT `date` FROM `".DB_speech_attendance_topic."` WHERE `date`='".db_fix($_GET['zymeti'])."' AND `kid_group_id`=".(int)$_GET['group_id']);
	if(mysqli_num_rows($result)) {
		db_query("UPDATE `".DB_speech_attendance_topic."` SET
		`topic`='".db_fix($_POST['topic'])."', 
		`notes`='".db_fix($_POST['notes'])."', 
		`updated`=CURRENT_TIMESTAMP, `updatedByUserId`=".USER_ID.", `updatedByEmployeeId`=".DARB_ID.",
		`updatedCounter`=`updatedCounter`+1
		WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `date`='".db_fix($_GET['zymeti'])."' AND `kid_group_id`=".(int)$_GET['group_id']." AND (`topic`<>'".db_fix($_POST['topic'])."' OR `notes`<>'".db_fix($_POST['notes'])."')");
	} else {
		$sql = "INSERT INTO `".DB_speech_attendance_topic."` SET 
			`diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']},
			`topic`='".db_fix($_POST['topic'])."', 
			`notes`='".db_fix($_POST['notes'])."', 
			`date`='".db_fix($_GET['zymeti'])."',
			`kid_group_id`=".(int)$_GET['group_id'].", `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'";
		db_query($sql);
	}
}
/*
?>
<form method="get" style="padding-bottom:10px;" class="no-print">
<div style="float: left; line-height: 33px; margin-right: 7px;">Visos: </div>
<div class="sel" style="float: left; margin-right: 7px;"><select name="group_id">
	<?php*/
	$result = db_query("SELECT * FROM `".DB_speech_schedule."` WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']}");
	$speech_groups_days = [];
	$speech_groups_exist = [];
	while($row = mysqli_fetch_assoc($result)) {
		$speech_groups_days[$row['day']][$row['speech_group_id']] = '';
		$speech_groups_exist[$row['speech_group_id']] = '';
	}
	$result = getDbQuerySpeechGroups();
	$speech_groups = [];
	$all_groups = [];
	$all_groups_ids = [];
	while($row = mysqli_fetch_assoc($result)) {
		$all_groups[] = $row;
		$all_groups_ids[] = $row['ID'];//Redundant
		$speech_groups[$row['ID']] = $row['title'];
	}
	/*
	foreach($all_groups as $row)
		echo "<option".(isset($speech_groups_days[$row['ID']][date('N')]) ? ' style="font-weight: bold;"' : '')." value=\"".$row['ID']."\"".(isset($_GET['group_id']) && $row['ID'] == $_GET['group_id'] ? ' selected="selected">'.$selectedMark : '>').$row['title']."</option>";
	?>
	</select></div>
	<input type="submit" class="filter" name="summary" value="Pasirinkti">
</form>
*/ ?>
<form method="get" style="padding-bottom:10px;" class="no-print">
	<div style="float: left; "><img src="/img/search.png" alt="Paieška" style="vertical-align: top;"><div class="sel" style="margin-right: 7px;"><select id="day">
	<?php
	echo '<option value="'.implode(',', $all_groups_ids).'">Visos dienos</option>';
	foreach($speech_groups_days as $day => $dump)
		echo '<option value="'.implode(',', array_keys($speech_groups_days[$day])).'"'.($day == date('N') ? ' selected="selected"' : '').">".$dienos_kada[$day].($day == date('N') ? ' (šiandien)' : '')."</option>";
	?></select></div></div><div style="float: left; width: 1px; height: 0; border-left: 20px solid #357d32; border-bottom: 16px solid transparent;border-top: 16px solid transparent; margin-right: 3px;"></div>
	<div class="sel" style="float: left; margin-right: 7px;"><select name="group_id" id="filtered-group-id"><?php
	foreach($all_groups as $row)
		//if(isset($speech_groups_exist[$row['ID']]))
			echo "<option value=\"".$row['ID']."\"".(isset($_GET['group_id']) && $row['ID'] == $_GET['group_id'] ? ' selected="selected">'.$selectedMark : '>').$row['title']."</option>";
	?></select></div>
	<input type="submit" class="filter" name="summary" value="Pasirinkti">
</form>

<?php
$URL = '?';
if(isset($_GET['group_id'])) {
	$URL = '?group_id='.(int)$_GET['group_id'].'&amp;';

/*<a href="<?=$URL.(isset($_GET['date']) ? 'date=' . filterText($_GET['date']).'&amp;' : '')?>new_date#attendance-day-form" id="new-day" class="no-print fast-action fast-action-add">Nauja lankomumo diena</a>*/



//if( isset($_GET['new_date']) ) {
if(!isset($_GET['zymeti'])) {
	?>
	<fieldset id="attendance-day-form">
	<legend>Nauja darbo diena lankomumo žymėjimui <!-- Naujos darbo dienos lankomumas --></legend>
	<form method="get" onsubmit="return validateDate(this)">
		<p>Šiandien - <?=date('Y-m-d')?> (<?=date('N').' '.mb_lcfirst($dienos[date('N')])?>)</p>
		<p><input type="hidden" name="group_id" value="<?=(int)$_GET['group_id']?>"><span class="abbr" title="Datos įprastas formatas <?=date('Y-m-d')?>"><!-- Data: --></span> <input class="datepicker abbr" type="text" name="zymeti" value="<?=date('Y-m-d')?>" data-date="<?=date('Y-m-d')?>" id="suggested-date" title="Žalia pariebinta data - pasiūlyta šiandiena. Kita nurodoma spustelėjus pele.">  <input style="margin:0" type="submit" value="Pereiti į lankomumo žymėjimą" class="submit"></p>
	</form>
	</fieldset>
	<?php
}
//else
{
	if(isset($_GET['zymeti'])) {
		$mark_date = $_GET['zymeti'];
		?>
		<script type="text/javascript" src="/libs/jquery.checkboxes.min.js"></script>
		<script type="text/javascript">
		$(function($) {
			$('#grupes-lankomumas').checkboxes('range', true);
		});
		</script>
		<fieldset id="attendance-form">
		<legend>Žurnalo redagavimas: <?=filterText($mark_date)?> „<?=$speech_groups[(int)$_GET['group_id']]?>“</legend>
		<form method="post" action="<?=$URL?>zymeti=<?=filterText($mark_date)?><?php /* remember filter parameters: */ if(isset($_GET['date'])) echo '&amp;date='.filterText($_GET['date']); ?>" class="not-saved-reminder">
			
			<?php //if($mark_date == '2014-10-03') echo 'Svarbu, kad rodytų tik tuos vaikus kurie nėra išregistruoti.';//Žymėkite kai žymėjimo sąrašo vaikai yra vis dar registruoti darželyje ?>
			<a href="#attendance-form" id="attendance-checkboxes" data-toggle="checkboxes" data-action="check">Pažymėti visus vaikus, kad yra</a>
			<!--<a href="#attendance-form" data-toggle="checkboxes" data-action="uncheck">Pažymėti, kad nė vieno nėra</a> -->
			<table id="grupes-lankomumas">
			<tr>
				<th title="Vaikai imami iš meniu punktų „Logo. grupės“ (grupės vaikų sąrašas) ir „Vaikai“ (vardas pavardė)"><span class="abbr">Vaikas</span></th><!--  ir „Logo. vaikai“ („Pratybas lanko nuo“, „Pratybas baigė lankyti“) -->
				<th>Yra<!-- Ar yra? --></th>
				<th>Pažymėta</th>
				<th>Pakeista</th>
				<th>Pratybas lanko nuo</th>
				<th>Pratybas baigė lankyti</th>
				<th>Trynimas</th>
			<?php
			$i = 0;
			//Kids: Valid, not archived
			$result = db_query("SELECT cr.*, `".DB_speech_kids."`.`attendance_start`, `".DB_speech_kids."`.`attendance_end` FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<='".db_fix($mark_date)."' GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
			JOIN `".DB_speech_groups_kids."` ON (`".DB_speech_groups_kids."`.`kid_id`=cr.parent_kid_id AND `".DB_speech_groups_kids."`.`group_id`=".(int)$_GET['group_id']." AND `".DB_speech_groups_kids."`.`diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']})
			JOIN `".DB_speech_kids."` ON `".DB_speech_groups_kids."`.`kid_id`=`".DB_speech_kids."`.`kid_id` AND `".DB_speech_kids."`.`diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']}
			WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0
			ORDER BY ".orderName('cr', $mark_date));
			//$sk = mysqli_num_rows($result);//TODO: Jeigu perdarysiu JS
			$kid_id = array();
			while($row = mysqli_fetch_assoc($result)) {
				$sel = db_query("SELECT * FROM `".DB_speech_attendance."` WHERE `kid_id`=".(int)$row['parent_kid_id']." AND `date`='".db_fix($mark_date)."' AND `kid_group_id`=".(int)$_GET['group_id']);
				$lankomumas = mysqli_fetch_assoc($sel);
				$kid_id[] = $row['parent_kid_id'];
				echo "\t<tr>
					<td>".filterText(getName($row['vardas'], $row['pavarde'], $mark_date))." <input type=\"hidden\" name=\"kid_id[".$i."]\" value=\"".$row['parent_kid_id']."\"></td>
					<td><input type=\"checkbox\" name=\"yra[".$i."]\" value=\"1\"".(mysqli_num_rows($sel) > 0 && $lankomumas['yra']  ? ' checked="checked"' : '')."></td>
					<td>".(mysqli_num_rows($sel) > 0 ? $lankomumas['created'] : '')."</td>
					<td>".(mysqli_num_rows($sel) > 0 && $lankomumas['updated'] != '0000-00-00 00:00:00' ? $lankomumas['updated'] : '')."</td>
					<td>".filterText($row['attendance_start'])."</td>
					<td>".filterText($row['attendance_end'])."</td>
					<td><a href=\"".$URL."deleteSingle=".$mark_date."&amp;kid_id=".$row['parent_kid_id']."&amp;zymeti=$mark_date\"  onclick=\"return confirm('Ar tikrai norite ištrinti?');\">Ištrinti</a></td>
					</tr>\n";
				$i++;
			}
			
			if(!empty($kid_id)) {
				$res = db_query("SELECT * FROM `".DB_speech_attendance."`
					JOIN `".DB_children."` ON `".DB_speech_attendance."`.`kid_id`=`parent_kid_id` AND `".DB_speech_attendance."`.`diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']}
					JOIN `".DB_speech_kids."` ON `".DB_speech_kids."`.`kid_id`=`".DB_speech_attendance."`.`kid_id` AND `".DB_speech_kids."`.`diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']}
					WHERE `date`='".db_fix($mark_date)."' AND `kid_group_id`=".(int)$_GET['group_id'].' AND `'.DB_speech_attendance.'`.`kid_id` NOT IN ('.implode(',', $kid_id).')
					GROUP BY `parent_kid_id`');
				if(mysqli_num_rows($res) > 0) {
					/*Išsaugotas vaikų lankomumas, kuriems buvo pakeisti duomenys:
					Išsaugoti likučiai (dėl duomenų meniu punkte „Vaikai“ pakeitimų t. y. įsigaliojimo datos, išregistravimo, grupės ar/ir kt.):
					*/
					echo '<tr><td colspan="7">Lankomumas vaikų šiuo metu nepriskirtų grupei:</tr>';
					while($row = mysqli_fetch_assoc($res)) {
						echo '<tr>
						<td>'.filterText($row['vardas'].' '.$row['pavarde']).'</td>
						<td><strong>'.($row['yra'] ? 'Buvo' : 'Nebuvo').'</strong></td>
						<td>'.$row['created'].'</td>
						<td>'.($row['updated'] != '0000-00-00 00:00:00' ? $row['updated'] : '').'</td>
						<td>'.filterText($row['attendance_start']).'</td>
						<td>'.filterText($row['attendance_end']).'</td>
						<td><a href="'.$URL.'deleteSingle='.$mark_date.'&amp;kid_id='.$row['parent_kid_id'].'&amp;zymeti='.$mark_date.'"  onclick="return confirm(\'Ar tikrai norite ištrinti?\');">Ištrinti</a></td>
						</tr>';
					}
					
				}
			}
			
			if($i == 0) {
				echo '<tr><td colspan="7">Nėra vaikų šioje grupėje.</td></tr>';// (šiai dienai oficialiai registruotų darželyje).<br>Vaikai imami iš meniu punkto „Vaikai“ pagal jų duomenų galiojimą.<br>Jei jau įvedėte vaikų, o jų nerodo, nors turėtų rodyti, tai pataisykite vaikuose jų duomenų galiojimą atsidarę to vaiko duomenis ir išsaugokite.
			}
			?>
			</table>
			<?php
			$prev_result = db_query("SELECT * FROM `".DB_speech_attendance_topic."` WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `kid_group_id`=".(int)$_GET['group_id']." AND `date`<'".db_fix($mark_date)."' ORDER BY `date` DESC LIMIT 1");
			$prev_attendance_topic = mysqli_fetch_assoc($prev_result);
			
			$result = db_query("SELECT * FROM `".DB_speech_attendance_topic."` WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `kid_group_id`=".(int)$_GET['group_id']." AND `date`='".db_fix($mark_date)."'");
			$attendance_topic = mysqli_fetch_assoc($result);
			?>
			<p><label><strong>Tema (veiklos turinys)</strong><?=(mysqli_num_rows($prev_result) > 0 ? ' (<strong>ankstesnė tema (veiklos turinys)</strong> „'.filterText($prev_attendance_topic['topic']).'“)' : '')?>:<br> <textarea name="topic" style="width: 250px;"><?=(mysqli_num_rows($result) > 0 ? filterText($attendance_topic['topic']) : '')?></textarea></label>
			<?php
			if(mysqli_num_rows($result) > 0)
				echo '<br>'.filterText('Sukurta '.$attendance_topic['created'].($attendance_topic['updated'] != '0000-00-00 00:00:00' ? '; Pakeista '.$attendance_topic['updated'] : '').'.');
			?></p>
			<?php
			$result = db_query("SELECT * FROM `".DB_speech_attendance_topic."` WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `kid_group_id`=".(int)$_GET['group_id']." AND `date`='".db_fix($mark_date)."'");
			$attendance_notes = mysqli_fetch_assoc($result);
			?>
			<p><label>Pastabos:<br> <textarea name="notes" style="width: 250px;"><?=(mysqli_num_rows($result) > 0 ? filterText($attendance_notes['notes']) : '')?></textarea></label></p>
			<?php if($i != 0) { ?>
			<p><input type="submit" value="Išsaugoti lankomumą" class="submit"> <input type="hidden" name="save"></p>
			<?php } ?>
		</form>
		</fieldset>
		<?php
	}





	$result = db_query("SELECT YEAR(`date`) AS `metai`, MONTH(`date`) AS `menuo` FROM `".DB_speech_attendance."` WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `kid_group_id`=".(int)$_GET['group_id']." GROUP BY YEAR(`date`) DESC, MONTH(`date`) DESC");
	if(mysqli_num_rows($result)) {
		?>
		<h2>Žymėtos lankomumo dienos</h2>
		<form method="get">
			<div class="sel" style="float: left; margin-right: 5px; line-height: 33px;">
			<input type="hidden" name="group_id" value="<?=(int)$_GET['group_id']?>">
			<select name="date">
				<?php
				if(isset($_POST['date'])) {
					list($year, $month) = explode('-', $_POST['date']);//, $day
					$year = (int) $year;
					$month = (int) $month;
				}
				if(isset($_GET['date'])) {
					list($year, $month) = explode('-', $_GET['date']);//, $day
					$year = (int) $year;
					$month = (int) $month;
				}
				while ($row = mysqli_fetch_assoc($result))
					echo "<option value=\"".$row['metai']."-".$row['menuo']."\"".((isset($_GET['date']) || isset($_POST['date'])) && $row['metai'] == $year && $row['menuo'] == $month ? ' selected="selected">'.$selectedMark : '>').$row['metai']."-".men($row['menuo'])."</option>";
				?>
			</select></div>
			<input type="submit" value="Filtruoti" class="filter">
		</form>
		<?php
	

		if(!isset($_GET['date']))
			echo '<em>Sąraše dabar rodoma 30 naujausių dienų.</em>';
		?>
		<table border="1">
		<tr>
			<th>Data</th>
			<th>Tema</th>
			<th>Veiksmai</th>
		</tr>
		<?php
		if(isset($_GET['date'])) {
			list($year, $month) = explode('-', $_GET['date']);
			$result = db_query("SELECT * FROM `".DB_speech_attendance."` WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND YEAR(`date`)=".(int)$year." AND MONTH(`date`)=".(int)$month." AND `kid_group_id`=".(int)$_GET['group_id']." GROUP BY `date` ORDER BY `date` DESC");
			$topic_result = db_query("SELECT * FROM `".DB_speech_attendance_topic."` WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND YEAR(`date`)=".(int)$year." AND MONTH(`date`)=".(int)$month." AND `kid_group_id`=".(int)$_GET['group_id']." GROUP BY `date` ORDER BY `date` DESC");
		} else {
			$result = db_query("SELECT * FROM `".DB_speech_attendance."` WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `kid_group_id`=".(int)$_GET['group_id']." GROUP BY `date` ORDER BY `date` DESC LIMIT 0,30");
			$topic_result = db_query("SELECT * FROM `".DB_speech_attendance_topic."` WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `kid_group_id`=".(int)$_GET['group_id']." GROUP BY `date` ORDER BY `date` DESC LIMIT 0,30");
		}
		if(mysqli_num_rows($result) > 0) {
			while ($row = mysqli_fetch_assoc($result)) {
				$topic = mysqli_fetch_assoc($topic_result);
				echo "\t<tr>
				<td>".$row['date']."</td>
				<td>".($row['date'] == $topic['date'] ? $topic['topic'] : '?'/*.$topic['ERROR_topic_not_found'] knsNezin_ kretBaubl_*/)."</td>
				<td><a href=\"".$URL."zymeti=".$row['date'].(isset($_GET['date']) ? "&amp;date=".filterText($_GET['date']) : '')."#attendance-form\">Žymėti lankomumą<!-- Lankomumas/žurnalas --></a>
					<a href=\"".$URL."delete=".$row['date']."\" onclick=\"return confirm('Ar tikrai norite ištrinti?')\">Trinti</a>
				
				</td>
			</tr>";
		
			}
		}
		?>
		</table>
		<?php
	}
}//without meaning

}
?>
</div>
<script type="text/javascript" src="/workers/speech_therapist/attendance.js?v3"></script>
