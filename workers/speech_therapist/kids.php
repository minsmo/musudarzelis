﻿<?php if(!defined('DARBUOT') || !(LOGOPEDAS || ADMIN)) exit(); ?>
<h1>Duomenys apie ugdytinius</h1>
<div id="content">
<?php
if(isset($_POST['add'])) {
	//`order`='".(int)$_POST['order']."',
	if(!mysqli_query($db_link, "INSERT INTO `".DB_speech_kids."` SET 
	`diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']},
--	
	`kid_id`='".(int)$_POST['kid_id']."',
	`recommendations`='".db_fix($_POST['recommendations'])."', `edu_program`='".db_fix($_POST['edu_program'])."', 
	`groups`='".db_fix($_POST['groups'])."', `attendance_start`='".db_fix($_POST['attendance_start'])."', 
	`attendance_end`='".db_fix($_POST['attendance_end'])."', `achievements`='".db_fix($_POST['achievements'])."',
	`createdByUserId`='".(int)USER_ID."', `createdByEmployeeId`='".(int)DARB_ID."'")) {
		logdie('Neteisinga užklausa: '. mysqli_error($db_link));
	} else {
		msgBox('OK', 'Sėkmingai išsaugota.');
	}
}
if (isset($_POST['update'])) {
	//`order`='".(int)$_POST['order']."', 
	if(!mysqli_query($db_link, "UPDATE `".DB_speech_kids."` SET
--	
	`kid_id`='".(int)$_POST['kid_id']."',
	`recommendations`='".db_fix($_POST['recommendations'])."', 
	`edu_program`='".db_fix($_POST['edu_program'])."', 
	`groups`='".db_fix($_POST['groups'])."', `attendance_start`='".db_fix($_POST['attendance_start'])."', 
	`attendance_end`='".db_fix($_POST['attendance_end'])."', 
	`achievements`='".db_fix($_POST['achievements'])."',
	`updated`=CURRENT_TIMESTAMP,
	`updatedByUserId`='".(int)USER_ID."', `updatedByEmployeeId`='".(int)DARB_ID."',
	`updatedCounter`=`updatedCounter`+1
	WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `ID`=".(int)$_POST['ID']))
		logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
	else
		msgBox('OK', "Informacija išsaugota!");
}
if(isset($_GET['delete'])) {
	db_query("DELETE FROM `".DB_speech_kids."` WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `ID`=".(int)$_GET['delete']);
	msgBox('OK', 'Informacija ištrinta!');
}

if (isset($_GET['archiveIt'])) {
	db_query("UPDATE `".DB_speech_kids."` SET `isArchived`=1 WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `ID`=".(int)$_GET['archiveIt']);
	msgBox('OK', "Vaikas sėkmingai suarchuotas.");
}
if (isset($_GET['unarchiveIt'])) {
	db_query("UPDATE `".DB_speech_kids."` SET `isArchived`=0 WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `ID`=".(int)$_GET['unarchiveIt']);
	msgBox('OK', "Vaikas sėkmingai atstatytas.");
}
?>

<a href="?#speech-kids-form" class="no-print fast-action fast-action-add">Naujo vaiko duomenų įvedimas</a>
<?php
echo (isset($_GET['archive']) ? ' <a href="?" class="no-print fast-action fast-action-archive">✖ Uždaryti archyvą</a>' : ' <a href="?archive" class="no-print fast-action fast-action-archive">Archyvas</a>');
echo ui_print();
?>

<table>
	<tr>
		<th>Eil. nr.</th>
		
		<th>Vardas ir pavardė<!-- pasirenka vaiką -->, adresas<!-- iš Vaikai -->, tel. nr. <!-- maybe text field --> <!-- „ugdytiniai surašomi pedagogo pasirinkta tvarka“ --></th>
		<th class="date-cell">Gimimo metai<!-- iš Vaikai --></th>
		<th>Grupė, klasė<!-- iš Vaikai --></th>
		
		<th>Pedagoginio psichologinio įvertinimo išvada / Specialistų rekomendacijos<!-- text field „ įrašoma Pedagoginės psichologinės tarnybos
	(jei ugdytinis buvo siųstas konsultuotis), švietimo įstaigos specialiojo ugdymo komisijos, Vaiko gerovės komisijos arba kitų
	specialistų (gydytojo, psichologo, spec. pedagogo ir kt.) išvados, rekomendacijos. “ --></th>
		<th>Ugdymo programa<!-- text field? Nurodant ugdymo programą galima naudoti
	trumpinius „BP“– bendrojo ugdymo programa, pritaikyta programa –„ad“ (adaptuota) ir kt.
	pvz., ikimokyklinė ugdymo
	--></th>
		<th>Grupės, pogrupio Nr.<!-- text field? grupės ar pogrupio, kuriam pedagogas priskyrė ugdytinį, numeris naudojant trumpinius
	grupė „G“, pogrupis „P“, dirbant individualiai „I“. Grupių ir pogrupių numeravimą nustato pedagogas. 

	Perkelto į kitą grupę ar pogrupį ugdytinio numeris atskiriamas nuo ankstesnio įrašo brūkšniu „–“. Ugdytinio, priskiriamo kelioms grupėms ar pogrupiams, numeriai atskiriami kableliais. Galioja paskutinis įrašas po brūkšnio. --></th>
		<th class="date-cell">Pratybas lanko nuo<!-- date field. tiksli data tų metų, kuriais ugdytiniui buvo pradėta teikti pagalba --></th>
		<th class="date-cell">Pratybas baigė lankyti<!-- date field. tiksli pratybų, pamokų lankymo pabaigos data --></th>
		<th>Mokinių pasiekimai <!-- text field. vertinami mokyklos nustatyta tvarka ir periodiškumu. Skiltyje įrašai daromi trimestrui/pusmečiui ir/ar mokslo
	metams baigiantis arba pratybų lankymo pabaigoje, įrašant „pp“– padarė pažangą, „np“– nepadarė pažangos ar kitu mokykloje priimtu vertinimo būdu. Logopedas metų pabaigoje įrašo „sutrikimas pašalintas“, „sutrikimas iš dalies pašalintas“, „sutrikimas nepašalintas“. Jei reikia, pedagogas šią skiltį gali padalyti į mažesnės skiltis. --></th>
		<th class="no-print">Veiksmai</th>
	</tr>
	<?php
	$result = db_query("SELECT cr.*, `".DB_speech_kids."`.*
	FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<=CURDATE() GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
	JOIN `".DB_speech_kids."` ON cr.`parent_kid_id`=`".DB_speech_kids."`.`kid_id`
	WHERE cr.`isDeleted`=0 AND `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `".DB_speech_kids."`.`isArchived`=".(isset($_GET['archive']) ? 1 : 0)."
	-- GROUP BY cr.parent_kid_id
	ORDER BY `".DB_speech_kids."`.`ID`
--	ORDER BY `order`");//isArchived
// AND cr.`archyvas`=0
	$i = 0;
	while($row = mysqli_fetch_assoc($result)) {
		$title = filterText((!empty($row['mam_tel']) ? 'Mamos tel. '.$row['mam_tel'].'.' : '').(!empty($row['tecio_tel']) ? ' Tėčio tel. '.$row['tecio_tel'] : ''));
		echo '<tr'.(isset($_GET['edit']) && $_GET['edit'] == $row['ID'] ? ' class="opened-row"' : '').'>
			<td>'.++$i/*(int)$row['order']*/.'</td>
			<td><span class="'.(!empty($title) ? 'abbr' : '').'" title="'.$title.'">'.filterText(getName($row['vardas'], $row['pavarde'])).($row['tevu_adresas'] != '' ? '<br>'.filterText($row['tevu_adresas']) : '').'</span></td>
			<td>'.filterText($row['gim_data']).'</td>
			<td>'.filterText(getAllGroups($row['grupes_id'])).'</td>
			<td>'.filterText($row['recommendations']).'</td>
			<td>'.filterText($row['edu_program']).'</td>
			<td>'.filterText($row['groups']).'</td>
			<td>'.filterText($row['attendance_start']).'</td>
			<td>'.filterText($row['attendance_end']).'</td>
			<td>'.filterText($row['achievements']).'</td>
			<td class="no-print"><a href="?edit='.$row['ID'].'">Keisti</a> '.($row['isArchived'] ? '<a href="?archive&amp;unarchiveIt='.$row['ID'].'">Grąžinti</a>' : '<a href="?archiveIt='.$row['ID'].'" onclick="return confirm(\'Ar tikrai norite archyvuoti (t. y. jau neturite užsiėmimų su šiuo vaiku)?\')">Archyvuoti</a>').' <a href="?delete='.$row['ID'].'" onclick="return confirm(\'Ar tikrai norite ištrinti?\')">Trinti</a></td>
			</tr>';
	}
	?>
</table>

	<?php
	if(isset($_GET['edit'])) {
		$result = db_query("SELECT * FROM `".DB_speech_kids."` WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `ID`=".(int)$_GET['edit']);
		if(mysqli_num_rows($result))
			$vaikas = mysqli_fetch_assoc($result);
		else
			unset($_GET['edit']);
	}
	?>
	<fieldset style="margin-top: 40px;" class="no-print">
		<legend><?=(isset($_GET['edit']) ? 'Redaguoti vaiko duomenis' : 'Nauji vaiko duomenys:')?></legend>
		<form method="post" id="speech-kids-form" class="not-saved-reminder">
			<!-- <p><label>Eilės numeris <input type="number" min="0" name="order" style="width: 50px;" value="<?=(isset($_GET['edit']) ? filterText($vaikas['order']) : '')?>"></label></p> -->
			
			<div style="margin-top: 8px;">
			<div class="sel" style="margin-top: 6px;"><select id="change-group-id">
				<option value="0" selected="selected">Visos grupės</option>
				<?php
				foreach($_SESSION['GROUPS'] as $id => $title)
					echo "<option value=\"".$id."\">".filterText($title)."</option>";
				?>
				</select></div>
				<button style="margin-top: 6px;" type="button" class="filter" id="filter-kids">Rodyti grupės vaikus</button>
			<label>Vaikas <div class="sel"><select name="kid_id" id="kids">
				<?php
				$result = db_query("SELECT cr.* FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<='".date('Y-m-d')."' GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from` WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 AND (cr.`parent_kid_id` NOT IN (SELECT `kid_id` FROM `".DB_speech_kids."` WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']})".(isset($vaikas['kid_id']) ? " OR cr.`parent_kid_id`=".$vaikas['kid_id'] : '').") ORDER BY ".orderName('cr'));//kid_id
				while($row = mysqli_fetch_assoc($result))
					echo "<option value=\"".$row['parent_kid_id']."\" data-group=\"".$row['grupes_id']."\" ".(isset($_POST['kid_id']) && $_POST['kid_id'] == $row['parent_kid_id'] || isset($_GET['edit']) && $vaikas['kid_id'] == $row['parent_kid_id'] ? ' selected="selected"' : '').">".filterText(getName($row['vardas'], $row['pavarde']))."</option>";
				?>
			</select></div></label>
				
				<script>
				"use strict";
				$(function() {
					var data = [];
					$('#kids option').each(function() {
						data.push({
							value: $(this).val(),
							text: $(this).text(),
							group: $(this).data('group')
						});
					});
					$('#filter-kids').click(function() {
						/*$('#kids').find('option').hide(); $('#kids').find('[data-group=\'' + $('#change-group-id').val() + '\']').show();*/
						var select = $('#kids').empty();
						for (var i = 0; i < data.length; ++i) {
							if($('#change-group-id').val() == 0 || $('#change-group-id').val() == data[i].group) {
								var o = $('<option>', { value: data[i].value })
									.text(data[i].text).data('group', data[i].group);
									//.prop('selected', i == 0);
								o.appendTo(select);
							}
						}
				
						//if($('#change-group-id').val() == 0) $('#kids option').show();
					});
					//select option list filter jquery
				});
				</script>
				
				</div>
			<p><label><span title="Įrašoma Pedagoginės psichologinės tarnybos (jei ugdytinis buvo siųstas konsultuotis), švietimo įstaigos specialiojo ugdymo komisijos, Vaiko gerovės komisijos arba kitų specialistų (gydytojo, psichologo, spec. pedagogo ir kt.) išvados, rekomendacijos" class="abbr">Pedagoginio psichologinio įvertinimo išvada/Specialistų rekomendacijos</span>: <br><textarea name="recommendations" style="width: 400px; height: 80px;"><?=(isset($_GET['edit']) ? filterText($vaikas['recommendations']) : '')?></textarea></label></p>
			<p><label><span title="Nurodant ugdymo programą galima naudoti trumpinius „BP“– bendrojo ugdymo programa, pritaikyta programa –„ad“ (adaptuota) ir kt." class="abbr">Ugdymo programa</span> <input type="text" name="edu_program" style="width: 250px;" value="<?=(isset($_GET['edit']) ? filterText($vaikas['edu_program']) : '')?>"></label></p>
			<p><label><span title="Grupės ar pogrupio, kuriam pedagogas priskyrė ugdytinį, numeris sudaromas naudojant trumpinius
grupė „G“, pogrupis „P“, dirbant individualiai „I“. Grupių ir pogrupių numeravimą nustato pedagogas. 

Perkelto į kitą grupę ar pogrupį ugdytinio numeris atskiriamas nuo ankstesnio įrašo brūkšniu „–“. Ugdytinio, priskiriamo kelioms grupėms ar pogrupiams, numeriai atskiriami kableliais. Galioja paskutinis įrašas po brūkšnio." class="abbr">Grupės, pogrupio Nr.</span>: <br><textarea name="groups" style="width: 150px; height: 80px;"><?=(isset($_GET['edit']) ? filterText($vaikas['groups']) : '')?></textarea></label></p>
			<p><label><span title="Tiksli data tų metų, kuriais ugdytiniui buvo pradėta teikti pagalba" class="abbr">Pratybas lanko nuo</span>: <br><textarea name="attendance_start" style="width: 150px; height: 80px;"><?=(isset($_GET['edit']) ? filterText($vaikas['attendance_start']) : '')?></textarea></label></p>
			<p><label><span title="Tiksli pratybų, pamokų lankymo pabaigos data" class="abbr">Baigė lankyti pratybas</span>: <br><textarea name="attendance_end" style="width: 150px; height: 80px;"><?=(isset($_GET['edit']) ? filterText($vaikas['attendance_end']) : '')?></textarea></label></p>
			<p><label><span title="Vertinami mokyklos nustatyta tvarka ir periodiškumu. Skiltyje įrašai daromi trimestrui/pusmečiui ir/ar mokslo
metams baigiantis arba pratybų lankymo pabaigoje, įrašant „pp“– padarė pažangą, „np“– nepadarė pažangos ar kitu mokykloje priimtu vertinimo būdu. Logopedas metų pabaigoje įrašo „sutrikimas pašalintas“, „sutrikimas iš dalies pašalintas“, „sutrikimas nepašalintas“. Jei reikia, pedagogas šią skiltį gali padalyti į mažesnės skiltis." class="abbr">Mokinių pasiekimai</span>:<br><textarea name="achievements" style="width: 400px; height: 80px;"><?=(isset($_GET['edit']) ? filterText($vaikas['achievements']) : '')?></textarea></label></p>
			<?=(isset($_GET['edit']) ? '<input type="hidden" name="ID" value="'.$vaikas['ID'].'">' : '')?>
			<p><input type="submit" name="<?=(isset($_GET['edit']) ? 'update' : 'add')?>" value="Išsaugoti" class="submit"></p>
		</form>
	</fieldset>
</div>
