$(function() {
	/*var EmployeeDiv = $('#employees');
	$('#addEmployee').on('click', function(event) {
		event.preventDefault();
		var id = $("#employeeId").val();
		if(!$("#employee"+id).length) {
			$('<p><label>'+$("#employeeId option:selected").text()+'<input type="hidden" name="priskyrimas[]" value="'+id+'"></label> <a href="#" class="remEmployee" id="employee'+id+'">- Trinti šį laukelį</a></p>').appendTo(EmployeeDiv);// data-id="'+id+'"
		} else
			alert("Šis vaikas jau pridėtas prie redaguojamos grupės.");
	});

	$(document).on('click', '.remEmployee', function (event) {
		event.preventDefault();//return false;
		$(this).parents('p').remove();
	});
	*/
	/*Code below is used only when button "assign kid" is in use
	$('select').change(function(event) {
		event.preventDefault();
		if($(this).val() != 0)
			$(this).parent().next().show();
		else
			$(this).parent().next().hide();
	});*/
	
	
	var addPerson = $('#persons');
	$('#addPerson').click(function(event) {
		event.preventDefault();
		var id = $("#personId").val();
		if(id) {
			if(!$("#person"+id).length) {
				$('<p><label>'+$("#personId option:selected").text()+' <input type="hidden" name="priskyrimas[]" value="'+id+'"></label> <a href="#" class="remPerson" id="person'+id+'">- Ištrinti susiejimą</a></p>').appendTo(addPerson);
				DONE("Vaikas „"+$("#personId option:selected").text()+"“ buvo pridėtas į sarašą!");
			} else
				alert("Šis vaikas jau pridėtas.");
		}
		
	});
	$('#personId').change(function() {
		$('#addPerson').click();
		if($(this).val())
			$(this).find('option[value='+$(this).val()+']').hide();
		$('#personId').val("");
	});
	$(document).on('click', '.remPerson', function(event) {
		event.preventDefault();
		var id = $(this).parent().find('input').val();
		$(this).parent().parent().parent().find('option[value='+id+']').show();
		$(this).parents('p').remove();
	});
	$("#persons > p").each(function() {
		$(this).parent().parent().find('option[value='+$(this).find('input').val()+']').hide();
	});
});
