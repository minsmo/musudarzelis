﻿<?php if(!defined('ADMIN') | !ADMIN) exit(); ?>
<h1>Logopedų, specialiųjų pedagogų, tiflopedagogų, surdopedagogų dienynai</h1>
<div id="content">
<?php
if(isset($_POST['save'])/* && in_array($_POST['position_id'], $allowed_positions_speech_diareis)*/) {
	db_query("INSERT INTO `".DB_speech_diaries."` SET 
		`position_id`='".(int)($_POST['position_id'])."',
		`employee_id`='".(int)($_POST['employee_id'])."',
		`notes`='".db_fix($_POST['notes'])."',
		`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
	msgBox('OK', 'Dienyno įrašas išsaugotas.');
}
if(isset($_POST['edit'])/* && in_array($_POST['position_id'], $allowed_positions_speech_diareis)*/) {
	db_query("UPDATE `".DB_speech_diaries."` SET 
		`position_id`='".(int)($_POST['position_id'])."',
		`employee_id`='".(int)($_POST['employee_id'])."',
		`notes`='".db_fix($_POST['notes'])."',
		`updated`=CURRENT_TIMESTAMP, `updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."',
		`updatedCounter`=`updatedCounter`+1
		WHERE `ID`=".(int)$_POST['edit']);
	msgBox('OK', 'Dienyno įrašas išsaugotas.');
}
if(isset($_GET['delete'])) {
	db_query("DELETE FROM `".DB_speech_diaries."` WHERE `ID`=".(int)$_GET['delete']);
	msgBox('OK', 'Dienyno įrašas ištrintas.');
}
?>
<a href="?new#menu-form" class="no-print fast-action fast-action-add">Naujas dienynas</a>
<table class="diaries">
<tr>
	<th title="Dienyno ID"><span class="abbr">ID</span></th>
	<th>Pareigos</th>
	<th>Darbuotojas</th>
	<th>Pastabos</th>
	<th class="no-print">Veiksmai</th>
	<?php if(KESV) { ?>
	<th class="no-print">Sukurtas</th>
	<?php } ?>
</tr>
<?php
$result = db_query("SELECT * FROM `".DB_speech_diaries."` ORDER BY `ID`");
while($row = mysqli_fetch_assoc($result)) {
	echo "<tr>
		<td>".$row['ID']."</td>
		<td>".$pareigybes[$row['position_id']]."</td>
		<td>".getAllEmployees($row['employee_id'])."</td>
		<td>".filterText($row['notes'])."</td>
		<td class=\"no-print\"><a href=\"?edit=".$row['ID']."\">Keisti</a> <a href=\"?delete=".$row['ID']."\" onclick=\"return confirm('Ar tikrai norite ištrinti įrašą?')\">Trinti</a></td>";
	if(KESV) {
		echo '<td class="no-print">'.$row['created'].'</td>';
	} 
	echo "
	</tr>";
}
echo '</table>';

if (isset($_GET['edit']) || isset($_GET['new'])) {
	if (isset($_GET['edit'])) {
		$result = db_query("SELECT * FROM `".DB_speech_diaries."` WHERE `ID`=".(int)$_GET['edit']);
		$edit = mysqli_fetch_assoc($result);
	}
	?>
	<fieldset id="menu-form">
	<legend><?=(isset($_GET['edit']) ? 'Dienyno tobulinimas' : 'Naujas dienynas')?></legend>
	<form method="post" class="not-saved-reminder no-print">
		<div><label for="pareigybes">Pareigos </label><div class="sel"><select name="position_id">
			<option value="" hidden selected disabled>Pasirinkite pareigas</option>
			<?php
			foreach( $pareigybes as $key => $value)
				//if(in_array($key, $allowed_positions_speech_diareis))
					echo "<option value=\"$key\"".(isset($_GET['edit']) && $edit['position_id'] == $key ? ' selected="selected"' : '').">$value</option>";
			?>
			</select></div></div>
		<div><label>Darbuotojas 
			<div class="sel"><select name="employee_id">
			<option value="" hidden selected disabled>Pasirinkite darbuotoją</option>
			<!--<option value="0">Pasirinkite darbuotoja</option> -->
			<?php
			$darbuotojai = array();
			$result = getValidEmployeesOrdered_db_query();
			while ($row = mysqli_fetch_assoc($result))
				$darbuotojai[$row['ID']] = filterText($row['vardas'].'  '.$row['pavarde']);
			foreach($darbuotojai as $ID => $name)
				echo "<option value=\"".$ID."\"".(isset($_GET['edit']) && $edit['employee_id'] == $ID ? ' selected="selected"' : '').">".$name."</option>";
			?>
			</select></div></div>
		<div><label>Pastabos <textarea name="notes"><?=(isset($edit) ? filterText($edit['notes']) : '')?></textarea></label></div>
		<p><input type="hidden" <?=(!isset($_GET['edit']) ? 'name="save"' : 'name="edit" value="'.(int)$edit['ID'].'"')?>><input type="submit" value="Išsaugoti" class="submit"></p>
	</form>
	</fieldset>
<?php
}
?>
</div>
