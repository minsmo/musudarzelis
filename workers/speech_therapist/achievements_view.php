﻿<?php if(!defined('DARBUOT')) exit(); ?>
<h1>Vaiko pasiekimai pas logopedą, specialųjį pedagogą, tiflopedagogą, surdopedagogą</h1>
<div id="content">
<table>
	<tr>
		<th>Vaiko vardas pavardė</th>
		<?php /*
		<th class="date-cell">Pratybas lanko nuo</th>
		<th class="date-cell">Pratybas baigė lankyti</th> */ ?>
		<th>Pasiekimai</th>
	</tr>
	<?php
	$result = db_query("SELECT cr.*, `".DB_speech_kids."`.*
	FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<=CURDATE() GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
	JOIN `".DB_speech_kids."` ON cr.`parent_kid_id`=`".DB_speech_kids."`.`kid_id`
	WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 AND cr.`grupes_id`=".GROUP_ID."
	-- GROUP BY cr.parent_kid_id
	ORDER BY `order`");//cr.`vardas` ASC, cr.`pavarde` ASC
	
	while($row = mysqli_fetch_assoc($result)) {
		echo '<tr>
			<td>'.filterText(getName($row['vardas'], $row['pavarde'])).'</td>
			
			<td>'.filterText($row['achievements']).'</td>
			</tr>';
			//<td>'.filterText($row['attendance_start']).'</td>
			//<td>'.filterText($row['attendance_end']).'</td>
	}
	?>
</table>
</div>
