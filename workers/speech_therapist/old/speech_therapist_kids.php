<?php if(!defined('DARBUOT')) exit(); ?>
<h1>Logopedui</h1>
<div id="content">
<h2>DUOMENYS APIE UGDYTINIUS</h2>
<table>
<tr>
	<th>Eil. nr.</th>
	<th>Vardas ir pavardė<!-- pasirenka vaiką -->, adresas<!-- iš Vaikai -->, telefono numeris <!-- maybe text field --> <!-- „ugdytiniai surašomi pedagogo pasirinkta tvarka“ --></th>
	<th>Gimimo metai<!-- iš Vaikai --></th>
	<th>Grupė, klasė<!-- iš Vaikai --></th>
	<th>Pedagoginio psichologinio įvertinimo išvada/Specialistų rekomendacijos<!-- text field „ įrašoma Pedagoginės psichologinės tarnybos
(jei ugdytinis buvo siųstas konsultuotis), švietimo įstaigos specialiojo ugdymo komisijos, Vaiko gerovės komisijos arba kitų
specialistų (gydytojo, psichologo, spec. pedagogo ir kt.) išvados, rekomendacijos. “ --></th>
	<th>|Ugdymo programa<!-- text field? Nurodant ugdymo programą galima naudoti
trumpinius „BP“– bendrojo ugdymo programa, pritaikyta programa –„ad“ (adaptuota) ir kt.
pvz., ikimokyklinė ugdymo
--></th>
	<th>Grupės, pogrupio Nr.<!-- text field? grupės ar pogrupio, kuriam pedagogas priskyrė ugdytinį, numeris naudojant trumpinius
grupė „G“, pogrupis „P“, dirbant individualiai „I“. Grupių ir pogrupių numeravimą nustato pedagogas. 

Perkelto į kitą grupę ar pogrupį ugdytinio numeris atskiriamas nuo ankstesnio įrašo brūkšniu „–“. Ugdytinio, priskiriamo kelioms grupėms ar pogrupiams, numeriai atskiriami kableliais. Galioja paskutinis įrašas po brūkšnio. --></th>
	<th>Pratybas lanko nuo<!-- date field. tiksli data tų metų, kuriais ugdytiniui buvo pradėta teikti pagalba --></th>
	<th>Baigė lankyti pratybas<!-- date field. tiksli pratybų, pamokų lankymo pabaigos data --></th>
	<th>Mokinių pasiekimai <!-- text field. vertinami mokyklos nustatyta tvarka ir periodiškumu. Skiltyje įrašai daromi trimestrui/pusmečiui ir/ar mokslo
metams baigiantis arba pratybų lankymo pabaigoje, įrašant „pp“– padarė pažangą, „np“– nepadarė pažangos ar kitu mokykloje priimtu vertinimo būdu. Logopedas metų pabaigoje įrašo „sutrikimas pašalintas“, „sutrikimas iš dalies pašalintas“, „sutrikimas nepašalintas“. Jei reikia, pedagogas šią skiltį gali padalyti į mažesnės skiltis. --></th>
</tr>
</table>
<!--


6. Kiekvieno ugdytinio ar grupės (pogrupio) ugdytinių lankomumui ir pratybų, specialiųjų pamokų turiniui „Individualus, grupinis ar pogrupinis darbas su ugdytiniais“ žymėti skiriami atskiri puslapiai. Mokinio neatvykimas žymimas raide „n“.
7. „Lankomumo“ puslapio viršuje įrašomas grupės (pogrupio) numeris, o jei darbas individualus, – ugdytinio vardas ir pavardė.
Individualių pratybų lankomumui žymėti skiriama viena eilutė, joje nurodomas mokinio vardas, pavardė, žymimas jo lankomumas.
Kitos šios lentelės eilutės (žemiau esančios) paliekamos tuščios.
(„Lankomumas“) 8. Eilutėse „Pastabos“ įrašomos žinios apie ugdytiniams teikiamą pagalbą (perkėlimus iš grupių, pogrupių ar individualaus darbo) ar kita pedagogui svarbi informacija, susijusi su ugdytiniais, pratybų organizavimu ir pan.

INDIVIDUALUS, GRUPINIS AR POGRUPINIS DARBAS SU UGDYTINIAIS 9. Skiltyje „Pratybų tema“ įrašoma konkreti tema. Vienoms pratyboms skiriama viena eilutė pildoma po kiekvienų pratybų.
Pedagogas pasirašo ties kiekvienu įrašu. Kartojantis temoms žymima „________„_________“. Šioje skiltyje spec. pedagogas gali įrašyti ir namų darbams skirtas užduotis.
KITA VEIKLA 10. Skyriuje „Kita veikla“ rašoma apie pedagogo veiklą, tiesiogiai nesusijusią su pratybomis, bet įeinančią į jo pareigas (pvz., pedagogų, tėvų konsultavimas, dalyvavimas mokyklos komisijų posėdžiuose, veiklos planavimas, ataskaitų rengimas ir pan.). Ties kiekvienu įrašu pedagogas pasirašo.

11. Taisomas klaidingas įrašas perbraukiamas vienu brūkšniu. Išnašoje nurodoma mokinio vardas ir pavardė, teisingas įrašas, taisymo data, taisęs asmuo parašo savo vardą ir pavardę, pasirašo.
12. Direktoriaus paskirtas pavaduotojas ugdymui, skyriaus vedėjas per mokslo metus atlieka Dienyno pildymo priežiūrą ir metams pasibaigus sutvarkytą Dienyną perduoda archyvui. 

Logopedui info apie vaikus
Lankomumas
-->
<h2>Lankomumas</h2>
(vardas ir pavardė; grupės ar pogrupio Nr.) input type="text"
<table>
<tr>
	<th rowspan="2">Eil. nr.</th>
	<th rowspan="2">Vardas ir pavardė</th>
	<th>Mėnuo</th>
	<th colspan="100"></th>
</tr>
<tr>
	<th>Klasė, grupė \ Diena</th>
	<th></th>
	<th></th>
	<th></th>
</tr>
</table>
PASTABOS: textarea
<h2>INDIVIDUALUS, GRUPINIS AR POGRUPINIS DARBAS SU UGDYTINIAIS</h2>
<table>
<tr>
	<th>Data</th>
	<th>Pratybų, specialiųjų pamokų tema</th>
	<th>Parašas</th>
</tr>
</table>
<h2>KITA VEIKLA</h2>
<table>
<tr>
	<th>Data</th>
	<th>Veiklos turinys</th>
	<th>Parašas</th>
</tr>
</table>

<h2>PASTABOS IR SIŪLYMAI DĖL DIENYNO TVARKYMO</h2>
<table>
<tr>
	<th>Data</th>
	<th>Pastabos ir siūlymai</th>
	<th>Vardas, pavardė ir pareigos</th>
	<th>Parašas</th>
</tr>
</table>

</div>
