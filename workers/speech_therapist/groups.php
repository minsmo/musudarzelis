<?php if(!defined('DARBUOT') || !(LOGOPEDAS || ADMIN)) exit(); ?>
<h1><?php
$name = $l->getName(filterText($pareigybes[$_SESSION['SPEECH_THERAPIST_position_id']]), 'kil');
if($name == 'Specialusio Pedagogo')
	$name = 'Specialiojo pedagogo';
//TODO: modify for other things.
echo $name;
?> vaikų grupės</h1>
<div id="content">
<?php
// What if there are the same kid in more than one group???? Show notification?
//When kid is delete from kids list (Logopedo vaikai) then warning is produced 14-12-22 14:19:17t_ NOTICE (Undefined offset: 19) C:\wamp\www\musudarzelis\workers\speech_therapist\groups.php:169 127.0.0.1 GET /speech_groups?priskirti_grupei=1 en-US,en;q=0.5 Mozilla/5.0 (Windows NT 6.3; WOW64; rv:34.0) Gecko/20100101 Firefox/34.0 http://martynas.musudarzelis.lt/speech_groups l@t.lt USER_ID:15 speech_therapist/groups.php:169 workers/index.php:418 musudarzelis/index.php:6

if(isset($_POST['save']) && isset($_POST['title'])) {
	db_query("INSERT INTO `".DB_speech_groups."` SET `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']}, `title`='".db_fix($_POST['title'])."', `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
	msgBox('OK', "Grupės pavadinimas išsaugotas!");
}
if(isset($_POST['update']) && isset($_POST['title'])) {
	db_query("UPDATE `".DB_speech_groups."` SET `title`='".db_fix($_POST['title'])."',
		`updated`=CURRENT_TIMESTAMP, `updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."',
		`updatedCounter`=`updatedCounter`+1
		WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `ID`=".(int)$_POST['update']);
	msgBox('OK', "Grupės pavadinimas pakeistas!");
}

//if(isset($_GET['delete'])) {
	//db_query("DELETE FROM `".DB_speech_groups."` WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `ID`=".(int)$_GET['delete']);
	//db_query("DELETE FROM `".DB_speech_groups_kids."` WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `group_id`=".(int)$_GET['delete']);
	//msgBox('OK', "Grupė ištrinta!");
//}
if(isset($_GET['archiveID'])) {
	db_query("UPDATE `".DB_speech_groups."` SET `isArchived`=1 WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `ID`=".(int)$_GET['archiveID']);
	msgBox('OK', "Grupė archyvuota!");
}

if(isset($_POST['assign_save'])) {//isset($_POST['priskyrimas'])
	if(empty($_POST['priskyrimas'])) {
		db_query("DELETE FROM `".DB_speech_groups_kids."` WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `group_id`=".(int)$_POST['assign_save']);
	} else {
		foreach($_POST['priskyrimas'] as &$val)
			$val = (int)$val;
		unset($val);// break/destroy the reference with the last element
		//http://php.net/manual/en/control-structures.foreach.php
	
		db_query("DELETE FROM `".DB_speech_groups_kids."` WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `group_id`=".(int)$_POST['assign_save']." AND `kid_id` NOT IN (".implode(',', $_POST['priskyrimas']).")");
		foreach($_POST['priskyrimas'] as $val) {
			$result = db_query("SELECT * FROM `".DB_speech_groups_kids."` WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `group_id`='".(int)$_POST['assign_save']."' AND `kid_id`=".(int)$val);
			if(mysqli_num_rows($result) >= 1) {
				db_query("UPDATE `".DB_speech_groups_kids."` SET `updated`=CURRENT_TIMESTAMP, `updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."', `updatedCounter`=`updatedCounter`+1
					WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `group_id`=".(int)$_POST['assign_save']." AND `kid_id`=".(int)$val);
			} else {
				db_query("INSERT INTO `".DB_speech_groups_kids."` SET `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']}, `group_id`=".(int)$_POST['assign_save'].", `kid_id`=".(int)$val.", `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
			}
		}
	}
	msgBox('OK', "Vaikai priskirti grupei išsaugoti.");
}

function URL($new_parameters = [], $unset_parameters = []) {
	global $_GET;
	$__GET = $_GET;//Keep what is saved in URL
	unset($__GET['q'], $__GET['delete'], $__GET['new']);//1. Clean garbage
	$__GET = array_merge($__GET, $new_parameters);//2. Add new
	foreach($unset_parameters as $val)//3. Remove unused
		unset($__GET[$val]);
	return '?'.http_build_query($__GET);
}
?>

<div class="no-print"><a href="#" id="groupTbl"<?=(isset($_GET['edit']) ? '' : ' class="active"')?>>Grupių lentelė</a> | <a href="#newGroup-form" id="newGroup"<?=(isset($_GET['edit']) ? ' class="active"' : '')?>><?=(isset($_GET['edit']) ? "Grupės pavadinimo keitimas" : "Nauja grupė")?></a>
<?=(isset($_GET['archive']) && $_GET['archive'] == 1 ? ' <a href="'.URL(['archive' => 0]).'" class="no-print fast-action fast-action-archive">✖ Uždaryti archyvą</a>' : ' <a href="'.URL(['archive' => 1]).'" class="no-print fast-action fast-action-archive">Archyvas</a>')?>
<?=ui_print()?></div>
<script type="text/javascript">
$(function(){
	function newGroup() {
		console.log('newGroup');
		$('#newGroup').addClass('active');
		$('#groupTbl').removeClass('active');
		$('#newGroup-form').show();
		$('table').hide();
		$('#assignEmployees').hide();
	}
	$('#newGroup').click(newGroup);
	function groupTbl() {
		$('#newGroup').removeClass('active');
		$('#groupTbl').addClass('active');
		$('#newGroup-form').hide();
		$('table').show();
	}

	$('#groupTbl').click(groupTbl);

	<?=(isset($_GET['edit']) ? 'newGroup();' : 'groupTbl();')?>
	$(window).on('hashchange', function() {
		if(location.hash.slice(1) == 'assignEmployees')
			$('#assignEmployees').show();
	});
});
</script>

<table>
<tr>
	<th>Grupės pavadinimas</th>
	<th class="no-print">Grupės valdymas</th>
	<th>Grupės vaikai</th>
	<th class="no-print">Vaikų priskyrimas grupei</th>
</tr>
<?php
$result = db_query("SELECT * FROM `".DB_speech_groups."` WHERE `isArchived`=".(isset($_GET['archive']) ? (int)$_GET['archive'] : 0 )." AND `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} ORDER BY `title`");
if(mysqli_num_rows($result) > 0) {

	$rows = [];
	while ($row = mysqli_fetch_assoc($result))
		$rows[filterText($row['title']).$row['ID']] = $row;
	//array_multisort($sort_by_field, SORT_ASC, SORT_NATURAL | SORT_FLAG_CASE, $rows);
	ksort($rows, SORT_NATURAL | SORT_FLAG_CASE);//SORT_LOCALE_STRING
	foreach($rows as $row) {
	//while ($row = mysqli_fetch_assoc($result)) {
		if(isset($_GET['priskirti_grupei']) && $_GET['priskirti_grupei'] == $row['ID'])
			$group_name = filterText($row['title']);
		echo "		<tr>
		<td>".filterText($row['title'])."</td>
		<td class=\"no-print\">
			<a href=\"".URL(['edit' => $row['ID']])."\">Keisti pavadinimą</a>
			<!-- <a href=\"".URL(['delete' => $row['ID']])."\" onclick=\"return confirm('Ar tikrai norite ištrinti grupę? Bus ištrinta grupės pavadinimas, jos vaikai, nebesimatys lankomumo ir temų, nes nebus galima pasirinkti grupės.')\">Trinti</a> -->
			<a href=\"".URL(['archiveID' => $row['ID']])."\" onclick=\"return confirm('Ar tikrai norite archyvuoti grupę?')\">Archyvuoti</a> 
			</td>
		<td>";
		
		//TODO: finish (get valid kid data)
		$result_d = db_query("SELECT `".DB_children."`.* 
		FROM `".DB_speech_groups_kids."` JOIN `".DB_children."`  ON `".DB_speech_groups_kids."`.`kid_id`=`".DB_children."`.`parent_kid_id` 
		WHERE `group_id`=".(int)$row['ID'].' AND `diary_id`='.$_SESSION['SPEECH_THERAPIST_DIARY'].'
		GROUP BY `kid_id` -- fix
		ORDER BY `vardas`, `pavarde`');//TODO: in the future maybe someone need to add the same kid twice then will be a need for GROUP BY `kid_id`
		
		if(mysqli_num_rows($result_d) > 0) {
			$tmp = [];
			while ($i = mysqli_fetch_assoc($result_d))
				$tmp[] = filterText(getName($i['vardas'], $i['pavarde']));
			echo implode(",<br>", $tmp);
		} else {
			//echo 'Dėmesio! Norint, kad auklėtojos ir muzikos pedagogės galėtų dirbti<!-- prisijungti ir prisijungusios dirbti --> su šia grupe privaloma priskirti norimus darbuotojus prie šios grupės. Tai padaryti pirmiausia spauskite ant nuorodos, esančios dešiniau, pavadinimu „Keisti darbuotojų priskyrimus“.';
		}
		echo "</td>
		<td class=\"no-print\"><a href=\"".URL(['priskirti_grupei' => $row['ID']])."#assignEmployees\">Keisti vaikų priskyrimus</a></td>
	</tr>";	
	}
}
?>
</table>



<?php
if(isset($_GET['priskirti_grupei']) && isset($group_name)) {
	?>
	<fieldset style="margin-top: 40px;" id="assignEmployees">
	<legend>Vaikų priskirtų grupei „<?=$group_name?>“ redagavimas</legend>
	<form action="<?=URL()?>" method="post">
		<input type="hidden" name="assign_save" value="<?=(int)$_GET['priskirti_grupei']?>">
		<!-- <a href="#" class="addEmployee">Pridėti darbuotoją</a>
		<div id="employees"> -->
		<?php
		$kids = array();
		/*if(ADMIN)
			$res = db_query("SELECT cr.*, `".DB_speech_kids."`.*
		FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<=CURDATE() GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
		JOIN `".DB_speech_kids."` ON cr.`parent_kid_id`=`".DB_speech_kids."`.`kid_id`
		WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0
		-- GROUP BY cr.parent_kid_id
		ORDER BY ".orderName('cr'));//cr.`vardas` ASC, cr.`pavarde` ASC
		else//LOGOPEDAS*/
			$res = db_query("SELECT cr.*, `".DB_speech_kids."`.*
		FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<=CURDATE() GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
		JOIN `".DB_speech_kids."` ON cr.`parent_kid_id`=`".DB_speech_kids."`.`kid_id`
		WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 AND `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']}
		-- GROUP BY cr.parent_kid_id
		ORDER BY ".orderName('cr'));//cr.`vardas` ASC, cr.`pavarde` ASC
		while ($row = mysqli_fetch_assoc($res))
			$kids[$row['parent_kid_id']] = filterText(getName($row['vardas'], $row['pavarde']));
	
		?>
		<div>
			<div class="sel" style="margin-right: 5px;"><select id="personId">
			<option value="" disabled selected hidden>Pasirinkite vaiką priskyrimui</option>
			<?php
			foreach($kids as $key => $value)
				echo "<option value=\"$key\">$value</option>";
			?>
			</select></div> <button id="addPerson" style="display: none;">Priskirti vaiką</button>
		</div>
		<h3>Priskirtų vaikų sąrašas:</h3>
		<div id="persons" style="clear: left">
		<?php
		$result = db_query("SELECT * FROM `".DB_speech_groups_kids."` WHERE `diary_id`=${_SESSION['SPEECH_THERAPIST_DIARY']} AND `group_id`=".(int)$_GET['priskirti_grupei']);
		//$i = 1;
		if(mysqli_num_rows($result)) {
			while ($row = mysqli_fetch_assoc($result)) {
				echo '<p><label>'.$kids[$row['kid_id']].' <input type="hidden" name="priskyrimas[]" value="'.$row['kid_id'].'"></label> <a href="#" class="remPerson" id="person'.$row['kid_id'].'">- Ištrinti susiejimą</a></p>';
			}
		}
		?>
		</div>
		<!-- </div> -->
		<p style="clear: left;"><input type="submit" value="Išsaugoti" class="submit"></p>
		<script src="/workers/speech_therapist/groups.js?v1"></script>
	</form>
	</fieldset>
	<?php
}


//Add/change group name
if(isset($_GET['edit'])) {
	$result = db_query("SELECT * FROM `".DB_speech_groups."` WHERE `ID`=".(int)$_GET['edit']);
	$grupes = mysqli_fetch_assoc($result);
}
?>
<fieldset id="newGroup-form" style="display: none; margin-top: 0px;">
<legend><?=(isset($_GET['edit']) ? "Grupės redagavimas" : "Naujos grupės įvedimas")?></legend>
<form action="<?=URL()?>" method="post" class="not-saved-reminder">
	<p><label><span title="Grupės ar pogrupio, kuriam pedagogas priskyrė ugdytinį, numeris sudaromas naudojant trumpinius grupė „G“, pogrupis „P“, dirbant individualiai „I“. Grupių ir pogrupių numeravimą nustato pedagogas. " class="abbr">Grupės pavadinimas</span><span class="required">*</span>: <input required="required" type="text" name="title" value="<?=(isset($_GET['edit']) ? filterText($grupes['title']) : '')?>"></label></p>
	<p><input type="hidden" <?=(!isset($_GET['edit']) ? 'name="save"' : 'name="update" value="'.(int)$grupes['ID'].'"')?>><input type="submit" value="Išsaugoti" class="submit"></p>
</form>
</fieldset>



</div>
