<?php if(!defined('DARBUOT') || ADMIN) exit(); ?>
<h1>Dokumentų pateikimo priminimai el. laišku tėvams</h1>
<div id="content">
<?php
if(isset($_POST['save'])) {
	if(isset($_POST['kid_id'])) {
		foreach($_POST['kid_id'] as $kid_id)
			if (!mysqli_query($db_link, "INSERT INTO `".DB_reminder."` SET `document_type`='".(int)$_POST['document_type']."',
				`group_id`=".GROUP_ID.", `kid_id`='".(int)$kid_id."', 
				`expiry_date`='".db_fix($_POST['expiry_date'])."', `remind_before`='".(int)$_POST['remind_before']."',
				`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'"))
				logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
		
		msgBox('OK', 'Informacija išsaugota sėkmingai!');
	} else {
		msgBox('ERROR', 'Nepasirinkote nė vieno vaiko!');
	}
}
if(isset($_POST['edit'])) {
	if (!mysqli_query($db_link, "UPDATE `".DB_reminder."` SET `document_type`='".(int)$_POST['document_type']."',
		`group_id`=".GROUP_ID.", `expiry_date`='".db_fix($_POST['expiry_date'])."', `remind_before`='".(int)$_POST['remind_before']."',
		`updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."', `updated`=CURRENT_TIMESTAMP,
		`updatedCounter`=`updatedCounter`+1
		WHERE `reminder_id`=".(int)$_POST['edit']))
		logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
	else
		msgBox('OK', 'Informacija atnaujinta sėkmingai!');
}

if(isset($_GET['delete'])) {
	if (!mysqli_query($db_link, "UPDATE `".DB_reminder."` SET `isDeleted`=1 WHERE `reminder_id`=".(int)$_GET['delete'])) {
		logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
	} else msgBox('OK', 'Sėkmingai ištrinta!');
}
/*
Sample:
2014-04-11 00:01:01 forkik@gmail.com/laiku domas.bla@gmail.com/laiku v.k@smf.vdu.lt/laiku r@yahoo.com/laiku

Rodyti kurie neatnešė.

Praneštų auklėtojas jei neatnešė iki galiojimo pabaigos?

Rodyti tėvams iki kada jų dokumentai galioja.
*/
?>
<a href="/priminimai#reminder-form" class="no-print fast-action fast-action-add">Naujas priminimas</a> <?=ui_print()?>

<?php
	$result = db_query("SELECT cr.*, `".DB_reminder."`.*
	FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<=CURDATE() GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
	JOIN `".DB_reminder."` ON cr.`parent_kid_id`=`".DB_reminder."`.`kid_id`
	WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 AND cr.`grupes_id`=".GROUP_ID." AND `".DB_reminder."`.`isDeleted`=0".(isset($_GET['kid_id']) ? ' AND `'.DB_reminder.'`.`kid_id`='.(int)$_GET['kid_id'] : '')."
	ORDER BY ".orderName('cr'));//cr.`vardas`, cr.`pavarde`
	/*"SELECT * 
		FROM `".DB_reminder."` JOIN `".DB_children."` ON `".DB_reminder."`.`kid_id`=`".DB_children."`.`ID`
		WHERE `".DB_children."`.`archyvas`=0 AND `".DB_children."`.`grupes_id`=".GROUP_ID." AND `".DB_children."`.`isDeleted`=0 AND `".DB_reminder."`.`isDeleted`=0".(isset($_GET['kid_id']) ? ' AND `'.DB_reminder.'`.`kid_id`='.(int)$_GET['kid_id'] : '')."
		ORDER BY vardas, pavarde"*/
if(mysqli_num_rows($result) > 0) {
?>
<h2><!-- Įvesti priminimai (kartu reiškia, kad dar neatnešę naujesnių pažymų negu čia įvesta): 
<del>Įvesti</del> dokumentų atnešimo priminimai (dažniausiai neatnešę naujesnių dokumentų nei čia įvesta):
 (nepateikę naujesnių dokumentų) -->
<!-- (Naujų) -->Dokumentų pateikimo priminimai (priminimas prieš parinktą dienų kiekį iki dokumento galiojimo pabaigos)<!-- <br><del>(pagal šių dokumentų galiojimą ir prieš kiek dienų priminti)</del><br> (pagal parinktą dienų kiekį prieš dokumento galiojimo pabaigą): -->
</h2>
<form method="get">
<div class="sel" style="float: left; margin-right: 5px;">
<select name="kid_id">
        <?php
        //DRY: sveikatos_pastabos.php
    	$result_kids = db_query($get_kids_sql);//"SELECT ".DB_children.".* FROM ".DB_children."  WHERE `archyvas`=0 AND `grupes_id`=".GROUP_ID." AND `isDeleted`=0 ORDER BY vardas, pavarde"
        while($row = mysqli_fetch_assoc($result_kids))
            echo "<option value=\"".$row['parent_kid_id']."\"".(isset($_GET['kid_id']) && $_GET['kid_id'] == $row['parent_kid_id'] ? ' selected="selected"' : '').">".filterText(getName($row['vardas'], $row['pavarde']))."</option>";
        ?>
    </select></div>
	<input type="submit" value="Filtruoti" class="filter">
</form>
<p class="notice">Lentelės eilutės <strong>fono</strong> spalvų reikšmės: <span class="late">raudonas - dokumentas nebegalioja (vėluojama pateikti naują)</span>, <span class="already-reminded">geltonas - artėja dokumento galiojimo pabaiga, turėtų būti išsiųstas tėvams priminimas, tėvams jau reikia susirūpinti ir stengtis kuo greičiau pateikti naują dokumentą (pedagogams verta užsiklausti apie tai)</span>, neutralus - dokumentas dar ilgai galioja, tad juo rūpintis dar nereikia<!-- viskas tvarkoje (dokumentas galioja) -->.</p>
<table>
	<tr>
		<th>Dokumentas</th>
		<th class="date-cell">Galioja iki</th>
		<th width="60">Priminti likus d.</th>
		<th>Vaikų tėvams</th>
		<th>Veiksmai</th>
		<?php if(KESV) { ?>
		<th>Sukūrė</th>
		<?php } ?>
	</tr>
	<?php
	$CUR_DATE = date('Y-m-d');
	while($row = mysqli_fetch_assoc($result)) {
		echo "
	<tr".($row['expiry_date'] < $CUR_DATE ? ' class="late"' : ($CUR_DATE >= date('Y-m-d', strtotime('-'.(int)$row['remind_before'].' day', strtotime($row['expiry_date']))) ? ' class="already-reminded"' : '' ) ).">
		<td>".filterText($reminder_document_types[$row['document_type']])."</td>
		<td>".filterText($row['expiry_date'])."</td>
		<td>".(int)$row['remind_before']."</td>
		<td>".filterText(getName($row['vardas'], $row['pavarde']))."</td>
		<td><a href=\"?edit=".$row['reminder_id']."#reminder-form\">Keisti</a> <a href=\"?delete=".$row['reminder_id']."\" onclick=\"return confirm('Ar tikrai norite ištrinti?')\">Trinti</a></td>
		";
		if(KESV) {
			echo '<td>';
			$res = db_query("SELECT * FROM `".DB_users_allowed."` JOIN `".DB_users."` ON `".DB_users_allowed."`.`user_id`=`".DB_users."`.`user_id` WHERE `person_type`>0 AND `person_id`='".(int)$row['createdByEmployeeId']."'");
			$f = '';
			while($r = mysqli_fetch_assoc($res))
				$f .= $person_type[$r['person_type']].' '.$r['email'].' ';
			$res = db_query("SELECT * FROM `".DB_users_allowed."` JOIN `".DB_users."` ON `".DB_users_allowed."`.`user_id`=`".DB_users."`.`user_id` WHERE `person_type`>0 AND `person_id`='".(int)$row['updatedByEmployeeId']."'");
			$s = '';
			while($r = mysqli_fetch_assoc($res))
				$s .= $person_type[$r['person_type']].' '.$r['email'].' ';
			echo 'Sukurta: &nbsp;'.$row['created'].' '.getAllEmployees($row['createdByEmployeeId']).' '.$f.';<br>Pakeista: '.$row['updated'].' '.getAllEmployees($row['updatedByEmployeeId']).' '.$s;
			echo '</td>';
		}
		echo "
	</tr>";
	}
	?>
</table>
<?php } ?>

<?php
//Nerodo archyvuotų vaikų info:
$result = db_query("SELECT cr.*, `".DB_reminder_sent."`.*, DATE(`".DB_reminder_sent."`.`sent_date`) AS `sent_date`
FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<=CURDATE() GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
JOIN `".DB_reminder_sent."` ON cr.`parent_kid_id`=`".DB_reminder_sent."`.`kid_id`
WHERE cr.`isDeleted`=0 AND cr.`archyvas`=0 AND cr.`grupes_id`=".GROUP_ID.(isset($_GET['kid_id']) ? ' AND `'.DB_reminder_sent.'`.`kid_id`='.(int)$_GET['kid_id'] : '')."
ORDER BY `sent_date` DESC -- ".orderName('cr')."
LIMIT 0,30");//cr.`vardas`, cr.`pavarde`
/*"SELECT *, DATE(`sent_date`) AS `sent_date`
	FROM `".DB_reminder_sent."` JOIN `".DB_children."` ON `".DB_reminder_sent."`.`kid_id`=`".DB_children."`.`ID`
	WHERE `".DB_children."`.`archyvas`=0 AND `".DB_reminder_sent."`.`group_id`=".GROUP_ID." AND `".DB_children."`.`isDeleted`=0".(isset($_GET['kid_id']) ? ' AND `'.DB_reminder_sent.'`.`kid_id`='.(int)$_GET['kid_id'] : '')."
	ORDER BY vardas, pavarde"*/
if(mysqli_num_rows($result) > 0) {
?>
<h2>Išsiųsti priminimai tėvams (rodoma 30 naujausių):</h2>
<table>
	<tr>
		<th class="date-cell">Išsiuntimo data</th>
		<th>Vaikas</th>
		<th>El. paštams</th>
		<th>Dokumento rūšiai</th>
	</tr>
	<?php
	while($row = mysqli_fetch_assoc($result))
		echo "<tr>
		<td>".filterText($row['sent_date'])."</td>
		<td>".filterText(getName($row['vardas'], $row['pavarde']))."</td>
		<td>".filterText($row['sent_emails'])."</td>
		<td>".filterText($reminder_document_types[$row['document_type']])."</td>
		</tr>";
	?>
</table>
<?php } ?>

<script type="text/javascript" src="/libs/jquery.checkboxes.min.js"></script>
<script type="text/javascript">
$(function($) {
    $('#reminder').checkboxes('range', true);
});
</script>
<fieldset>
	<?php
	if(isset($_GET['edit'])) {
		$result = db_query("SELECT * FROM `".DB_reminder."` JOIN `".DB_children."` ON `".DB_reminder."`.`kid_id`=`".DB_children."`.`ID` WHERE `reminder_id`=".(int)$_GET['edit']);
		$row = mysqli_fetch_assoc($result);
	}
	?>
	<legend><?=(isset($_GET['edit']) ? 'Redaguoti priminimą' : 'Naujas priminimas')?></legend>
	<form action="#" method="post" id="reminder-form" class="not-saved-reminder">
	<div>Dokumento rūšis<span class="required">*</span>: <div class="sel"><select name="document_type">
	<?php
		foreach($reminder_document_types as $key => $val) {
			echo '<option value="'.$key.'"'.(isset($_GET['edit']) && $row['document_type'] == $key ? ' selected="selected"' : '').'>'.$val.'</option>';
		}
	?>
	</select></div></div>
	<p><label>Galioja iki<span class="required">*</span>: <input class="datepicker" type="text" name="expiry_date" value="<?=(isset($_GET['edit']) ? filterText($row['expiry_date']) : date('Y-m-d'))?>" required="required"></label></p>
	<p><label>Priminti likus dienų iki galiojimo pabaigos<span class="required">*</span>: <input type="number" name="remind_before" min="0" placeholder="10" required="required" value="<?=(isset($_GET['edit']) ? (int)$row['remind_before'] : '')?>"></label></p>
	<?php if(!isset($_GET['edit'])) { ?>
	<div>Priminti vaikų tėvams<span class="required">*</span>:<br>
		<div style="margin-left: 25px;">
		<a href="#reminder" id="reminder-checkboxes" data-toggle="checkboxes" data-action="check">Pažymėti visus priminimui</a>
		<table id="reminder">
		<tr>
			<th>Vaikas</th>
			<th>Ar priminti?</th>
		</tr>
		<?php
		$result = db_query($get_kids_sql);//"SELECT * FROM `".DB_children."` WHERE `archyvas`=0 AND `grupes_id`=".GROUP_ID." AND `isDeleted`=0 ORDER BY `vardas` ASC, `pavarde` ASC"
		while($kid = mysqli_fetch_assoc($result)) {
			echo "\t<tr><td>".filterText(getName($kid['vardas'], $kid['pavarde']))."</td>
				<td><input type=\"checkbox\" name=\"kid_id[]\" value=\"".$kid['parent_kid_id']."\"".(isset($_POST['kid_id']) && in_array($kid['parent_kid_id'], $_POST['kid_id'])  ? ' checked="checked"' : '')."></td></tr>\n";
		}
		?>
		</table>
		</div>
	</div>
	<?php } else {
		echo '<p>Primins vaikų tėvams: '.filterText(getName($row['vardas'], $row['pavarde'])).'</p>';
	} ?>
	<p><input type="hidden" name="<?=(isset($_GET['edit']) ? 'edit' : 'save')?>" value="<?=(isset($_GET['edit']) ? (int)$row['reminder_id'] : '')?>"><input type="submit" value="Išsaugoti" class="submit"></p>
	</form>
</fieldset>
</div>
