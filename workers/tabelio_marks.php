<?php if(!defined('DARBUOT') && !DARBUOT) exit(); /*TODO: tik vadovams? Galimai ir kitiems darbuotojams */
$max_opened_msg_delay = 15;//30 s.
?>
<h1>Žiniaraščio (tabelio) žymenys</h1>
<div id="content">
<?php
if(isset($_POST['save'])) {
	db_query("INSERT INTO `".DB_attendance_marks."` SET 
		`abbr`='".db_fix($_POST['abbr'])."',
		`title`='".db_fix($_POST['title'])."',
		`kindergarten_id`=".DB_ID.",
		`createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'");
	msgBox('OK', 'Žymuo išsaugotas.');
}
if(isset($_POST['edit'])) {
	db_query("UPDATE `".DB_attendance_marks."` SET 
		`abbr`='".db_fix($_POST['abbr'])."',
		`title`='".db_fix($_POST['title'])."',
		`updated`=CURRENT_TIMESTAMP, `updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."',
		`updatedCounter`=`updatedCounter`+1
		WHERE `ID`=".(int)$_POST['edit']." AND `kindergarten_id`=".DB_ID);
	msgBox('OK', 'Žymuo atnaujintas.');
}
/*if(isset($_GET['delete'])) {
	db_query("DELETE FROM `".DB_attendance_marks."` WHERE `ID`=".(int)$_GET['delete']." AND `kindergarten_id`=".DB_ID);
	msgBox('OK', 'Žymuo ištrintas.');
}*/

?>
<a href="?new#mark" class="no-print fast-action fast-action-add">Naujas žymuo</a> <?=ui_print()?>
<div>Standartiniai žymenys:
<ul>
<li>p - nelankyta pateisinta diena</li>
<li>n - nelankyta nepateisinta diena</li>
<li> &nbsp; - lankyta diena</li>
<li>- - vaiko oficialiai darželyje nėra</li>
<?php /* <li>Ž - dienai lankomumas pažymėtas, bet pagal vaiko duomenis vaiko šiai dienai dabar nėra oficialiai registruoto</li> */ ?>
</ul>
</div>
<table>
<tr>
	<th>Santrumpa/Žymuo</th>
	<th>Pavadinimas</th>
	<th class="no-print">Veiksmai</th>
	<th class="no-print">Sukūrė</th>
	<th class="no-print">Atnaujino</th>
</tr>
<?php
$result = db_query("SELECT * FROM `".DB_attendance_marks."` WHERE `kindergarten_id`=".DB_ID." AND `isDeleted`=0 ORDER BY `abbr`");
while($row = mysqli_fetch_assoc($result)) {
	echo "<tr".(isset($_GET['edit']) && $_GET['edit'] == $row['ID'] ? ' class="opened-row"' : '').">
	<td>".filterText($row['abbr'])."</td>
	<td>".filterText($row['title'])."</td>
	<td class=\"no-print\"> <a href=\"?edit=".$row['ID']."#mark\">Keisti</a>
	<td class=\"no-print\">".$row['created']." ".getAllEmployees($row['createdByEmployeeId'])."</td>
	<td class=\"no-print\">".date_empty($row['updated'])." ".getAllEmployees($row['updatedByEmployeeId'])."</td>
	</tr>";// <a href=\"?delete=".$row['ID']."\" onclick=\"return confirm('Ar tikrai norite ištrinti žymenį?')\">Trinti</a></td>
}
echo '</table>';

if (isset($_GET['edit']) || isset($_GET['new'])) {
	if (isset($_GET['edit'])) {
		$result = db_query("SELECT * FROM `".DB_attendance_marks."` WHERE `ID`=".(int)$_GET['edit']." AND `kindergarten_id`=".DB_ID);
		$mark = mysqli_fetch_assoc($result);
	}
	?>
	<fieldset id="mark">
	<legend><?=(isset($_GET['edit']) ? 'Žymens keitimas' : 'Naujas žymuo')?></legend>
	<form method="post" class="not-saved-reminder no-print">
		<p><label>Santrumpa/žymuo <input type="text" name="abbr" style="width: 50px;" value="<?=(isset($mark) ? filterText($mark['abbr']) : '')?>"> (pvz., a)</label></p>
		<p><label>Pavadinimas <input type="text" name="title" value="<?=(isset($mark) ? filterText($mark['title']) : '')?>"> (pvz., atostogos)</label></p>
		<p><input type="hidden" <?=(!isset($_GET['edit']) ? 'name="save"' : 'name="edit" value="'.(int)$mark['ID'].'"')?>><input type="submit" value="Išsaugoti" class="submit"></p>
	</form>
	</fieldset>
<?php
}
?>
</div>
