<?php if(!ADMIN) exit(); ?>
<h1 class="no-print">Kvitai (tik spausdinimui)</h1>
<div id="content" class="kvitai">
<?=ui_print()?>
<form method="get" style="padding-bottom:10px;" class="no-print">
	<div class="sel" style="float: left; margin-right: 5px;"><select name="data" required="required" title="Žymėto lankomumo laikotarpis">
	<?php
	if(isset($_GET['kvitas']) && isset($_GET['data'])) {
		//$data = explode('-', $_POST['data']);
		list($year, $month) = explode('-', $_GET['data']);//, $day
		$year = (int) $year;
		$month = (int) $month;
	}
	//$result = db_query("SELECT `metai`, `menuo` FROM `".DB_working_d."` GROUP BY `metai`, `menuo`");
	$result = db_query("SELECT YEAR(`data`) `metai`, MONTH(`data`) `menuo` FROM `".DB_attendance."` GROUP BY YEAR(`data`) DESC, MONTH(`data`) DESC");
	while ($row = mysqli_fetch_assoc($result))
		echo "<option value=\"".$row['metai']."-".$row['menuo']."\"".(isset($_GET['kvitas']) && isset($year) && $row['metai'] == $year && $row['menuo'] == $month ? ' selected="selected"' : '').">".$row['metai']."-".men($row['menuo'])."</option>";
	?>
	</select></div>
	<div class="sel" style="float: left; margin-right: 5px;"><select name="grupes_id">
	<?php 
	foreach(getAllGroups() as $ID => $title) {
		echo '<option value="'.$ID.'"'.(isset($_GET['grupes_id']) && $ID == $_GET['grupes_id'] ? ' selected="selected"' : '').">".filterText($title)."</option>";
		if(isset($_GET['grupes_id']) && $ID == $_GET['grupes_id'])
			$grupes_pav = filterText($title);
	}
	?>
	</select></div>
	<input type="hidden" name="kvitas">
	<input type="submit" value="Sudaryti kvitus spausdinimui" class="submit" title="Žiūrėkite tik spausdinimo variantą interneto naršyklėje, kad matytumėte vaizdą kaip galėtų atrodyti atspausdinus">
	<?php
	if(isset($_GET['kvitas']) && (TESTINIS || RODOMASIS || NAUJAS))
		echo '<input type="submit" name="save" value="Sudaryti kvitus mokėjimams" class="submit">';
	?>
</form>


<?php
if(isset($_GET['kvitas']) && isset($_GET['grupes_id']) && isset($_GET['data'])) {
	$data_max = $year.'-'.men($month).'-'.date('t', strtotime($year.'-'.men($month).'-01'));
	$data_min = $year.'-'.men($month).'-01';
	// SUM(`priezastis`<>'' AND `yra`=0) AS `pateisino`
	/*$result = db_query("SELECT *, COUNT(*) AS `kiek`, SUM(`yra`) AS `buvo`
		FROM `".DB_attendance."` JOIN `".DB_children."` ON `".DB_attendance."`.`vaiko_id`=`".DB_children."`.`ID`
		WHERE `grupes_id`=".(int)$_GET['grupes_id']." AND
			YEAR(`".DB_attendance."`.`data`)=".$year." AND MONTH(`".DB_attendance."`.`data`)=".$month." AND
			`".DB_children."`.`archyvas`=0
		GROUP BY `vaiko_id`");*/
	//DRY from tabeliai.php
	$darbo_dienos = db_query("SELECT DAY(`".DB_attendance."`.`data`) AS `diena`
		FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<='".db_fix($data_max)."' AND `grupes_id`=".(int)$_GET['grupes_id']." GROUP BY `parent_kid_id` --  AND `archyvas`=0
		) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from`
			JOIN `".DB_attendance."` ON cr.`parent_kid_id`=`".DB_attendance."`.vaiko_id
		WHERE cr.`isDeleted`= 0 AND YEAR(`".DB_attendance."`.`data`)=".$year." AND MONTH(`".DB_attendance."`.`data`)=".$month."
		GROUP BY DAY(`".DB_attendance."`.`data`)");
	$kiek_darbo_dienu = mysqli_num_rows($darbo_dienos);
	
	$result = db_query("SELECT `".DB_attendance."`.*, cr.*, COUNT(*) AS `kiek`, SUM(`yra`) AS `buvo`
		FROM `".DB_attendance."` 
		JOIN `".DB_children."` cr ON (cr.`parent_kid_id`=`".DB_attendance."`.vaiko_id AND cr.`valid_from`<=`".DB_attendance."`.`data`)
		LEFT JOIN `".DB_children."` mx ON cr.`parent_kid_id`=mx.`parent_kid_id` AND cr.`valid_from`<mx.`valid_from` AND cr.`valid_from`<=`".DB_attendance."`.`data` AND mx.`valid_from`<=`".DB_attendance."`.`data`
		WHERE cr.`isDeleted`= 0 AND YEAR(`".DB_attendance."`.`data`)=".$year." AND MONTH(`".DB_attendance."`.`data`)=".$month." AND cr.`grupes_id`=".(int)$_GET['grupes_id']." AND mx.`parent_kid_id` IS NULL
		GROUP BY cr.`parent_kid_id` -- `vardas`, `pavarde`
		ORDER BY ".orderName('cr', $data_max));
	$dienu_arr = array();
	while ($row = mysqli_fetch_assoc($darbo_dienos)) {
		$dienu_arr[] = $row['diena'];
	}
	$i = 1;
	while ($row = mysqli_fetch_assoc($result)) {
		//Validation/Checking
		$exists_in_group = false;
		$pre_date = $year.'-'.men($month).'-';
		$groups = array();
		foreach($dienu_arr as $diena) {
			$kid = getKid($row['parent_kid_id'], $pre_date.dien($diena));
			if( $kid != false && !in_array($kid['grupes_id'], $groups) )
				$groups[] = $kid['grupes_id'];
			if( $kid != false && $kid['grupes_id'] == (int)$_GET['grupes_id'] && !$kid['isDeleted'] )//Pagal idėją turėtų būti && !$kid['archyvas']
				$exists_in_group = true;
		}
		if(!$exists_in_group) continue;
		
		$group_sums = array();
		$sums = db_query("SELECT * FROM `".DB_tabeliai_charge."` WHERE `kid_id`=".$row['parent_kid_id']." AND `date`='".$data_min."'");
		while ($data = mysqli_fetch_assoc($sums))
			$group_sums[$data['group_id']] = $data['charge'];
		
		$vaiko_lankomumas = db_query("SELECT *, DAY(`".DB_attendance."`.`data`) AS `diena`
			FROM `".DB_attendance."` 
			WHERE `vaiko_id`=".(int)$row['parent_kid_id']." AND YEAR(`data`)=".$year." AND MONTH(`data`)=".$month."
			ORDER BY `data`");
		$total_cost = array();
		$kid_food = array();
		$ugdymas = array();
		$sport = 0;
		$additional_cost_per_day = array();
		foreach($groups as $group_id) {
			$total_cost[$group_id] = 0;
			$kid_food[$group_id] = 0;
			$ugdymas[$group_id] = 0;
			$additional_cost_per_day[$group_id] = 0;
		}
		while ($lankomumas = mysqli_fetch_assoc($vaiko_lankomumas)) {
			//Prepare cost calculation
			$data_dabar = $year.'-'.men($month).'-'.dien($lankomumas['diena']);
			$kid = getKid($lankomumas['vaiko_id'], $data_dabar);
			if(!is_array($kid))
				loga('Not found: '.$lankomumas['vaiko_id'] . ' '.$data_dabar.print_r($lankomumas, true).print_r($kid, true));
			else {
			//if($kid['grupes_id'] == (int)$_GET['grupes_id']) {
				$lankomumas = array_merge($kid, $lankomumas);
				//print_r($lankomumas);
				//$buvo += $lankomumas['yra'];
				//++$zymeta;
				$yra[$lankomumas['diena']] = $lankomumas['yra'];
				$justifies[$lankomumas['diena']] = $lankomumas['justified_d'];
  
				//Cost calculation
				$nuolaid = $nuolaidos[$lankomumas['arNuolaida']];
				$kid_level = array(
					0 => 'lopselyje',
					1 => 'darzelyje',
					2 => 'priesmokykliniame'
				);
				$cur_kid_level = $kid_level[$lankomumas['arDarzelyje']];
				$doRound = getConfig('atskirai_apvalinti_kiekvieno_maitinimo_karto_ir_ugdymo_kainas', $data_dabar);
				$day_food = $lankomumas['arPusryciaus']*!$lankomumas['free_breakfast']*(double)getConfig('vieneriu_pusryciu_kaina_'.$cur_kid_level, $data_dabar)+$lankomumas['arPietaus']*!$lankomumas['free_lunch']*(double)getConfig('vieneriu_pietu_kaina_'.$cur_kid_level, $data_dabar)+$lankomumas['arPavakariaus']*!$lankomumas['free_afternoon_tea']*(double)getConfig('vieneriu_pavakariu_kaina_'.$cur_kid_level, $data_dabar)+$lankomumas['arVakarieniaus']*!$lankomumas['free_dinner']*(double)getConfig('vienerios_vakarienes_kaina_'.$cur_kid_level, $data_dabar);
				
				$ugdym = (double)kaina_ugdymo_reikmiu($lankomumas['arDarzelyje'], $data_dabar);
				
				if(KaunoRBaibokyne) {
					//Neturi
					/*if($lankomumas['yra'] || !$lankomumas['yra'] && !$lankomumas['justified_d']) {
						$maistas_ugdymas += $day_food + $nuolaid*((double)getConfig('ugdymo_dienos_kaina', $data_dabar));
						$dienos_mokestis = $lankomumas['dienosMokestis'] ? (double)getConfig('puses_dienos_kaina', $data_dabar) : (double)getConfig('pilnos_dienos_kaina', $data_dabar);
						$islaikymas += $nuolaid*((double)getConfig('islaikymo_dienos_kaina', $data_dabar)+$dienos_mokestis);
						//Maistas - $maistas_ugdymas
						//Dienos mokestis - $islaikymas = išlaikymo dienos kaina=0 + dienos mokestis
						//Iš viso
					}*/
				} elseif(KaunoVaikystesLobiai) {
					if($lankomumas['yra'] || !$lankomumas['yra'] && !$lankomumas['justified_d']) {//Būtoms ir nelankytoms be pateisinimo
						$kid_food[$kid['grupes_id']] += $lankomumas['dienosMokestis'] ? (double)getConfig('puses_dienos_maitinimas_per_d._d.', $data_dabar) : (double)getConfig('pilnos_dienos_maitinimas_per_d._d.', $data_dabar);
					}
				} elseif(DruskininkuBitute) {
					if($lankomumas['yra'] || !$lankomumas['yra'] && !$lankomumas['justified_d']) {//Būtoms ir nelankytoms be pateisinimo
						if($nuolaidos_title[$lankomumas['arNuolaida']] != '100_%_nuolaida')
							$total_cost[$kid['grupes_id']] += (double)getConfig('dienos_kaina_per_d._'.$cur_kid_level.'_'.$nuolaidos_title[$lankomumas['arNuolaida']], $data_dabar);
					} /*else {
						if($cur_kid_level != 'priesmokykliniame' && $nuolaidos_title[$lankomumas['arNuolaida']] != '100_%_nuolaida')
							$total_cost[$kid['grupes_id']] += (double)getConfig('dienos_kaina_per_d._'.$cur_kid_level.'_'.$nuolaidos_title[4], $data_dabar);//80 % nuolaida => *0,2
					}*/
				} else {
					if($lankomumas['yra'] || !$lankomumas['yra'] && !$lankomumas['justified_d']) {//Būtoms ir nelankytoms be pateisinimo
						if($doRound) {
							$day_food = round($lankomumas['arPusryciaus']*!$lankomumas['free_breakfast']*(double)getConfig('vieneriu_pusryciu_kaina_'.$cur_kid_level, $data_dabar)*$nuolaid, 2);
							$day_food += round($lankomumas['arPietaus']*!$lankomumas['free_lunch']*(double)getConfig('vieneriu_pietu_kaina_'.$cur_kid_level, $data_dabar)*$nuolaid, 2);
							$day_food += round($lankomumas['arPavakariaus']*!$lankomumas['free_afternoon_tea']*(double)getConfig('vieneriu_pavakariu_kaina_'.$cur_kid_level, $data_dabar)*$nuolaid, 2);
							$day_food += round($lankomumas['arVakarieniaus']*!$lankomumas['free_dinner']*(double)getConfig('vienerios_vakarienes_kaina_'.$cur_kid_level, $data_dabar)*$nuolaid, 2);
							
							$kid_food[$kid['grupes_id']] += $day_food + (double)getConfig('papildoma_kaina_uz_patiekalu_gamyba_per_d._d.', $data_dabar)*$nuolaid;
							if($apply_discount_for_educational_needs_fee_each_day) {
								$ugdym = round($ugdym*$nuolaid, 2);
								$ugdymas[$kid['grupes_id']] += $ugdym;
								$total_cost[$kid['grupes_id']] += $day_food + $ugdym;
							} else {
								$ugdymas[$kid['grupes_id']] += $ugdym;
								$total_cost[$kid['grupes_id']] += $day_food + $ugdym;
							}
						} else {
							$kid_food[$kid['grupes_id']] += ($day_food+(double)getConfig('papildoma_kaina_uz_patiekalu_gamyba_per_d._d.', $data_dabar))*$nuolaid;
							if($apply_discount_for_educational_needs_fee_each_day) {
								$ugdymas[$kid['grupes_id']] += $ugdym*$nuolaid;
								$total_cost[$kid['grupes_id']] += ($day_food+$ugdym)*$nuolaid;
							} else {
								$ugdymas[$kid['grupes_id']] += $ugdym;
								$total_cost[$kid['grupes_id']] += $day_food*$nuolaid + $ugdym;
							}
						}
					}
				}
				$additional_cost_per_day[$kid['grupes_id']] += (double)$lankomumas['additional_cost_per_day'];
			//}
			}
		}
		
		$kid_food = array_sum($kid_food);
		$ugdymas = array_sum($ugdymas);
		if(KaunoVaikystesLobiai) {
			$kid = getKid($row['parent_kid_id'], $data_max);
			$max_food_cost = $kid['dienosMokestis'] ? (double)getConfig('puses_dienos_maitinimo_per_men._sumos_riba', $data_max) : (double)getConfig('pilnos_dienos_maitinimo_per_men._sumos_riba', $data_max);
			if($kid_food > $max_food_cost) {
				$kid_food = $max_food_cost;
			}
			$montly_day_cost = ($kid['dienosMokestis'] ? (double)getConfig('puses_dienos_kaina_per_men.', $data_max) : (double)getConfig('pilnos_dienos_kaina_per_men.', $data_max) * ((100-(double)$kid['percentage_discount_from_day_tax_per_month'])/100));
			$montly_day_cost -= (double)$kid['monthly_discount_sum'];
			$total_cost = $montly_day_cost + $kid_food + array_sum($additional_cost_per_day);		  		
			/* (pilnos_ar_pusės_dienos_kaina_per_mėn.*((100 - nuolaidos_procentai_nuo_dienos_mokesčio_per_mėn.)/100) +
(vaikui_žymėtos_lankomumo_dienos - vaikui_pateisintos_dienos)*maitinimas_vienam_vaikui_per_pilną/pusę_dienos +
suma_lankomumo_laukelio_kiekvienos_dienos_„Papildoma kaina“)
- mėnesinės_nuolaidos_suma */
			//echo "<td>".price($total_cost)." ".price($montly_day_cost)." ".price($kid_food)." ".price($additional_cost_per_day)." ".price($kid['monthly_discount_sum'])."</td>";//Mokėjimui (Lt)
			
			//$total_cost = ($row['charge'] == '' ? $total_cost : $row['charge']);
		} else {
			if(!KaunoRBaibokyne) {
				$total_cost = array_sum($total_cost);
				$kid = getKid($row['parent_kid_id'], $data_max);
				$ugdymas += (double)kaina_ugdymo_reikmiu_men($kid['arDarzelyje'], $data_max);
				/*$ugdymas_agg = (double)kaina_ugdymo_reikmiu_men($kid['arDarzelyje'], $data_max);
				foreach($groups as $group_id) {
					$ugdymas_agg += $ugdymas[$group_id];
				}*/
				$nuolaid = $nuolaidos[$kid['arNuolaida']];
				$total_cost/*_agg*/ += array_sum($additional_cost_per_day)*$nuolaid;
				$total_cost/*_agg*/ += (double)$kid['feeForPlace'];
				if(TauragesRAzuoliukas || TESTINIS) {
					$sport = (double)((int)$kid['attend_sport']*(double)getConfig('kaina_uz_sporta_per_men.,_taikoma_pazymejus_vaiko_duomenyse', $data_max));
        			$total_cost += $sport;
        		}
        		
				$total_cost_agg = 0;
				foreach($groups as $group_id) {
					//if(!KaunoRBaibokyne)
					//$total_cost_agg += ($group_sums[$group_id] == '' ? $total_cost : $group_sums[$group_id]);
					$total_cost_agg += (isset($group_sums[$group_id]) ? $group_sums[$group_id] : 0);
				}
				if((double)$total_cost_agg != 0) {
					$total_cost = $total_cost_agg;
				}
			}
		}
		$additional_cost_per_day = array_sum($additional_cost_per_day);
		$additional_day_tax_title = getConfig('papildomos_dienos_kainos,_kuri_ivedama_zymint_lankomuma,_pavadinimas', $data_max);//getConfig('pavadinimas_laukeliui_irasoma_papildoma_dienos_kaina_zymint_lankomuma', $data_max);
		
		
		/*$res_pateisinimai = db_query("SELECT * FROM ".DB_attendance." l, ".DB_documents." d, ".DB_justified_d." p
		WHERE YEAR(l.`data`)=".$year." AND MONTH(l.`data`)=".$month." AND
		p.veikimo_d_id=l.veikimo_d_id AND
		l.vaiko_id=".(int)$row['vaiko_id']." AND
		d.vaiko_id=l.vaiko_id AND
		d.ID=p.dok_id");*/
		ob_start();
		echo "<p><em>Gavėjas</em> ".filterText(getConfig('darzelio_pavadinimas', $data_max))."</p>";
		?>
		<table>
		<tr>
			<th>Gavėjo bankas</th>
			<th>Gavėjo sąskaita</th>
			<th>Banko kodas</th>
		</tr>
		<tr>
			<td><?=filterText(getConfig('banko_pavadinimas', $data_max))?></td>
			<td><?=filterText(getConfig('banko_saskaita', $data_max))?></td>
			<td><?=filterText(getConfig('banko_kodas', $data_max))?></td>
		</tr>
		<?php
		$banko_saskaita_2 = filterText(getConfig('2_banko_saskaita', $data_max));
		if(!empty($banko_saskaita_2)) {
		?>
		<tr>
			<td><?=filterText(getConfig('2_banko_pavadinimas', $data_max))?></td>
			<td><?=$banko_saskaita_2?></td>
			<td><?=filterText(getConfig('2_banko_kodas', $data_max))?></td>
		</tr>
		<?php
		}
		$banko_saskaita_3 = filterText(getConfig('3_banko_saskaita', $data_max));
		if(!empty($banko_saskaita_3)) {
		?>
		<tr>
			<td><?=filterText(getConfig('3_banko_pavadinimas', $data_max))?></td>
			<td><?=filterText(getConfig('3_banko_saskaita', $data_max))?></td>
			<td><?=filterText(getConfig('3_banko_kodas', $data_max))?></td>
		</tr>
		<?php
		}
		$banko_saskaita_4 = filterText(getConfig('4_banko_saskaita', $data_max));
		if(!empty($banko_saskaita_4)) {
		?>
		<tr>
			<td><?=filterText(getConfig('4_banko_pavadinimas', $data_max))?></td>
			<td><?=filterText(getConfig('4_banko_saskaita', $data_max))?></td>
			<td><?=filterText(getConfig('4_banko_kodas', $data_max))?></td>
		</tr>
		<?php } ?>
		</table>
		<?php
		/*
		echo "<p><em>Gavėjo bankas</em> ".filterText(getConfig('banko_pavadinimas', $data_max))." ".filterText(getConfig('banko_skyrius', $data_max))."</p>";
		echo "<p><em>Gavėjo sąskaita</em> ".filterText(getConfig('banko_saskaita', $data_max))." <em>Banko kodas</em> ".filterText(getConfig('banko_kodas', $data_max))."</p>";
		echo "<p><em>Gavėjo 2 bankas</em> ".filterText(getConfig('2_banko_pavadinimas', $data_max))." ".filterText(getConfig('2_banko_skyrius', $data_max))."</p>";
		echo "<p><em>Gavėjo 2 sąskaita</em> ".filterText(getConfig('2_banko_saskaita', $data_max))." <em>Banko kodas</em> ".filterText(getConfig('2_banko_kodas', $data_max))."</p>";
		*/
		echo "<p><em>Mokėtojo vardas, pavardė</em> ".filterText($row['vardas'])." ".filterText($row['pavarde'])."</p>";

		$euro = $year >= '2015';
		$litai = ($euro ? 3.4528 : 1);
		$eurai = ($euro ? 1 : 3.4528);
		?>
		<table>
		<tr>
			<th>Įmokos, mokesčio pavadinimas</th>
			<th>Mokesčio kodas</th>
			<th>Mokėtojo kodas</th>
			<th>Atsiskaitymo laikotarpis</th>
			<th>Suma (Litai)</th>
			<th>Suma (Eurai)</th>
		</tr>
		<?php if($kid_food > 0) { ?>
		<tr>
			<td>Už maitinimą<!-- <?=(isset($_GET['maitinimas']) || KaunoVaikystesLobiai ? 'Maitinimas'/*Tauragė*/ : 'Už darželį')?> --></td>
			<td><!-- Nėra --></td>
			<td class="center"><?php echo filterText($row['asm_sas']); ?></td>
			<td class="center"><?php echo $year."-".men($month); ?></td>
			<td class="right"><?php echo price($kid_food*$litai, true); ?></td>
			<td class="right"><?php echo price(round($kid_food, 2)/$eurai, true) ?></td>
		</tr>
		<?php } ?>
		<?php if(isset($montly_day_cost)) { ?>
	   	<tr>
			<td>Ugdymas ir priežiūra</td>
			<td></td>
			<td class="center"></td>
			<td class="center"></td>
			<td class="right"><?php echo price($montly_day_cost*$litai, true); ?></td>
			<td class="right"><?php echo price(round($montly_day_cost, 2)/$eurai, true) ?></td>
		</tr>
	   	<?php } ?>
		<?php if($ugdymas > 0) { ?>
		<tr>
			<td>Už ugdymą</td>
			<td></td>
			<td></td>
			<td></td>
			<td class="right"><?php echo price($ugdymas*$litai, true); ?></td>
			<td class="right"><?php echo price(round($ugdymas, 2)/$eurai, true) ?></td>
		</tr>
		<?php } ?>
		<?php if((double)$kid['feeForPlace'] > 0) { ?>
		<tr>
			<td>Už vietą darželyje</td>
			<td></td>
			<td></td>
			<td></td>
			<td class="right"><?php echo price((double)$kid['feeForPlace']*$litai, true); ?></td>
			<td class="right"><?php echo price(round((double)$kid['feeForPlace'], 2)/$eurai, true) ?></td>
		</tr>
		<?php } ?>
		
		<!-- per mėn. fiksuota Žiogelis (Šiauliai ir Kretinga 25 Lt)
		<tr>
			<td>Aplinkos tvarkymas</td>
			<td></td>
			<td></td>
			<td></td>
			<td class="right"><?php echo price(0, true); ?></td>
			<td class="right"><?php echo price(round(0, 2)/$eurai, true) ?></td>
		</tr>
		-->
		<?php if($sport > 0) { //Tauragė (nėra aplinkos tvarkymo)?>
		<tr>
			<td>Už sportą</td>
			<td></td>
			<td></td>
			<td></td>
			<td class="right"><?php echo price($sport*$litai, true); ?></td>
			<td class="right"><?php echo price(round($sport, 2)/$eurai, true) ?></td>
		</tr>
		<?php } ?>
		<?php if($additional_cost_per_day > 0) { //VL (ilgiau) ir Šiaulių Žiogelis (Baseinas už užsiėmimą) ?>
		<tr>
			<td><?=$additional_day_tax_title?></td>
			<td></td>
			<td></td>
			<td></td>
			<td class="right"><?php echo price($additional_cost_per_day*$litai, true); ?></td>
			<td class="right"><?php echo price(round($additional_cost_per_day, 2)/$eurai, true) ?></td>
		</tr>
		<?php } ?>
		<tr>
			<td>Galutinė suma<!-- Iš viso --></td>
			<td></td>
			<td></td>
			<td></td>
			<td class="right"><?php echo price($total_cost*$litai, true)/*($isViso = ($ugdym+$uzDarzeli)*$nuolaid)*/; ?></td>
			<td class="right"><?php echo price(round($total_cost, 2)/$eurai, true) ?></td>
		</tr>
		<?php
		echo "</table>
			<p>
			<div class=\"fl\">Mokėjimo<br>data _____________</div>
			<div class=\"fl\">Mokėtojo<br>parašas _________________</div>
			<div class=\"fl\">Banko darbuotojo<br>parašas __________________</div>
			</p>";
		$duom = ob_get_contents();
		ob_end_clean();

		//if($i % 2 == 1) echo "<div style=\"page-break-after: always;\">";
		
		echo "<div style=\"width: 460px; height: 620px; margin-right: 20px; page-break-inside: avoid;\" class=\"fl\">
			<h1 class=\"center\" style=\"margin-right: 10px;\">KVITAS</h1><div style=\"float: right\">f.4</div>";
		echo $duom;
		echo "<p class=\"center\" style=\"clear:left;\">-------------------------------------------------------------------------------------------</p>
			<h1 class=\"center\" style=\"margin-right: 10px;\">PRANEŠIMAS</h1><div style=\"float: right\">f.4</div>";//line-height: 3px;
		echo $duom;
		echo "</div>";
		if ($i % 2 == 0 ) {
			echo "<div class=\"cl\" style=\"page-break-after: always;\"></div>";
			//echo "<br><br>";//</div>";//<br><br><br> - dėl Chrome //Chrome page-break-after: always doesn't work, but works with addition of page-break-inside: avoid before
			$i = 0;
		}
		$i++;

		if(isset($_GET['save'])) {
			$result2 = db_query("SELECT * FROM ".DB_payments." WHERE `kindergarten_id`=".DB_ID.", vaiko_id=".(int)$row['vaiko_id']." AND metai=".$year." AND menuo=".$month);
			if(!mysqli_num_rows($result2)) {
				if(db_query("INSERT INTO ".DB_payments." SET `kindergarten_id`=".DB_ID.", vaiko_id=".(int)$row['vaiko_id'].", metai=".$year.", menuo=".$month.", uzDarzeli=".(double)$uzDarzeli.", ugdymoReikmem=".(double)$ugdymoReikmem.", isViso=".(double)$isViso.", arApmoketa=0"))
					echo '<div class="green center">Išsaugota</div>';
				else logdie(mysqli_error($db_link));
			} else echo '<div class="red center">Toks mokėjimas duombazėje jau numatytas!</div>';
		}
	}
}
?>
</div>
