<?php if(!defined('DARBUOT')) exit();

//if(isset($_POST['pavadinimas']) && !preg_match("/^[A-Ž]+$/i", $_POST['pavadinimas'])) logdie("Pavadinimas gali būti sudarytas tik iš raidžių. ".back());
?>
<h1><!-- Papildomų -->Neformaliojo ugdymo veiklų pavadinimų sąrašas</h1>
<div id="content">
<?php
if(isset($_GET['issaugoti'])) {
	if (!mysqli_query($db_link, "INSERT INTO `".DB_bureliai."` SET `pavadinimas`='".db_fix($_POST['pavadinimas'])."', `createdByUserId`='".USER_ID."', `createdByEmployeeId`='".DARB_ID."'")) {
		logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
	} else msgBox('OK', "Grupė sėkmingai išsaugota!");
}
if(isset($_GET['atnaujinti'])) {
	if (!mysqli_query($db_link, "UPDATE `".DB_bureliai."` SET `pavadinimas`='".db_fix($_POST['pavadinimas'])."',
		`updated`=CURRENT_TIMESTAMP, `updatedByUserId`='".USER_ID."', `updatedByEmployeeId`='".DARB_ID."',
		`updatedCounter`=`updatedCounter`+1 WHERE `ID`=".(int)$_GET['atnaujinti'])) {
		logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
	} else msgBox('OK', "Grupė sėkmingai atnaujinta!");
}
if(isset($_GET['edit'])) {
	$result = db_query("SELECT * FROM `".DB_bureliai."` WHERE `ID`=".(int)$_GET['edit']);
	$burelis = mysqli_fetch_assoc($result);
}
if(isset($_GET['delete'])) {
	if (!mysqli_query($db_link, "DELETE FROM `".DB_bureliai."` WHERE `ID`=".(int)$_GET['delete'])) {
		logdie('Neteisinga užklausa: ' . mysqli_error($db_link));
	} else msgBox('OK', "Grupės informacija <strong>ištrinta</strong>!");
}
?>

<a href="?#activity-form" class="no-print fast-action fast-action-add">Įvesti naują papildomos veiklos pavadinimą</a> <?=ui_print()?>
<table>
<tr>
	<th>Neformaliojo ugdymo veiklos pavadinimas</th>
	<th>Kiekis</th>
	<th class="no-print">Veiksmai</th>
</tr>
<?php
$cnt = array();
$result = db_query("SELECT `burelio_id`, COUNT(`burelio_id`) `cnt` FROM `".DB_bureliai_schedule."` GROUP BY `burelio_id`");
while ($row = mysqli_fetch_assoc($result))
	$cnt[$row['burelio_id']] = $row['cnt'];
//ORDER BY `".DB_bureliai."`.`pavadinimas` DESC
$result = db_query("SELECT * FROM `".DB_bureliai."`");
if(mysqli_num_rows($result) > 0) {
	while ($row = mysqli_fetch_assoc($result)) {
		echo "\t<tr>
		<td>".filterText($row['pavadinimas'])."</td>
		<td>".(isset($cnt[$row['ID']]) ? $cnt[$row['ID']] : '-')."</td>
		<td class=\"no-print\"><a href=\"?edit=".$row['ID']."\">Keisti pavadinimą</a>
			".(isset($cnt[$row['ID']]) && !ADMIN ? '' : "<a href=\"?delete=".$row['ID']."\" onclick=\"return confirm('Ar tikrai norite trinti?')\">Trinti</a>")."</td>
	</tr>";	
	}
}
?>
</table>
<script type="text/javascript">
/*
function validateTitle(thisForm) {
    var error="";
    var filter = /^[A-Ž -]+$/i;
    if (!filter.test(thisForm.pavadinimas.value)) {
        thisForm.pavadinimas.style.background = 'Yellow';
        error += "Pavadinimas gali būti sudarytas tik iš raidžių.\n";
    }

    if (error != "") {
        alert("Blogai užpildyta:\n" + error);
        return false;
    }

    return true;
}
*/
</script>

<fieldset style="margin-top: 40px;" class="no-print">
<legend id="activity-form"><?=(isset($_GET['edit']) ? /*Papildomos*/"Veiklos pavadinimo keitimas" : "Naujas pavadinimas <!-- papildomas --> neformaliojo ugdymo veiklos")?></legend>
<form action="?<?=(!isset($_GET['edit']) ? 'issaugoti' : 'atnaujinti='.(int)$burelis['ID'])?>" method="post" class="not-saved-reminder"><!--  onsubmit="return validateTitle(this)" -->
	<p><label><!-- Papildomos -->Veiklos pavadinimas<span class="required">*</span>: <input required="required" type="text" name="pavadinimas" value="<?=(isset($_GET['edit']) ? filterText($burelis['pavadinimas']) : '')?>"></label></p>

	<p><input type="submit" value="Išsaugoti" class="submit"></p>
</form>
</fieldset>
