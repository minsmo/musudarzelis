$(function(){
//	$( document ).tooltip();
	$( document ).tooltip({
		position: {
		my: "center bottom-20",
		at: "center top",
		using: function( position, feedback ) {
			$( this ).css( position );
			$( "<div>" )
			.addClass( "arrow" )
			.addClass( feedback.vertical )
			.addClass( feedback.horizontal )
			.appendTo( this );
		}
		}
	});
	$('#vaikai-tbl th, #vaikai-tbl td, #employee-tbl th, #employee-tbl td, #logins-tbl th, #logins-tbl td, .vertical-hover th, .vertical-hover td').mouseenter(function(){
		if( $(this).get(0).tagName == 'TH' ){
			var i = $(this).prevAll('th').length;
		}else if( $(this).get(0).tagName == 'TD' ){
			var i = $(this).prevAll('td').length;
		}

		$('#vaikai-tbl tr, #employee-tbl tr, #logins-tbl tr, .vertical-hover tr').each(function(index) {
			$(this).find('th:eq('+i+')').addClass('hover');
			//$('td:nth-child(' + (i) + ')').addClass('hover');
			//$($(".vertical-hover td")[index]).addClass('hover');
			//$($(".vertical-hover tr")[index]).addClass('hover');
			//elements = $("th, td");
			//elements.filter(":nth-child(" + targetIndex + ")").css("background-color", "pink");
			$(this).find('td:eq('+i+')').addClass('hover');
			
			//elements = $("th, td");
			//elements.filter(":nth-child(" + (i+1) + ")").addClass('hover');
		});
	}).mouseleave(function(){
		if( $(this).get(0).tagName == 'TH' ){
			var i = $(this).prevAll('th').length;
		}else if( $(this).get(0).tagName == 'TD' ){
			var i = $(this).prevAll('td').length;
		}

		$('#vaikai-tbl tr, #employee-tbl tr, #logins-tbl tr, .vertical-hover tr').each(function(index) {
			$(this).find('th:eq('+i+')').removeClass('hover');
			//$('td:nth-child(' + (i) + ')').removeClass('hover');
			//$($(".vertical-hover td")[index]).removeClass("hover");
			//$($(".vertical-hover tr")[index]).removeClass("hover");
			$(this).find('td:eq('+i+')').removeClass('hover');
			//elements = $("th, td");
			//elements.filter(":nth-child(" + (i+1) + ")").removeClass('hover');
		});
	});
	/*
	$('#tabelis-tbl th, #tabelis-tbl td').mouseenter(function(){
		if( $(this).get(0).tagName == 'TH' ){
			var i = $(this).prevAll('th').length;
		}else if( $(this).get(0).tagName == 'TD' ){
			var i = $(this).prevAll('td').length;
		}

		$('#tabelis-tbl tr#second').each(function(index) {
			$(this).find('th:eq('+i+')').addClass('hover');
			$(this).find('td:eq('+i+')').addClass('hover');
		});
	}).mouseleave(function(){
		if( $(this).get(0).tagName == 'TH' ){
			var i = $(this).prevAll('th').length;
		}else if( $(this).get(0).tagName == 'TD' ){
			var i = $(this).prevAll('td').length;
		}

		$('#tabelis-tbl tr#sec').each(function(index) {
			$(this).find('th:eq('+i+')').removeClass('hover');
			$(this).find('td:eq('+i+')').removeClass('hover');
		});
	});*/
	//------------------------------------------------------------------------
	$('.not-saved-reminder :input').change(function() {
		$('.not-saved').show();
	});
	$('.not-saved-reminder .submit').submit(function() {
		$('.not-saved').text('Saugoma. Prašome palaukti.').prop("disabled", true);;
		setTimeout(function(){
			$('.not-saved').text('Saugoma įtartinai ilgai. Paspaudus dar kartą, bus bandoma siųsti dar kartą.').prop("disabled", false);
		}, 3000);
	});
	
	var is_dirty = false;//, is_dirty_global = false;
	var report_msg = 'Gal kažką keitėte ir neišsaugojote naujai pakeistų/įvestų duomenų. Jei jų neišsaugosite nauji duomenys dings. Norint, kad jie išliktų turite likti šiame puslapyje (palikti tinklalapį atvertą jo neįkeliant iš naujo) ir šio puslapio apačioje spustelėti ant mygtuko „Išsaugoti“';
	//Jūs tik ką kažką parašėte ir neišsaugojote įvestų duomenų, jeigu išeisite jie dings. Norint, kad jie išliktų turite likti šiame puslapyje (palikti tinklalapį atvertą jo neįkeliant iš naujo) ir šio puslapio apačioje paspausti mygtuką „Išsaugoti“

	$(".new-input-form :input").change(function() {//input, #child-form textarea
		/*if(first) {
			$(window).bind("beforeunload", function() {
				if(is_dirty)
					return 'Jūs tik ką kažką parašėte ir neišsaugojote įvestų duomenų, jeigu išeisite jie dings, kad jie išliktų turite likti šiame puslapyje (neįkeliant šio puslapio iš naujo) ir šio puslapio apačioje paspausti mygtuką „Išsaugoti“';//confirm( "Jūs turite neišsaugotų pakeitimų, jeigu išeisite jie dings."
			});
			first = false;
		}*/
		/*if(first) {
			window.onbeforeunload = function() {
				return confirm('Jūs tik ką kažką parašėte ir neišsaugojote įvestų duomenų, jeigu išeisite jie dings, kad jie išliktų turite likti šiame puslapyje (neįkeliant šio puslapio iš naujo) ir šio puslapio apačioje paspausti mygtuką „Išsaugoti“');
			}
			first = false;
		}*/
		is_dirty = true;
		//is_dirty_global = true;
	});

	setTimeout(function() {
		//http://stackoverflow.com/questions/194101/what-is-the-best-way-to-track-changes-in-a-form-via-javascript
		function formUnloadPrompt(formSelector) {
			//var formSelector = 'form:not(#search_form)';
			var formA = $(formSelector).serialize(), formB, formSubmit = false;

			// Detect Form Submit
			$('form'/*formSelector*/).submit( function(){
				formSubmit = true;
			});

			// Handle Form Unload    
			window.onbeforeunload = function(){
				if (formSubmit) return;
				formB = $(formSelector).serialize();
				if (formA != formB || typeof CKEDITOR !== 'undefined' && typeof CKEDITOR.instances['content'] !== 'undefined' && CKEDITOR.instances['content'].checkDirty()) return "NEBUVO išsaugoti Jūsų pakeitimai (naujai įvesti duomenys).\n\nNorint išsaugoti turite:\n \t  1) likti šiame puslapyje (neįkeliant šio puslapio iš naujo, angliškai „Stay on this Page“) tada\n \t \t 2) šio puslapio apačioje spustelėti mygtuką „Išsaugoti“";
			};

			// Enable & Disable Submit Button
			/*var formToggleSubmit = function(){
				formB = $(formSelector).serialize();
				$(formSelector+' [type="submit"]').attr( "disabled", formA == formB);
			};
			formToggleSubmit();
			$(formSelector).change(formToggleSubmit);
			$(formSelector).keyup(formToggleSubmit);*/
		}
		formUnloadPrompt('.new-input-form, .not-saved-reminder');
		/*
		window.onbeforeunload = function (e) {
		//window.onunload
			if(is_dirty || typeof CKEDITOR.instances['content'] !== 'undefined' && CKEDITOR.instances['content'].checkDirty()) {
				var e = e || window.event;

				// For IE and Firefox prior to version 4
				if (e) {
					e.returnValue = report_msg;
				}

				// For Safari
				return report_msg;
			}
			//http://jonathonhill.net/2011-03-04/catching-the-javascript-beforeunload-event-the-cross-browser-way/
			//http://www.i-programmer.info/programming/htmlcss/5476-web-apps-in-the-browser-beforeunload.html
			//http://stackoverflow.com/questions/276660/how-can-i-override-the-onbeforeunload-dialog-and-replace-it-with-my-own
		
			//https://github.com/Aelios/crossbrowser-onbeforeunload/blob/master/crossbrowser-onbeforeunload.js
		};
		*/
	}, 1000);
	$('.new-input-form').submit(function() {
		//e.preventDefault();
		is_dirty = false;
		//$(window).unbind("beforeunload");
	});
	/*$('a').click(function(e) {//mousedown
		if(is_dirty_global) {
			// if the user navigates away from this page via an anchor link, 
			//	popup a new boxy confirmation.
			confirm("<?=$report?>");//answer = 
			e.preventDefault();
		}
	});*/
	//$('.green').fadeTo( 0, 1 );
	
	/*var check = true;
	$(document).on('submit', 'form', function(e) {
		
		//return true;
		//$(this).unbind('submit').submit();
		//var formData = new FormData($('#login'));
		var this_ = $(this);
		if(check) {
			//$(this).unbind('submit');
			e.preventDefault();
			$.ajax({
				url: '/logged.php',
				//processData: false,
				//contentType: false,
				type: 'GET',
				timeout: 1000//,
				//success: function(data){
				//}
			}).done(function (data) {
				console.log(data);
				if(data == 'OK') {
					//$(this).unbind('submit').submit();
					check = false;
					this_.unbind('submit').submit();
				} else {
					$('#autoLogout').show();
				}
			}).fail(function (jqXHR, textStatus) {
				console.log(jqXHR);
				console.log(textStatus);
				$('#autoLogout').show();//TODO: inform about availability
			});
			return false;//http://stackoverflow.com/questions/6318073/how-to-send-post-with-jquery-onsubmit-but-still-send-form
			//http://stackoverflow.com/questions/11172811/jquery-on-submit-doesnt-work
		} else {
			return true;
		}
	});*/
	
	//$('html').append(login);
});
var lastDONE = 0;
function DONE(msg) {
	$('#DONE').text(msg);
	$("#DONE").slideDown(1000);
	++lastDONE;
	var current = lastDONE; 
	setTimeout(function() {
		if(lastDONE == current)
			$("#DONE").slideUp(1000);
	}, 4000);
}
