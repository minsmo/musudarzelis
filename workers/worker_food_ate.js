$(function() {
	function eachCheckbox() {
		var attended = 0, not_attended = 0;
		$("input[type='checkbox']").each(function() {
			//var thisCheck = $(this);
			if ( this.checked ) {//thischeck.is(':checked')
				$(this).parent().removeClass("absent");
				$(this).parent().prev().removeClass("absent");
				$(this).parent().addClass("present");
				$(this).parent().prev().addClass("present");
				++attended;
			} else {
				$(this).parent().addClass("absent");
				$(this).parent().prev().addClass("absent");
				$(this).parent().removeClass("present");
				$(this).parent().prev().removeClass("present");
				++not_attended;
			}
			$('#counter').html('Valgė <strong>'+attended+'</strong>; Nevalgė <strong>'+not_attended+'</strong>; Iš viso <strong>'+(attended+not_attended)+'</strong>.'+(not_attended == 0 ? ' ☺' : ''));
		});
	}
	eachCheckbox();
	$( ".attendance-checkboxes" ).click(function() {
		setTimeout(eachCheckbox, 1);
	});
	$(":checkbox").click(function() {
		setTimeout(eachCheckbox, 1);
	});
});
function validateDate(thisForm) {
    var error="";
    var dataFilter = /^\d{4}-\d{1,2}-\d{1,2}$/;
    //if (!dataFilter.test(thisForm.data.value)) {
    //    thisForm.data.style.background = 'Yellow';
    if (!dataFilter.test(thisForm.zymeti.value)) {
        thisForm.zymeti.style.background = 'Yellow';
        error += "Blogas datos formatas: jis turi būti „****-**-**“.\n";
    }

    if (error != "") {
        alert("Blogai užpildyta:\n" + error);
        return false;
    }

    return true;
}
