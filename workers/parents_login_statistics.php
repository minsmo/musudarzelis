<?php if(!defined('DARBUOT') /*|| !DARBUOT not needed*/) exit(); ?>
<h1>Vaikų tėvų prisijungimų statistika</h1>
<div id="content">
	<?php
	$parents_logins = [];
	$result = db_query("SELECT * FROM `".DB_users_allowed."` JOIN `".DB_users."` ON `".DB_users_allowed."`.`user_id`=`".DB_users."`.`user_id` AND `".DB_users_allowed."`.`person_type`=0");
	while ($row = mysqli_fetch_assoc($result))
		$parents_logins[$row['person_id']][$row['person_subtype']] = $row;
	//person_subtype - 1 - mama, 2 - tėtis
	
	if(!empty($_GET['valid_from']))
		$_GET['valid_from'] = db_fix($_GET['valid_from']);
	else
		$_GET['valid_from'] = date('Y-m-d');
			
	if(ADMIN) {
		?>
		<form method="get" style="padding-bottom:10px;" onsubmit="return validateSearch(this)" id="search_form" class="no-print">
			<input type="hidden" name="ieskoti">
			<?php if(ADMIN) { ?>
			<div class="sel" style="float: left; margin-right: 5px;">
			<select name="grupes_id">
				<option value="0">Visos grupės</option>
				<?php
				foreach(getAllowedGroups() as $ID => $title)
					echo '<option value="'.$ID.'"'.(isset($_GET['ieskoti']) && isset($_GET['grupes_id']) && $ID == $_GET['grupes_id'] ? ' selected="selected"' : '').">".$title."</option>";
				?>
			</select>
			</div>
			<?php } ?>
			<span class="abbr" title="nurodytą dieną galiojusi vaiko informacija">Duomenų galiojimo data</span> <input class="datepicker" type="text" name="valid_from" value="<?=(!empty($_GET['valid_from']) ? filterText($_GET['valid_from']) : '')?>" placeholder="Galioja nuo" title="Vaiko informacija galiojanti nuo">
			<label><span class="abbr" title="Vaiko oficialiai nėra darželyje (sistemoje jis archyve) laikotarpiu nurodytu laukelyje „Duomenų galiojimo data“. Įprastai rodomi vaikai oficialiai esantys darželyje.">archyvas (išregistruotųjų)<!-- Tik išregistruoti vaikai --></span> <input type="checkbox" name="archyvas" value="1" <?=(!empty($_GET['archyvas']) && $_GET['archyvas'] ? 'checked="checked"' : '')?>></label>
			<input type="submit" value="Filtruoti" class="filter">
		</form>
		<?php
	}
	$ieskoti = '';
	if(isset($_GET['ieskoti'])) {
		if(!empty($_GET['grupes_id']) && $_GET['grupes_id'] != 0 && ADMIN) $ieskoti .= " AND cr.`grupes_id`=".(int)$_GET['grupes_id'];
		if(isset($_GET['archyvas'])) $ieskoti .= " AND cr.`archyvas`=".(int)$_GET['archyvas'];//Or just 1
		else $ieskoti .= ' AND cr.`archyvas`=0';
	} else {
		$ieskoti = ' AND cr.`archyvas`=0';
	}
//}

	$grupes = getAllGroups();
	$grupes[0] = 'Neįvesta';

	if(ADMIN)
		$sql_group_by_kid_id = "SELECT `parent_kid_id`, COUNT(`parent_kid_id`) AS `cnt` FROM `".DB_children."` GROUP BY `parent_kid_id`";
	else
		$sql_group_by_kid_id = "SELECT `parent_kid_id`, COUNT(`parent_kid_id`) AS `cnt` FROM `".DB_children."` WHERE `grupes_id`=".GROUP_ID." GROUP BY `parent_kid_id`";
	$res = db_query($sql_group_by_kid_id);
	$versions_counter = [];
	while ($row = mysqli_fetch_assoc($res))
		$versions_counter[$row['parent_kid_id']] = ($row['cnt'] == 1 ? '' : $row['cnt']);
	
	if(ADMIN)
		$sql = "SELECT cr.* FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<='".$_GET['valid_from']."' GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from` AND cr.`isDeleted`=0$ieskoti ORDER BY ".orderName('cr');
	else
		$sql = "SELECT cr.* FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` WHERE `valid_from`<='".$_GET['valid_from']."' GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from` AND cr.`isDeleted`=0 AND cr.`grupes_id`=".GROUP_ID."$ieskoti ORDER BY ".orderName('cr');

	if(ADMIN)
		$sql_all = "SELECT cr.* FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from` AND cr.`isDeleted`=0 ORDER BY ".orderName('cr');
	else
		$sql_all = "SELECT cr.* FROM `".DB_children."` cr JOIN (SELECT `parent_kid_id`, MAX(`valid_from`) `valid_from` FROM `".DB_children."` GROUP BY `parent_kid_id`) fi ON cr.`parent_kid_id`=fi.`parent_kid_id` AND cr.`valid_from`=fi.`valid_from` AND cr.`isDeleted`=0 AND cr.`grupes_id`=".GROUP_ID." ORDER BY ".orderName('cr');
	$res = db_query($sql_all);
	$all_kids_count = mysqli_num_rows($res);
	/*while ($row = mysqli_fetch_assoc($res)) {
		if($row['valid_from'] > date('Y-m-d')) {
		}
	}*/
	
	
	$result = db_query($sql);
	$kids_count = mysqli_num_rows($result);
	if($kids_count > 0) {

		if($kids_count != $all_kids_count)
			echo '<p class="no-print">Rodoma vaikų: '.$kids_count.'; Iš viso vaikų: '.$all_kids_count.'; Vadinasi nerodoma: '.($all_kids_count-$kids_count).'. <span class="abbr" title="Vaikai: jau archyvuoti ir nebelanko darželio arba jie ateityje pradės eiti į darželį">Kodėl taip būna?</span> <span class="abbr" title="Punkte „Vaikų paieška (filtravimas)“ galite: a) nustatyti metais vėlesnę datą nuo kurios vaiko informacija galios ir/arba b) pažymėti varnelę ant „archyvas/išregistruotų“.">Kaip matyti daugiau?</span></p>';
		?>
		<table id="vaikai-tbl"<?=(isset($_GET['edit']) ? ' class="no-print"' : '')?>>
		<tr>
			<th title="Eilutės numeris"><span class="abbr">#</span></th>
			<th>Vaikas</th>
			<th>Mama (globėja)</th>
			<th>Prisijun-<br>gimų k.</th>
			<th>Paskutinis prisijungimas</th>
			<th>Tėtis (globėjas)</th>
			<th>Prisijun-<br>gimų k.</th>
			<th>Paskutinis prisijungimas</th>
			<th>Grupė</th>
			<th title="Duomenų galiojančių nuo skirtingų datų kiekis rodomas jei yra daugiau nei 1" class="no-print"><span class="abbr">K</span></th>
			<th title="Ar archyve?"><span class="abbr">A</span><span style="font-size: 0px;">rchy-<br>vuotas</span></th>
		</tr>
		<?php
		$i = 0;
		while ($row = mysqli_fetch_assoc($result)) {
			echo "		<tr".(isset($_GET['edit']) && $_GET['edit'] == $row['ID'] ? ' class="opened-row"' : '').">
			<td>".++$i."</td>
			<td><a href=\"vaikai?edit=".$row['ID']."\">".filterText(getName($row['vardas'], $row['pavarde']))."</a>";//#child-form
		
			/*if(!KaunoVaikystesLobiai) {
				if(KaunoRBaibokyne) {
					$nuo_txt = array(
						0 => '',
						1 => '<span class="infoBox" title="10 % nuolaida">10</span>',//'10 % nuolaida',
						2 => '<span class="infoBox" title="20 % nuolaida">20</span>',//'20 % nuolaida',
						3 => '<span class="infoBox" title="50 % nuolaida">50</span>'//'50 % nuolaida'
					);
				} else {
					$nuo_txt = array(
						0 => '',
						1 => '<span class="infoBox" title="50 % nuolaida">50</span>',// '50 % nuolaida',
						2 => '<span class="infoBox" title="100 % nuolaida">100</span>',//'100 % nuolaida'
						3 => '<span class="infoBox" title="70 % nuolaida">70</span>',//'70 % nuolaida'
						4 => '<span class="infoBox" title="80 % nuolaida">80</span>',//'80 % nuolaida'
						5 => '<span class="infoBox" title="90 % nuolaida">90</span>',//'90 % nuolaida'
						6 => '<span class="infoBox" title="20 % nuolaida">20</span>'//'20 % nuolaida'
					);
				}
				echo "<br>
				<span class=\"infoBox\" title=\"".($row['arDarzelyje'] == 2 ? 'Priešmokyklinis">P' : ($row['arDarzelyje'] == 1 ? 'Darželyje">D' : 'Lopšelyje">L') )."</span>
				<span class=\"infoBox\" title=\"".($row['arPusryciaus']   ? 'Pusryčiaus">Pus' : 'Nepusryčiaus">___' )."</span>
				<span class=\"infoBox\" title=\"".($row['arPietaus']      ? 'Pietaus">Pie' : 'Nepietaus">___' )."</span>
				<span class=\"infoBox\" title=\"".($row['arPavakariaus'] ? 'Gaus pavakarius">Pavak' : 'Neužsakyti pavakariai">_____' )."</span>
				<span class=\"infoBox\" title=\"".($row['arVakarieniaus'] ? 'Vakarieniaus">Vak' : 'Nevakarieniaus">___' )."</span>
				".$nuo_txt[$row['arNuolaida']];
			}*/
		
			$m_set_email = isset($parents_logins[$row['parent_kid_id']][1]);
			$m_used = isset($parents_logins[$row['parent_kid_id']][1]) ? $parents_logins[$row['parent_kid_id']][1]['login_counter'] : 0;
			$m_td = $m_set_email ? ' class="abbr email'.($m_used ? '-accessed' : '').'" style="color: #000;"' : '';//Jau įvestas mamos el. pašto adresas
			$f_set_email = isset($parents_logins[$row['parent_kid_id']][2]);
			$f_used = isset($parents_logins[$row['parent_kid_id']][2]) ? $parents_logins[$row['parent_kid_id']][2]['login_counter'] : 0;
			$f_td = $f_set_email ? ' class="abbr email'.($f_used ? '-accessed' : '').'" style=""' : '';//Jau įvestas mamos el. pašto adresas
			echo "</td>
			<td$m_td>".($m_set_email ? '@' : '').filterText(getName($row['mamos_vardas'], $row['mamos_pavarde']))."</td>
			<td$m_td>".($m_used ? $m_used : '')."</td>
			<td$m_td>".($m_used ? $parents_logins[$row['parent_kid_id']][1]['last_login_time'] : ($m_set_email ? 'Jau įvestas el. paštas' : ''))."</td>
			
			<!-- <td>".(isset($parents_logins[$row['parent_kid_id']][1]) ? '<span class="abbr email'.($parents_logins[$row['parent_kid_id']][1]['login_counter'] ? '-accessed" title="Mama paskutinį kartą buvo prisijungus '.$parents_logins[$row['parent_kid_id']][1]['last_login_time'] : '" title="Jau įvestas mamos el. pašto adresas').'">@'.filterText(getName($row['mamos_vardas'], $row['mamos_pavarde'])).'</span>' : filterText(getName($row['mamos_vardas'], $row['mamos_pavarde'])))."
			</td> -->
			<td$f_td>".($f_set_email ? '@' : '').filterText(getName($row['tevo_vardas'], $row['tevo_pavarde']))."</td>
			<td$f_td>".($f_used ? $f_used : '')."</td>
			<td$f_td>".($f_used ? $parents_logins[$row['parent_kid_id']][2]['last_login_time'] : ($f_set_email ? 'Jau įvestas el. paštas' : ''))."</td>

			<td>".$grupes[$row['grupes_id']]."</td>
			<td class=\"no-print\">".$versions_counter[$row['parent_kid_id']]."</td>
			<td title=\"Ar archyve?\"><span class=\"abbr\">".($row['archyvas'] ? '+' : '-')."</span></td>
		</tr>";	
		}
		echo '</table>';
	}
?>
</div>
