<h1>Kontaktai</h1>
<div id="content">
	<!-- Kilus neaiškumams ar problemoms kurių nepavyksta išspręsti prašome susisiekite šiais kontaktais:<br> -->
	<h2>Su Mūsų Darželis komanda kontaktuokite<!-- Mūsų Darželis komandos pagrindiniai kontaktai -->:</h2>
	<dl>
		<dt>Rašykite:</dt>
		<dd><a href="mailto:info@musudarzelis.lt">info@musudarzelis.lt</a></dd>
	</dl>
	<dl>
		<dt>Skambinkite:</dt>
		<dd>
		Ramunė +3706 07 72 346<br>
		Kęstutis +3706 71 85 132</dd>
	</dl>
	<?php
	/* Šiuo metu tik esant labai
	 Kęstutis +3706 76 89209 +3706 71 85132 */
	
	if (isset($_POST['save'])) {
		$content = '<pre>'.filterText($_POST['content']).'</pre><br>'.$config['darzelio_pavadinimas'].'<br>'.
		filterText($person_type_sau[$_SESSION['USER_DATA']['person_type']].' '.$_SESSION['USER_DATA']['name'].' '.$_SESSION['USER_DATA']['surname']);
		if(sendemail('MusuDarzelis', "info@musudarzelis.lt", $_SESSION['USER_DATA']['name'].' '.$_SESSION['USER_DATA']['surname'], $_SESSION['USER_DATA']['email'], 'Per MD.lt '.filterText($_POST['title']), $content, 'html'))
			msgBox('OK', 'Sėkmingai išsiųsta.');
	} else {
	?>
	<form method="post">
		<h2>Siųsti laiškus į info@musudarzelis.lt galite ir per šią formą:</h2>
		<p>Tema: <input type="text" name="title" style="width: 400px;"></p>
		<p>Žinutė:<br><textarea name="content" style="width: 600px; height: 300px;"></textarea></p>
		<p><input type="submit" class="submit" name="save" value="Išsiųsti"></p>
	</form>
	<?php } ?>
</div>
